/** 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
 * 
 * */
package com.gps.hubble.ui;
public final class Constants {

	public static final String jsonContentType = "application/json";
	public static final String xmlContentType = "application/xml";
	public static final String INIT_SERVLETS_MAPPING = "/init/";
	public static final String TOKEN_COOKIE_NAME = "hubbletoken";
	public static final String LOGIN_COOKIE_NAME = "hubbleloginid";
	public static final String TRACE_COOKIE_NAME = "hubbletrace";
	public static final String CSRF_COOKIE_NAME = "hubblecsrf";
	public static final String CSRF_HEADER_NAME = "hubblecsrfheader";
	public static final String ORG_CODE = "currentorg";
	public static final String ORG_NAME = "orgname";
	public static final String FRAMEWORK_ELEM_NAME="framework";
	public static final String LOGIN_COOKIE_EMAIL_ID="hubblemailid";
	public static final String LTPA_COOKIE_NAME = "LtpaToken";
	public static final String LOGIN_COOKIE_TEAM_ID = "hubbleteamid";
	public static final String USER_NAME_COOKIE = "hubbleusername";
}

/** 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/
package com.gps.hubble.ui.utilities;

import java.util.Iterator;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * 
 * @author Murali
 * This class parses the input JSON and validates the same based on a pattern to ensure not all special characters are allowed.
 */
public class JsonParserUtil {
	
	private static Pattern pattern = Pattern.compile("^[a-zA-Z0-9.!@#&(),\\-\\/+=_:* \u007F-\uFFFF]*$");
	
	/**
	 * 
	 * @param jsonObject
	 * @return
	 */
	public ParserOutput validateJson(JSONObject jsonObject) {
		ParserOutput output = new ParserOutput();
		//default values
		output.setSuccess(true);
		return handleJson("root",jsonObject,output);
	}
	
	private ParserOutput handleJson(String path,JSONObject jsonObject,ParserOutput output) {
		
		if(!output.isSuccess()){
			return output;
		}
		if(jsonObject == null) {
			return output;
		}
						
		Set<String> attrs = jsonObject.keySet();

		for (Iterator<String> iterator = attrs.iterator(); iterator.hasNext();) {
			String attr = (String) iterator.next();
			
			Object value = jsonObject.get(attr);
			
			if (value instanceof String) {
				handleString(path+"."+attr,(String)value,output);
			}else if(value instanceof JSONArray){
				handleJsonArray(path,attr,(JSONArray)value,output);
			}else if(value instanceof JSONObject){
				handleJson(path+"."+attr,(JSONObject)value,output);
			}
		}
		return output;
	}
	
	private void populateOutputForFailure(ParserOutput output, String value, String path) {
		
		output.setPath(path);
		output.setValue(value);
		output.setSuccess(false);
	}

	private void handleJsonArray(String path,String attrName,JSONArray value,ParserOutput output) {
		
		//Need to check with vinay
		/*if(value.isEmpty()){
			output.setSuccess(false);
			return;
		}*/
		int count = 0;
		for (Iterator iterator = value.iterator(); iterator.hasNext();count++) {
			JSONObject object = (JSONObject) iterator.next();
			String pathSuffix = attrName+"["+count+"]";
			handleJson(path+"."+pathSuffix,object,output);
			if(!output.isSuccess()){
				return;
			}
		}
	}

	private void handleString(String attrName,String value,ParserOutput output) {
		Matcher matcher = pattern.matcher(value);
		if(!matcher.matches()) {
			populateOutputForFailure(output, value, attrName);
		}
	}

}

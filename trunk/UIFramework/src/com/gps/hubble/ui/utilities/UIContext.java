/** 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/
package com.gps.hubble.ui.utilities;

import java.net.HttpURLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.security.SecureRandom;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.gps.hubble.ui.AppInfo;
import com.gps.hubble.ui.Constants;


/**
 * 
 * @author Bridge
 *  This class maintains the UI Context for all the portal applications.
 *  This maintains the information in the form of cookies and the same are manipulated by this class.
 */
public class UIContext {

	private static final String UICONTEXT_ATTR_NAME = "uicontext";
	private static final String UI_STERLING_PORTAL = "SP_";
	private static final String UI_VENDOR_PORTAL = "VP_";
	private static final String UI_TELSTRA_PORTAL = "TP_";	
	private final static String DEFAULT_PRNG = "SHA1PRNG"; //algorithm to generate key
	
	private HttpServletRequest request;
	private HttpServletResponse response;
	private String loginid = null;
	private String token = null;
	private String userOrgCode=null;
	private String userOrgName= null;
	private String emailId = null;
	private String ltpaToken=null;
	private String portalIdentifier = null;
	private String csrfToken;
	private String teamId = "";
	private String userName = "";

	private int remoteServerErrorStatusCode = -1;
	private boolean remoteServerUnreachable = false;
	private boolean sso_mode = false;
	private boolean tracing = false;
	
	private String remoteServerErrorDescription; 

	private static final Logger logger = LogManager.getLogger();
	
	public static UIContext getUIContext(HttpServletRequest request, HttpServletResponse response) {
		Object context = request.getAttribute(UICONTEXT_ATTR_NAME);
		if (context != null) {
			return (UIContext) context;
		}
		UIContext newContext = new UIContext(request, response);
		request.setAttribute(UICONTEXT_ATTR_NAME, newContext);
		return newContext;
	}

	private static String generateCSRFToken() {
		SecureRandom sr;
		String basecsrfToken = null;
		String hashedCSRFToken = null;
		try {
			sr = SecureRandom.getInstance(DEFAULT_PRNG);
			basecsrfToken= Long.toString(sr.nextLong());
		} catch (NoSuchAlgorithmException e) {
			logger.catching(e);
		}
		if( basecsrfToken == null) {
			basecsrfToken = Long.toString(System.currentTimeMillis());
		}
		try{
	        MessageDigest md = MessageDigest.getInstance("SHA-256");
	        md.update(basecsrfToken.getBytes());
	        byte byteArr[] = md.digest();
	        StringBuffer sb = new StringBuffer();
	        for (int i = 0; i < byteArr.length; i++) {
	         sb.append(Integer.toString((byteArr[i] & 0xff) + 0x100, 16).substring(1));
	        }	        
	        hashedCSRFToken = sb.toString();
			
		} catch (NoSuchAlgorithmException e) {
			logger.catching(e);
		}
		if(hashedCSRFToken == null) {
			hashedCSRFToken = basecsrfToken; 
		}
		return hashedCSRFToken;
	}
	
	private UIContext(HttpServletRequest request, HttpServletResponse response) {
		this.request = request;
		this.response = response;
		this.setPortalContext();
		this.setupContext();
	}

	private void setupContext() {
		Cookie[] cookies = request.getCookies();
		if (cookies == null) {
			return;
		}
		for (int i = 0; i < cookies.length; i++) {
			Cookie cookie = cookies[i];
			String cookieName = Utils.getValidCookieName(cookie.getName(), getPortalIdentifier());
			if (Constants.TOKEN_COOKIE_NAME.equals(cookieName)) {
				this.token = cookie.getValue();
			} else if (Constants.LOGIN_COOKIE_NAME.equals(cookieName)) {
				this.loginid = cookie.getValue();
			} else if (Constants.LOGIN_COOKIE_EMAIL_ID.equals(cookieName)){
				this.emailId =cookie.getValue();
			} else if (Constants.TRACE_COOKIE_NAME.equals(cookieName)) {
				this.tracing = Utils.getBooleanValue(cookie.getValue());
			} else if (Constants.CSRF_COOKIE_NAME.equals(cookieName)) {
				this.csrfToken = cookie.getValue();
			} else if(Constants.ORG_CODE.equals(cookieName)) {
				  this.userOrgCode = cookie.getValue();
			}else if(Constants.ORG_NAME.equals(cookieName)) {
				  this.userOrgName = cookie.getValue();
			}else if(Constants.LTPA_COOKIE_NAME.equals(cookie.getName())) {
				this.setLtpaToken(cookie.getValue());
			} else if (Constants.LOGIN_COOKIE_TEAM_ID.equals(cookieName)){
				this.teamId =cookie.getValue();
			} else if (Constants.USER_NAME_COOKIE.equals(cookieName)){
				this.userName =cookie.getValue();
			}
			
		}
	}

	public String getToken() {
		return token;
	}

	public String getLoginid() {
		return loginid;
	}

	public String getEmailId(){
		return emailId;
	}

	public String getCsrfToken() {
		return csrfToken;
	}
	
	public String getTeamId(){
		return teamId;
	}
	
	public String getUserOrgName(){
		return userOrgName;
	}
	
	/* 
	 * Method setToken, set the token to cookie name 
	 * prefixed with associated portal Identifiers
	 */

	public void setToken(String token) {
		this.token = token;
		Cookie cookie = new Cookie(getPortalIdentifier() + Constants.TOKEN_COOKIE_NAME, token);
		cookie.setMaxAge(-1);
		cookie.setPath("/");
		cookie.setHttpOnly(true);
		response.addCookie(cookie);
	}

	/* 
	 * Method setToken, set the login Id to cookie name 
	 * prefixed with associated portal Identifiers
	 */
	public void setLoginid(String loginid) {
		this.loginid = loginid;
		Cookie cookie = new Cookie(getPortalIdentifier() + Constants.LOGIN_COOKIE_NAME, loginid);
		cookie.setMaxAge(-1);
		cookie.setPath("/");
		cookie.setHttpOnly(true);
		response.addCookie(cookie);
	}
	
	public void setEmailId(String mailId){
		this.emailId=mailId;
		Cookie cookie =new Cookie(getPortalIdentifier() +Constants.LOGIN_COOKIE_EMAIL_ID, mailId);
		cookie.setMaxAge(-1);
		cookie.setPath("/");
		cookie.setHttpOnly(true);
		response.addCookie(cookie);
		
	}	

	public void setTeamId(String teamId){
		this.teamId=teamId;
		Cookie cookie =new Cookie(getPortalIdentifier() +Constants.LOGIN_COOKIE_TEAM_ID, teamId);
		cookie.setMaxAge(-1);
		cookie.setPath("/");
		cookie.setHttpOnly(true);
		response.addCookie(cookie);
		
	}	
	
	/* 
	 * Method setUserOrgCode, set the Organization code to cookie name 
	 * prefixed with associated portal Identifiers
	 */
	public void setUserOrgCode(String userOrgCode) {
		this.userOrgCode = userOrgCode;
		Cookie cookie = new Cookie(getPortalIdentifier() + Constants.ORG_CODE,userOrgCode );
		cookie.setMaxAge(-1);
		cookie.setPath("/");
		cookie.setHttpOnly(true);;
		response.addCookie(cookie);
	}
	
	/* 
	 * Method setUserOrgName, set the Organization code to cookie name 
	 * prefixed with associated portal Identifiers
	 */
	public void setUserOrgName(String userOrgName) {
		this.userOrgName = userOrgName;
		Cookie cookie = new Cookie(getPortalIdentifier() + Constants.ORG_NAME,userOrgName );
		cookie.setMaxAge(-1);
		cookie.setPath("/");
		cookie.setHttpOnly(true);;
		response.addCookie(cookie);
	}	
	
	/* 
	 * Method setUsername, set the Organization code to cookie name 
	 * prefixed with associated portal Identifiers
	 */
	public void setUsername(String userName) {
		this.userName = userName;
		Cookie cookie = new Cookie(getPortalIdentifier() + Constants.USER_NAME_COOKIE,userName );
		cookie.setMaxAge(-1);
		cookie.setPath("/");
		cookie.setHttpOnly(true);;
		response.addCookie(cookie);
	}
	
	
	
	/* 
	 * Method setToken, set the CSRF token to cookie name 
	 * prefixed with associated portal Identifiers
	 */
	public void createCSRFToken() {
		csrfToken = generateCSRFToken();
		
		Cookie cookie = new Cookie(getPortalIdentifier() + Constants.CSRF_COOKIE_NAME, csrfToken);
		cookie.setMaxAge(-1);
		cookie.setPath("/");
		cookie.setHttpOnly(true);
		response.addCookie(cookie);
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public boolean isValidRequest() {
		
		if(isSso_mode()) {
			
			Principal oUserPrincipal = request.getUserPrincipal();
			String sPrincipalName = null;
			if(oUserPrincipal != null){
				sPrincipalName = oUserPrincipal.getName();
				if((sPrincipalName != null)){
					
					this.loginid = sPrincipalName;
					
				}else {
					 logger.error("getUserPrincipal Name is Null");
		  			 return false; 
				}
				
			}else {
				 logger.error("getUserPrincipal is Null");
	  			 return false; 
				
			}
            
			
		}
		//TODO has to be resolved
		/*if (getLoginid() == null || getToken() == null || getCsrfToken() == null) {
			return false;
		}*/
		if(logger.isDebugEnabled()) {
			logger.debug("Validating Request.. CSRF Token from Header "+request.getHeader(Constants.CSRF_HEADER_NAME));
			logger.debug("Validating Request.. CSRF Token from Request Param "+request.getParameter(Constants.CSRF_COOKIE_NAME));
			logger.debug("Validating Request.. CSRF Token from Cookie "+csrfToken);
			
		}
		
		String reqCSRFToken = request.getHeader(Constants.CSRF_HEADER_NAME);
		if (reqCSRFToken == null) {
			reqCSRFToken = request.getParameter(Constants.CSRF_COOKIE_NAME);
		}
		
		if (reqCSRFToken == null) {
			logger.error("CSRF Token is invalid for loginid {}", getLoginid());
			return false;
		}
		//String decodedCSRFToken = new String(Base64.decode(reqCSRFToken));
		if (!reqCSRFToken.equals(csrfToken)) {
			logger.error("CSRF Token is invalid for loginid {}", getLoginid());
			return false;
		}

		return true;
	}

	/* 
	 * Method to set the portal context for the UI
	 * Assumption is Context path for Sterling Portal Should be Start with 'S' and
	 * for Vendor Portal Should Start with 'V'
	 */
	
	private void setPortalContext() {
		
	    String appCode = AppInfo.getIdentifier();
	    if ((appCode != null) && ("SP".equalsIgnoreCase(appCode)))
	    	this.setPortalIdentifier(UI_STERLING_PORTAL);
	    else if ((appCode != null) && ("VP".equalsIgnoreCase(appCode)))
	    	this.setPortalIdentifier(UI_VENDOR_PORTAL);
	    else if ((appCode != null) && ("TP".equalsIgnoreCase(appCode)))
	    	this.setPortalIdentifier(UI_TELSTRA_PORTAL);
	    
	}
	
	public boolean isRemoteServerUnreachable() {
		return remoteServerUnreachable;
	}

	public void setRemoteServerUnreachable(boolean remoteServerUnreachable) {
		this.remoteServerUnreachable = remoteServerUnreachable;
	}
	
	public int getRemoteServerErrorStatusCode() {
		return remoteServerErrorStatusCode;
	}

	public void setRemoteServerErrorStatusCode(int errorStatusCode) {
		this.remoteServerErrorStatusCode = errorStatusCode;

		if (errorStatusCode != HttpURLConnection.HTTP_BAD_REQUEST) {
			return;
		}
		
		// TODO handle invalid 400 errors from remote server
		
//		Cookie tokenCookie = new Cookie(TOKEN_COOKIE_NAME, "");
//		tokenCookie.setMaxAge(0);
//		tokenCookie.setValue("");
//		tokenCookie.setPath("/");
//		response.addCookie(tokenCookie);
//		
//		Cookie loginCookie = new Cookie(LOGIN_COOKIE_NAME, "");
//		loginCookie.setMaxAge(0);
//		loginCookie.setValue("");
//		loginCookie.setPath("/");
//		response.addCookie(loginCookie);
	}

	public boolean isTracing() {
		return tracing;
	}

	public void setTracing(boolean trace) {
		if (tracing == trace) {
			return;
		}
		tracing = trace;
		Cookie cookie = null;
		if (tracing) {
			cookie = new Cookie(getPortalIdentifier()+Constants.TRACE_COOKIE_NAME, "y");
			cookie.setHttpOnly(true);
			cookie.setPath("/");
			// Enable tracing for 10 minutes
			cookie.setMaxAge(600);
		} else {
			cookie = new Cookie(getPortalIdentifier()+Constants.TRACE_COOKIE_NAME, "n");
			cookie.setMaxAge(0);
		}
		response.addCookie(cookie);
	}

	public String getRedirectUrl(String url) {
		String urlWithCSRFToken = null; 
	//	String encodedCSRFToken = Base64.encode(getCsrfToken().getBytes());
		//getCsrfToken()
		if (url.contains("?")) {
			urlWithCSRFToken = url + "&" + Constants.CSRF_COOKIE_NAME + "=" + getCsrfToken();
		} else {
			urlWithCSRFToken = url + "?" + Constants.CSRF_COOKIE_NAME + "=" + getCsrfToken();
		}
		return urlWithCSRFToken;
	}

	
	
	public String getUserOrgCode() {
		return userOrgCode;
	}
	
	public String getUserName() {
		return userName;
	}	
	
	public String getRemoteServerErrorDescription() {
		return remoteServerErrorDescription;
	}

	public void setRemoteServerErrorDescription(String remoteServerErrorDescription) {
		this.remoteServerErrorDescription = remoteServerErrorDescription;
	}

	public String getLtpaToken() {
		return ltpaToken;
	}

	public void setLtpaToken(String ltpaToken) {
		this.ltpaToken = ltpaToken;
		Cookie cookie = new Cookie(Constants.LTPA_COOKIE_NAME, ltpaToken );
		cookie.setMaxAge(-1);
		cookie.setPath("/");
		cookie.setHttpOnly(true);
		cookie.setSecure(true);
		response.addCookie(cookie);
		
	}

	public boolean isSso_mode() {
		return sso_mode;
	}

	public void setSso_mode(boolean sso_mode) {
		this.sso_mode = sso_mode;
	}

	public String getPortalIdentifier() {
		
		if(portalIdentifier != null)
			return portalIdentifier;
		else
		    return "";
	}

	public void setPortalIdentifier(String portalIdentifier) {
		this.portalIdentifier = portalIdentifier;
	}

	

}

/** 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/
package com.gps.hubble.ui.utilities;

import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.w3c.dom.Element;

/**
 * 
 * @author Bridge
 *  Util class to parse JSON
 */
public class JsonUtils {

	private static final Logger logger = LogManager.getLogger();
	
	public static final JSONObject createJsonObjectFromFile(String file) {
		
		JSONParser parser = new JSONParser();
		JSONObject jsonObject = new JSONObject();
		try {
			InputStream inputStream = JsonUtils.class.getResourceAsStream(file);
			
			jsonObject = (JSONObject) parser.parse(IOUtils.toString(inputStream,"UTF-8"));
		} catch (Exception e) {
			logger.catching(e);
		}

		return jsonObject;
	}
	
	public static final boolean getBooleanAttribute(JSONObject obj, String attrName, boolean defaultValue) {
		if (!obj.containsKey(attrName)) {
			return defaultValue;
		}
		String value = (String) obj.get(attrName);
		return Utils.getBooleanValue(value);
	}
	
	public static final int getIntAttribute(JSONObject obj, String attrName, int defaultValue) {
		if (!obj.containsKey(attrName)) {
			return defaultValue;
		}
		String value = (String) obj.get(attrName);
		return Utils.getIntValue(value, defaultValue);
	}
	
	public static final boolean isVoid(JSONObject obj){
		if(obj == null || obj.isEmpty()){
			return true;
		}
		return false;
	}

}

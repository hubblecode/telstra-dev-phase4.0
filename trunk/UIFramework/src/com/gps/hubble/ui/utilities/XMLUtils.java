/** 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/
package com.gps.hubble.ui.utilities;


import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * 
 * @author Bridge
 *  Generic Utility class to parse XML
 */
public class XMLUtils {
	private static final Logger logger = LogManager.getLogger();
	
	public static final Element getChildElement(String childName, Element parent) {
		NodeList childList = parent.getElementsByTagName(childName);
		if (childList == null || childList.getLength() == 0) {
			return null;
		}

		Node childNode = childList.item(0);
		if (childNode instanceof Element) {
			return (Element) childNode;
		}
		return null;
	}

	public static final ArrayList<Element> getChildElements(String childName, Element parent) {
		NodeList childList = parent.getElementsByTagName(childName);
		if (childList == null || childList.getLength() == 0) {
			return null;
		}

		ArrayList<Element> childElemList = new ArrayList<>();

		int childCount = childList.getLength();

		for (int index = 0; index < childCount; index++) {
			Node childNode = childList.item(index);
			if (childNode instanceof Element) {
				childElemList.add((Element) childNode);
			}
		}
		return childElemList;
	}

	public static final List<Element> getChildElements(Element parent) {
		NodeList childList = parent.getChildNodes();
		if (childList == null || childList.getLength() == 0) {
			return null;
		}

		List<Element> childElemList = new ArrayList<>();

		int childCount = childList.getLength();

		for (int index = 0; index < childCount; index++) {
			Node childNode = childList.item(index);
			if (childNode instanceof Element) {
				childElemList.add((Element) childNode);
			}
		}
		return childElemList;
	}

	public static final Document createDocumentFromElement(Element element) {
		DocumentBuilder builder = getDocumentBuilder();

		Document doc = builder.newDocument();
		Node inpElem=doc.importNode(element, true);
		doc.appendChild(inpElem);
		return doc;
	}

	public static final Document createDocumentFromFile(String file) {
		DocumentBuilder builder = getDocumentBuilder();
		Document doc = null;
		try {
			doc = builder.parse(XMLUtils.class.getResourceAsStream(file));
		} catch (Exception e) {
			logger.catching(e);
		}

		return doc;
	}

	public static final Document createDocumentWithNodeNamed(String name) {
		DocumentBuilder builder = getDocumentBuilder();
		Document doc = null;
		try {
			doc = builder.newDocument();
			Element root = doc.createElement(name);
			doc.importNode(root, true);
			doc.appendChild(root);
		} catch (Exception e) {
			logger.catching(e);
		}

		return doc;
	}
	
	public static final Document clone(Document doc) {
		DocumentBuilder db = getDocumentBuilder();
		Document clonedDocument = db.newDocument();
		Node clonedRoot = clonedDocument.importNode(doc.getDocumentElement(), true);
		clonedDocument.appendChild(clonedRoot);
		return clonedDocument;
	}

	public static final void copyElement(Element parent, Element toBeCopied) {
		Document parentDoc = parent.getOwnerDocument();
		Node importedNode = parentDoc.importNode(toBeCopied, true);
		parent.appendChild(importedNode);
	}

	private static DocumentBuilder getDocumentBuilder() {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;

		try {
			builder = dbf.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			logger.catching(e);
		}
		return builder;
	}

	public static final boolean getBooleanAttribute(Element elem, String attrName, boolean defaultValue) {
		if (!elem.hasAttribute(attrName)) {
			return defaultValue;
		}
		String value = elem.getAttribute(attrName);
		return Utils.getBooleanValue(value);
	}

	public static final int getIntAttribute(Element elem, String attrName, int defaultValue) {
		if (!elem.hasAttribute(attrName)) {
			return defaultValue;
		}
		String value = elem.getAttribute(attrName);
		return Utils.getIntValue(value, defaultValue);
	}
	
	public static String getString(Document doc) {
		try {
		    Transformer transformer = TransformerFactory.newInstance().newTransformer();
		    StreamResult result = new StreamResult(new StringWriter());
		    DOMSource source = new DOMSource(doc);
		    transformer.transform(source, result);
		    return result.getWriter().toString();
		} catch(TransformerException e) {
		    logger.catching(e);
		    return null;
		}
	}
}

/** 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/
package com.gps.hubble.ui.utilities;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.gps.hubble.ui.AppInfo;
import com.gps.hubble.ui.dataService.CompositeDataService;
import com.gps.hubble.ui.dataService.DataService;
import com.gps.hubble.ui.dataService.DataServiceDef;
import com.gps.hubble.ui.dataService.DataServiceManager;
import com.gps.hubble.ui.dataService.IDataService;

/**
 * 
 * @author Bridge
 * Utils to invoke Data Services
 */
public class DataServiceUtils {
	
	private static final Logger logger = LogManager.getLogger();
	
	public static JSONObject invokeDataService(String dataServiceId, JSONObject input, UIContext context) {
	    JsonParserUtil util = new JsonParserUtil();
		IDataService dataService = getDataService(dataServiceId, context);
		if (dataService == null) {
			context.setRemoteServerErrorStatusCode(HttpURLConnection.HTTP_NOT_FOUND);
			return null;
		}
		ParserOutput isValidJson = new ParserOutput();
        if(!dataService.getDataServiceDef().isComposite() && dataService.getDataServiceDef().isValidationRequired()) {
          isValidJson = util.validateJson(input);
        } else {
          isValidJson.setSuccess(true);
        }
		JSONObject output = null;
		if(isValidJson.isSuccess()) {
		  output = dataService.execute(input);
		} else {
			logger.debug("JSON is invalid");
			context.setRemoteServerErrorStatusCode(HttpURLConnection.HTTP_BAD_REQUEST);
			//Constructing our own Error message
			JSONObject errorObj = new JSONObject();
			JSONArray errorArr= new JSONArray();
			JSONObject errorDesc = new JSONObject();
			errorDesc.put("ErrorCode", "TEL-UI0001");
			errorDesc.put("ErrorDescription", "Invalid Input. Input had special characters, which could not be processed. Please remove the same and try again.");
			errorArr.add(errorDesc);
			errorObj.put("errors", errorArr);
			String errorDetails = errorObj.toJSONString();
			context.setRemoteServerErrorDescription(errorDetails);
		}
		return output;
	}

	public static IDataService getDataService(String dataServiceId, UIContext context) {
		DataServiceDef dataServiceDef = getDataServiceDef(dataServiceId, context);
		if (dataServiceDef == null) {
			return null;
		}
		IDataService dataService = getDataServiceImpl(dataServiceDef, context);
		dataService.setDataServiceDef(dataServiceDef);
		dataService.setUIContext(context);
		return dataService;
	}

	private static IDataService getDataServiceImpl(DataServiceDef dataServiceDef, UIContext context) {
		if(dataServiceDef.isComposite()){
			return new CompositeDataService();
		}else{
			return new DataService();
		}
	}

	private static DataServiceDef getDataServiceDef(String dataServiceId, UIContext context) {
		return DataServiceManager.getDataServiceDef(dataServiceId);
	}
	
	public static JSONObject getJSONObject(String json) {
		if (json == null) {
			return new JSONObject();
		}
		try {
			JSONParser parser = new JSONParser();
			JSONObject jsonOutput = (JSONObject) parser.parse(json);
			return jsonOutput;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String getDataServiceId(UIContext context) {
		String path = context.getRequest().getPathInfo();
		String dataServiceId = path.replaceFirst("/", "");
		return dataServiceId;
	}

	public static String getDataServiceInput(UIContext context, String dataServiceId) {
		return context.getRequest().getParameter(dataServiceId);
	}
	
	public static void logoutUser(UIContext context,String sessionTimeOut) {
		JSONObject logoutInput = new JSONObject();
		logoutInput.put("UserId", context.getLoginid());
		if(AppInfo.isSsomode()) {
			if(context.getLtpaToken() != null) {
				logoutInput.put("SessionId", context.getLtpaToken());
			}
		}else {
			if(context.getToken() != null) {
				logoutInput.put("SessionId", context.getToken());
			}
		}
		
		if(sessionTimeOut != null && "Y".equals(sessionTimeOut)) {
			logoutInput.put("LogoutType", "T");
		}else {
			logoutInput.put("LogoutType", "N");
		}
		DataServiceUtils.invokeDataService("logout",logoutInput,context);
		HttpServletRequest request = context.getRequest();
		HttpServletResponse response = context.getResponse();
		Cookie[] cookies = request.getCookies();
		for(Cookie reqCookie : cookies) {
			reqCookie.setMaxAge(0);
			reqCookie.setPath("/");
			reqCookie.setValue("");
			if(logger.isDebugEnabled()) {
				logger.debug("Deleting Cookie "+reqCookie.getName());
			}
			
			response.addCookie(reqCookie);
		}
		
	}
	
	public static List<String> getResourceListForUser(UIContext context) {
	    try{
	      JSONObject inputObj = new JSONObject();
	      List<String> list = new ArrayList<String>();
	      logger.debug("Invoking getResourceListForUser");
	      JSONObject object = invokeDataService("resourcePermission", inputObj, context);
	      if(object!=null) {
		      Iterator<String> keysItr = object.keySet().iterator();
		      while(keysItr.hasNext()){
		        String key = keysItr.next();
		        JSONArray resourceListArray = (JSONArray)object.get(key);
		        for(int i=0 ; i < resourceListArray.size();i++) {
		          String resourceId = resourceListArray.get(i).toString();
		          JSONObject finalJsonObject = DataServiceUtils.getJSONObject(resourceId);
		          Iterator<String> resourceIdItr = finalJsonObject.keySet().iterator();
		          while(resourceIdItr.hasNext()){
		            String finalKey = resourceIdItr.next();
		            String resId = finalJsonObject.get(finalKey).toString();
		            list.add(resId);
		          }
		        }   
		      }
	      }
	      return list;
	    } catch (Exception e) {
	      throw e;
	    }
	  }
}

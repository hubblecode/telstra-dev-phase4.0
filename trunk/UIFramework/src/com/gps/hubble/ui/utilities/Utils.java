/** 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/
package com.gps.hubble.ui.utilities;

/**
 * 
 * @author Bridge
 *  Generic Utility class used by the UI applications.
 */
public class Utils {

	public static final boolean getBooleanValue(String value) {
		if (value == null) {
			return false;
		}
		String lowerCaseValue = value.toLowerCase();
		if ( "y".equals(lowerCaseValue) || "yes".equals(lowerCaseValue) || "true".equals(lowerCaseValue) ) {
			return true;
		}
		return false;
	}

	public static final int getIntValue(String value, int defaultValue) {
		if (value == null) {
			return defaultValue;
		}
		return new Integer(value);
	}
	
	public static final String getValidCookieName(String sCookieName, String sPortalIdentifier) {
		if((sCookieName.indexOf(sPortalIdentifier)) == 0){
			return sCookieName.substring(sPortalIdentifier.length());  
		}
		return "";
	}
	
}

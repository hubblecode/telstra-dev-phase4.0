/** 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/
package com.gps.hubble.ui.filters;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.Enumeration;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.gps.hubble.ui.AppInfo;
import com.gps.hubble.ui.Constants;
import com.gps.hubble.ui.utilities.UIContext;

/*
@WebFilter(dispatcherTypes = {DispatcherType.REQUEST }
, description = "SSO Authorization Filter", urlPatterns = { "/api/*", "/init/*" })
*/
public class SSOAuthorizationFilter implements Filter {

	private static final Logger logger = LogManager.getLogger();
	
	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		if (request instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) request;
			HttpServletResponse httpResponse = (HttpServletResponse) response;
			UIContext context = UIContext.getUIContext(httpRequest, httpResponse);
			context.setSso_mode(AppInfo.isSsomode());
			boolean tracing = context.isTracing();
			String path = httpRequest.getRequestURI();
			if(tracing) {
				logger.info("Authorization filter called for path: {}", path);
				printData(httpRequest,httpResponse);
			}
			
			if (path.contains(Constants.INIT_SERVLETS_MAPPING)) {
				logger.error("Invalid request path {} invoked by loginid {}", path, context.getLoginid());
				httpResponse.setStatus(HttpURLConnection.HTTP_FORBIDDEN);
				return;
			}
			if (context.isValidRequest()) {
				logger.info("Authorization filter succeeded");
				chain.doFilter(request, response);
			} else {
				logger.info("Authorization filter failed, redirecting to login page.");
				httpResponse.setStatus(HttpURLConnection.HTTP_UNAUTHORIZED);
				//String logoutUrl = httpRequest.getContextPath()+"/pkmslogout";
				//httpResponse.sendRedirect(logoutUrl);
			}
		} else {
			chain.doFilter(request, response);
		}
		

	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub

	}
	
	private void printData(HttpServletRequest request,HttpServletResponse response) {
		if(request.getRemoteUser() !=null) {
			logger.debug("Inside Login.. Remote User is "+request.getRemoteUser());
		}
		if(request.getUserPrincipal() !=null) {
			logger.debug("Inside Login.. user Principal is "+request.getUserPrincipal().getName());
		}
		Enumeration<String> headerNames = request.getHeaderNames();
		logger.debug("Printing Headers...");
		while(headerNames.hasMoreElements()) {
			String headerName=(String)headerNames.nextElement();
			logger.debug("Header  "+ headerName +" Value "+request.getHeader(headerName));
		}
		logger.debug("Done Printing Headers ");
		
		Cookie[] cookies = request.getCookies();
		logger.debug("Printing Cookies...");
		if(cookies != null) {
		for(int i=0;i<cookies.length;i++) {
				Cookie reqCookie =cookies[i];
				logger.debug("Cookie Name "+reqCookie.getName());
				logger.debug("Cookie Value "+reqCookie.getValue());
				logger.debug("Cookie Path "+reqCookie.getPath());
				logger.debug("Cookie Max Age "+reqCookie.getMaxAge());
				//response.addCookie(reqCookie);
			}
		}
		logger.debug("Done Printing Cookies ");
		
	}
}

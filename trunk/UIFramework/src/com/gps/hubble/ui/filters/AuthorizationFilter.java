/** 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/
package com.gps.hubble.ui.filters;

import java.io.IOException;
import java.net.HttpURLConnection;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.gps.hubble.ui.AppInfo;
import com.gps.hubble.ui.Constants;
import com.gps.hubble.ui.utilities.UIContext;

/**
 * Servlet Filter implementation class AuthorizationFilter
 *
@WebFilter(dispatcherTypes = {DispatcherType.REQUEST }
					, description = "Authorization Filter", urlPatterns = { "/api/*", "/init/*"})
*/
public class AuthorizationFilter implements Filter {
	private static final Logger logger = LogManager.getLogger();
	
    /**
     * Default constructor. 
     */
    public AuthorizationFilter() {
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		if (request instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) request;
			HttpServletResponse httpResponse = (HttpServletResponse) response;
			UIContext context = UIContext.getUIContext(httpRequest, httpResponse);
			boolean tracing = context.isTracing();
			String path = httpRequest.getRequestURI();
			if (tracing) {
				logger.info("Authorization filter called for path: {}", path);
			}
			if (path.contains(Constants.INIT_SERVLETS_MAPPING)) {
				logger.error("Invalid request path {} invoked by loginid {}", path, context.getLoginid());
				httpResponse.setStatus(HttpURLConnection.HTTP_FORBIDDEN);
				return;
			}
			if (context.isValidRequest()) {
				if (tracing) {
					logger.info("Authorization filter succeeded");
				}
				chain.doFilter(request, response);
			} else {
				if (tracing) {
					logger.info("Authorization filter failed, redirecting to login page.");
				}
				httpResponse.setStatus(HttpURLConnection.HTTP_UNAUTHORIZED);
				httpResponse.sendError(HttpURLConnection.HTTP_UNAUTHORIZED, "Invalid CSRF Token");
				/*String loginPage = httpRequest.getContextPath()+"/"+AppInfo.getLoginpage();
				httpResponse.sendRedirect(loginPage);*/
			}
		} else {
			chain.doFilter(request, response);
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
	}

}

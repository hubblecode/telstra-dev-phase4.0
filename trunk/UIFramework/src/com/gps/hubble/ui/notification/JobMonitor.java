package com.gps.hubble.ui.notification;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.AsyncContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

import com.gps.hubble.ui.utilities.DataServiceUtils;
import com.gps.hubble.ui.utilities.UIContext;

public class JobMonitor implements Runnable {

	private AsyncContext asyncContext;
	private HttpServletRequest request;
	private HttpServletResponse response;
	private int sleepTime;
	private static final Logger logger = LogManager.getLogger();
	
	
	public JobMonitor() {
		
	}
	
	public JobMonitor(AsyncContext asyncContext) {
		this(asyncContext,10000);
	}
	
	public JobMonitor(AsyncContext asyncContext,int sleepTime) {
		this.asyncContext = asyncContext;
		this.sleepTime = sleepTime;
		this.request=(HttpServletRequest)asyncContext.getRequest();
		this.response=(HttpServletResponse)asyncContext.getResponse();
	}
	
	@Override
	public void run() {
		String jobType = request.getParameter("JobType");
		String jobKey = request.getParameter("JobKey");
		String jobUser = "";
		UIContext context = UIContext.getUIContext(request, response);
		boolean isComplete = false;
		JSONObject alertOutput = null;		
		while(!isComplete) {
			try {
				//Thread.sleep(sleepTime);
				//reducing the sleep time so that the notification is shown in the UI almost immediately.
				Thread.sleep(600);
			} catch(InterruptedException ie) {
				
			}
			JSONObject alertInput = new JSONObject();
			JSONObject inboxReferenceList = new JSONObject();
			JSONObject inboxReference = new JSONObject();

			inboxReferenceList.put("InboxReferences", inboxReference);
			alertInput.put("InboxReferencesList", inboxReferenceList);
			if("Export".equals(jobType)) {
				/*JSONObject apiInput = new JSONObject();
				apiInput.put("ExportRequestKey",jobKey); */
				inboxReference.put("Name", "Export ID");
				inboxReference.put("Value",jobKey);
				alertInput.put("ExceptionType", "GPS_EXPORT_ALERT");
				logger.debug("Invoking Data Service to get ExportJob Status "+alertInput.toJSONString());				
				if(context.isTracing()) {
					logger.debug("Invoking Data Service to get ExportJob Status "+alertInput.toJSONString());
				}

				/*JSONObject output = DataServiceUtils.invokeDataService("exportDetails", apiInput, context);
				String processedFlag = (String)output.get("ProcessedFlag");
				jobUser = (String)output.get("CreateUserid");
				if(processedFlag != null && "Y".equals(processedFlag)) {
					isComplete = true;
				} */
				
				
			}else if("Upload".equals(jobType)) {
				/*JSONObject apiInput = new JSONObject();
				apiInput.put("FileUploadKey",jobKey); */
				inboxReference.put("Name", "Upload ID");
				inboxReference.put("Value",jobKey);
				alertInput.put("ExceptionType", "GPS_EXCEL_UPLOAD_ALERT");
				
				if(context.isTracing()) {
					logger.debug("Invoking Data Service to get UploadJob Status "+alertInput.toJSONString());
				}
								
				/*JSONObject output = DataServiceUtils.invokeDataService("uploadDetails", apiInput, context);
				jobUser = (String)output.get("CreateUserid");
				String processedFlag = (String)output.get("ProcessedFlag");
				if(processedFlag != null && "Y".equals(processedFlag)) {
					isComplete = true;
				}*/
			}else {
				//Unknown Job Type.. Mark it completed
				isComplete = true;
			}
			alertOutput = DataServiceUtils.invokeDataService("jobAlerts", alertInput, context);
			if(alertOutput != null) {
				int totalNumberOfRecords = Integer.parseInt((String)alertOutput.get("TotalNumberOfRecords"));
				if(totalNumberOfRecords > 0) {
					isComplete = true;
				}
			}			
			
		}
		try {
			if(response != null) {
				response.setStatus(HttpServletResponse.SC_OK);
				response.setContentType("application/json");
				JSONObject output = new JSONObject();
				output.put("JobType", jobType);
				output.put("JobKey", jobKey);
				output.put("User", jobUser);
				output.put("AlertDetails", alertOutput);
				
				PrintWriter out = response.getWriter();
				//out.write(alertOutput.toJSONString());
				out.write(output.toJSONString());
				
			}
			//out.write("The "+jobType+" Job Request Submitted By You has completed");
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}
		asyncContext.complete();
	}

}

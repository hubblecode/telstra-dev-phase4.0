/** 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/
package com.gps.hubble.ui.initializers;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.gps.hubble.ui.AppInfo;

/**
 * 
 * @author Bridge
 * Init Servlet to initialize the application
 */
@WebServlet(urlPatterns = { "/init/AppInitializer" }, loadOnStartup = 5)
public class AppInitializer extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AppInitializer() {
		super();
	}

	@Override
	public void init() throws ServletException {
		logger.info("App initializer started");
		super.init();
		String homepage = getServletContext().getInitParameter("homepage");
		AppInfo.setHomepage(homepage);
		logger.info("App home page is: {}", homepage);
		String loginpage = getServletContext().getInitParameter("loginpage");
		AppInfo.setLoginpage(loginpage);
		logger.info("App login page is: {}", loginpage);
		String remoteserverurl = getServletContext().getInitParameter(
				"remoteserverurl");
		AppInfo.setRemoteserverurl(remoteserverurl);
		logger.info("App remote server url is: {}", remoteserverurl);
		String index = getServletContext().getInitParameter("index");
		AppInfo.setIndex(index);
		logger.info("App indexpage  is: {}", index);
		String identifier = getServletContext().getInitParameter("AppIdentifier");
		AppInfo.setIdentifier(identifier);
		logger.info("App identifier is: {}", identifier);
		String ssomode = getServletContext().getInitParameter("SSO_MODE");
		AppInfo.setSsomode(ssomode);
		logger.info("App identifier is: {}", identifier);
	}
}

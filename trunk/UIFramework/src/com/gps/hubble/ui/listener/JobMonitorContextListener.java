package com.gps.hubble.ui.listener;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class JobMonitorContextListener implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent sc) {
		ThreadPoolExecutor threadPool = (ThreadPoolExecutor)sc.getServletContext().getAttribute("monitorPool");
		threadPool.shutdown();
	}

	@Override
	public void contextInitialized(ServletContextEvent sc) {
		ThreadPoolExecutor monitorPool = new ThreadPoolExecutor(10,200,60000L,TimeUnit.MILLISECONDS, new ArrayBlockingQueue<Runnable>(100));
		sc.getServletContext().setAttribute("monitorPool", monitorPool);

	}

}

package com.gps.hubble.ui.listener;

import java.io.IOException;

import javax.servlet.AsyncEvent;
import javax.servlet.AsyncListener;
import javax.servlet.http.HttpServletResponse;




public class JobMonitorListener implements AsyncListener {

	@Override
	public void onComplete(AsyncEvent paramAsyncEvent) throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void onError(AsyncEvent paramAsyncEvent) throws IOException {
		HttpServletResponse response = (HttpServletResponse)paramAsyncEvent.getAsyncContext().getResponse();
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);

	}

	@Override
	public void onStartAsync(AsyncEvent paramAsyncEvent) throws IOException {

	}

	@Override
	public void onTimeout(AsyncEvent paramAsyncEvent) throws IOException {
		HttpServletResponse response = (HttpServletResponse)paramAsyncEvent.getAsyncContext().getResponse();
		response.setStatus(HttpServletResponse.SC_REQUEST_TIMEOUT);
		
	}

}

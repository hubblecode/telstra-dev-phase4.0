/** 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/
package com.gps.hubble.ui.dataService;

import org.json.simple.JSONObject;

import com.gps.hubble.ui.utilities.UIContext;

/**
 * 
 * @author Bridge
 * The DataService class implements this interface. 
 *
 */
public interface IDataService {
	
	public JSONObject execute(JSONObject input);
	
	public JSONObject mungeInput(JSONObject input);
	
	public DataServiceDef getDataServiceDef();
	
	public void setDataServiceDef(DataServiceDef dataServiceDef);

	public void setUIContext(UIContext context);
	
	public UIContext getUIContext();
}

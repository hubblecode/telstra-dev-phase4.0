/** 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/
package com.gps.hubble.ui.dataService;                

import java.util.Iterator;

import javax.xml.parsers.ParserConfigurationException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.gps.hubble.ui.utilities.UIContext;
/**
 * 
 * @author Bridge
 * This class is a Hook class, which augments the output of the ShipmentDetails DataService by adding attributes for Document Type Description and Vendor Name
 */

public class ShipmentDetailsDataService implements IDataServiceHook {

	@Override
	public JSONObject transformInput(UIContext context, JSONObject input) {
		return input;
	}

	@Override
	public JSONObject transformOutput(UIContext context,JSONObject output)  {
		JSONObject massagedOutput = output;
		try {
			String documentType=(String)massagedOutput.get("DocumentType");
			JSONObject inputObj = new JSONObject();
			//inputObj.put("DocumentType",documentType);
			DataServiceDef dataServiceDef= DataServiceManager.getDataServiceDef("getdocumenttypelist");
			JSONParser parser = new JSONParser();
			JSONObject output1 = new JSONObject();
			String strOutput = APIInvoker.invoke(context, dataServiceDef, inputObj.toJSONString(), dataServiceDef.getAPIName(), dataServiceDef.isExtended(), true);
			if(strOutput != null) {
				output1 = (JSONObject) parser.parse(strOutput)  ;
			}
			 
			/*if(output1 !=null){
			JSONArray documentParams=(JSONArray)output1.get("DocumentParams");
			if(documentParams!=null){
			       for(Iterator<JSONObject>itr=documentParams.iterator();itr.hasNext();) {
			       String docDesc=(String)itr.next().get("Description");
			        massagedOutput.put("DocumentDescription",docDesc);
			    }
			   }
			} */
			GetOrganizationList getOrgList = new GetOrganizationList();
			String organizationCode=(String)massagedOutput.get("SellerOrganizationCode");
			String organizationName=getOrgList.getOrganization(context,organizationCode);
			massagedOutput.put("VendorName",organizationName);
			boolean bfound = false;
			if(output1 !=null){
			JSONArray commonCodeList=(JSONArray)output1.get("CommonCode");
			if(commonCodeList!=null){
			       for(Iterator<JSONObject>itr=commonCodeList.iterator();itr.hasNext();) {
			    	   JSONObject commonCode = itr.next();
			    	   String codeValue = (String)commonCode.get("CodeValue");
			    	   String codeDesc = (String)commonCode.get("CodeShortDescription");
			       //String docDesc=(String)itr.next().get("Description");
			    	   if(documentType.equals(codeValue)) {
			    		    bfound = true;
					        massagedOutput.put("DocumentDescription",codeDesc);
			    	   }
			       }
			       if(!bfound) {
			    	   massagedOutput.put("DocumentDescription",documentType);
			       }
			   }
			}			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return massagedOutput;
		
	}

}

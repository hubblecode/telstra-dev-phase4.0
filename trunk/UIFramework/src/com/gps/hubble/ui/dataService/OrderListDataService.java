/** 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/
package com.gps.hubble.ui.dataService;

import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.gps.hubble.ui.dataService.DataService;
import com.gps.hubble.ui.utilities.UIContext;

/**
 * 
 * @author Bridge
 * This class is a Hook class, which augments the output of the OrderList DataService by adding attributes to indicate whether the Order is watched by the user
 */
public class OrderListDataService implements IDataServiceHook {

	@Override
	public JSONObject transformInput(UIContext context, JSONObject input) {
		return input;
	}

	@Override
	public JSONObject transformOutput(UIContext context,JSONObject output) {
		JSONObject massagedOutput = output;
		String loginId= context.getLoginid();
		
		JSONArray orderList= (JSONArray)massagedOutput.get("Order");
	    //       System.out.print(orderList);
		if(orderList!=null && orderList.size()>0) {
			JSONObject orderObj=null;
			for(Iterator<JSONObject>itr=orderList.iterator();itr.hasNext();){
				 orderObj=itr.next();
		     
				JSONObject extnObj= (JSONObject)orderObj.get("Extn");
				
				if(extnObj !=null) {
			
					JSONObject watchedOrders=(JSONObject)extnObj.get("WatchedOrderList");
			
					if(watchedOrders!=null) {
						JSONArray watchedOrderList=(JSONArray)watchedOrders.get("WatchedOrder");
						JSONObject watchedOrder=null;
						boolean found=false;
						if(watchedOrderList!=null && watchedOrderList.size()>0) {
							for(Iterator<JSONObject>itr1=watchedOrderList.iterator();itr1.hasNext();) {
								watchedOrder=itr1.next();
								String orderlogin=(String)watchedOrder.get("Loginid");
								if(orderlogin!=null && loginId.equals(orderlogin)) {
									orderObj.put("IsWatchedByUser","Y");
			
									found=true;
									break;
								}
								
							}
						}
						if(!found) {
							orderObj.put("IsWatchedByUser","N");
			
						}
					}
				}
				
				
			
			}	
		}
		return massagedOutput;
		
}

}

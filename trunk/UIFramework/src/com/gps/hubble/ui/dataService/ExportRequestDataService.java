package com.gps.hubble.ui.dataService;

import org.apache.commons.codec.binary.Base64;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.gps.hubble.ui.utilities.DataServiceUtils;
import com.gps.hubble.ui.utilities.UIContext;

public class ExportRequestDataService implements IDataServiceHook {

	@Override
	public JSONObject transformInput(UIContext context, JSONObject input) {
		
		JSONObject massagedInput = input;
		String encodedCriteria = (String) massagedInput.get("ExportCriteria");
		String exportCriteria = new String(Base64.decodeBase64(encodedCriteria)); 
		JSONParser parser = new JSONParser();
		
		JSONObject exportCritObj = new JSONObject(); 
		try {
			exportCritObj = (JSONObject)parser.parse(exportCriteria);
			
		}catch(ParseException pe) {
			pe.printStackTrace();
		}
		String dataServiceId = (String)exportCritObj.get("InputDataService");
		if(dataServiceId != null) {
			IDataService dataService = DataServiceUtils.getDataService(dataServiceId, context);
			JSONObject searchCriteria = (JSONObject)exportCritObj.get("SearchCriteria");
			JSONObject dataServiceInput = dataService.mungeInput(searchCriteria);
			exportCritObj.put("SearchCriteria",dataServiceInput);
			String newJSONInputStr = exportCritObj.toJSONString();
			String newEnocdedCriteria = new String(Base64.encodeBase64(newJSONInputStr.getBytes()));
			massagedInput.put("ExportCriteria", newEnocdedCriteria);
		}
		return massagedInput;
	}

	@Override
	public JSONObject transformOutput(UIContext context, JSONObject output) {
		return output;
	}

}

/** 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/
package com.gps.hubble.ui.dataService;

import java.util.ArrayList;
import java.util.Iterator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.gps.hubble.ui.utilities.DataServiceUtils;
import com.gps.hubble.ui.utilities.UIContext;

public class CompositeDataService implements IDataService {

	private static final String NAME = "name";
	private DataServiceDef dataServiceDef = null;
	private UIContext uiContext = null;
	private static final Logger logger = LogManager.getLogger();



	@SuppressWarnings("unchecked")
	@Override
	public final JSONObject execute(JSONObject input) {
		if (uiContext.isTracing()) {
			logger.info("Aggregator: Invoking aggregate data service: {}", dataServiceDef.getName());
		}
		ArrayList<JSONObject> dataServiceRefs = dataServiceDef.getDataServiceRefs();
		JSONObject output = new JSONObject();
		for (Iterator<JSONObject> iterator = dataServiceRefs.iterator(); iterator.hasNext();) {
			JSONObject ref = iterator.next();
			String refDataServiceId = (String) ref.get(NAME);
			if (uiContext.isTracing()) {
				logger.info("Aggregator: Invoking data Service ref: {}", refDataServiceId);
			}
			 Object obj = input.get(refDataServiceId);
			 JSONObject refInput = null;
			 if(obj instanceof JSONArray) {
				 JSONArray arr = (JSONArray)obj;
				 if(arr.size() > 0) {
					 refInput = (JSONObject)arr.get(0);
				 }
			 } else {
				 refInput = (JSONObject)obj;
			 }
			 
			 
			if(refInput == null) {
				refInput= new JSONObject();
			}
			JSONObject refOutput = DataServiceUtils.invokeDataService(refDataServiceId, refInput, uiContext);
			if (refOutput != null) {
				output.put(refDataServiceId, refOutput);
			}
		}
		
		return output;
	}



	@Override
	public JSONObject mungeInput(JSONObject input) {
		// Composite Data Service input will be returned as is
		return input;
	}

	@Override
	public DataServiceDef getDataServiceDef() {
		return dataServiceDef;
	}

	@Override
	public void setDataServiceDef(DataServiceDef dataServiceDef) {
		this.dataServiceDef = dataServiceDef;
	}

	@Override
	public void setUIContext(UIContext context) {
		this.uiContext = context;
	}

	@Override
	public UIContext getUIContext() {
		return uiContext;
	}

	
}

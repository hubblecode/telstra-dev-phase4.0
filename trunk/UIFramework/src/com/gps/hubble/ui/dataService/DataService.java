/** 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/
package com.gps.hubble.ui.dataService;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.gps.hubble.ui.Constants;
import com.gps.hubble.ui.utilities.UIContext;
import com.gps.hubble.ui.utilities.Utils;
import com.sun.corba.se.impl.javax.rmi.CORBA.Util;

public class DataService implements IDataService {

	private DataServiceDef dataServiceDef = null;
	private UIContext uiContext = null;
	private static final Logger logger = LogManager.getLogger();
	

	@Override
	public final JSONObject execute(JSONObject jsonInput) {
		JSONObject massagedInput = mungeInput(jsonInput);
		JSONObject output = executeInternal(massagedInput);
		if (output != null) {
			JSONObject massagedOutput = output;
			if(dataServiceDef.getHookClass() != null) {
				massagedOutput = dataServiceDef.getDataServiceHook().transformOutput(uiContext, output);
			}
			if (uiContext.isTracing()) {
				String outputStr = massagedOutput.toJSONString();
				logger.info("API Output after munging data service: {} output: {}", dataServiceDef.getName(), outputStr);
			}
			return massagedOutput;
		}
		return null;
	}
	
	@Override
	public JSONObject mungeInput(JSONObject jsonInput) {
		if (jsonInput == null) {
			jsonInput = new JSONObject();
		}
		//need to test
		if (!dataServiceDef.isComposite()) {
			updateWithInputDataDef(jsonInput);
			updateWithHeaderDataDef(jsonInput);
			updateWithConstDataDef(jsonInput);
		}
		String input = jsonInput.toJSONString();
		if (uiContext.isTracing()) {
			logger.info("API Input after munging data service: {} input: {}", dataServiceDef.getName(), input);
		}
		JSONObject massagedInput = jsonInput;
		if(dataServiceDef.getHookClass() != null) {
			massagedInput = dataServiceDef.getDataServiceHook().transformInput(uiContext, jsonInput);
		}
		//JSONObject massagedInput = massageInput(jsonInput);
		if (!dataServiceDef.isRetainEmptyAttributes()) {
			logger.info("Removing Empty Attributes");
			removeEmptyAttributesAndElements(massagedInput);
		}
		removeFrameworkElements(massagedInput);
		if (uiContext.isTracing()) {
			input = massagedInput.toJSONString();
			logger.info("API Input after removing empty attributes: {} input: {}", dataServiceDef.getName(), input);
		}
		return massagedInput;
	}
	
	private boolean removeEmptyAttributesAndElements(JSONObject input) {
		boolean childrenFound = false;
		/*Object[] keys = input.keySet().toArray();
		for (int i = 0; i < keys.length; i++) {
			String key = (String) keys[i];
			Object value = input.get(key);
			if(value instanceof JSONObject) {
				childrenFound = childrenFound || removeEmptyAttributesAndElements((JSONObject)value);
				if (!childrenFound) {
					input.remove(key);
				}
			} else if (value instanceof JSONArray) {
				childrenFound = childrenFound || removeEmptyAttributesAndElements((JSONArray)value);
				if (!childrenFound) {
					input.remove(key);
				}
			} else if (value instanceof String) {
				if("".equalsIgnoreCase((String)value)) {
					input.remove(key);
				} else {
					childrenFound = true;
				}
			}
		}*/
		for(Iterator itr=input.keySet().iterator();itr.hasNext();) {
			String key = (String) itr.next();
			Object value = input.get(key);
			if(value instanceof JSONObject) {
				boolean isChildAvail = removeEmptyAttributesAndElements((JSONObject)value);
				childrenFound = childrenFound || isChildAvail ;
				if (!isChildAvail) {
					//input.remove(key);
					itr.remove();
				}
			} else if (value instanceof JSONArray) {
				boolean isChildAvail = removeEmptyAttributesAndElements((JSONArray)value);
				childrenFound = childrenFound || isChildAvail; 
				if (!isChildAvail) {
					//input.remove(key);
					itr.remove();
				}
			} else if (value instanceof String) {
				if("".equalsIgnoreCase((String)value)) {
					//input.remove(key);
					itr.remove();
				} else {
					childrenFound = true;
				}
			}
			
		}
		
		
		return childrenFound;
	}
	
	private void removeFrameworkElements(JSONObject input) {
		Object[] keys = input.keySet().toArray();
		for (int i = 0; i < keys.length; i++) {
			String key = (String) keys[i];
			Object value = input.get(key);
			if(value instanceof JSONObject) {
				if(Constants.FRAMEWORK_ELEM_NAME.equalsIgnoreCase(key)) {
					input.remove(key);
				} else {
					removeFrameworkElements((JSONObject) value);
				}
			} else if (value instanceof JSONArray) {
				JSONArray array = (JSONArray) value;
				for (Iterator iterator = array.iterator(); iterator.hasNext();) {
					Object object = (Object) iterator.next();
					if(object instanceof JSONObject) {
						removeFrameworkElements((JSONObject) object);
					}
				}
			}
		}
	}
	private boolean removeEmptyAttributesAndElements(JSONArray value) {
		for (Iterator iterator = value.iterator(); iterator.hasNext();) {
			Object object = (Object) iterator.next();
			if(object instanceof JSONObject) {
				removeEmptyAttributesAndElements((JSONObject) object);
			}
		}
		return true;
	}

	public void updateWithConstDataDef(JSONObject jsonInput) {
		HashMap<String, String> constdataDef = dataServiceDef.getConstDataDef();
		if (constdataDef == null) {
			return;
		}
		for (String path : constdataDef.keySet()) {
			String data = constdataDef.get(path);
			updateJsonObject(jsonInput, path, data);
		}
	}
	
	private void updateWithHeaderDataDef(JSONObject jsonInput) {
		HashMap<String, String> dataDef = dataServiceDef.getHeaderDataDef();
		if (dataDef == null) {
			return;
		}
		for (String path : dataDef.keySet()) {
			String dataKey = dataDef.get(path);
			String data = getUserDataUsingHeaderDef(dataKey);
			updateJsonObject(jsonInput, path, data);
		}
	}

	private void updateWithInputDataDef(JSONObject jsonInput) {
		HashMap<String, String> dataDef = dataServiceDef.getInputDataDef();
		if (dataDef == null) {
			return;
		}
		for (String path : dataDef.keySet()) {
			String dataKey = dataDef.get(path);
			String data = getUserDataUsingDataDef(dataKey);
			updateJsonObject(jsonInput, path, data);
		}
	}

	@SuppressWarnings("unchecked")
	private void updateJsonObject(JSONObject jsonInput, String path, String data) {
		if (uiContext.isTracing()) {
			logger.info("Updating input json using path: {} data: {}", path, data);
		}
		
		String[] paths = path.split("\\.");
		if (paths == null || paths.length == 1) {
			if (!jsonInput.containsKey(path)) {
				jsonInput.put(path, data);
			}
			return;
		}
		JSONObject currentObject = jsonInput;
		int i = 0;
		for (i = 0; i < paths.length - 1; i++) {
			//Array Handling
			if(paths[i].indexOf('[') != -1) {
				String arrPath = paths[i].substring(0, paths[i].indexOf('['));
				if (uiContext.isTracing()) {
					logger.info("The path "+paths[i]+" Is an Array "+arrPath);
					logger.info("The value is "+currentObject.get(paths[i]));
				}
				JSONArray pathArray = (JSONArray) currentObject.get(arrPath);
				if (pathArray == null) {
					pathArray = new JSONArray();
					currentObject.put(arrPath, pathArray);
					//currentObject.put(paths[i], pathObject);
				}
				JSONObject arrObj = new JSONObject();
				pathArray.add(arrObj);
				currentObject = arrObj;
			} else {
				JSONObject pathObject = (JSONObject) currentObject.get(paths[i]);
				if (pathObject == null) {
					pathObject = new JSONObject();
				}
				currentObject.put(paths[i], pathObject);
				currentObject = pathObject;
				
			}
		}
		if (!currentObject.containsKey(paths[paths.length - 1])) {
			currentObject.put(paths[paths.length - 1], data);
		}
	}

	private String getUserDataUsingDataDef(String dataDefKey) {
		return uiContext.getRequest().getParameter(dataDefKey);
	}

	private String getUserDataUsingHeaderDef(String dataDefKey) {
		String data = uiContext.getRequest().getHeader(dataDefKey);
		if (data != null) {
			return data;
		}
		Cookie[] cookies = uiContext.getRequest().getCookies();
		
		if(cookies == null)
			return "";
		
		for (int i = 0; i < cookies.length; i++) {
			Cookie cookie = cookies[i];
			String cookieName = Utils.getValidCookieName(cookie.getName(), uiContext.getPortalIdentifier());
			if(dataDefKey.equals(cookieName)) {
				data = cookie.getValue();
			}
		}
		return data;
	}

	private JSONObject executeInternal(JSONObject input) {
		if (dataServiceDef.isPrototype()) {
			logger.error("Invoking data service {} in prototype mode.", dataServiceDef.getName());
			String fileName = "/prototype/"+dataServiceDef.getName()+".json";
			InputStream fileStream = DataService.class.getResourceAsStream(fileName);
			JSONParser parser = new JSONParser();
			try {
				Object output = parser.parse(new InputStreamReader(fileStream, "UTF-8"));
				JSONObject jsonOutput = (JSONObject) output;
				return jsonOutput;
			} catch (Exception e) {
				logger.catching(e);
			}
			return null;
		}
		String output =  APIInvoker.invoke(uiContext, dataServiceDef, input.toJSONString(), dataServiceDef.getAPIName(), dataServiceDef.isExtended(), true);
		
		if (output == null) {
			return null;
		}
		if("".equals(output)) {
			output = "{}";
		}
		try {
			JSONParser parser = new JSONParser();
			JSONObject jsonOutput = (JSONObject) parser.parse(output);
			return jsonOutput;
		} catch (ParseException e) {
			if(uiContext.isTracing()) {
				logger.info("Parsing Failed.. Output Size is "+output.length());
			}
			
			if(output.length() > 0 && output.charAt(0) == '<')
			{
				logger.debug(" The first character is "+output.charAt(0));
				uiContext.setRemoteServerErrorStatusCode(HttpURLConnection.HTTP_UNAUTHORIZED);
			}
			e.printStackTrace();
		}catch(Exception e){
			if(output.length() > 0 && output.charAt(0) == '<')
			{
				uiContext.setRemoteServerErrorStatusCode(HttpURLConnection.HTTP_UNAUTHORIZED);
			}
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public DataServiceDef getDataServiceDef() {
		return dataServiceDef;
	}

	@Override
	public void setDataServiceDef(DataServiceDef dataServiceDef) {
		this.dataServiceDef = dataServiceDef;
	}

	@Override
	public void setUIContext(UIContext context) {
		this.uiContext = context;
	}

	@Override
	public UIContext getUIContext() {
		return uiContext;
	}

}

/** 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/
package com.gps.hubble.ui.dataService;

import org.json.simple.JSONObject;

import com.gps.hubble.ui.utilities.UIContext;

/**
 * @author Vinay
 * DataServiceHook classes allows the application to add additional logic in the Backend layer. 
 * All The Hook classes need to implement this interface.
 */
public interface IDataServiceHook {

	/**
	 * 
	 * @param context
	 * @param input
	 * @return JSONObject
	 * This hook method can be used to transform the input before invoking the REST api.
	 * Typically, Defaulting Logic can be added in this method
	 */
	public JSONObject transformInput(UIContext context,JSONObject input);
	
	/**
	 * 
	 * @param context
	 * @param output
	 * @return JSONObject
	 * This hook method can be used to transform the output returned by the REST api before returning the response to the client.
	 * Typically, Output defaulting logic can be added in this method
	 */
	public JSONObject transformOutput(UIContext context,JSONObject output);
}

/** 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/
package com.gps.hubble.ui.dataService;                

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.gps.hubble.ui.utilities.DataServiceUtils;
import com.gps.hubble.ui.utilities.UIContext;

/**
 * 
 * @author Bridge
 * This class is a Hook class, which augments the output of the OrderDetails DataService by adding attributes to indicate whether the Order is watched by the user
 */

public class OrderDetailsDataService implements IDataServiceHook {

	@Override
	public JSONObject transformInput(UIContext context, JSONObject input) {
		return input;
	}

	@Override
	public JSONObject transformOutput(UIContext context,JSONObject output)  {
		JSONObject massagedOutput = output;
		String loginId= context.getLoginid();
		JSONObject extnObj= (JSONObject)massagedOutput.get("Extn");
			
		try {
			/* We are now using getCommonCodeList to get the Document Type list */
			String documentType=(String)massagedOutput.get("DocumentType");
			JSONObject inputObj = new JSONObject();
			DataServiceDef dataServiceDef= DataServiceManager.getDataServiceDef("getdocumenttypelist");
			JSONParser parser = new JSONParser();	
			JSONObject output1 = DataServiceUtils.invokeDataService("getdocumenttypelist", inputObj, context);
			/*String strOutput = APIInvoker.invoke(context, dataServiceDef, inputObj.toJSONString(), dataServiceDef.getAPIName(), dataServiceDef.isExtended(), true);
			if(strOutput != null) {
				System.out.println("Log Output" + strOutput);
			}
			JSONObject output1 = (JSONObject) parser.parse(strOutput)  ;*/
			boolean bfound = false;
			if(output1 !=null){
			JSONArray commonCodeList=(JSONArray)output1.get("CommonCode");
			if(commonCodeList!=null){
			       for(Iterator<JSONObject>itr=commonCodeList.iterator();itr.hasNext();) {
			    	   JSONObject commonCode = itr.next();
			    	   String codeValue = (String)commonCode.get("CodeValue");
			    	   String codeDesc = (String)commonCode.get("CodeShortDescription");
			    	   if(documentType.equals(codeValue)) {
			    		    bfound = true;
					        massagedOutput.put("DocumentDescription",codeDesc);
			    	   }
			       }
			       if(!bfound) {
			    	   massagedOutput.put("DocumentDescription",documentType);
			       }
			   }
			}
			
			GetOrganizationList getOrgList = new GetOrganizationList();
			String organizationCode=(String)massagedOutput.get("SellerOrganizationCode");
			String organizationName=getOrgList.getOrganization(context,organizationCode);
			massagedOutput.put("VendorName",organizationName);
		if(extnObj !=null) {
			JSONObject watchedOrders=(JSONObject)extnObj.get("WatchedOrderList");
			if(watchedOrders!=null) {
				JSONArray watchedOrderList=(JSONArray)watchedOrders.get("WatchedOrder");
				JSONObject watchedOrder=null;
				boolean found=false;
				if(watchedOrderList!=null && watchedOrderList.size()>0) {
					for(Iterator<JSONObject>itr=watchedOrderList.iterator();itr.hasNext();) {
						watchedOrder=itr.next();
						String orderlogin=(String)watchedOrder.get("Loginid");
						if(orderlogin!=null && loginId.equals(orderlogin)) {
							massagedOutput.put("IsWatchedByUser","Y");
							found=true;
							break;
						}
						
					}
				}
				if(!found) {
					massagedOutput.put("IsWatchedByUser","N");
				}
			}
		}
		Map <String,JSONArray> refGrpMap = new HashMap<>();
		JSONObject refGroups= new JSONObject(); 
		JSONArray refGrpArray = new JSONArray();
		JSONObject referencesObj = (JSONObject)massagedOutput.get("References");
		if(referencesObj != null) {
			JSONArray referenceArr = (JSONArray)referencesObj.get("Reference");
			
			if(referenceArr != null) {
				if(massagedOutput.get("ReferenceGroups") == null) {
					massagedOutput.put("ReferenceGroups", refGroups);
					refGroups.put("ReferenceGroup", refGrpArray);
				}
				
				for(int i=0;i < referenceArr.size();i++) {
					JSONObject referenceObj = (JSONObject)referenceArr.get(i);
					if(referenceObj != null) {
						String refName = (String)referenceObj.get("Name");
						String refVal = (String)referenceObj.get("Value");
						if(refName.indexOf(':') != -1) {
							String [] refNameArr = refName.split(":");
							JSONObject refMapObj = new JSONObject();
							refMapObj.put("Name", refNameArr[1]);
							refMapObj.put("Value",refVal);
							JSONArray refMapArr = null;
							if(refGrpMap.containsKey(refNameArr[0])) {
								refMapArr =  refGrpMap.get(refNameArr[0]);
							} else {
								refMapArr = new JSONArray();
							}
							refMapArr.add(refMapObj);
							refGrpMap.put(refNameArr[0], refMapArr);
						}
								
					}
				}
				for(Iterator<Map.Entry<String, JSONArray>> itr=refGrpMap.entrySet().iterator();itr.hasNext();) {
					Map.Entry<String, JSONArray> entry = itr.next();
					String grpName=entry.getKey();
					JSONArray refArray=entry.getValue();
					JSONObject grpObj = new JSONObject();
					grpObj.put("GroupName",grpName);
					grpObj.put("Reference", refArray);
					refGrpArray.add(grpObj);
				}
			}
			
		}
		JSONObject linelistObject = (JSONObject)massagedOutput.get("OrderLines");
		JSONArray lineArray = (JSONArray) linelistObject.get("OrderLine");
		if(lineArray != null && !lineArray.isEmpty()) {
			for(int index=0;index < lineArray.size(); index++) {
				JSONObject orderlineObj = (JSONObject)lineArray.get(index);
				JSONObject lineExtnObj = (JSONObject)orderlineObj.get("Extn");
				JSONObject allcommitmentList = (JSONObject)lineExtnObj.get("OrderLineCommitmentList");
				JSONObject pastCommitment = null;
				JSONArray pastCommitmentArr = null;
				JSONArray pastCommitmentGrp = null;
				
				TreeMap<String,JSONArray> sortedGrpMap = new TreeMap<>(Collections.reverseOrder());
				
				if(allcommitmentList != null) {
					JSONArray allcommitmentsArray = (JSONArray)allcommitmentList.get("OrderLineCommitment");
					if(allcommitmentsArray != null && !allcommitmentsArray.isEmpty()) {
						for(Iterator itr=allcommitmentsArray.iterator();itr.hasNext();) {
							JSONObject commitmentObj = (JSONObject)itr.next();
							
							String isActive = (String)commitmentObj.get("IsActive");
							if(!"Y".equals(isActive)) {
								itr.remove();
								String key=(String)commitmentObj.get("CommitRequestDate");
								String dateKey=key.substring(0,10);
							    if(!(sortedGrpMap.containsKey(dateKey)))	{
							    	  pastCommitmentArr =new JSONArray();
							    	  pastCommitmentArr.add(commitmentObj);
							    	  sortedGrpMap.put(dateKey, pastCommitmentArr);
							      
							    }else  {
							    	     JSONArray commitArr= sortedGrpMap.get(dateKey);
							    	    commitArr.add(commitmentObj);
								      }
								   
							}
							
						}
						
						 Set<String> keys = sortedGrpMap.keySet();
						 JSONArray	pastCommitmentDateGroup = new JSONArray();
						 lineExtnObj.put("PastOrderLineCommitmentGroup",pastCommitmentDateGroup);
					   for(String key: keys){
						   JSONObject  commitReqDate = new JSONObject();
						   commitReqDate.put("CommitRequestDate", key);
				            JSONArray pastCommitmentDateArr = (JSONArray)sortedGrpMap.get(key);
						   commitReqDate.put("PastCommitmentList", pastCommitmentDateArr);
				            pastCommitmentDateGroup.add(commitReqDate);
				                }
						
				}
				}				
			}
				
		}
			

			
		
	
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return massagedOutput;
		
	}

}

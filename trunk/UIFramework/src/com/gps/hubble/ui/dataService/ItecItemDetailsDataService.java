package com.gps.hubble.ui.dataService;

import java.text.DecimalFormat;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import com.gps.hubble.ui.utilities.UIContext;

public class ItecItemDetailsDataService implements IDataServiceHook {

	@Override
	public JSONObject transformInput(UIContext context, JSONObject input) {
		// TODO Auto-generated method stub
		return input;
	}

	@Override
	public JSONObject transformOutput(UIContext context, JSONObject output) {
		JSONObject massagedOutput = output;

		JSONObject items= (JSONObject)massagedOutput.get("Item");
		HashMap<String, String> altItemMap= new HashMap<String, String>();
		JSONArray altItems;
		DecimalFormat df = new DecimalFormat("0.00");

		if(items!=null && items.size()>0) {
			if(items != null){
				JSONObject alternateItemList= (JSONObject) items.get("AlternateItemList");
				if(alternateItemList != null){
					altItems=(JSONArray) alternateItemList.get("AlternateItem");
					JSONObject altitem= null;

					if(altItems!= null && altItems.size()> 0){

						for (Iterator<JSONObject>itr1=altItems.iterator();itr1.hasNext();) {
							altitem=itr1.next();
							String location="";
							String status= "";
							String uniqueid=(String) altitem.get("MaterialId")+":"+(String)altitem.get("ExtendedDescription")+":";
							if(altitem.get("Location") != null){
								location=(String) altitem.get("Location");
								uniqueid=uniqueid+location+":";
							}
							else{
								uniqueid=uniqueid.concat(" :");
							}							

							if(altitem.get("Status") != null){
								status=(String) altitem.get("Status");
								uniqueid=uniqueid+status;
							}
							uniqueid=uniqueid.concat(" ");

							double currentQty=Double.parseDouble((String)altitem.get("Quantity"));
							if(altItemMap.containsKey(uniqueid)){
								String temp=altItemMap.get(uniqueid);
								double updateQty=Double.parseDouble(temp)+ currentQty;
								altItemMap.put(uniqueid, df.format(updateQty));
							}
							else{
								altItemMap.put(uniqueid, df.format(currentQty));
							}
						}

						Collection<String> keys = altItemMap.keySet();
						altItems.removeAll(altItems);
						for ( String key: keys){
							System.out.println("key"+ key);
							String itemDtlArray[]=key.split(":");

							JSONObject object= new JSONObject();
							object.put("MaterialId", itemDtlArray[0]); 
							object.put("ExtendedDescription", itemDtlArray[1]); //ExtendedDescription
							object.put("Location", itemDtlArray[2]);
							object.put("Status", itemDtlArray[3]);
							object.put("Quantity", altItemMap.get(key) );

							altItems.add(object);
						}
					}
				}
			}
		}
		//		System.out.println(massagedOutput.toString());
		return massagedOutput;

	}
}
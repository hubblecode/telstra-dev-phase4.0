/** 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/
package com.gps.hubble.ui.dataService;                

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.gps.hubble.ui.utilities.UIContext;
/**
 * 
 * @author Bridge
 * This class is a Hook class, which augments the output of the ShipmentDetails DataService by adding attributes for Document Type Description and Vendor Name
 */

public class SerialListDataService implements IDataServiceHook {

	@Override
	public JSONObject transformInput(UIContext context, JSONObject input) {
		return input;
	}

	@Override
	public JSONObject transformOutput(UIContext context,JSONObject output)  {
		JSONObject massagedOutput = output;
		Map<String,String> nodeMap = new HashMap<>();
		String nodeName="";
		try {
			JSONArray serialListArr = (JSONArray)massagedOutput.get("SerialNumber");
			
			if(serialListArr != null) {
				for(int i=0;i<serialListArr.size();i++) {
					JSONObject serialNumObj = (JSONObject)serialListArr.get(i);
					String nodeCode= (String)serialNumObj.get("ShipNode");
					if(nodeMap.containsKey(nodeCode)) {
						nodeName = nodeCode+"-"+nodeMap.get(nodeCode);
					} else {
						nodeName = GetOrganizationList.getOrganization(context,nodeCode);
						if(nodeName == null || "".equals(nodeName)) {
							nodeName = nodeCode;
						}
						nodeMap.put(nodeCode, nodeName);
					}
					serialNumObj.put("ShipNodeDescription", nodeName);
				}
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return massagedOutput;
		
	}

}

/** 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/
package com.gps.hubble.ui.servlets.filehandler;

import java.io.IOException;

import java.io.InputStream;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.xml.sax.SAXException;

import com.gps.hubble.ui.dataService.APIInvoker;
import com.gps.hubble.ui.dataService.DataServiceDef;
import com.gps.hubble.ui.dataService.DataServiceManager;
import com.gps.hubble.ui.utilities.UIContext;

/**
 * Servlet class implementation for ExcelUploadServlet
 * @author Sambeet Mohanty
 * @version 1.0 ( 21 Jul 2016)
 */
@WebServlet("/api/exceluploadservlet")
public class ExcelUploadServlet extends HttpServlet  {


	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger();
	private static final String jsonContentType = "application/json";
	private static final String strUploadProcess="process";
	private static final String strReceiverMailId="emailnotify";
	private static final String strProcessType="uploadType";
	private static final String strUploadDate="spsFormattedFromDate";
	private static final String dataServiceId = "fileUploadUtil";  
	private static final String trigAgentDataServiceId="triggerAgentForUpload";
	private static final String compositeDataServiceId = "uploadAggregator";
	private static final String strReceiverCheckEmail="emailCheck";


	/**
	 * Upon receiving file upload submission, parses the request to read upload
	 * data and saves the file in DB as in base64 String format.
	 * Method name: doPost
	 * @param request
	 * @return void
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String strProcess = null; 
		String strMailId = null;
		String strProcessTyp = null;
		String encodedString = null;
		String strFromDate= null;
		String strCheckEmail=null;
		

		UIContext context = UIContext.getUIContext(request, response);	
		boolean tracing = context.isTracing();

		if (tracing) {
			logger.info("upload servlet triggered");
		}

		try {
			if (!ServletFileUpload.isMultipartContent(request)) {
				return;
			}
			ServletFileUpload upload = new ServletFileUpload();
			FileItemIterator iter = upload.getItemIterator(request);

			while (iter.hasNext()) {

				FileItemStream item = iter.next();
				String strFieldName = item.getFieldName();
				if (item.isFormField()) 
				{
					if(strFieldName.equalsIgnoreCase(strUploadProcess))
					{

						InputStream stream = item.openStream();
						strProcess = Streams.asString(stream);

						if (tracing) {
							logger.info("upload process type is: "+ strProcess);
						}
						stream.close();

					}
					else if(strFieldName.equalsIgnoreCase(strReceiverMailId))
					{
						InputStream stream = item.openStream();
						strMailId=Streams.asString(stream);

						if (tracing) {
							logger.info("mail id value is: "+ strMailId);
						}						
						stream.close();
					}
					else if(strFieldName.equalsIgnoreCase(strProcessType))
					{
						InputStream stream = item.openStream();
						strProcessTyp=Streams.asString(item.openStream());
						if (tracing) {
							logger.info("Other upload process value is: "+ strProcessTyp);
						}						
						stream.close();
					}
					else if(strFieldName.equalsIgnoreCase(strUploadDate)){
						InputStream stream = item.openStream();
						strFromDate= Streams.asString(stream);
						
						if (tracing) {
							logger.info("From date for upload process value is: "+ strFromDate);
						}	
						stream.close();
					}
					else if(strFieldName.equalsIgnoreCase(strReceiverCheckEmail)){
						InputStream stream = item.openStream();
						strCheckEmail= Streams.asString(stream);
						
						if (tracing) {
							logger.info("strCheckEmail for upload process value is: "+ strCheckEmail);
						}	
						stream.close();
					}

				} else if (!item.isFormField() && !("".equals(item.getName()))) {

					InputStream is = item.openStream();
					byte[] encodedBytes = Base64.encodeBase64(IOUtils
							.toByteArray(is));
					encodedString = new String(encodedBytes);
					is.close();
				}
				
			}
			
			if (strReceiverCheckEmail.equalsIgnoreCase("true")){
				strMailId="";
			}
			
			if(strProcessTyp!=null)
			{
				updateExcelInDB(encodedString, strProcessTyp,strMailId,strFromDate,context, response);
			}			
			else
			{
				updateExcelInDB(encodedString, strProcess,strMailId,strFromDate,context, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("File Upload failed for "+strProcess+ "with error "+e.getMessage() );
		} 

	}

	private void updateExcelInDB(String encodedString, String strProcess, String strMailId, String strFromDate,UIContext context, HttpServletResponse response )
					throws ParserConfigurationException, SAXException, IOException, TransformerException, ParseException {

		JSONObject inputObj = new JSONObject();
		boolean tracing = context.isTracing();
		String strProgressedId="-1";
		String strStatus="Pending Processing";
		if(strMailId != null && !("".equals(strMailId))) {
			inputObj.put("NotifyEmail", strMailId);			
		}
		inputObj.put("FileObject", encodedString);
		inputObj.put("TransactionType",strProcess);
		inputObj.put("FileProgressed",strProgressedId);
		inputObj.put("Status",strStatus);
		if(context.getUserName() != null) {
			inputObj.put("UserName", context.getUserName());
		}
		
		if(strFromDate != null){
			inputObj.put("FromDate",strFromDate);
		}

		if (tracing) {
			logger.info("DataService input: {}", inputObj);
		} 

		DataServiceDef dataServiceDef= DataServiceManager.getDataServiceDef(dataServiceId);
			
		JSONParser parser = new JSONParser();		
		
		JSONObject output = (JSONObject) parser.parse( APIInvoker.invoke(context, dataServiceDef, inputObj.toJSONString(), dataServiceDef.getAPIName(), dataServiceDef.isExtended(), true))  ;
		
		if (context.isRemoteServerUnreachable()) {
			if (tracing) {
				logger.info("Remote server unreachable");
			}

			response.setStatus(0);
		} else if (context.getRemoteServerErrorStatusCode() != -1) {
			if (tracing) {
				logger.info("Remote server error code: {}", context.getRemoteServerErrorStatusCode());
			}

			response.setStatus(context.getRemoteServerErrorStatusCode());
		} else {

			String outputStr = "";
			logger.info("File Upload completed");	 
			
			 triggerUploadedFile(strProcess,context,response);
				
			 logger.info("File uplaod agent is triggered");

			/*output = new JSONObject();
			output.put("Result", "ApiSuccess"); */
			 
			outputStr= output.toJSONString();

			if (tracing) {
				logger.info("Output of DataService: {}", outputStr);
			}
			response.setContentType(jsonContentType);

			response.setCharacterEncoding("UTF-8");
			response.getWriter().print(output);  

		}		
		

	}
	
	public void triggerUploadedFile(String strProcess, UIContext context, HttpServletResponse response) throws ParseException, IOException{

		JSONObject inputObj = new JSONObject();
		boolean tracing = context.isTracing();	
		
		inputObj.put("CriteriaId",strProcess);
		
		if (tracing) {
			logger.info("DataService input: {}", inputObj);
		} 

		DataServiceDef dataServiceDef= DataServiceManager.getDataServiceDef(trigAgentDataServiceId);
			
		JSONParser parser = new JSONParser();		
		
		String strOutput = APIInvoker.invoke(context, dataServiceDef, inputObj.toJSONString(), dataServiceDef.getAPIName(), dataServiceDef.isExtended(), true)  ;
		
		if (context.isRemoteServerUnreachable()) {
			if (tracing) {
				logger.info("Remote server unreachable");
			}

			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		} else if (context.getRemoteServerErrorStatusCode() != -1) {
			if (tracing) {
				logger.info("Remote server error code: {}", context.getRemoteServerErrorStatusCode());
			}

			response.setStatus(context.getRemoteServerErrorStatusCode());
		} 
		
		/*else {

			String outputStr = "";
			if(strOutput != null) {
				JSONObject output = (JSONObject) parser.parse(strOutput);
			}


			output = new JSONObject();
			output.put("Result", "ApiSuccess");
			outputStr= output.toJSONString();

			if (tracing) {
				logger.info("Output of DataService: {}", outputStr);
			}
			response.setContentType(jsonContentType);

			response.setCharacterEncoding("UTF-8");
			response.getWriter().print(output);   

		}*/		
		
	}



}


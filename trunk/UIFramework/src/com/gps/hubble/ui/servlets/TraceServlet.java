/** 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/
package com.gps.hubble.ui.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.gps.hubble.ui.utilities.UIContext;

/**
 * 
 * @author Bridge
 * This servlet toggles the tracing. Both UI and application logs are enabled when tracing is enabled.
 */
@WebServlet("/api/trace")
public class TraceServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger();
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TraceServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.info("Trace servlet called");
		UIContext context = UIContext.getUIContext(request, response);
		context.setTracing(!context.isTracing());
		logger.info("Trace servlet status: {}", context.isTracing());
		response.getWriter().append("Is tracing: " + context.isTracing());
	}

}

/** 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/
package com.gps.hubble.ui.servlets;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.security.Principal;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import com.gps.hubble.ui.AppInfo;
import com.gps.hubble.ui.dataService.GetOrganizationList;
import com.gps.hubble.ui.utilities.DataServiceUtils;
import com.gps.hubble.ui.utilities.UIContext;

/**
 * Servlet implementation class SSOLoginServlet
 */
@WebServlet("/home")
public class SSOLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SSOLoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    @Override
	public void init() throws ServletException {
		super.init();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UIContext context = UIContext.getUIContext(request, response);
		Principal oPrincipal = request.getUserPrincipal();
		boolean tracing = context.isTracing();
		
		if ((oPrincipal == null) || (context.getLtpaToken() == null)) {
			if(tracing) {
				logger.error("User Principal || LTPA token is Null.");
			}
			response.getWriter().append("User Principal || LTPA token is Null.");
			return;
		}
		
		context.setLoginid((String) oPrincipal.getName());
		context.setSso_mode(true);
		logger.debug("LTPA Cookie Before Calling API "+context.getLtpaToken());
		JSONObject userDetail = new JSONObject();
		userDetail.put("UserId", context.getLoginid());
		
		JSONObject recordLoginInput = new JSONObject();
		recordLoginInput.put("UserId",  context.getLoginid());
		//Fix to track the User Session. We are currently considering LtpaToken as the Session ID, as it is expected to changed after timeout
		recordLoginInput.put("SessionId", context.getLtpaToken());
		DataServiceUtils.invokeDataService("recordLogin", recordLoginInput, context);
		JSONObject userListOut = DataServiceUtils.invokeDataService("getUserInfo", userDetail, context);
		if(userListOut != null) {
			JSONArray userArray=(JSONArray) userListOut.get("User");
			if(userArray!=null) {
				JSONObject userObj = (JSONObject) userArray.get(0);
				
				String mailid="";
				String orgCode = "";
				if(userObj !=null)
				{
					orgCode = (String)userObj.get("OrganizationKey") ;
					String orgName = "";
					try{
						orgName = GetOrganizationList.getOrganization(context, orgCode);
					}catch(ParseException pe) {
						pe.printStackTrace();
					} 
					context.setUsername((String) userObj.get("Username"));
					JSONObject contactObj= (JSONObject) userObj.get("ContactPersonInfo");
					if(contactObj !=null)
					{
						mailid = (String)contactObj.get("EMailID") ;
					}
					context.setUserOrgCode(orgCode);
					context.setUserOrgName(orgName);
					context.setEmailId(mailid); 
					context.setTeamId((String) userObj.get("DataSecurityGroupId"));
				}
			
			}
		}
		
		if (context.getRemoteServerErrorStatusCode() != -1) {
				logger.error("API Call Failed "+context.getRemoteServerErrorStatusCode()+" With Error Code "+context.getRemoteServerErrorDescription());
			
			response.getWriter().append("Invalid LTPA Token."+context.getRemoteServerErrorStatusCode()+" With Error Code "+context.getRemoteServerErrorDescription());
			return;
		} else if (context.isRemoteServerUnreachable()) {
			if(tracing) {
				logger.error("Remote server Unreachable.");
			}
			response.getWriter().append("Remote server Unreachable.");
			return;
		}
		
		context.createCSRFToken();
		response.setStatus(HttpURLConnection.HTTP_MOVED_TEMP);

		String url = context.getRedirectUrl(AppInfo.getHomepage());
		url = url.concat(AppInfo.getIndex()) ;
		response.sendRedirect(url);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("POST Invoke in SSO Login Servlets");
		response.getWriter().append("POST Served by SSO login servlet: ").append(request.getContextPath());
	}

}

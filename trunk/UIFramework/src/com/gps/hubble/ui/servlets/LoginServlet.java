/** 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/
package com.gps.hubble.ui.servlets;

import java.io.IOException;
import java.net.HttpURLConnection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import com.gps.hubble.ui.AppInfo;
import com.gps.hubble.ui.dataService.GetOrganizationList;
import com.gps.hubble.ui.utilities.DataServiceUtils;
import com.gps.hubble.ui.utilities.UIContext;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger();
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
    }

	@Override
	public void init() throws ServletException {
		super.init();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served by login servlet: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UIContext context = UIContext.getUIContext(request, response);
		boolean tracing = context.isTracing();
		JSONObject loginInput = new JSONObject();
		JSONObject userListInput = new JSONObject();
		JSONObject userListOut = new JSONObject();
//		loginInput.put("LoginID", request.getParameter("LoginID"));
//		loginInput.put("Password", request.getParameter("Password"));
		JSONObject output = DataServiceUtils.invokeDataService("login", loginInput, context);
		
		if (context.getRemoteServerErrorStatusCode() != -1) {
			if(tracing) {
				logger.error("Invalid user password combination."+context.getRemoteServerErrorDescription());
			}
			response.getWriter().append("Invalid user password combination. ");
			return;
		} else if (context.isRemoteServerUnreachable()) {
			if(tracing) {
				logger.error("Remote server Unreachable."+AppInfo.getRemoteserverurl());
			}
			response.getWriter().append("Remote server Unreachable.");
			return;
		}
		String orgCode = (String)output.get("OrganizationCode");
		context.setLoginid((String) output.get("LoginID"));
		context.setUsername((String) output.get("UserName"));
		context.setToken((String) output.get("UserToken"));
		context.setUserOrgCode(orgCode);
		String orgName = "";
		try{
			orgName = GetOrganizationList.getOrganization(context, orgCode);
		}catch(ParseException pe) {
			pe.printStackTrace();
		} 
		context.setUserOrgName(orgName);

		userListInput.put("Loginid",(String) output.get("LoginID"));

		JSONObject recordLoginInput = new JSONObject();
		recordLoginInput.put("UserId", (String) output.get("LoginID"));
		recordLoginInput.put("SessionId", (String) output.get("UserToken"));
		DataServiceUtils.invokeDataService("recordLogin", recordLoginInput, context);

		userListOut = DataServiceUtils.invokeDataService("getUserInfo", userListInput, context);
		if(userListOut != null) {
			JSONArray array=(JSONArray) userListOut.get("User");
			if(array != null) {
				JSONObject userObj = (JSONObject) array.get(0);
				JSONObject contactObj= (JSONObject) userObj.get("ContactPersonInfo");
				String mailid=""; 
		
				if(contactObj !=null)
				{
					mailid = (String)contactObj.get("EMailID") ;
				}
				context.setEmailId(mailid);
				context.setTeamId((String)userObj.get("DataSecurityGroupId"));
				
			}
		}	
		
		context.createCSRFToken();
		response.setStatus(HttpURLConnection.HTTP_MOVED_TEMP);
		String url = context.getRedirectUrl(AppInfo.getHomepage());
		url = url.concat(AppInfo.getIndex()) ;
		//String url = context.getRedirectUrlNew(AppInfo.getHomepage(),AppInfo.getIndex());
		//url = url.concat(AppInfo.getIndex()) ;		
		response.sendRedirect(url);
	}

}

/** 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/
package com.gps.hubble.ui.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.gps.hubble.ui.dataService.DataServiceManager;
import com.gps.hubble.ui.utilities.UIContext;

/**
 * 
 * @author Bridge
 * This class loads the API output templates from the template xmls into the DB table.
 * These templates are referenced when invoking the REST apis.
 */

@WebServlet("/api/synctemplates")
public class SyncTemplatesServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger();
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SyncTemplatesServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.info("Template sync called.");
		UIContext context = UIContext.getUIContext(request, response);
		DataServiceManager.syncDataServiceTemplates(context);
		logger.info("Template Sync Completed");
		response.getWriter().append("Template sync completed.");
	}

}

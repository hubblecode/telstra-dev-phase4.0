/** 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/
package com.gps.hubble.ui.servlets.filehandler;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.gps.hubble.ui.utilities.UIContext;

import java.io.*;
import javax.servlet.Servlet;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;  

/**
 * Servlet class implementation for DownloadServlet
 * @author Sambeet Mohanty
 * @version 1.0 ( 21 Jul 2016)
 */
@WebServlet("/filedownloadservlet")
public class DownloadServlet extends HttpServlet implements Servlet{
	static final long serialVersionUID = 1L;
	private static final int BUFSIZE = 4096;
	private static final Logger logger = LogManager.getLogger();
	private static final String fileExtn=".xlsx";
	
	/**
	 * Upon receiving file download submission, parses the request and downloads the file to users window
	 * Method name: doGet
	 * @param request
	 * @return void
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		UIContext context = UIContext.getUIContext(request, response);				
		boolean tracing = context.isTracing();
		
		String downloadFileName=request.getParameter("process")+fileExtn;

		if (tracing) {
			logger.info("upload servlet triggered for file: "+ downloadFileName);

		}

		ServletContext servletContext = request.getSession().getServletContext();
		String absoluteDiskPath = servletContext.getRealPath("WEB-INF")+ File.separator + downloadFileName;

		if (tracing) {
			logger.info("absoluteDiskPath: "+ absoluteDiskPath);

		}
		
		if (context.isRemoteServerUnreachable()) {
			if (tracing) {
				logger.info("Remote server unreachable");
			}

			response.setStatus(0);
		} else if (context.getRemoteServerErrorStatusCode() != -1) {
			if (tracing) {
				logger.info("Remote server error code: {}", context.getRemoteServerErrorStatusCode());
			}

			response.setStatus(context.getRemoteServerErrorStatusCode());
		}
		else{
			downloadFileTemplate(absoluteDiskPath, context, response);
		}

		
	}
	private void downloadFileTemplate(String absoluteDiskPath,UIContext context, HttpServletResponse response) throws IOException
	{
		File file = new File(absoluteDiskPath);
		int length   = 0;
		ServletOutputStream outStream = response.getOutputStream();
		//response.setContentType("text/html");

		response.setContentLength((int)file.length());
		String fileName = (new File(absoluteDiskPath)).getName();
		
		ServletContext sc = getServletContext();
		String mimeType = sc.getMimeType(fileName);
		if(mimeType == null) {
			//Default file download type is xslx
			//mimeType = "application/octet-stream";
			mimeType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
		}
		response.setContentType(mimeType);
		response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

		byte[] byteBuffer = new byte[BUFSIZE];
		DataInputStream in = new DataInputStream(new FileInputStream(file));

		while ((in != null) && ((length = in.read(byteBuffer)) != -1))
		{
			outStream.write(byteBuffer,0,length);
		}	
		
		if(context.isTracing())
		{
			logger.info("Template File downloaded successfully");
		}


		in.close();
		outStream.close();


	}

}  
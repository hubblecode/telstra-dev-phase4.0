/** 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/
package com.gps.hubble.ui.servlets;

import java.io.IOException;
import java.util.concurrent.ThreadPoolExecutor;

import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gps.hubble.ui.listener.JobMonitorListener;
import com.gps.hubble.ui.notification.JobMonitor;

/**
 * Servlet implementation class APIServlet
 */
@WebServlet(urlPatterns = {"/jobmonitor"}, asyncSupported = true)
public class JobMonitorServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		AsyncContext asyncCtx = req.startAsync(req,resp);
		asyncCtx.addListener(new JobMonitorListener());
		asyncCtx.setTimeout(0);
		String initSleepTime = req.getServletContext().getInitParameter("SLEEP_TIME");
		int iSleepTime = 20000;
		if(initSleepTime != null) {
			try {
				iSleepTime = Integer.parseInt(initSleepTime);
			}catch (NumberFormatException nfe) {
				iSleepTime = 20000;
			}
			
		}
		ThreadPoolExecutor executor = (ThreadPoolExecutor)req.getServletContext().getAttribute("monitorPool");
		executor.execute(new JobMonitor(asyncCtx,iSleepTime));
		//super.doGet(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		super.doPost(req, resp);
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		super.destroy();
	}

	@Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
	}
	
	

}

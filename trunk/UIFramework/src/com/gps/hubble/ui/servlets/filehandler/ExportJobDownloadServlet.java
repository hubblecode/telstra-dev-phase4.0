/** 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/
package com.gps.hubble.ui.servlets.filehandler;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.gps.hubble.ui.dataService.APIInvoker;
import com.gps.hubble.ui.dataService.DataServiceDef;
import com.gps.hubble.ui.dataService.DataServiceManager;
import com.gps.hubble.ui.utilities.UIContext;

import java.io.*;

import javax.servlet.Servlet;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;  

/**
 * Servlet class implementation for JobDownloadServlet
 * @author Shravan Narayan
 * @version 1.0 ( 26 Jul 2017)
 */
@WebServlet("/exportjobservlet")
public class ExportJobDownloadServlet extends HttpServlet implements Servlet{
	static final long serialVersionUID = 1L;
	private static final int BUFSIZE = 4096;
	private static final Logger logger = LogManager.getLogger();
	private static final String fileExtn=".csv";
	private static final String dataServiceId = "exportDetails"; 
	
	
	/**
	 * Upon receiving file download submission, parses the request and downloads the file to users window
	 * Method name: doGet
	 * @param request
	 * @return void
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String downloadFileName=request.getParameter("ExportKey");
		UIContext context = UIContext.getUIContext(request, response);				
		boolean tracing = context.isTracing();
		JSONObject inputObj = new JSONObject();
		inputObj.put("ExportRequestKey",downloadFileName);
		if (tracing) {
			logger.info("DataService input: {}", inputObj);
		} 

		DataServiceDef dataServiceDef= DataServiceManager.getDataServiceDef(dataServiceId);
	    try {
			JSONParser parser = new JSONParser();	
			JSONObject output = (JSONObject) parser.parse( APIInvoker.invoke(context, dataServiceDef, inputObj.toJSONString(), dataServiceDef.getAPIName(), dataServiceDef.isExtended(), true))  ;

			if (context.isRemoteServerUnreachable()) {
				if (tracing) {
					logger.info("Remote server unreachable");
				}

				response.setStatus(0);
			} else if (context.getRemoteServerErrorStatusCode() != -1) {
				if (tracing) {
					logger.info("Remote server error code: {}", context.getRemoteServerErrorStatusCode());
				}

				response.setStatus(context.getRemoteServerErrorStatusCode());
			} else {

				String FileEncodedStr = "";
				FileEncodedStr= (String) output.get("ExportFile");

				if (tracing) {
					logger.info("FileEncodedString: {}",FileEncodedStr);
				}
				if(FileEncodedStr != null && FileEncodedStr.length() > 0) {
					 byte[] decodedBytes = Base64.decodeBase64(FileEncodedStr.getBytes())  ;    
					 downloadFileTemplate(output,decodedBytes,context, response);
				}
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	

		
	}
	private void downloadFileTemplate(JSONObject output,byte[] decodedBytes,UIContext context, HttpServletResponse response) throws IOException
	{
		String fileName =(String) output.get("ExportType")+"_"+(String) output.get("ExportRequestKey")+fileExtn;
		int length   = 0;
		ServletOutputStream outStream = response.getOutputStream();
		response.setContentLength((int)decodedBytes.length);
		ServletContext sc = getServletContext();
		String mimeType = sc.getMimeType(fileName);
		if(mimeType == null) {
			mimeType = "text/csv";
		}
		response.setContentType(mimeType);
		response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

		byte[] byteBuffer = new byte[BUFSIZE];
		DataInputStream in = new DataInputStream(new ByteArrayInputStream(decodedBytes));

		while ((in != null) && ((length = in.read(byteBuffer)) != -1))
		{
			outStream.write(byteBuffer,0,length);
		}	
		
		if(context.isTracing())
		{
			logger.info(" File downloaded successfully");
		}


		in.close();
		outStream.close();


	}

}  
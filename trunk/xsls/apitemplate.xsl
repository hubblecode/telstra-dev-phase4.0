<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output indent="yes"/>
    <xsl:template match="/mashups">
		<xsl:element name="DataServiceTemplateList">
			<xsl:attribute name="AppCode"><xsl:value-of select="@appshortcode"/></xsl:attribute>
			<xsl:apply-templates select="mashup[not(@aggregator='true')]"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="mashup">
		<xsl:element name="DataServiceTemplate">
			<xsl:attribute name="DataServiceId"><xsl:value-of select="@id"/></xsl:attribute>
			<xsl:attribute name="APIName"><xsl:value-of select="API/@name"/></xsl:attribute>
			<xsl:copy-of select="API/Template"/>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>	
	
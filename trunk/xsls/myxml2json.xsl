<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text" encoding="utf-8" indent="yes"/>
 
    <xsl:template match="/mashups">
        <xsl:text>{</xsl:text>
		<xsl:text>"appshortcode" :</xsl:text>
		<xsl:text>"</xsl:text>
		<xsl:value-of select="@appshortcode"/>
		<xsl:text>"</xsl:text>
		<xsl:text>,</xsl:text>
		<xsl:text>&#xa;</xsl:text>
		<xsl:text>"dataservice"</xsl:text>
		<xsl:text>:</xsl:text>
		<xsl:text>[</xsl:text>
        <xsl:apply-templates select="mashup" />
		<xsl:text>]</xsl:text>
        <xsl:text>}</xsl:text>
    </xsl:template>
	<xsl:template match="mashup">
		<xsl:text>{</xsl:text>
		<xsl:text>&#xa;</xsl:text>
		<xsl:text>"name"</xsl:text>
		<xsl:text>:</xsl:text>
		<xsl:text>"</xsl:text>
		<xsl:value-of select="@id"/>
		<xsl:text>"</xsl:text>
		<xsl:text>,</xsl:text>
		<xsl:text>&#xa;</xsl:text>
		<xsl:text>"description"</xsl:text>
		<xsl:text>:</xsl:text>
		<xsl:text>"</xsl:text>
		<xsl:value-of select="@description"/>
		<xsl:text>"</xsl:text>
		<xsl:text>,</xsl:text>
		<xsl:text>&#xa;</xsl:text>
		<xsl:text>"apiname"</xsl:text>
		<xsl:text>:</xsl:text>
		<xsl:text>"</xsl:text>
		<xsl:value-of select="API/@name"/>
		<xsl:text>"</xsl:text>
		<xsl:text>,</xsl:text>
		<xsl:text>&#xa;</xsl:text>
		<xsl:text>"isextended"</xsl:text>
		<xsl:text>:</xsl:text>
		<xsl:text>"</xsl:text>
		<xsl:value-of select="API/@service"/>
		<xsl:text>"</xsl:text>
		<xsl:text>,</xsl:text>
		<xsl:text>&#xa;</xsl:text>
		<xsl:text>"isprototype"</xsl:text>
		<xsl:text>:</xsl:text>
		<xsl:text>"</xsl:text>
		<xsl:value-of select="@prototype"/>
		<xsl:text>"</xsl:text>
		<xsl:text>&#xa;</xsl:text>
		<xsl:choose>
		<xsl:when test="@useTemplate = 'false'">
			<xsl:text>,</xsl:text>		
			<xsl:text>"isTemplated"</xsl:text>
			<xsl:text>:</xsl:text>
			<xsl:text>"</xsl:text>
			<xsl:value-of select="@useTemplate"/>
			<xsl:text>"</xsl:text>
			<xsl:text>&#xa;</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text>,</xsl:text>		
			<xsl:text>"isTemplated"</xsl:text>
			<xsl:text>:</xsl:text>
			<xsl:text>"</xsl:text>
			<xsl:text>true</xsl:text>
			<xsl:text>"</xsl:text>
			<xsl:text>&#xa;</xsl:text>
		</xsl:otherwise>
		</xsl:choose>
		<xsl:choose>
		<xsl:when test="@RemoveEmptyAttr = 'false'">
			<xsl:text>,</xsl:text>		
			<xsl:text>"retainemptyattrs"</xsl:text>
			<xsl:text>:</xsl:text>
			<xsl:text>"</xsl:text>
			<xsl:text>true</xsl:text>
			<xsl:text>"</xsl:text>
			<xsl:text>&#xa;</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text>,</xsl:text>		
			<xsl:text>"retainemptyattrs"</xsl:text>
			<xsl:text>:</xsl:text>
			<xsl:text>"</xsl:text>
			<xsl:text>false</xsl:text>
			<xsl:text>"</xsl:text>
			<xsl:text>&#xa;</xsl:text>
		</xsl:otherwise>
		</xsl:choose>		
		
		<xsl:choose>
		<xsl:when test="API/@validationRequired = 'false'">
			<xsl:text>,</xsl:text>		
			<xsl:text>"validate"</xsl:text>
			<xsl:text>:</xsl:text>
			<xsl:text>"</xsl:text>
			<xsl:value-of select="API/@validationRequired"/>
			<xsl:text>"</xsl:text>
			<xsl:text>&#xa;</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text>,</xsl:text>		
			<xsl:text>"validate"</xsl:text>
			<xsl:text>:</xsl:text>
			<xsl:text>"</xsl:text>
			<xsl:text>true</xsl:text>
			<xsl:text>"</xsl:text>
			<xsl:text>&#xa;</xsl:text>
		</xsl:otherwise>
		</xsl:choose>
		<xsl:if test="@aggregator = 'true'">
			<xsl:text>,</xsl:text>		
			<xsl:text>"iscomposite"</xsl:text>
			<xsl:text>:</xsl:text>
			<xsl:text>"</xsl:text>
			<xsl:value-of select="@aggregator"/>
			<xsl:text>"</xsl:text>
				<xsl:text>,</xsl:text>
				<xsl:text>&#xa;</xsl:text>
				<xsl:text>"composite"</xsl:text>
				<xsl:text>:</xsl:text>
				<xsl:text>[</xsl:text>
				<xsl:apply-templates select="ref"/>
				<xsl:text>]</xsl:text>
				<xsl:text>&#xa;</xsl:text>				
		</xsl:if>
<!--		<xsl:if test="classInformation/@name != 'com.gps.hubble.ui.mashup.Mashup' and classInformation/@name != 'com.gps.hubble.ui.mashup.AggregatorMashup'">
			<xsl:text>,</xsl:text>		
			<xsl:text>"hookclass"</xsl:text>
			<xsl:text>:</xsl:text>
			<xsl:text>"</xsl:text>
			<xsl:value-of select="translate(classInformation/@name,'mashup','dataService')"/>
			<xsl:text>"</xsl:text>
			<xsl:text>&#xa;</xsl:text>
		</xsl:if>
	-->	
				<xsl:if test="classInformation/@name = 'com.gps.hubble.ui.mashup.OrderListMashup'">
			<xsl:text>,</xsl:text>		
			<xsl:text>"hookclass"</xsl:text>
			<xsl:text>:</xsl:text>
			<xsl:text>"</xsl:text>
			<xsl:text>com.gps.hubble.ui.dataService.OrderListDataService</xsl:text>
			<xsl:text>"</xsl:text>
			<xsl:text>&#xa;</xsl:text>
		</xsl:if>
				<xsl:if test="classInformation/@name = 'com.gps.hubble.ui.mashup.OrderDetailsMashup'">
			<xsl:text>,</xsl:text>		
			<xsl:text>"hookclass"</xsl:text>
			<xsl:text>:</xsl:text>
			<xsl:text>"</xsl:text>
			<xsl:text>com.gps.hubble.ui.dataService.OrderDetailsDataService</xsl:text>
			<xsl:text>"</xsl:text>
			<xsl:text>&#xa;</xsl:text>
		</xsl:if>
				<xsl:if test="classInformation/@name = 'com.gps.hubble.ui.mashup.ShipmentDetailsMashup'">
			<xsl:text>,</xsl:text>		
			<xsl:text>"hookclass"</xsl:text>
			<xsl:text>:</xsl:text>
			<xsl:text>"</xsl:text>
			<xsl:text>com.gps.hubble.ui.dataService.ShipmentDetailsDataService</xsl:text>
			<xsl:text>"</xsl:text>
			<xsl:text>&#xa;</xsl:text>
		</xsl:if>
		
		<xsl:if test="classInformation/@name = 'com.gps.hubble.ui.mashup.ItecItemDetailsMashup'">
			<xsl:text>,</xsl:text>		
			<xsl:text>"hookclass"</xsl:text>
			<xsl:text>:</xsl:text>
			<xsl:text>"</xsl:text>
			<xsl:text>com.gps.hubble.ui.dataService.ItecItemDetailsDataService</xsl:text>
			<xsl:text>"</xsl:text>
			<xsl:text>&#xa;</xsl:text>
		</xsl:if>
		<xsl:if test="classInformation/@name = 'com.gps.hubble.ui.mashup.SerialListMashup'">
			<xsl:text>,</xsl:text>		
			<xsl:text>"hookclass"</xsl:text>
			<xsl:text>:</xsl:text>
			<xsl:text>"</xsl:text>
			<xsl:text>com.gps.hubble.ui.dataService.SerialListDataService</xsl:text>
			<xsl:text>"</xsl:text>
			<xsl:text>&#xa;</xsl:text>
		</xsl:if>
		<xsl:if test="classInformation/@name = 'com.gps.hubble.ui.mashup.ExportRequestMashup'">
			<xsl:text>,</xsl:text>		
			<xsl:text>"hookclass"</xsl:text>
			<xsl:text>:</xsl:text>
			<xsl:text>"</xsl:text>
			<xsl:text>com.gps.hubble.ui.dataService.ExportRequestDataService</xsl:text>
			<xsl:text>"</xsl:text>
			<xsl:text>&#xa;</xsl:text>
		</xsl:if>		
		<xsl:apply-templates select="API/Input"/>
		<xsl:text>}</xsl:text>
		<xsl:if test="name(following-sibling::*) = name(current())">, </xsl:if>	
	</xsl:template>

	<xsl:template match="ref">
			<xsl:text>{</xsl:text>
			<xsl:text>"name"</xsl:text>
			<xsl:text>:</xsl:text>						
			<xsl:text>"</xsl:text>
			<xsl:value-of select="@id"/>
			<xsl:text>"</xsl:text>
			<xsl:text>,</xsl:text>
			<xsl:text>"sequence"</xsl:text>
			<xsl:text>:</xsl:text>
			<xsl:text>"</xsl:text>
			<xsl:value-of select="@sequence"/>
			<xsl:text>"</xsl:text>
			<xsl:text>}</xsl:text>
		<xsl:if test="name(following-sibling::*) = name(current())">, </xsl:if>
	</xsl:template>
	<xsl:template match="Input">
		
<!--		<xsl:text>"input"</xsl:text>-->
		<xsl:choose>
		<xsl:when test="count(child::*) > 0 or count(@*) > 0">
			<xsl:text>,</xsl:text>
			<xsl:text>"input" : {</xsl:text> 
			<xsl:apply-templates select="child::*" mode="detect" />
			<xsl:text>}</xsl:text>
			<!--<xsl:if test="count(following-sibling::*) &gt; 0">, </xsl:if>-->
		</xsl:when>
	</xsl:choose>
	</xsl:template>
	
    <xsl:template match="*" mode="detect">
        <xsl:choose>
            <xsl:when test="name(preceding-sibling::*[1]) = name(current()) and name(following-sibling::*[1]) != name(current())">
                    <xsl:apply-templates select="." mode="obj-content" />
                <xsl:text>]</xsl:text>
                <xsl:if test="count(following-sibling::*[name() != name(current())]) &gt; 0">, </xsl:if>
            </xsl:when>
            <xsl:when test="name(preceding-sibling::*[1]) = name(current())">
                    <xsl:apply-templates select="." mode="obj-content" />
                    <xsl:if test="name(following-sibling::*) = name(current())">, </xsl:if>
            </xsl:when>
            <xsl:when test="following-sibling::*[1][name() = name(current())]">
                <xsl:text>"</xsl:text><xsl:value-of select="name()"/><xsl:text>" : [</xsl:text>
                    <xsl:apply-templates select="." mode="obj-content" /><xsl:text>, </xsl:text>
            </xsl:when>
            <xsl:when test="count(./child::*) > 0 or count(@*) > 0">
                <xsl:text>"</xsl:text><xsl:value-of select="name()"/>" : <xsl:apply-templates select="." mode="obj-content" />
                <xsl:if test="count(following-sibling::*) &gt; 0">, </xsl:if>
            </xsl:when>
            <xsl:when test="count(./child::*) = 0">
                <xsl:text>"</xsl:text><xsl:value-of select="name()"/>" : "<xsl:apply-templates select="."/><xsl:text>"</xsl:text>
                <xsl:if test="count(following-sibling::*) &gt; 0">, </xsl:if>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
 
    <xsl:template match="*" mode="obj-content">
        <xsl:text>{</xsl:text>
            <xsl:apply-templates select="@*" mode="attr" />
            <!--<xsl:if test="count(@*) &gt; 0 and (count(child::*) &gt; 0 or text())">, </xsl:if>-->
			<xsl:if test="count(@*) &gt; 0 and count(child::*) &gt; 0">, </xsl:if>
            <xsl:apply-templates select="./*" mode="detect" />
            <!--<xsl:if test="count(child::*) = 0 and text() and not(@*)">
                <xsl:text>"</xsl:text><xsl:value-of select="name()"/>" : "<xsl:value-of select="text()"/><xsl:text>"</xsl:text>
            </xsl:if>-->
            <!--<xsl:if test="count(child::*) = 0 and text() and @*">
                <xsl:text>"text" : "</xsl:text><xsl:value-of select="text()"/><xsl:text>"</xsl:text>
            </xsl:if>-->
        <xsl:text>}</xsl:text>
       <!-- <xsl:if test="position() &lt; last()">, </xsl:if>-->
    </xsl:template>
 
    <xsl:template match="@*" mode="attr">
        <xsl:text>"</xsl:text><xsl:value-of select="name()"/>" : "<xsl:value-of select="."/><xsl:text>"</xsl:text>
        <xsl:if test="position() &lt; last()">,</xsl:if>
		<xsl:text>&#xa;</xsl:text>
    </xsl:template>
	
	
</xsl:stylesheet>	
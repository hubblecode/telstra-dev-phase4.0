<!-- Copyright (c) IBM Australia Limited 2016. All rights reserved.  -->

<%@page import="com.gps.hubble.ui.utilities.Utils"%>
<%@page import="com.gps.hubble.ui.AppInfo"%>
<%@page import="com.gps.hubble.ui.utilities.UIContext"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.gps.hubble.ui.utilities.DataServiceUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Telstra Portal</title>

<style>
[v-cloak] {
	display: none;
}
</style>
</head>

<body>
	<%
  	UIContext context = UIContext.getUIContext(request, response);
  	if(AppInfo.isSsomode() && context.getLoginid() == null) {
  		RequestDispatcher rd = request.getRequestDispatcher("home");
  		rd.forward(request, response);
  	}
  //	String csrfToken = context.getCsrfToken();
  String csrfToken = request.getParameter("hubblecsrf");
  	String loginid = context.getLoginid();
        String mailid = context.getEmailId();
        String teamId = context.getTeamId();
        String userName = context.getUserName();        
        boolean traceenabled = context.isTracing();
        String enableTrace = traceenabled ? "Y" : "N";
  	boolean isUsingWebpack = Utils.getBooleanValue(request.getHeader("hubblewebpack"));
	List<String> resourceList = DataServiceUtils.getResourceListForUser(context);
	String orgCode = context.getUserOrgCode();
String logoutUrl = application.getContextPath()+"/logout.jsp";
  %>
	<script type="text/javascript">
	if (!window.AppInfo) {
	  	window.AppInfo = {}
	}
  	window.AppInfo.contextroot = "<%=request.getContextPath()%>";
  	window.AppInfo.hubblecsrftoken = "<%=csrfToken%>";
  	window.AppInfo.teamId = "<%=teamId%>";
  	window.AppInfo.loginpage = "<%=AppInfo.getLoginpage()%>";
                            window.AppInfo.ssomode = "<%=AppInfo.isSsomode()%>";
  	window.AppInfo.loginid = "<%=loginid%>";
 							window.AppInfo.emailid = "<%=mailid%>";
 							window.AppInfo.traceenabled = "<%=enableTrace%>";
							window.AppInfo.resources="<%=resourceList%>";
							window.AppInfo.orgcode = "<%=orgCode%>";
  </script>
	<div id="app" v-cloak>
		<div>
			<header class=" top-bar">
			<div class="navbar-fixed-top">
				<div class="row top-bar-border no-gutters nav-down">
					<div class="container-fluid">
						<div class="row">
							<div class="col-sm-5 col-xs-5">
								<div class="navbar-brand p-l-0">
									<img src="images/ibm-logo.png" class="pull-left" alt="">
								</div>
							</div>
							<div class="col-md-7 col-xs-7 ">
								<div class="clearfix">

									<div class="pull-right">
										<ul class="nav navbar-nav navbar-nav-dropdown clearfix">
											<li>
												<div id="custom-search-input">
													<searchorder> </searchorder>

												</div>
											</li>
											<li class="dropdown"><a href="#" class="dropdown-toggle header_dropdown"
												data-toggle="dropdown" role="button" aria-haspopup="true"
												aria-expanded="false" type="button"><i
													class="glyphicon glyphicon-user"></i></a>
												<ul class="dropdown-menu">
													<li class="UserName"><span>Logged In As</span><i
														class="fa fa-user" aria-hidden="true"></i> <%=userName%></li>
													<!--   <li><a href="#"><i class="fa fa-info-circle" aria-hidden="true"></i> About</a></li>  -->
													<li><a href="<%=logoutUrl%>"><i
															class="fa fa-sign-out" aria-hidden="true"></i> Sign Out</a></li>


												</ul></li>
										</ul>

									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
				<nav class="navbar navbar-default cusBottom">
				<div class="row-border-bottom no-gutters">
					<div class="container-fluid">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed"
								data-toggle="collapse"
								data-target="#bs-example-navbar-collapse-1"
								aria-expanded="false">
								<span class="sr-only">Toggle navigation</span> <span
									class="icon-bar"></span> <span class="icon-bar"></span> <span
									class="icon-bar"></span>
							</button>
							<div>
								<a class="navbar-brand" v-link="{ path:'/home'}">
									 <i class="glyphicon glyphicon-home"></i> <h4>Telstra Portal</h4>
								</a>
							</div>
						</div>
						<div class="collapse navbar-collapse"
							id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav navbar-right">
								<%
    		if(resourceList.contains("GPSTPORD0001")) {
        %>
								<li><a v-link="{ path:'/orderlist'}"> Orders</a></li>
								<% 
    		} if(resourceList.contains("GPSTPINV0001")) {
        %>
								<li><a v-link="{ path:'/inventoryList'}">Inventory</a></li>
								<%
    		}
   		%>
							</ul>
						</div>
					</div>
				</div>
				</nav>
			</div>

			</header>
		</div>
		<!-- route outlet -->
		<router-view></router-view>
	</div>
	<% if(isUsingWebpack) { %>
	<script src="/dist/vendor.js"></script>
	<script src="/dist/app.js"></script>
	<%} else {%>
	<script src="./dist/vendor.js"></script>
	<script src="./dist/app.js"></script>
	<% } %>

	<footer class="footer LoginFooter">
	<div class="container">
		<div class="text-center">© Copyright IBM Corporation 2016</div>
	</div>
	</footer>
</body>

</html>

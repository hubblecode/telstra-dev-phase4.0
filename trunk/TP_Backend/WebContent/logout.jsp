<!-- Copyright (c) IBM Australia Limited 2016. All rights reserved.  -->

<%@page import="com.gps.hubble.ui.utilities.DataServiceUtils"%>
<%@page import="com.gps.hubble.ui.utilities.Utils"%>
<%@page import="com.gps.hubble.ui.AppInfo"%>
<%@page import="com.gps.hubble.ui.utilities.UIContext"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
 <meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Login</title>
<script type="text/javascript">
	if (typeof(Storage) !== "undefined" ) {
		sessionStorage.clear();
	}
</script>
</head>
<body class="bodybg">

 <%
  	UIContext context = UIContext.getUIContext(request, response);
  	String csrfToken = context.getCsrfToken();
  	String loginid = context.getLoginid();
        String mailid = context.getEmailId();
  	boolean isUsingWebpack = Utils.getBooleanValue(request.getHeader("hubblewebpack"));
	String sessionTimeOut = request.getParameter("SessionTimeOut");
    DataServiceUtils.logoutUser(context,sessionTimeOut);
    
	boolean ssomode = AppInfo.isSsomode();
		
	String contextPath=request.getRequestURL().toString();
	
	String logoutUrl = contextPath.substring(0,contextPath.indexOf(request.getRequestURI()))+"/pkmslogout";
	if(ssomode) {
		response.sendRedirect(logoutUrl);
	}
	
  %>
    <header class=" top-bar">
        <div class="">
            <div class="row top-bar-border no-gutters nav-down">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-5 col-xs-5">
                            <div class="navbar-brand p-l-0">
                                <a href=""><img src="./images/ibm-logo.png" class="pull-left" alt=""></a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <nav class="navbar navbar-default cusBottom">
                <div class="row-border-bottom no-gutters">
                    <div class="container-fluid">
                        <div class="navbar-header">

                            <div class="navbar-brand" ><h4>Telstra Portal</h4></div>
                        </div>

                    </div>
                </div>
            </nav>
        </div>

    </header>
       <div class="container">
       <span>You have been logged out. Click <a href="<%=application.getContextPath()+"/login.jsp"%>">Here</a> to Login again.</span>
    </div>

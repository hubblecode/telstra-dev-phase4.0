/* 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/	
var BridgeDateUtils = require('./../utils/utils.js')
var Vue = require('vue')
Vue.directive('datepicker', {
  twoWay: true,
  params: ['usage', 'timeofday','fieldname','allowpastdate','allowfuturedate'],
  bind: function () {
    var self = this
    var fromfieldname='#From'+self.params.fieldname
    var tofieldname='#To'+self.params.fieldname
    var isValidData = true
 if(!this.params.usage) {
      this.params.usage = "datetime"
    }
    
if(this.params.allowpastdate == undefined) {
	this.params.allowpastdate = 'Y'
}  

if(this.params.allowfuturedate == undefined) {
	this.params.allowfuturedate = 'Y'
} 
    $(this.el).on("keyup", function(event) {
          validateDate(this);
       });
    $(this.el).datepicker({
    	dateFormat: 'dd/mm/yy',
        changeYear: true,
        changeMonth: true,
    	onSelect: function (date) {
         if(self.params.timeofday && self.params.timeofday === 'begin'){
              var dt2 = $(tofieldname);
               var minDate = $(this).datepicker('getDate');
               dt2.datepicker('option', 'minDate', minDate);
		}
    
            
	   	$(this).change()
          this.focus()
      	},
      onClose: function (date) {
          this.blur()
         /* if(self.isValidData) {
            this.blur()    
          } else {
              this.focus()
          } */
          

        }
			

    }).on("change", function() {
         var serverdate = BridgeDateUtils.formatForServer(this.value, self.params.usage,self.params.timeofday)
        // var dateValue = $(this).datepicker('getDate');
         var dateValue = new Date(serverdate)
         if(self.params.timeofday && self.params.timeofday === 'begin'){
              var dt2 = $(tofieldname);
               dt2.datepicker('option', 'minDate', dateValue);
		}
        validateDate(this,dateValue)
        self.set(serverdate)
    });
      
    $(this.el).datepicker($.datepicker.regional[ "en-AU" ])
	
     if(self.params.allowpastdate === 'N') {
     	$(this.el).datepicker("option","minDate",0);
	}
	if(self.params.allowfuturedate === 'N') {
     	$(this.el).datepicker("option","maxDate",0);
	}
    var validateDate = function(datefield,dateValue) {
         var sClassName = datefield.className;
         var oClassNameArray = sClassName.split(' ');
         var sInvalidClass = 'invalid';
         var iInavlidClassIndex = oClassNameArray.indexOf(sInvalidClass);
         var oErrorMaskTag = datefield.parentElement.querySelector('p');
         var sStyleAttr = null;
         if(oErrorMaskTag != null)
            sStyleAttr = oErrorMaskTag.getAttribute('style');
        
          if(isValidDate(datefield.value,dateValue)){
  
              if(iInavlidClassIndex != -1){
                  oClassNameArray.splice(iInavlidClassIndex,1);
                  datefield.className = oClassNameArray.join(' '); 
              }
              
              if(sStyleAttr == null) {
                  if(oErrorMaskTag != null) {
                      oErrorMaskTag.setAttribute('style',"display:none");
                  }
              }
                 
          }              
          else {
             
              if(iInavlidClassIndex == -1)
                  oClassNameArray.push(sInvalidClass);
              
               if(oErrorMaskTag != null && sStyleAttr != null)
                  oErrorMaskTag.removeAttribute('style');
              
              datefield.className = oClassNameArray.join(' ');
              
          } 
        }

    /* Validates the date value entered manually */
    
    var isValidDate = function(dateFieldVal,dateVal) {
         try {
            var validDate = $.datepicker.parseDate('dd/mm/yy', dateFieldVal);
             
             //var today = $.datepicker.formatDate('dd/mm/yy',new Date())
             var tempDate = dateVal;
             var today = new Date()
             //var tempDate = dateFieldVal;
             var isValid = true
             
             if(self.params.timeofday) {
                 if(self.params.timeofday === 'begin'){
                    var dt1 = $(fromfieldname);
                     tempDate = dt1.datepicker("getDate");  
                 } else if(self.params.timeofday === 'end'){
                    var dt2 = $(tofieldname);
                     tempDate = dt2.datepicker("getDate");  
                     
                 }
                 
             } else {
                 var dt = $('#'+self.params.fieldname)
                 tempDate = dt.datepicker("getDate");
                 
             }
             
             if(self.params.allowpastdate === 'N') {
               //  today.setMinutes(0)
                // today.setHours(0)
                // today.setSeconds(0)
                 today.setDate(today.getDate() - 1)
                 if(tempDate < today) {
                     isValid = false
                 }
             }
             if(self.params.allowfuturedate === 'N') {
                 if(tempDate > today) {
                     isValid = false
                 }
             }             
             

             if(self.params.timeofday && self.params.timeofday === 'end'){
                  var dt1 = $(fromfieldname);
                   var fromDate = dt1.datepicker("getDate");
                   var dt2 = $(tofieldname);
                   var toDate = dt2.datepicker("getDate");
                   if(fromDate && toDate && fromDate > toDate) {
                       isValid = false
                   }
            }             
             return isValid
         }catch(error) {
             return false
         }
         
     }
  },
  update: function (newval,oldval) {
    $(this.el).datepicker('setDate', BridgeDateUtils.formatForDisplay(newval,this.params.usage,this.params.timeofday))
  },
  unbind: function () {
    $(this.el).off().remove()
  }
});

/* 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/	
var Utils = require('./../utils/utils.js')
var Vue = require('vue')
Vue.filter('nodefilter', function (rows, DistributionRuled) {

    if (!rows || rows.length == 0) {
        return rows
    }
    
    var res = []
    var nodeList = function(row) {
		 
        if(row.DistributionRuleId === DistributionRuled) {
            return true
        } else {
            return false
        }
    }
    res = rows.filter(nodeList)

  return res
})


/* 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/	
	var BridgeUtils = new Object();
    var Logger = require('./loggerUtils.js')
    var $log = Logger.getInstance('Utils')

    /*const RESOURCE_LIST = {
        
    }*/
    
    var ERROR_MESSAGES = [{
        'ErrorMessageId': 'MAX_RECORDS',
        'ErrorMessageDescription': 'Please enter a positive number less than or equal to 500'
    },
    {
        'ErrorMessageId': 'POSITIVE_NUM',
        'ErrorMessageDescription': 'Please Enter a positive number'
    },
      {
        'ErrorMessageId': 'MAX_CHAR',
        'ErrorMessageDescription': 'Maximum of three characters allowed'
    },
      {
        'ErrorMessageId': 'INVALID_DATE',
        'ErrorMessageDescription': 'Enter a Valid Date'
    }                          
    ]
    
    const API_ERROR_MESSAGE = 'This could be due to incorrect configuration or improper input. Please fix the error and try again.'
    
    const REDIRECT_STATUS_LIST = [401, 403]
    
    const VARIOUS='VARIOUS'
    
    const DEFAULT_DATA_VALUE = 'NA';

    const DEFAULT_TIMEOUT = 10000;

    BridgeUtils.DashboarError = [];
    
    BridgeUtils.getErrorMessage = function(errorMessageId) {
        var errorMessageDesc = ""
        for(index=0;index < ERROR_MESSAGES.length;index++) {
            var errorMessage =  ERROR_MESSAGES[index]  
            if(errorMessage.ErrorMessageId === errorMessageId) {
                errorMessageDesc = errorMessage.ErrorMessageDescription
            }
        }
        return errorMessageDesc
    }
	
	BridgeUtils.getAPIErrorMessage = function() {
		return API_ERROR_MESSAGE;
	}
	
    BridgeUtils.getVariousString = function() {
		return VARIOUS;
	}
    
	BridgeUtils.getTimeOut = function() {
		return DEFAULT_TIMEOUT;
	}
    
    
	BridgeUtils.formatNumberForDisplay = function(serverNumber, usage) {
		var displayNumberFormat
        var displayQuantityFormat
        var displayIntFormat
        
        if(window.Intl && typeof window.Intl === "object") {
          displayNumberFormat=Intl.NumberFormat('en-AU', { minimumFractionDigits: 2, maximumFractionDigits: 3 })  
          displayQuantityFormat=  Intl.NumberFormat('en-AU', { minimumFractionDigits: 2, maximumFractionDigits: 2 })
          displayIntFormat =  Intl.NumberFormat('en-AU', { minimumFractionDigits: 0, maximumFractionDigits: 0 })
        } 
        if(!displayNumberFormat) {
            var newNum
            if(number) {
                if ("number" === usage) {
                    newNum = number.toFixed(2)
                } else if("int" == usage) {
                    newNum = number.toFixed(0)
                } else {
                    newNum = number.toFixed(2)
                }
            }
            return newNum
        }     
		var displayFormat
		if ("number" === usage) {
			displayFormat = displayNumberFormat
		} else if("int" == usage) {
			displayFormat = displayIntFormat
		} else {
			displayFormat = displayQuantityFormat
		}
		try {
			var number = Number(serverNumber)
			return displayFormat.format(number)
		} catch(error) {
			//console.log(error)
			return serverNumber
		}
	}
    
    BridgeUtils.formatDateForServer = function(date) {
	if (date) {
    	return date.toJSON();
	}
	return "";
	}

	BridgeUtils.formatNumberForServer = function(userNumber) {
		try {
			var cleanNumber = userNumber.replace(/,/g, '')
			return Number.parseFloat(cleanNumber) + ""
		} catch(error) {
			//console.log(error)
			return userNumber
		}
	}

	BridgeUtils.getNumber = function(userNumber) {
		if(!userNumber) {
			return
		}
		try {
			var cleanNumber = userNumber.replace(/,/g, '')
			return Number.parseFloat(cleanNumber)
		} catch(error) {
			//console.log(error)
			return userNumber
		}
	}

	BridgeUtils.formatForDisplay = function(serverDate,format) {
        
		if(serverDate) {
            var tmpServerDate = serverDate
            var dateVal = new Date(tmpServerDate);
            /*if(format) {
                if(format === "filter") {
                    dateVal.setMinutes(dateVal.getMinutes() - dateVal.getTimezoneOffset())
                }                
            }*/
            $.datepicker.setDefaults( $.datepicker.regional[ "en-AU" ] );
			return $.datepicker.formatDate('dd/mm/yy', dateVal);
		}
		return "";
	}
    
	BridgeUtils.formatDateTime = function(serverDate) {
        
		if(serverDate) {
			var fulldate = new Date(serverDate);
            $.datepicker.setDefaults( $.datepicker.regional[ "en-AU" ] );
            var strdate = $.datepicker.formatDate('dd/mm/yy', fulldate)
            var strhours= fulldate.getHours() < 10 ? '0'+fulldate.getHours():fulldate.getHours() 
            var strmins= fulldate.getMinutes() < 10 ? '0'+fulldate.getMinutes():fulldate.getMinutes() 
            var strtime= strhours +':'+strmins
			return strdate+' '+strtime;
		}
		return "";
	}    

	BridgeUtils.formatForServer = function(displayDate, format, timeofday) {
		if (displayDate) {
            $.datepicker.setDefaults( $.datepicker.regional[ "en-AU" ] );
            var date 
            try {
                date = $.datepicker.parseDate('dd/mm/yy', displayDate);    
            }catch(error) {
                //console.log('Eror in Date parsing');
                return "";
            }
			
			if (format === "filter") {
				// For filter we want to get the date as selected by user, ignoring the timezone
				//date.setHours(12)
                //date.setMinutes(date.getMinutes() - date.getTimezoneOffset())
				return date.toJSON().slice(0, 10);
                //return date.toJSON();    
			} else {
                if(timeofday) {
                    if(timeofday === "begin") {
                        date.setHours(0)

                    }
                    if(timeofday === "end") {
                        date.setHours(23,59,59)
                    }
                }
                //date.setMinutes(date.getMinutes() - date.getTimezoneOffset())
                
            }

            
			return date.toJSON();
		}
		return "";
	}
	
		BridgeUtils.executeDataServicePromise = function(mashupId, success, jsonData,errorHandler) {
            var timeout = function() {
                window.location.href = window.AppInfo.contextroot;
                return;
            }
      
            var apiError = function(jqXHR,textStatus,errorThrown) {
				if(REDIRECT_STATUS_LIST.indexOf(jqXHR.status) > -1) {
					window.location.href = window.AppInfo.contextroot+"/logout.jsp?SessionTimeOut=Y";
					return;	
				}
				var errorArrObj = {}
                try{
                   errorArrObj = JSON.parse(jqXHR.responseText)
                }catch(error) {
                    var errorData = jqXHR.responseText
                    if(errorData && errorData.charAt(0) === '<'){
                       window.location.href = window.AppInfo.contextroot;
                       return;
                    }
                    errorArrObj ={}
                    errorArrObj.errors = []
                    var newErrorObj = {'ErrorCode' : '', 'ErrorDescription': '', 'HttpCode': ''}
                    newErrorObj.ErrorCode = errorThrown
                    newErrorObj.HttpCode = jqXHR.status
                    errorArrObj.errors.push(newErrorObj)

                }
                
                var errorArr = []
                if(errorArrObj.errors) {
                    errorArr = errorArrObj.errors
                }
                var errorObj = {'ErrorCode' : '', 'ErrorDescription': '', 'HttpCode': ''}
                if(errorArr && errorArr.length > 0) {
                    errorObj.ErrorCode = errorArr[0].ErrorCode
                    errorObj.ErrorDescription = errorArr[0].ErrorDescription
                    errorObj.HttpCode = errorArr[0].httpcode
                }
                if(errorHandler) {
                    errorHandler(errorObj)
                }
                
                
            }
                                
            var successWrapper = function (data) {
                            //console.log("Output of call to mashup: %s", mashupId);
                        //console.log("inside executeDataServicePromise "+ data);
							
				if( data && data.constructor === String ){
                 //console.log("logging out executeDataService "+ data.charAt(0));
					if(data.charAt(0) === '<'){
						window.location.href = window.AppInfo.contextroot;
                        return;
					}
				}
			    success(data);
            }
            var url = window.AppInfo.contextroot + "/api/" + mashupId
            var ajaxInput = {url: url, method: 'POST', dataType: 'json'}

            if (jsonData) {
                            var inputData = {}
                            inputData[mashupId] = JSON.stringify(jsonData);
                            ajaxInput['data'] = inputData;
            }
            var promise= $.ajax(ajaxInput);
            promise.done(successWrapper);
            promise.fail(apiError);
             return promise;
      } 
	
	BridgeUtils.executeDataService = function(mashupId, success, jsonData,errorHandler) {
		var timeout = function() {
			window.location.href = window.AppInfo.contextroot;
            return;
            //console.log("Timeout Occurred");
		}

        
        var apiError = function(jqXHR,textStatus,errorThrown) {
            //JSON.stringify(data.errors)
            if(REDIRECT_STATUS_LIST.indexOf(jqXHR.status) > -1) {
                window.location.href = window.AppInfo.contextroot+"/logout.jsp?SessionTimeOut=Y";
				return;	
            }
                    
            var errorArrObj = {}
            try{
               errorArrObj = JSON.parse(jqXHR.responseText)
            }catch(error) {
                var errorData = jqXHR.responseText
                if(errorData && errorData.charAt(0) === '<'){
                   window.location.href = window.AppInfo.contextroot;
                    return;
                }
                errorArrObj ={}
                errorArrObj.errors = []
                var newErrorObj = {'ErrorCode' : '', 'ErrorDescription': '', 'HttpCode': ''}
                newErrorObj.ErrorCode = errorThrown
                newErrorObj.HttpCode = jqXHR.status
                errorArrObj.errors.push(newErrorObj)
                
            }
            var errorArr = []
            if(errorArrObj.errors) {
                errorArr = errorArrObj.errors
            }
            var errorObj = {'ErrorCode' : '', 'ErrorDescription': '', 'HttpCode': ''}
            if(errorArr && errorArr.length > 0) {
                errorObj.ErrorCode = errorArr[0].ErrorCode
                errorObj.ErrorDescription = errorArr[0].ErrorDescription
                errorObj.HttpCode = errorArr[0].httpcode
            }
            if(errorObj.HttpCode === 401) {
                timeout()
            }
            if(errorHandler) {
                errorHandler(errorObj)
            }
           
		}
        
		
		var successWrapper = function (data) {
            //TODO: Remove these console.log and change it to log4j
			//console.log("Output of call to mashup: %s", mashupId);
			//console.log(data);
          //  console.log("inside executeDataService "+ data);
            
            if( data && data.constructor === String ){
                 //console.log("logging out executeDataService "+ data.charAt(0));
					if(data.charAt(0) === '<'){
						window.location.href = window.AppInfo.contextroot;
                        return;
					}
            }			
			/*else {
				 console.log("No logging out.. Valid JSON");
			}*/
            var successTime = Date.now()
            $log.log("Success Callback "+mashupId +" Called At "+successTime);            
            success(data);
		}
        var startTime;
        var endTime;
        
        var beforeSendFn = function(jqXHR,settings) {
            startTime=Date.now();
            $log.log("Before Invoke "+mashupId +" Called At "+startTime);
        }
        var completeFn = function(jqXHR,textStatus) {
            endTime=Date.now();
            $log.log("After Complete.. "+mashupId+" Time Taken "+(endTime - startTime));
        }

        
		var url = window.AppInfo.contextroot + "/api/" + mashupId
		var ajaxInput = {url: url, beforeSend: beforeSendFn, success: successWrapper, error: apiError, method: 'POST', dataType: 'json', complete: completeFn}
		if (jsonData) {
			var inputData = {}
			inputData[mashupId] = JSON.stringify(jsonData);
			ajaxInput['data'] = inputData;
		}
		$.ajax(ajaxInput);
        /*var promise= $.ajax(ajaxInput);
        promise.done(successWrapper);
        promise.fail(apiError);
		return promise; */

	}
	 BridgeUtils.exportTitle = function(title) {
       if(title != undefined){
             var  dateObj = new Date();
      var month = dateObj.getUTCMonth() + 1; //months from 1-12
      var day = dateObj.getUTCDate();
      var year = dateObj.getUTCFullYear();
      var hour = dateObj.getHours();
      var min= dateObj.getMinutes();
 //var ReportTitle = 'Shipments Export'+'-'+ year+month+date+hour+min;
        var getTitle = title+'-'+year+month+day+hour+min;
        return getTitle;
    }  
     }
    BridgeUtils.convertJSONToCSV = function(data,columns,title,showLabel) {
        var CSV = '';
        //CSV += title + '\r\n\n';
        if(showLabel) {
            var columnTitles = "";
            for(var columnIndex = 0; columnIndex < columns.length; columnIndex++) {
               // columnTitles += getColumnName(columns[columnIndex])
                if(columns[columnIndex].Label) {
                    columnTitles += columns[columnIndex].Label
                } else {
                    columnTitles += this.getColumnName(columns[columnIndex].Binding)    
                }
                
                if(columnIndex < columns.length -1 ) {
                    columnTitles += ',' ;
                }
            }
            CSV += columnTitles + '\r\n';
        }
        
        for(var i=0; i < data.length; i++) {
            var row = "";
            for(var j = 0; j < columns.length; j++) {
                row += '"' + this.getDefaultValue(data[i],columns[j].Binding)+'"';
                if(j < columns.length -1) {
                    row += ',';
                }
            }
            CSV += row + '\r\n';
        }
        return CSV;
    }
    BridgeUtils.getColumnName = function(columnBinding) {
        var paths = columnBinding.split('.')
        if (paths.length > 1) {
            return paths[paths.length-1];
        } else {
            return columnBinding;
        }
    }
	BridgeUtils.getValue = function (model, binding) {
		var paths = binding.split('.')
		var displayValue;
        if(model) {
		if (paths.length > 1) {
			var currentObj = model
			var bindingAttr = paths.pop()
			paths.forEach( function(path) {
				if (currentObj[path] == undefined) {
					return // DEFAULT_DATA_VALUE;
				}
				currentObj = currentObj[path]
			})
			displayValue = currentObj[bindingAttr]
		} else {
			displayValue = model[binding]
		}
        }
/*        if ((displayValue == undefined) || (displayValue == '')) {
                displayValue = DEFAULT_DATA_VALUE;
         }
  */      
		return displayValue
	}
    
    BridgeUtils.getDefaultValue = function (model, binding) {
		
		var displayValue = this.getValue(model,binding);
		
        if ((displayValue == undefined) || (displayValue == '')) {
                displayValue = DEFAULT_DATA_VALUE;
         }
  
		return displayValue
	}


	BridgeUtils.setValue = function (model, binding, value) {
		var paths = binding.split('.')
		if (paths.length > 1) {
			var currentObj = model
			var bindingAttr = paths.pop()
			paths.forEach( function(path) {
				if (currentObj[path] == undefined) {
					return
				}
				currentObj = currentObj[path]
			})
			if(value) {
				currentObj[bindingAttr] = value
			} else {
				delete currentObj[bindingAttr]
			}
		} else {
			if(value) {
				model[binding] = value
			} else {
				delete model[binding]
			}
		}
	}
	
	
	BridgeUtils.executeSinglePageDataService = function(mashupId, jsonData,errorHandler) {
		var timeout = function() {
			$log.log('Timeout Error in SinglePage Data Service');
            window.location.href = window.AppInfo.contextroot;
            return;
            
		}

		var apiError = function(data,textStatus,errorThrown) {
            if(REDIRECT_STATUS_LIST.indexOf(data.status) > -1) {
                window.location.href = window.AppInfo.contextroot+"/logout.jsp?SessionTimeOut=Y";
				return;	
            }			
            var errorArrObj = {}
            try{
               errorArrObj = JSON.parse(data.responseText)
            }catch(error) {
                var errorData = data.responseText
                if(errorData && errorData.charAt(0) === '<'){
                   window.location.href = window.AppInfo.contextroot;
                   return;
                }                
                errorArrObj ={}
                errorArrObj.errors = []
                var newErrorObj = {'ErrorCode' : '', 'ErrorDescription': '', 'HttpCode': ''}
                newErrorObj.ErrorCode = errorThrown
                newErrorObj.HttpCode = data.status
                errorArrObj.errors.push(newErrorObj)
                
            }
            
//			var errorArrObj = JSON.parse(data.responseText)
            errorArrObj.MashId = mashupId;
            BridgeUtils.DashboarError.push(errorArrObj)
            if(errorHandler) {
                errorHandler(errorArrObj)
            }
            
		}
		
		var url = window.AppInfo.contextroot + "/api/" + mashupId
		var ajaxInput = {url: url, error: apiError, method: 'POST', dataType: 'json'}
        
		if (jsonData) {
			var inputData = {}
			inputData[mashupId] = JSON.stringify(jsonData);
			ajaxInput['data'] = inputData;
		}
		return $.ajax(ajaxInput);
	}

BridgeUtils.hasPermission = function(resourceId) {
    var isAllowed = true
    if(window.AppInfo.resources) {
        if(window.AppInfo.resources.indexOf(resourceId) == -1) {
            isAllowed = false
        }
    }
    return isAllowed
}
    
	module.exports = BridgeUtils



var Vue = require('vue')
Vue.config.debug = true
var VueRouter = require('vue-router')
Vue.use(VueRouter)

var NotFound = Vue.extend({
    template: '<p>This page is not found!</p>'
})
Vue.component('notfound',NotFound)


var Index = require('./components/Index.vue')
Vue.component('Index',Index)


var GridPagination = require('./components/GridPagination.vue')
Vue.component('gridpagination',GridPagination)

var AddressDetails = require('./components/AddressDetails.vue')
Vue.component('addressdetails',AddressDetails)

var Vendor = require('./components/VendorList.vue')
Vue.component('vendorlist',Vendor)

var VendorDetails = require('./components/VendorDetails.vue')
Vue.component('vendordetails',VendorDetails )

var OrderList = require('./components/OrderList.vue')
Vue.component('orderlist',OrderList )


var Carrier = require('./components/Carrier.vue')
Vue.component('carrier',Carrier )

var CarrierDetails = require('./components/CarrierDetails.vue')
Vue.component('carrierdetails',CarrierDetails )

var OrderDetails = require('./components/OrderDetails.vue')
Vue.component('orderdetails',OrderDetails )

var Alert = require('./components/AlertList.vue')
Vue.component('alertlist',Alert)

var AlertDetails = require('./components/AlertDetails.vue')
Vue.component('alertdetails',AlertDetails )

var VendorContact = require('./components/VendorContact.vue')
Vue.component('vendorcontact',VendorContact )

var VendorAddressDetails = require('./components/VendorAddressDetails.vue')
Vue.component('vendoraddressdetails',VendorAddressDetails)

var AssignAlertToUser = require('./components/AssignAlertToUser.vue')
Vue.component('assignalerttouser',AssignAlertToUser)

var VendorDeleteAdrContact = require('./components/DelContactConfirm.vue')
Vue.component('vendordeleteadrcontact',VendorDeleteAdrContact)

var OrderComments = require('./components/OrderComments.vue')
Vue.component('ordercomments',OrderComments)

var ItemComments = require('./components/ItemComments.vue')
Vue.component('itemcomments',ItemComments)

var ItemMovingAverage =  require ('./components/ItemNodeDefnList.vue')
Vue.component('itemmovingavg',ItemMovingAverage)

var OrderLineDetails = require('./components/OrderLineDetails.vue')
Vue.component('orderlinedetails', OrderLineDetails)

var OrderHoldDetails = require('./components/OrderHoldDetails.vue')
Vue.component('orderholddetails', OrderHoldDetails)

var Inventory=require('./components/InventoryList.vue')
Vue.component('inventoryList',Inventory )

var InventoryDetail=require('./components/InventoryDetails.vue')
Vue.component('inventorydetails',InventoryDetail )

var InventoryiTech=require('./components/InventoryiTechList.vue')
Vue.component('inventoryitechList',InventoryiTech )

var InventoryiTechDetail=require('./components/InventoryiTechDetails.vue')
Vue.component('inventoryitechdetails',InventoryiTechDetail )

var ItecAssetDetails = require('./components/ItecAssetDetails.vue')
Vue.component('itecassetdetails',ItecAssetDetails)

var SupplyDetail= require('./components/InventorySupplyDetails.vue')
Vue.component('supplydetail',SupplyDetail )

var FileUpload=require('./components/FileUploadUtil.vue')
Vue.component('fileuploads',FileUpload )

var carrierModal=require('./components/carrierModal.vue')
Vue.component('carriermodal',carrierModal )

var carrierModalEdit=require('./components/carrierModalEdit.vue')
Vue.component('carriermodaledit',carrierModalEdit )

var SPSFileUpload=require('./components/SPSFileUploadUtil.vue')
Vue.component('spsfileuploads',SPSFileUpload )


var StkMvmtUpload= require('./components/StockMvmtUpload.vue')
Vue.component('stockmvmtupload',StkMvmtUpload)

var UploadOthers= require('./components/OtherUploads.vue')
Vue.component ('otherupload',UploadOthers)

var ExceptionList= require('./components/ExceptionList.vue')
Vue.component ('exceptionlist',ExceptionList)

var ExceptionDetails= require('./components/ExceptionDetails.vue')
Vue.component ('exceptiondetails',ExceptionDetails)

var GoogleCharts= require ('./components/GoogleChartUtil.vue')
Vue.component ('googlechart',GoogleCharts)

var NodeSupplyDtl = require ('./components/NodeSupplyDetails.vue')
Vue.component ('nodesupdetails', NodeSupplyDtl )

var Shipments=require('./components/Shipments.vue')
Vue.component('shipments',Shipments )

var ShipmentDetails=require('./components/ShipmentDetails.vue')
Vue.component('shipmentdetails',ShipmentDetails)

var TrackConsignment=require('./components/Consignment.vue')
Vue.component('trackconsignment',TrackConsignment)

var SendVendorEmail = require('./components/SendVendorEmail.vue')
Vue.component('sendvendoremail',SendVendorEmail)

var ChartData= require ('./components/ChartDataUtil.vue')
Vue.component ('chartdata',ChartData)

var MessageBox = require('./components/MessageBox.vue')
Vue.component('messagebox',MessageBox)

var ErrorBox = require('./components/ErrorBox.vue')
Vue.component('errorbox',ErrorBox)

var SearchOrder = require('./components/SearchOrder.vue')
Vue.component('searchorder',SearchOrder )

var Schedule= require('./components/Schedule.vue')
Vue.component('schedule',Schedule)

var CreateOrder = require('./components/CreateOrder.vue')
Vue.component('createorder',CreateOrder )

var AdhocLogistic = require('./components/AdhocLogistic.vue')
Vue.component('adhoclogistic',AdhocLogistic )

var BookFreight=require('./components/BookFreight.vue')
Vue.component('bookfreight',BookFreight)

var AlertConfirmation=require('./components/AlertConfirmation.vue')
Vue.component('alertconfirmation',AlertConfirmation )

var AddressLocationNonEditable = require('./components/AddressLocation.vue')
Vue.component('noneditableaddress',AddressLocationNonEditable)

var EditOrder = require('./components/EditOrder.vue')
Vue.component('editorder',EditOrder)

var printConsignmentNote = require('./components/printConsignmentNote.vue')
Vue.component('printconsignmentnote',printConsignmentNote)

var printConsignmentNoteA5 = require('./components/printConsignmentNoteA5.vue')
Vue.component('printconsignmentnoteA5',printConsignmentNoteA5)		

var JobsList = require('./components/JobsList.vue')
Vue.component('jobslist',JobsList)

var notification = require('./components/Notification.vue')
Vue.component('notification',notification)

require('./directives/datepicker.js')
require('./filters/datefilter.js')
require('./filters/gridfilter.js')
require('./filters/numberfilter.js')
require('./filters/integerfilter.js')
require('./filters/quantityfilter.js')
require('./filters/gridsorter.js')
require('./filters/statusfilter.js')
require('./filters/shipnodefilter.js')
require('./filters/distGroupfilter.js')
require('./filters/spslocationfilter.js')


var App = Vue.extend({})
var router = new VueRouter()
var Logger = require('./utils/loggerUtils.js')
router.beforeEach(function (transition) {
	 /*if(transition.from.router == null){
        //First Page   Ajax Api Call to made
        var loginput=Logger.getloggedEntries()  
        var logger = ''
        Object.keys(loginput).forEach(function (key) {
          logger = logger + loginput[key] + '\n';
        });
        var input = {'LogSP':logger}
        $.ajax({
            url: window.AppInfo.contextroot+'/api/uilogger',
            type: 'POST',
            data: input,
            cache: false,
            dataType: 'text',
            success: function(data, textStatus, jqXHR)
            { },
            error: function(jqXHR, textStatus, errorThrown)
            { }
        });
         Logger.ClearloggedEntries()
    }else {
        var loginput=Logger.getloggedEntries()  
        var logger = ''
        Object.keys(loginput).forEach(function (key) {
            logger = logger + loginput[key] + '\n';
        });
        var input = {'Log':logger}
        //console.log(logger)
        $.ajax({
            url: window.AppInfo.contextroot+'/api/uilogger',
            type: 'POST',
            data: input,
            cache: false,
            dataType: 'text',
            success: function(data, textStatus, jqXHR)
            { },
            error: function(jqXHR, textStatus, errorThrown)
            { }
        });
         Logger.ClearloggedEntries()
    } */
        var loginput=Logger.getloggedEntries()  
        var logger = ''
        Object.keys(loginput).forEach(function (key) {
            logger = logger + loginput[key] + '\n';
        });
        var input = {'Log':logger}
        //Invoke only if there are log entries
        if(loginput && loginput.length > 0) {
            $.ajax({
            url: window.AppInfo.contextroot+'/api/uilogger',
            type: 'POST',
            data: input,
            cache: false,
            dataType: 'text',
            success: function(data, textStatus, jqXHR)
            { },
            error: function(jqXHR, textStatus, errorThrown)
            { }
        });

        }
         Logger.ClearloggedEntries()

    
    var Autoclear = transition.to.autoclear;
   if(!((Autoclear == undefined) || (Autoclear == null) || !(Autoclear))) {
       //window.Xhistory = [];
       sessionStorage.removeItem('breadcrumbContext');
   }
     var skipbreadcrumb = transition.from.skipbreadcrumb
    if(!((skipbreadcrumb == undefined) || (skipbreadcrumb == null) || !(skipbreadcrumb))) {
        //window.Xhistory = [];
        //sessionStorage.removeItem('vpbreadcrumbContext');
        if(sessionStorage.getItem("breadcrumbContext")) {
            var currentbcArr = JSON.parse(sessionStorage.getItem("breadcrumbContext"));
            //Remove the last element from the Breadcrumb array
            currentbcArr.splice(-1,1);
            var spbcContext = JSON.stringify(currentbcArr)
            sessionStorage.setItem("breadcrumbContext",spbcContext);
            
        }
        
    }
    transition.next()
});

router.map({

	'/home': {
    	component: Index,
		Description: 'Dashboard',
        id:'home',
        autoclear: true
    },
    '/alertlist': {
    	component:Alert,
        name:'alertlist',
		Description: 'Alerts',
        id:'alertlist'
    },
    
    '/assetslist': {
        component:AssetsList,
        name:'assetslist',
        Description: 'AssetsList',
        id:'assetslist',
        autoclear: true
    },
     '/assetdetails': {
        component: AssetDetails,
        name:'AssetDetails',
        Description: 'AssetDetails',
        id:'assetdetails'
    },
        '/carrier': {
        component:Carrier,
        name:'Carrier',
        Description: 'Carrier',
        id:'carrier',
        autoclear: true
    },
        '/carrierdetails': {
        component:CarrierDetails,
        name:'CarrierDetails',
        Description: 'CarrierDetails',
        id:'carrierdetails'
    },
    '/alertdetails': {
    	component:AlertDetails,
		Description: 'Alert Details',
        id:'alertdtl'
    },
    '/vendorlist': {
    	component:Vendor,
		Description: 'Vendors',
        id:'vendorlist',
        autoclear: true
    },
    '/vendordetails': {
    	component:VendorDetails,
		Description: 'Vendor Details',
        id:'vendordtl'        
    },
    '/inventoryList':{
		component:Inventory,
		Description: 'Inventory',
        id:'inventorylist',
        autoclear: true
    },
    '/inventorydetails':{
	   component:InventoryDetail,
       Description: 'Inventory Details',
       id:'inventorydtl',    
       name:'invdtl'	
	},	
    '/inventoryitechList':{
		component:InventoryiTech,
		Description: 'iTec Inventory',
        id:'inventoryitechlist',
        autoclear: true
    },
    '/inventoryitechdetails':{
	   component:InventoryiTechDetail,
       Description: 'iTec Inventory Details',
       id:'inventoryitechdtl',    
       name:'invitechdtl'	
	},	
   '/supplydetail':{
	  component:SupplyDetail,     
	},
      '/googlechart': {
        component:GoogleCharts,        
	},
       '/nodesupdetails':{
        component: NodeSupplyDtl,         
	},	
	
	'/chartdata': {
        component:ChartData,        
	},
    
    '/fileuploads':{
	component:FileUpload
	},
    '/carriermodal':{
    component:carrierModal
    },
       '/carriermodaledit':{
    component:carrierModalEdit
    },

	'/spsfileuploads':{
	component:SPSFileUpload
	},
	
	'/stockmvmtupload':{
	 component:StkMvmtUpload	
	},
	
	 '/otherupload': {
		component:UploadOthers  
    },
    '/orderlist': {
         name: 'OrderList',
    	component:OrderList,
		Description: 'Orders',
        id:'orderlist',
        autoclear: true
    },
     '/orderdetails': {
         name: 'orderdtl',
    	component:OrderDetails,
		Description: 'Order Details',
        id:'orderdtl'
    },

	'/exceptionlist': {
        component:ExceptionList,
        Description: 'Exceptions',
            id:'exceptionlist',
        autoclear: true
    },
    '/exceptiondetails/:ErrorTxnId': {
         component:ExceptionDetails,
        Description: 'Exception Details',
        id:'exceptiondtl',
        name:'expdtl'
    },
     '/shipments':{
		component:Shipments,
		Description: 'Shipments',
        id:'shipments',
        autoclear: true
	},
    '/shipmentdetails':{
		component:ShipmentDetails,
		Description: 'Shipment Details',
        id:'ShipmentDetails',
		name:'shpdtl'
     },
	 '/trackconsignment':{
		component:TrackConsignment,
		Description: 'Consignment',
        id:'consignment',
        name:'trackconsignment'
     },
       '/createorder': {
         name: 'createorder',
        component:CreateOrder,
        Description: 'Create Order',
        id:'CreateOrder',
        skipbreadcrumb: true
    },
    '/adhoclogistic': {
         name: 'adhoclogistic',
        component:AdhocLogistic,
        Description: 'Adhoc Logistic',
        id:'AdhocLogistic',
        skipbreadcrumb: true 
    },
	'/bookfreight':{
        name: 'bookfreight',
		component:BookFreight,
		Description: 'Book Freight',
        id:'bookfreight',
        skipbreadcrumb: true
     },
      '/editorder':{
        name: 'editorder',
		component:EditOrder,
		Description: 'Edit Order',
        id:'editorder',
        skipbreadcrumb: true
      },
	  '/printconsignmentnote':{
        name: ' printconsignmentnote',
        component:printConsignmentNote,
        Description: 'Print Consignment',
        id:'printconsignmentnote',
        skipbreadcrumb: true
      },
        '/printConsignmentNoteA5':{
        name: ' printconsignmentnoteA5',
        component:printConsignmentNoteA5,
        Description: 'Print ConsignmentA5',
        id:'printconsignmentnoteA5',
        skipbreadcrumb: true
      },
     '/jobslist':{
        name: 'jobslist',
        component:JobsList,
        Description: 'Job Requests',
        id:'jobslist',
        skipbreadcrumb: true,
        autoclear: true
      },
       '/*any': {
    	component: NotFound
    }
})

Vue.directive('breadcrumb',{
    bind: function(){
        var HomePath = '/home';
       // debugger;
        var windowSession = [];

       /*if((window.Xhistory == undefined) || (window.Xhistory == null))
              window.Xhistory = [];*/
        
        if(!(sessionStorage.breadcrumbContext == undefined))
            windowSession = JSON.parse(sessionStorage.breadcrumbContext);
        
        this.el.innerHTML = '<ol class="breadcrumb cusbreadcrumb">'+generateTemplate('',windowSession, router._currentRoute, HomePath)+'</ol>';
        
    },
    method: [
        generateTemplate = function(template ,historyNav, CurrentRoute, HomePath){
            
            maintainCapacity(historyNav, CurrentRoute);
            var historyArraylen = historyNav.length;
            var Dashboardhandler =  router._recognizer.recognize(HomePath)[0].handler;
            template = '<li><a href="#!'+HomePath+'">'+Dashboardhandler.Description+'</a></li>';
            for (var history in historyNav) {
                var handlerObj = historyNav[history];
                template = template + manageTemplate(handlerObj, history, historyArraylen);
            }

            return template;
            
        },
        maintainCapacity = function(historyNav, CurrentRoute){
            
            clearHistoryObject(CurrentRoute, historyNav);
            
            if(historyNav.length > 3){
                historyNav.splice(0,1);
                maintainCapacity(historyNav,CurrentRoute);
            }
                
        },
        manageTemplate = function(history, currentIndex, totalIndex){
            
           return genlistTemplate(paramMatchObject(history,[]), history, currentIndex, totalIndex);
            
        },
        
        getParamValue = function(queryParam, key){
            
            var key_values = queryParam.split('&');
            var key_str = key+'=';
            var paramvalue = '';
            for (var key_value in key_values) {
                var keyValueStr = key_values[key_value];
                if((keyValueStr.indexOf(key_str))==0){
                    paramvalue = keyValueStr.split(key_str)[1];
                }
                    
            }
            return paramvalue;
            
        },
        
        paramMatchObject = function(currentHandlerObj, exceptional_list){
            
            var Key_Array = Object.keys(currentHandlerObj.query);
            var queryParam = ''; 
            if(Key_Array.length > 1){
               
               for (i = 0; i < Key_Array.length; i++) { 
                   var key = Key_Array[i];
                  if((key != 'hubblecsrf') && (exceptional_list.indexOf(key) == -1)){
                     var param = currentHandlerObj.query[key];
                      if(queryParam != '')
                          queryParam = queryParam + '&';
                     queryParam = queryParam + key + '=' + param;
                  }
                }
                
            }
            
            return queryParam;
        },
        
        clearHistoryObject = function(currentRouter, routerArraylist){
            //debugger;
            var match = false;
            var clearCmd = getParamValue(paramMatchObject(currentRouter,[]),'autoclear');
            var matchedindex;
            for (var router in routerArraylist) {
                var routerObj = routerArraylist[router];
                if(currentRouter.id == routerObj.id){ 
                    match = true;
                    matchedindex = router;
                }
            }
            
            if(clearCmd){
               routerArraylist.length = 0; 
               routerArraylist.push(currentRouter);
                
            }else if(match){
                var len =routerArraylist.length;
                routerArraylist.splice(parseInt(matchedindex)+1, parseInt(len));
            }
            else
                routerArraylist.push(currentRouter);
            
            sessionStorage.breadcrumbContext = JSON.stringify(routerArraylist);
           
        },
        
        genlistTemplate = function(queryParam, handler, currentIndex, totalIndex){
            
            var path = handler.path;
            var massagedpath = '/'+path.split('?')[0].split('/')[1];
            var template = '';
            var description = '';
            var aliasEnabled = handler.AliasKey;
            //debugger;
            if(!((aliasEnabled == undefined) || aliasEnabled == null || aliasEnabled == ''))
                description = getParamValue(paramMatchObject(handler,[]), aliasEnabled);
            else
                description = handler.Description;
            
            if(currentIndex == (totalIndex-1))
                template = template + '<li>'+description+'</li>';
            else{
            
                if(queryParam != '')
                   template = template + '<li><a href="#!'+massagedpath+'?'+queryParam+'">'+description+'</a></li>';
                else 
                   template = template + '<li><a href="#!'+massagedpath+'">'+description+'</a></li>';
                
            }
            
            return template;
        },
      getParentComponent = function (template, path, CallerPath){
             var handler =  router._recognizer.recognize(path)[0].handler;
             if((handler.parent == undefined) || (handler.parent == null))
                 template = template + UpdateTemplate(CallerPath, handler);
             else{
                 template = getParentComponent(template, handler.parent, CallerPath);
                 template = template + UpdateTemplate(CallerPath, handler);
             }
           
            return template;
         },
        UpdateTemplate = function(CallerPath, handler){
          
            var template;
            
            if(handler.path == CallerPath){
                template = '<li>'+handler.Description+'</li>';
            }else
                template = '<li><a href="#!'+handler.path+'">'+handler.Description+'</a></li>';
            
            return template;
        },
        
    ]
    
    
});

$.ajaxPrefilter(function (options, originalOptions, jqXHR) {
  jqXHR.setRequestHeader('hubblecsrfheader', window.AppInfo.hubblecsrftoken);
});

router.start(App, '#app')

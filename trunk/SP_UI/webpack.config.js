var webpack = require("webpack");
var path = require('path');
module.exports = {
  // entry point of our application
  entry: {
     vendor: ['jquery', 'jquery-ui','jquery-ui/themes/smoothness/jquery-ui.min.css', 'vue', 'chart.js','vue-router', './vendor.js'],     
    app: './app.js'
  },
  // where to place the compiled bundle
  output: {
    path: __dirname + '/hubble-sp/dist/',
    publicPath: '/hubble-sp/dist/',
    filename: '[name].js'
  },
  devServer: {
    proxy: {
      '/scv/*': {
        target: 'http://localhost:8085/',
        secure: false,
        headers: {
          'hubblewebpack': 'yes'
        },
        bypass: function(req, res, proxyOptions) {
            var url = req.url;
            if ((url.indexOf('.jsp') !== -1) || (url.indexOf('/api/') !== -1) || (url.indexOf('/filedownloadservlet') !== -1 ) || (url.indexOf('/uploadjobservlet') !== -1 ) ||(url.indexOf('/exportjobservlet') !== -1 ) || (url.indexOf('/login') !== -1)) {
              return false;
            } else {
              var bypassUrl = url.replace(/^\/scv/, '');
              return bypassUrl;
            }
          }
      }
    }
  },
  module: {
    // `loaders` is an array of loaders to use.
    loaders: [
      {
        test: /\.vue$/, // a regex for matching all files that end in `.vue`
        loader: 'vue'   // loader to use for matched files
      },
      { 
        test: /\.css$/, 
        loader: 'style-loader!css-loader' 
      },
      { 
        test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, 
        loader: "file" 
      },
      { 
        test: /\.(woff|woff2)$/, 
        loader:"url?prefix=font/&limit=3000" 
      },
      { 
        test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, 
        loader: "url?limit=3000&mimetype=application/octet-stream" 
      },
      { 
        test: /\.png$/, 
        loader: "url?limit=3000&mimetype=image/png" 
      },
      { 
        test: /\.gif$/, 
        loader: "url?limit=3000&mimetype=image/gif" 
      },
      { 
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, 
        loader: "url?limit=3000&mimetype=image/svg+xml" 
      },
      {
        test: /\.scss$/,
        loaders: ["style", "css", "sass?sourceMap"]
      }
    ]
  },
  plugins: [
    new webpack.ProvidePlugin({
      "$":"jquery",
      "jQuery":"jquery",
      "window.jQuery":"jquery"
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: ["vendor"],
      minChunks: Infinity
      // (with more entries, this ensures that no other module
      //  goes into the vendor chunk)
    })
  ],
  resolve: {
    alias: {
        "jquery": "jquery/dist/jquery.js",
        "jquery-ui-js": "jquery-ui/jquery-ui.js",  
        "jquery-ui-css": "jquery-ui/themes/smoothness/",
    }
  }
}

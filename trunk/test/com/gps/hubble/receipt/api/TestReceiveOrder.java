package com.gps.hubble.receipt.api;

import org.apache.xerces.util.SynchronizedSymbolTable;
import org.junit.Assert;
import org.junit.Test;

import com.gps.hubble.TestBase;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfs.japi.YFSException;

public class TestReceiveOrder extends TestBase {

	static int i=1;
	
	@Test
	public void testReceiveOrder1(){

		try {
			
			//System.out.println("useractivity = "+invokeEntityApi("listUserActivity", YFCDocument.getDocumentFor("<User UserId='admin'/>"), null));
			/*
				Scenario:-
					PO - 1 line - 10 qty Shipment created but not Shippped
				Expectations
					Error with error code = TEL_ERR_1181_001 should be thrown
			 */
			
			//setting Test Case Related Data
			String sEnterpriseCode = "TELSTRA_SCLSA";		
			String sSellerOrganizationCode = "Vendor1";
			String sBuyerOrganizationCode = "TELSTRA";
			String sEntryType = "INTEGRAL_PLUS";
			String sOrderType = "PURCHASE_ORDER";
			String sInterfaceNo = "INT_ODR_1";
			String sOrderName = "TestCaseOrder"+i++;
			String sBillToID = "AARON";
			String sShipToID = "000021844";
			String sExternalCustomerID = "TestOrg5";
			String sNodeType = "LC";
			
			//get Default Order XML with 1 lines
			YFCDocument input = getMultiLineOrder(2, sEnterpriseCode);

			input.getDocumentElement().setAttribute("EnterpriseCode", sEnterpriseCode);
			input.getDocumentElement().setAttribute("OrderName", sOrderName);
			input.getDocumentElement().setAttribute("OrderHeaderKey", sOrderName);
			input.getDocumentElement().setAttribute("EntryType", sEntryType);
			input.getDocumentElement().setAttribute("OrderType", sOrderType);
			input.getDocumentElement().setAttribute("SellerOrganizationCode", sSellerOrganizationCode);
			input.getDocumentElement().setAttribute("BuyerOrganizationCode", sBuyerOrganizationCode);
			input.getDocumentElement().setAttribute("InterfaceNo", sInterfaceNo);
			input.getDocumentElement().setAttribute("BillToID", sBillToID);
			input.getDocumentElement().setAttribute("ShipToID", sBillToID);

			//create Customer which is used in Input XML
			createCustomer(sBillToID, sShipToID, sExternalCustomerID);

			//Setting all the Order Validations OFF
			manageCommonCodeForOrderValidation("COST_CENTER","N");
			manageCommonCodeForOrderValidation("ITEM_ROUND","N");
			manageCommonCodeForOrderValidation("NEW_DAC","N");
			manageCommonCodeForOrderValidation("ZIP_CODE","N");

			//setting NodeType as per test case
			setNodeType(sExternalCustomerID,sNodeType);
			
			//call ManageOrder Service to Test
			invokeYantraService("GpsManageOrder", input);
			System.out.println("<--------- Order Create = "+sOrderName+" --------->");

			//get details of newly created Order
			YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>");
			YFCDocument output = invokeYantraApi("getOrderList", getOrderListInput,orderListTemplate());

			createASN(output);
			
			YFCDocument inputXML = getReceiveOrderXML(output);
			
			//receive order
			invokeYantraService("GpsReceiveOrder", inputXML);
			
			
			System.out.println("******************* Receive Order Test Case 1 *******************");

		}catch(YFSException ex) {
			System.out.println(ex.getErrorCode());
		}
		catch(Exception e){
			//e.printStackTrace();
			System.out.println("inside Exception");
		      if(e.toString().contains("TEL_ERR_1181_001")){
		        Assert.assertEquals("Pass", "Pass");
		      }else{
		        Assert.assertEquals("Pass", "Fail");
		      }
		    }
	}
	
	@Test
	public void testReceiveOrder2(){

		try {
			/*
				Scenario:-
					PO - 1 line - 10 qty - Shipment is in delivered status.
				Expectations
					Order line should be Received.
			 */
			
			//setting Test Case Related Data
			String sEnterpriseCode = "TELSTRA_SCLSA";		
			String sSellerOrganizationCode = "Vendor1";
			String sBuyerOrganizationCode = "TELSTRA";
			String sEntryType = "INTEGRAL_PLUS";
			String sOrderType = "PURCHASE_ORDER";
			String sInterfaceNo = "INT_ODR_1";
			String sOrderName = "TestCaseOrder"+i++;
			String sBillToID = "AARON";
			String sShipToID = "000021844";
			String sExternalCustomerID = "TestOrg5";
			String sNodeType = "LC";
			
			//get Default Order XML with 1 lines
			YFCDocument input = getMultiLineOrder(1, sEnterpriseCode);

			input.getDocumentElement().setAttribute("EnterpriseCode", sEnterpriseCode);
			input.getDocumentElement().setAttribute("OrderName", sOrderName);
			input.getDocumentElement().setAttribute("OrderHeaderKey", sOrderName);
			input.getDocumentElement().setAttribute("EntryType", sEntryType);
			input.getDocumentElement().setAttribute("OrderType", sOrderType);
			input.getDocumentElement().setAttribute("SellerOrganizationCode", sSellerOrganizationCode);
			input.getDocumentElement().setAttribute("BuyerOrganizationCode", sBuyerOrganizationCode);
			input.getDocumentElement().setAttribute("InterfaceNo", sInterfaceNo);
			input.getDocumentElement().setAttribute("BillToID", sBillToID);
			input.getDocumentElement().setAttribute("ShipToID", sBillToID);

			//create Customer which is used in Input XML
			createCustomer(sBillToID, sShipToID, sExternalCustomerID);

			//Setting all the Order Validations OFF
			manageCommonCodeForOrderValidation("COST_CENTER","N");
			manageCommonCodeForOrderValidation("ITEM_ROUND","N");
			manageCommonCodeForOrderValidation("NEW_DAC","N");
			manageCommonCodeForOrderValidation("ZIP_CODE","N");

			//setting NodeType as per test case
			setNodeType(sExternalCustomerID,sNodeType);
			
			
			//call ManageOrder Service to Test
			invokeYantraService("GpsManageOrder", input);
			System.out.println("<--------- Order Create = "+sOrderName+" --------->");

			//get details of newly created Order
			YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>");
			YFCDocument output = invokeYantraApi("getOrderList", getOrderListInput,orderListTemplate());

			YFCDocument docShipment = createAndConfirmASN(output);
			
			//change SO Shipment to InTransit status
			invokeYantraApi("changeShipmentStatus", YFCDocument.getDocumentFor("<Shipment BaseDropStatus='1400.10000' ShipmentKey='"+docShipment.getDocumentElement().getAttribute("ShipmentKey")+"' TransactionId='GPS_IN_TRANSIT.0005.ex'/>"));
				
			//deliver shipment
			invokeYantraApi("deliverShipment", docShipment);
			
			YFCDocument inputXML = getReceiveOrderXML(output);
			
			//receive order
			invokeYantraService("GpsReceiveOrder", inputXML);
			
			//get details of newly created Order
			getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>");
			output = invokeYantraApi("getOrderList", getOrderListInput,orderListTemplate());

			YFCElement eleOutputOrder = output.getElementsByTagName("Order").item(0); 

			Assert.assertEquals("Order Line Status Check = ",eleOutputOrder.getElementsByTagName("OrderLine").item(0).getAttribute("Status"),"Received");
			
			System.out.println("******************* Receive Order Test Case 2 *******************");

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}

	}
		
	@Test
	public void testReceiveOrder3(){

		try {
			/*
				Scenario:-
					PO - 1 line - 10 qty - create 2 shipment of 3 and 7 both delivered, receipt message is for 7 qyt and then 6 qty.
				Expectations
					Shipment having 7 qty should completely received and shipment with 3 qty should over received by 6 qty
			 */
			
			//setting Test Case Related Data
			String sEnterpriseCode = "TELSTRA_SCLSA";		
			String sSellerOrganizationCode = "Vendor1";
			String sBuyerOrganizationCode = "TELSTRA";
			String sEntryType = "INTEGRAL_PLUS";
			String sOrderType = "PURCHASE_ORDER";
			String sInterfaceNo = "INT_ODR_1";
			String sOrderName = "TestCaseOrder"+i++;
			String sBillToID = "AARON";
			String sShipToID = "000021844";
			String sExternalCustomerID = "TestOrg5";
			String sNodeType = "LC";
			
			//get Default Order XML with 1 lines
			YFCDocument input = getMultiLineOrder(1, sEnterpriseCode);

			input.getDocumentElement().setAttribute("EnterpriseCode", sEnterpriseCode);
			input.getDocumentElement().setAttribute("OrderName", sOrderName);
			input.getDocumentElement().setAttribute("OrderHeaderKey", sOrderName);
			input.getDocumentElement().setAttribute("EntryType", sEntryType);
			input.getDocumentElement().setAttribute("OrderType", sOrderType);
			input.getDocumentElement().setAttribute("SellerOrganizationCode", sSellerOrganizationCode);
			input.getDocumentElement().setAttribute("BuyerOrganizationCode", sBuyerOrganizationCode);
			input.getDocumentElement().setAttribute("InterfaceNo", sInterfaceNo);
			input.getDocumentElement().setAttribute("BillToID", sBillToID);
			input.getDocumentElement().setAttribute("ShipToID", sBillToID);
			input.getElementsByTagName("OrderLine").item(0).setAttribute("OrderedQty", "10");

			//create Customer which is used in Input XML
			createCustomer(sBillToID, sShipToID, sExternalCustomerID);

			//Setting all the Order Validations OFF
			manageCommonCodeForOrderValidation("COST_CENTER","N");
			manageCommonCodeForOrderValidation("ITEM_ROUND","N");
			manageCommonCodeForOrderValidation("NEW_DAC","N");
			manageCommonCodeForOrderValidation("ZIP_CODE","N");

			//setting NodeType as per test case
			setNodeType(sExternalCustomerID,sNodeType);
			
			
			//call ManageOrder Service to Test
			invokeYantraService("GpsManageOrder", input);
			System.out.println("<--------- Order Create = "+sOrderName+" --------->");

			//get details of newly created Order
			YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>");
			YFCDocument output = invokeYantraApi("getOrderList", getOrderListInput,orderListTemplate());

			YFCDocument docShipment1 = createAndConfirmASNWithQuantity(output,"3");
			YFCDocument docShipment2 = createAndConfirmASNWithQuantity(output,"7");
			
			//change SO Shipment to InTransit status
			invokeYantraApi("changeShipmentStatus", YFCDocument.getDocumentFor("<Shipment BaseDropStatus='1400.10000' ShipmentKey='"+docShipment1.getDocumentElement().getAttribute("ShipmentKey")+"' TransactionId='GPS_IN_TRANSIT.0005.ex'/>"));
			invokeYantraApi("changeShipmentStatus", YFCDocument.getDocumentFor("<Shipment BaseDropStatus='1400.10000' ShipmentKey='"+docShipment2.getDocumentElement().getAttribute("ShipmentKey")+"' TransactionId='GPS_IN_TRANSIT.0005.ex'/>"));	
			
			//deliver shipment
			invokeYantraApi("deliverShipment", docShipment1);
			invokeYantraApi("deliverShipment", docShipment2);
			
			YFCDocument inputXML1 = getReceiveOrderXMLWithQuantity(output,"7");
			YFCDocument inputXML2 = getReceiveOrderXMLWithQuantity(output,"6");
			
			//receive order
			invokeYantraService("GpsReceiveOrder", inputXML1);
			invokeYantraService("GpsReceiveOrder", inputXML2);
			
			
			//System.out.println(invokeYantraApi("getShipmentList", YFCDocument.getDocumentFor("<Shipment><ShipmentLines><ShipmentLine OrderHeaderKey='"+sOrderName+"'/></ShipmentLines></Shipment>") ,shipmentListTemplate()));
			YFCDocument receiptList = invokeYantraApi("getReceiptList", YFCDocument.getDocumentFor("<Receipt><ReceiptLines><ReceiptLine  OrderHeaderKey='"+sOrderName+"'/></ReceiptLines></Receipt>"),YFCDocument.getDocumentFor("<ReceiptList><Receipt ReceiptNo=''><ReceiptLines><ReceiptLine/></ReceiptLines></Receipt></ReceiptList>"));
			
			double dTotalReceiptQuantity = receiptList.getElementsByTagName("ReceiptLine").item(0).getDoubleAttribute("Quantity") + 
					receiptList.getElementsByTagName("ReceiptLine").item(1).getDoubleAttribute("Quantity");
			
			
			Assert.assertEquals("Total Received Quantity check = ",String.valueOf(dTotalReceiptQuantity), "13.0");
			
			System.out.println("******************* Receive Order Test Case 3 *******************");

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}

	}
	
	protected YFCDocument getReceiveOrderXML(YFCDocument docGetOrderList) {
		
		YFCElement eleOrder = docGetOrderList.getElementsByTagName("Order").item(0);
		
		YFCDocument docOutput = YFCDocument.getDocumentFor("<ReceiptList><Receipt ReceivingNode='"
		+eleOrder.getElementsByTagName("OrderLine").item(0).getAttribute("ReceivingNode")+"' OrderName='"+eleOrder.getAttribute("OrderName")+"'>"
				+ "<ReceiptLines></ReceiptLines></Receipt></ReceiptList>");
		
		for(YFCElement eleOrderLine : eleOrder.getElementsByTagName("OrderLine")) {
			YFCElement eleReceiptLine = docOutput.createElement("ReceiptLine");
	
			eleReceiptLine.setAttribute("PrimeLineNo", eleOrderLine.getAttribute("PrimeLineNo"));
			eleReceiptLine.setAttribute("SubLineNo", eleOrderLine.getAttribute("SubLineNo"));
			eleReceiptLine.setAttribute("Quantity", eleOrderLine.getAttribute("OrderedQty"));
			eleReceiptLine.setAttribute("ItemID", eleOrderLine.getElementsByTagName("Item").item(0).getAttribute("ItemID"));
			eleReceiptLine.setAttribute("UnitOfMeasure", eleOrderLine.getElementsByTagName("Item").item(0).getAttribute("UnitOfMeasure"));
			docOutput.getElementsByTagName("ReceiptLines").item(0).appendChild(eleReceiptLine);
		}
		
		return docOutput;
				
	}
	
	protected YFCDocument getReceiveOrderXMLWithQuantity(YFCDocument docGetOrderList, String sOrderedQty) {
		
		YFCElement eleOrder = docGetOrderList.getElementsByTagName("Order").item(0);
		
		YFCDocument docOutput = YFCDocument.getDocumentFor("<ReceiptList><Receipt ReceivingNode='"
		+eleOrder.getElementsByTagName("OrderLine").item(0).getAttribute("ReceivingNode")+"' OrderName='"+eleOrder.getAttribute("OrderName")+"'>"
				+ "<ReceiptLines></ReceiptLines></Receipt></ReceiptList>");
		
		for(YFCElement eleOrderLine : eleOrder.getElementsByTagName("OrderLine")) {
			YFCElement eleReceiptLine = docOutput.createElement("ReceiptLine");
	
			eleReceiptLine.setAttribute("PrimeLineNo", eleOrderLine.getAttribute("PrimeLineNo"));
			eleReceiptLine.setAttribute("SubLineNo", eleOrderLine.getAttribute("SubLineNo"));
			eleReceiptLine.setAttribute("Quantity", sOrderedQty);
			eleReceiptLine.setAttribute("ItemID", eleOrderLine.getElementsByTagName("Item").item(0).getAttribute("ItemID"));
			eleReceiptLine.setAttribute("UnitOfMeasure", eleOrderLine.getElementsByTagName("Item").item(0).getAttribute("UnitOfMeasure"));
			docOutput.getElementsByTagName("ReceiptLines").item(0).appendChild(eleReceiptLine);
		}
		
		return docOutput;
				
	}
	
	protected void createItem(String sItemId, String sUOM, String sOrganizationCode) {

		String ItemXml = "<ItemList><Item CanUseAsServiceTool='N' GlobalItemID='' ItemGroupCode='PROD' ItemID='' OrganizationCode='' "
				+ " UnitOfMeasure=''><PrimaryInformation AllowGiftWrap='N' AssumeInfiniteInventory='N' BundleFulfillmentMode='01' "
				+ "BundlePricingStrategy='PARENT' CapacityQuantityStrategy='' CostCurrency='AUD' CountryOfOrigin='' CreditWOReceipt='N'"
				+ " DefaultProductClass='' Description='' ExtendedDescription='' ExtendedDisplayDescription='Test Item' "
				+ "FixedCapacityQtyPerLine='0.00' FixedPricingQtyPerLine='0.00' InvoiceBasedOnActuals='N' IsAirShippingAllowed='Y' "
				+ "IsDeliveryAllowed='N' IsEligibleForShippingDiscount='Y' IsFreezerRequired='N' IsHazmat='N' IsParcelShippingAllowed='Y'"
				+ " IsPickupAllowed='N' IsProcurementAllowed='Y' IsReturnService='N' IsReturnable='Y' IsShippingAllowed='Y' "
				+ "IsStandaloneService='' IsSubOnOrderAllowed='' ItemType='' KitCode='' ManufacturerItem='' ManufacturerItemDesc='' "
				+ "ManufacturerName='' MasterCatalogID='' MaxOrderQuantity='0.00' MinOrderQuantity='0.00' MinimumCapacityQuantity='0.00' "
				+ "NumSecondarySerials='0' OrderingQuantityStrategy='ENT' PricingQuantityConvFactor='0.00' PricingQuantityStrategy='IQTY'"
				+ " PricingUOM='EACH' PricingUOMStrategy='INV' PrimaryEnterpriseCode='' PrimarySupplier='' ProductLine='' "
				+ "RequiresProdAssociation='N' ReturnWindow='0' RunQuantity='0.00' SerializedFlag='N' ServiceTypeID='' "
				+ "ShortDescription='testitem1' Status='3000' TaxableFlag='N' UnitCost='0.00' UnitHeight='0.00' UnitHeightUOM='' "
				+ "UnitLength='0.00' UnitLengthUOM='' UnitWeight='0.00' UnitWeightUOM='' UnitWidth='0.00' UnitWidthUOM=''/>"
				+ "		<InventoryParameters ATPRule='' AdvanceNotificationTime='0' DefaultExpirationDays='0' InventoryMonitorRule='' "
				+ "IsFifoTracked='N' IsItemBasedAllocationAllowed='Y' IsSerialTracked='N' LeadTime='0' MaximumNotificationTime='0.00'"
				+ " MinNotificationTime='0' NodeLevelInventoryMonitorRule='' ProcessingTime='0' TagControlFlag='N' TimeSensitive='N' "
				+ "UseUnplannedInventory='N'/><ClassificationCodes CommodityCode='' ECCNNo='' HarmonizedCode='' HazmatClass='' "
				+ "NAICSCode='' NMFCClass='' NMFCCode='' OperationalConfigurationComplete='N' PickingType='' PostingClassification='' "
				+ "Schedule_B_Code='' StorageType='' TaxProductCode='' UNSPSC='' VelocityCode=''/></Item></ItemList>";

		YFCDocument docManageItemInput = YFCDocument.getDocumentFor(ItemXml);
		YFCElement eleItem = docManageItemInput.getDocumentElement().getElementsByTagName("Item").item(0);

		eleItem.setAttribute("ItemID", sItemId);
		eleItem.setAttribute("UnitOfMeasure", sUOM);
		eleItem.setAttribute("OrganizationCode", sOrganizationCode);

		invokeYantraApi("manageItem", docManageItemInput);
		
		System.out.println("<--------- Item Created = "+sItemId+" --------->");

	}

	protected YFCDocument getOrderXmlWIthOneLine() throws Exception {
		String orderXmlString = 
				"<Order PriorityCode='N' SearchCriteria2='' SearchCriteria1='' Division=''>"
						+ "  <PersonInfoBillTo FirstName='Divisi  n de Personal Model-New' AddressLine1='3ferntree Gully RD' AddressLine2='20' AddressLine3='HUME ST 321777'"
						+ "  AddressLine4='' City='Wien' ZipCode='1130  W AT' State='NT' DayPhone='5461801' DayFaxNo='031 23 2854'/>"
						+ " <OrderLines>"
						+ " <OrderLine OrderedQty='10' PrimeLineNo='1' ChangeStatus=''  ReceivingNode=''>"
						+ " <Item ItemID='TestItem1' UnitOfMeasure='EACH' />"
						+ " </OrderLine>"
						+ " </OrderLines>"
						+ " </Order>";

		YFCDocument orderDoc = YFCDocument.parse(orderXmlString);
		return orderDoc;
	}

	protected YFCDocument orderListTemplate() throws Exception {
		String orderXmlString = "<OrderList><Order OrderNo='' EnterpriseCode='' BuyerOrganizationCode='' SellerOrganizationCode='' EntryType=''   "
				+ "DocumentType='' OrderName='' BillToID='' ShipToID=''    OrderType='' Division='' ShipNode='' Status='' DepartmentCode='' OrderHeaderKey=''>"
				+ "<PersonInfoBillTo FirstName='' AddressLine2='' AddressLine3='' AddressLine4='' City='' ZipCode='' State='' DayPhone='' DayFaxNo=''/>"
				+ "<OrderLines><OrderLine Status='' OrderedQty='' PrimeLineNo='' ReceivingNode='' ShipNode='' SubLineNo='' OrderLineKey='' OrderHeaderKey=''>"
				+ "<Item ItemID='' UnitOfMeasure='' /></OrderLine></OrderLines><OrderStatuses><OrderStatus OrderReleaseKey='' Status='' StatusQty=''/>"
				+ "</OrderStatuses></Order></OrderList>";

		YFCDocument orderDoc = YFCDocument.parse(orderXmlString);
		return orderDoc;
	}
	
	protected YFCDocument shipmentListTemplate() throws Exception {
		String shipmentXmlString = "<Shipments TotalNumberOfRecords=''><Shipment ShipmentKey='' ShipmentNo=''><ShipmentLines><ShipmentLine/></ShipmentLines></Shipment></Shipments>";

		YFCDocument shipmentDoc = YFCDocument.parse(shipmentXmlString);
		return shipmentDoc;
	}
	
	

	
	protected YFCDocument chainedOrderListTemplate() throws Exception {
		String orderXmlString = "<Order DocumentType='' EnterpriseCode='' OrderHeaderKey='' OrderName=''><ChainedOrderList>"
				+ "<Order DocumentType='' EnterpriseCode='' OrderHeaderKey='' OrderNo=''><OrderLines><OrderLine OrderedQty='' PrimeLineNo='' OrderLineKey='' "
				+ "OrderHeaderKey=''><Item ItemID='' UnitOfMeasure='' /></OrderLine></OrderLines></Order></ChainedOrderList></Order>";

		YFCDocument orderDoc = YFCDocument.parse(orderXmlString);
		return orderDoc;
	}
	
	
	
	protected YFCDocument getMultiLineOrder(int iOrderLineCount, String sEnterpriseCode) throws Exception {
		String orderXmlString = 		
				"<Order PriorityCode='N' SearchCriteria2='' SearchCriteria1='' Division=''>"
						+ "  <PersonInfoBillTo FirstName='Divisi  n de Personal Model-New' AddressLine1='3ferntree Gully RD' AddressLine2='20' AddressLine3='HUME ST 321777'"
						+ "  AddressLine4='' City='Wien' ZipCode='1130  W AT' State='NT' DayPhone='5461801' DayFaxNo='031 23 2854'/>"
						+ " <OrderLines>"
						+ " </OrderLines>"
						+ " </Order>";
		YFCDocument orderDoc = YFCDocument.parse(orderXmlString);
		
		for(int i=1;i<=iOrderLineCount; i++) {
			YFCDocument docOrderLine = YFCDocument.getDocumentFor("<OrderLine OrderedQty='10' PrimeLineNo='"+i+"' ChangeStatus=''  ReceivingNode=''>"
					+ " <Item ItemID='TestItem"+i+"' UnitOfMeasure='EACH' />"
					+ " </OrderLine>");
			orderDoc.getElementsByTagName("OrderLines").item(0).importNode(docOrderLine.getDocumentElement());
			
			//Create Item which is used in Input XML
			createItem("TestItem"+i,"EACH", sEnterpriseCode);
		}
		
		return orderDoc;
	}

	protected void manageCommonCodeForOrderValidation(String sCodeValue, String sValue) {
		YFCDocument manageCommonCodeInput = YFCDocument
				.getDocumentFor("<CommonCode Action=\"Manage\" CodeShortDescription=\"" + sValue + "\" CodeValue=\""
						+ sCodeValue + "\" CodeType='ORDER_VALIDATION' OrganizationCode='DEFAULT'/>");
		invokeYantraApi("manageCommonCode", manageCommonCodeInput);

		System.out.println("<--------- manageCommonCode called for = "+sCodeValue+" --------->");
	}

	protected void createCustomer(String sCustomerID, String sCustomerKey, String sExternalCustomerID) {

		String CustomerXml = "<Customer CustomerID='' CustomerKey='' CustomerType='02' ExternalCustomerID='' "
				+ "OrganizationCode='TELSTRA_SCLSA' Status='10'><Consumer><BillingPersonInfo PersonID='' AddressLine1='3ferntree Road' "
				+ "AddressLine2=' 20  '	AddressLine3='HUME ST 321777      ' AddressLine4='' AddressLine5=''	AddressLine6='' AlternateEmailID='' "
				+ "City='Wien'	Company='' Country='AU' DayFaxNo='     031 23 2854   ' DayPhone='     5461801' Department=''	EMailID='' "
				+ "FirstName='Divisi  n de Personal Model-New' 	LastName='' MiddleName=''	MobilePhone='' State='0' Suffix='' Title='' ZipCode='     1130  W AT'/>"
				+ "</Consumer></Customer>";

		YFCDocument docCustomerInput = YFCDocument.getDocumentFor(CustomerXml);
		YFCElement eleCustomer = docCustomerInput.getDocumentElement();

		eleCustomer.setAttribute("CustomerID", sCustomerID);
		eleCustomer.setAttribute("CustomerKey", sCustomerKey);
		eleCustomer.setAttribute("ExternalCustomerID", sExternalCustomerID);

		docCustomerInput.getElementsByTagName("BillingPersonInfo").item(0).setAttribute("PersonID", sCustomerKey);

		invokeYantraApi("createCustomer", docCustomerInput);
		
		System.out.println("<--------- Customer Create = "+sCustomerID+" --------->");

	}
	
	protected YFCDocument createASN(YFCDocument docOrderListOutput) {
		
		String shipmentInput = "<Shipment DocumentType='' ShipmentKey='' EnterpriseCode=''><ShipmentLines></ShipmentLines></Shipment>";

		YFCDocument docShipmentXML = YFCDocument.getDocumentFor(shipmentInput);
		YFCElement eleShipment = docShipmentXML.getDocumentElement();

		YFCElement eleOrder = docOrderListOutput.getElementsByTagName("Order").item(0);
		eleShipment.setAttribute("DocumentType", eleOrder.getAttribute("DocumentType"));
		eleShipment.setAttribute("EnterpriseCode", eleOrder.getAttribute("EnterpriseCode"));

		YFCElement eleShipmentLines = eleShipment.getChildElement("ShipmentLines");
		for(YFCElement eleOrderLine : eleOrder.getElementsByTagName("OrderLine")) {
			YFCElement eleShipmentLine = docShipmentXML.createElement("ShipmentLine");
			eleShipmentLine.setAttribute("OrderLineKey", eleOrderLine.getAttribute("OrderLineKey"));
			eleShipmentLine.setAttribute("OrderHeaderKey", eleOrderLine.getAttribute("OrderHeaderKey"));
			eleShipmentLine.setAttribute("ItemID", eleOrderLine.getChildElement("Item").getAttribute("ItemID"));
			eleShipmentLine.setAttribute("Quantity", eleOrderLine.getAttribute("OrderedQty"));
			eleShipmentLine.setAttribute("UnitOfMeasure", eleOrderLine.getChildElement("Item").getAttribute("UnitOfMeasure"));
			eleShipmentLines.appendChild(eleShipmentLine);
		}

		YFCDocument output = invokeYantraApi("createShipment", docShipmentXML, YFCDocument.getDocumentFor("<Shipment ShipmentKey='' Status='' />"));
		
		System.out.println("<--------- Shipment Created for PO --------->");

		return output;
	}
	
	protected YFCDocument createASNWithQuantity(YFCDocument docOrderListOutput, String sOrderedQty) {
		
		String shipmentInput = "<Shipment DocumentType='' ShipmentKey='' EnterpriseCode=''><ShipmentLines></ShipmentLines></Shipment>";

		YFCDocument docShipmentXML = YFCDocument.getDocumentFor(shipmentInput);
		YFCElement eleShipment = docShipmentXML.getDocumentElement();

		YFCElement eleOrder = docOrderListOutput.getElementsByTagName("Order").item(0);
		eleShipment.setAttribute("DocumentType", eleOrder.getAttribute("DocumentType"));
		eleShipment.setAttribute("EnterpriseCode", eleOrder.getAttribute("EnterpriseCode"));

		YFCElement eleShipmentLines = eleShipment.getChildElement("ShipmentLines");
		for(YFCElement eleOrderLine : eleOrder.getElementsByTagName("OrderLine")) {
			YFCElement eleShipmentLine = docShipmentXML.createElement("ShipmentLine");
			eleShipmentLine.setAttribute("OrderLineKey", eleOrderLine.getAttribute("OrderLineKey"));
			eleShipmentLine.setAttribute("OrderHeaderKey", eleOrderLine.getAttribute("OrderHeaderKey"));
			eleShipmentLine.setAttribute("ItemID", eleOrderLine.getChildElement("Item").getAttribute("ItemID"));
			eleShipmentLine.setAttribute("Quantity", sOrderedQty);
			eleShipmentLine.setAttribute("UnitOfMeasure", eleOrderLine.getChildElement("Item").getAttribute("UnitOfMeasure"));
			eleShipmentLines.appendChild(eleShipmentLine);
		}

		YFCDocument output = invokeYantraApi("createShipment", docShipmentXML, YFCDocument.getDocumentFor("<Shipment ShipmentKey='' Status='' />"));
		
		System.out.println("<--------- Shipment Created for PO --------->");

		return output;
	}
	
	protected YFCDocument createAndConfirmASNWithQuantity(YFCDocument docOrderListOutput,String sOrderedQty) {
		
		String shipmentInput = "<Shipment DocumentType='' ShipmentKey='' EnterpriseCode=''><ShipmentLines></ShipmentLines></Shipment>";

		YFCDocument docShipmentXML = YFCDocument.getDocumentFor(shipmentInput);
		YFCElement eleShipment = docShipmentXML.getDocumentElement();

		YFCElement eleOrder = docOrderListOutput.getElementsByTagName("Order").item(0);
		eleShipment.setAttribute("DocumentType", eleOrder.getAttribute("DocumentType"));
		eleShipment.setAttribute("EnterpriseCode", eleOrder.getAttribute("EnterpriseCode"));

		YFCElement eleShipmentLines = eleShipment.getChildElement("ShipmentLines");
		for(YFCElement eleOrderLine : eleOrder.getElementsByTagName("OrderLine")) {
			YFCElement eleShipmentLine = docShipmentXML.createElement("ShipmentLine");
			eleShipmentLine.setAttribute("OrderLineKey", eleOrderLine.getAttribute("OrderLineKey"));
			eleShipmentLine.setAttribute("OrderHeaderKey", eleOrderLine.getAttribute("OrderHeaderKey"));
			eleShipmentLine.setAttribute("ItemID", eleOrderLine.getChildElement("Item").getAttribute("ItemID"));
			eleShipmentLine.setAttribute("Quantity", sOrderedQty);
			eleShipmentLine.setAttribute("UnitOfMeasure", eleOrderLine.getChildElement("Item").getAttribute("UnitOfMeasure"));
			eleShipmentLines.appendChild(eleShipmentLine);
		}

		YFCDocument output = invokeYantraApi("confirmShipment", docShipmentXML, YFCDocument.getDocumentFor("<Shipment ShipmentKey='' Status='' />"));
		
		System.out.println("<--------- Shipment Created And Confirmed for PO --------->");

		return output;
	}
	
	protected YFCDocument createAndConfirmASN(YFCDocument docOrderListOutput) {
		
		String shipmentInput = "<Shipment DocumentType='' ShipmentKey='' EnterpriseCode=''><ShipmentLines></ShipmentLines></Shipment>";

		YFCDocument docShipmentXML = YFCDocument.getDocumentFor(shipmentInput);
		YFCElement eleShipment = docShipmentXML.getDocumentElement();

		YFCElement eleOrder = docOrderListOutput.getElementsByTagName("Order").item(0);
		eleShipment.setAttribute("DocumentType", eleOrder.getAttribute("DocumentType"));
		eleShipment.setAttribute("EnterpriseCode", eleOrder.getAttribute("EnterpriseCode"));

		YFCElement eleShipmentLines = eleShipment.getChildElement("ShipmentLines");
		for(YFCElement eleOrderLine : eleOrder.getElementsByTagName("OrderLine")) {
			YFCElement eleShipmentLine = docShipmentXML.createElement("ShipmentLine");
			eleShipmentLine.setAttribute("OrderLineKey", eleOrderLine.getAttribute("OrderLineKey"));
			eleShipmentLine.setAttribute("OrderHeaderKey", eleOrderLine.getAttribute("OrderHeaderKey"));
			eleShipmentLine.setAttribute("ItemID", eleOrderLine.getChildElement("Item").getAttribute("ItemID"));
			eleShipmentLine.setAttribute("Quantity", eleOrderLine.getAttribute("OrderedQty"));
			eleShipmentLine.setAttribute("UnitOfMeasure", eleOrderLine.getChildElement("Item").getAttribute("UnitOfMeasure"));
			eleShipmentLines.appendChild(eleShipmentLine);
		}

		YFCDocument output = invokeYantraApi("confirmShipment", docShipmentXML, YFCDocument.getDocumentFor("<Shipment ShipmentKey='' Status='' />"));
		
		System.out.println("<--------- Shipment Created And Confirmed for PO --------->");

		return output;
	}
	protected YFCDocument createAndConfirmSalesOrderShipment(YFCDocument docSalesOrder) {
		YFCElement eleOrder = docSalesOrder.getElementsByTagName("Order").item(0);
		String sOrderReleaseKey ="";
		for(YFCElement eleOrderStatus : docSalesOrder.getElementsByTagName("OrderStatus")) {
			if(eleOrderStatus.getAttribute("Status").equals("3200")) {
				sOrderReleaseKey = eleOrderStatus.getAttribute("OrderReleaseKey");
			}
		}
		YFCDocument inDoc = YFCDocument.getDocumentFor("<Shipment ShipmentKey='"+eleOrder.getAttribute("OrderHeaderKey")+"' "
				+ "OrderHeaderKey='"+eleOrder.getAttribute("OrderHeaderKey")+"'><OrderReleases><OrderRelease OrderReleaseKey="
						+ "'"+sOrderReleaseKey+"'><Order DocumentType='"+eleOrder.getAttribute("DocumentType")+"'"
								+ " EnterpriseCode='"+eleOrder.getAttribute("EnterpriseCode")+"' OrderNo='"+eleOrder.getAttribute("OrderNo")+"'/>"
										+ "</OrderRelease></OrderReleases></Shipment>"); 

	    YFCDocument docOut = invokeYantraApi("createShipment",inDoc); 
	    
	    invokeYantraApi("confirmShipment",docOut); 

	    
		System.out.println("<--------- Shipment Create And Confirmed for Sales Order --------->");

		return docOut;
	}
	
	protected void setNodeType(String sShipNode, String sNodeType) {
		
		String ManageOrgXml = "<Organization OrganizationCode='"+sShipNode+"'><Node ShipNode='"+sShipNode+"' NodeType='"+sNodeType+"'/></Organization>"; 
		
		invokeYantraApi("manageOrganizationHierarchy", YFCDocument.getDocumentFor(ManageOrgXml));
		
		System.out.println("<--------- NodeType set for "+sShipNode+" to "+sNodeType+" --------->");
		
	}
	protected void setInventoryTrackNoForShipNode(String sShipNode) {
		
		String ManageOrgXml = "<Organization OrganizationCode='"+sShipNode+"'><Node ShipNode='"+sShipNode+"' InventoryTracked='N'/></Organization>"; 
		
		invokeYantraApi("manageOrganizationHierarchy", YFCDocument.getDocumentFor(ManageOrgXml));
		
		System.out.println("<--------- InventoryTracked set to N for "+sShipNode+" --------->");
		
	}
	
	

	protected void setRequiresChainedOrderForSeller(String sSellerOrgCode) {
		
		String ManageOrgXml = "<Organization OrganizationCode='"+sSellerOrgCode+"' RequiresChainedOrder='Y'/>"; 
		
		invokeYantraApi("manageOrganizationHierarchy", YFCDocument.getDocumentFor(ManageOrgXml));
		
		System.out.println("<--------- RequiresChainedOrder set to Y for "+sSellerOrgCode+" --------->");
		
	}
	
}
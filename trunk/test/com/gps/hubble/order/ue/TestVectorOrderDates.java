package com.gps.hubble.order.ue;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.date.YDate;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.util.YFCDateUtils;

public class TestVectorOrderDates extends DateCalculationTestBase{
	
	@Test
	public void testMedicalOrder() throws Exception{
		YFCDocument input = getOrderDocument();
		YFCDocument template = getOrderTemplate();
		String shipNode = "DC13";
		String shipToId = "DUMMY001";
		String fromState = "VIC";
		String suburb = "DummyTown2";
		String toPostCode = "9998";
		String departmentCode = "DUMMYDEPARTMENT";
		String materialClass = "DUMMY_MATERIAL";
		String matrixName = "DummyMatrix";
		String priorityString = "Medical";
		YDate dateToWham = new YDate("20/07/2016", "dd/MM/yyyy", true);
		input.getDocumentElement().setAttribute("ShipToID", shipToId);
		setShipNode(input, shipNode);
		setOrderAttributes(input, Priority.MEDICAL, false,dateToWham);
		setPersonInfoShipTo(input.getDocumentElement(), toPostCode, suburb);
		input.getDocumentElement().setAttribute("EntryType", "VECTOR");
		createMaterialClassForDepartmentCode(departmentCode, materialClass, priorityString, matrixName);
		int monTransitTime = 2;
		int tueTransitTime = 3;
		int wedTransitTime = 4;
		int thuTransitTime = 5;
		int friTransitTime = 6;
		YFCDocument transportMatrix = MatrixRecordBuilder.getInstance(this)
		                   .materialClass(materialClass)
		                   .priority(priorityString)
		                   .fromState(fromState)
		                   .toPostCode(toPostCode)
		                   .suburb(suburb)
		                   .mondayTransitTime(monTransitTime)
		                   .tuesdayTransitTime(tueTransitTime)
		                   .wednesdayTransitTime(wedTransitTime)
		                   .thursdayTransitTime(thuTransitTime)
		                   .fridayTransitTime(friTransitTime)
		                   .build();
		input.getDocumentElement().setAttribute("DepartmentCode", departmentCode);
		System.out.println("Input testDatesForDacAndTransportMatrixCase3 is ==========\n"+input);
		YFCDocument output =  invokeYantraApi("createOrder", input,template);
		System.out.println("Output testDatesForDacAndTransportMatrixCase3 is =========\n"+output);
		YDate expShipDate = new YDate(true);
		YDate reqShipDate = output.getDocumentElement().getYDateAttribute("ReqShipDate");
		System.out.println("required shipdate is "+reqShipDate.getString());
		YDate reqDeliveryDate = output.getDocumentElement().getYDateAttribute("ReqDeliveryDate");
		System.out.println("required delivery date is "+reqDeliveryDate.getString());
		Assert.assertEquals(reqShipDate, expShipDate);
		YFCDocument calendarDoc = getCalendarDoc(fromState);
		YDate expDeliveryDate = calculateDeliveryDate(calendarDoc, transportMatrix, expShipDate, false);
		Assert.assertEquals(reqDeliveryDate,expDeliveryDate);
		checkDateAttributeOnLine(output);
	}
	
	@Test
	public void testMedicalOrderWithShipDatePassed() throws Exception{
		YFCDocument input = getOrderDocument();
		YFCDocument template = getOrderTemplate();
		String shipNode = "DC13";
		String shipToId = "DUMMY001";
		String fromState = "VIC";
		String suburb = "DummyTown2";
		String toPostCode = "9998";
		String departmentCode = "DUMMYDEPARTMENT";
		String materialClass = "DUMMY_MATERIAL";
		String matrixName = "DummyMatrix";
		String priorityString = "Medical";
		YDate dateToWham = new YDate("20/07/2016", "dd/MM/yyyy", true);
		input.getDocumentElement().setAttribute("ShipToID", shipToId);
		setShipNode(input, shipNode);
		setOrderAttributes(input, Priority.MEDICAL, false,dateToWham);
		setPersonInfoShipTo(input.getDocumentElement(), toPostCode, suburb);
		input.getDocumentElement().setAttribute("EntryType", "VECTOR");
		YDate expShipDate = new YDate(true);
		YFCDateUtils.addHours(expShipDate, 24);			
		input.getDocumentElement().setAttribute("ReqShipDate", expShipDate);
		createMaterialClassForDepartmentCode(departmentCode, materialClass, priorityString, matrixName);
		int monTransitTime = 2;
		int tueTransitTime = 3;
		int wedTransitTime = 4;
		int thuTransitTime = 5;
		int friTransitTime = 6;
		YFCDocument transportMatrix = MatrixRecordBuilder.getInstance(this)
		                   .materialClass(materialClass)
		                   .priority(priorityString)
		                   .fromState(fromState)
		                   .toPostCode(toPostCode)
		                   .suburb(suburb)
		                   .mondayTransitTime(monTransitTime)
		                   .tuesdayTransitTime(tueTransitTime)
		                   .wednesdayTransitTime(wedTransitTime)
		                   .thursdayTransitTime(thuTransitTime)
		                   .fridayTransitTime(friTransitTime)
		                   .build();
		input.getDocumentElement().setAttribute("DepartmentCode", departmentCode);
		System.out.println("Input testDatesForDacAndTransportMatrixCase3 is ==========\n"+input);
		YFCDocument output =  invokeYantraApi("createOrder", input,template);
		System.out.println("Output testDatesForDacAndTransportMatrixCase3 is =========\n"+output);
		YDate reqShipDate = output.getDocumentElement().getYDateAttribute("ReqShipDate");
		System.out.println("required shipdate is "+reqShipDate.getString());
		YDate reqDeliveryDate = output.getDocumentElement().getYDateAttribute("ReqDeliveryDate");
		System.out.println("required delivery date is "+reqDeliveryDate.getString());
		Assert.assertEquals(reqShipDate, expShipDate);
		YFCDocument calendarDoc = getCalendarDoc(fromState);
		YDate expDeliveryDate = calculateDeliveryDate(calendarDoc, transportMatrix, expShipDate, false);
		Assert.assertEquals(reqDeliveryDate,expDeliveryDate);
		checkDateAttributeOnLine(output);
	}
	
	@Test
	public void testMedicalOrderWithDeliveryDatePassed() throws Exception{
		YFCDocument input = getOrderDocument();
		YFCDocument template = getOrderTemplate();
		String shipNode = "DC13";
		String shipToId = "DUMMY001";
		String fromState = "VIC";
		String suburb = "DummyTown2";
		String toPostCode = "9998";
		String departmentCode = "DUMMYDEPARTMENT";
		String materialClass = "DUMMY_MATERIAL";
		String matrixName = "DummyMatrix";
		String priorityString = "Medical";
		YDate dateToWham = new YDate("20/07/2016", "dd/MM/yyyy", true);
		input.getDocumentElement().setAttribute("ShipToID", shipToId);
		setShipNode(input, shipNode);
		setOrderAttributes(input, Priority.MEDICAL, false,dateToWham);
		setPersonInfoShipTo(input.getDocumentElement(), toPostCode, suburb);
		input.getDocumentElement().setAttribute("EntryType", "VECTOR");
		YDate expDeliveryDate = new YDate(true);
		YFCDateUtils.addHours(expDeliveryDate, 24*7);			
		input.getDocumentElement().setAttribute("ReqDeliveryDate", expDeliveryDate);
		createMaterialClassForDepartmentCode(departmentCode, materialClass, priorityString, matrixName);
		int monTransitTime = 2;
		int tueTransitTime = 3;
		int wedTransitTime = 4;
		int thuTransitTime = 5;
		int friTransitTime = 6;
		MatrixRecordBuilder.getInstance(this)
		                   .materialClass(materialClass)
		                   .priority(priorityString)
		                   .fromState(fromState)
		                   .toPostCode(toPostCode)
		                   .suburb(suburb)
		                   .mondayTransitTime(monTransitTime)
		                   .tuesdayTransitTime(tueTransitTime)
		                   .wednesdayTransitTime(wedTransitTime)
		                   .thursdayTransitTime(thuTransitTime)
		                   .fridayTransitTime(friTransitTime)
		                   .build();
		input.getDocumentElement().setAttribute("DepartmentCode", departmentCode);
		System.out.println("Input testDatesForDacAndTransportMatrixCase3 is ==========\n"+input);
		YFCDocument output =  invokeYantraApi("createOrder", input,template);
		System.out.println("Output testDatesForDacAndTransportMatrixCase3 is =========\n"+output);
		YDate reqShipDate = output.getDocumentElement().getYDateAttribute("ReqShipDate");
		System.out.println("required shipdate is "+reqShipDate.getString());
		YDate reqDeliveryDate = output.getDocumentElement().getYDateAttribute("ReqDeliveryDate");
		System.out.println("required delivery date is "+reqDeliveryDate.getString());
		YDate expShipDate = new YDate(true);
		Assert.assertEquals(reqShipDate, expShipDate);
		Assert.assertEquals(reqDeliveryDate,expDeliveryDate);
		checkDateAttributeOnLine(output);
	}
	
	@Test
	public void testMedicalOrderWithBothShipDateAndDeliveryDatePassed() throws Exception{
		YFCDocument input = getOrderDocument();
		YFCDocument template = getOrderTemplate();
		String shipNode = "DC13";
		String shipToId = "DUMMY001";
		String fromState = "VIC";
		String suburb = "DummyTown2";
		String toPostCode = "9998";
		String departmentCode = "DUMMYDEPARTMENT";
		String materialClass = "DUMMY_MATERIAL";
		String matrixName = "DummyMatrix";
		String priorityString = "Medical";
		YDate dateToWham = new YDate("20/07/2016", "dd/MM/yyyy", true);
		input.getDocumentElement().setAttribute("ShipToID", shipToId);
		setShipNode(input, shipNode);
		setOrderAttributes(input, Priority.MEDICAL, false,dateToWham);
		setPersonInfoShipTo(input.getDocumentElement(), toPostCode, suburb);
		input.getDocumentElement().setAttribute("EntryType", "VECTOR");
		YDate expShipDate = new YDate(true);
		YFCDateUtils.addHours(expShipDate, 24*2);
		YDate expDeliveryDate = new YDate(true);
		YFCDateUtils.addHours(expDeliveryDate, 24*7);
		input.getDocumentElement().setAttribute("ReqShipDate", expShipDate);
		input.getDocumentElement().setAttribute("ReqDeliveryDate", expDeliveryDate);
		createMaterialClassForDepartmentCode(departmentCode, materialClass, priorityString, matrixName);
		int monTransitTime = 2;
		int tueTransitTime = 3;
		int wedTransitTime = 4;
		int thuTransitTime = 5;
		int friTransitTime = 6;
		MatrixRecordBuilder.getInstance(this)
		                   .materialClass(materialClass)
		                   .priority(priorityString)
		                   .fromState(fromState)
		                   .toPostCode(toPostCode)
		                   .suburb(suburb)
		                   .mondayTransitTime(monTransitTime)
		                   .tuesdayTransitTime(tueTransitTime)
		                   .wednesdayTransitTime(wedTransitTime)
		                   .thursdayTransitTime(thuTransitTime)
		                   .fridayTransitTime(friTransitTime)
		                   .build();
		input.getDocumentElement().setAttribute("DepartmentCode", departmentCode);
		System.out.println("Input testDatesForDacAndTransportMatrixCase3 is ==========\n"+input);
		YFCDocument output =  invokeYantraApi("createOrder", input,template);
		System.out.println("Output testDatesForDacAndTransportMatrixCase3 is =========\n"+output);
		YDate reqShipDate = output.getDocumentElement().getYDateAttribute("ReqShipDate");
		System.out.println("required shipdate is "+reqShipDate.getString());
		YDate reqDeliveryDate = output.getDocumentElement().getYDateAttribute("ReqDeliveryDate");
		System.out.println("required delivery date is "+reqDeliveryDate.getString());
		Assert.assertEquals(reqShipDate, expShipDate);
		Assert.assertEquals(reqDeliveryDate,expDeliveryDate);
		checkDateAttributeOnLine(output);
	}
	
	/*
	 * isHazmat : N
	 * beforeCutoff : N
	 */
	@Test
	public void testUrgentOrderWithBothShipDateAndDeliveryDatePassed() throws Exception{
		YFCDocument input = getOrderDocument();
		YFCDocument template = getOrderTemplate();
		String shipNode = "DC13";
		String shipToId = "DUMMY001";
		String fromState = "VIC";
		String suburb = "DummyTown2";
		String toPostCode = "9998";
		String departmentCode = "DUMMYDEPARTMENT";
		String materialClass = "DUMMY_MATERIAL";
		String matrixName = "DummyMatrix";
		String priorityString = "Urgent";
		YDate dateToWham = new YDate("20/07/2016", "dd/MM/yyyy", true);
		setOrderAttributes(input, Priority.URGENT, false,dateToWham);
		setPersonInfoShipTo(input.getDocumentElement(), toPostCode, suburb);
		input.getDocumentElement().setAttribute("EntryType", "VECTOR");
		YDate expShipDate = new YDate(true);
		YFCDateUtils.addHours(expShipDate, 24*2);
		YDate expDeliveryDate = new YDate(true);
		YFCDateUtils.addHours(expDeliveryDate, 24*7);
		input.getDocumentElement().setAttribute("ReqShipDate", expShipDate);
		input.getDocumentElement().setAttribute("ReqDeliveryDate", expDeliveryDate);
		createMaterialClassForDepartmentCode(departmentCode, materialClass, priorityString, matrixName);
		int monTransitTime = 2;
		int tueTransitTime = 3;
		int wedTransitTime = 4;
		int thuTransitTime = 5;
		int friTransitTime = 6;
		MatrixRecordBuilder.getInstance(this)
		                   .materialClass(materialClass)
		                   .priority(priorityString)
		                   .fromState(fromState)
		                   .toPostCode(toPostCode)
		                   .suburb(suburb)
		                   .mondayTransitTime(monTransitTime)
		                   .tuesdayTransitTime(tueTransitTime)
		                   .wednesdayTransitTime(wedTransitTime)
		                   .thursdayTransitTime(thuTransitTime)
		                   .fridayTransitTime(friTransitTime)
		                   .build();
		input.getDocumentElement().setAttribute("ShipToID", shipToId);
		input.getDocumentElement().setAttribute("ShipNode", shipNode);
		input.getDocumentElement().setAttribute("DepartmentCode", departmentCode);
		System.out.println("Input testDatesForDacAndTransportMatrixCase3 is ==========\n"+input);
		YFCDocument output =  invokeYantraApi("createOrder", input,template);
		System.out.println("Output testDatesForDacAndTransportMatrixCase3 is =========\n"+output);
		YDate reqShipDate = output.getDocumentElement().getYDateAttribute("ReqShipDate");
		System.out.println("required shipdate is "+reqShipDate.getString());
		YDate reqDeliveryDate = output.getDocumentElement().getYDateAttribute("ReqDeliveryDate");
		System.out.println("required delivery date is "+reqDeliveryDate.getString());
		Assert.assertEquals(reqShipDate, expShipDate);
		Assert.assertEquals(reqDeliveryDate,expDeliveryDate);
		checkDateAttributeOnLine(output);
	}
	
	/*
	 * BeforeCutOff is false
	 */
	@Test
	public void testUrgentOrderWithDeliveryDatePassed() throws Exception{
		YFCDocument input = getOrderDocument();
		YFCDocument template = getOrderTemplate();
		String shipNode = "DC13";
		String shipToId = "DUMMY001";
		String fromState = "VIC";
		String suburb = "DummyTown2";
		String toPostCode = "9998";
		String departmentCode = "DUMMYDEPARTMENT";
		String materialClass = "DUMMY_MATERIAL";
		String matrixName = "DummyMatrix";
		String priorityString = "Urgent";
		YDate dateToWham = new YDate("20/07/2016", "dd/MM/yyyy", true);
		setOrderAttributes(input, Priority.URGENT, false,dateToWham);
		setPersonInfoShipTo(input.getDocumentElement(), toPostCode, suburb);
		setShipNode(input, shipNode);
		manageCommonCodeForCutOff(false, shipNode);
		input.getDocumentElement().setAttribute("EntryType", "VECTOR");
		YDate expDeliveryDate = new YDate(true);
		YFCDateUtils.addHours(expDeliveryDate, 24*7);
		input.getDocumentElement().setAttribute("ReqDeliveryDate", expDeliveryDate);
		createMaterialClassForDepartmentCode(departmentCode, materialClass, priorityString, matrixName);
		int monTransitTime = 2;
		int tueTransitTime = 3;
		int wedTransitTime = 4;
		int thuTransitTime = 5;
		int friTransitTime = 6;
		MatrixRecordBuilder.getInstance(this)
		                   .materialClass(materialClass)
		                   .priority(priorityString)
		                   .fromState(fromState)
		                   .toPostCode(toPostCode)
		                   .suburb(suburb)
		                   .mondayTransitTime(monTransitTime)
		                   .tuesdayTransitTime(tueTransitTime)
		                   .wednesdayTransitTime(wedTransitTime)
		                   .thursdayTransitTime(thuTransitTime)
		                   .fridayTransitTime(friTransitTime)
		                   .build();
		input.getDocumentElement().setAttribute("ShipToID", shipToId);
		
		input.getDocumentElement().setAttribute("DepartmentCode", departmentCode);
		System.out.println("Input testDatesForDacAndTransportMatrixCase3 is ==========\n"+input);
		YFCDocument output =  invokeYantraApi("createOrder", input,template);
		System.out.println("Output testDatesForDacAndTransportMatrixCase3 is =========\n"+output);
		YDate reqShipDate = output.getDocumentElement().getYDateAttribute("ReqShipDate");
		System.out.println("required shipdate is "+reqShipDate.getString());
		YDate reqDeliveryDate = output.getDocumentElement().getYDateAttribute("ReqDeliveryDate");
		System.out.println("required delivery date is "+reqDeliveryDate.getString());
		YDate expShipDate = new YDate(true);
		YFCDocument calendarDoc = getCalendarDoc(fromState);
		YFCElement allDayShiftElement = getAllDayShiftElement(calendarDoc, expShipDate);
		List<String> exceptionDates = getExceptionDates(calendarDoc);
		expShipDate = getNextBusinessDay(expShipDate, allDayShiftElement, exceptionDates);
		System.out.println("expShipDate is "+expShipDate);
		System.out.println("reqShipDate is "+reqShipDate);
		Assert.assertEquals(reqShipDate, expShipDate);
		Assert.assertEquals(reqDeliveryDate,expDeliveryDate);
		checkDateAttributeOnLine(output);
	}
	
	
	@Test
	public void testUrgentOrderWithShipDatePassed() throws Exception{
		YFCDocument input = getOrderDocument();
		YFCDocument template = getOrderTemplate();
		String shipNode = "DC13";
		String shipToId = "DUMMY001";
		String fromState = "VIC";
		String suburb = "DummyTown2";
		String toPostCode = "9998";
		String departmentCode = "DUMMYDEPARTMENT";
		String materialClass = "DUMMY_MATERIAL";
		String matrixName = "DummyMatrix";
		String priorityString = "Urgent";
		YDate dateToWham = new YDate("20/07/2016", "dd/MM/yyyy", true);
		input.getDocumentElement().setAttribute("ShipToID", shipToId);
		setShipNode(input, shipNode);
		setOrderAttributes(input, Priority.URGENT, false,dateToWham);
		setPersonInfoShipTo(input.getDocumentElement(), toPostCode, suburb);
		manageCommonCodeForCutOff(false, shipNode);
		input.getDocumentElement().setAttribute("EntryType", "VECTOR");
		YDate expShipDate = new YDate(true);
		YFCDateUtils.addHours(expShipDate, 24*3);
		input.getDocumentElement().setAttribute("ReqShipDate", expShipDate);
		createMaterialClassForDepartmentCode(departmentCode, materialClass, priorityString, matrixName);
		int monTransitTime = 2;
		int tueTransitTime = 3;
		int wedTransitTime = 4;
		int thuTransitTime = 5;
		int friTransitTime = 6;
		YFCDocument transportMatrix = MatrixRecordBuilder.getInstance(this)
		                   .materialClass(materialClass)
		                   .priority(priorityString)
		                   .fromState(fromState)
		                   .toPostCode(toPostCode)
		                   .suburb(suburb)
		                   .mondayTransitTime(monTransitTime)
		                   .tuesdayTransitTime(tueTransitTime)
		                   .wednesdayTransitTime(wedTransitTime)
		                   .thursdayTransitTime(thuTransitTime)
		                   .fridayTransitTime(friTransitTime)
		                   .build();
		input.getDocumentElement().setAttribute("DepartmentCode", departmentCode);
		System.out.println("Input testDatesForDacAndTransportMatrixCase3 is ==========\n"+input);
		YFCDocument output =  invokeYantraApi("createOrder", input,template);
		System.out.println("Output testDatesForDacAndTransportMatrixCase3 is =========\n"+output);
		YDate reqShipDate = output.getDocumentElement().getYDateAttribute("ReqShipDate");
		System.out.println("required shipdate is "+reqShipDate.getString());
		YDate reqDeliveryDate = output.getDocumentElement().getYDateAttribute("ReqDeliveryDate");
		System.out.println("required delivery date is "+reqDeliveryDate.getString());
		YFCDocument calendarDoc = getCalendarDoc(fromState);
		YDate expDeliveryDate = calculateDeliveryDate(calendarDoc, transportMatrix, expShipDate, false);
		Assert.assertEquals(reqShipDate, expShipDate);
		Assert.assertEquals(reqDeliveryDate,expDeliveryDate);
		checkDateAttributeOnLine(output);
	}
	
	
	@Test
	public void testUrgentOrderBeforeCutOff() throws Exception{
		System.out.println("testUrgentOrderBeforeCutOff begins ");
		YFCDocument input = getOrderDocument();
		YFCDocument template = getOrderTemplate();
		String shipNode = "DC13";
		String shipToId = "DUMMY001";
		String fromState = "VIC";
		String suburb = "DummyTown2";
		String toPostCode = "9998";
		String departmentCode = "DUMMYDEPARTMENT";
		String materialClass = "DUMMY_MATERIAL";
		String matrixName = "DummyMatrix";
		String priorityString = "Urgent";
		YDate dateToWham = new YDate("20/07/2016", "dd/MM/yyyy", true);
		input.getDocumentElement().setAttribute("ShipToID", shipToId);
		setShipNode(input, shipNode);
		setOrderAttributes(input, Priority.URGENT, false,dateToWham);
		setPersonInfoShipTo(input.getDocumentElement(), toPostCode, suburb);
		manageCommonCodeForCutOff(true, shipNode);
		input.getDocumentElement().setAttribute("EntryType", "VECTOR");
		createMaterialClassForDepartmentCode(departmentCode, materialClass, priorityString, matrixName);
		int monTransitTime = 2;
		int tueTransitTime = 3;
		int wedTransitTime = 4;
		int thuTransitTime = 5;
		int friTransitTime = 6;
		YFCDocument transportMatrix = MatrixRecordBuilder.getInstance(this)
		                   .materialClass(materialClass)
		                   .priority(priorityString)
		                   .fromState(fromState)
		                   .toPostCode(toPostCode)
		                   .suburb(suburb)
		                   .mondayTransitTime(monTransitTime)
		                   .tuesdayTransitTime(tueTransitTime)
		                   .wednesdayTransitTime(wedTransitTime)
		                   .thursdayTransitTime(thuTransitTime)
		                   .fridayTransitTime(friTransitTime)
		                   .build();
		
		input.getDocumentElement().setAttribute("DepartmentCode", departmentCode);
		System.out.println("Input testDatesForDacAndTransportMatrixCase3 is ==========\n"+input);
		YFCDocument output =  invokeYantraApi("createOrder", input,template);
		System.out.println("Output testDatesForDacAndTransportMatrixCase3 is =========\n"+output);
		YDate reqShipDate = output.getDocumentElement().getYDateAttribute("ReqShipDate");
		System.out.println("required shipdate is "+reqShipDate.getString());
		YDate reqDeliveryDate = output.getDocumentElement().getYDateAttribute("ReqDeliveryDate");
		System.out.println("required delivery date is "+reqDeliveryDate.getString());
		YFCDocument calendarDoc = getCalendarDoc(fromState);
		YDate expShipDate = new YDate(true);
		YDate expDeliveryDate = calculateDeliveryDate(calendarDoc, transportMatrix, expShipDate, false);
		Assert.assertEquals(reqShipDate, expShipDate);
		Assert.assertEquals(reqDeliveryDate,expDeliveryDate);
		checkDateAttributeOnLine(output);
	}
	
	@Test
	public void testUrgentHazmatOrder() throws Exception{
		YFCDocument input = getOrderDocument();
		YFCDocument template = getOrderTemplate();
		String shipNode = "DC13";
		String shipToId = "DUMMY001";
		String fromState = "VIC";
		String suburb = "DummyTown2";
		String toPostCode = "9998";
		String departmentCode = "DUMMYDEPARTMENT";
		String materialClass = "DUMMY_MATERIAL";
		String matrixName = "DummyMatrix";
		String priorityString = "Urgent";
		YDate dateToWham = new YDate("20/07/2016", "dd/MM/yyyy", true);
		input.getDocumentElement().setAttribute("ShipToID", shipToId);
		setShipNode(input, shipNode);
		setOrderAttributes(input, Priority.URGENT, true,dateToWham);
		setPersonInfoShipTo(input.getDocumentElement(), toPostCode, suburb);
		manageCommonCodeForCutOff(false, shipNode);
		input.getDocumentElement().setAttribute("EntryType", "VECTOR");
		createMaterialClassForDepartmentCode(departmentCode, materialClass, priorityString, matrixName);
		int monTransitTime = 2;
		int tueTransitTime = 3;
		int wedTransitTime = 4;
		int thuTransitTime = 5;
		int friTransitTime = 6;
		YFCDocument transportMatrix = MatrixRecordBuilder.getInstance(this)
		                   .materialClass(materialClass)
		                   .priority(priorityString)
		                   .fromState(fromState)
		                   .toPostCode(toPostCode)
		                   .suburb(suburb)
		                   .mondayTransitTime(monTransitTime)
		                   .tuesdayTransitTime(tueTransitTime)
		                   .wednesdayTransitTime(wedTransitTime)
		                   .thursdayTransitTime(thuTransitTime)
		                   .fridayTransitTime(friTransitTime)
		                   .build();
		
		input.getDocumentElement().setAttribute("DepartmentCode", departmentCode);
		System.out.println("Input testDatesForDacAndTransportMatrixCase3 is ==========\n"+input);
		YFCDocument output =  invokeYantraApi("createOrder", input,template);
		System.out.println("Output testDatesForDacAndTransportMatrixCase3 is =========\n"+output);
		YDate reqShipDate = output.getDocumentElement().getYDateAttribute("ReqShipDate");
		System.out.println("required shipdate is "+reqShipDate.getString());
		YDate reqDeliveryDate = output.getDocumentElement().getYDateAttribute("ReqDeliveryDate");
		System.out.println("required delivery date is "+reqDeliveryDate.getString());
		YFCDocument calendarDoc = getCalendarDoc(fromState);
		YDate expShipDate = new YDate(true);
		List<String> exceptionDates = getExceptionDates(calendarDoc);
		YFCElement allDayShift = getAllDayShiftElement(calendarDoc, expShipDate);
	    expShipDate = getNextBusinessDay(expShipDate, allDayShift, exceptionDates);
		YDate expDeliveryDate = calculateDeliveryDate(calendarDoc, transportMatrix, expShipDate, true);
		Assert.assertEquals(reqShipDate, expShipDate);
		Assert.assertEquals(reqDeliveryDate,expDeliveryDate);
		checkDateAttributeOnLine(output);
	}
	
	@Test
	public void testLowPriorityOrder() throws Exception{
		YFCDocument input = getOrderDocument();
		YFCDocument template = getOrderTemplate();
		String shipNode = "DC13";
		String shipToId = "DUMMY001";
		String fromState = "VIC";
		String suburb = "DummyTown2";
		String toPostCode = "9998";
		String departmentCode = "DUMMYDEPARTMENT";
		String materialClass = "DUMMY_MATERIAL";
		String matrixName = "DummyMatrix";
		YDate dateToWham = new YDate("20/07/2016", "dd/MM/yyyy", true);
		input.getDocumentElement().setAttribute("ShipToID", shipToId);
		setShipNode(input, shipNode);
		setOrderAttributes(input, Priority.LOW, false,dateToWham);
		String priorityString = input.getDocumentElement().getAttribute(TelstraConstants.PRIORITY_CODE);
		setPersonInfoShipTo(input.getDocumentElement(), toPostCode, suburb);
		manageCommonCodeForCutOff(false, shipNode);
		input.getDocumentElement().setAttribute("EntryType", "VECTOR");
		createMaterialClassForDepartmentCode(departmentCode, materialClass, priorityString, matrixName);
		int monTransitTime = 2;
		int tueTransitTime = 3;
		int wedTransitTime = 4;
		int thuTransitTime = 5;
		int friTransitTime = 6;
		YFCDocument transportMatrix = MatrixRecordBuilder.getInstance(this)
		                   .materialClass(materialClass)
		                   .priority(priorityString)
		                   .fromState(fromState)
		                   .toPostCode(toPostCode)
		                   .suburb(suburb)
		                   .mondayTransitTime(monTransitTime)
		                   .tuesdayTransitTime(tueTransitTime)
		                   .wednesdayTransitTime(wedTransitTime)
		                   .thursdayTransitTime(thuTransitTime)
		                   .fridayTransitTime(friTransitTime)
		                   .build();
		
		input.getDocumentElement().setAttribute("DepartmentCode", departmentCode);
		System.out.println("Input testDatesForDacAndTransportMatrixCase3 is ==========\n"+input);
		YFCDocument output =  invokeYantraApi("createOrder", input,template);
		System.out.println("Output testDatesForDacAndTransportMatrixCase3 is =========\n"+output);
		YDate reqShipDate = output.getDocumentElement().getYDateAttribute("ReqShipDate");
		System.out.println("required shipdate is "+reqShipDate.getString());
		YDate reqDeliveryDate = output.getDocumentElement().getYDateAttribute("ReqDeliveryDate");
		System.out.println("required delivery date is "+reqDeliveryDate.getString());
		YFCDocument calendarDoc = getCalendarDoc(fromState);
		YDate expShipDate = new YDate(true);
		List<String> exceptionDates = getExceptionDates(calendarDoc);
		YFCElement allDayShift = getAllDayShiftElement(calendarDoc, expShipDate);
	    expShipDate = getNextBusinessDay(expShipDate, allDayShift, exceptionDates);
		YDate expDeliveryDate = calculateDeliveryDate(calendarDoc, transportMatrix, expShipDate, false);
		Assert.assertEquals(reqShipDate, expShipDate);
		Assert.assertEquals(reqDeliveryDate,expDeliveryDate);
		checkDateAttributeOnLine(output);
	}
	
	
	@Test
	public void testLowPriorityHazmatOrder() throws Exception{
		YFCDocument input = getOrderDocument();
		YFCDocument template = getOrderTemplate();
		String shipNode = "DC13";
		String shipToId = "DUMMY001";
		String fromState = "VIC";
		String suburb = "DummyTown2";
		String toPostCode = "9998";
		String departmentCode = "DUMMYDEPARTMENT";
		String materialClass = "DUMMY_MATERIAL";
		String matrixName = "DummyMatrix";
		YDate dateToWham = new YDate("20/07/2016", "dd/MM/yyyy", true);
		input.getDocumentElement().setAttribute("ShipToID", shipToId);
		setShipNode(input, shipNode);
		setOrderAttributes(input, Priority.LOW, true,dateToWham);
		String priorityString = input.getDocumentElement().getAttribute(TelstraConstants.PRIORITY_CODE);
		setPersonInfoShipTo(input.getDocumentElement(), toPostCode, suburb);
		input.getDocumentElement().setAttribute("EntryType", "VECTOR");
		createMaterialClassForDepartmentCode(departmentCode, materialClass, priorityString, matrixName);
		int monTransitTime = 2;
		int tueTransitTime = 3;
		int wedTransitTime = 4;
		int thuTransitTime = 5;
		int friTransitTime = 6;
		YFCDocument transportMatrix = MatrixRecordBuilder.getInstance(this)
		                   .materialClass(materialClass)
		                   .priority(priorityString)
		                   .fromState(fromState)
		                   .toPostCode(toPostCode)
		                   .suburb(suburb)
		                   .mondayTransitTime(monTransitTime)
		                   .tuesdayTransitTime(tueTransitTime)
		                   .wednesdayTransitTime(wedTransitTime)
		                   .thursdayTransitTime(thuTransitTime)
		                   .fridayTransitTime(friTransitTime)
		                   .build();
		
		input.getDocumentElement().setAttribute("DepartmentCode", departmentCode);
		System.out.println("Input testDatesForDacAndTransportMatrixCase3 is ==========\n"+input);
		YFCDocument output =  invokeYantraApi("createOrder", input,template);
		System.out.println("Output testDatesForDacAndTransportMatrixCase3 is =========\n"+output);
		YDate reqShipDate = output.getDocumentElement().getYDateAttribute("ReqShipDate");
		System.out.println("required shipdate is "+reqShipDate.getString());
		YDate reqDeliveryDate = output.getDocumentElement().getYDateAttribute("ReqDeliveryDate");
		System.out.println("required delivery date is "+reqDeliveryDate.getString());
		YFCDocument calendarDoc = getCalendarDoc(fromState);
		YDate expShipDate = new YDate(true);
		List<String> exceptionDates = getExceptionDates(calendarDoc);
		YFCElement allDayShift = getAllDayShiftElement(calendarDoc, expShipDate);
	    expShipDate = getNextBusinessDay(expShipDate, allDayShift, exceptionDates);
		YDate expDeliveryDate = calculateDeliveryDate(calendarDoc, transportMatrix, expShipDate, true);
		Assert.assertEquals(reqShipDate, expShipDate);
		Assert.assertEquals(reqDeliveryDate,expDeliveryDate);
		checkDateAttributeOnLine(output);
	}
	
	
	@Test
	public void testLowPriorityOrderWithShipDatePassed() throws Exception{
		YFCDocument input = getOrderDocument();
		YFCDocument template = getOrderTemplate();
		String shipNode = "DC13";
		String shipToId = "DUMMY001";
		String fromState = "VIC";
		String suburb = "DummyTown2";
		String toPostCode = "9998";
		String departmentCode = "DUMMYDEPARTMENT";
		String materialClass = "DUMMY_MATERIAL";
		String matrixName = "DummyMatrix";
		YDate dateToWham = new YDate("20/07/2016", "dd/MM/yyyy", true);
		input.getDocumentElement().setAttribute("ShipToID", shipToId);
		setShipNode(input, shipNode);
		setOrderAttributes(input, Priority.LOW, false,dateToWham);
		String priorityString = input.getDocumentElement().getAttribute(TelstraConstants.PRIORITY_CODE);
		setPersonInfoShipTo(input.getDocumentElement(), toPostCode, suburb);
		input.getDocumentElement().setAttribute("EntryType", "VECTOR");
		YDate expShipDate = new YDate(true);
		YFCDateUtils.addHours(expShipDate, 24*4);
		input.getDocumentElement().setAttribute("ReqShipDate", expShipDate);
		createMaterialClassForDepartmentCode(departmentCode, materialClass, priorityString, matrixName);
		int monTransitTime = 2;
		int tueTransitTime = 3;
		int wedTransitTime = 4;
		int thuTransitTime = 5;
		int friTransitTime = 6;
		YFCDocument transportMatrix = MatrixRecordBuilder.getInstance(this)
		                   .materialClass(materialClass)
		                   .priority(priorityString)
		                   .fromState(fromState)
		                   .toPostCode(toPostCode)
		                   .suburb(suburb)
		                   .mondayTransitTime(monTransitTime)
		                   .tuesdayTransitTime(tueTransitTime)
		                   .wednesdayTransitTime(wedTransitTime)
		                   .thursdayTransitTime(thuTransitTime)
		                   .fridayTransitTime(friTransitTime)
		                   .build();
		input.getDocumentElement().setAttribute("DepartmentCode", departmentCode);
		System.out.println("Input testDatesForDacAndTransportMatrixCase3 is ==========\n"+input);
		YFCDocument output =  invokeYantraApi("createOrder", input,template);
		System.out.println("Output testDatesForDacAndTransportMatrixCase3 is =========\n"+output);
		YDate reqShipDate = output.getDocumentElement().getYDateAttribute("ReqShipDate");
		System.out.println("required shipdate is "+reqShipDate.getString());
		YDate reqDeliveryDate = output.getDocumentElement().getYDateAttribute("ReqDeliveryDate");
		System.out.println("required delivery date is "+reqDeliveryDate.getString());
		YFCDocument calendarDoc = getCalendarDoc(fromState);
		YDate expDeliveryDate = calculateDeliveryDate(calendarDoc, transportMatrix, expShipDate, false);
		Assert.assertEquals(reqShipDate, expShipDate);
		Assert.assertEquals(reqDeliveryDate,expDeliveryDate);
		checkDateAttributeOnLine(output);
	}
	
	
	
	@Test
	public void testLowPriorityOrderWithDeliveryDatePassed() throws Exception{
		YFCDocument input = getOrderDocument();
		YFCDocument template = getOrderTemplate();
		String shipNode = "DC13";
		String shipToId = "DUMMY001";
		String fromState = "VIC";
		String suburb = "DummyTown2";
		String toPostCode = "9998";
		String departmentCode = "DUMMYDEPARTMENT";
		String materialClass = "DUMMY_MATERIAL";
		String matrixName = "DummyMatrix";
		YDate dateToWham = new YDate("20/07/2016", "dd/MM/yyyy", true);
		input.getDocumentElement().setAttribute("ShipToID", shipToId);
		setShipNode(input, shipNode);
		setOrderAttributes(input, Priority.LOW, false,dateToWham);
		String priorityString = input.getDocumentElement().getAttribute("PriorityCode");
		setPersonInfoShipTo(input.getDocumentElement(), toPostCode, suburb);
		input.getDocumentElement().setAttribute("EntryType", "VECTOR");
		createMaterialClassForDepartmentCode(departmentCode, materialClass, priorityString, matrixName);
		YDate expDeliveryDate = new YDate(true);
		YFCDateUtils.addHours(expDeliveryDate, 24*24);
		input.getDocumentElement().setAttribute("ReqDeliveryDate", expDeliveryDate);
		int monTransitTime = 2;
		int tueTransitTime = 3;
		int wedTransitTime = 4;
		int thuTransitTime = 5;
		int friTransitTime = 6;
		MatrixRecordBuilder.getInstance(this)
		                   .materialClass(materialClass)
		                   .priority(priorityString)
		                   .fromState(fromState)
		                   .toPostCode(toPostCode)
		                   .suburb(suburb)
		                   .mondayTransitTime(monTransitTime)
		                   .tuesdayTransitTime(tueTransitTime)
		                   .wednesdayTransitTime(wedTransitTime)
		                   .thursdayTransitTime(thuTransitTime)
		                   .fridayTransitTime(friTransitTime)
		                   .build();
		input.getDocumentElement().setAttribute("DepartmentCode", departmentCode);
		System.out.println("Input testDatesForDacAndTransportMatrixCase3 is ==========\n"+input);
		YFCDocument output =  invokeYantraApi("createOrder", input,template);
		System.out.println("Output testDatesForDacAndTransportMatrixCase3 is =========\n"+output);
		YDate reqShipDate = output.getDocumentElement().getYDateAttribute("ReqShipDate");
		System.out.println("required shipdate is "+reqShipDate.getString());
		YDate reqDeliveryDate = output.getDocumentElement().getYDateAttribute("ReqDeliveryDate");
		System.out.println("required delivery date is "+reqDeliveryDate.getString());
		YFCDocument calendarDoc = getCalendarDoc(fromState);
		YDate expShipDate = new YDate(true);
		List<String> exceptionDates = getExceptionDates(calendarDoc);
		YFCElement allDayShift = getAllDayShiftElement(calendarDoc, expShipDate);
	    expShipDate = getNextBusinessDay(expShipDate, allDayShift, exceptionDates);
		Assert.assertEquals(reqShipDate, expShipDate);
		Assert.assertEquals(reqDeliveryDate,expDeliveryDate);
		checkDateAttributeOnLine(output);
	}
	
	
	@Test
	public void testLowPriorityOrderWithBothShipDateAndDeliveryDatePassed() throws Exception{
		YFCDocument input = getOrderDocument();
		YFCDocument template = getOrderTemplate();
		String shipNode = "DC13";
		String shipToId = "DUMMY001";
		String fromState = "VIC";
		String suburb = "DummyTown2";
		String toPostCode = "9998";
		String departmentCode = "DUMMYDEPARTMENT";
		String materialClass = "DUMMY_MATERIAL";
		String matrixName = "DummyMatrix";
		YDate dateToWham = new YDate("20/07/2016", "dd/MM/yyyy", true);
		input.getDocumentElement().setAttribute("ShipToID", shipToId);
		setShipNode(input, shipNode);
		input.getDocumentElement().setAttribute("DepartmentCode", departmentCode);
		setOrderAttributes(input, Priority.LOW, false,dateToWham);
		String priorityString = input.getDocumentElement().getAttribute(TelstraConstants.PRIORITY_CODE);
		setPersonInfoShipTo(input.getDocumentElement(), toPostCode, suburb);
		manageCommonCodeForCutOff(false, shipNode);
		input.getDocumentElement().setAttribute("EntryType", "VECTOR");
		YDate expShipDate = new YDate(true);
		YFCDateUtils.addHours(expShipDate, 24*5);
		input.getDocumentElement().setAttribute("ReqShipDate", expShipDate);
		YDate expDeliveryDate = new YDate(true);
		YFCDateUtils.addHours(expDeliveryDate, 24*24);
		input.getDocumentElement().setAttribute("ReqDeliveryDate", expDeliveryDate);
		createMaterialClassForDepartmentCode(departmentCode, materialClass, priorityString, matrixName);
		int monTransitTime = 2;
		int tueTransitTime = 3;
		int wedTransitTime = 4;
		int thuTransitTime = 5;
		int friTransitTime = 6;
		MatrixRecordBuilder.getInstance(this)
		                   .materialClass(materialClass)
		                   .priority(priorityString)
		                   .fromState(fromState)
		                   .toPostCode(toPostCode)
		                   .suburb(suburb)
		                   .mondayTransitTime(monTransitTime)
		                   .tuesdayTransitTime(tueTransitTime)
		                   .wednesdayTransitTime(wedTransitTime)
		                   .thursdayTransitTime(thuTransitTime)
		                   .fridayTransitTime(friTransitTime)
		                   .build();
		System.out.println("Input testDatesForDacAndTransportMatrixCase3 is ==========\n"+input);
		YFCDocument output =  invokeYantraApi("createOrder", input,template);
		System.out.println("Output testDatesForDacAndTransportMatrixCase3 is =========\n"+output);
		YDate reqShipDate = output.getDocumentElement().getYDateAttribute("ReqShipDate");
		System.out.println("required shipdate is "+reqShipDate.getString());
		YDate reqDeliveryDate = output.getDocumentElement().getYDateAttribute("ReqDeliveryDate");
		System.out.println("required delivery date is "+reqDeliveryDate.getString());
		Assert.assertEquals(reqShipDate, expShipDate);
		Assert.assertEquals(reqDeliveryDate,expDeliveryDate);
		checkDateAttributeOnLine(output);
	}
}

package com.gps.hubble.order.ue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;



import org.junit.Assert;

import com.gps.hubble.TestBase;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.tools.ydk.config.bucket.YFCEntityList;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.date.YDate;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.util.YFCDateUtils;
import com.yantra.yfc.util.YFCLocale;

public class DateCalculationTestBase extends TestBase{
	protected static enum Priority{
		STANDARD,STANDARN_HOME,URGENT,MEDICAL,LOW
	}
	
	
	
	protected void createDACRecord(String node,String shipToId, String schedule,String routeId, String destId ){
		YFCDocument docDacShedule = YFCDocument.getDocumentFor("<DacSchedule Node='"+node+"' DeliveryAddressCode='"+shipToId+"' Schedule='"+schedule+"' RouteId='"+routeId+"' DestId='"+destId+"'  />");
		invokeYantraService("GpsCreateDacSchedule", docDacShedule);
	}
	
	protected void createMilkRunRecord(String routeId, String milkRunName, String weekday, String frequncy, YDate effectiveFrom, YDate effectiveTo){
		YFCDocument milkRunSchedule = YFCDocument.getDocumentFor("<MilkRunSchedule RouteId='"+routeId+"'  MilkRunName='"+milkRunName+"'  Weekday='"+weekday+"'  Frequency='"+frequncy+"' />");
		if(effectiveFrom != null){
			milkRunSchedule.getDocumentElement().setAttribute("EffectiveFrom", effectiveFrom);
		}
		if(effectiveTo != null){
			milkRunSchedule.getDocumentElement().setAttribute("EffectiveTo",effectiveTo);
		}
		invokeYantraService("GpsCreateMilkRunSchedule", milkRunSchedule);
	}

	
	protected void setOrderAttributes(YFCDocument orderDoc,Priority priority,boolean hazmat,YDate dateToWham){
		YFCElement orderElem = orderDoc.getDocumentElement();
		switch(priority){
			case STANDARD:
				orderElem.setAttribute(TelstraConstants.PRIORITY_CODE, "N");
				break;
			case STANDARN_HOME:
				orderElem.setAttribute(TelstraConstants.PRIORITY_CODE, "N");
				setHomeDeliveryAddress(orderElem);
				break;
			case URGENT:
				orderElem.setAttribute(TelstraConstants.PRIORITY_CODE, "1");
				break;
			case MEDICAL:
				orderElem.setAttribute(TelstraConstants.PRIORITY_CODE, "Medical");
				break;
			case LOW:
				orderElem.setAttribute(TelstraConstants.PRIORITY_CODE, "Low");
				break;
		}
		
		if(hazmat){
			setHazmatFlag(orderDoc);//orderElem.setAttribute("IsHazmat", "Y");
		}else{
			orderElem.setAttribute("IsHazmat", "N");
		}
		if(dateToWham != null){
			setShipDateForDateToWham(dateToWham, orderElem);
		}
	}
	
	
	protected void setHomeDeliveryAddress(YFCElement elemOrder){
		String shipToId = elemOrder.getAttribute("ShipToID");
		YFCDocument docManageCustomer = YFCDocument.getDocumentFor("<Customer CustomerKey='"+shipToId+"' CustomerID='"+shipToId+"' CustomerType='02' ExternalCustomerID='' Status='10' CustomerClassificationCode='HOME_ADDRESS' OrganizationCode='TELSTRA_SCLSA' >"
				+ "<Consumer>"
				+ "<BillingPersonInfo PersonID='"+shipToId+"' AddressLine1='' AddressLine2='' AddressLine3='' AddressLine6='' City='SYDNEY' ZipCode='' State='' Country='AU' DayPhone='' DayFaxNo=''/>"
				+ "</Consumer>"
				+ "</Customer>");
		invokeYantraApi("manageCustomer", docManageCustomer);
	}
	
	protected void setShipDateForDateToWham(YDate shipDate, YFCElement order){
		YFCElement orderDates = order.getChildElement("OrderDates", true);
		YFCElement orderDate = orderDates.getChildElement("OrderDate", true);
		String dateTypeId = orderDate.getAttribute("DateTypeId");
		if(YFCObject.isVoid(dateTypeId)){
			orderDate.setAttribute("DateTypeId", "DATE_TO_WHAM");
			orderDate.setAttribute("RequestedDate", shipDate);
		}
	}
	
	protected void setPersonInfoShipTo(YFCElement elemOrder, String zipCode, String city){
		YFCElement elemPersonInfoBillTo = elemOrder.getChildElement(TelstraConstants.PERSON_INFO_BILL_TO,true);
		elemPersonInfoBillTo.setAttribute("City", city);
		elemPersonInfoBillTo.setAttribute("ZipCode", zipCode);
	}
	
	protected void setPersonInfoShipTo(YFCElement elemOrder, String zipCode, String city,String state){
		YFCElement elemPersonInfoBillTo = elemOrder.getChildElement(TelstraConstants.PERSON_INFO_BILL_TO,true);
		elemPersonInfoBillTo.setAttribute("City", city);
		elemPersonInfoBillTo.setAttribute("ZipCode", zipCode);
		elemPersonInfoBillTo.setAttribute("State", state);
	}
	
	protected YFCDocument getWeekNumbers(){
		return invokeYantraService("GpsGetWeekNumberList", YFCDocument.getDocumentFor("<WeekNumber />"));
	}
	
	protected void checkDateAttributeOnLine(YFCDocument docOrder){
		YFCElement elemOrder = docOrder.getDocumentElement();
		YDate expShipDate = elemOrder.getYDateAttribute(TelstraConstants.REQUIRED_SHIP_DATE);
		YDate expDeliveryDate = elemOrder.getYDateAttribute(TelstraConstants.REQUIRED_DELIVERY_DATE);
		checkDateAttributeOnLine(docOrder, expShipDate, expDeliveryDate);
	}
	
	protected void checkDateAttributeOnLine(YFCDocument docOrder, YDate lineShipDate, YDate lineDeliveryDate){
		YFCElement elemOrder = docOrder.getDocumentElement();
		YFCElement elemOrderLines = elemOrder.getChildElement(TelstraConstants.ORDER_LINES);
		if(elemOrderLines == null || elemOrderLines.getChildren(TelstraConstants.ORDER_LINE) == null){
			return;
		}
		for(Iterator<YFCElement> itr = elemOrderLines.getChildren(TelstraConstants.ORDER_LINE);itr.hasNext();){
			YFCElement elemOrderLine = itr.next();
			if(lineDeliveryDate != null){
				YDate reqDeliveryDate = elemOrderLine.getYDateAttribute(TelstraConstants.REQUIRED_DELIVERY_DATE);
				Assert.assertEquals(lineDeliveryDate, reqDeliveryDate);
			}
			if(lineShipDate != null){
				YDate reqShipDate = elemOrderLine.getYDateAttribute(TelstraConstants.REQUIRED_SHIP_DATE);
				Assert.assertEquals(lineShipDate, reqShipDate);
			}
			
		}
	}
	
	protected int getWeekNumber(YFCDocument weekNumbers, YDate date){
		int weekNumber = 0;
		for(Iterator<YFCElement> itr = weekNumbers.getDocumentElement().getChildren();itr.hasNext();){
			YFCElement weekNumberElem = itr.next();
			YDate fromDate = weekNumberElem.getYDateAttribute("FromDate");
			YDate toDate = weekNumberElem.getYDateAttribute("ToDate");
			if((fromDate.before(date) || fromDate.getString().equals(date.getString())) && toDate.after(date)){
				System.out.println("weekNumber element is "+weekNumberElem);
				weekNumber = weekNumberElem.getIntAttribute("WeekNumber");
			}
		}
		return weekNumber;
	}
	
	protected String createScheduleString(int weekNumber, ShipOn shipOn, String weekDays){
		switch(shipOn){
			case THIS_WEEK:
				return "0" + weekDays; 
			case NEXT_WEEK:
				if(weekNumber == 1 || weekNumber == 3 || weekNumber == 5){
					return "2" + weekDays;
				}else{
					return "1" + weekDays;
				}
			default:
				return "0" + weekDays;
		}
	}
	
	
	protected void manageCommonCodeForCutOff(boolean before, String shipNode) throws ParseException{
		YDate localTime = new YDate(false);
		System.out.println("managing common code and setting time so that it bacame beforeCutOff "+before);
		System.out.println("current time is "+localTime.getString(TelstraConstants.DATE_TIME_FORMAT));
		
		YFCDocument docShipNodeOrgDetails = getOrganizationHeirarchy(shipNode);
		String shipNodeLocaleCode = docShipNodeOrgDetails.getDocumentElement().getAttribute("LocaleCode");
		YFCLocale yfcLocale = YFCLocale.getYFCLocale(shipNodeLocaleCode);
		String shipNodeTimeZoneString = yfcLocale.getTimezone();
		java.util.TimeZone shipNodeTimeZone = java.util.TimeZone.getTimeZone(shipNodeTimeZoneString);
		
		//Currently calculating based on current time
		YDate now = new YDate(false);
		YDate today = new YDate(true);
		SimpleDateFormat hostFormatter = new SimpleDateFormat(TelstraConstants.TELSTRA_DATE_TIME_FORMAT);
		SimpleDateFormat nodeFormatter = new SimpleDateFormat(TelstraConstants.TELSTRA_DATE_TIME_FORMAT);
		nodeFormatter.setTimeZone(shipNodeTimeZone);
		String convertedDateString = nodeFormatter.format(now);
		Date convrtedJavaDate = hostFormatter.parse(convertedDateString); 
		YDate convertedDate = new YDate(convrtedJavaDate.getTime(), false);	
		System.out.println("Converted node date is "+convertedDate.getString(TelstraConstants.DATE_TIME_FORMAT));
		//Check if convertedDate and today's does not match
		int convertedDateDay = convertedDate.getDayOfMonth();
		int todaysDay = today.getDayOfMonth();
		String description = "1330";
		
		if(todaysDay == convertedDateDay || !before){
			int addMinute = -10;
			if(before){
				addMinute = 10;
			}
			
			YFCDateUtils.addMinutes(convertedDate, addMinute);
			int hours = convertedDate.getHours();
			int minutes = convertedDate.getMinutes();
			String hourString = hours + "";
			if(hours < 10){
				hourString = "0" + hours;
			}
			String minuteString = minutes + "";
			if(minutes < 10){
				minuteString = "0" + minutes;
			}
		    description = hourString + minuteString;
		    System.out.println("setting cutoff time for shipNode "+shipNode+" as "+description);
		}else{
			int addMinute = -10;
			if(before){
				addMinute = 10;
			}
			YFCDateUtils.addMinutes(convertedDate, addMinute);
			int hours = convertedDate.getHours();
			hours = hours + 24;
			int minutes = convertedDate.getMinutes();
			String hourString = hours + "";
			if(hours < 10){
				hourString = "0" + hours;
			}
			String minuteString = minutes + "";
			if(minutes < 10){
				minuteString = "0" + minutes;
			}
		    description = hourString + minuteString;
		    System.out.println("setting cutoff time for shipNode by adding extra 12 hours for offset next day scenrio "+shipNode+" as "+description);
		}
		
		
		
		
		
		//If this logic as passed next date
		
		
//		YDate now = new YDate(false);
//		int addHours = -1;
//		if(before){
//			addHours = 1;
//		}
//		YFCDateUtils.addHours(now, addHours);
//		int hours = now.getHours();
//		int minutes = now.getMinutes();
		
	    
	    YFCDocument manageCommonCodeInput = YFCDocument.getDocumentFor("<CommonCode Action=\"Manage\" CodeShortDescription=\""+description+"\" CodeValue=\""+shipNode+"\" CodeType=\"CUT_OFF_TIME\"/>");
	    YFCDocument docManageCommonCodeOutput = invokeYantraApi("manageCommonCode", manageCommonCodeInput);
	    System.out.println("docManageCommonCodeOutput is "+docManageCommonCodeOutput);
	}
	
	
	private YFCDocument getOrganizationHeirarchy(String node) {
		String orgXmlStr = "<Organization OrganizationCode='"+node+"' />";
		YFCDocument docOrgInput = YFCDocument.getDocumentFor(orgXmlStr);
		String orgTemplate = "<Organization LocaleCode=\"\" OrganizationCode=\"\"><CorporatePersonInfo State=\"\" City=\"\" /></Organization>";
		YFCDocument docOrgTemplate = YFCDocument.getDocumentFor(orgTemplate);
		return invokeYantraApi(TelstraConstants.API_GET_ORGANIZATION_HEIRARCHY, docOrgInput, docOrgTemplate);
	}
	
	
	protected void createMaterialClassForDepartmentCode(String departmentCode, String materialClass, String priority, String matrixName){
		//Create common code
		YFCDocument manageCommonCodeInput = YFCDocument.getDocumentFor("<CommonCode Action=\"Manage\" CodeShortDescription=\""+materialClass.toUpperCase()+"\" CodeValue=\""+departmentCode.toUpperCase()+"\" CodeType=\"DEP_MAT_CLASS_MAP\"/>");
	    invokeYantraApi("manageCommonCode", manageCommonCodeInput);
	    //Create Material Class Map
	    YFCDocument docMaterialToTransportMap = YFCDocument.getDocumentFor("<MaterialToTransportMap Priority='"+priority.toUpperCase()+"' MaterialClass='"+materialClass.toUpperCase()+"' MatrixName='"+matrixName.toUpperCase()+"' />");
		invokeYantraService("GpsCreateMaterialToTransportMap", docMaterialToTransportMap);
	}
	
	
	protected YFCDocument getOrderDocument() throws Exception{
		String orderXmlString = "<Order EnterpriseCode=\"TELSTRA_SCLSA\" BuyerOrganizationCode=\"TELSTRA\" SellerOrganizationCode=\"TELSTRA\" EntryType=\"INTEGRAL_PLUS\"   DocumentType=\"0001\" "
				+ " PriorityCode=\"N\"   OrderName=\"MyOrder101\" BillToID=\"DAC\" ShipToID=\"DAC\" SearchCriteria2=\"\"   OrderType=\"MATERIAL_RESERVATION\" SearchCriteria1=\"\" "
				+ " Division=\"\" >"
				+ " <PersonInfoBillTo FirstName=\"\" AddressLine2=\"\" AddressLine3=\"\" AddressLine4=\"\" City=\"Sidney\" ZipCode=\"\" State=\"Victoria\" DayPhone=\"\""
				+ " DayFaxNo=\"\"/>"
				+ " <References>"
				+ " <Reference Name=\"DELIVERY_INSTRUCTIONS\" Value=\"\"/>"
				+ " <Reference Name=\"PM_ORDER\" Value=\"\"/>"
				+ " <Reference Name=\"ACCOUNT_NUMBER\" Value=\"\"/>"
				+ " <Reference Name=\"ACTIVITY_NUMBER\" Value=\"\"/>"
				+ " </References>"
				+ " <OrderLines>"
				+ " <OrderLine ShipNode=\"DC12\" OrderedQty=\"10\" PrimeLineNo=\"\" ChangeStatus=\"\"  ReceivingNode=\"\">"
				+ " <Item ItemID=\"Item1\" UnitOfMeasure=\"EACH\" />"
				+ " </OrderLine>"
				+ " </OrderLines>"
				+ " </Order>";
		
		YFCDocument orderDoc = YFCDocument.parse(orderXmlString);
		return orderDoc;
	}
	
	protected YFCDocument getOrderTemplate() throws Exception{
		String orderXmlString = "<Order EnterpriseCode=\"TELSTRA_SCLSA\" BuyerOrganizationCode=\"TELSTRA\" SellerOrganizationCode=\"TELSTRA\" EntryType=\"INTEGRAL_PLUS\"   DocumentType=\"0001\" "
				+ " PriorityCode=\"N\"   OrderName=\"MyOrder101\" BillToID=\"DAC\" ShipToID=\"DAC\" SearchCriteria2=\"\"   OrderType=\"MATERIAL_RESERVATION\" SearchCriteria1=\"\" "
				+ " Division=\"\" ReqDeliveryDate=\"\" ReqShipDate=\"\">"
				+ " <PersonInfoBillTo FirstName=\"\" AddressLine2=\"\" AddressLine3=\"\" AddressLine4=\"\" City=\"Sidney\" ZipCode=\"\" State=\"Victoria\" DayPhone=\"\""
				+ " DayFaxNo=\"\"/>"
				+ " <PersonInfoShipTo FirstName=\"\" AddressLine2=\"\" AddressLine3=\"\" AddressLine4=\"\" City=\"Sidney\" ZipCode=\"\" State=\"Victoria\" DayPhone=\"\""
				+ " DayFaxNo=\"\"/>"
				+ " <References>"
				+ " <Reference Name=\"DELIVERY_INSTRUCTIONS\" Value=\"\"/>"
				+ " <Reference Name=\"PM_ORDER\" Value=\"\"/>"
				+ " <Reference Name=\"ACCOUNT_NUMBER\" Value=\"\"/>"
				+ " <Reference Name=\"ACTIVITY_NUMBER\" Value=\"\"/>"
				+ " </References>"
				+ " <OrderLines>"
				+ " <OrderLine ShipNode=\"DC12\" OrderedQty=\"10\" PrimeLineNo=\"\" ChangeStatus=\"\"  ReceivingNode=\"\" ReqShipDate=\"\" ReqDeliveryDate=\"\">"
				+ " <Item ItemID=\"Item1\" UnitOfMeasure=\"EACH\" />"
				+ " </OrderLine>"
				+ " </OrderLines>"
				+ " <OrderDates>"
				+ " <OrderDate DateTypeId=\"DATE_TO_WHAM\" RequestedDate=\"2016-07-23\"/>"
				+ " </OrderDates>"
				+" <Extn RouteId='' />"
				+ " </Order>";
		
		YFCDocument orderDoc = YFCDocument.parse(orderXmlString);
		return orderDoc;
	}
	
	
	
	protected YFCDocument getCalendarDoc(String shipNodeState){
		YFCDocument calendarInput = YFCDocument.getDocumentFor("<Calendar CalendarId='"+shipNodeState+"' OrganizationCode='"+"TELSTRA_SCLSA"+"'/>");
		YFCDocument calendarDoc = invokeYantraApi("getCalendarDetails", calendarInput);
		return calendarDoc;
	}
	
	
	protected int getTransitTime(YDate shipDate,YFCElement transportMatrix){
		int dayOfWeek = shipDate.getDayOfWeek();
	    int transitTime = 0;
	    switch(dayOfWeek){
	    	case 2:
	    		transitTime = transportMatrix.getIntAttribute("MondayTransitTime");
	    		break;
	    	case 3:
	    		transitTime = transportMatrix.getIntAttribute("TuesdayTransitTime");
	    		break; 
	    	case 4:
	    		transitTime = transportMatrix.getIntAttribute("WednesdayTransitTime");
	    		break;
	    	case 5:
	    		transitTime = transportMatrix.getIntAttribute("ThursdayTransitTime");
	    		break;
	    	default:
	    		transitTime = transportMatrix.getIntAttribute(TelstraConstants.FRIDAY_TRANSIT_TIME);
	    		break;
	    }
	    return transitTime;
	}
	
	
	protected YDate calculateDeliveryDate(YFCDocument calendarDoc,YFCDocument transportMatrix,YDate shipDate,boolean isHazmat){
		int transitTime = getTransitTime(shipDate,transportMatrix.getDocumentElement());
		int totalTransitTime = transitTime;
		if(isHazmat){
			totalTransitTime = transitTime + 1;
		}
		YDate deliveryDate = calculateDeliveryDate(calendarDoc,shipDate,totalTransitTime);
		return deliveryDate;
	}
	
	protected YDate calculateDeliveryDate(YFCDocument calendarDoc, YDate shipDate, int transitTime){
		if(transitTime == 0){
			return shipDate;
		}
		LinkedList<YDate> transitDates = new LinkedList<YDate>();
		//First get list of transit dates
		for(int i=1; i<=transitTime;i++){
			YDate transitDate = new YDate(shipDate.getString("yyyy-MM-dd HH:mm"), "yyyy-MM-dd HH:mm", false);
			YFCDateUtils.addHours(transitDate, 24*i);
			transitDates.add(transitDate);
		}
		List<String> exceptionDates = getExceptionDates(calendarDoc);
		YFCElement allDayShift = getAllDayShiftElement(calendarDoc, shipDate);
		//Now remove and exception days
		removeExceptionAndWeekends(transitDates, allDayShift,exceptionDates);
		return transitDates.getLast();
	}
	
	
	protected YFCElement getAllDayShiftElement(YFCDocument calendarDoc, YDate shipDate){
		YFCElement allDayShift = null;
		YFCElement calendarElem = calendarDoc.getDocumentElement();
		YFCElement effectivePeriods = calendarElem.getChildElement("EffectivePeriods");
		if(effectivePeriods == null || effectivePeriods.getChildren("EffectivePeriod") == null){
			return null;
		}
		for(Iterator<YFCElement> itr = effectivePeriods.getChildren("EffectivePeriod").iterator();itr.hasNext();){
			YFCElement effectivePeriod = itr.next();
			YDate effectiveFromDate = effectivePeriod.getYDateAttribute("EffectiveFromDate");
			YDate effectiveToDate = effectivePeriod.getYDateAttribute("EffectiveToDate");
			if(!shipDate.after(effectiveFromDate) || !shipDate.before(effectiveToDate)){
				continue;
			}
			YFCElement shiftsElem = effectivePeriod.getChildElement("Shifts");
			for(Iterator<YFCElement> itr2 = shiftsElem.getChildren("Shift").iterator();itr2.hasNext();){
				YFCElement shiftElem = itr2.next();
				if(shiftElem.getAttribute("ShiftName") != null && shiftElem.getAttribute("ShiftName").equalsIgnoreCase("ALL_DAY")){
					allDayShift = shiftElem;
					break;
				}
			}
			
		}
		return allDayShift;
	}
	
	protected YDate getNextBusinessDay(YDate date, YFCElement allDayShift, List<String> exceptionDates){
		long time = date.getTime();
		YDate newYDate = new YDate(time, false);
		YFCDateUtils.addHours(newYDate, 24);
		LinkedList<YDate> nextDates = new LinkedList<YDate>();
		nextDates.add(newYDate);
		removeExceptionAndWeekends(nextDates, allDayShift, exceptionDates);
		return nextDates.getLast();
	}
	
	
	protected List<String> getExceptionDates(YFCDocument calendarDoc){
		YFCElement calendar = calendarDoc.getDocumentElement();
		List<String> exceptionDates = new ArrayList<String>();
		YFCElement calendarDayExceptions = calendar.getChildElement("CalendarDayExceptions");
		for(Iterator<YFCElement> itr = calendarDayExceptions.getChildren("CalendarDayException").iterator();itr.hasNext();){
			YFCElement calendarDayException = itr.next();
			YDate date = calendarDayException.getYDateAttribute("Date");
			if(date != null){
				exceptionDates.add(date.getString("yyyy-MM-dd"));
			}
		}
		return exceptionDates;
	}
	
	protected void removeExceptionAndWeekends(LinkedList<YDate> transitDates, YFCElement allDayShift,List<String> exceptionDates){
		YDate toRemove = null;
		for(YDate transitDate : transitDates){
			if(isNonWorkingDay(transitDate, allDayShift)){
				toRemove = transitDate;
				break;
			}
			if(isExceptionDay(transitDate,exceptionDates)){
				toRemove = transitDate;
				break;
			}
		}
		if(toRemove != null){
			//Add a new date
			YDate lastTransitDate = transitDates.peekLast();
			YDate newTransitDate = new YDate(lastTransitDate.getString("yyyy-MM-dd HH:mm"), "yyyy-MM-dd HH:mm", false);
			YFCDateUtils.addHours(newTransitDate, 24);
			transitDates.remove(toRemove);
			transitDates.add(newTransitDate);
			removeExceptionAndWeekends(transitDates, allDayShift,exceptionDates);
		}
		
	}
	
	protected boolean isExceptionDay(YDate date, List<String> exceptionDates){
		String dateString = date.getString("yyyy-MM-dd");
		boolean exDay = exceptionDates.contains(dateString);
		return exDay;
	}
	
	protected boolean isNonWorkingDay(YDate date, YFCElement allDayShift){
		if(allDayShift == null){
			return false;
		}
		int dayOfWeek = date.getDayOfWeek();
		boolean isNonWorking = false;
		switch(dayOfWeek){
			case 1:
				if(!allDayShift.getBooleanAttribute("SundayValid")){
					isNonWorking = true;
				}
				break;
			case 2:
				if(!allDayShift.getBooleanAttribute("MondayValid")){
					isNonWorking = true;
				}
				break;
			case 3:
				if(!allDayShift.getBooleanAttribute("TuesdayValid")){
					isNonWorking = true;
				}
				break;
			case 4:
				if(!allDayShift.getBooleanAttribute("WednesdayValid")){
					isNonWorking = true;
				}
				break;
			case 5:
				if(!allDayShift.getBooleanAttribute("ThursdayValid")){
					isNonWorking = true;
				}
				break;
			case 6:
				if(!allDayShift.getBooleanAttribute("FridayValid")){
					isNonWorking = true;
				}
				break;
			case 7:
				if(!allDayShift.getBooleanAttribute("SaturdayValid")){
					isNonWorking = true;
				}
				break;
			default:
				isNonWorking = false;
		}
		return isNonWorking;
	}
	
	
	protected void setShipNode(YFCDocument docOrder, String shipNode){
		YFCElement elemOrder = docOrder.getDocumentElement();
		YFCElement elemOrderLines = elemOrder.getChildElement("OrderLines");
		for(Iterator<YFCElement> itr = elemOrderLines.getChildren().iterator();itr.hasNext();){
			YFCElement elemOrderLine = itr.next();
			elemOrderLine.setAttribute(TelstraConstants.SHIP_NODE, shipNode);
		}
	}
	
	protected void setReceivingNode(YFCDocument docOrder, String receivingNode){
		YFCElement elemOrder = docOrder.getDocumentElement();
		YFCElement elemOrderLines = elemOrder.getChildElement("OrderLines");
		for(Iterator<YFCElement> itr = elemOrderLines.getChildren().iterator();itr.hasNext();){
			YFCElement elemOrderLine = itr.next();
			elemOrderLine.setAttribute(TelstraConstants.RECEIVING_NODE, receivingNode);
		}
	}
	
	
	protected void setHazmatFlag(YFCDocument docOrder){
		YFCElement elemOrder = docOrder.getDocumentElement();
		YFCElement elemOrderLines = elemOrder.getChildElement("OrderLines");
		for(Iterator<YFCElement> itr = elemOrderLines.getChildren().iterator();itr.hasNext();){
			YFCElement elemOrderLine = itr.next();
			YFCElement elemItem = elemOrderLine.getChildElement(TelstraConstants.ITEM);
			if(elemItem == null){
				continue;
			}
			String itemId = elemItem.getAttribute(TelstraConstants.ITEM_ID);
			YFCDocument docManageItem = YFCDocument.getDocumentFor("<ItemList>"
										+ "<Item ItemID='"+itemId+"' UnitOfMeasure='EACH' OrganizationCode='"+TelstraConstants.ORG_TELSTRA_SCLSA+"' >"
										+ "<PrimaryInformation IsHazmat='Y' />"
										+ "</Item>"
										+ "</ItemList>");
			System.out.println(docManageItem);
			invokeYantraApi("manageItem", docManageItem);
		}
	}
	
	
	protected static enum ShipOn{
		THIS_WEEK, NEXT_WEEK, NEXT_NEXT_WEEK
	}
	
	
	protected void removeCalendarFor(String calendarId, String organizationCode){
		YFCDocument docDeleteCalendar = YFCDocument.getDocumentFor("<Calendar CalendarId='"+calendarId+"' OrganizationCode='"+organizationCode+"' />");
		invokeYantraApi("deleteCalendar", docDeleteCalendar);
	}
	
	protected String getRouteId(YFCDocument docOrder){
		YFCElement elemOrder = docOrder.getDocumentElement();
		YFCElement elemOrderExtn = elemOrder.getChildElement(TelstraConstants.EXTN);
		String routeId = elemOrderExtn.getAttribute(TelstraConstants.ROUTE_ID);
		return routeId;
	}
	
	
		
	protected static class MatrixRecordBuilder{
		protected TestBase classInstance;
		protected String matrixName;
		protected String materialClass;
		protected String priority;
		protected String fromState;
		protected String toPostCode;
		protected String suburb;
		
		protected int monTransitTime;
		protected int tueTransitTime;
		protected int wedTransitTime;
		protected int thuTransitTime;
		protected int friTransitTime;
		
		
		protected MatrixRecordBuilder(){
		}
		
		static MatrixRecordBuilder getInstance(TestBase classInstance){
			MatrixRecordBuilder builder = new MatrixRecordBuilder();
			builder.classInstance = classInstance;
			return builder;
		}
		
		MatrixRecordBuilder priority(String priority){
			this.priority = priority.toUpperCase();
			return this;
		}
		
		MatrixRecordBuilder materialClass(String materialClass){
			this.materialClass = materialClass.toUpperCase();
			return  this;
		}
		
		MatrixRecordBuilder toPostCode(String toPostCode){
			this.toPostCode = toPostCode;
			return this;
		}
		
		MatrixRecordBuilder fromState(String fromState){
			this.fromState = fromState;
			return this;
		}
		
		MatrixRecordBuilder suburb(String suburb){
			this.suburb = suburb;
			return this;
		}
		
		MatrixRecordBuilder mondayTransitTime(int monTransitTime){
			this.monTransitTime = monTransitTime;
			return this;
		}
		
		MatrixRecordBuilder tuesdayTransitTime(int tueTransitTime){
			this.tueTransitTime = tueTransitTime;
			return this;
		}
		
		MatrixRecordBuilder wednesdayTransitTime(int wedTransitTime){
			this.wedTransitTime = wedTransitTime;
			return this;
		}
		
		MatrixRecordBuilder thursdayTransitTime(int thuTransitTime){
			this.thuTransitTime = thuTransitTime;
			return this;
		}
		
		MatrixRecordBuilder fridayTransitTime(int friTransitTime){
			this.friTransitTime = friTransitTime;
			return this;
		}
			
		YFCDocument build(){
			fetchMatrixName();
			YFCDocument docTransportMatrix = YFCDocument.getDocumentFor("<TransportMatrix MatrixName='"+matrixName.toUpperCase()+"' FromState='"+fromState.toUpperCase()+"' ToPostcode='"+toPostCode.toUpperCase()+"' ToSuburb='"+suburb.toUpperCase()+"' "
				+ "MondayTransitTime='"+monTransitTime+"' TuesdayTransitTime='"+tueTransitTime+"' WednesdayTransitTime='"+wedTransitTime+"' ThursdayTransitTime='"+thuTransitTime+"' FridayTransitTime='"+friTransitTime+"'/>");
			YDate fromDate = new YDate("01/01/2016","dd/MM/yyyy",true);
			YDate toDate = new YDate("31/12/2021","dd/MM/yyyy",true);
			setDates(docTransportMatrix, fromDate, toDate);
			return classInstance.invokeYantraService("GpsCreateTransportMatrix", docTransportMatrix);
		}
		
		protected void setDates(YFCDocument docTransportMatrix,YDate fromDate, YDate toDate){
			docTransportMatrix.getDocumentElement().setAttribute("FromDate", fromDate);
			docTransportMatrix.getDocumentElement().setAttribute("ToDate", toDate);
		}
		
		protected void fetchMatrixName(){
			YFCDocument docMaterialToTransportMap = YFCDocument.getDocumentFor("<MaterialToTransportMap Priority='"+priority.toUpperCase()+"' MaterialClass='"+materialClass.toUpperCase()+"' />");
			YFCDocument docMaterialToTransportMapOutput = classInstance.invokeYantraService("GpsGetMaterialToTransportMap", docMaterialToTransportMap);
			if(docMaterialToTransportMapOutput == null || docMaterialToTransportMapOutput.getDocumentElement() == null){
				this.matrixName = "DUMMY_MATRIX";
				docMaterialToTransportMap.getDocumentElement().setAttribute("MatrixName", this.matrixName);
				classInstance.invokeYantraService("GpsCreateMaterialToTransportMap", docMaterialToTransportMap);
				return;
			}
			this.matrixName = docMaterialToTransportMapOutput.getDocumentElement().getAttribute("MatrixName");
		}
	}
	
	
	protected void changeCalendarAddExceptionDates(String calendarState, List<YDate> exceptionDates){
		YFCDocument docExistingCalendar = getCalendarDoc(calendarState);
		if(docExistingCalendar != null && docExistingCalendar.getDocumentElement() != null){
			//manage calendar
			YFCDocument docChangeCalendar = getChangeCalendarDocument(calendarState,docExistingCalendar.getDocumentElement().getAttribute("CalendarKey"));
			addExceptionDates(docChangeCalendar, exceptionDates);
			invokeYantraApi("changeCalendar", docChangeCalendar);
		}else{
			//Delete existing calendar if any
			deleteExistingCalendar(docExistingCalendar);
			//Create new calendar
			YFCDocument docCreateCalendar = getCreateCalendarDocument(calendarState);
			//add exception day
			addExceptionDates(docCreateCalendar, exceptionDates);
			invokeYantraApi("createCalendar", docCreateCalendar);
		}
	}
	
	private YFCDocument getChangeCalendarDocument(String calendarState,String calendarKey){
		return YFCDocument.getDocumentFor("<Calendar CalendarKey='"+calendarKey+"' CalendarId='"+calendarState+"' OrganizationCode='TELSTRA_SCLSA'>"
                + "<CalendarDayExceptions>"
                + "</CalendarDayExceptions>"
                + "</Calendar>");

	}
	
	
	
	
	
	private void addExceptionDates(YFCDocument docCreateCalendar, List<YDate> exceptionDates){
		if(exceptionDates == null || exceptionDates.isEmpty()){
			return;
		}
		YFCElement elemExceptions = docCreateCalendar.getDocumentElement().getChildElement("CalendarDayExceptions", true);		
		for(YDate exceptionDate : exceptionDates){
			YFCDocument exceptionDateDoc = YFCDocument.getDocumentFor("<CalendarDayException ExceptionType='0'/>");
			exceptionDateDoc.getDocumentElement().setAttribute("Date", exceptionDate);
			elemExceptions.importNode(exceptionDateDoc.getDocumentElement());
		}
	}
	
	private YFCDocument getCreateCalendarDocument(String calendarState){
		return YFCDocument.getDocumentFor("<Calendar CalendarDescription='"+calendarState+"' CalendarId='"+calendarState+"' OrganizationCode='TELSTRA_SCLSA'>"
                                          + "<EffectivePeriods>"
                                          + "<EffectivePeriod EffectiveFromDate='2016-01-01' EffectiveToDate='2116-12-31'>"
                                          + "<Shifts>"
                                          + "<Shift FridayValid='Y' MondayValid='Y' SaturdayValid='N' ShiftEndTime='23:59:59' ShiftName='ALL_DAY' ShiftStartTime='00:00:00' SundayValid='N' ThursdayValid='Y' TuesdayValid='Y' WednesdayValid='Y'/>"
                                          + "</Shifts>"
                                          + "</EffectivePeriod>"
                                          + "</EffectivePeriods>"
                                          + "<CalendarDayExceptions>"
                                          + "</CalendarDayExceptions>"
                                          + "</Calendar>");
										
	}
	
	private void deleteExistingCalendar(YFCDocument docExistingCalendar){
		if(docExistingCalendar == null || docExistingCalendar.getDocumentElement() == null){
			return;
		}
		String calendarKey = docExistingCalendar.getDocumentElement().getAttribute("CalendarKey");
		String calendarId = docExistingCalendar.getDocumentElement().getAttribute("CalendarId");
		String organizationId = docExistingCalendar.getDocumentElement().getAttribute("OrganizationCode");
		
		YFCDocument docDeleteCalendar = YFCDocument.getDocumentFor("<Calendar CalendarId='"+calendarKey+"' CalendarKey='"+calendarId+"' OrganizationCode='"+organizationId+"' />");
		invokeYantraApi("deleteCalendar", docDeleteCalendar);
	}
	
	protected void setOrderLineDates(YFCDocument input, YDate lineShipDate, YDate lineDeliveryDate){
		YFCElement elemOrder = input.getDocumentElement();
		YFCElement elemOrderLines = elemOrder.getChildElement(TelstraConstants.ORDER_LINES);
		if(elemOrderLines == null || elemOrderLines.getChildren() == null){
			return;
		}
		for(Iterator<YFCElement> itr = elemOrderLines.getChildren(TelstraConstants.ORDER_LINE).iterator();itr.hasNext();){
			YFCElement elemOrderLine = itr.next();
			elemOrderLine.setAttribute(TelstraConstants.REQUIRED_SHIP_DATE, lineShipDate);
			elemOrderLine.setAttribute(TelstraConstants.REQUIRED_DELIVERY_DATE, lineDeliveryDate);
		}
	}
	
	
}

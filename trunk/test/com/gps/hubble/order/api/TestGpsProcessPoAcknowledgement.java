package com.gps.hubble.order.api;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;
import org.xml.sax.SAXException;

import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.TestBase;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.dom.YFCDocument;

public class TestGpsProcessPoAcknowledgement  extends TestBase {

	@Test
	public void testProcessPoAcknowledgementSent() throws SAXException, IOException {
		
		YFCDocument createOrderOp = createOrder();
		String sOrderHeaderKey = createOrderOp.getElementsByTagName("OrderLine").item(0).getAttribute("OrderHeaderKey");
		String sOrderName = createOrderOp.getElementsByTagName("Order").item(0).getAttribute("OrderName");
		scheduleAndReleaseOrder(sOrderHeaderKey);
		System.out.println("maxOrderStatus>>> "+maxOrderStatus(sOrderHeaderKey));
	
		YFCDocument docChainedOrderOp =  createChainedOrder(sOrderHeaderKey);
		System.out.println("TestGPSCopyAttributesToChainedOrderAPI :: docChainedOrderOp\n"+docChainedOrderOp);
		
		AcknowledgePO(sOrderName, "1100.10000");
		
		String sMaxOrderStatus = maxOrderStatus(getPOOrderHeaderKey(sOrderName));
		System.out.println("maxOrderStatus>>> "+sMaxOrderStatus);

		Assert.assertEquals("1100.10000", sMaxOrderStatus);
		System.out.println("Expected status: 1100.10000");
		System.out.println("Actual status: "+sMaxOrderStatus);
	
	}
	
	@Test
	public void testProcessPoAcknowledgementNotSent() throws SAXException, IOException {
		
		YFCDocument createOrderOp = createOrder();
		String sOrderHeaderKey = createOrderOp.getElementsByTagName("OrderLine").item(0).getAttribute("OrderHeaderKey");
		String sOrderName = createOrderOp.getElementsByTagName("Order").item(0).getAttribute("OrderName");
		scheduleAndReleaseOrder(sOrderHeaderKey);
		System.out.println("maxOrderStatus>>> "+maxOrderStatus(sOrderHeaderKey));
	
		YFCDocument docChainedOrderOp =  createChainedOrder(sOrderHeaderKey);
		System.out.println("TestGPSCopyAttributesToChainedOrderAPI :: docChainedOrderOp\n"+docChainedOrderOp);
		
		AcknowledgePO(sOrderName, "1100.05000");
		
		String sMaxOrderStatus = maxOrderStatus(getPOOrderHeaderKey(sOrderName));
		System.out.println("maxOrderStatus>>> "+sMaxOrderStatus);

		Assert.assertEquals("1100.05000", sMaxOrderStatus);
		System.out.println("Expected status: 1100.05000");
		System.out.println("Actual status: "+sMaxOrderStatus);
	
	}
	
	@Test
	public void testProcessPoAcknowledgementNotSent_Sent() throws SAXException, IOException {
		
		YFCDocument createOrderOp = createOrder();
		String sOrderHeaderKey = createOrderOp.getElementsByTagName("OrderLine").item(0).getAttribute("OrderHeaderKey");
		String sOrderName = createOrderOp.getElementsByTagName("Order").item(0).getAttribute("OrderName");
		scheduleAndReleaseOrder(sOrderHeaderKey);
		System.out.println("maxOrderStatus>>> "+maxOrderStatus(sOrderHeaderKey));
	
		YFCDocument docChainedOrderOp =  createChainedOrder(sOrderHeaderKey);
		System.out.println("TestGPSCopyAttributesToChainedOrderAPI :: docChainedOrderOp\n"+docChainedOrderOp);
		
		AcknowledgePO(sOrderName, "1100.05000");
		AcknowledgePO(sOrderName, "1100.10000");
		String sMaxOrderStatus = maxOrderStatus(getPOOrderHeaderKey(sOrderName));
		System.out.println("maxOrderStatus>>> "+sMaxOrderStatus);

		Assert.assertEquals("1100.10000", sMaxOrderStatus);
		System.out.println("Expected status: 1100.10000");
		System.out.println("Actual status: "+sMaxOrderStatus);
	
	}
	
	
	private String getPOOrderHeaderKey(String sOrderName) {
		YFCDocument docGetOrderListIp = YFCDocument.getDocumentFor("<Order OrderName='"+sOrderName+"' DocumentType='0005'/>");
		YFCDocument docGetOrderListOp = invokeYantraApi(TelstraConstants.API_GET_ORDER_LIST, docGetOrderListIp);

		return docGetOrderListOp.getElementsByTagName("Order").item(0).getAttribute("OrderHeaderKey");
		
	}




	private void AcknowledgePO(String sOrderName, String sBAseDropStatus) {

		String sChangeOrderStatus = "<OrderStatusChange BaseDropStatus='"+sBAseDropStatus+"' OrderName='"+sOrderName+"' TransactionId='GPS_PO_TECH_CONFIRM.0005.ex'/>";
		invokeYantraService("GpsPOAcknowledgement", YFCDocument.getDocumentFor(sChangeOrderStatus));
	}




	private YFCDocument createChainedOrder(String sOrderHeaderKey) {

		YFCDocument docCreateChainedOrderIp = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderHeaderKey+"'/>");		
		return invokeYantraApi("createChainedOrder", docCreateChainedOrderIp);
		
	
	}




	private YFCDocument createOrder() throws SAXException, IOException {

		String sCreateOrderIpXML = "<Order EnterpriseCode='TELSTRA_SCLSA' BuyerOrganizationCode='TELSTRA' SellerOrganizationCode='TELSTRA' EntryType='INTEGRAL_PLUS' "
				+ "DocumentType='' PriorityCode='1' InterfaceNo='INT_ODR_2' OrderName='KEINRHGJHGJHGJHGV89' BillToID='INV_MAT2' ShipToID='INV_MAT2' SearchCriteria2='+61 413 014 829' OrderType='MATERIAL_RESERVATION' SearchCriteria1='9090909090' Division='CXS053'> "
				+ "  <PersonInfoBillTo FirstName='' AddressLine1='Hosur road' AddressLine2='' AddressLine3='' AddressLine4='' City='Alpurrurulam' ZipCode='4830' State='NT' DayPhone='5461801' DayFaxNo='031 23 2854' Country='AU' /> "
				+ "  <References> <Reference Name='DELIVERY_INSTRUCTIONS' Value='Handle with care' /> <Reference Name='PM_ORDER' Value='' /> <Reference Name='ACCOUNT_NUMBER' Value='00007777' /> "
				+ " <Reference Name='ACTIVITY_NUMBER' Value='00007771' /> </References> <OrderLines> <OrderLine ShipNode='Vendor20_N1' OrderedQty='10' PrimeLineNo='1' ChangeStatus='' ReceivingNode=''> "
				+ " <Item ItemID='000000000002900508' UnitOfMeasure='EACH' ItemShortDesc='WASHER,FLAT MS GALV 12MM' /> </OrderLine>  <OrderLine ShipNode='Vendor20_N1' OrderedQty='10' PrimeLineNo='2' ChangeStatus='' ReceivingNode=''> "
				+ " <Item ItemID='000000000006700205' UnitOfMeasure='EACH' ItemShortDesc='WASHER,FLAT MS GALV 12MM' />  </OrderLine>   </OrderLines>  <OrderDates> "
				+ "     <OrderDate DateTypeId='DATE_TO_WHAM' RequestedDate='20160718' />   </OrderDates> <AdditionalAddresses> <AdditionalAddress AddressType='SHIP_FROM'> <PersonInfo FirstName='Vendor Name' AddressLine1='Street Address' "
				+ " City='vendor city' ZipCode='vendor zip code' State='vendor state' DayPhone='vendor phone' /> </AdditionalAddress> </AdditionalAddresses>  </Order>";

		

		YFCDocument docCreateOrderIp = YFCDocument.parse(sCreateOrderIpXML);
		return invokeYantraService("GpsManageOrder", docCreateOrderIp);

	}
	
	
	private void scheduleAndReleaseOrder(String sOrderHeaderKey) throws SAXException, IOException {

		String sScheduleOrderIp = "<ScheduleOrder OrderHeaderKey='" + sOrderHeaderKey
				+ "' CheckInventory='N' ScheduleAndRelease='Y'/>";
		YFCDocument docScheduleOrderIp = YFCDocument.parse(sScheduleOrderIp);
		invokeYantraApi("scheduleOrder", docScheduleOrderIp);

	}
	
	
	private String maxOrderStatus(String sOrderHeaderKey) {
		// TODO Auto-generated method stub
		return XPathUtil.getXpathAttribute(invokeYantraApi("getOrderList", YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderHeaderKey+"'/>"),  YFCDocument.getDocumentFor("<OrderList><Order MaxOrderStatus='' /></OrderList>")),"//@MaxOrderStatus");
	}
}

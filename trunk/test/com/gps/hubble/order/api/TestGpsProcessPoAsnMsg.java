package com.gps.hubble.order.api;

import org.junit.Assert;
import org.junit.Test;

import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.TestBase;
import com.gps.hubble.constants.TelstraConstants;
import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;

public class TestGpsProcessPoAsnMsg extends TestBase{

	@Test
	public void validateInXmlOrderName(){
		System.out.print("======Test Case : validateInXmlOrderName - Start =========");

		YFCDocument sInDoc = YFCDocument.getDocumentFor("<Shipment/>");
		YFCElement sEle = sInDoc.getDocumentElement();
		sEle.setAttribute("OrderName", "ASN-T1");
		YFCElement sLinesEle = sEle.createChild("ShipmentLines");
		YFCElement cLineEle = sLinesEle.createChild("ShipmentLine");
		String pLineNo = "1";
		String itemID = "PenStand";
		String qty = "10";
		String uom = "Each";		
		prepareShipmentLine(cLineEle,pLineNo,itemID,qty,uom);		
		System.out.print("Service Input Doc"+sEle.toString());

		try{
			getServiceInvoker().invokeYantraService("GpsProcessPOASNMsg", sInDoc);
		}catch (Exception e){
			if(!e.getMessage().contains("TEL_ERR_11623_001")){
				Assert.fail();
			}
		}

		System.out.print("======Test Case : validateInXmlOrderName - End =========");
	}

	@Test
	public void validateInXmlMandatoryFields(){
		System.out.print("======Test Case : validateInXmlMandatoryFields - Start =========");

		YFCDocument sInDoc = YFCDocument.getDocumentFor("<Shipment/>");
		YFCElement sEle = sInDoc.getDocumentElement();
		sEle.setAttribute("OrderName", "ASN111");
		YFCElement sLinesEle = sEle.createChild("ShipmentLines");
		YFCElement cLineEle = sLinesEle.createChild("ShipmentLine");
		String pLineNo = "1";
		String itemID = "";
		String qty = "1";
		String uom = "Each";		
		prepareShipmentLine(cLineEle,pLineNo,itemID,qty,uom);		
		System.out.print("Service Input Doc"+sEle.toString());

		try{
			getServiceInvoker().invokeYantraService("GpsProcessPOASNMsg", sInDoc);
		}catch (Exception e){
			if(!e.getMessage().contains("TEL_ERR_11623_002")){
				Assert.fail();
			}
		}

		System.out.print("======Test Case : validateInXmlMandatoryFields - End =========");
	}

	@Test
	public void validateInXmlZeroLines(){
		System.out.print("======Test Case : validateInXmlZeroLines - Start =========");

		YFCDocument sInDoc = YFCDocument.getDocumentFor("<Shipment/>");
		YFCElement sEle = sInDoc.getDocumentElement();
		sEle.setAttribute("OrderName", "");
		sEle.createChild("ShipmentLines");
		System.out.print("Service Input Doc"+sEle.toString());

		try{
			getServiceInvoker().invokeYantraService("GpsProcessPOASNMsg", sInDoc);
		}catch (Exception e){
			if(!e.getMessage().contains("TEL_ERR_11623_002")){
				Assert.fail();
			}
		}

		System.out.print("======Test Case : validateInXmlZeroLines - End =========");
	}


	//@Test
	public void validateSOHold(){
		System.out.print("======Test Case : validateSOHold - Start =========");

		YFCDocument soInDoc = getSOOrdDoc();
		YFCElement soEle = soInDoc.getDocumentElement();
		soEle.setAttribute("OrderName", "ASN-TV-01");
		soEle.setAttribute("DocumentType", "0001");
		YFCElement orderLine = soEle.getChildElement("OrderLines",true).getChildElement("OrderLine",true);
		orderLine.setAttribute("ShipNode", "TestOrg5");
		orderLine.setAttribute("OrderedQty", "17");
		orderLine.setAttribute("PrimeLineNo", "1");
		orderLine.setAttribute("ChangeStatus", "");
		YFCElement itemEle = orderLine.getChildElement("Item", true);
		itemEle.setAttribute("ItemID", "PenStand");
		itemEle.setAttribute("UnitOfMeasure", "Each");
		System.out.println("Order doc with Order Line : " + soInDoc.toString());
		YFCDocument sDoc = invokeYantraApi("createOrder", soInDoc,YFCDocument.getDocumentFor("<Order OrderHeaderKey=''/>"));
		System.out.println("Order sDoc : " + sDoc.toString());

		YFCElement sEle = sDoc.getDocumentElement();
		sEle.setAttribute("ScheduleAndRelease", "Y");
		sEle.setAttribute("CheckInventory", "N");
		invokeYantraApi("scheduleOrder", sDoc);


		sEle.setAttribute("Action", "MODIFY");
		sEle.setAttribute("HoldFlag", "Y");
		sEle.setAttribute("HoldReasonCode", "Junit Test");
		sEle.removeAttribute("ScheduleAndRelease");
		sEle.removeAttribute("CheckInventory");
		sEle.removeAttribute("Action");

		System.out.println("Order sDoc : " + sDoc.toString());
		sDoc = invokeYantraApi("changeOrder", sDoc,YFCDocument.getDocumentFor("<Order OrderHeaderKey=''/>"));

		YFCDocument sInDoc = YFCDocument.getDocumentFor("<Shipment/>");
		YFCElement shipEle = sInDoc.getDocumentElement();
		shipEle.setAttribute("OrderName", "ASN-TV-01");
		YFCElement sLinesEle = shipEle.createChild("ShipmentLines");
		YFCElement cLineEle = sLinesEle.createChild("ShipmentLine");
		String pLineNo = "1";
		String itemID = "PenStand";
		String qty = "1";
		String uom = "Each";		
		prepareShipmentLine(cLineEle,pLineNo,itemID,qty,uom);		
		System.out.print("Service Input Doc"+sEle.toString());

		try{
			getServiceInvoker().invokeYantraService("GpsProcessPOASNMsg", sInDoc);
		}catch (Exception e){
			if(!e.getMessage().contains("TEL_ERR_006_002")){
				Assert.fail();
			}
		}

		System.out.print("======Test Case : validateSOHold - End =========");
	}


	@Test
	public void withAllValidFieldsJMS(){
		System.out.print("======Test Case : withAllValidFields - Start =========");

		YFCDocument pD = getPoOrdDoc();
		pD.getDocumentElement().setAttribute("OrderName", "ASN-TV-02");
		System.out.println("created PO : "+invokeYantraApi("createOrder", pD));
		YFCDocument sInDoc = YFCDocument.getDocumentFor("<Shipment/>");
		YFCElement shipEle = sInDoc.getDocumentElement();
		shipEle.setAttribute("OrderName", "ASN-TV-02");
		YFCElement sLinesEle = shipEle.createChild("ShipmentLines");
		YFCElement cLineEle = sLinesEle.createChild("ShipmentLine");
		String pLineNo = "1";
		String itemID = "PenStand";
		String qty = "1";
		String uom = "Each";		
		prepareShipmentLine(cLineEle,pLineNo,itemID,qty,uom);		
		System.out.print("Service Input Doc"+shipEle.toString());
		YFCDocument oD = invokeYantraService("GpsProcessPOASNMsg", sInDoc);
		System.out.print("Service Output Doc"+oD.toString());
		if(XmlUtils.isVoid(XPathUtil.getXpathAttribute(oD, "//@ShipmentKey"))){
			Assert.fail();
		}

		System.out.print("======Test Case : withAllValidFields - End =========");

	}

	@Test
	public void withAllValidFieldsExcelUpload(){
		System.out.print("======Test Case : withAllValidFieldsExcelUpload - Start =========");

		YFCDocument pD = getPoOrdDoc();
		pD.getDocumentElement().setAttribute("OrderName", "ASN-TV-02");
		System.out.println("created PO : "+invokeYantraApi("createOrder", pD));
		YFCDocument sInDoc = YFCDocument.getDocumentFor("<Shipment/>");
		YFCElement shipEle = sInDoc.getDocumentElement();
		shipEle.setAttribute("OrderName", "ASN-TV-02");
		shipEle.setAttribute("ConfirmShip", "N");
		YFCElement sLinesEle = shipEle.createChild("ShipmentLines");
		YFCElement cLineEle = sLinesEle.createChild("ShipmentLine");
		String pLineNo = "1";
		String itemID = "PenStand";
		String qty = "1";
		String uom = "Each";		
		prepareShipmentLine(cLineEle,pLineNo,itemID,qty,uom);		
		System.out.print("Service Input Doc"+shipEle.toString());
		YFCDocument oD = invokeYantraService("GpsProcessPOASNMsg", sInDoc);
		System.out.print("Service Output Doc"+oD.toString());
		if(XmlUtils.isVoid(XPathUtil.getXpathAttribute(oD, "//@ShipmentKey"))){
			Assert.fail();
		}

		System.out.print("======Test Case : withAllValidFieldsExcelUpload - End =========");

	}

	@Test
	public void withShipmentNoPassedButExistingShipped(){
		System.out.print("======Test Case : withShipmentNoPassedButExistingShipped - Start =========");

		YFCDocument pD = getPoOrdDoc();
		pD.getDocumentElement().setAttribute("OrderName", "ASN-TV-02");
		System.out.println("created PO : "+invokeYantraApi("createOrder", pD));
		YFCDocument sInDoc = YFCDocument.getDocumentFor("<Shipment/>");
		YFCElement shipEle = sInDoc.getDocumentElement();
		shipEle.setAttribute("OrderName", "ASN-TV-02");
		shipEle.setAttribute("ShipmentNo", "ASNS01");
		shipEle.setAttribute("ConfirmShip", "Y");
		YFCElement sLinesEle = shipEle.createChild("ShipmentLines");
		YFCElement cLineEle = sLinesEle.createChild("ShipmentLine");
		String pLineNo = "1";
		String itemID = "PenStand";
		String qty = "1";
		String uom = "Each";		
		prepareShipmentLine(cLineEle,pLineNo,itemID,qty,uom);
		System.out.print("Service Input Doc"+shipEle.toString());
		YFCDocument oD1 = invokeYantraService("GpsProcessPOASNMsg", sInDoc);
		System.out.print("Service Output Doc"+oD1.toString());

		sInDoc = YFCDocument.getDocumentFor("<Shipment/>");
		shipEle = sInDoc.getDocumentElement();
		shipEle.setAttribute("OrderName", "ASN-TV-02");
		shipEle.setAttribute("ShipmentNo", "ASNS01");
		shipEle.setAttribute("ConfirmShip", "N");
		sLinesEle = shipEle.createChild("ShipmentLines");
		cLineEle = sLinesEle.createChild("ShipmentLine");
		pLineNo = "1";
		itemID = "PenStand";
		qty = "1";
		uom = "Each";
		prepareShipmentLine(cLineEle,pLineNo,itemID,qty,uom);		
		System.out.print("Service Input Doc"+shipEle.toString());
		YFCDocument oD2 = invokeYantraService("GpsProcessPOASNMsg", sInDoc);
		System.out.print("Service Output Doc"+oD2.toString());

		if(oD2.getDocumentElement().getAttribute("ShipmentKey").equals(oD1.getDocumentElement().getAttribute("ShipmentKey"))){
			Assert.fail();
		}



		System.out.print("======Test Case : withShipmentNoPassedButExistingShipped - End =========");

	}

	@Test
	public void withShipmentNotPassedButExisting(){
		System.out.print("======Test Case : withShipmentNotPassedButExisting - Start =========");

		YFCDocument pD = getPoOrdDoc();
		pD.getDocumentElement().setAttribute("OrderName", "ASN-TV-02");
		System.out.println("created PO : "+invokeYantraApi("createOrder", pD));
		YFCDocument sInDoc = YFCDocument.getDocumentFor("<Shipment SCAC=  '1111' ><FromAddress  AddressLine1='453534' AddressLine2='' State='' City='' Country=''/></Shipment>");
		YFCElement shipEle = sInDoc.getDocumentElement();
		shipEle.setAttribute("OrderName", "ASN-TV-02");
		//shipEle.setAttribute("ShipmentNo", "ASNS01");
		shipEle.setAttribute("ConfirmShip", "N");
		YFCElement sLinesEle = shipEle.createChild("ShipmentLines");
		YFCElement cLineEle = sLinesEle.createChild("ShipmentLine");
		String pLineNo = "1";
		String itemID = "PenStand";
		String qty = "1";
		String uom = "Each";		
		shipEle.createChild("Extn").setAttribute("VendorShipNode", "N1");	
		prepareShipmentLine(cLineEle,pLineNo,itemID,qty,uom);
		System.out.print("Service Input Doc"+shipEle.toString());
		YFCDocument oD1 = invokeYantraService("GpsProcessPOASNMsg", sInDoc);
		System.out.print("Service Output Doc"+oD1.toString());
		
		YFCDocument inApi1 = YFCDocument.getDocumentFor("<Shipment ShipmentKey='"+oD1.getDocumentElement().getAttribute("ShipmentKey")+"'/>");
		YFCDocument oApiT1 = YFCDocument.getDocumentFor("<Shipment SCAC='' > <Extn/><FromAddress AddressLine1='' AddressLine2='' State='' City='' Country='' /></Shipment>");
		YFCDocument slD = invokeYantraApi("getShipmentDetails", inApi1,oApiT1);
		System.out.println("getShipmentDetails : " + slD.getString());
		
		
		sInDoc = YFCDocument.getDocumentFor("<Shipment SCAC=  '2222' ><FromAddress  AddressLine1='453534' AddressLine2='12435' State='ST' City='NY' Country='US'/></Shipment>");
		shipEle = sInDoc.getDocumentElement();
		shipEle.setAttribute("OrderName", "ASN-TV-02");
		//shipEle.setAttribute("ShipmentNo", "ASNS01");
		shipEle.setAttribute("ConfirmShip", "N");
		sLinesEle = shipEle.createChild("ShipmentLines");
		cLineEle = sLinesEle.createChild("ShipmentLine");
		shipEle.createChild("Extn").setAttribute("VendorShipNode", "N1");
		pLineNo = "1";
		itemID = "PenStand";
		qty = "1";
		uom = "Each";
		prepareShipmentLine(cLineEle,pLineNo,itemID,qty,uom);		
		System.out.print("Service Input Doc"+shipEle.toString());
		YFCDocument oD2 = invokeYantraService("GpsProcessPOASNMsg", sInDoc);
		System.out.print("Service Output Doc"+oD2.toString());

		inApi1 = YFCDocument.getDocumentFor("<Shipment ShipmentKey='"+oD2.getDocumentElement().getAttribute("ShipmentKey")+"'/>");
		oApiT1 = YFCDocument.getDocumentFor("<Shipment SCAC=''><Extn/><FromAddress AddressLine1='' AddressLine2='' State='' City='' Country='' /></Shipment>");
		slD = invokeYantraApi("getShipmentDetails", inApi1,oApiT1);
		System.out.print("getShipmentDetails : " + slD.getString());
		
		if(!oD2.getDocumentElement().getAttribute("ShipmentKey").equals(oD1.getDocumentElement().getAttribute("ShipmentKey"))){
			Assert.fail();
		}

		System.out.print("======Test Case : withShipmentNotPassedButExisting - End =========");

	}

	@Test
	public void withAllDatePassedButExisting(){
		System.out.print("======Test Case : withAllDatePassedButExisting - Start =========");

		YFCDocument pD = getPoOrdDoc();
		pD.getDocumentElement().setAttribute("OrderName", "ASN-TV-02");
		System.out.println("created PO : "+invokeYantraApi("createOrder", pD));
		YFCDocument sInDoc = YFCDocument.getDocumentFor("<Shipment/>");
		YFCElement shipEle = sInDoc.getDocumentElement();
		shipEle.setAttribute("OrderName", "ASN-TV-02");
		//shipEle.setAttribute("ShipmentNo", "ASNS01");
		shipEle.setAttribute("ConfirmShip", "N");
		YFCElement sLinesEle = shipEle.createChild("ShipmentLines");
		shipEle.setAttribute("TrackingNo", "T1");
		shipEle.createChild("Extn").setAttribute("VendorShipNode", "N1");	
		YFCElement cLineEle = sLinesEle.createChild("ShipmentLine");
		String pLineNo = "1";
		String itemID = "PenStand";
		String qty = "1";
		String uom = "Each";		
		prepareShipmentLine(cLineEle,pLineNo,itemID,qty,uom);
		System.out.print("Service Input Doc"+shipEle.toString());
		YFCDocument oD1 = invokeYantraService("GpsProcessPOASNMsg", sInDoc);
		System.out.print("Service Output Doc"+oD1.toString());

		sInDoc = YFCDocument.getDocumentFor("<Shipment/>");
		shipEle = sInDoc.getDocumentElement();
		shipEle.setAttribute("OrderName", "ASN-TV-02");
		//shipEle.setAttribute("ShipmentNo", "ASNS01");
		shipEle.setAttribute("ConfirmShip", "N");
		shipEle.createChild("Extn").setAttribute("VendorShipNode", "N1");
		shipEle.setAttribute("TrackingNo", "T1");
		sLinesEle = shipEle.createChild("ShipmentLines");
		cLineEle = sLinesEle.createChild("ShipmentLine");
		pLineNo = "1";
		itemID = "PenStand";
		qty = "1";
		uom = "Each";
		prepareShipmentLine(cLineEle,pLineNo,itemID,qty,uom);		
		System.out.print("Service Input Doc"+shipEle.toString());
		YFCDocument oD2 = invokeYantraService("GpsProcessPOASNMsg", sInDoc);
		System.out.print("Service Output Doc"+oD2.toString());
		
		if(!oD2.getDocumentElement().getAttribute("ShipmentKey").equals(oD1.getDocumentElement().getAttribute("ShipmentKey"))){
			Assert.fail();
		}
		System.out.print("======Test Case : withAllDatePassedButExisting - End =========");

	}

	@Test
	public void shipmentNoSplitValidation(){
		System.out.print("======Test Case : shipmentNoSplitValidation - Start =========");

		YFCDocument pD = getPoOrdDoc();
		pD.getDocumentElement().setAttribute("OrderName", "ASN-TV-02");
		System.out.println("created PO : "+invokeYantraApi("createOrder", pD));
		YFCDocument sInDoc = YFCDocument.getDocumentFor("<Shipment/>");
		YFCElement shipEle = sInDoc.getDocumentElement();
		shipEle.setAttribute("OrderName", "ASN-TV-02");
		shipEle.setAttribute("ShipmentNo", "ASNS01");
		shipEle.setAttribute("ConfirmShip", "Y");
		YFCElement sLinesEle = shipEle.createChild("ShipmentLines");
		shipEle.setAttribute("TrackingNo", "T1");
		shipEle.createChild("Extn").setAttribute("VendorShipNode", "N1");	
		YFCElement cLineEle = sLinesEle.createChild("ShipmentLine");
		String pLineNo = "1";
		String itemID = "PenStand";
		String qty = "1";
		String uom = "Each";		
		prepareShipmentLine(cLineEle,pLineNo,itemID,qty,uom);
		System.out.print("Service Input Doc"+shipEle.toString());
		YFCDocument oD1 = invokeYantraService("GpsProcessPOASNMsg", sInDoc);
		System.out.print("Service Output Doc"+oD1.toString());

		sInDoc = YFCDocument.getDocumentFor("<Shipment/>");
		shipEle = sInDoc.getDocumentElement();
		shipEle.setAttribute("OrderName", "ASN-TV-02");
		shipEle.setAttribute("ShipmentNo", "ASNS01");
		shipEle.setAttribute("ConfirmShip", "N");
		shipEle.createChild("Extn").setAttribute("VendorShipNode", "N1");
		shipEle.setAttribute("TrackingNo", "T1");
		sLinesEle = shipEle.createChild("ShipmentLines");
		cLineEle = sLinesEle.createChild("ShipmentLine");
		pLineNo = "1";
		itemID = "PenStand";
		qty = "1";
		uom = "Each";
		prepareShipmentLine(cLineEle,pLineNo,itemID,qty,uom);		
		System.out.print("Service Input Doc"+shipEle.toString());
		YFCDocument oD2 = invokeYantraService("GpsProcessPOASNMsg", sInDoc);
		System.out.print("Service Output Doc"+oD2.toString());
		YFCDocument slD = invokeYantraApi("getShipmentList", YFCDocument.getDocumentFor("<Shipment ShipmentKey='"+oD2.getDocumentElement().getAttribute("ShipmentKey")+"'/>"));
		System.out.print("Service Output Doc"+slD.toString());			
		if(!slD.toString().contains("ASNS01-0")){
			Assert.fail();
		}

		System.out.print("======Test Case : shipmentNoSplitValidation - End =========");

	}

	@Test
	public void committedQtyLesserThanShipped(){
		System.out.print("======Test Case : committedQtyLesserThanShipped - Start =========");

		YFCDocument pD = getPoOrdDoc();
		pD.getDocumentElement().setAttribute("OrderName", "ASN-TV-02");
		YFCDocument ordOutputTemplate = YFCDocument.getDocumentFor("<Order><OrderLines><OrderLine/></OrderLines></Order>");
		YFCDocument yApiOutDoc = invokeYantraApi("createOrder", pD,ordOutputTemplate);
		System.out.println("created PO : "+yApiOutDoc.getString());

		String pOrdHdrKey = XPathUtil.getXpathAttribute(yApiOutDoc, "//@OrderHeaderKey");
		YFCDocument inApiDoc = YFCDocument.getDocumentFor("<OrderStatusChange TransactionId='GPS_VENDOR_UPDATE.0005.ex' " +
				"OrderHeaderKey='"+pOrdHdrKey+"'><OrderLines/></OrderStatusChange >");
		YFCElement curOrdLineEle = inApiDoc.getDocumentElement().getChildElement("OrderLines").createChild(TelstraConstants.ORDER_LINE);
		curOrdLineEle.setAttribute("BaseDropStatus", "1260.10000");		
		curOrdLineEle.setAttribute(TelstraConstants.QUANTITY, "1");
		YFCElement orderLine1 =  XPathUtil.getXPathElement(yApiOutDoc, "//OrderLines/OrderLine[@PrimeLineNo='1']");
		curOrdLineEle.setAttribute(TelstraConstants.ORDER_LINE_KEY, orderLine1.getAttribute(TelstraConstants.ORDER_LINE_KEY));		
		YFCDocument outDocApi1 = invokeYantraApi("changeOrderStatus", inApiDoc);
		System.out.println("changeOrderStatus : "+outDocApi1.getString());

		YFCDocument sInDoc = YFCDocument.getDocumentFor("<Shipment/>");
		YFCElement shipEle = sInDoc.getDocumentElement();
		shipEle.setAttribute("OrderName", "ASN-TV-02");
		shipEle.setAttribute("ShipmentNo", "ASNS01");
		shipEle.setAttribute("ConfirmShip", "Y");
		YFCElement sLinesEle = shipEle.createChild("ShipmentLines");
		shipEle.setAttribute("TrackingNo", "T1");
		shipEle.createChild("Extn").setAttribute("VendorShipNode", "N1");	
		YFCElement cLineEle = sLinesEle.createChild("ShipmentLine");
		String pLineNo = "1";
		String itemID = "PenStand";
		String qty = "2";
		String uom = "Each";		
		prepareShipmentLine(cLineEle,pLineNo,itemID,qty,uom);
		System.out.print("Service Input Doc"+shipEle.toString());
		YFCDocument oD1 = invokeYantraService("GpsProcessPOASNMsg", sInDoc);
		System.out.print("Service Output Doc"+oD1.toString());



		YFCDocument inDocgetOrdList = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+pOrdHdrKey+"'/>");
		YFCDocument templateForGetOrdList = YFCDocument.getDocumentFor("<OrderList>" +
				"<Order DocumentType='' SellerOrganizationCode='' OrderHeaderKey='' HoldFlag=''  OrderNo='' EnterpriseCode='' >" +
				"<OrderLines>" +
				"<OrderLine PrimeLineNo='' SubLineNo='' OrderLineKey='' OrderHeaderKey=''> " +
				"<OrderStatuses>" +
				"<OrderStatus OrderReleaseKey='' Status='' StatusQty=''/>" +
				"</OrderStatuses>" +
				"<ChainedFromOrderLine PrimeLineNo='' OrderLineKey='' ChainedFromOrderHeaderKey=''/>" +
				"</OrderLine>" +
				"</OrderLines>" +
				"</Order>" +
				"</OrderList>");		
		YFCDocument outputGetOrdListForComQry =  invokeYantraApi("getOrderList", inDocgetOrdList,templateForGetOrdList);   
		System.out.println("outputGetOrdListForComQry : "+outputGetOrdListForComQry.getString());

		YFCElement comittedStatusEle = XPathUtil.getXPathElement(outputGetOrdListForComQry, "//Order/OrderLines/OrderLine/OrderStatuses/OrderStatus[@Status='1260.10000']");

		if(!XmlUtils.isVoid(comittedStatusEle)){
			Assert.fail();
		}

		System.out.print("======Test Case : committedQtyLesserThanShipped - End =========");

	}

	@Test
	public void itemIDMismatching(){
		System.out.print("======Test Case : itemIDMismatching - Start =========");

		YFCDocument pD = getPoOrdDoc();
		pD.getDocumentElement().setAttribute("OrderName", "ASN-TV-02");
		YFCDocument ordOutputTemplate = YFCDocument.getDocumentFor("<Order><OrderLines><OrderLine/></OrderLines></Order>");
		YFCDocument yApiOutDoc = invokeYantraApi("createOrder", pD,ordOutputTemplate);
		System.out.println("created PO : "+yApiOutDoc.getString());

		String pOrdHdrKey = XPathUtil.getXpathAttribute(yApiOutDoc, "//@OrderHeaderKey");

		YFCDocument sInDoc = YFCDocument.getDocumentFor("<Shipment/>");
		YFCElement shipEle = sInDoc.getDocumentElement();
		shipEle.setAttribute("OrderName", "ASN-TV-02");
		shipEle.setAttribute("ShipmentNo", "ASNS01");
		shipEle.setAttribute("ConfirmShip", "Y");
		YFCElement sLinesEle = shipEle.createChild("ShipmentLines");
		shipEle.setAttribute("TrackingNo", "T1");
		shipEle.createChild("Extn").setAttribute("VendorShipNode", "N1");	
		YFCElement cLineEle = sLinesEle.createChild("ShipmentLine");
		String pLineNo = "1";
		String itemID = "PenStand1";
		String qty = "2";
		String uom = "Each";		
		prepareShipmentLine(cLineEle,pLineNo,itemID,qty,uom);
		System.out.print("Service Input Doc"+shipEle.toString());
		try{
			YFCDocument oD1 = invokeYantraService("GpsProcessPOASNMsg", sInDoc);
			System.out.print("Service Output Doc"+oD1.toString());
		}catch (Exception e){
			if(!e.toString().contains("TEL_ERR_11623_003")){
				Assert.fail();
			}
		}



		YFCDocument inDocgetOrdList = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+pOrdHdrKey+"'/>");
		YFCDocument templateForGetOrdList = YFCDocument.getDocumentFor("<OrderList>" +
				"<Order DocumentType='' SellerOrganizationCode='' OrderHeaderKey='' HoldFlag=''  OrderNo='' EnterpriseCode='' >" +
				"<OrderLines>" +
				"<OrderLine PrimeLineNo='' SubLineNo='' OrderLineKey='' OrderHeaderKey=''> " +
				"<OrderStatuses>" +
				"<OrderStatus OrderReleaseKey='' Status='' StatusQty=''/>" +
				"</OrderStatuses>" +
				"<ChainedFromOrderLine PrimeLineNo='' OrderLineKey='' ChainedFromOrderHeaderKey=''/>" +
				"</OrderLine>" +
				"</OrderLines>" +
				"</Order>" +
				"</OrderList>");		
		YFCDocument outputGetOrdListForComQry =  invokeYantraApi("getOrderList", inDocgetOrdList,templateForGetOrdList);   
		System.out.println("outputGetOrdListForComQry : "+outputGetOrdListForComQry.getString());

		YFCElement comittedStatusEle = XPathUtil.getXPathElement(outputGetOrdListForComQry, "//Order/OrderLines/OrderLine/OrderStatuses/OrderStatus[@Status='1260.10000']");

		if(!XmlUtils.isVoid(comittedStatusEle)){
			Assert.fail();
		}

		System.out.print("======Test Case : itemIDMismatching - End =========");

	}


	@Test
	public void shipmentLineAndPOLineValidation(){
		System.out.print("======Test Case : shipmentLineAndPOLineValidation - Start =========");

		YFCDocument pD = getPoOrdDoc();
		pD.getDocumentElement().setAttribute("OrderName", "ASN-TV-02");
		YFCDocument ordOutputTemplate = YFCDocument.getDocumentFor("<Order><OrderLines><OrderLine/></OrderLines></Order>");
		YFCDocument yApiOutDoc = invokeYantraApi("createOrder", pD,ordOutputTemplate);
		System.out.println("created PO : "+yApiOutDoc.getString());

		String pOrdHdrKey = XPathUtil.getXpathAttribute(yApiOutDoc, "//@OrderHeaderKey");

		YFCDocument sInDoc = YFCDocument.getDocumentFor("<Shipment/>");
		YFCElement shipEle = sInDoc.getDocumentElement();
		shipEle.setAttribute("OrderName", "ASN-TV-02");
		shipEle.setAttribute("ShipmentNo", "ASNS01");
		shipEle.setAttribute("ConfirmShip", "Y");
		YFCElement sLinesEle = shipEle.createChild("ShipmentLines");
		shipEle.setAttribute("TrackingNo", "T1");
		shipEle.createChild("Extn").setAttribute("VendorShipNode", "N1");	
		YFCElement cLineEle = sLinesEle.createChild("ShipmentLine");
		String pLineNo = "10";
		String itemID = "PenStand1";
		String qty = "2";
		String uom = "Each";		
		prepareShipmentLine(cLineEle,pLineNo,itemID,qty,uom);
		System.out.print("Service Input Doc"+shipEle.toString());
		try{
			YFCDocument oD1 = invokeYantraService("GpsProcessPOASNMsg", sInDoc);
			System.out.print("Service Output Doc"+oD1.toString());
		}catch (Exception e){
			if(!e.toString().contains("TEL_ERR_11623_004")){
				Assert.fail();
			}
		}



		YFCDocument inDocgetOrdList = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+pOrdHdrKey+"'/>");
		YFCDocument templateForGetOrdList = YFCDocument.getDocumentFor("<OrderList>" +
				"<Order DocumentType='' SellerOrganizationCode='' OrderHeaderKey='' HoldFlag=''  OrderNo='' EnterpriseCode='' >" +
				"<OrderLines>" +
				"<OrderLine PrimeLineNo='' SubLineNo='' OrderLineKey='' OrderHeaderKey=''> " +
				"<OrderStatuses>" +
				"<OrderStatus OrderReleaseKey='' Status='' StatusQty=''/>" +
				"</OrderStatuses>" +
				"<ChainedFromOrderLine PrimeLineNo='' OrderLineKey='' ChainedFromOrderHeaderKey=''/>" +
				"</OrderLine>" +
				"</OrderLines>" +
				"</Order>" +
				"</OrderList>");		
		YFCDocument outputGetOrdListForComQry =  invokeYantraApi("getOrderList", inDocgetOrdList,templateForGetOrdList);   
		System.out.println("outputGetOrdListForComQry : "+outputGetOrdListForComQry.getString());

		YFCElement comittedStatusEle = XPathUtil.getXPathElement(outputGetOrdListForComQry, "//Order/OrderLines/OrderLine/OrderStatuses/OrderStatus[@Status='1260.10000']");

		if(!XmlUtils.isVoid(comittedStatusEle)){
			Assert.fail();
		}

		System.out.print("======Test Case : shipmentLineAndPOLineValidation - End =========");

	}


	@Test
	public void committedQtyGreaterThanShipped(){
		System.out.print("======Test Case : committedQtyGreaterThanShipped - Start =========");

		YFCDocument pD = getPoOrdDoc();
		pD.getDocumentElement().setAttribute("OrderName", "ASN-TV-02");
		YFCDocument ordOutputTemplate = YFCDocument.getDocumentFor("<Order><OrderLines><OrderLine/></OrderLines></Order>");
		YFCDocument yApiOutDoc = invokeYantraApi("createOrder", pD,ordOutputTemplate);
		System.out.println("created PO : "+yApiOutDoc.getString());

		String pOrdHdrKey = XPathUtil.getXpathAttribute(yApiOutDoc, "//@OrderHeaderKey");
		YFCDocument inApiDoc = YFCDocument.getDocumentFor("<OrderStatusChange TransactionId='GPS_VENDOR_UPDATE.0005.ex' " +
				"OrderHeaderKey='"+pOrdHdrKey+"'><OrderLines/></OrderStatusChange >");
		YFCElement curOrdLineEle = inApiDoc.getDocumentElement().getChildElement("OrderLines").createChild(TelstraConstants.ORDER_LINE);
		curOrdLineEle.setAttribute("BaseDropStatus", "1260.10000");		
		curOrdLineEle.setAttribute(TelstraConstants.QUANTITY, "3");
		YFCElement orderLine1 =  XPathUtil.getXPathElement(yApiOutDoc, "//OrderLines/OrderLine[@PrimeLineNo='1']");
		curOrdLineEle.setAttribute(TelstraConstants.ORDER_LINE_KEY, orderLine1.getAttribute(TelstraConstants.ORDER_LINE_KEY));		
		YFCDocument outDocApi1 = invokeYantraApi("changeOrderStatus", inApiDoc);
		System.out.println("changeOrderStatus : "+outDocApi1.getString());

		YFCDocument sInDoc = YFCDocument.getDocumentFor("<Shipment/>");
		YFCElement shipEle = sInDoc.getDocumentElement();
		shipEle.setAttribute("OrderName", "ASN-TV-02");
		shipEle.setAttribute("ShipmentNo", "ASNS01");
		shipEle.setAttribute("ConfirmShip", "Y");
		YFCElement sLinesEle = shipEle.createChild("ShipmentLines");
		shipEle.setAttribute("TrackingNo", "T1");
		shipEle.createChild("Extn").setAttribute("VendorShipNode", "N1");	
		YFCElement cLineEle = sLinesEle.createChild("ShipmentLine");
		String pLineNo = "1";
		String itemID = "PenStand";
		String qty = "2";
		String uom = "Each";		
		prepareShipmentLine(cLineEle,pLineNo,itemID,qty,uom);
		System.out.print("Service Input Doc"+shipEle.toString());
		YFCDocument oD1 = invokeYantraService("GpsProcessPOASNMsg", sInDoc);
		System.out.print("Service Output Doc"+oD1.toString());



		YFCDocument inDocgetOrdList = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+pOrdHdrKey+"'/>");
		YFCDocument templateForGetOrdList = YFCDocument.getDocumentFor("<OrderList>" +
				"<Order DocumentType='' SellerOrganizationCode='' OrderHeaderKey='' HoldFlag=''  OrderNo='' EnterpriseCode='' >" +
				"<OrderLines>" +
				"<OrderLine PrimeLineNo='' SubLineNo='' OrderLineKey='' OrderHeaderKey=''> " +
				"<OrderStatuses>" +
				"<OrderStatus OrderReleaseKey='' Status='' StatusQty=''/>" +
				"</OrderStatuses>" +
				"<ChainedFromOrderLine PrimeLineNo='' OrderLineKey='' ChainedFromOrderHeaderKey=''/>" +
				"</OrderLine>" +
				"</OrderLines>" +
				"</Order>" +
				"</OrderList>");		
		YFCDocument outputGetOrdListForComQry =  invokeYantraApi("getOrderList", inDocgetOrdList,templateForGetOrdList);   
		System.out.println("outputGetOrdListForComQry : "+outputGetOrdListForComQry.getString());

		YFCElement comittedStatusEle = XPathUtil.getXPathElement(outputGetOrdListForComQry, "//Order/OrderLines/OrderLine/OrderStatuses/OrderStatus[@Status='1260.10000']");

		if("1.00".equals(comittedStatusEle.getAttribute("StatusQty"))){
			Assert.assertEquals("Pass", "Pass");
		}else{
			Assert.fail();
		}

		System.out.print("======Test Case : committedQtyGreaterThanShipped - End =========");

	}

	@Test
	public void committedQtyEqualToShipped(){
		System.out.print("======Test Case : committedQtyEqualToShipped - Start =========");

		YFCDocument pD = getPoOrdDoc();
		pD.getDocumentElement().setAttribute("OrderName", "ASN-TV-02");
		YFCDocument ordOutputTemplate = YFCDocument.getDocumentFor("<Order><OrderLines><OrderLine/></OrderLines></Order>");
		YFCDocument yApiOutDoc = invokeYantraApi("createOrder", pD,ordOutputTemplate);
		System.out.println("created PO : "+yApiOutDoc.getString());

		String pOrdHdrKey = XPathUtil.getXpathAttribute(yApiOutDoc, "//@OrderHeaderKey");
		YFCDocument inApiDoc = YFCDocument.getDocumentFor("<OrderStatusChange TransactionId='GPS_VENDOR_UPDATE.0005.ex' " +
				"OrderHeaderKey='"+pOrdHdrKey+"'><OrderLines/></OrderStatusChange >");
		YFCElement curOrdLineEle = inApiDoc.getDocumentElement().getChildElement("OrderLines").createChild(TelstraConstants.ORDER_LINE);
		curOrdLineEle.setAttribute("BaseDropStatus", "1260.10000");		
		curOrdLineEle.setAttribute(TelstraConstants.QUANTITY, "3");
		YFCElement orderLine1 =  XPathUtil.getXPathElement(yApiOutDoc, "//OrderLines/OrderLine[@PrimeLineNo='1']");
		curOrdLineEle.setAttribute(TelstraConstants.ORDER_LINE_KEY, orderLine1.getAttribute(TelstraConstants.ORDER_LINE_KEY));		
		YFCDocument outDocApi1 = invokeYantraApi("changeOrderStatus", inApiDoc);
		System.out.println("changeOrderStatus : "+outDocApi1.getString());

		YFCDocument sInDoc = YFCDocument.getDocumentFor("<Shipment/>");
		YFCElement shipEle = sInDoc.getDocumentElement();
		shipEle.setAttribute("OrderName", "ASN-TV-02");
		shipEle.setAttribute("ShipmentNo", "ASNS01");
		shipEle.setAttribute("ConfirmShip", "Y");
		YFCElement sLinesEle = shipEle.createChild("ShipmentLines");
		shipEle.setAttribute("TrackingNo", "T1");
		shipEle.createChild("Extn").setAttribute("VendorShipNode", "N1");	
		YFCElement cLineEle = sLinesEle.createChild("ShipmentLine");
		String pLineNo = "1";
		String itemID = "PenStand";
		String qty = "3";
		String uom = "Each";		
		prepareShipmentLine(cLineEle,pLineNo,itemID,qty,uom);
		System.out.print("Service Input Doc"+shipEle.toString());
		YFCDocument oD1 = invokeYantraService("GpsProcessPOASNMsg", sInDoc);
		System.out.print("Service Output Doc"+oD1.toString());



		YFCDocument inDocgetOrdList = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+pOrdHdrKey+"'/>");
		YFCDocument templateForGetOrdList = YFCDocument.getDocumentFor("<OrderList>" +
				"<Order DocumentType='' SellerOrganizationCode='' OrderHeaderKey='' HoldFlag=''  OrderNo='' EnterpriseCode='' >" +
				"<OrderLines>" +
				"<OrderLine PrimeLineNo='' SubLineNo='' OrderLineKey='' OrderHeaderKey=''> " +
				"<OrderStatuses>" +
				"<OrderStatus OrderReleaseKey='' Status='' StatusQty=''/>" +
				"</OrderStatuses>" +
				"<ChainedFromOrderLine PrimeLineNo='' OrderLineKey='' ChainedFromOrderHeaderKey=''/>" +
				"</OrderLine>" +
				"</OrderLines>" +
				"</Order>" +
				"</OrderList>");		
		YFCDocument outputGetOrdListForComQry =  invokeYantraApi("getOrderList", inDocgetOrdList,templateForGetOrdList);   
		System.out.println("outputGetOrdListForComQry : "+outputGetOrdListForComQry.getString());

		YFCElement comittedStatusEle = XPathUtil.getXPathElement(outputGetOrdListForComQry, "//Order/OrderLines/OrderLine/OrderStatuses/OrderStatus[@Status='3700']");

		if("3.00".equals(comittedStatusEle.getAttribute("StatusQty"))){
			Assert.assertEquals("Pass", "Pass");
		}else{
			Assert.fail();
		}

		System.out.print("======Test Case : committedQtyEqualToShipped - End =========");

	}

	@Test
	public void shipmentNoWithSplitIncrementer(){
		System.out.print("======Test Case : shipmentNoWithSplitIncrementer - Start =========");

		YFCDocument pD = getPoOrdDoc();
		pD.getDocumentElement().setAttribute("OrderName", "ASN-TV-02");
		System.out.println("created PO : "+invokeYantraApi("createOrder", pD));
		YFCDocument sInDoc = YFCDocument.getDocumentFor("<Shipment/>");
		YFCElement shipEle = sInDoc.getDocumentElement();
		shipEle.setAttribute("OrderName", "ASN-TV-02");
		shipEle.setAttribute("ShipmentNo", "ASNS01");
		shipEle.setAttribute("ConfirmShip", "Y");
		YFCElement sLinesEle = shipEle.createChild("ShipmentLines");
		shipEle.setAttribute("TrackingNo", "T1");
		shipEle.createChild("Extn").setAttribute("VendorShipNode", "N1");	
		YFCElement cLineEle = sLinesEle.createChild("ShipmentLine");
		String pLineNo = "1";
		String itemID = "PenStand";
		String qty = "1";
		String uom = "Each";		
		prepareShipmentLine(cLineEle,pLineNo,itemID,qty,uom);
		System.out.print("Service Input Doc"+shipEle.toString());
		YFCDocument oD1 = invokeYantraService("GpsProcessPOASNMsg", sInDoc);
		System.out.print("Service Output Doc"+oD1.toString());

		sInDoc = YFCDocument.getDocumentFor("<Shipment/>");
		shipEle = sInDoc.getDocumentElement();
		shipEle.setAttribute("OrderName", "ASN-TV-02");
		shipEle.setAttribute("ShipmentNo", "ASNS01");
		shipEle.setAttribute("ConfirmShip", "Y");
		shipEle.createChild("Extn").setAttribute("VendorShipNode", "N1");
		shipEle.setAttribute("TrackingNo", "T1");
		sLinesEle = shipEle.createChild("ShipmentLines");
		cLineEle = sLinesEle.createChild("ShipmentLine");
		pLineNo = "1";
		itemID = "PenStand";
		qty = "1";
		uom = "Each";
		prepareShipmentLine(cLineEle,pLineNo,itemID,qty,uom);		
		System.out.print("Service Input Doc"+shipEle.toString());
		YFCDocument oD2 = invokeYantraService("GpsProcessPOASNMsg", sInDoc);
		System.out.print("Service Output Doc"+oD2.toString());

		sInDoc = YFCDocument.getDocumentFor("<Shipment/>");
		shipEle = sInDoc.getDocumentElement();
		shipEle.setAttribute("OrderName", "ASN-TV-02");
		shipEle.setAttribute("ShipmentNo", "ASNS01");
		shipEle.setAttribute("ConfirmShip", "Y");
		shipEle.createChild("Extn").setAttribute("VendorShipNode", "N1");
		shipEle.setAttribute("TrackingNo", "T1");
		sLinesEle = shipEle.createChild("ShipmentLines");
		cLineEle = sLinesEle.createChild("ShipmentLine");
		pLineNo = "1";
		itemID = "PenStand";
		qty = "1";
		uom = "Each";
		prepareShipmentLine(cLineEle,pLineNo,itemID,qty,uom);		
		System.out.print("Service Input Doc"+shipEle.toString());
		oD2 = invokeYantraService("GpsProcessPOASNMsg", sInDoc);
		System.out.print("Service Output Doc"+oD2.toString());
		YFCDocument slD = invokeYantraApi("getShipmentList", YFCDocument.getDocumentFor("<Shipment ShipmentKey='"+oD2.getDocumentElement().getAttribute("ShipmentKey")+"'/>"));
		System.out.print("Service Output Doc"+slD.toString());			
		if(!slD.toString().contains("ASNS01-1")){
			Assert.fail();
		}

		System.out.print("======Test Case : shipmentNoWithSplitIncrementer - End =========");

	}



	private void prepareShipmentLine(YFCElement cLineEle, String pLineNo, String itemID, String qty, String uom) {
		cLineEle.setAttribute("PrimeLineNo", pLineNo);
		cLineEle.setAttribute("ItemID", itemID);
		cLineEle.setAttribute("Quantity", qty);
		cLineEle.setAttribute("UnitOfMeasure", itemID);
	}



	private YFCDocument getPoOrdDoc() {
		YFCDocument ordDoc = YFCDocument.getDocumentFor("<Order DocumentType='0005' EnterpriseCode='TELSTRA_SCLSA'  BuyerOrganizationCode='TELSTRA'   SellerOrganizationCode='TELSTRA' ReceivingNode='TestOrg5' CarrierServiceCode='Ground' SCAC='UPSN'>" +
				"	<OrderLines>" +
				"		<OrderLine PrimeLineNo='1' SubLineNo='1' OrderedQty='10' >" +
				"			<Item ItemID='PenStand' UnitOfMeasure='EACH'/>" +
				"		</OrderLine>" +
				"		<OrderLine PrimeLineNo='2' SubLineNo='1' OrderedQty='10' >" +
				"			<Item ItemID='PenStand' UnitOfMeasure='EACH'/>" +
				"		</OrderLine>" +
				"	</OrderLines>" +
				"</Order> 	"); 
		return ordDoc;
	}

	private YFCDocument getSOOrdDoc() {
		return YFCDocument.getDocumentFor("<Order EnterpriseCode='TELSTRA_SCLSA' InterfaceNo='INT_ODR_1' BuyerOrganizationCode='TELSTRA' SellerOrganizationCode='TELSTRA' EntryType='INTEGRAL_PLUS'  BillToID='TESTCUSTOMER' ShipToID='DAC'   OrderType='MATERIAL_RESERVATION' > " +
				"<PersonInfoBillTo FirstName='Keerthi' AddressLine2='' AddressLine3='' AddressLine4='' City='Bangalore' ZipCode='' State='' DayPhone='' DayFaxNo=''/><OrderLines/></Order>");
	}

}

<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">
<xsl:output method="html"/>

<xsl:template match="/">
<span>Comment added by IBM Users for PO 
<xsl:element name="a">
<xsl:attribute name="href" >
<xsl:text>#!/orderdetails?key=</xsl:text>
<xsl:value-of select="/GpsIBMComNotification/@OrderHeaderKey"/>
<xsl:text>&amp;orderNo=</xsl:text>
<xsl:value-of select="/GpsIBMComNotification/@OrderNo"/>
</xsl:attribute>
<xsl:value-of select="/GpsIBMComNotification/@OrderName"/>
</xsl:element>
 to be recieved at  <xsl:value-of select="/GpsIBMComNotification/@ReceivingNode"/> by user : <xsl:value-of select="/GpsIBMComNotification/@ReqUserID"/>
</span>
</xsl:template>
</xsl:stylesheet> 
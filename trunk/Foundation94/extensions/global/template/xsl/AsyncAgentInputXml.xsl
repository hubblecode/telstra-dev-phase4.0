<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="1.0"
	xmlns:in="http://www.sterlingcommerce.com/ws/SPWPActiveSystemSerialNoServiceType/SPWPActiveSystemSerialNoServiceType/input">
	<xsl:template match="/">
		<xsl:element name="TriggerAgent">
			<xsl:attribute name="CriteriaId">ASYNC_REQ_1</xsl:attribute>
			<xsl:element name="CriteriaAttributes">			
				<xsl:element name="Attribute">
					<xsl:attribute name="Name">InterfaceNo</xsl:attribute>
					<xsl:attribute name="Value">ASN_PO_UPLOAD</xsl:attribute>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
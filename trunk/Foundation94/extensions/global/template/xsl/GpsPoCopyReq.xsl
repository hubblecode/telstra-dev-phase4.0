<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
               version="1.0">
<xsl:output method="html"/>
<xsl:template match="/">
<span>Copy Requested For PO 
<xsl:element name="a">
<xsl:attribute name="href" >
<xsl:text>#!/orderdetails?key=</xsl:text>
<xsl:value-of select="/GpsPoCopyReq/@OrderHeaderKey"/>
<xsl:text>&amp;orderNo=</xsl:text>
<xsl:value-of select="/GpsPoCopyReq/@OrderNo"/>
</xsl:attribute>
<xsl:value-of select="/GpsPoCopyReq/@OrderName"/>
</xsl:element>
 to be recieved at  <xsl:value-of select="/GpsPoCopyReq/@ReceivingNode"/> by user : <xsl:value-of select="/GpsPoCopyReq/@ReqUserID"/>Comments:<xsl:value-of select="/GpsPoCopyReq/@Details"/>
</span>
</xsl:template>
</xsl:stylesheet> 


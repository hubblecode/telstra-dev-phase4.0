<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">
<xsl:output method="html"/>
<xsl:template match="/">
<span>Comment added by vendor for PO
<xsl:element name="a">
<xsl:attribute name="href" >
<xsl:text>#!/orderdetails?key=</xsl:text>
<xsl:value-of select="/GpsVendorComNotification/@OrderHeaderKey"/>
<xsl:text>&amp;orderNo=</xsl:text>
<xsl:value-of select="/GpsVendorComNotification/@OrderNo"/>
</xsl:attribute>
<xsl:value-of select="/GpsVendorComNotification/@OrderName"/>
</xsl:element>
 to be recieved at  <xsl:value-of select="/GpsVendorComNotification/@ReceivingNode"/> by user : <xsl:value-of select="/GpsVendorComNotification/@ReqUserID"/>
</span>
</xsl:template>
</xsl:stylesheet> 




	
var Vue = require('vue')
Vue.config.debug = true
var VueRouter = require('vue-router')
Vue.use(VueRouter)

var NotFound = Vue.extend({
    template: '<p>This page is not found!</p>'
})
Vue.component('notfound',NotFound)



var Index = require('./components/Index.vue')
Vue.component('Index',Index)

var AddressDetails = require('./components/AddressDetails.vue')
Vue.component('addressdetails',AddressDetails)


var GridPagination = require('./components/GridPagination.vue')
Vue.component('gridpagination',GridPagination)


var OrderList = require('./components/OrderList.vue')
Vue.component('orderlist',OrderList )

var OrderDetails = require('./components/OrderDetails.vue')
Vue.component('orderdetails',OrderDetails )

var OrderLineDetails = require('./components/OrderLineDetails.vue')
Vue.component('orderlinedetails', OrderLineDetails)

var CreateAsn = require('./components/CreateAsn.vue')
Vue.component('createasn',CreateAsn)

var Schedule= require('./components/Schedule.vue')
Vue.component('schedule',Schedule)

var FileUpload=require('./components/FileUploadUtil.vue')
Vue.component('fileuploads',FileUpload )

var Shipments=require('./components/Shipments.vue')
Vue.component('shipments',Shipments )

var ShipmentDetails=require('./components/ShipmentDetails.vue')
Vue.component('shipmentdetails',ShipmentDetails)

var AlertConfirmation=require('./components/AlertConfirmation.vue')
Vue.component('alertconfirmation',AlertConfirmation )

var AsnUpload=require('./components/AsnUpload.vue')
Vue.component('asnupload',AsnUpload )

var ShipLocation=require('./components/Locations.vue')
Vue.component('shiplocationlist',ShipLocation )

var ShipLocationDetail=require('./components/LocationDetails.vue')
Vue.component('shiplocationdetails',ShipLocationDetail )


var OrderComments = require('./components/OrderComments.vue')
Vue.component('ordercomments',OrderComments)

var TrackConsignment=require('./components/Consignment.vue')
Vue.component('trackconsignment',TrackConsignment)

var BookFreight=require('./components/BookFreight.vue')
Vue.component('bookfreight',BookFreight)

var MessageBox=require('./components/MessageBox.vue')
Vue.component('messagebox',MessageBox)

var DashAlertSummary=require('./components/DashAlertSummary.vue')
Vue.component('dashalertsummary',DashAlertSummary)

var DashCommonTask=require('./components/DashCommonTask.vue')
Vue.component('dashCommonTask',DashCommonTask)

var DashOpenOrder=require('./components/DashOpenOrder.vue')
Vue.component('dashopenorder',DashOpenOrder)

var DashOrderList=require('./components/DashOrderList.vue')
Vue.component('dashorderlist',DashOrderList)

var DashOrderSummary=require('./components/DashOrderSummary.vue')
Vue.component('dashordersummary',DashOrderSummary)

var DashShipmentList=require('./components/DashShipmentList.vue')
Vue.component('dashshipmentlist',DashShipmentList)

var DashShipmentSummary=require('./components/DashShipmentSummary.vue')
Vue.component('dashshipmentsummary',DashShipmentSummary)

var DashWatchList=require('./components/DashWatchList.vue')
Vue.component('dashwatchlist',DashWatchList)

var AlertDetails = require('./components/AlertDetails.vue')
Vue.component('alertdetails',AlertDetails )

var AssignAlertToUser = require('./components/AssignAlertToUser.vue')
Vue.component('assignalerttouser',AssignAlertToUser )

var ErrorBox = require('./components/ErrorBox.vue')
Vue.component('errorbox',ErrorBox)

var SearchOrder = require('./components/SearchOrder.vue')
Vue.component('searchorder',SearchOrder )

var AddressLocationNonEditable = require('./components/AddressLocation.vue')
Vue.component('noneditableaddress',AddressLocationNonEditable)

var printConsignmentNote = require('./components/printConsignmentNote.vue')
Vue.component('printconsignmentnote',printConsignmentNote)

var printConsignmentNoteA5 = require('./components/printConsignmentNoteA5.vue')
Vue.component('printconsignmentnoteA5',printConsignmentNoteA5)

var JobsList = require('./components/JobsList.vue')
Vue.component('jobslist',JobsList)

var notification = require('./components/Notification.vue')
Vue.component('notification',notification)  

var poamendment = require('./components/PoAmendment.vue')
Vue.component('poamendment',poamendment)


require('./directives/datepicker.js')
require('./filters/datefilter.js')
require('./filters/gridfilter.js')
require('./filters/numberfilter.js')
require('./filters/integerfilter.js')
require('./filters/quantityfilter.js')
require('./filters/gridsorter.js')
require('./filters/statusfilter.js')

var App = Vue.extend({})
var router = new VueRouter()
var Logger = require('./utils/loggerUtils.js')
router.beforeEach(function (transition) {
	 if(transition.from.router == null){
        //First Page   Ajax Api Call to made
        var loginput=Logger.getloggedEntries()  
        var logger = ''
        Object.keys(loginput).forEach(function (key) {
          logger = logger + loginput[key] + '\n';
        });
        var input = {'LogVP':logger}
        $.ajax({
            url: window.AppInfo.contextroot+'/api/uilogger',
            type: 'POST',
            data: input,
            cache: false,
            dataType: 'text',
            success: function(data, textStatus, jqXHR)
            { },
            error: function(jqXHR, textStatus, errorThrown)
            { }
        });
         Logger.ClearloggedEntries()
    }else {
        var loginput=Logger.getloggedEntries()  
        var logger = ''
        Object.keys(loginput).forEach(function (key) {
            logger = logger + loginput[key] + '\n';
        });
        var input = {'Log':logger}
        //console.log(logger)
        $.ajax({
            url: window.AppInfo.contextroot+'/api/uilogger',
            type: 'POST',
            data: input,
            cache: false,
            dataType: 'text',
            success: function(data, textStatus, jqXHR)
            { },
            error: function(jqXHR, textStatus, errorThrown)
            { }
        });
         Logger.ClearloggedEntries()
    }
    
    var Autoclear = transition.to.autoclear;
    if(!((Autoclear == undefined) || (Autoclear == null) || !(Autoclear))) {
        //window.Xhistory = [];
        sessionStorage.removeItem('vpbreadcrumbContext');
    }
    
    var skipbreadcrumb = transition.from.skipbreadcrumb
    if(!((skipbreadcrumb == undefined) || (skipbreadcrumb == null) || !(skipbreadcrumb))) {
        //window.Xhistory = [];
        //sessionStorage.removeItem('vpbreadcrumbContext');
        if(sessionStorage.getItem("vpbreadcrumbContext")) {
            var currentbcArr = JSON.parse(sessionStorage.getItem("vpbreadcrumbContext"));
            //Remove the last element from the Breadcrumb array
            currentbcArr.splice(-1,1);
            var vpbcContext = JSON.stringify(currentbcArr)
            sessionStorage.setItem("vpbreadcrumbContext",vpbcContext);
            
        }
        
    }

    
    transition.next()
});

router.map({

	'/home': {
    	component: Index,
		Description: 'Dashboard',
        id:'home',
        autoclear: true
    },

    '/orderlist': {
        name: 'OrderList',
    	component:OrderList,
		Description: 'Orders',
        id:'orderlist',
        autoclear: true
    },
     '/orderdetails': {
        name: 'OrderDetails',
    	component:OrderDetails,
		Description: 'Order Details',
        id:'orderdtl'
    },	
	'/fileuploads':{
	   component:FileUpload
	}, 
    '/shipments':{
		component:Shipments,
		Description: 'Shipments',
        id:'shipments',
  	},
    '/shipmentdetails':{
		component:ShipmentDetails,
		Description: 'Shipment Details',
        id:'ShipmentDetails'
     },
    '/asnupload':{
	  component:AsnUpload
	},
	'/shiplocationlist':{
		component:ShipLocation,
		Description: 'Shipping Locations',
        id:'shiplocations',
        autoclear: true
	},
	'/shiplocationdetails':{
		component:ShipLocationDetail,
		Description: 'Shipping Locations Details',
        id:'shiplocationsdtl',
        autoclear: true
	},
    '/createasn':{
	  component:CreateAsn,
      Description: 'Create ASN',
      id:'CreateAsn',
      skipbreadcrumb: true    
	},
	'/trackconsignment':{
        name: 'trackconsignment',
		component:TrackConsignment,
		Description: 'Consignment Tracking',
        id:'consignment'
     },
	'/bookfreight':{
        name: 'bookfreight',
		component:BookFreight,
		Description: 'Book Freight',
        id:'bookfreight',
     },
    '/alertdetails': {
    	component:AlertDetails,
		Description: 'Alert Details',
        id:'alertdtl',
    },
	'/printconsignmentnote':{
        name: ' printconsignmentnote',
        component:printConsignmentNote,
        Description: 'Print Consignment',
        id:'printconsignmentnote',
        skipbreadcrumb: true
      },
        '/printConsignmentNoteA5':{
        name: ' printconsignmentnoteA5',
        component:printConsignmentNoteA5,
        Description: 'Print ConsignmentA5',
        id:'printconsignmentnoteA5',
        skipbreadcrumb: true
      },
     '/jobslist':{
        name: 'jobslist',
        component:JobsList,
        Description: 'Job Requests',
        id:'jobslist',
        skipbreadcrumb: true,
        autoclear: true
      },
       '/*any': {
    	component: NotFound
    }
})

Vue.directive('breadcrumb',{
    bind: function(){
        var HomePath = '/home';
       // debugger;
        var windowSession = [];
       /*if((window.Xhistory == undefined) || (window.Xhistory == null)) {
            window.Xhistory = [];
           
       }*/
         if(!(sessionStorage.vpbreadcrumbContext == undefined))
            windowSession = JSON.parse(sessionStorage.vpbreadcrumbContext);
             
        
        this.el.innerHTML = '<ol class="breadcrumb cusbreadcrumb">'+generateTemplate('',windowSession, router._currentRoute, HomePath)+'</ol>';
        
    },
    method: [
        generateTemplate = function(template ,historyNav, CurrentRoute, HomePath){
            
            maintainCapacity(historyNav, CurrentRoute);
            var historyArraylen = historyNav.length;
            var Dashboardhandler =  router._recognizer.recognize(HomePath)[0].handler;
            template = '<li><a href="#!'+HomePath+'">'+Dashboardhandler.Description+'</a></li>';
            for (var history in historyNav) {
                var handlerObj = historyNav[history];
                template = template + manageTemplate(handlerObj, history, historyArraylen);
            }

            return template;
            
        },
        maintainCapacity = function(historyNav, CurrentRoute){
            
            clearHistoryObject(CurrentRoute, historyNav);
            
            if(historyNav.length > 3){
                historyNav.splice(0,1);
                maintainCapacity(historyNav,CurrentRoute);
            }
                
        },
        manageTemplate = function(history, currentIndex, totalIndex){
            
           return genlistTemplate(paramMatchObject(history,[]), history, currentIndex, totalIndex);
            
        },
        
        getParamValue = function(queryParam, key){
            
            var key_values = queryParam.split('&');
            var key_str = key+'=';
            var paramvalue = '';
            for (var key_value in key_values) {
                var keyValueStr = key_values[key_value];
                if((keyValueStr.indexOf(key_str))==0){
                    paramvalue = keyValueStr.split(key_str)[1];
                }
                    
            }
            return paramvalue;
            
        },
        
        paramMatchObject = function(currentHandlerObj, exceptional_list){
            
            var Key_Array = Object.keys(currentHandlerObj.query);
            var queryParam = ''; 
            if(Key_Array.length > 1){
               
               for (i = 0; i < Key_Array.length; i++) { 
                   var key = Key_Array[i];
                  if((key != 'hubblecsrf') && (exceptional_list.indexOf(key) == -1)){
                     var param = currentHandlerObj.query[key];
                      if(queryParam != '')
                          queryParam = queryParam + '&';
                     queryParam = queryParam + key + '=' + param;
                  }
                }
                
            }
            
            return queryParam;
        },
        
        clearHistoryObject = function(currentRouter, routerArraylist){
            //debugger;
            var match = false;
            var clearCmd = getParamValue(paramMatchObject(currentRouter,[]),'autoclear');
            var matchedindex;
            for (var router in routerArraylist) {
                var routerObj = routerArraylist[router];
                if(currentRouter.id == routerObj.id){ 
                    match = true;
                    matchedindex = router;
                }
            }
            
            if(clearCmd){
               routerArraylist.length = 0; 
               routerArraylist.push(currentRouter);
                
            }else if(match){
                var len =routerArraylist.length;
                routerArraylist.splice(parseInt(matchedindex)+1, parseInt(len));
            }
            else
                routerArraylist.push(currentRouter);
            sessionStorage.vpbreadcrumbContext = JSON.stringify(routerArraylist);
           
        },
        
        genlistTemplate = function(queryParam, handler, currentIndex, totalIndex){
            
            var path = handler.path;
            var massagedpath = '/'+path.split('?')[0].split('/')[1];
            var template = '';
            var description = '';
            var aliasEnabled = handler.AliasKey;
            //debugger;
            if(!((aliasEnabled == undefined) || aliasEnabled == null || aliasEnabled == ''))
                description = getParamValue(paramMatchObject(handler,[]), aliasEnabled);
            else
                description = handler.Description;
            
            if(currentIndex == (totalIndex-1))
                template = template + '<li>'+description+'</li>';
            else{
            
                if(queryParam != '')
                   template = template + '<li><a href="#!'+massagedpath+'?'+queryParam+'">'+description+'</a></li>';
                else 
                   template = template + '<li><a href="#!'+massagedpath+'">'+description+'</a></li>';
                
            }
            
            return template;
        },
      getParentComponent = function (template, path, CallerPath){
             var handler =  router._recognizer.recognize(path)[0].handler;
             if((handler.parent == undefined) || (handler.parent == null))
                 template = template + UpdateTemplate(CallerPath, handler);
             else{
                 template = getParentComponent(template, handler.parent, CallerPath);
                 template = template + UpdateTemplate(CallerPath, handler);
             }
           
            return template;
         },
        UpdateTemplate = function(CallerPath, handler){
          
            var template;
            
            if(handler.path == CallerPath){
                template = '<li>'+handler.Description+'</li>';
            }else
                template = '<li><a href="#!'+handler.path+'">'+handler.Description+'</a></li>';
            
            return template;
        },
        
    ]
    
    
});

$.ajaxPrefilter(function (options, originalOptions, jqXHR) {
  jqXHR.setRequestHeader('hubblecsrfheader', window.AppInfo.hubblecsrftoken);
});

router.start(App, '#app')


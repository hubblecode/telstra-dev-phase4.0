package com.gps.hubble.shipment.api;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.json.JSONArray;
import org.apache.commons.json.JSONException;
import org.apache.commons.json.JSONObject;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.utils.GpsJSONXMLUtil;
import com.sterlingcommerce.woodstock.util.Base64;
import com.yantra.custom.dbi.GPS_Order_Export_Vw;
import com.yantra.custom.dbi.GPS_Shipment_Export_Vw;
import com.yantra.shared.dbclasses.GPS_Order_Export_VwDBHome;
import com.yantra.shared.dbclasses.GPS_Shipment_Export_VwDBHome;
import com.yantra.shared.ycp.YFSContext;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dblayer.PLTQueryBuilder;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSException;

public class GpsShipmentExport extends AbstractCustomApi {

	private static YFCLogCategory cat = YFCLogCategory.instance(GpsShipmentExport.class);
	private static final String ATTR_LABEL = "Label";
	private static final String ATTR_BINDING = "Binding";
	private static final String ATTR_NAME = "Name";
	
	@Override
	public YFCDocument invoke(YFCDocument inDoc) throws YFSException {
		YFCElement inElem = inDoc.getDocumentElement();
		String searchCriteria =	inElem.getAttribute("ExportCriteria");
		String decodedCriteria = new String(Base64.decode(searchCriteria.getBytes()));
		String attrList = inElem.getAttribute("ExportColumns");
		
		YFCDocument outDoc = YFCDocument.createDocument("ExportData");
		//YFCDocument outDoc = YFCDocument.createDocument("OrderExportList");
		YFCElement rootElem = outDoc.getDocumentElement();
		YFCElement columnListElem = rootElem.createChild("ColumnList");
		YFCElement dataListElem = rootElem.createChild("DataList");
		
		YFCDocument templateDoc = YFCDocument.createDocument(GPS_Shipment_Export_VwDBHome.getInstance().getXMLName());
		YFCElement templateElem = templateDoc.getDocumentElement();
		Map<String,JSONObject> columnMapping = new LinkedHashMap<>();
		Set<String> dateColumns = new HashSet<>();
		try{	
			YFSContext ctx = getServiceInvoker().getContext();
			PLTQueryBuilder pltQry = new PLTQueryBuilder();
			JSONObject criteriaJSON = new JSONObject(decodedCriteria);
			JSONObject searchCriteriaObj = (JSONObject)criteriaJSON.get("SearchCriteria");
			JSONObject mappingListObj = (JSONObject)criteriaJSON.get("MappingList");
			YFCDocument criteriaDoc = GpsJSONXMLUtil.getXmlFromJSON(searchCriteriaObj,"Shipment");
			YFCDocument shpListtemplateDoc = YFCDocument.getDocumentFor("<ShipmentList><Order ShipmentKey=''/></ShipmentList>");
			
			YFCDocument shpListOutDoc = getServiceInvoker().invokeYantraApi(TelstraConstants.API_GET_SHIPMENT_LIST, criteriaDoc,shpListtemplateDoc);
			YFCElement shpListElem = shpListOutDoc.getDocumentElement();
			List<String> shipmentKeys=new ArrayList<>();
			for(Iterator<YFCElement> eleIter= shpListElem.getChildren();eleIter.hasNext();) {
				YFCElement shpElem = eleIter.next();
				shipmentKeys.add(shpElem.getAttribute(TelstraConstants.SHIPMENT_KEY));
			}
			if(shipmentKeys.isEmpty()) {
				return outDoc;
			}
			if(mappingListObj != null) {
				JSONArray mappingArray = (JSONArray)mappingListObj.get("Mapping");
				for(int i=0;i<mappingArray.length();i++) {
					JSONObject mappingObject=(JSONObject)mappingArray.get(i);
					String columnName = (String)mappingObject.get("Binding");
					String labelName = (String)mappingObject.get("Label");
					String isDate = "";
					if(mappingObject.containsKey("IsDate")) {
						isDate = (String)mappingObject.get("IsDate");
					}
					if(YFCUtils.isVoid(labelName)) {
						labelName=columnName;
					}
					if(!YFCUtils.isVoid(isDate) && "Y".equals(isDate)) {
						dateColumns.add(columnName);
					}
					columnMapping.put(columnName,mappingObject);
					
				}

			}

			/*YFCElement mappingListElem = criteriaElem.getChildElement("MappingList");
			for(Iterator<YFCElement> mappingIter=mappingListElem.getChildren();mappingIter.hasNext();) {
				YFCElement mappingElem = mappingIter.next();
				String columnName = mappingElem.getAttribute("Binding");
				String labelName = mappingElem.getAttribute("Label");
				if(YFCUtils.isVoid(labelName)) {
					labelName=columnName;
				}
				columnMapping.put(columnName,labelName);
			}*/
			//Removing the mapping before invoking the query
			//criteriaElem.removeChild(mappingListElem);
			String [] shpArr=shipmentKeys.toArray(new String[0]);
			
			
			//String whereClause = GPS_Order_Export_VwDBHome.getInstance().getWhereClause(criteriaElem,GPS_Order_Export_VwDBHome.getInstance().getTableCode());
			if(shpArr.length > 0) {
				pltQry.appendWHERE();
				//pltQry.append(whereClause);
				pltQry.appendStringForIN(GPS_Shipment_Export_VwDBHome.getInstance(), "SHIPMENT_KEY", shpArr);
				cat.debug("The Generated Query is "+pltQry.getReadableWhereClause());				
			}
			if(YFCUtils.isVoid(attrList)) {
				attrList = getProperty("AttributeList");
			}
			if(!columnMapping.isEmpty()) {
				for(Iterator<String> mapIter=columnMapping.keySet().iterator();mapIter.hasNext(); ) {
					String colName= mapIter.next();
					//String colValue = columnMapping.get(colName);
					JSONObject colObj = columnMapping.get(colName);
					YFCElement columnElem = columnListElem.createChild("Column");
					columnElem.setAttribute("Name", colName);	
					columnElem.setAttribute("Label", (String)colObj.getString("Label"));
					if(colObj.containsKey("IsDate")) {
						columnElem.setAttribute("IsDate", (String)colObj.getString("IsDate"));
					}
					if(colObj.containsKey("EscapeChars")) {
						columnElem.setAttribute("EscapeChars", (String)colObj.getString("EscapeChars"));
					}
										
					/*if(dateColumns.contains(colName)) {
						columnElem.setAttribute("IsDate", "Y");
					}*/
					templateElem.setAttribute(colName, "");
				}
			} else if(!YFCUtils.isVoid(attrList)) {
				String [] attrArray = attrList.split(",");
				for(String attrName: attrArray) {
					YFCElement columnElem = columnListElem.createChild("Column");
					String [] attrLabels = attrName.split(":");
					if(attrLabels.length > 2) {
						columnElem.setAttribute("Name", attrLabels[0]);
						columnElem.setAttribute("Label", attrLabels[1]);
						columnElem.setAttribute("IsDate", attrLabels[2]);
					} else if(attrLabels.length > 1) {
						columnElem.setAttribute("Name", attrLabels[0]);
						columnElem.setAttribute("Label", attrLabels[1]);
					} else {
						columnElem.setAttribute("Name", attrLabels[0]);
						columnElem.setAttribute("Label", attrLabels[0]);
					}
					templateElem.setAttribute(attrLabels[0], "");
				}
			}else {
				Set<String> attrNames = GPS_Shipment_Export_Vw.getXMLAttributeNames();
				for(String attrName: attrNames) {
					YFCElement columnElem = columnListElem.createChild("Column");
					columnElem.setAttribute("Name", attrName);
					columnElem.setAttribute("Label", attrName);
					if(dateColumns.contains(attrName)) {
						columnElem.setAttribute("IsDate", "Y");
					}					
					templateElem.setAttribute(attrName, "");
				}
			}
			/*if(!YFCUtils.isVoid(attrList)) {
				String[] attrArr = attrList.split(",");
				for(String attrName: attrArr) {
					YFCElement columnElem = columnListElem.createChild("Column");
					if(columnMapping.containsKey(attrName)) {
						columnElem.setAttribute("Name", attrName);	
						columnElem.setAttribute("Label", columnMapping.get(attrName));
						templateElem.setAttribute(attrName, "");
					}

				}
			} else {
				Set<String> attrNames = GPS_Order_Export_Vw.getXMLAttributeNames();
				for(String attrName: attrNames) {
					YFCElement columnElem = columnListElem.createChild("Column");
					if(columnMapping.containsKey(attrName)) {
						columnElem.setAttribute("Name", attrName);
						columnElem.setAttribute("Label", columnMapping.get(attrName));
						templateElem.setAttribute(attrName, "");
					}					
				}
			}*/
			pltQry.appendOrderBy("ORDER BY SHIPMENT_KEY, RECORD_TYPE DESC");
			List<GPS_Shipment_Export_Vw> resultList = GPS_Shipment_Export_VwDBHome.getInstance().listWithWhereUnCommited(ctx, pltQry, Integer.MAX_VALUE);
			YFCElement lineElem = null;
			for(Iterator<GPS_Shipment_Export_Vw> iter=resultList.iterator();iter.hasNext();) {
				GPS_Shipment_Export_Vw row = iter.next();
				YFCElement rowElem = row.getXML(outDoc, templateElem,"Data");
				if("LINE".equals(rowElem.getAttribute("RecordType"))) {
					lineElem = rowElem;
				}else if("CONTAINER".equals(rowElem.getAttribute("RecordType"))) {
					if(lineElem != null) {
						rowElem.setAttribute("OrderNo", lineElem.getAttribute("OrderNo"));
						rowElem.setAttribute("SourceID", lineElem.getAttribute("SourceID"));
					}
				}
				dataListElem.appendChild(rowElem);
			}
		}catch(JSONException je) {
			je.printStackTrace();
			throw new YFSException("JSON Parse Error");
		}
		return outDoc;
	}

}


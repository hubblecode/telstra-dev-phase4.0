package com.gps.hubble.shipment.api;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSException;

/**
 * @author Prateek
 *
 */
public class GpsUpdateShippedOutSerial extends AbstractCustomApi{
	
	private static YFCLogCategory logger = YFCLogCategory.instance(GpsUpdateShippedOutSerial.class);
	
	/**
	 * 
	 */
	@Override
	public YFCDocument invoke(YFCDocument yfcInDoc) throws YFSException {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", yfcInDoc);
		YFCDocument yfcDocShippedSerialNum = (YFCDocument) getTxnObject("yfcDocShipmentSerialNum");
		if(!YFCCommon.isVoid(yfcDocShippedSerialNum)){
			updateShippedOutSerial(yfcDocShippedSerialNum);		
		}
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", yfcInDoc);

		return yfcInDoc;
	}
	
	/**
	 * 
	 * @param docShipOrderIp
	 */
	private void updateShippedOutSerial(YFCDocument docShipOrderIp){

		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "updateShippedOutSerial", docShipOrderIp);
		String sShipNode =  docShipOrderIp.getDocumentElement().getAttribute(TelstraConstants.SHIP_NODE);
		YFCNodeList<YFCElement> yfcNlShipmentLine = docShipOrderIp.getElementsByTagName(TelstraConstants.SHIPMENT_LINE);
		for(YFCElement yfcEleShipmentLine :  yfcNlShipmentLine){

			YFCNodeList<YFCElement> yfcNlShipmentSerialNumber = yfcEleShipmentLine.getElementsByTagName(TelstraConstants.SHIPMENT_SERIAL_NUMBER);
			if(yfcNlShipmentSerialNumber.getLength()>0){

				String sItemId = yfcEleShipmentLine.getAttribute(TelstraConstants.ITEM_ID);
//				String sUnitOfMeasure = yfcEleShipmentLine.getAttribute(TelstraConstants.UNIT_OF_MEASURE);
				if(!(YFCCommon.isStringVoid(sItemId))){
					String sInventoryItemKey = getInventoryItemKey(sItemId);

					if(!YFCCommon.isStringVoid(sInventoryItemKey)){
						for(YFCElement yfcEleShipmentTagSerial :  yfcNlShipmentSerialNumber){
							String sSerialNo = yfcEleShipmentTagSerial.getAttribute(TelstraConstants.SERIAL_NO);
							if(!YFCCommon.isStringVoid(sSerialNo)){
								manageSerialNumber(sInventoryItemKey, sSerialNo, sShipNode);
							}
						}
					}else{
						LoggerUtil.errorLog("Inventory item doesn't exist for the item ", logger, sItemId);
					}			
				}
			}
		}
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "updateShippedOutSerial", docShipOrderIp);

	}

	/**
	 * 
	 * @param sInventoryItemKey
	 * @param sSerialNo
	 * @param sShipNode
	 */
	private void manageSerialNumber(String sInventoryItemKey, String sSerialNo, String sShipNode) {

		YFCDocument yfcDocManageSerial = YFCDocument.getDocumentFor("<SerialNumber SerialNo='' InventoryItemKey='' AtNode='' ShipNode=''/>");
		YFCElement yfcEleManageSerialRoot = yfcDocManageSerial.getDocumentElement();
 		yfcEleManageSerialRoot.setAttribute(TelstraConstants.SERIAL_NO, sSerialNo);
		yfcEleManageSerialRoot.setAttribute(TelstraConstants.INVENTORY_ITEM_KEY, sInventoryItemKey);
		yfcEleManageSerialRoot.setAttribute(TelstraConstants.SHIP_NODE, sShipNode);
		yfcEleManageSerialRoot.setAttribute(TelstraConstants.AT_NODE, TelstraConstants.NO);
		LoggerUtil.verboseLog("GpsUpdateShippedOutSerial::manageSerialNumber::yfcDocManageSerial", logger, yfcDocManageSerial);

		
		YFCDocument yfcDocGetSerialNumList = YFCDocument.getDocumentFor("<SerialNum InventoryItemKey=''  SerialNo='' ShipNode=''/>");
		YFCElement yfcEleGetSerialNumListRoot = yfcDocGetSerialNumList.getDocumentElement();
		yfcEleGetSerialNumListRoot.setAttribute(TelstraConstants.SERIAL_NO, sSerialNo);
		yfcEleGetSerialNumListRoot.setAttribute(TelstraConstants.INVENTORY_ITEM_KEY, sInventoryItemKey);
		yfcEleGetSerialNumListRoot.setAttribute(TelstraConstants.SHIP_NODE, sShipNode);		

		YFCDocument yfcDocGetSerialNumberListOp = invokeYantraService("GpsGetSerialNumList", yfcDocGetSerialNumList);

		LoggerUtil.verboseLog("GpsUpdateShippedOutSerial::manageSerialNumber::yfcDocGetSerialNumberListOp", logger, yfcDocGetSerialNumberListOp);

		if (!YFCElement.isNull(yfcDocGetSerialNumberListOp.getElementsByTagName(TelstraConstants.SERIAL_NUMBER).item(0))) {
			/* Update the Attributes of the Inventory Item */
			invokeYantraService("GpsChangeSerialNum", yfcDocManageSerial);
		} else {
			/* Create the Serial of the Inventory Item */
			invokeYantraService("GpsCreateSerialNum", yfcDocManageSerial);
		}


	}

	/**
	 * This method returns the inventory item key
	 * @param sItemId
	 * @param sUnitOfMeasure
	 * @return
	 */
	private String getInventoryItemKey(String sItemId) {

		YFCDocument yfcDocGetInventoryItemListIp = YFCDocument
				.getDocumentFor("<InventoryItem InventoryOrganizationCode='' ItemID=''/>");
		YFCElement yfcEleInventoryItem = yfcDocGetInventoryItemListIp.getDocumentElement();
		yfcEleInventoryItem.setAttribute(TelstraConstants.INVENTORY_ORGANIZATION_CODE, TelstraConstants.ORG_TELSTRA);
		yfcEleInventoryItem.setAttribute(TelstraConstants.ITEM_ID, sItemId);
		//yfcEleInventoryItem.setAttribute(TelstraConstants.UNIT_OF_MEASURE, sUnitOfMeasure);

		LoggerUtil.verboseLog("GpsUpdateShippedOutSerial::getInventoryItemKey::yfcDocGetInventoryItemListIp", logger,
				yfcDocGetInventoryItemListIp);

		YFCDocument yfcDocGetInventoryItemListTemp = YFCDocument.getDocumentFor(
				"<InventoryList TotalInventoryItemList=''> <InventoryItem InventoryItemKey='' InventoryOrganizationCode='' ItemID=''  UnitOfMeasure=''/> </InventoryList>");

		YFCDocument yfcDocGetInventoryItemListOp = invokeYantraApi(TelstraConstants.GET_INVENTORY_ITEM_LIST,
				yfcDocGetInventoryItemListIp, yfcDocGetInventoryItemListTemp);
		LoggerUtil.verboseLog("GpsUpdateShippedOutSerial::getInventoryItemKey::yfcDocGetInventoryItemListOp", logger,
				yfcDocGetInventoryItemListOp);

		YFCElement yfcEleInventoryItemOp = yfcDocGetInventoryItemListOp
				.getElementsByTagName(TelstraConstants.INVENTORY_ITEM).item(0);
		String sInventoryItemKey = TelstraConstants.BLANK;
		if (!YFCCommon.isVoid(yfcEleInventoryItemOp)) {
			sInventoryItemKey = yfcEleInventoryItemOp.getAttribute(TelstraConstants.INVENTORY_ITEM_KEY);
		}
		return sInventoryItemKey;
	}
}

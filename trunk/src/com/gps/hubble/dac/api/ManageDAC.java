/***********************************************************************************************
 * File	Name		: ManageDAC.java
 *
 * Description		: This class is called for creating, updating the DAC. 
 * 
 * Modification	Log	:
 * ---------------------------------------------------------------------------------------------
 * Ver #	Date			Author					Modification


 * ---------------------------------------------------------------------------------------------
 * 1.0												Initial	Version
 * 1.1		08/03/17		Prateek Kumar			If DAC comes with status as 30, directly call manage customer api. 
 * 1.2		17/04/17		Prateek Kumar			[HUB-8701]If incoming and existing customer key is same, call manage customer api.
 * 1.3      29/05/17        Keerthi Yadav			HUB-9157: Error when updating DAC
 * 1.4 		12/06/17		Keerthi Yadav			HUB-9191: Unable to add a new DAC in the ship to address for an existing transfer order.
 * ---------------------------------------------------------------------------------------------
 * 
 */

package com.gps.hubble.dac.api;

import org.apache.commons.lang.StringUtils;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;

/**
 * Custom API to create Customer and remove Duplicate customerKey if they exist
 * 
 * @author Keerthi Yadav
 * @version 1.0
 *
 * Extends AbstractCustomApi Class
 */


/**
 * 
 * @author Keerthi
 * Custom API to create Customer and remove Duplicate customerKey if they exist
 */

public class ManageDAC extends AbstractCustomApi {
	private static YFCLogCategory logger = YFCLogCategory.instance(ManageDAC.class);

	/**
	 * 
	 */
	public YFCDocument invoke(YFCDocument inXml) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(),"invoke", inXml);
		YFCDocument outXml =validateandcreateCustomer(inXml);
		LoggerUtil.endComponentLog(logger, this.getClass().getName(),"invoke", outXml);
		return outXml;
	}

	/**
	 * 
	 * @param inXml
	 * @return outxml
	 */

	private YFCDocument validateandcreateCustomer(YFCDocument inXml) {
		YFCDocument outxml = null;
		YFCElement yfcEleInXMlRoot = inXml.getDocumentElement();
		String sCustomerID = yfcEleInXMlRoot.getAttribute(TelstraConstants.CUSTOMER_ID,"");
		logger.verbose("The CustomerID passed is " + sCustomerID);

		if(!YFCObject.isVoid(sCustomerID)){
			logger.verbose("The CustomerID passed is not null");

			String sStatus = yfcEleInXMlRoot.getAttribute(TelstraConstants.STATUS);

			YFCDocument docCustomerListinXml = YFCDocument.getDocumentFor("<Customer CustomerID=''></Customer>");
			//HUB-6076 : Begin
			docCustomerListinXml.getDocumentElement().setAttribute("CustomerID", sCustomerID);
			//HUB-6076 : End
			YFCDocument docCustomerListtempXml = YFCDocument.getDocumentFor(
					"<CustomerList><Customer CustomerKey='' CustomerID='' ExternalCustomerID=''> <Extn NewDac='' CustomerNodeType=''/> </Customer> </CustomerList>");
			YFCDocument docCustomerListoutXml = invokeYantraApi("getCustomerList",docCustomerListinXml, docCustomerListtempXml);
			YFCElement eleCustomer=docCustomerListoutXml.getElementsByTagName(TelstraConstants.CUSTOMER).item(0);
			logger.verbose("Output from getCustomerList output : " + docCustomerListoutXml.getDocumentElement().toString());

			String sNewDac = TelstraConstants.YES;
			if(!YFCObject.isVoid(eleCustomer) && !YFCObject.isVoid(yfcEleInXMlRoot.getAttribute(TelstraConstants.CUSTOMER_KEY))){
				logger.verbose("The customerkey is not null");

				YFCElement yfcEleExtn = XPathUtil.getXPathElement(docCustomerListoutXml, "//Customer/Extn");
				/*
				 * Fetching the newDac flag value so that it can be copied into the new updated DAC
				 */
				sNewDac = yfcEleExtn.getAttribute(TelstraConstants.NEW_DAC, TelstraConstants.NO);
				/*
				 * if the CT creation is happening for the first time, ser customer node type as CT
				 */
				String sCustomerNodeType = yfcEleExtn.getAttribute(TelstraConstants.CUSTOMER_NODE_TYPE);
				if(!YFCCommon.isStringVoid(sCustomerNodeType)){
					YFCElement yfcEleExtnIp = XPathUtil.getXPathElement(inXml, "//Customer/Extn");
					if(YFCCommon.isVoid(yfcEleExtnIp)){
						yfcEleExtnIp = yfcEleInXMlRoot.createChild(TelstraConstants.EXTN);						
					}	
					yfcEleExtnIp.setAttribute(TelstraConstants.CUSTOMER_NODE_TYPE, sCustomerNodeType);
				}
				/*if(YFCCommon.isStringVoid(sCustomerNodeType) && sCustomerID.startsWith("V")){
					YFCElement yfcEleExtnIp = XPathUtil.getXPathElement(inXml, "//Customer/Extn");
					if(YFCCommon.isVoid(yfcEleExtnIp)){
						yfcEleExtnIp = yfcEleInXMlRoot.createChild(TelstraConstants.EXTN);						
					}	
					yfcEleExtnIp.setAttribute(TelstraConstants.CUSTOMER_NODE_TYPE, "CT");
				}*/
				String sCustomerKey = inXml.getDocumentElement().getAttribute(TelstraConstants.CUSTOMER_KEY,"0");
				int iCustomerKey=0;
				logger.verbose("The new customerkey is " + sCustomerKey);
				if(StringUtils.isNumeric(sCustomerKey)){
					//When the Customer is created from the create order interface, the CusomerKey is 24 digits.
					//As per the INT_DAC_1 spec sheet, CustomerKey, which is the Address Number can be a maximum of 10 characters
					//The below logic will only be invoked if the Customerkey is less than equal to 10 chars.
					if(sCustomerKey.length()<=10){
						logger.verbose("The new customerKey is a numeric value");
						iCustomerKey=Integer.parseInt(sCustomerKey);
					}
				}
				String sExistingCustomerKey=eleCustomer.getAttribute(TelstraConstants.CUSTOMER_KEY, "");
				logger.verbose("The existing customerkey is " + sExistingCustomerKey);
				int iExistingCustomerKey=0;
				if(StringUtils.isNumeric(sExistingCustomerKey)){
					//When the Customer is created from the create order interface, the CusomerKey is 24 digits.
					//As per the INT_DAC_1 spec sheet, CustomerKey, which is the Address Number can be a maximum of 10 characters
					//The below logic will only be invoked if the Customerkey is less than equal to 10 chars; else the CustomerKey will be overriden with the new CustomerKey
					if(sExistingCustomerKey.length()<=10){
						logger.verbose("The existing customerKey is a numeric value and can be compared with the new customerKey");
						iExistingCustomerKey=Integer.parseInt(sExistingCustomerKey);
					}
				}

				/* This condition takes care of the scenario where the incoming customerKey is less than the 
				 * existing customerKey then no updates must be done */

				if(!sExistingCustomerKey.equals(sCustomerKey) && iCustomerKey <= iExistingCustomerKey){
					logger.verbose("The existing CustmerKey is greater than or equal (if both the keys are non-numric) to "
							+ "the new CustomerKey; "
							+ "and hence the customer does not have to be updated.");
					return null;
				}

				//HUB-9157[END]

				else if(iCustomerKey > iExistingCustomerKey){
					/*
					 * If new DAC comes with inactive status, then return back from the code without updating the DAC 
					 */
					if(TelstraConstants.CUSTOMER_INACTIVE_STATUS.equals(sStatus)){
						return inXml;
					}
					logger.verbose("Either the new CustomerKey is greater than the old CustomerKey, "
							+ "or, the old CustomerKey was non-numric");
					logger.verbose("In either case, the customer will be updated with teh newly passed info");
					//if already existing customer has external customer id and incoming feed don't have it, retain the external customer id 
					String sIncomingExternalCustID = inXml.getDocumentElement().getAttribute(TelstraConstants.EXTERNAL_CUSTOMER_ID,"");
					logger.verbose("The ExternalCustomerId from the new data is:" + sIncomingExternalCustID);
					if(YFCCommon.isStringVoid(sIncomingExternalCustID)){
						logger.verbose("The ExternalCustomerId from the new data is null");
						String sExternalCustomerID = eleCustomer.getAttribute(TelstraConstants.EXTERNAL_CUSTOMER_ID, "");
						logger.verbose("The ExternalCustomerId from the existing data is:" + sIncomingExternalCustID);
						if(!YFCCommon.isStringVoid(sExternalCustomerID)){
							logger.verbose("Replacing the new ExternalCustomerID withe the old one");
							inXml.getDocumentElement().setAttribute(TelstraConstants.EXTERNAL_CUSTOMER_ID, sExternalCustomerID);
						}
					}
					logger.verbose("Deleting the custiomer with the existing customer key");
					deleteCustomer(sExistingCustomerKey);											
				}							
			}	
			else{
				//HUB-9191[START]
				/*When the Customer is created for the first time, update the extn_customer_node_type
				 * to CT if the CustomerID starts with V. This is used as a reference for the UI
				 * to validate if the Node is a CT or not while editing an Order with a new DAC address.
				 */
				YFCElement yfcEleExtnIp = XPathUtil.getXPathElement(inXml, "//Customer/Extn");
				if(!YFCCommon.isVoid(yfcEleExtnIp)){
					String sCustNodeType = yfcEleExtnIp.getAttribute(TelstraConstants.CUSTOMER_NODE_TYPE);
					if(YFCCommon.isStringVoid(sCustNodeType) && sCustomerID.startsWith("V")){
						yfcEleExtnIp.setAttribute(TelstraConstants.CUSTOMER_NODE_TYPE, "CT");
					}
				}
				else{
					yfcEleExtnIp = inXml.createElement(TelstraConstants.EXTN);
					inXml.getDocumentElement().appendChild(yfcEleExtnIp);
					if(sCustomerID.startsWith("V")){
						yfcEleExtnIp.setAttribute(TelstraConstants.CUSTOMER_NODE_TYPE, "CT");
					}
				}				
				//HUB-9191[END]
			}
			/*
			 * updateInDocWithNewDacField method will update the modified customer with the existing NewDAC field value
			 * If existing DAC has NewDac value as N, modified DAC will also be created with N otherwise with Y
			 */
			YFCElement yfcEleExtnIp = XPathUtil.getXPathElement(inXml, "//Customer/Extn");
			if(YFCCommon.isVoid(yfcEleExtnIp)){
				yfcEleExtnIp = inXml.createElement(TelstraConstants.EXTN);
				inXml.getDocumentElement().appendChild(yfcEleExtnIp);
			}	
			yfcEleExtnIp.setAttribute(TelstraConstants.NEW_DAC, sNewDac);

			/*
			 * Create/update the customer			 
			 */
			logger.verbose("Creating the new customer");
			outxml = invokeYantraApi("manageCustomer",inXml);
		}
		return outxml;
	}

	/**
	 * Delete customer with Multiple CustomerKey
	 * @param inXml
	 * @param iExistingCustomerKey2
	 * @param iCustomerKey2
	 * @param sCustomerID
	 * @param flag
	 * @return
	 */


	private void deleteCustomer(String sExistingCustomerKey) {

		//Delete the Customer with the CustomerKey
		YFCDocument inXmlDeleteCustomer = YFCDocument.getDocumentFor("<Customer OrganizationCode='TELSTRA_SCLSA' CustomerID='' CustomerKey='' ></Customer>");
		inXmlDeleteCustomer.getDocumentElement().setAttribute("CustomerKey", sExistingCustomerKey);
		invokeYantraApi("deleteCustomer",inXmlDeleteCustomer);				

	}
}

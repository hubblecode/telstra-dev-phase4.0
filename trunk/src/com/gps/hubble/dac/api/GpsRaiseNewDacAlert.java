/***********************************************************************************************
 * File	Name		: GpsRaiseNewDacAlert.java
 *
 * Description		: This class is called for raising new dac alert. 
 * 					If new dac flag is Y for the customer, it calls GpsNewDACAlert service to raise an alert and call manage customer
 * 					api to update the new dac flag to N
 * 					If the new dac flag is N for the customer,
 * 
 * Modification	Log	:
 * ---------------------------------------------------------------------------------------------
 * Ver #	Date			Author					Modification


 * ---------------------------------------------------------------------------------------------
 * 1.0		09/03/17		Prateek Kumar			Initial	Version
 *  
 * ---------------------------------------------------------------------------------------------
 * 
 */

package com.gps.hubble.dac.api;

import java.util.HashMap;
import java.util.Map;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSException;

/**
 * 
 * @author Prateek
 *
 */
public class GpsRaiseNewDacAlert extends AbstractCustomApi {

	private static YFCLogCategory logger = YFCLogCategory.instance(GpsRaiseNewDacAlert.class);

	/**
	 * 
	 */
	@Override
	public YFCDocument invoke(YFCDocument yfcInDoc) throws YFSException {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(),"invoke", yfcInDoc);
		
		Map<String, YFCDocument> mapCustIDCustomerDetail = new HashMap<>();
		
		YFCNodeList<YFCElement> yfcNlOrderLine = yfcInDoc.getElementsByTagName(TelstraConstants.ORDER_LINE);
		for(YFCElement yfcEleOrderLine : yfcNlOrderLine){
//			String sShipToId = yfcEleOrderLine.getAttribute(TelstraConstants.SHIP_TO_ID);
			YFCElement yfcEleOrderLineExtn = yfcEleOrderLine.getChildElement(TelstraConstants.EXTN);
			if(!YFCCommon.isVoid(yfcEleOrderLineExtn)){
			String sCustomerKey = yfcEleOrderLineExtn.getAttribute(TelstraConstants.CUSTOMER_KEY);
			YFCDocument docCustomerListOp = null;
			if(mapCustIDCustomerDetail.containsKey(sCustomerKey)){
				docCustomerListOp = mapCustIDCustomerDetail.get(sCustomerKey);
			}
			else{
				docCustomerListOp = callGetCustomerListApi(sCustomerKey);
				mapCustIDCustomerDetail.put(sCustomerKey, docCustomerListOp);
			}
			
			YFCElement eleExtn = XPathUtil.getXPathElement(docCustomerListOp, "//Customer/Extn");

			if(!YFCCommon.isVoid(eleExtn)){
				String sNewDac = eleExtn.getAttribute(TelstraConstants.NEW_DAC);

				LoggerUtil.verboseLog("GpsRaiseNewDacAlert :: invoke :: DacId ", logger, sCustomerKey);
				LoggerUtil.verboseLog("GpsRaiseNewDacAlert :: invoke :: sNewDac ", logger, sNewDac);
				/*
				 * If new dac flag is not Y, then returning from this class.
				 */
				String sStatus = XPathUtil.getXpathAttribute(docCustomerListOp, "//Customer/@Status");
				if(!TelstraConstants.YES.equals(sNewDac)||"30".equals(sStatus)){
					continue;					
				}
				/*
				 * Raise the new dac alert
				 */
				invokeYantraService(TelstraConstants.SERVICE_GPS_NEW_DAC_ALERT, docCustomerListOp);

				/*
				 * Update new dac flag to N as new dac alert has already been raise.
				 */
				updateDac(docCustomerListOp);					
				
				/*
				 * Update the new dac flag to N in the map so that multiple alert is not raised for the multiple line sharing the same DAC id
				 */
				
				eleExtn.setAttribute(TelstraConstants.NEW_DAC, TelstraConstants.NO);
				mapCustIDCustomerDetail.put(sCustomerKey, docCustomerListOp);

			}
			}
		}
		LoggerUtil.endComponentLog(logger, this.getClass().getName(),"invoke", yfcInDoc);	
		return yfcInDoc;
	}

	/**
	 * 
	 * @param docCustomerListOp
	 */
	private void updateDac(YFCDocument docCustomerListOp) {

		YFCElement yfcEleCust = docCustomerListOp.getElementsByTagName(TelstraConstants.CUSTOMER).item(0);
		String sCustId = yfcEleCust.getAttribute(TelstraConstants.CUSTOMER_ID);
		String sOrganizationCode = yfcEleCust.getAttribute(TelstraConstants.ORGANIZATION_CODE);

		YFCDocument docManageCustomerIp = YFCDocument.getDocumentFor("<Customer/>");
		YFCElement yfcEleRoot = docManageCustomerIp.getDocumentElement();

		yfcEleRoot.setAttribute(TelstraConstants.CUSTOMER_ID, sCustId);
		yfcEleRoot.setAttribute(TelstraConstants.ORGANIZATION_CODE, sOrganizationCode);

		YFCElement yfcEleExtn = docManageCustomerIp.createElement(TelstraConstants.EXTN);
		yfcEleRoot.appendChild(yfcEleExtn);
		yfcEleExtn.setAttribute(TelstraConstants.NEW_DAC, TelstraConstants.NO);

		invokeYantraApi("manageCustomer",docManageCustomerIp);

	}

	/**
	 * 
	 * @param sShipToId
	 * @return
	 */
	private YFCDocument callGetCustomerListApi(String sCustomerKey) {
		/*
		 * constructing get customer list api input
		 */
		YFCDocument docCustomerListIp = YFCDocument.getDocumentFor("<Customer CustomerKey=''></Customer>");
		docCustomerListIp.getDocumentElement().setAttribute("CustomerKey", sCustomerKey);
		/*
		 * constructing get customer list api template
		 */
		YFCDocument docCustomerListTemp = YFCDocument.getDocumentFor(
				"<CustomerList><Customer CustomerID='' OrganizationCode='' Status='' CustomerKey=''> <Extn NewDac=''/> </Customer> </CustomerList>");
		/*
		 * invoking get customer list api
		 */
		YFCDocument docCustomerListOp = invokeYantraApi("getCustomerList",docCustomerListIp, docCustomerListTemp);
		LoggerUtil.verboseLog("GpsRaiseNewDacAlert :: callGetCustomerListApi :: docCustomerListOp ", logger, docCustomerListOp);
		return docCustomerListOp;
	}



}

/***********************************************************************************************
 * File Name        : AdjustITECSupply.java
 *
 * Description      : This class is called to update the ITEC Supply from ManageItecInv code
 * 
 * Modification Log :
 * ---------------------------------------------------------------------------------------------
 * Ver #    Date            Author                  Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0      Jan 04,2017     Santosh Nagaraj            Initial Version
 * ---------------------------------------------------------------------------------------------
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
 **********************************************************************************************/
package com.gps.hubble.inventory.api;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfs.japi.YFSException;

public class AdjustITECSupply extends AbstractCustomApi {
  
  /**
   * @param inXml
   * This Method update the ITEC Supply picture in GPS_ITEC_SUPPLY table.
   * 
   */
  public YFCDocument invoke(YFCDocument inXml) throws YFSException {
    YFCDocument gpsGetItecSupplyListDoc = YFCDocument.getDocumentFor(inXml.getDocumentElement().toString());
    gpsGetItecSupplyListDoc.getDocumentElement().removeAttribute("Quantity");
    /*
     * Checking if a record exists for the MaterialId, ITECLocation, Location and Status combination passed in the input.
     * If an entry with the combination is already present in the GPS_ITEC_SUPPLY table, 
     * then the quantity for the same be adjusted based on the quantity passed in the input xml (+1 or -1)
     */
    YFCDocument itecSupplyOutDoc = invokeYantraService("GpsGetItecSupplyList", gpsGetItecSupplyListDoc);
    if(itecSupplyOutDoc != null && itecSupplyOutDoc.getDocumentElement().hasChildNodes()) {
      YFCElement itecSupplyRootEle = itecSupplyOutDoc.getDocumentElement().getElementsByTagName("ItecSupply").item(0);
      int quantity = itecSupplyRootEle.getIntAttribute("Quantity");
      int incomingQuantity = inXml.getDocumentElement().getIntAttribute("Quantity");
      int newQuantity = quantity + incomingQuantity;
      inXml.getDocumentElement().setIntAttribute("Quantity", newQuantity);
      inXml.getDocumentElement().setAttribute("ItecSupplyKey", itecSupplyRootEle.getAttribute("ItecSupplyKey"));
      /*
       * Updating the quantity to be adjusted based on the quantity passed in the input xml (+1 or -1)
       */
      invokeYantraService("GpsChangeItecSupply", inXml);
    } else {
      /*
       * Creating a new record in GPS_ITEC_SUPPLY if no record exists for MaterialId, ITECLocation, Location and Status combination.
       */
      invokeYantraService("GpsCreateItecSupply", inXml);
    }
    // Returning the input XML processed
    return inXml;
  }
}

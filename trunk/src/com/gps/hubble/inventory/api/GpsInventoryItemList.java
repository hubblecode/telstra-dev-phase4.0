/**
 * 
 */
package com.gps.hubble.inventory.api;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCException;


/**
 * Custom API to invoke getInventoryItemList
 * @author Bridge
 *
 */
public class GpsInventoryItemList extends AbstractCustomApi {
	private YFCDocument retDoc; 

	private static YFCLogCategory logger = YFCLogCategory.instance(GpsInventoryItemList.class);
	/* (non-Javadoc)
	 * @see com.bridge.sterling.framework.api.AbstractCustomApi#invoke(com.yantra.yfc.dom.YFCDocument)
	 */
	@Override
	public YFCDocument invoke(YFCDocument inpDoc) {

		try {

			LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke",inpDoc);
		/*	String itemID= inpDoc.getDocumentElement().getAttribute(TelstraConstants.ITEM_ID);
			String itemIDQryType= inpDoc.getDocumentElement().getAttribute(TelstraConstants.ITEM_ID+"QryType","EQ");
			String itemDesc=inpDoc.getDocumentElement().getAttribute("ShortDescription");
			String itemDescQryType= inpDoc.getDocumentElement().getAttribute("ShortDescriptionQryType","EQ");			
			String productLine = inpDoc.getDocumentElement().getAttribute("ProductLine");
			String productLineQryType= inpDoc.getDocumentElement().getAttribute("ProductLineQryType","EQ");			
			 */
			String maxrecords = inpDoc.getDocumentElement().getAttribute("MaximumRecords");
			Connection connection = getServiceInvoker().getDBConnection();
//itemID, itemDesc, productLine

			try (PreparedStatement ps = createQuery(connection, inpDoc.getDocumentElement());
					ResultSet rs = ps.executeQuery()) {
				retDoc = constructDocument(rs, maxrecords);

			}catch(SQLException sqlEx){
				logger.error("SQLException while executing query ",sqlEx);
				throw new YFCException(sqlEx);
			} 
			return retDoc;

		}finally {

			LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke",inpDoc);
		}

	}
	private PreparedStatement createQuery(Connection connection,YFCElement inpElem) throws SQLException{

		String itemID= inpElem.getAttribute(TelstraConstants.ITEM_ID);
		String itemIDQryType= inpElem.getAttribute(TelstraConstants.ITEM_ID+"QryType","FLIKE");
		String itemDesc=inpElem.getAttribute("ShortDescription");
		String itemDescQryType= inpElem.getAttribute("ShortDescriptionQryType","LIKE");			
		String productLine = inpElem.getAttribute("ProductLine");
		String productLineQryType= inpElem.getAttribute("ProductLineQryType","FLIKE");			
		String maxrecords = inpElem.getAttribute("MaximumRecords");
		
		int psIndex = 1;
		String query= "select YII.INVENTORY_ITEM_KEY INVENTORY_ITEM_KEY, YI.ITEM_KEY ITEM_KEY, COALESCE(YII.ITEM_ID,YI.ITEM_ID) ITEM_ID,YI.SHORT_DESCRIPTION SHORT_DESCRIPTION,COALESCE(YII.UOM,YI.UOM) UOM,COALESCE(YII.ORGANIZATION_CODE,YI.ORGANIZATION_CODE) ORGANIZATION_CODE,YI.PRODUCT_LINE PRODUCT_LINE"
				+" FROM YFS_INVENTORY_ITEM YII FULL OUTER JOIN YFS_ITEM YI"
				+" ON YII.ITEM_ID = YI.ITEM_ID AND YII.UOM = YI.UOM AND YII.ORGANIZATION_CODE='"+TelstraConstants.ORG_TELSTRA+"' AND YI.ORGANIZATION_CODE = '"+TelstraConstants.ORG_TELSTRA_SCLSA+"' "
				+" WHERE ";		

		StringBuilder builder= new StringBuilder();
		builder.append(query);
		boolean critAdded = false;
		if(!XmlUtils.isVoid(itemID)){
			builder.append(" ( ");
			builder.append(" YII.ITEM_ID  ");
			if("EQ".equals(itemIDQryType)) {
				builder.append(" = ? ");
			} else {
				builder.append(" LIKE ? ");
			}
			builder.append(" OR ");
			builder.append(" YI.ITEM_ID ");
			if("EQ".equals(itemIDQryType)) {
				builder.append(" =  ? ");
			} else {
				builder.append(" LIKE ? ");
			}			
			builder.append(" ) ");
			critAdded = true;
		}

		if(!XmlUtils.isVoid(productLine)){
			if(critAdded) {
				builder.append(" AND ");
			}
			builder.append(" YI.PRODUCT_LINE ");
			if("EQ".equals(productLineQryType)) {
				builder.append(" =  ? ");
			} else {
				builder.append(" LIKE ? ");
			}
			if(!critAdded) {
				critAdded = true;
			}
		}

		if(!XmlUtils.isVoid(itemDesc)){
			if(critAdded) {
				builder.append(" AND ");
			}
			builder.append(" YI.SHORT_DESCRIPTION ");
			if("EQ".equals(itemDescQryType)) {
				builder.append(" =  ? ");
			} else {
				builder.append(" LIKE ? ");
			}
		}

		PreparedStatement ps= connection.prepareStatement(builder.toString());
		if(!XmlUtils.isVoid(itemID)) {
			String itemStr = appendQryType(itemID,itemIDQryType);
			ps.setString(psIndex, itemStr);
			psIndex++;
			ps.setString(psIndex, itemStr);
			psIndex++;
			
		}
		if(!XmlUtils.isVoid(productLine)) {
			String productLineStr = appendQryType(productLine,productLineQryType);
			ps.setString(psIndex, productLineStr);
			psIndex++;
		}
		if(!XmlUtils.isVoid(itemDesc)) {
			String itemDescStr = appendQryType(itemDesc,itemDescQryType);
			ps.setString(psIndex, itemDescStr);
			
		}

		logger.verbose("Query to be executed "+builder.toString());
		return ps;
	}
	private String appendQryType (String attrVal,String attrQryType) {
		String attrStr = attrVal;
		if("FLIKE".equals(attrQryType)) {
			attrStr = attrVal + "%";
		}else if("LIKE".equals(attrQryType)) {
			attrStr = "%"+ attrVal + "%";
		}else if("ELIKE".equals(attrQryType)) {
			attrStr = "%"+ attrVal;
		}
		return attrStr;
	}
	private YFCDocument constructDocument(ResultSet rs, String maxrecords) throws SQLException{
		YFCDocument doc = YFCDocument.getDocumentFor("<InventoryList />");
		YFCElement elem = doc.getDocumentElement();
		int noOfRecords=Integer.parseInt(maxrecords); 
		int i=0;

		while(rs.next())
		{
			if(i == noOfRecords){
				break;
			}

			String sInventoryItemKey = rs.getString("INVENTORY_ITEM_KEY");
			String sItemKey = rs.getString("ITEM_KEY");

			YFCElement eleInventoryItem = elem.createChild("InventoryItem");

			//Change for Inventory Export [START]
			if(!YFCObject.isVoid(sInventoryItemKey)){
				eleInventoryItem.setAttribute("InventoryItemKey", sInventoryItemKey.trim());
			}
			//Change for Inventory Export [END]

			eleInventoryItem.setAttribute("ItemID", rs.getString("ITEM_ID").trim());
			eleInventoryItem.setAttribute("UnitOfMeasure", rs.getString("UOM").trim());
			if(YFCObject.isVoid(sInventoryItemKey) ) {
				eleInventoryItem.setAttribute("InventoryOrganizationCode", TelstraConstants.ORG_TELSTRA);
			} else {
				eleInventoryItem.setAttribute("InventoryOrganizationCode", rs.getString("ORGANIZATION_CODE").trim());
			}
			YFCElement elemItem = eleInventoryItem.createChild("Item");
			elemItem.setAttribute("OrganizationCode", rs.getString("ORGANIZATION_CODE").trim());
			
			YFCElement elemPrimaryInfo = elemItem.createChild("PrimaryInformation".trim());
			String shortDescription = rs.getString("SHORT_DESCRIPTION");
			if(shortDescription != null) {
				shortDescription = shortDescription.trim();
			}else {
				shortDescription = "";
			}
			
			elemPrimaryInfo.setAttribute("ShortDescription", shortDescription);
			
			ArrayList<String> sourceSystemList = new ArrayList<>();
			//String sourceSystem = rs.getString("SOURCE_SYSTEM");
			if(!YFCObject.isVoid(sItemKey)) {
				YFCDocument inItemDetailDoc = YFCDocument.createDocument(TelstraConstants.ITEM);
				inItemDetailDoc.getDocumentElement().setAttribute(TelstraConstants.ITEM_KEY, sItemKey);
				YFCDocument itemOutDoc = invokeYantraApi(TelstraConstants.API_GET_ITEM_DETAILS,inItemDetailDoc,getItemDetailsTemplate());
				if(itemOutDoc != null) {
					YFCElement itemOutElem = itemOutDoc.getDocumentElement();
					YFCNodeList<YFCElement> additionalAttrList = itemOutElem.getElementsByTagName("AdditionalAttribute");
					for(Iterator<YFCElement> itr=additionalAttrList.iterator();itr.hasNext();) {
						YFCElement additionalAttrElem = itr.next();
						String attrName = additionalAttrElem.getAttribute("Name");
						String attrval = additionalAttrElem.getAttribute("Value");
						if("SOURCE_SYSTEM".equals(attrName)) {
							sourceSystemList.add(attrval);
						}
					}
					
				}
			}
			
			//Inventory Item is not present. This could either be a Vector Item or Integral Item without Inventory. 
			//If its a Vector Item, we need to get the corresponding Integral Item Id
			if(sourceSystemList != null && sourceSystemList.contains(TelstraConstants.VECTOR) && !YFCObject.isVoid(sItemKey) ) {
				YFCDocument inAssocDoc = YFCDocument.createDocument("AssociationList");
				inAssocDoc.getDocumentElement().setAttribute("AssociationType", "Alternative.Y");
				inAssocDoc.getDocumentElement().setAttribute("ItemKey", sItemKey);
				YFCDocument outDoc = invokeYantraApi("getItemAssociations", inAssocDoc, getAssociationsTemplate());
				YFCElement associateItemElem = getAssociatedItemDetails(outDoc);
				if(associateItemElem != null) {
					elemItem.setAttribute("LinkedItemID", associateItemElem.getAttribute("ItemID"));
					elemItem.setAttribute("LinkedUOM", associateItemElem.getAttribute("UnitOfMeasure"));
					elemItem.setAttribute("LinkedInventoryOrganizationCode",TelstraConstants.ORG_TELSTRA);
				}
			}
			String productLine = rs.getString("PRODUCT_LINE");
			if(productLine != null) {
				productLine = productLine.trim();
			} else {
				productLine = "";
			}
			
			elemPrimaryInfo.setAttribute("ProductLine", productLine);

			i++;		

		}

		logger.verbose("Sql return document"+ doc);
		return doc;
	}
	
	private YFCDocument getAssociationsTemplate() {
		YFCDocument templateDoc;
		templateDoc= YFCDocument.getDocumentFor("<AssociationList><Association AssociationType='' AssociatedKey=''><Item ItemID='' ItemKey='' OrganizationCode='' UnitOfMeasure=''/></Association></AssociationList>");
		return templateDoc;
	}
	
	private YFCDocument getItemDetailsTemplate() {
		YFCDocument itemtemplateDoc;
		itemtemplateDoc= YFCDocument.getDocumentFor("<Item ItemID='' ItemKey='' OrganizationCode='' UnitOfMeasure=''><AdditionalAttributeList><AdditionalAttribute AttributeDomainID='' AttributeGroupID=''  Name=''  Value=''/></AdditionalAttributeList></Item>");
		return itemtemplateDoc;
	}	
	private YFCElement getAssociatedItemDetails(YFCDocument outDoc) {
		YFCElement itemOutElem = null;
		YFCElement assocListElem = outDoc.getDocumentElement();
		YFCNodeList<YFCElement> assocList = assocListElem.getElementsByTagName("Association");
		if(assocList.getLength() > 0) {
			YFCElement assocElem = assocList.item(0);
			itemOutElem = assocElem.getChildElement("Item");
		}
		return itemOutElem;
	}
}

package com.gps.hubble.inventory.api;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSException;

/**
 * Custom API to calculate Available To Sell inventory required for Inventory Details UI 
 * 
 * @author Sambeet Mohanty
 * @version 1.0
 *
 *  Extends AbstractCustomApi Class
 */
public class FindAvailableInventory extends AbstractCustomApi{
	private static YFCLogCategory logger = YFCLogCategory.instance(FindAvailableInventory.class);
	private YFCDocument getAtpRespDoc; 

	/* (non-Javadoc)
	 * @see com.bridge.sterling.framework.api.AbstractCustomApi#invoke(com.yantra.yfc.dom.YFCDocument)
	 */
	@Override
	public YFCDocument invoke(YFCDocument inDoc) throws YFSException {
		try {

			LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", inDoc);
			DecimalFormat format = new DecimalFormat("0.00");
			double totalSupplyQty=0.00;
			double totalDemandQty=0.00;
			double availableToSell= 0.00;

			LoggerUtil.verboseLog("Input Document To service", logger, inDoc);
			inDoc = prepareInputDocument(inDoc);

			getAtpRespDoc= invokeYantraApi(TelstraConstants.API_GET_ATP, inDoc, getATPTemplate());

			LoggerUtil.verboseLog("Output Document of getATP API", logger, getAtpRespDoc);	

			/*This section calculates Total ONHAND Supply Quantity*/

			YFCElement totalSupEle=getAtpRespDoc.getElementsByTagName("InventoryTotals").item(0).
					getElementsByTagName("Supplies").item(0);

			totalSupplyQty= Double.parseDouble(totalSupEle.getAttribute("TotalSupply"));
			if(totalSupplyQty > 0 && totalSupEle.hasChildNodes())
			{
				YFCNodeList<YFCElement> supplyList= totalSupEle.getElementsByTagName("Supply");
				totalSupplyQty = getListTotalValue(supplyList);
			}

			LoggerUtil.verboseLog("Total ONHAND Supply", logger, totalSupplyQty);	

			/*This section calculates Total Demand Quantity*/

			String totalDemand = getAtpRespDoc.getElementsByTagName("InventoryTotals").item(0).
					getElementsByTagName("Demands").item(0).getAttribute("TotalDemand");

			totalDemandQty= Double.parseDouble(totalDemand);

			LoggerUtil.verboseLog("Total Demand", logger, totalDemandQty);

			//This section calculates Available TO Sell Inventory
			if (totalSupplyQty>totalDemandQty)
			{
				availableToSell=totalSupplyQty-totalDemandQty;
			}

			LoggerUtil.verboseLog("Calculated Available to Sell", logger, availableToSell);
			getAtpRespDoc.getElementsByTagName("Item").item(0).setAttribute("AvailableToSell", format.format(availableToSell));
			
			//Update Supply Type and Demand Type Description to the getATP output DOc
			getAtpRespDoc=updateSupplyDemandTypDesc(getAtpRespDoc);

			LoggerUtil.verboseLog("Service Output Document", logger, getAtpRespDoc);
			return getAtpRespDoc; 

		}finally {

			LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke",inDoc);
		}

	}

	public YFCDocument updateSupplyDemandTypDesc(YFCDocument doc){
		
		HashMap<String, String> demandMap =getDemandTypListDoc();
		YFCNodeList<YFCElement> demandList=doc.getElementsByTagName("Demand");
		
		for(int i=0; i<demandList.getLength();i++){
			YFCElement ele=demandList.item(i);
			logger.verbose("Inventory Demand Type: "+ele.getAttribute("DemandType"));
			logger.verbose("Demand Type Description"+ demandMap.get(ele.getAttribute("DemandType")));
			ele.setAttribute("Description", demandMap.get(ele.getAttribute("DemandType")));
		}
		
		HashMap<String, String> supplyMap= getSupTypListDoc();
		YFCNodeList<YFCElement> supList=doc.getElementsByTagName("Supply");
		for(int i=0; i<supList.getLength();i++){
			YFCElement ele=supList.item(i);
			ele.setAttribute("Description", supplyMap.get(ele.getAttribute("SupplyType")));
		}

		return doc;
	}


	public HashMap<String, String> getSupTypListDoc(){

		YFCDocument inputDoc=YFCDocument.getDocumentFor("<InventorySupplyType SupplyType='' />");
		String templateStr="<InventorySupplyTypeList><InventorySupplyType Description='' SupplyType=''/></InventorySupplyTypeList>";
		YFCDocument respDoc= invokeYantraApi("getInventorySupplyTypeList", inputDoc, templateStr);
		

		YFCNodeList<YFCElement> supNodeList=respDoc.getElementsByTagName("InventorySupplyType");
		HashMap<String, String> map= new HashMap<>();
		for(int i=0; i<supNodeList.getLength();i++){
			map.put(supNodeList.item(i).getAttribute("SupplyType"), supNodeList.item(i).getAttribute("Description"));
		}
		return map;
	}

	public HashMap<String, String> getDemandTypListDoc(){

		YFCDocument inDoc=YFCDocument.getDocumentFor("<InventoryDemandType />");
		//Invoking Custom API GpsGetInvDemandTypeList to get Demand Type and Description list
		YFCDocument respDoc=invokeYantraService("GpsGetInvDemandTypeList", inDoc);
		logger.verbose("GpsGetInvDemandTypeList output Document :"+respDoc);

		YFCNodeList<YFCElement> demandNodeList=respDoc.getElementsByTagName("InventoryDemandType");
		HashMap<String, String> map= new HashMap<>();
		for(int i=0; i<demandNodeList.getLength();i++){
			map.put(demandNodeList.item(i).getAttribute("DemandType"), demandNodeList.item(i).getAttribute("Description"));
		}
		
		return map; 
	}

	public YFCDocument getATPTemplate()
	{
		return YFCDocument.getDocumentFor("<InventoryInformation><Item><AvailableToPromiseInventory />"
				+"</Item></InventoryInformation>");
	}

	/**
	 * Method name: getListTotalValue
	 * @param nodeList
	 * @return double
	 * This methods calculates the total ONHAND inventory for the input NodeList 
	 */

	public double getListTotalValue(YFCNodeList<YFCElement> nodeList)
	{
		double listTotal=0.00;
		for(YFCElement elem : nodeList)
		{
			if(elem.getAttribute("SupplyType").trim().equals(TelstraConstants.ONHAND))
			{
				listTotal+= Double.parseDouble(elem.getAttribute("Quantity"));
			}
		}

		return listTotal;
	}

	public YFCDocument prepareInputDocument(YFCDocument inpDoc)
	{

		YFCDocument retDoc= YFCDocument.createDocument("GetATP");
		retDoc.getDocumentElement().setAttributes(inpDoc.getDocumentElement().getAttributes());

		if(XmlUtils.isVoid(inpDoc.getDocumentElement().getAttribute("EndDate")))
		{
			String modifieddate= getATPEndDate();
			retDoc.getDocumentElement().setAttribute("EndDate", modifieddate);
		}

		LoggerUtil.verboseLog("Input Document getATP api", logger, retDoc);
		return retDoc;

	}

	public String getATPEndDate()
	{
		Date date= new Date();
		SimpleDateFormat formatdate= new SimpleDateFormat("YYYY-MM-dd");
		LoggerUtil.verboseLog("enddate", logger, formatdate.format(date));

		return formatdate.format(date);
	}

}

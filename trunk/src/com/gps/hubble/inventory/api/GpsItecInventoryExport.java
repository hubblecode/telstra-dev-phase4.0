/***********************************************************************************************
 * File Name        : GpsItecInventoryExport.java
 *
 * Description      : This class prepares the data for the Itec inventory export
 * 
 * Modification Log :
 * ---------------------------------------------------------------------------------------------
 * Ver #    Date            Author                  Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0      Aug 22,2017     Prateek Kumar          Initial Version
 * ---------------------------------------------------------------------------------------------
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
 **********************************************************************************************/
package com.gps.hubble.inventory.api;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.json.JSONException;
import org.apache.commons.json.JSONObject;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.utils.GpsJSONXMLUtil;
import com.sterlingcommerce.woodstock.util.Base64;
import com.yantra.custom.dbi.GPS_Itec_Supply;
import com.yantra.shared.dbclasses.GPS_Itec_SupplyDBHome;
import com.yantra.shared.ycp.YFSContext;
import com.yantra.yfc.dblayer.PLTQueryBuilder;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSException;

/**
 * The class is executed to send the itec inventory data back to the agent
 * @author Prateek
 *
 */
public class GpsItecInventoryExport extends AbstractCustomApi {
	private static YFCLogCategory logger = YFCLogCategory.instance(GpsItecInventoryExport.class);

	@Override
	public YFCDocument invoke(YFCDocument yfcInDoc) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", yfcInDoc);

		YFCDocument yfcOpDoc = getItecExportData(yfcInDoc);

		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", yfcInDoc);

		return yfcOpDoc;
	}

	/**
	 * 
	 * @param yfcInDoc
	 * @return
	 */
	private YFCDocument getItecExportData(YFCDocument yfcInDoc) {

		YFCElement yfcInEle = yfcInDoc.getDocumentElement();

		YFCDocument yfcOpDoc = YFCDocument.createDocument(TelstraConstants.EXPORT_DATA);
		YFCElement yfcEleOpDocRoot = yfcOpDoc.getDocumentElement();
		YFCElement yfcEleColumnList = yfcEleOpDocRoot.createChild(TelstraConstants.COLUMN_LIST);
		YFCElement yfcEleDataList = yfcEleOpDocRoot.createChild(TelstraConstants.DATA_LIST);

		String sSearchCriteria =	yfcInEle.getAttribute(TelstraConstants.EXPORT_CRITERIA);
		String sDecodedCriteria = new String(Base64.decode(sSearchCriteria.getBytes()));

		try {
			JSONObject criteriaJSON = new JSONObject(sDecodedCriteria);
			JSONObject searchCriteriaObj = (JSONObject)criteriaJSON.get("SearchCriteria");
			YFCDocument criteriaDoc = GpsJSONXMLUtil.getXmlFromJSON(searchCriteriaObj,TelstraConstants.ITEC_INVENTORY);

			YFCDocument yfcDocGetItecInventory = getServiceInvoker().invokeYantraService("GpsGetItecInventory", criteriaDoc);

			if(yfcDocGetItecInventory.getDocumentElement().hasChildNodes()){

				Map<String, YFCElement> itecMaterialIdDataList = new HashMap<>();

				for(YFCElement yfcEleItem : yfcDocGetItecInventory.getElementsByTagName(TelstraConstants.ITEM)){				
					String sMaterialId = yfcEleItem.getAttribute(TelstraConstants.MATERIAL_ID);
					itecMaterialIdDataList.put(sMaterialId, yfcEleItem);
				}

				String [] materialIdArr=itecMaterialIdDataList.keySet().toArray(new String[0]);

				PLTQueryBuilder pltQry = new PLTQueryBuilder();

				if(materialIdArr.length > 0) {
					pltQry.appendWHERE();				
					pltQry.appendStringForIN(GPS_Itec_SupplyDBHome.getInstance(), "MATERIAL_ID", materialIdArr);
					pltQry.appendOrderBy("MATERIAL_ID, Status");

					logger.debug("The Generated Query is "+pltQry.getReadableWhereClause());				
				}
				YFSContext ctx = getServiceInvoker().getContext();

				@SuppressWarnings("unchecked")
				List<GPS_Itec_Supply> resultList = GPS_Itec_SupplyDBHome.getInstance().listWithWhereUnCommited(ctx, pltQry, Integer.MAX_VALUE);

				YFCDocument templateDoc = YFCDocument.createDocument(GPS_Itec_SupplyDBHome.getInstance().getXMLName());
				YFCElement templateElem = templateDoc.getDocumentElement();


				YFCDocument yfcDocItecResult = YFCDocument.getDocumentFor("<ItecResultList />");
				YFCElement yfcEleItec = yfcDocItecResult.getDocumentElement();

				for(Iterator<GPS_Itec_Supply> iter=resultList.iterator();iter.hasNext();) {
					GPS_Itec_Supply row = iter.next();
					YFCElement yfcEleRow = row.getXML(yfcOpDoc, templateElem, TelstraConstants.ITEC_RESULT_ROW);
					yfcEleItec.importNode(yfcEleRow);								
				}

				List<String> lMaterialId = new ArrayList<>();
				Collections.addAll(lMaterialId, materialIdArr);
				
				
				/* This method will set available quantity in the item tag*/
				setAvailableQty(itecMaterialIdDataList,yfcEleItec);
				/* Below method will populate the column list tag*/
				prepareColumnElement(yfcEleColumnList);
				/* Below method will populate the data list tag */
				createDataRows(yfcEleItec,yfcEleDataList,itecMaterialIdDataList,lMaterialId);

			}
		} catch (JSONException je) {

			LoggerUtil.errorLog("JSON Parse Error", logger, je);
			throw new YFSException("JSON Parse Error");

		}

		return yfcOpDoc;
	}

	/**
	 * 
	 * @param itecMaterialIdDataList
	 * @param yfcEleItec
	 */

	private void setAvailableQty(Map<String, YFCElement> itecMaterialIdDataList, YFCElement yfcEleItec) {

		String statusesToBeConsidered = getProperty("StatusesToBeConsidered","");
		for(YFCElement yfcEleItecResultRow : yfcEleItec.getElementsByTagName(TelstraConstants.ITEC_RESULT_ROW)){

			String sStatus = yfcEleItecResultRow.getAttribute(TelstraConstants.STATUS);

			if(statusesToBeConsidered.contains(sStatus)) {

				String sMaterialId = yfcEleItecResultRow.getAttribute(TelstraConstants.MATERIAL_ID);
				if(itecMaterialIdDataList.containsKey(sMaterialId)){

					double dQuantity = yfcEleItecResultRow.getDoubleAttribute(TelstraConstants.QUANTITY); 
					YFCElement yfcEleItem = itecMaterialIdDataList.get(sMaterialId);
					String sAvailableQty = yfcEleItem.getAttribute(TelstraConstants.AVAILABILABLE_QUANTITY);
					if(YFCCommon.isStringVoid(sAvailableQty)){
						yfcEleItem.setDoubleAttribute(TelstraConstants.AVAILABILABLE_QUANTITY, dQuantity);
					}
					else{
						double dExistingAvailableQty = Double.parseDouble(sAvailableQty);
						yfcEleItem.setDoubleAttribute(TelstraConstants.AVAILABILABLE_QUANTITY, dQuantity+dExistingAvailableQty);
					}
				}
			}
		}		
	}

	/**
	 * 
	 * @param yfcEleItec
	 * @param yfcEleDataList
	 * @param itecMaterialIdDataList
	 * @param lMaterialId 
	 * @param lcolumnNames
	 */
	private void createDataRows(YFCElement yfcEleItec, YFCElement yfcEleDataList,
			Map<String, YFCElement> itecMaterialIdDataList, List<String> lMaterialId) {

		Map<String,String> mapLocationDesc = callGetCommonCodeList(TelstraConstants.SPS_LOC_CODE_TYPE);
		
		for(YFCElement yfcEleItecResultRow : yfcEleItec.getElementsByTagName(TelstraConstants.ITEC_RESULT_ROW)){

			String sMaterialId = yfcEleItecResultRow.getAttribute(TelstraConstants.MATERIAL_ID);
			YFCElement yfcEleData = yfcEleDataList.createChild(TelstraConstants.DATA);
			yfcEleData.setAttribute(TelstraConstants.MATERIAL_ID, sMaterialId);
			yfcEleData.setAttribute(TelstraConstants.LOCATION, mapLocationDesc.get(yfcEleItecResultRow.getAttribute(TelstraConstants.LOCATION)));
			yfcEleData.setAttribute(TelstraConstants.STATUS, yfcEleItecResultRow.getAttribute(TelstraConstants.STATUS));
			yfcEleData.setAttribute(TelstraConstants.QTY, yfcEleItecResultRow.getAttribute(TelstraConstants.QUANTITY));
			stampItemLevelDetail(yfcEleData, itecMaterialIdDataList, sMaterialId);			
			lMaterialId.remove(sMaterialId);
		}
		
		/* Below piece of code will populate the itec item which does not exist in the itec supply table*/
		for(String sMaterialId : lMaterialId){
			
			YFCElement yfcEleData = yfcEleDataList.createChild(TelstraConstants.DATA);
			yfcEleData.setAttribute(TelstraConstants.MATERIAL_ID, sMaterialId);
			yfcEleData.setAttribute(TelstraConstants.LOCATION, TelstraConstants.NOT_APPLICABLE);
			yfcEleData.setAttribute(TelstraConstants.STATUS, TelstraConstants.NOT_APPLICABLE);
			yfcEleData.setAttribute(TelstraConstants.QTY, "0");
			stampItemLevelDetail(yfcEleData, itecMaterialIdDataList, sMaterialId);
		}
	}

	/**
	 * 
	 * @param yfcEleData
	 * @param itecMaterialIdDataList
	 * @param sMaterialId 
	 */
	private void stampItemLevelDetail(YFCElement yfcEleData, Map<String, YFCElement> itecMaterialIdDataList, String sMaterialId) {
		
		YFCElement yfcEleItem = itecMaterialIdDataList.get(sMaterialId);					
		yfcEleData.setAttribute(TelstraConstants.UNIT_OF_MEASURE, yfcEleItem.getAttribute(TelstraConstants.UNIT_OF_MEASURE));
		yfcEleData.setAttribute(TelstraConstants.DESCRIPTION, yfcEleItem.getAttribute(TelstraConstants.MATERIAL_DESCRIPTION));
		yfcEleData.setAttribute(TelstraConstants.AVAILABILABLE_QUANTITY, yfcEleItem.getAttribute(TelstraConstants.AVAILABILABLE_QUANTITY,"0"));
		yfcEleData.setAttribute(TelstraConstants.ITEM_TYPE, yfcEleItem.getAttribute(TelstraConstants.ITEM_TYPE));
		yfcEleData.setAttribute(TelstraConstants.PRODUCT_LINE, yfcEleItem.getAttribute(TelstraConstants.PRODUCT_LINE));
		yfcEleData.setAttribute(TelstraConstants.MANUFACTURER_NAME, yfcEleItem.getAttribute(TelstraConstants.MANUFACTURER_NAME));
		yfcEleData.setAttribute(TelstraConstants.COMMODITY_CODE, yfcEleItem.getAttribute(TelstraConstants.COMMODITY_CODE));
		yfcEleData.setAttribute(TelstraConstants.EXTENDED_DESCRIPTION, yfcEleItem.getAttribute(TelstraConstants.EXTENDED_DESCRIPTION));
		
	}

	/**
	 * 
	 * @param sCodeType
	 * @return
	 */
	private Map<String, String> callGetCommonCodeList(String sCodeType) {

		Map<String, String> mapLocationDesc = new HashMap<>();

		YFCDocument yfcDocGetCommonCodeListTemp = YFCDocument.getDocumentFor("<CommonCodeList> <CommonCode CodeShortDescription='' CodeType='' CodeValue='' /> </CommonCodeList>");
		YFCDocument yfcDocGetCommonCodeListOp =
				invokeYantraApi(
						TelstraConstants.API_GET_COMMON_CODE_LIST,
						YFCDocument.getDocumentFor("<CommonCode CodeType='"+sCodeType+"'/>"),yfcDocGetCommonCodeListTemp);

		if (yfcDocGetCommonCodeListOp.getDocumentElement().hasChildNodes()) {

			for(YFCElement yfcEleCommonCode : yfcDocGetCommonCodeListOp.getElementsByTagName(TelstraConstants.COMMON_CODE)){

				String sCodeValue = yfcEleCommonCode.getAttribute(TelstraConstants.CODE_VALUE);
				String sCodeShortDesc = yfcEleCommonCode.getAttribute(TelstraConstants.CODE_SHORT_DESCRIPTION);

				mapLocationDesc.put(sCodeValue, sCodeShortDesc);

			}
		}
		return mapLocationDesc;
	}

	/**
	 * 
	 * @param yfcEleColumnList
	 * @return
	 */
	private void prepareColumnElement(YFCElement yfcEleColumnList) {

		YFCDocument docTemplateCommonCodeList = YFCDocument.getDocumentFor("<CommonCodeList><CommonCode CodeValue='' CodeShortDescription='' /></CommonCodeList>");
		YFCDocument docGetCommonCodeListOutput = invokeYantraApi(TelstraConstants.API_GET_COMMON_CODE_LIST,
				YFCDocument.getDocumentFor("<CommonCode CodeType='ITEC_EXPORT'><OrderBy><Attribute Desc='N' Name='CodeLongDescription' /></OrderBy></CommonCode>"),docTemplateCommonCodeList);
		for (YFCElement eleCommonCode : docGetCommonCodeListOutput.getElementsByTagName(TelstraConstants.COMMON_CODE)) {
			YFCElement yfcEleColumn = yfcEleColumnList.createChild("Column");
			yfcEleColumn.setAttribute("Name", eleCommonCode.getAttribute(TelstraConstants.CODE_VALUE,""));
			yfcEleColumn.setAttribute("Label", eleCommonCode.getAttribute(TelstraConstants.CODE_SHORT_DESCRIPTION,""));
			if (!eleCommonCode.getAttribute(TelstraConstants.CODE_VALUE, "").contains("Qty")
					&& !(eleCommonCode.getAttribute(TelstraConstants.CODE_VALUE, "").contains("Quantity"))) {
				yfcEleColumn.setAttribute(TelstraConstants.ESCAPE_CHARS, "Y");
			}
		}
	}
}

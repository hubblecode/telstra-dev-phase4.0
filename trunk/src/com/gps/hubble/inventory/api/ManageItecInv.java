/***********************************************************************************************
 * File Name        : ManageItecInv.java
 *
 * Description      : This class is called when we consume ITEC Inventory data from Queue
 * 
 * Modification Log :
 * ---------------------------------------------------------------------------------------------
 * Ver #    Date            Author                  Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0      Jan 04,2017     Santosh Nagaraj         Initial Version
 * 1.1		Jun 12,29=017	Prateek Kumar			HUB-9318: fixed sax parse exception
 * ---------------------------------------------------------------------------------------------
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
 **********************************************************************************************/
package com.gps.hubble.inventory.api;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSException;

public class ManageItecInv extends AbstractCustomApi  {
  private static YFCLogCategory logger = YFCLogCategory.instance(ManageItecInv.class);
  
  /**
   * @param inXml
   * This Method will create or update the itec inventory in GPS_ITEC_SERIAL and GPS_ITEC_SUPPLY tables.
   * 
   */
  public YFCDocument invoke(YFCDocument inXml) {
    logger.verbose("checking if the ITEC inventory exists for the item");
    YFCNodeList<YFCElement> itecSerialList = inXml.getElementsByTagName("ItecSerial");
    for (YFCElement itecSerial : itecSerialList) {
      validateInput(itecSerial);
      YFCDocument createOrChangeItecSerialDoc = YFCDocument.getDocumentFor(itecSerial.toString());
      String getItecSerialListDoc = "<ItecSerialList  MaterialId='"+itecSerial.getAttribute("MaterialId")+"' "
          + "EquipmentNo='"+itecSerial.getAttribute("EquipmentNo")+"' />";
      
      
      YFCDocument getItecSerialListInDoc = YFCDocument.getDocumentFor(getItecSerialListDoc);
      //HUB-9318
      getItecSerialListInDoc.getDocumentElement().setAttribute("Barcode", itecSerial.getAttribute("Barcode"));
      /*
       * Checking if the Serial exists for the MaterialId, Barcode and EquipmentNo combination in GPS_ITEC_SERIAL table.
       * If an entry with the combination is already present in the GPS_ITEC_SERIAL table, 
       * then the Location, ITECLocation, Status, HW_Revision, Other_Config will be updated for the serial in GPS_ITEC_SERIAL table and
       * the supply will also be udpated accordingly in GPS_ITEC_SUPPLY table  
       */
      YFCDocument getItecSeriallistOutDoc = invokeYantraService("GpsGetItecSerialList", getItecSerialListInDoc);
      if(getItecSeriallistOutDoc != null && getItecSeriallistOutDoc.getDocumentElement().hasChildNodes()) {
        logger.verbose("ITEC Serial exists in the table");
        YFCElement itecSerialOutEle = getItecSeriallistOutDoc.getElementsByTagName("ItecSerial").item(0);
        String materialId = itecSerialOutEle.getAttribute("MaterialId");
        String itecLocation = itecSerialOutEle.getAttribute("ITECLocation");
        String location = itecSerialOutEle.getAttribute("Location");
        String status = itecSerialOutEle.getAttribute("Status");
        createOrChangeItecSerialDoc.getDocumentElement().setAttribute("ItecSerialKey", getItecSeriallistOutDoc.getElementsByTagName("ItecSerial").item(0).getAttribute("ItecSerialKey"));
        if(!itecSerial.getAttribute("ITECLocation").equals(itecLocation) ||
            !itecSerial.getAttribute("Status").equals(status) || !itecSerial.getAttribute("Location").equals(location)) {
          /*
           * If ITECLocation or Status in the database is not matching with the ITECLocation or Status in the incoming ITEC Inventory,
           * AdjustITECSupply will be called to reduce 1 quantity for the ITECLocation and Status combination.
           * The quantity for the existing ITECLocation and Status combination will be reduced by 1.
           */
          YFCDocument reduceITECSupply = YFCDocument.getDocumentFor("<ItecSupply "
              + "Location='"+location+"' "
              + "MaterialId='"+materialId+"' "
                  + "ITECLocation='"+itecLocation+"' "
                  + "Status='"+status+"' "
                      + "Quantity='-1'></ItecSupply>");
          invokeYantraService("AdjustITECSupply", reduceITECSupply);
          /*
           * AdjustITECSupply will be called to increase 1 quantity for the new ITECLocation and Status combination.
           */
          YFCDocument increaseITECSupply = YFCDocument.getDocumentFor("<ItecSupply "
              + "Location='"+itecSerial.getAttribute("Location")+"' "
                  + "MaterialId='"+itecSerial.getAttribute("MaterialId")+"' "
                      + "ITECLocation='"+itecSerial.getAttribute("ITECLocation")+"' "
                          + "Status='"+itecSerial.getAttribute("Status")+"' "
                          + "Quantity='1'></ItecSupply>");
          invokeYantraService("AdjustITECSupply", increaseITECSupply);
        }
        
        /*
         * There are chances of HW_Revision and Other_config being modified for the incoming ITEC inventory, 
         * GpsChangeItecSerial will be called to udpate the same in GPS_ITEC_SERIAL table
         */
        
        invokeYantraService("GpsChangeItecSerial", createOrChangeItecSerialDoc);        
      } else {
        /*
         * The Serial does not exist for the MaterialId, Barcode and EquipmentNo combination, hence creating new record for the combination in GPS_ITEC_SERIAL table.
         * Also calling AdjustITECSupply for to increase the supply quantity in GPS_ITEC_SUPPLY table
         */
        invokeYantraService("GpsCreateItecSerial", createOrChangeItecSerialDoc);
        YFCDocument adjustItecSupply = YFCDocument.getDocumentFor("<ItecSupply "
            + "Location='"+itecSerial.getAttribute("Location")+"' "
                + "MaterialId='"+itecSerial.getAttribute("MaterialId")+"' "
                    + "ITECLocation='"+itecSerial.getAttribute("ITECLocation")+"' "
                        + "Status='"+itecSerial.getAttribute("Status")+"' "
                            + "Quantity='1'></ItecSupply>");
        invokeYantraService("AdjustITECSupply", adjustItecSupply);
      }
    }
    // Returning the input XML processed
    return inXml;
  }
  
  /**
   * This method will validate if the input xml has the mandatory fields to be passed.  Viz. ITECLocation, Location,
   * MaterialId, EquipmentNo, Barcode and Status.
   * If one of these attributes are not present or has a blank value, then an exception will be raised which can be reprocessed through UI
   * @param itecSerial
   */
  private void validateInput(YFCElement itecSerial) {
    if(YFCObject.isVoid(itecSerial.getAttribute("EquipmentNo")) ||
        YFCObject.isVoid(itecSerial.getAttribute("ITECLocation")) ||
        YFCObject.isVoid(itecSerial.getAttribute("Location")) ||
        YFCObject.isVoid(itecSerial.getAttribute("MaterialId")) ||
        YFCObject.isVoid(itecSerial.getAttribute("Barcode")) ||
        YFCObject.isVoid(itecSerial.getAttribute("Status"))) {
      //throw exception
      LoggerUtil.verboseLog("Mandatory fields missing in the manageItecInv input", logger, " throwing exception");
      throw ExceptionUtil.getYFSException("TEL_ERR_0502_0001",new YFSException());
    }
  }
}
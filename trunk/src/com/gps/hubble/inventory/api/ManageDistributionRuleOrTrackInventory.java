package com.gps.hubble.inventory.api;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSException;

public class ManageDistributionRuleOrTrackInventory extends AbstractCustomApi {
  
  private static YFCLogCategory logger = YFCLogCategory.instance(ManageDistributionRuleOrTrackInventory.class);
  
  /*
   * yfInDoc : Input to the service 
   * Sample Input:    <Organization  Action="ManageDistributionRule || TrackInventory" OrganizationCode="AL_N1" >
                        <Node  NodeType="DC"/>
                      </Organization>
   * If Action="ManageDistributionRule", The GpsManageDistributionRule Service will be invoked to update the distribution rule configuration for the Node Type
   * If Action="TrackInventory", The manageOrganizationHierarchy API will be called to update the node configuration to Track the Inventory
   * If No Action is passed then the default action will be ManageDistributionRule
   */
  public YFCDocument invoke(YFCDocument inXml) throws YFSException 
  {
    LoggerUtil.startComponentLog(logger, this.getClass().getName(),"invoke", inXml);
    inXml.getDocumentElement().setAttribute("IsNode", "Y");
    String strAction = inXml.getDocumentElement().getAttribute("Action");
    if(TelstraConstants.ACTION_TRACK_INVENTORY.equals(strAction)) {
      /*
       * Call manageOrganizationHierarchy to change the Node configuration to track the inventory (InventoryTracked=Y flag)
       */
    	
      YFCElement nodeEle = inXml.getElementsByTagName("Node").item(0);
      nodeEle.setAttribute("InventoryTracked", "Y");
      /*
       * Call manageOrganizationHierarchy API 
       */
	  logger.verbose("Input to manageOrganizationHierarchy Api invoked from ManageDistributionRuleOrTrackInventory class  : : " + inXml.toString());
      invokeYantraApi("manageOrganizationHierarchy", inXml);
    } else {
      /*
       * call getOrganizationList for the Organization or the NodeType passed
       */
      YFCDocument getOrganizationListOutput = invokeYantraApi("getOrganizationList", inXml, getOrganizationListTemplate());
      if(!YFCObject.isVoid(getOrganizationListOutput) && getOrganizationListOutput.getDocumentElement().hasChildNodes()) {
        YFCNodeList<YFCElement> organizationList = getOrganizationListOutput.getElementsByTagName("Organization");
        for(YFCElement organization : organizationList) {
          /*
           * Append the Operation="Manage" attribute to the input xml for the GPSManageDistributionRule service
           */
          organization.setAttribute("Operation", "Manage");
          /*
           * Call GpsManageDistributionRule service to change the distribution rule for the NodeTypes to NodeType_NT
           */
    	  logger.verbose("Input to GpsManageDistributionRule : : " + YFCDocument.getDocumentFor(organization.toString()));
          invokeYantraService("GpsManageDistributionRule", YFCDocument.getDocumentFor(organization.toString()));
        }
      }
    }
    return null;
  }
  
  /**
   * This Method will return the template for the getOrganizationList API
   * @return
   */
  private YFCDocument getOrganizationListTemplate() {
    YFCDocument getOrganizationListTemplate = YFCDocument.getDocumentFor("<Organization IsNode='' OrganizationCode=''><Node NodeType='' InventoryTracked=''/></Organization>");
    return getOrganizationListTemplate;
  }
}
/***********************************************************************************************
 * File Name        : ManageItecInv.java
 *
 * Description      : This class is called from UI to retrieve the ITEC inventory data based on 
 *                    the search criteria
 * 
 * Modification Log :
 * ---------------------------------------------------------------------------------------------
 * Ver #    Date            Author                  Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0      Jan 06,2017     Santosh Nagaraj            Initial Version
 * ---------------------------------------------------------------------------------------------
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
 **********************************************************************************************/
package com.gps.hubble.inventory.api;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSException;

public class GetItecInventory extends AbstractCustomApi {
	private static YFCLogCategory logger = YFCLogCategory.instance(GetItecInventory.class);

	/**
	 * @param inXml
	 * This method will get the supply and serial information for the ITEC inventory based on the search criteira passed from UI
	 */
	public YFCDocument invoke(YFCDocument inXml) throws YFSException {
		logger.verbose("getting the ITEC inventory data for the search criteria passed");
		String revisionNo = inXml.getDocumentElement().getAttribute("RevisionNo");
		String serialNo = inXml.getDocumentElement().getAttribute("SerialNo");
		String barcode = inXml.getDocumentElement().getAttribute("Barcode");
		String materialName = inXml.getDocumentElement().getAttribute("ExtendedDescription");

		String materialId=inXml.getDocumentElement().getAttribute("MaterialId");
		String maxRecords=inXml.getDocumentElement().getAttribute("MaximumRecords");
		YFCDocument itecInventoryListDoc = YFCDocument.getDocumentFor("<ItecInventoryList />");

		/*
		 * Checking if one among revisionNo, serialNo or barcode is passed in the UI Search screen.
		 * Since all these three attributes are specific to the records in GPS_ITEC_SERIAL table, 
		 * the search will be performed based on the GPS_ITEC_SERIAL table 
		 * 
		 * */
		if(!YFCObject.isNull(revisionNo) || !YFCObject.isNull(serialNo) || !YFCObject.isNull(barcode)) {
			// Calling itecSerialList for the passed revisionNo, serialNo or barcode
			String sIsSerial = inXml.getDocumentElement().getAttribute(TelstraConstants.IS_SERIAL);
			if(!YFCObject.isVoid(materialName) && TelstraConstants.YES.equalsIgnoreCase(sIsSerial)){

				return getItecSerialForMaterialName(revisionNo,serialNo,barcode,materialName,materialId,maxRecords,itecInventoryListDoc);

			}else{
				YFCDocument itecSerialListOutDoc = getItecSerialListOutputDocument(inXml);

				YFCDocument getItemListInDoc = YFCDocument.getDocumentFor("<Item OrganizationCode='"+TelstraConstants.ORG_TELSTRA_SCLSA+"' >"
						+ "<ComplexQuery Operator='AND'> "
						+ "</ComplexQuery> "
						+ "<AdditionalAttributeList> "
						+ "<AdditionalAttribute AttributeDomainID='ItemAttribute' AttributeGroupID='ItemReferenceGroup' Name='SOURCE_SYSTEM' Value='ITEC'/>"
						+ "</AdditionalAttributeList> </Item>");

				if(!YFCObject.isNull(maxRecords)){
					getItemListInDoc.getDocumentElement().setAttribute("MaximumRecords", maxRecords);
				}
				YFCElement complexQuery = getItemListInDoc.getElementsByTagName("ComplexQuery").item(0);
				YFCNodeList<YFCElement> serialsList = itecSerialListOutDoc.getElementsByTagName("ItecSerial");

				if(!YFCObject.isNull(itecSerialListOutDoc) &&  serialsList.getLength() >0) {
					YFCElement andEleForMaterialID = complexQuery.createChild("And");
					YFCElement orForMaterialID = andEleForMaterialID.createChild("Or");	

					// Hash set is used to for generating complex query for getItemList with unique items
					boolean isUniqueItem= false; 
					HashSet<String> uriqueItems = new HashSet<String>();

					for(YFCElement serial : serialsList) {
						String itemID=serial.getAttribute("MaterialId");
						isUniqueItem= uriqueItems.add(itemID);
						if(isUniqueItem) {
							YFCElement expressionEle = orForMaterialID.createChild("Exp");
							expressionEle.setAttribute("Name", "ItemID");
							expressionEle.setAttribute("QryType", "EQ");
							expressionEle.setAttribute("Value", itemID);
						}
					}

					if(!YFCObject.isVoid(materialName)) {
						YFCElement andEleForMaterialName=getItemListInDoc.getDocumentElement().createChild("PrimaryInformation");
						andEleForMaterialName.setAttribute("ExtendedDescription", materialName);
						andEleForMaterialName.setAttribute("ExtendedDescriptionQryType", "LIKE");
						getItemListInDoc.getDocumentElement().appendChild(andEleForMaterialName);
					}


					YFCDocument getItemListOutputDoc = getServiceInvoker().invokeYantraApi("getItemList", getItemListInDoc, getItemListTemplate());
					/*Below piece of condition is invoked if IsSerial is passed from GpsItecSerialExport*/
					if(TelstraConstants.YES.equalsIgnoreCase(sIsSerial)){
						for(YFCElement serial : serialsList) {
							YFCElement yfcEleSerial = itecInventoryListDoc.getDocumentElement().createChild(TelstraConstants.SERIAL);
							creategetItecInventoryOutput(yfcEleSerial,serial,getItemListOutputDoc);
						}
					}
					else if(!YFCObject.isVoid(getItemListOutputDoc) && getItemListOutputDoc.getDocumentElement().hasChildNodes()) {
						YFCNodeList<YFCElement> items = getItemListOutputDoc.getElementsByTagName("Item");
						for(YFCElement item : items) {
							YFCElement itemEle = itecInventoryListDoc.createElement("Item");
							itemEle.setAttribute("MaterialId", item.getAttribute("ItemID"));
							itemEle.setAttribute("UnitOfMeasure", item.getAttribute("UnitOfMeasure"));
							YFCElement primaryInfoEle= item.getElementsByTagName("PrimaryInformation").item(0);
							if(!YFCObject.isVoid(primaryInfoEle)){
								itemEle.setAttribute("MaterialDescription", primaryInfoEle.getAttribute("Description", ""));
								itemEle.setAttribute("ExtendedDescription", primaryInfoEle.getAttribute("ExtendedDescription", ""));
								itemEle.setAttribute("ItemType", primaryInfoEle.getAttribute("ItemType", ""));
								itemEle.setAttribute("ProductLine", primaryInfoEle.getAttribute("ProductLine", ""));
								itemEle.setAttribute("ManufacturerName", primaryInfoEle.getAttribute("ManufacturerName", ""));
							}						

							YFCElement classificationCode = item.getElementsByTagName("ClassificationCodes").item(0);
							if(!YFCObject.isVoid(classificationCode)) {
								itemEle.setAttribute("Model", classificationCode.getAttribute("Model",""));
								itemEle.setAttribute("CommodityCode", classificationCode.getAttribute("CommodityCode",""));
							}						
							itecInventoryListDoc.getDocumentElement().appendChild(itemEle);												
						}
					}
				}
			}
			return itecInventoryListDoc;

		}
		else {
			/*
			 *  If the search criteria is passed without RevisionNo, Barcode or serial, then the search is performed for the 
			 *  GPS_ITEC_SUPPLY table.  This will result in giving the supply for the search criteria.
			 */

			String isDetail = inXml.getDocumentElement().getAttribute("IsDetail");
			double availableQty = 0;
			YFCElement itemEle = null;
			YFCElement inventoryListEle = null;
			if("Y".equals(isDetail)) {
				itemEle = itecInventoryListDoc.getDocumentElement().createChild("Item");
				YFCDocument itecSupplyListOutDoc = getSupplyDetails(inXml);
				setItemLevelAttributesForDetail(itemEle, inXml.getDocumentElement().getAttribute("MaterialId"),maxRecords);
				if(!YFCObject.isNull(itecSupplyListOutDoc) && itecSupplyListOutDoc.getDocumentElement().hasChildNodes()) {	

					//If IsDetail flag is passed as Y, then it is assumed that the search being performed is for Material details.
					inventoryListEle = itemEle.createChild("InventoryList");

					/*
					 * The statuses to be considered for quantitiy computations are passed as service arguments with comma seperated
					 * values.  
					 */
					String statusesToBeConsidered = getProperty("StatusesToBeConsidered","");
					String[] statuses = statusesToBeConsidered.split(",");
					for(String status : statuses) {
						itemEle.setAttribute(status+"Qty", "0");
					}
				}
				YFCNodeList<YFCElement> supplyEles = itecSupplyListOutDoc.getElementsByTagName("ItecSupply");
				for(YFCElement supply : supplyEles) {
					if(supply.getDoubleAttribute("Quantity") <= 0) {
						continue;
					}
					// Setting the location and inventory level information for the material for Detail API
					YFCElement locationEle = itecInventoryListDoc.getDocumentElement().createChild("Location");
					locationEle.setAttribute("Location", getSPSLocation(supply.getAttribute("Location")));
					locationEle.setAttribute("ITECLocation", supply.getAttribute("ITECLocation"));
					YFCElement inventoryEle = itecInventoryListDoc.getDocumentElement().createChild("Inventory");
					inventoryEle.setAttribute("Qty", supply.getAttribute("Quantity"));
					inventoryEle.setAttribute("Status", supply.getAttribute("Status"));

					String statusesToBeConsidered = getProperty("StatusesToBeConsidered","");
					double supplyQty = supply.getDoubleAttribute("Quantity",0);
					/*
					 * If the supply status is one among the statuses to be considered then the available quantity will be 
					 * computed.  Else the quantity will not be computed for the AvailableQuantity attribute 
					 */
					if(statusesToBeConsidered.contains(supply.getAttribute("Status"))) {
						availableQty = supplyQty + availableQty;
					}
					double statusQuantity = itemEle.getDoubleAttribute(supply.getAttribute("Status")+"Qty",0);
					// The status quantity break up will be computed for every status.
					itemEle.setAttribute(supply.getAttribute("Status")+"Qty", supplyQty+statusQuantity);
					locationEle.appendChild(inventoryEle);
					inventoryListEle.appendChild(locationEle);
				}

				itemEle.setAttribute("AvailableQuantity", availableQty);
				YFCElement alternateItems = addItemAssociations(itecInventoryListDoc , inXml.getDocumentElement().getAttribute("MaterialId"));
				itemEle.appendChild(alternateItems);
				return itecInventoryListDoc;
			}else {
				/*
				 * If IsDetail="N", only the supply level information to be displayed 
				 */

				/*
				 * Calling getItemList API for every material id to set the PrimaryInformation and the classification
				 * code information at the Item element
				 */				

				YFCDocument itemListDoc = null;
				itemListDoc = getItemListDoc(materialId, materialName, maxRecords);

				if(!YFCObject.isVoid(itemListDoc) && itemListDoc.getDocumentElement().hasChildNodes()) {
					YFCNodeList<YFCElement> items = itemListDoc.getElementsByTagName("Item");
					for(YFCElement item : items) {
						itemEle = itecInventoryListDoc.getDocumentElement().createChild("Item");
						itemEle.setAttribute("MaterialId", item.getAttribute("ItemID"));
						itemEle.setAttribute("UnitOfMeasure", item.getAttribute("UnitOfMeasure"));
						YFCElement primaryInfoEle= item.getElementsByTagName("PrimaryInformation").item(0);
						if(!YFCObject.isVoid(primaryInfoEle)){
							itemEle.setAttribute("MaterialDescription", primaryInfoEle.getAttribute("Description", ""));
							itemEle.setAttribute("ExtendedDescription", primaryInfoEle.getAttribute("ExtendedDescription", ""));
							itemEle.setAttribute("ItemType", primaryInfoEle.getAttribute("ItemType", ""));
							itemEle.setAttribute("ProductLine", primaryInfoEle.getAttribute("ProductLine", ""));
							itemEle.setAttribute("ManufacturerName", primaryInfoEle.getAttribute("ManufacturerName", ""));
						}

						YFCElement classificationCode = item.getElementsByTagName("ClassificationCodes").item(0);
						if(!YFCObject.isVoid(classificationCode)) {
							itemEle.setAttribute("Model", classificationCode.getAttribute("Model",""));
							itemEle.setAttribute("CommodityCode", classificationCode.getAttribute("CommodityCode",""));
						}


						itecInventoryListDoc.getDocumentElement().appendChild(itemEle);
					}
				}
				return itecInventoryListDoc;
			}
		}
	}

	private void creategetItecInventoryOutput(YFCElement yfcEleSerial, YFCElement serial, YFCDocument getItemListOutputDoc) {
		yfcEleSerial.setAttribute(TelstraConstants.BAR_CODE, serial.getAttribute(TelstraConstants.BAR_CODE));
		yfcEleSerial.setAttribute(TelstraConstants.EQUIPMENT_NO, serial.getAttribute(TelstraConstants.EQUIPMENT_NO));
		yfcEleSerial.setAttribute(TelstraConstants.ITEC_LOCATION, serial.getAttribute(TelstraConstants.ITEC_LOCATION));
		yfcEleSerial.setAttribute(TelstraConstants.LOCATION, serial.getAttribute(TelstraConstants.LOCATION));
		yfcEleSerial.setAttribute(TelstraConstants.REVISION_NO, serial.getAttribute(TelstraConstants.REVISION_NO));
		yfcEleSerial.setAttribute(TelstraConstants.SERIAL_NO, serial.getAttribute(TelstraConstants.SERIAL_NO));
		yfcEleSerial.setAttribute(TelstraConstants.STATUS, serial.getAttribute(TelstraConstants.STATUS));

		String sMaterialId = serial.getAttribute(TelstraConstants.MATERIAL_ID);
		yfcEleSerial.setAttribute(TelstraConstants.MATERIAL_ID, sMaterialId);


		YFCElement yfcEleItem = XPathUtil.getXPathElement(getItemListOutputDoc, "//Item[@ItemID='"+sMaterialId+"']");
		if(!YFCCommon.isVoid(yfcEleItem)){

			yfcEleSerial.setAttribute(TelstraConstants.UNIT_OF_MEASURE, yfcEleItem.getAttribute(TelstraConstants.UNIT_OF_MEASURE));

			YFCElement yfcElePrimaryInfo= yfcEleItem.getElementsByTagName(TelstraConstants.PRIMARY_INFORMATION).item(0);
			if(!YFCObject.isVoid(yfcElePrimaryInfo)){
				yfcEleSerial.setAttribute(TelstraConstants.DESCRIPTION, yfcElePrimaryInfo.getAttribute(TelstraConstants.DESCRIPTION, ""));
				yfcEleSerial.setAttribute(TelstraConstants.EXTENDED_DESCRIPTION, yfcElePrimaryInfo.getAttribute(TelstraConstants.EXTENDED_DESCRIPTION, ""));
				yfcEleSerial.setAttribute(TelstraConstants.ITEM_TYPE, yfcElePrimaryInfo.getAttribute(TelstraConstants.ITEM_TYPE, ""));
				yfcEleSerial.setAttribute(TelstraConstants.PRODUCT_LINE, yfcElePrimaryInfo.getAttribute(TelstraConstants.PRODUCT_LINE, ""));
				yfcEleSerial.setAttribute(TelstraConstants.MANUFACTURER_NAME, yfcElePrimaryInfo.getAttribute(TelstraConstants.MANUFACTURER_NAME, ""));
			}						

			YFCElement yfcEleClassificationCode = yfcEleItem.getElementsByTagName(TelstraConstants.CLASSIFICATION_CODES).item(0);
			if(!YFCObject.isVoid(yfcEleClassificationCode)) {
				yfcEleSerial.setAttribute(TelstraConstants.MODEL, yfcEleClassificationCode.getAttribute(TelstraConstants.MODEL,""));
				yfcEleSerial.setAttribute(TelstraConstants.COMMODITY_CODE, yfcEleClassificationCode.getAttribute(TelstraConstants.COMMODITY_CODE,""));
			}							
		}						

	}

	/**
	 * 
	 * @param revisionNo
	 * @param serialNo
	 * @param barcode
	 * @param materialName
	 * @param materialId
	 * @param maxRecords
	 * @param itecInventoryListDoc
	 * @return
	 */
	private YFCDocument getItecSerialForMaterialName(String revisionNo,
			String serialNo, String barcode, String materialName,
			String materialId, String maxRecords, YFCDocument itecInventoryListDoc) {
		// TODO Auto-generated method stub

		List<String> lItemID = new ArrayList<>();

		YFCDocument getItemListInDoc = YFCDocument.getDocumentFor("<Item OrganizationCode='"+TelstraConstants.ORG_TELSTRA_SCLSA+"' >"
				+ "<ComplexQuery Operator='AND'> "
				+ "</ComplexQuery> "
				+ "<AdditionalAttributeList> "
				+ "<AdditionalAttribute AttributeDomainID='ItemAttribute' AttributeGroupID='ItemReferenceGroup' Name='SOURCE_SYSTEM' Value='ITEC'/>"
				+ "</AdditionalAttributeList> </Item>");

		if(!YFCObject.isNull(maxRecords)){
			getItemListInDoc.getDocumentElement().setAttribute("MaximumRecords", "500");
		}

		YFCElement complexQuery = getItemListInDoc.getElementsByTagName("ComplexQuery").item(0);
		YFCElement andEleForMaterialID = complexQuery.createChild("And");
		YFCElement orForMaterialID = andEleForMaterialID.createChild("Or");	

		YFCElement expressionEle = orForMaterialID.createChild("Exp");
		expressionEle.setAttribute("Name", "ItemID");
		expressionEle.setAttribute("QryType", "EQ");
		expressionEle.setAttribute("Value", materialId);

		YFCElement andEleForMaterialName=getItemListInDoc.getDocumentElement().createChild("PrimaryInformation");
		andEleForMaterialName.setAttribute("ExtendedDescription", materialName);
		andEleForMaterialName.setAttribute("ExtendedDescriptionQryType", "LIKE");
		getItemListInDoc.getDocumentElement().appendChild(andEleForMaterialName);

		YFCDocument getItemListOutputDoc = getServiceInvoker().invokeYantraApi("getItemList", getItemListInDoc, getItemListTemplate());

		YFCElement eleItemList = getItemListOutputDoc.getDocumentElement();
		for (YFCElement eleItem : eleItemList.getElementsByTagName(TelstraConstants.ITEM)) {
			lItemID.add(eleItem.getAttribute(TelstraConstants.ITEM_ID,""));
		}

		if(lItemID!=null){
			YFCDocument getItecSerialListInDoc = YFCDocument.getDocumentFor("<ItecSerial Barcode='"+barcode+"' BarcodeQryType='FLIKE' "
					+ "MaximumRecords='"+maxRecords+"' RevisionNo='"+revisionNo+"' RevisionNoQryType='FLIKE' SerialNo='"+serialNo+"' SerialNoQryType='FLIKE'>"
					+ "<ComplexQuery Operator='AND'>"
					+ "<And>"
					+ "<Or>"
					+ "</Or>"
					+ "</And>"
					+ "</ComplexQuery> "
					+ "</ItecSerial>");
			for(String sItemId : lItemID){
				YFCElement orMaterialID = getItecSerialListInDoc.getDocumentElement().getElementsByTagName("Or").item(0);
				YFCElement expForMaterialID = orMaterialID.createChild("Exp");
				expForMaterialID.setAttribute("Name", "MaterialId");
				expForMaterialID.setAttribute("QryType", "EQ");
				expForMaterialID.setAttribute("Value", sItemId);
			}

			YFCDocument getItecSerialListOutputDoc = invokeYantraService("GpsGetItecSerialList", getItecSerialListInDoc);

			YFCElement eleItecSerialList = getItecSerialListOutputDoc.getDocumentElement();

			YFCElement eleOutputitecInventoryListDoc = itecInventoryListDoc.getDocumentElement();

			for (YFCElement serial : eleItecSerialList.getElementsByTagName("ItecSerial")) {
				YFCElement yfcEleSerial = eleOutputitecInventoryListDoc.createChild("Serial");
				creategetItecInventoryOutput(yfcEleSerial,serial,getItemListOutputDoc);
			}

		}
		return itecInventoryListDoc;
	}

	/**
	 * This Method will return the SPS Location Description for the corresponding location code passed.
	 * EX:
	 * If the Location is IS01, the value returned will be MELBOURNE
	 * If the Location is IS02, the value returned will be SYDNEY
	 * If the Location is TELSTRA, the value returned will be TELSTRA
	 * @param attribute
	 * @return
	 */
	private String getSPSLocation(String location) {
		if(YFCObject.isVoid(location)) {
			return "";
		}
		YFCDocument docGetCommonCodeListOutput =
				invokeYantraApi(
						TelstraConstants.API_GET_COMMON_CODE_LIST,
						YFCDocument.getDocumentFor("<CommonCode CodeType=\"SPS_LOCATIONS\" CodeValue='"+location+"'/>"));
		YFCElement commonCodeEle = docGetCommonCodeListOutput.getElementsByTagName(TelstraConstants.COMMON_CODE).item(0);
		if (!YFCObject.isVoid(commonCodeEle)) {
			return commonCodeEle.getAttribute(TelstraConstants.CODE_SHORT_DESCRIPTION);
		}
		return "";
	}

	/**
	 * 
	 * @param supply
	 * @param itemEle
	 * This method will set the supply level attributes
	 */
	private void setSupplyAttributesForList(YFCElement supply, YFCElement itemEle) {
		itemEle.setAttribute("MaterialId", supply.getAttribute("MaterialId"));
		itemEle.setAttribute("ITECLocation", supply.getAttribute("ITECLocation"));
		itemEle.setAttribute("Location", getSPSLocation(supply.getAttribute("Location")));
		itemEle.setAttribute("Quantity", supply.getAttribute("Quantity"));
		itemEle.setAttribute("Status", supply.getAttribute("Status"));
	}

	/**
	 * 
	 * @param itemEle
	 * @param materialId
	 * This method will set the item level attributes for detail view at the <Item /> tag.
	 * The attributes includes the classification codes as well
	 */
	private void setItemLevelAttributesForDetail(YFCElement itemEle, String materialId, String maxRecords) {
		YFCDocument itemListDoc = getItemListDoc(materialId, "", maxRecords);
		itemEle.setAttribute("MaterialId", materialId);
		if(!YFCObject.isNull(itemListDoc) && itemListDoc.getDocumentElement().hasChildNodes()) {

			YFCElement primaryInfoEle=itemListDoc.getElementsByTagName("PrimaryInformation").item(0);
			if(!YFCObject.isVoid(primaryInfoEle)){
				itemEle.setAttribute("MaterialDescription", primaryInfoEle.getAttribute("Description"));
				itemEle.setAttribute("ManufacturerName", primaryInfoEle.getAttribute("ManufacturerName"));
				itemEle.setAttribute("ItemType", primaryInfoEle.getAttribute("ItemType"));
				itemEle.setAttribute("ProductLine", primaryInfoEle.getAttribute("ProductLine"));
				itemEle.setAttribute("ExtendedDescription", primaryInfoEle.getAttribute("ExtendedDescription", ""));
			}

			itemEle.setAttribute("UnitOfMeasure", itemListDoc.getElementsByTagName("Item").item(0).getAttribute("UnitOfMeasure"));
			YFCElement classificationCode = itemListDoc.getElementsByTagName("ClassificationCodes").item(0);
			if(!YFCObject.isVoid(classificationCode)) {
				//	itemEle.setAttribute("Model", classificationCode.getAttribute("Model",""));
				itemEle.setAttribute("CommodityCode", classificationCode.getAttribute("CommodityCode",""));
			}
		}
	}

	/**
	 * 
	 * @param itecSupplyListInDoc
	 * @return
	 * This method will get the supply for the Material Passed from the GPS_ITEC_SUPPLY Table
	 */
	private YFCDocument getSupplyDetails(YFCDocument itecSupplyListInDoc) {
		YFCDocument getItecSupplyListOutDoc = getServiceInvoker().invokeYantraService("GpsGetItecSupplyList", itecSupplyListInDoc);
		return getItecSupplyListOutDoc;
	}

	/**
	 * 
	 * @param itecInventoryListDoc
	 * @param itemId
	 * @return
	 * 
	 * This Method will get the item associations for the materials passed along with the inventory information 
	 * available for the associated items in the supply table.
	 */
	private YFCElement addItemAssociations(YFCDocument itecInventoryListDoc, String itemId) {

		YFCElement alternateItems = itecInventoryListDoc.createElement("AlternateItemList");
		YFCDocument getItemAssociationsInDoc = YFCDocument.getDocumentFor("<AssociationList AssociationType='Alternative.Y' ItemID='"+itemId+"' "
				+ "OrganizationCode='"+TelstraConstants.ORG_TELSTRA_SCLSA+"' UnitOfMeasure='"+TelstraConstants.ITEC_INVENTORY_UOM+"'/>");
		/*
		 * Getting the Item associations for the material passed 
		 */
		YFCDocument getItemAssociationsOutDoc = getServiceInvoker().invokeYantraApi("getItemAssociations", getItemAssociationsInDoc, getItemAssociationsTemplate());
		if(getItemAssociationsOutDoc != null && getItemAssociationsOutDoc.getDocumentElement().hasChildNodes()) {
			YFCNodeList<YFCElement> itemList =  getItemAssociationsOutDoc.getElementsByTagName("Item");
			for(YFCElement itemEle : itemList) {
				/*
				 * For every Associated item calling the getItemList API to get the primary information and classifications
				 */
				YFCDocument getItemListOutput = getItemListDoc(itemEle.getAttribute("ItemID"), "", "");
				String itemDesc = "";
				String materialName="";
				if(!YFCObject.isVoid(getItemListOutput)) {
					itemDesc = getItemListOutput.getElementsByTagName("PrimaryInformation").item(0).getAttribute("Description");
					materialName= getItemListOutput.getElementsByTagName("PrimaryInformation").item(0).getAttribute("ExtendedDescription");
				}
				YFCElement classificationCode = getItemListOutput.getElementsByTagName("ClassificationCodes").item(0);
				YFCDocument itecSupplyListInDoc = YFCDocument.getDocumentFor("<ItecInventory MaterialId='"+itemEle.getAttribute("ItemID")+"' />");
				YFCDocument itecSupplyListOutDoc = getSupplyDetails(itecSupplyListInDoc);
				/*
				 * Getting the Supply information for the Associated item passed
				 */
				if(!YFCObject.isVoid(itecSupplyListOutDoc) && itecSupplyListOutDoc.getDocumentElement().hasChildNodes()) {
					/*
					 * If inventory exists in the GPS_ITEC_SUPPLY and GPS_ITEC_SERIAL table, then setting the supply
					 * information and item details for the associated material id
					 */
					YFCNodeList<YFCElement> supplyEles = itecSupplyListOutDoc.getElementsByTagName("ItecSupply");
					for(YFCElement supply : supplyEles) {
						YFCElement alternateItem = itecInventoryListDoc.createElement("AlternateItem");
						alternateItem.setAttribute("MaterialDescription", itemDesc);
						alternateItem.setAttribute("ExtendedDescription", materialName);


						if(!YFCObject.isVoid(classificationCode)) {
							//	alternateItem.setAttribute("Model", classificationCode.getAttribute("Model",""));
							alternateItem.setAttribute("CommodityCode", classificationCode.getAttribute("CommodityCode",""));
						}
						// Setting the supply level attributes for the associated item
						setSupplyAttributesForList(supply, alternateItem);
						alternateItems.appendChild(alternateItem);
					}
				} else {
					/*
					 * If No inventory found for the associated item, then setting the Quantity to 0 along witht he 
					 * Item Primary information and classifications
					 */
					YFCElement alternateItem = itecInventoryListDoc.createElement("AlternateItem");
					alternateItem.setAttribute("MaterialDescription", itemDesc);
					alternateItem.setAttribute("ExtendedDescription", materialName);
					if(!YFCObject.isVoid(classificationCode)) {
						//	alternateItem.setAttribute("Model", classificationCode.getAttribute("Model",""));
						alternateItem.setAttribute("CommodityCode", classificationCode.getAttribute("CommodityCode",""));
					}

					alternateItem.setAttribute("MaterialId", itemEle.getAttribute("ItemID"));
					alternateItem.setAttribute("Quantity", "0");
					alternateItems.appendChild(alternateItem);
				}
			}
		}
		return alternateItems;
	}


	private YFCDocument getItemAssociationsTemplate() {
		YFCDocument itemAssociationsTemplate = YFCDocument.getDocumentFor("<AssociationList> <Association> "
				+ "<Item ItemID='' OrganizationCode='' UnitOfMeasure=''/> "
				+ "</Association> </AssociationList>");
		return itemAssociationsTemplate;
	}

	/**
	 * 
	 * @param itemId
	 * @return
	 * This Method will return the Item Details for the MaterialId passed.
	 */
	private YFCDocument getItemListDoc(String materialId, String materialName, String maxRecords) {
		YFCDocument getItemListInput = getInputDocumentforItemList(materialId, materialName,maxRecords);
		YFCDocument getItemListOutputDoc = getServiceInvoker().invokeYantraApi("getItemList", getItemListInput, getItemListTemplate());
		if(getItemListOutputDoc != null && getItemListOutputDoc.getDocumentElement().hasChildNodes()) {
			/*
			 * If getItemList returns the valid output then return the same
			 */
			return getItemListOutputDoc;
		}
		/*
		 * If getItemList is not valid for the MaterialId passed, then return null
		 */
		return null;
	}

	/**
	 * 
	 * @param itecSerialListInDoc
	 * @return
	 * This method is called to get the ITEC Serials for the input passed
	 */
	private YFCDocument getItecSerialListOutputDocument(YFCDocument itecSerialListInDoc) {
		String sIsSerial = itecSerialListInDoc.getDocumentElement().getAttribute(TelstraConstants.IS_SERIAL);
		if(!TelstraConstants.YES.equals(sIsSerial)){
			itecSerialListInDoc.getDocumentElement().removeAttribute("MaximumRecords");
		}
		YFCDocument getItecSerialListOutDoc = getServiceInvoker().invokeYantraService("GpsGetItecSerialList", itecSerialListInDoc);
		return getItecSerialListOutDoc;
	}

	/**
	 * 
	 * @param itemId
	 * @return
	 * This Method will return the input document for getItemList API
	 */
	private YFCDocument getInputDocumentforItemList(String materialId, String materialName,String maxRecords) {

		YFCDocument itemListInputDoc =  YFCDocument.getDocumentFor("<Item OrganizationCode='"+TelstraConstants.ORG_TELSTRA_SCLSA+"' >"
				+ "<AdditionalAttributeList> "
				+ "<AdditionalAttribute AttributeDomainID='ItemAttribute' AttributeGroupID='ItemReferenceGroup' Name='SOURCE_SYSTEM' Value='ITEC' />"
				+ "</AdditionalAttributeList> </Item>");

		if(!YFCObject.isVoid(materialId)){
			itemListInputDoc.getDocumentElement().setAttribute("ItemID", materialId);
			itemListInputDoc.getDocumentElement().setAttribute("ItemIDQryType", "FLIKE");
		}

		if(!YFCObject.isVoid(materialName)){
			YFCElement primaryInfoEle= itemListInputDoc.getDocumentElement().createChild("PrimaryInformation");
			primaryInfoEle.setAttribute("ExtendedDescription", materialName);
			primaryInfoEle.setAttribute("ExtendedDescriptionQryType","LIKE");

			itemListInputDoc.getDocumentElement().appendChild(primaryInfoEle);
		}

		itemListInputDoc.getDocumentElement().setAttribute("MaximumRecords",maxRecords );
		return itemListInputDoc;	

	}

	/**
	 * 
	 * @return
	 * This method will return he getItemListTemplate in the below format
	 * <ItemList>
	 *    <Item ItemID='' UnitOfMeasure=''>
	 *        <PrimaryInformation ManufacturerName='' Description='' ItemType='' ProductLine='' ExtendedDescription=''/>
	 *        <ClassificationCodes Model='' CommodityCode=''/>
	 *    </Item>
	 * </ItemList>
	 */
	private YFCDocument getItemListTemplate() {
		YFCDocument itemListTemplateDoc = YFCDocument.getDocumentFor("<ItemList><Item ItemID='' UnitOfMeasure=''><PrimaryInformation ManufacturerName='' Description='' ItemType='' ProductLine='' ExtendedDescription='' />"
				+ "<ClassificationCodes Model='' CommodityCode=''/></Item></ItemList>");
		return itemListTemplateDoc;
	}
}
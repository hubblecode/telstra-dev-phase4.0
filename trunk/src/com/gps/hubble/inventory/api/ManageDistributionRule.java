/***********************************************************************************************
 * File	Name		: ManageDistributionRule.java
 *
 * Description		: This class is called on success of manageOrganizationHierarchy API.
 * 						If the organization being added/updated is a Node and tracks inventory, 
 * 						it will be added to 2 distribution rules.
 * 						Distribution Rule 1: Same as the NodeType 
 * 						Distribution Rule 2: ALL
 * 
 * Modification	Log	:
 * ---------------------------------------------------------------------------------------------
 * Ver #	Date			Author					Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0		Nov 02,2016	  	Manish Kumar 		   	Initial	Version 
 * 1.1 		Jun 23,2017		Keerthi Yadav			HUB-9269: Distribution Rules not set up properly
 * ---------------------------------------------------------------------------------------------
 * 
 */
package com.gps.hubble.inventory.api;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSException;

/**
 * Custom API to manage distribution rule on success of manageOrganizationHierarchy API.
 * 
 * @author Manish Kumar
 * @version 1.0
 *
 * Extends AbstractCustomApi Class
 */


public class ManageDistributionRule extends AbstractCustomApi
{
	
	private static YFCLogCategory logger = YFCLogCategory.instance(ManageDistributionRule.class);
	private static String strOtherRegionDescription = "OTHERS";
	
	/*
	 * yfInDoc : Input to the service 
	 * Sample Input: 	<Organization Operation="Manage/Delete" OrganizationCode="831412_N1"/>
	 */
	public YFCDocument invoke(YFCDocument yfcInDoc) throws YFSException 
	{
		LoggerUtil.startComponentLog(logger, this.getClass().getName(),"invoke", yfcInDoc);
		Boolean bIsNode = false;
		String strOrgCode = null;
		String strNodeType = null;
		String strNodeDescription = null;
		String strZipCode = null;
		Boolean bInventoryTracked = false;
		String operation = yfcInDoc.getDocumentElement().getAttribute("Operation","");
		if(!"Delete".equals(operation)) {
		  yfcInDoc =  getOrganizationHierarchy(yfcInDoc);
		  if(YFCObject.isVoid(yfcInDoc)) {
	        return null;
	      }
		}
		
		YFCElement yfcOrgELe = yfcInDoc.getDocumentElement();
		logger.verbose("Input to ManageDistributionRule Class: " + yfcOrgELe.toString());
		YFCElement yfcCorporatePersonInfoELe = yfcOrgELe.getChildElement("CorporatePersonInfo", true);
		//Get the Zip Code for the organization form the getOrganizationHierarchy Output
        strZipCode = yfcCorporatePersonInfoELe.getAttribute("ZipCode","");
        
        /*
         * Call getRegionList API to get the region name for the zip code passed
         */
        List<String> strRegion = getRegionForZipCode(strZipCode);
		bIsNode = yfcOrgELe.getBooleanAttribute("IsNode", false);
		if(bIsNode)
		{
			logger.verbose("The updated organization is a node");
			strOrgCode = yfcOrgELe.getAttribute("OrganizationCode", "");
			YFCElement yfcNodeEle = yfcOrgELe.getChildElement("Node");
			bInventoryTracked = yfcNodeEle.getBooleanAttribute("InventoryTracked", false);
			strNodeType = yfcNodeEle.getAttribute("NodeType", "");
			YFCElement yfcNodeTypeEle = yfcNodeEle.getChildElement("NodeType");
			strNodeDescription = (yfcNodeTypeEle.getAttribute("NodeTypeDescription","")).toUpperCase();
			manageDistributionRule(strOrgCode, strNodeType, strNodeDescription, bInventoryTracked, strRegion);
		}
		
		
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", yfcInDoc);
		return null;
	}
	
	/**
	 * This method will return the RegionName from the getRegionList output for the zipcode passed.
	 * @param strZipCode
	 * @return
	 */
	private List<String> getRegionForZipCode(String strZipCode) {
	  List<String> strRegionList = new ArrayList<String>();
	  if(!YFCObject.isVoid(strZipCode) && strZipCode.length() < 4) {
	    /*
	     * If the length of the zip code is less than 4 then prefixing 0 to make it 4 characters
	     */
	    while(strZipCode.length() < 4) {
	        strZipCode = "0"+strZipCode;
	    }
	  }
      YFCDocument getRegionListInDoc = YFCDocument.getDocumentFor("<Region RegionSchemaName='Australlia'> <PersonInfo Country='AU' ZipCode='"+strZipCode+"'/></Region>");
      YFCDocument getRegionListOutDoc = invokeYantraApi("getRegionList", getRegionListInDoc, getRegionListTemplate());
      if(!YFCObject.isVoid(getRegionListOutDoc) && getRegionListOutDoc.getDocumentElement().hasChildNodes()) {
        YFCNodeList<YFCElement> regions = getRegionListOutDoc.getElementsByTagName("Region");
        for(YFCElement region : regions) {
          if(!strRegionList.contains(region.getAttribute("RegionName"))) {
            strRegionList.add(region.getAttribute("RegionName"));
          }
        }
      }
      return strRegionList;
    }
  
   /**
   * This method will return the template for getRegionList API
   * @return
   */
  private YFCDocument getRegionListTemplate() {
    YFCDocument getRegionListTemplate = YFCDocument.getDocumentFor("<Regions> <RegionSchema RegionSchemaName=''> <Region RegionName=''/> </RegionSchema> </Regions>");
    return getRegionListTemplate;
  }

  private YFCDocument getOrganizationHierarchy(YFCDocument yfcInDoc) {
	  YFCDocument getOrganizationHierarchyInput = YFCDocument.getDocumentFor("<Organization OrganizationCode='"+yfcInDoc.getDocumentElement().getAttribute("OrganizationCode")+"' />");
	  YFCDocument getOrganizationHierarchyTemplate = YFCDocument.getDocumentFor("<Organization IsNode='' OrganizationCode=''> <CorporatePersonInfo />"
	      + "<Node NodeType='' InventoryTracked='' ><NodeType NodeTypeDescription=''/></Node> </Organization>");
	  YFCDocument getOrganizationHierarchyOutput =  invokeYantraApi("getOrganizationHierarchy", getOrganizationHierarchyInput, getOrganizationHierarchyTemplate);
      return getOrganizationHierarchyOutput;
    }

  /**
	 * This method is used to correctly update the distribution rules, 
	 * based on the whether the node tracks inventory or not,
	 * and the NodeType of the node.
	 * 
	 * @param strOrgCode The OrganizationCode of the node being modified
	 * @param strNodeType The NodeType of the node
	 * @param bInventoryTracked Does the node track inventory
	 * @return Nothing
	 * 
	 */
	private void manageDistributionRule(String strOrgCode, String strNodeType, String strNodeTypeDescription, Boolean bInventoryTracked, List<String> regionsList) 
	{
		if(bInventoryTracked)
		{
			logger.verbose("The node tracks inventory");
			//check if the node is added to any distribution rule.
			//If yes, and it does not match the NodeType, remove the node from the distribution rule 
			//and add it to the new distribution rule (if inventory is tracked).
			//Santosh Nagaraj -- And If yes and it does not match the Region, remove the ndoe from the distribution rule and add it to the new distribution rule
			//Finally If the node is not added to any of the distrubution rule, add the node to the distribution rules (based on the the node type, Region and ALL).
			List<String> distributionRuleList = getDistributionRule(strOrgCode);
			if(!(distributionRuleList == null || distributionRuleList.isEmpty()))
			{
				logger.verbose("There are distribution rules defined for the node.");
				logger.verbose("Distribution Rule List : " + distributionRuleList);
				for(String strDistributionRule : distributionRuleList)
				{
					if(!strDistributionRule.equals(strNodeType+"_NT"))
					{
						logger.verbose("The DistributionRule is not defined for the correct NodeType");
						logger.verbose("DistributionRule: " + strDistributionRule);
						logger.verbose("NodeType: " + strNodeType);
						
	                     //HUB-9269 [START]
	                     /*This additional check is done to Delete '_NT' and add it again with the new nodetype or existing */
	                      
	                    if(strDistributionRule.endsWith("NT")){
	                    	deleteFromDistributionRule(strOrgCode, strDistributionRule);
	                    }
	                    
	                      //HUB-9269 [END]
	                    
						addToDistributionRule(strOrgCode, strNodeType+"_NT", strNodeTypeDescription);
					}
					
					if((YFCObject.isVoid(regionsList) || regionsList.size() == 0))
					{
					  logger.verbose("The region is void for the Node");
                      logger.verbose("DistributionRule: " + strDistributionRule);
                      logger.verbose("NodeType: " + strNodeType);
                      
                      //HUB-9269 [START]
                      /*This additional check is done to Delete '_RG' and add OTHER_RG */
                      
	                  if(strDistributionRule.endsWith("RG")){
	                	   deleteFromDistributionRule(strOrgCode, strDistributionRule);
                      }
	                    
	                  //HUB-9269 [END]

                      addToDistributionRule(strOrgCode, "OTHER_RG", strOtherRegionDescription);
					}
					
					if(!regionsList.contains(strDistributionRule) && regionsList.size() > 0)
					{
					  logger.verbose("The DistributionRule is not defined for the correct Region");
                      logger.verbose("DistributionRule: " + strDistributionRule);
                      logger.verbose("Regions: " + regionsList);
                      
                      //HUB-9269 [START]
                      /*This additional check is done to prevent 'NodeType'_NT from being deleted */
                      
                      if(strDistributionRule.endsWith("RG")){
                    	  deleteFromDistributionRule(strOrgCode, strDistributionRule);
                      }
                      
                      //HUB-9269 [END]

                      for(String strRegion : regionsList) {
                        addToDistributionRule(strOrgCode, strRegion+"_RG", strRegion.toUpperCase());
                      }
					}
				}
			}
			else
			{
				addToDistributionRule(strOrgCode, strNodeType, strNodeTypeDescription, "ALL", regionsList);
			}
		}
		else
		{
			logger.verbose("The node does not track inventory");
			//check if the node is added to any distribution rule.
			//If yes, remove the node from all the distribution rules (as it does not track inventory anymore).
			List<String> distributionRuleList = getDistributionRule(strOrgCode);
			if(!(distributionRuleList == null || distributionRuleList.isEmpty()))
			{
				logger.verbose("There are distribution rules defined for the node. "
						+ "Since, this node does not track inventory anymore, "
						+ "deleting the distribution rule for the node");
				deleteFromDistributionRule(strOrgCode, distributionRuleList);
			}
			
		}
		
		
	}

	/**
	 * This method adds the Node to both the DistributionRules. 
	 * Distribution Rule 1: NodeType
	 * Distribution Rule 2: ALL
	 * 
	 * @param strOrgCode The OrganizationCode of the node being modified
	 * @param strNodeType The NodeType of the node
	 * @param strAllDG Add the Node to the 'ALL'DistributionRule as well
	 */
	private void addToDistributionRule(String strOrgCode, String strNodeType, String strNodeTypeDescription, String strAllDG, List<String> regionsList) 
	{
		addToDistributionRule(strOrgCode, strNodeType+"_NT", strNodeTypeDescription);
		addToDistributionRule(strOrgCode, strAllDG, "ALL");
		if(YFCObject.isVoid(regionsList) || regionsList.size() == 0) {
		  addToDistributionRule(strOrgCode, "OTHER_RG", strOtherRegionDescription);
		} else {
		  for(String region : regionsList) {
		    addToDistributionRule(strOrgCode, region+"_RG", region.toUpperCase());
		  }
		}
	}

	/**
	 * This method adds/deletes the Node to the DistributionRule strDistributionRule
	 * @param strOrgCode The OrganizationCode of the node being modified
	 * @param strDistributionRule The Distribution Rule Id
	 * @param strIsActive Whether the ShipNode is active or not.
	 */
	private void manageDistributionRule(String strOrgCode, String strDistributionRule, String strDistributionRuleDescription, String strIsActive) 
	{
		YFCDocument yfcManageDistributionRule = YFCDocument.getDocumentFor("<DistributionRule DefaultFlag='N' "
														+ "Description='"+strDistributionRuleDescription+"' "
														+ "DistributionRuleId='"+strDistributionRule+"' "
														+ "OwnerKey='"+TelstraConstants.ORG_TELSTRA_SCLSA+"' "
														+ "ItemGroupCode='PROD' Purpose='SOURCING'>"
															+ "<ItemShipNodes Reset='N'>"
																+ "<ItemShipNode ActiveFlag='"+strIsActive+"' ItemId='ALL' "
																	+ "ItemType='ALL' Priority='1' "
																	+ "ShipnodeKey='"+strOrgCode+"'/>"
															+ "</ItemShipNodes>"
														+ "</DistributionRule>");
		getServiceInvoker().invokeYantraApi("manageDistributionRule", yfcManageDistributionRule);
	}

	/**
	 * This method removes the Node from all the DistributionRules passed in the distributionRuleList
	 * 
	 * @param strOrgCode The OrganizationCode of the node being modified
	 * @param distributionRuleList The list of distribution rule ids that have the Node
	 */
	private void deleteFromDistributionRule(String strOrgCode, List<String> distributionRuleList) 
	{
		for(String strDistributionRule : distributionRuleList)
		{
			deleteFromDistributionRule(strOrgCode, strDistributionRule);
		}
		
	}

	/**
	 * This method removes the Node from the passed DistributionRule
	 * 
	 * @param strOrgCode The OrganizationCode of the node being modified
	 * @param strDistributionRule The DistributionRule being modified
	 */
	private void deleteFromDistributionRule(String strOrgCode, String strDistributionRule) 
	{
		//manageDistributionRule(strOrgCode, strDistributionRule, "N");
		YFCDocument yfcDeleteDistribution = YFCDocument.getDocumentFor("<ItemShipNode "
													+ "DistributionRuleId='"+strDistributionRule+"' "
													+ "OwnerKey='"+TelstraConstants.ORG_TELSTRA_SCLSA+"' "
													+ "ItemId='ALL' "
													+ "ShipnodeKey='"+strOrgCode+"'/>");
		getServiceInvoker().invokeYantraApi("deleteDistribution", yfcDeleteDistribution);
	}
	
	/**
	 * This method removes the Node from the passed DistributionRule
	 * 
	 * @param strOrgCode The OrganizationCode of the node being modified
	 * @param strDistributionRule The DistributionRule being modified
	 */
	private void addToDistributionRule(String strOrgCode, String strDistributionRule, String strDistributionRuleDescription) 
	{
		manageDistributionRule(strOrgCode, strDistributionRule, strDistributionRuleDescription, "Y");
	}

	/**
	 * This method returns a list of distribution rule ids that have the Node configured in the DistributionRule.
	 * 
	 * @param strOrgCode The OrganizationCode of the node being modified
	 * @return A list of distribution rule ids that has the Node configured.
	 */
	private List<String> getDistributionRule(String strOrgCode) 
	{
		List<String> distributionGroupIdList = new ArrayList<String>();
		YFCDocument yfcGetDistributionListIp = YFCDocument.getDocumentFor("<ItemShipNode ActiveFlag='Y' "
													+ "ShipnodeKey='"+strOrgCode+"' "
													+ "OwnerKey='"+TelstraConstants.ORG_TELSTRA_SCLSA+"'/>");
		YFCDocument yfcGetDitributionListOp = getServiceInvoker().invokeYantraApi("getDistributionList", yfcGetDistributionListIp);
		YFCElement yfcItemShipNodeListEle = yfcGetDitributionListOp.getDocumentElement();
		for(Iterator<YFCElement> itr = yfcItemShipNodeListEle.getChildren("ItemShipNode").iterator();itr.hasNext();)
		{
			YFCElement yfcItemShipNodeEle = itr.next();
			String strDistributionGroupId = yfcItemShipNodeEle.getAttribute("DistributionRuleId");
			if(strDistributionGroupId != null)
			{
				distributionGroupIdList.add(strDistributionGroupId);
			}
		}
		
		return distributionGroupIdList;
	}
}

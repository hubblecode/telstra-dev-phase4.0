package com.gps.hubble.inventory.api;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCException;

public class GpsGetInvDmdTypeList extends AbstractCustomApi{


	private static YFCLogCategory logger = YFCLogCategory.instance(GpsGetInvDmdTypeList.class);
	PreparedStatement statement;
	YFCDocument retDoc;
	Connection connection;

	public YFCDocument invoke(YFCDocument inpDoc)   {

		try{
			LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke",inpDoc);

			connection= getServiceInvoker().getDBConnection();
			try (PreparedStatement ps = createQuery(connection);
					ResultSet rs = ps.executeQuery()) {
				retDoc = constructDocument(rs);
				ps.close();
				rs.close();
			//	connection.close();

			}catch(SQLException sqlEx){
				logger.error("SQLException while executing query ",sqlEx);
				throw new YFCException(sqlEx);
			}		

			return retDoc;
		}finally {

			LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke",inpDoc);
		}
	}

	public YFCDocument constructDocument(ResultSet rs) throws SQLException
	{

		YFCDocument demandTypListDoc= YFCDocument.createDocument("InventoryDemandTypeList");
		YFCElement elem=demandTypListDoc.getDocumentElement();

		while(rs.next()){

			YFCElement node= elem.createChild("InventoryDemandType");

			node.setAttribute("DemandType", rs.getString("DEMAND_TYPE").trim());
			node.setAttribute("Description", rs.getString("DESCRIPTION").trim());

		}

		logger.verbose("Inventory Demand Type List Doc"+ demandTypListDoc);
		return demandTypListDoc;

	}

	private PreparedStatement createQuery(Connection connection) {
		// TODO Auto-generated method stub

		String query="SELECT DEMAND_TYPE,DESCRIPTION FROM YFS_INVENTORY_DEMAND_TYPE " ;

		try {
			statement= connection.prepareStatement(query);			

		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			logger.error("Exception while querying the DemandTypeList:"+ ex);
		}

		return statement;
	}

}

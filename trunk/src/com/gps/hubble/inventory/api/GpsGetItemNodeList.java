package com.gps.hubble.inventory.api;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCException;

/**
 * Custom API to invoke getItemNodeList
 * @author Bridge
 *
 */

public class GpsGetItemNodeList extends AbstractCustomApi {
	private static YFCLogCategory logger = YFCLogCategory.instance(GpsGetItemNodeList.class);
	PreparedStatement statement;
	YFCDocument retDoc;
	Connection connection;
	public YFCDocument invoke(YFCDocument inpDoc)   {

		try{

			LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke",inpDoc);

			connection= getServiceInvoker().getDBConnection();
			try (PreparedStatement ps = createQuery(connection);
					ResultSet rs = ps.executeQuery()) {
				retDoc = constructDocument(rs);
				ps.close();
				rs.close();
				connection.close();

			}catch(SQLException sqlEx){
				logger.error("SQLException while executing query ",sqlEx);
				throw new YFCException(sqlEx);
			}		

			return retDoc;
		}finally {

			LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke",inpDoc);
		}
	}

	public YFCDocument constructDocument(ResultSet rs) throws SQLException
	{

		YFCDocument itemNodeListDoc= YFCDocument.createDocument("ItemShipNodeList");
		YFCElement elem=itemNodeListDoc.getDocumentElement();

		while(rs.next()){

			YFCElement node= elem.createChild("ItemShipNode");
			node.setAttribute("DistributionRuleId", rs.getString("DISTRIBUTION_RULE_ID").trim());
			node.setAttribute("ShipnodeKey", rs.getString("SHIPNODE_KEY").trim());
			node.setAttribute("Description", rs.getString("DESCRIPTION").trim());

		}
		logger.verbose("Item Ship Node Document"+ itemNodeListDoc);
		return itemNodeListDoc;

	}

	private PreparedStatement createQuery(Connection connection) {
		// TODO Auto-generated method stub

		String query="SELECT YISN.DISTRIBUTION_RULE_ID,YISN.SHIPNODE_KEY,YSN.DESCRIPTION "+
				" FROM YFS_ITEM_SHIP_NODE YISN "+
				" JOIN YFS_SHIP_NODE YSN ON YISN.SHIPNODE_KEY=YSN.SHIPNODE_KEY "+
				" WHERE YSN.INVENTORY_TRACKED= ? "+
				" ORDER BY YSN.DESCRIPTION ";

		try {
			statement= connection.prepareStatement(query);
			statement.setString(1, "Y");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return statement;
	}



}

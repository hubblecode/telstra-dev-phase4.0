/***********************************************************************************************
 * File Name        : GpsItecSerialExport.java
 *
 * Description      : This class prepares the data for the Itec serial export
 * 
 * Modification Log :
 * ---------------------------------------------------------------------------------------------
 * Ver #    Date            Author                  Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0      Sep 06,2017     Prateek Kumar          Initial Version
 * ---------------------------------------------------------------------------------------------
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
 **********************************************************************************************/
package com.gps.hubble.inventory.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.json.JSONException;
import org.apache.commons.json.JSONObject;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.utils.GpsJSONXMLUtil;
import com.sterlingcommerce.woodstock.util.Base64;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSException;

/**
 * 
 * @author Prateek
 *
 */
public class GpsItecSerialExport  extends AbstractCustomApi {
	private static YFCLogCategory logger = YFCLogCategory.instance(GpsItecInventoryExport.class);

	@Override
	public YFCDocument invoke(YFCDocument yfcInDoc) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", yfcInDoc);

		YFCDocument yfcOpDoc = getItecSerialExportData(yfcInDoc);

		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", yfcInDoc);

		return yfcOpDoc;
	}

	/**
	 * 
	 * @param yfcInDoc
	 * @return
	 */
	private YFCDocument getItecSerialExportData(YFCDocument yfcInDoc) {

		YFCElement yfcInEle = yfcInDoc.getDocumentElement();

		YFCDocument yfcOpDoc = YFCDocument.createDocument(TelstraConstants.EXPORT_DATA);
		YFCElement yfcEleOpDocRoot = yfcOpDoc.getDocumentElement();
		YFCElement yfcEleColumnList = yfcEleOpDocRoot.createChild(TelstraConstants.COLUMN_LIST);
		YFCElement yfcEleDataList = yfcEleOpDocRoot.createChild(TelstraConstants.DATA_LIST);

		String sSearchCriteria =	yfcInEle.getAttribute(TelstraConstants.EXPORT_CRITERIA);
		String sDecodedCriteria = new String(Base64.decode(sSearchCriteria.getBytes()));

		try {
			JSONObject criteriaJSON = new JSONObject(sDecodedCriteria);
			JSONObject searchCriteriaObj = (JSONObject)criteriaJSON.get("SearchCriteria");
			YFCDocument criteriaDoc = GpsJSONXMLUtil.getXmlFromJSON(searchCriteriaObj,TelstraConstants.ITEC_INVENTORY);
			criteriaDoc.getDocumentElement().setAttribute(TelstraConstants.IS_SERIAL, TelstraConstants.YES);
			YFCDocument yfcDocGetItecInventoryOp = getServiceInvoker().invokeYantraService("GpsGetItecInventory", criteriaDoc);
			if(yfcDocGetItecInventoryOp.getDocumentElement().hasChildNodes()){								

				/* Below method will populate the column list tag*/
				prepareColumnElement(yfcEleColumnList);
				/* Below method will populate the data list tag */
				createDataRows(yfcDocGetItecInventoryOp,yfcEleDataList);
			}		
		} catch (JSONException je) {

			LoggerUtil.errorLog("JSON Parse Error", logger, je);
			throw new YFSException("JSON Parse Error");

		}

		return yfcOpDoc;
	}

	/**
	 * 
	 * @param yfcDocGetItecInventoryOp
	 * @param yfcEleDataList
	 */
	private void createDataRows(YFCDocument yfcDocGetItecInventoryOp, YFCElement yfcEleDataList) {

		Map<String,String> mapLocationDesc = callGetCommonCodeList(TelstraConstants.SPS_LOC_CODE_TYPE);

		for(YFCElement yfcEleItecSerial : yfcDocGetItecInventoryOp.getElementsByTagName(TelstraConstants.SERIAL)){

			YFCElement yfcEleData = yfcEleDataList.createChild(TelstraConstants.DATA);
			yfcEleData.setAttributes(yfcEleItecSerial.getAttributes());
			yfcEleData.setAttribute(TelstraConstants.LOCATION, mapLocationDesc.get(yfcEleItecSerial.getAttribute(TelstraConstants.LOCATION)));
		}
	}

	/**
	 * 
	 * @param yfcEleColumnList
	 * @return
	 */
	private void prepareColumnElement(YFCElement yfcEleColumnList) {

		YFCDocument docTemplateCommonCodeList = YFCDocument.getDocumentFor("<CommonCodeList><CommonCode CodeValue='' CodeShortDescription='' /></CommonCodeList>");
		YFCDocument docGetCommonCodeListOutput = invokeYantraApi(TelstraConstants.API_GET_COMMON_CODE_LIST,
				YFCDocument.getDocumentFor("<CommonCode CodeType='ITEC_SERIAL_EXPORT'><OrderBy><Attribute Desc='N' Name='CodeLongDescription' /></OrderBy></CommonCode>"),docTemplateCommonCodeList);
		for (YFCElement eleCommonCode : docGetCommonCodeListOutput.getElementsByTagName(TelstraConstants.COMMON_CODE)) {
			YFCElement yfcEleColumn = yfcEleColumnList.createChild("Column");
			yfcEleColumn.setAttribute("Name", eleCommonCode.getAttribute(TelstraConstants.CODE_VALUE,""));
			yfcEleColumn.setAttribute("Label", eleCommonCode.getAttribute(TelstraConstants.CODE_SHORT_DESCRIPTION,""));			
			yfcEleColumn.setAttribute(TelstraConstants.ESCAPE_CHARS, "Y");

		}
	}

	/**
	 * 
	 * @param sCodeType
	 * @return
	 */
	private Map<String, String> callGetCommonCodeList(String sCodeType) {

		Map<String, String> mapLocationDesc = new HashMap<>();

		YFCDocument yfcDocGetCommonCodeListTemp = YFCDocument.getDocumentFor("<CommonCodeList> <CommonCode CodeShortDescription='' CodeType='' CodeValue='' /> </CommonCodeList>");
		YFCDocument yfcDocGetCommonCodeListOp =
				invokeYantraApi(
						TelstraConstants.API_GET_COMMON_CODE_LIST,
						YFCDocument.getDocumentFor("<CommonCode CodeType='"+sCodeType+"'/>"),yfcDocGetCommonCodeListTemp);

		if (yfcDocGetCommonCodeListOp.getDocumentElement().hasChildNodes()) {

			for(YFCElement yfcEleCommonCode : yfcDocGetCommonCodeListOp.getElementsByTagName(TelstraConstants.COMMON_CODE)){

				String sCodeValue = yfcEleCommonCode.getAttribute(TelstraConstants.CODE_VALUE);
				String sCodeShortDesc = yfcEleCommonCode.getAttribute(TelstraConstants.CODE_SHORT_DESCRIPTION);
				mapLocationDesc.put(sCodeValue, sCodeShortDesc);
			}
		}
		return mapLocationDesc;
	}


}
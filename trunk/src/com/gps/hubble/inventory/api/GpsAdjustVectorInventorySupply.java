/***********************************************************************************************
 * File Name        : GpsAdjustVectorInventorySupply.java
 *
 * Description      : Custom API for processing EDI Vendor Inventory and Vector Inventory supply along with serials.
 * 
 * Modification Log :
 * ---------------------------------------------------------------------------------------------
 * Ver #    Date            Author                              Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0		19/01/2017		Prateek Kumar						Initial Version
 * 1.1		27/06/2017		Keerthi Yadav						HUB-9415: Adding Reason Text while calling Adjust Inventory for Audit
 * 1.2		17/08/2017		Prateek Kumar						HUB-9465 : Allowing non-onhand serial to be committed as well
 * ---------------------------------------------------------------------------------------------
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
 **********************************************************************************************/
package com.gps.hubble.inventory.api;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.constants.TelstraErrorCodeConstants;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSException;

/**
 * 
 * @author Prateek
 *
 */
public class GpsAdjustVectorInventorySupply extends AbstractCustomApi {

	private static YFCLogCategory logger = YFCLogCategory.instance(GpsAdjustVectorInventorySupply.class);
	boolean isVendor = false;

	/**
	 * Base Method execution starts here
	 * 
	 */

	/*
	 * Sample input to the method
	 * 
		<Items ApplyDifferences="Y" BatchMode="Y"  CompleteInventoryFlag="N" ShipNode="Required" YantraMessageGroupID="YYYYMMDD">
		   <Item InventoryOrganizationCode="Required" ItemID="Required" UnitOfMeasure="">
		      <Supplies>
		         <Supply AvailabilityType="" Quantity=""  SupplyType="">
		            <Serials>
		               <Serial SerialNo=""/>
		            </Serials>
		         </Supply>
		      </Supplies>
		   </Item>
		</Items> 
	 */

	@Override
	public YFCDocument invoke(YFCDocument yfcInDoc) throws YFSException {

		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", yfcInDoc);
		boolean bInventoryAdjusted = false;

		String sIncomingShipNode = yfcInDoc.getDocumentElement().getAttribute(TelstraConstants.SHIP_NODE);

		YFCNodeList<YFCElement> yfcNlItem = yfcInDoc.getElementsByTagName(TelstraConstants.ITEM);
		/*
		 * Looping through the Item tag
		 */

		for (YFCElement yfcEleItem : yfcNlItem) {

			String sItemId = yfcEleItem.getAttribute(TelstraConstants.ITEM_ID);
			/*
			 * The unit of measure coming from Vector is different from the one coming from Integrals plus. So fetching the unit of measure from 
			 * item created in Sterling. If the item id does not exist, then considering incoming unit of measure.
			 */
			String sUom = getUomFromItemDetails(sItemId);
			if(YFCCommon.isStringVoid(sUom)){
				sUom = yfcEleItem.getAttribute(TelstraConstants.UNIT_OF_MEASURE);
			}
			String sInventoryOrganizationCode = yfcEleItem.getAttribute(TelstraConstants.INVENTORY_ORGANIZATION_CODE,
					TelstraConstants.ORG_TELSTRA);
			/*
			 * This method returns the inventory item key by calling
			 * getInvetoryItemList api
			 */
			String sInventoryItemKey = getInventoryItemKey(sItemId, sUom, sInventoryOrganizationCode);

			YFCNodeList<YFCElement> yfcNlSupply = yfcEleItem.getElementsByTagName(TelstraConstants.SUPPLY);

			/*
			 * Looping through the supply tag
			 */
			for (YFCElement yfcEleSupply : yfcNlSupply) {

				LoggerUtil.verboseLog("Supply tag: ", logger, yfcEleSupply);

				YFCNodeList<YFCElement> yfcNlSerial = yfcEleSupply.getElementsByTagName(TelstraConstants.SERIAL);
				String sSupplyType = yfcEleSupply.getAttribute(TelstraConstants.SUPPLY_TYPE);
				String sAvailabilityType = yfcEleSupply.getAttribute(TelstraConstants.AVAILABILITY_TYPE);
				double dQty = yfcEleSupply.getDoubleAttribute(TelstraConstants.QUANTITY, 0.0);

				/*
				 * Assumption: If the serial number is not present in the
				 * message, it means that the stock received is absolute
				 * 
				 */
				if (yfcNlSerial.getLength() == 0) {

					adjustInventory(TelstraConstants.ABSOLUTE, sItemId, sIncomingShipNode, dQty, sSupplyType, sUom,
							sAvailabilityType,"");
					bInventoryAdjusted = true;
				}

				/*
				 * Looping through the serial tag if its there
				 */
				for (YFCElement yfcEleSerial : yfcNlSerial) {

					String sSerialNo = yfcEleSerial.getAttribute(TelstraConstants.SERIAL_NO);

					if (YFCCommon.isStringVoid(sSerialNo)) {
						continue;
					}
					LoggerUtil.verboseLog("Serial Number tag: ", logger, yfcEleSerial);

					/*
					 * call get serial num list service for the given serial
					 * number
					 */

					YFCDocument yfcDocGetSerialNumberListOp = callGetSerialNumList(sSerialNo, sInventoryItemKey);

					YFCNodeList<YFCElement> yfcnlSerialNumber = yfcDocGetSerialNumberListOp
							.getElementsByTagName(TelstraConstants.SERIAL_NUMBER);
					/*
					 * Combination of serial number and inventory item key
					 * should be unique. If multiple record is found for this
					 * combination, throw and exception
					 */
					if (yfcnlSerialNumber.getLength() > 1) {
						LoggerUtil.errorLog("Multiple records found for the given serial number: ", logger, sSerialNo);
						throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.VECTOR_INV_MULTIPLE_SERIAL_NUM,
								new YFSException());
					}

					/*
					 * Retrieving the inventory status, at node and ship node
					 * value from gps_serial_num table for the passed serial
					 * number
					 */
					if (yfcnlSerialNumber.getLength() != 0) {

						YFCElement yfcEleSerialNum = yfcDocGetSerialNumberListOp
								.getElementsByTagName(TelstraConstants.SERIAL_NUMBER).item(0);
						String sAtNode = yfcEleSerialNum.getAttribute(TelstraConstants.AT_NODE);
						String sShipNode = yfcEleSerialNum.getAttribute(TelstraConstants.SHIP_NODE);
						String sSerialInventoryStatus = yfcEleSerialNum.getAttribute(TelstraConstants.INVENTORY_STATUS);

						/*
						 * If the serial is present in the node
						 */
						if (TelstraConstants.YES.equalsIgnoreCase(sAtNode)
								&& !sSupplyType.equalsIgnoreCase(TelstraConstants.COMMITTED)) {
							/*
							 * If the supply type in the incoming message is
							 * other than committed
							 */

							if (!sShipNode.equalsIgnoreCase(sIncomingShipNode)
									|| !sSupplyType.equalsIgnoreCase(sSerialInventoryStatus)) {

								/*
								 * Decreasing the inventory by 1 for the
								 * existing ship node and inventory status and
								 * increasing the inventory by 1 for the
								 * incoming node and status.
								 */
								adjustInventory(TelstraConstants.ADJUSTMENT, sItemId, sShipNode, -1,
										sSerialInventoryStatus, sUom, sAvailabilityType,sSerialNo);								
								adjustInventory(TelstraConstants.ADJUSTMENT, sItemId, sIncomingShipNode, 1, sSupplyType,
										sUom, sAvailabilityType,sSerialNo);
								updateInventoryAudit(sSerialNo, sShipNode, sSerialInventoryStatus, sAtNode,
										sIncomingShipNode, sSupplyType, TelstraConstants.YES);
								/*
								 * There could be scenario where inventory item
								 * key is generated only by the above inventory
								 * adjustment. So fetching it, if it is void.
								 */
								if (YFCCommon.isStringVoid(sInventoryItemKey)) {
									sInventoryItemKey = getInventoryItemKey(sItemId, sUom, sInventoryOrganizationCode);
								}
								updateSerialNum(sSerialNo, sInventoryItemKey, sIncomingShipNode, sSupplyType,
										TelstraConstants.YES, true);
							}
						}
						/*
						 * if the supply type in the message is Committed, it
						 * means inventory is already shipped or is going to be
						 * shipped. So reduce the inventory by 1 and update
						 * serial num table with at node as N
						 */
						else if (TelstraConstants.YES.equalsIgnoreCase(sAtNode)
								&& sSupplyType.equalsIgnoreCase(TelstraConstants.COMMITTED)) {
						    /*
						     * HUB-9465
						     * if(!YFCObject.equals(sSerialInventoryStatus, TelstraConstants.ONHAND)) {
						      LoggerUtil.errorLog("Only the ONHAND inventory can be committed: ", logger,
				                    "throwing exception");
				              throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.VECTOR_INV_INV_COMMITMENT_FAILED,
				                    new YFSException());
						    }*/
							adjustInventory(TelstraConstants.ADJUSTMENT, sItemId, sShipNode, -1,
									sSerialInventoryStatus, sUom, sAvailabilityType,sSerialNo);
							updateInventoryAudit(sSerialNo, sShipNode, sSerialInventoryStatus, sAtNode,
									sIncomingShipNode, sSupplyType, TelstraConstants.NO);
							/*
							 * There could be scenario where inventory item key
							 * is generated only by the above inventory
							 * adjustment. So fetching it, if it is void.
							 */
							if (YFCCommon.isStringVoid(sInventoryItemKey)) {
								sInventoryItemKey = getInventoryItemKey(sItemId, sUom, sInventoryOrganizationCode);
							}
							updateSerialNum(sSerialNo, sInventoryItemKey, sIncomingShipNode, sSupplyType,
									TelstraConstants.NO, true);
						}

						/*
						 * If the serial is not present in the node (At node
						 * value is N)
						 */
						else {
							String sAtNodeUpdate = TelstraConstants.NO;
							/*
							 * This is receipt scenario, where the serial is not
							 * present in any node and it is getting received.
							 * In this scenario, inventory will be adjusted and
							 * serial num table will be updated
							 */
							if (!sSupplyType.equalsIgnoreCase(TelstraConstants.COMMITTED)) {
								adjustInventory(TelstraConstants.ADJUSTMENT, sItemId, sIncomingShipNode, 1, sSupplyType,
										sUom, sAvailabilityType,sSerialNo);
								sAtNodeUpdate = TelstraConstants.YES;
								updateInventoryAudit(sSerialNo, sShipNode, sSerialInventoryStatus, sAtNode,
										sIncomingShipNode, sSupplyType, sAtNodeUpdate);
							}

							if (YFCCommon.isStringVoid(sInventoryItemKey)) {
								sInventoryItemKey = getInventoryItemKey(sItemId, sUom, sInventoryOrganizationCode);
							}
							/*
							 * Since the serial which is getting shipped out is
							 * not present in the node, we are not going to
							 * adjust the inventory assuming it has already been
							 * adjusted. Only serial num table will get updated.
							 */
							updateSerialNum(sSerialNo, sInventoryItemKey, sIncomingShipNode, sSupplyType, sAtNodeUpdate,
									true);
						}
					}
					/*
					 * If the incoming serial is not present in the
					 * GPS_SERIAL_NUM table
					 */
					else {
						String sAtNodeUpdate = TelstraConstants.NO;
						/*
						 * If the supply type is other than committed, increase
						 * the supply and update the serial num table with At
						 * Node as Y If the supply type is committed, only
						 * update the serial num table with At Node as N
						 */
						if (!sSupplyType.equalsIgnoreCase(TelstraConstants.COMMITTED)) {
							sAtNodeUpdate = TelstraConstants.YES;
							adjustInventory(TelstraConstants.ADJUSTMENT, sItemId, sIncomingShipNode, 1, sSupplyType,
									sUom, sAvailabilityType,sSerialNo);
							updateInventoryAudit(sSerialNo, null, null, null, sIncomingShipNode, sSupplyType, sAtNodeUpdate);
							
						}

						if (YFCCommon.isStringVoid(sInventoryItemKey)) {
							sInventoryItemKey = getInventoryItemKey(sItemId, sUom, sInventoryOrganizationCode);
						}
						updateSerialNum(sSerialNo, sInventoryItemKey, sIncomingShipNode, sSupplyType, sAtNodeUpdate, false);
					}

					/*
					 * We are decreasing quantity by 1 because serial
					 * corresponding to this quantity has already been
					 * processed.
					 */
					dQty = dQty - 1;
				}

				/*
				 * If there is mismatch in the number of serial and quantity or
				 * if the input message doesn't have serials in it below method
				 * will adjust the inventory for the remaining quantity
				 */
				if (dQty > 0 && TelstraConstants.ONHAND.equalsIgnoreCase(sSupplyType) && !bInventoryAdjusted) {
					adjustInventory(TelstraConstants.ADJUSTMENT, sItemId, sIncomingShipNode, dQty, sSupplyType, sUom,
							sAvailabilityType,"");
				}
			}
		}
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", yfcInDoc);
		return yfcInDoc;
	}

	/**
	 * 
	 * @param sSerialNo - Serial number of the stock
	 * @param sCurrentLocation - Current location of the stock
	 * @param sCurrentStatus - Current status of the stock
	 * @param sCurrentAtNodeFlag - Current At node flag value
	 * @param sNewLocation - New location of the stock
	 * @param sNewStatus - New status of the stock
	 * @param sNewAtNodeFlag - New at node flag value
	 */
	private void updateInventoryAudit(String sSerialNo, String sCurrentLocation, String sCurrentStatus, String sCurrentAtNodeFlag,
			String sNewLocation, String sNewStatus, String sNewAtNodeFlag) {

		YFCDocument yfcDocCreateInvAudit = YFCDocument.getDocumentFor("<VectorInventoryAudit/>");
		YFCElement yfcEleInvAudit = yfcDocCreateInvAudit.getDocumentElement();
		yfcEleInvAudit.setAttribute(TelstraConstants.SERIAL_NO, sSerialNo);
		yfcEleInvAudit.setAttribute(TelstraConstants.CURRENT_LOCATION, sCurrentLocation);
		yfcEleInvAudit.setAttribute(TelstraConstants.CURRENT_STATUS, sCurrentStatus);
		yfcEleInvAudit.setAttribute(TelstraConstants.CURRENT_AT_NODE_FLAG, sCurrentAtNodeFlag);
		yfcEleInvAudit.setAttribute(TelstraConstants.NEW_LOCATION, sNewLocation);
		yfcEleInvAudit.setAttribute(TelstraConstants.NEW_STATUS, sNewStatus);
		yfcEleInvAudit.setAttribute(TelstraConstants.NEW_AT_NODE_FLAG, sNewAtNodeFlag);

		LoggerUtil.verboseLog("GpsAdjustVectorInventorySupply::updateInventoryAudit::yfcDocCreateInvAudit ", logger, yfcDocCreateInvAudit);
		invokeYantraService(TelstraConstants.GPS_CREATE_VECTOR_INVENTORY_AUDIT, yfcDocCreateInvAudit);
	}

	/**
	 * This method calls the get item list api with the passed item id in the parameter and returns unit of measure corresponding to it.
	 * @param sItemId
	 * @return sUnitOfMeasure
	 */

	private String getUomFromItemDetails(String sItemId) {

		YFCDocument yfDocGetItemListIp =  YFCDocument.getDocumentFor("<Item/>");
		yfDocGetItemListIp.getDocumentElement().setAttribute(TelstraConstants.ITEM_ID, sItemId);
		YFCDocument yfDocGetItemListTemp =  YFCDocument.getDocumentFor("<ItemList><Item ItemID='' UnitOfMeasure=''/></ItemList>");

		YFCDocument yfDocGetItemListOp = invokeYantraApi(TelstraConstants.API_GET_ITEM_LIST, yfDocGetItemListIp, yfDocGetItemListTemp);
		String sUnitOfMeasure = XPathUtil.getXpathAttribute(yfDocGetItemListOp, "//Item/@UnitOfMeasure");
		return sUnitOfMeasure;
	}

	/**
	 * This method call get serial num list service for the given serial number
	 * 
	 * @param sSerialNo
	 * @param sInventoryItemKey
	 * @return
	 */
	private YFCDocument callGetSerialNumList(String sSerialNo, String sInventoryItemKey) {

		YFCDocument yfcDocGetSerialNumList = YFCDocument.getDocumentFor("<SerialNum />");
		YFCElement yfcEleGetSerialNumListRoot = yfcDocGetSerialNumList.getDocumentElement();
		yfcEleGetSerialNumListRoot.setAttribute(TelstraConstants.SERIAL_NO, sSerialNo);
		yfcEleGetSerialNumListRoot.setAttribute(TelstraConstants.INVENTORY_ITEM_KEY, sInventoryItemKey);
		YFCDocument yfcDocGetSerialNumberListOp = invokeYantraService(TelstraConstants.GPS_GET_SERIAL_NUM_LIST,
				yfcDocGetSerialNumList);

		return yfcDocGetSerialNumberListOp;
	}

	/**
	 * This method will update the serial num table with the passed parameter
	 * 
	 * @param sSerialNo
	 * @param sInventoryItemKey
	 * @param sShipNode
	 * @param sSupplyType
	 * @param sAtNode
	 * @param bIsUpdate
	 */

	private void updateSerialNum(String sSerialNo, String sInventoryItemKey, String sShipNode, String sSupplyType,
			String sAtNode, boolean bIsUpdate) {

		/*
		 * If the inventory item key is missing, then throw an exception
		 */
		if (YFCCommon.isStringVoid(sInventoryItemKey)) {
			LoggerUtil.errorLog("Inventory item key was not found for one of the item.: ", logger,
					"throwing exception");
			throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.VECTOR_INV_INV_ITEM_KEY_NOT_FOUND,
					new YFSException());
		}

		YFCDocument yfcDocAdjustInvIp = YFCDocument.getDocumentFor("<SerialNumber/>");
		YFCElement yfcEleAdjustInvIp = yfcDocAdjustInvIp.getDocumentElement();
		yfcEleAdjustInvIp.setAttribute(TelstraConstants.SHIP_NODE, sShipNode);
		yfcEleAdjustInvIp.setAttribute(TelstraConstants.INVENTORY_STATUS, sSupplyType);
		yfcEleAdjustInvIp.setAttribute(TelstraConstants.AT_NODE, sAtNode);
		yfcEleAdjustInvIp.setAttribute(TelstraConstants.INVENTORY_ITEM_KEY, sInventoryItemKey);
		yfcEleAdjustInvIp.setAttribute(TelstraConstants.SERIAL_NO, sSerialNo);

		LoggerUtil.verboseLog("GpsAdjustVectorInventorySupply :: updateSerial :: yfcDocAdjustInvIp", logger,
				yfcDocAdjustInvIp);
		if (bIsUpdate) {
			invokeYantraService(TelstraConstants.GPS_CHANGE_SERIAL_NUM, yfcDocAdjustInvIp);
		} else {
			invokeYantraService(TelstraConstants.GPS_CREATE_SERIAL_NUM, yfcDocAdjustInvIp);
		}
	}

	/**
	 * This method will adjust the inventory based on the passed parameter
	 * 
	 * @param sAdjustmentType
	 * @param sItemId
	 * @param sShipNode
	 * @param dQty
	 * @param sSupplyType
	 * @param sAvailabilityType
	 */

	private void adjustInventory(String sAdjustmentType, String sItemId, String sShipNode, double dQty,
			String sSupplyType, String sUoM, String sAvailabilityType, String sSerialNo) {

		//HUB-9415[START]
		/*Added the Serial No as a argument value wherever this method was being called*/
		
		String sReasonText= "";
		if(!YFCObject.isVoid(sSerialNo)){
			sReasonText = "Inventory Supply adjusted for Vendor/Vector Inventory for Serial No : " +sSerialNo ;
		}else{
			sReasonText = "Inventory Supply adjusted for Vendor/Vector Inventory";
		}

		//HUB-9415[END]

		YFCDocument yfcDocAdjustInvIp = YFCDocument.getDocumentFor("<Items><Item/></Items>");
		YFCElement yfcEleItem = yfcDocAdjustInvIp.getElementsByTagName(TelstraConstants.ITEM).item(0);
		yfcEleItem.setAttribute(TelstraConstants.ADJUSTMENT_TYPE, sAdjustmentType);
		yfcEleItem.setAttribute(TelstraConstants.ITEM_ID, sItemId);
		yfcEleItem.setAttribute(TelstraConstants.SHIP_NODE, sShipNode);
		yfcEleItem.setDoubleAttribute(TelstraConstants.QUANTITY, dQty);
		yfcEleItem.setAttribute(TelstraConstants.SUPPLY_TYPE, sSupplyType);
		yfcEleItem.setAttribute(TelstraConstants.AVAILABILITY, sAvailabilityType);
		yfcEleItem.setAttribute(TelstraConstants.UNIT_OF_MEASURE, sUoM);
		yfcEleItem.setAttribute(TelstraConstants.REASON_TEXT, sReasonText);
		LoggerUtil.verboseLog("GpsAdjustVectorInventorySupply :: adjustInventory ::", logger, yfcDocAdjustInvIp);
		invokeYantraApi(TelstraConstants.API_ADJUST_INVENTORY, yfcDocAdjustInvIp);

	}

	/**
	 * This method returns the inventory item key by calling getInvetoryItemList
	 * api
	 * 
	 * @param sItemId
	 * @param sUom
	 * @param sInventoryOrganizationCode
	 * @return
	 */

	private String getInventoryItemKey(String sItemId, String sUom, String sInventoryOrganizationCode) {

		YFCDocument yfcDocGetInventoryItemListIp = YFCDocument
				.getDocumentFor("<InventoryItem InventoryOrganizationCode='' ItemID='' UnitOfMeasure=''/>");
		YFCElement yfcEleInventoryItem = yfcDocGetInventoryItemListIp.getDocumentElement();
		yfcEleInventoryItem.setAttribute(TelstraConstants.INVENTORY_ORGANIZATION_CODE, sInventoryOrganizationCode);
		yfcEleInventoryItem.setAttribute(TelstraConstants.ITEM_ID, sItemId);
		yfcEleInventoryItem.setAttribute(TelstraConstants.UNIT_OF_MEASURE, sUom);

		LoggerUtil.verboseLog("GpsAdjustVectorInventorySupply::getInventoryItemKey::yfcDocGetInventoryItemListIp",
				logger, yfcDocGetInventoryItemListIp);

		YFCDocument yfcDocGetInventoryItemListTemp = YFCDocument.getDocumentFor(
				"<InventoryList TotalInventoryItemList=''> <InventoryItem InventoryItemKey='' InventoryOrganizationCode='' ItemID=''  UnitOfMeasure=''/> </InventoryList>");

		YFCDocument yfcDocGetInventoryItemListOp = invokeYantraApi(TelstraConstants.GET_INVENTORY_ITEM_LIST,
				yfcDocGetInventoryItemListIp, yfcDocGetInventoryItemListTemp);
		LoggerUtil.verboseLog("GpsAdjustVectorInventorySupply::getInventoryItemKey::yfcDocGetInventoryItemListOp",
				logger, yfcDocGetInventoryItemListOp);

		YFCElement yfcEleInventoryItemOp = yfcDocGetInventoryItemListOp
				.getElementsByTagName(TelstraConstants.INVENTORY_ITEM).item(0);
		String sInventoryItemKey = TelstraConstants.BLANK;
		if (!YFCCommon.isVoid(yfcEleInventoryItemOp)) {
			sInventoryItemKey = yfcEleInventoryItemOp.getAttribute(TelstraConstants.INVENTORY_ITEM_KEY, "");
		}
		return sInventoryItemKey;
	}
}

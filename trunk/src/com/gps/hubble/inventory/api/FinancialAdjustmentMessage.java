package com.gps.hubble.inventory.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.framework.service.ServiceInvoker;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.utils.ExcelUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSException;

/**
 * 
 * @author Prateek
 *
 */
public class FinancialAdjustmentMessage {

	private static YFCLogCategory logger = YFCLogCategory.instance(FinancialAdjustmentMessage.class);
	private ServiceInvoker serviceInvoker = null;


	/**
	 * 
	 * @param workbook
	 * @param serviceInvoker 
	 * @return
	 */
	public void prepareFinancialAdjustmentMessage(Workbook workbook, ServiceInvoker serviceInvoker) {

		setServiceInvoker(serviceInvoker);

		Sheet doneSheet = workbook.getSheet(TelstraConstants.DONE_SHEET);
		Iterator<Row> rowIterator = doneSheet.iterator();

		Map<String, List<String>> mapShipNodeItemQty = new HashMap<>();
		boolean isHeader = true;
		while (rowIterator.hasNext()) {
			if(isHeader){
				isHeader = false;
				continue;
			}
			Row row = rowIterator.next();
			String sShipNode = ExcelUtil.getCellvalue(row, 0);
			String sItemId = ExcelUtil.getCellvalue(row, 1);
			String sQuantity = ExcelUtil.getCellvalue(row, 2);

			if (mapShipNodeItemQty.containsKey(sShipNode)) {
				List<String> lItemQty = mapShipNodeItemQty.get(sShipNode);
				if (YFCCommon.isVoid(lItemQty)) {
					lItemQty = new ArrayList<>();
					mapShipNodeItemQty.put(sShipNode, lItemQty);
				}
				lItemQty.add(sItemId + "_" + sQuantity);
			} else {
				List<String> lItemQty = new ArrayList<>();
				lItemQty.add(sItemId + "_" + sQuantity);
				mapShipNodeItemQty.put(sShipNode, lItemQty);
			}
		}

		LoggerUtil.verboseLog(
				"FinancialAdjustmentMessage :: prepareFinancialAdjustmentMessage :: map ShipNode and ItemQty\n", logger,
				mapShipNodeItemQty);

		for (Map.Entry<String, List<String>> entry : mapShipNodeItemQty.entrySet()) {

			YFCDocument docFinancialAdjMsg = YFCDocument.createDocument(TelstraConstants.ITEMS);
			YFCElement eleItems = docFinancialAdjMsg.getDocumentElement();

			String sShipNode = entry.getKey();
			YFCDocument docGetCustomerListOp = callGetCustomerList(sShipNode);
			YFCElement eleCustomer = docGetCustomerListOp.getElementsByTagName(TelstraConstants.CUSTOMER).item(0);
			if(!YFCCommon.isVoid(eleCustomer)){
				updateItemsElement(docGetCustomerListOp, sShipNode, eleItems);
				List<String> lItemQty = entry.getValue();

				for (String sItemIdQty : lItemQty) {

					YFCElement eleItem = docFinancialAdjMsg.createElement(TelstraConstants.ITEM);
					eleItems.appendChild(eleItem);

					String[] parts = sItemIdQty.split(TelstraConstants.UNDERSCORE_STRING);
					String sItemID = parts[0];
					String sQty = parts[1];

					eleItem.setAttribute(TelstraConstants.ITEM_ID, sItemID);
					eleItem.setAttribute(TelstraConstants.QUANTITY, sQty);
					eleItem.setAttribute(TelstraConstants.SHIP_NODE, sShipNode);
					eleItem.setAttribute(TelstraConstants.UNIT_OF_MEASURE, getUnitOfMeasure(sItemID));
					if(StringUtils.isNotBlank(sQty)&&StringUtils.isNumeric(sQty)&&Integer.valueOf(sQty)>0){
						eleItem.setAttribute(TelstraConstants.ORDER_TYPE, TelstraConstants.RECOVERY_CODE);
					}
					else{
						eleItem.setAttribute(TelstraConstants.ORDER_TYPE, TelstraConstants.ISSUE_CODE);
					}
				}			
				getServiceInvoker().invokeYantraService(TelstraConstants.GPS_SEND_FINANCIAL_ADJUSTMENT_MESSAGE, docFinancialAdjMsg);
			}
		}
	}

	private YFCDocument callGetCustomerList(String sShipNode) {

		YFCDocument docGetCustomerListIp = YFCDocument
				.getDocumentFor("<Customer ExternalCustomerID='" + sShipNode + "'/>");
		LoggerUtil.verboseLog("FinancialAdjustmentMessage :: updateItemsElement :: getCustomerList input doc\n", logger,
				docGetCustomerListIp);
		YFCDocument docGetCustomerListTemp = YFCDocument.getDocumentFor(
				"<Customer ExternalCustomerID=\"\"> <Consumer BillingAddressKey=\"\"> <BillingPersonInfo AddressLine1=\"\" State=\"\" PersonID=\"\" City=\"\" Country=\"\" FirstName=\"\" ZipCode=\"\" /> </Consumer> </Customer>");
		LoggerUtil.verboseLog("FinancialAdjustmentMessage :: updateItemsElement :: getCustomerList template doc\n",
				logger, docGetCustomerListTemp);
		YFCDocument docGetCustomerListOp = getServiceInvoker().invokeYantraApi(TelstraConstants.API_GET_CUSTOMER_LIST,
				docGetCustomerListIp, docGetCustomerListTemp);
		LoggerUtil.verboseLog("FinancialAdjustmentMessage :: updateItemsElement :: getCustomerList output doc\n",
				logger, docGetCustomerListOp);

		return docGetCustomerListOp;

	}

	/**
	 * 
	 * @param sItemID
	 * @return
	 */
	private String getUnitOfMeasure(String sItemID) {

		String sUnitOfMeasure = TelstraConstants.BLANK;
		YFCDocument docGetItemListIp = YFCDocument.getDocumentFor("<Item ItemID='" + sItemID + "'/>");
		LoggerUtil.verboseLog("FinancialAdjustmentMessage :: getUnitOfMeasure :: getItemList input doc\n", logger,
				docGetItemListIp);
		YFCDocument docGetItemListTemp = YFCDocument
				.getDocumentFor("<ItemList><Item ItemID=\"\" OrganizationCode=\"\" UnitOfMeasure=\"\" /></ItemList>");
		LoggerUtil.verboseLog("FinancialAdjustmentMessage :: getUnitOfMeasure :: getItemList template doc\n", logger,
				docGetItemListTemp);
		YFCDocument docGetItemListOp = getServiceInvoker().invokeYantraApi(TelstraConstants.API_GET_ITEM_LIST,
				docGetItemListIp, docGetItemListTemp);
		LoggerUtil.verboseLog("FinancialAdjustmentMessage :: getUnitOfMeasure :: getItemList output doc\n", logger,
				docGetItemListOp);

		YFCElement eleItem = docGetItemListOp.getElementsByTagName(TelstraConstants.ITEM).item(0);
		if (!YFCCommon.isVoid(eleItem)) {
			sUnitOfMeasure = eleItem.getAttribute(TelstraConstants.UNIT_OF_MEASURE);
		}
		return sUnitOfMeasure;
	}

	/**
	 * 
	 * @param docGetCustomerListOp 
	 * @param sShipNode
	 * @param eleItems
	 */
	private void updateItemsElement(YFCDocument docGetCustomerListOp, String sShipNode, YFCElement eleItems) {

		YFCElement eleBillingPersonInfo = docGetCustomerListOp.getElementsByTagName(TelstraConstants.BILLING_PERSON_INFO).item(0);
		if(!YFCCommon.isVoid(eleBillingPersonInfo)){
			eleItems.setAttribute(TelstraConstants.VIRTUAL_STORE_STATE, eleBillingPersonInfo.getAttribute(TelstraConstants.STATE));
			eleItems.setAttribute(TelstraConstants.STORE_DAC_CODE, eleBillingPersonInfo.getAttribute(TelstraConstants.PERSON_ID));
			eleItems.setAttribute(TelstraConstants.STORE_ADDRESS_LINE_1, eleBillingPersonInfo.getAttribute(TelstraConstants.ADDRESS_LINE_1));
			eleItems.setAttribute(TelstraConstants.STORE_CITY, eleBillingPersonInfo.getAttribute(TelstraConstants.CITY));
			eleItems.setAttribute(TelstraConstants.STORE_COUNTRY, eleBillingPersonInfo.getAttribute(TelstraConstants.COUNTRY));
			eleItems.setAttribute(TelstraConstants.STORE_NAME, eleBillingPersonInfo.getAttribute(TelstraConstants.FIRST_NAME));
			eleItems.setAttribute(TelstraConstants.STORE_ZIP_CODE, eleBillingPersonInfo.getAttribute(TelstraConstants.ZIP_CODE));
			eleItems.setAttribute(TelstraConstants.COST_CENTER, getCommonCodeValue(TelstraConstants.COST_CENTER_CODE_TYPE, sShipNode));
			eleItems.setAttribute(TelstraConstants.NETWORK_ID, getCommonCodeValue(TelstraConstants.NETWORK_ID_CODE_TYPE, sShipNode));
			eleItems.setAttribute(TelstraConstants.ACTIVITY_CODE, getCommonCodeValue(TelstraConstants.ACTIVITY_CODE_CODE_TYPE, sShipNode));
		}
	}

	/**
	 * 
	 * @param sCodeType
	 * @param sShipNode 
	 * @return sCodeShortDesc
	 */
	private String getCommonCodeValue(String sCodeType, String sShipNode) {

		String sCodeShortDesc = TelstraConstants.BLANK;
		YFCDocument docGetCommonCodeListIp = YFCDocument.getDocumentFor("<CommonCode CodeType='" + sCodeType + "' CodeValue='"+sShipNode+"'/>");
		LoggerUtil.verboseLog("StockMovementReportFileProcessor :: getCommonCodeCodeValueList :: getCommonCode input doc\n",
				logger, docGetCommonCodeListIp);
		YFCDocument docGetCommonCodeListTemp = YFCDocument.getDocumentFor(
				"<CommonCodeList><CommonCode CodeType=\"\" CodeValue=\"\" CodeShortDescription=\"\"/></CommonCodeList>");
		LoggerUtil.verboseLog(
				"StockMovementReportFileProcessor :: getCommonCodeCodeValueList :: getCommonCode template doc\n", logger,
				docGetCommonCodeListTemp);
		YFCDocument docGetCommonCodeListOp = getServiceInvoker().invokeYantraApi(
				TelstraConstants.API_GET_COMMON_CODE_LIST, docGetCommonCodeListIp, docGetCommonCodeListTemp);
		LoggerUtil.verboseLog(
				"StockMovementReportFileProcessor :: getCommonCodeCodeValueList :: getCommonCode output doc\n", logger,
				docGetCommonCodeListOp);

		YFCElement eleCommonCode = docGetCommonCodeListOp.getElementsByTagName(TelstraConstants.COMMON_CODE).item(0);
		if(!YFCCommon.isVoid(eleCommonCode)){
			sCodeShortDesc = eleCommonCode.getAttribute(TelstraConstants.CODE_SHORT_DESCRIPTION);
		}
		return sCodeShortDesc;
	}


	protected void setServiceInvoker(ServiceInvoker serviceInvoker){
		this.serviceInvoker=serviceInvoker;
	}

	protected ServiceInvoker getServiceInvoker() {
		return serviceInvoker;
	}
}



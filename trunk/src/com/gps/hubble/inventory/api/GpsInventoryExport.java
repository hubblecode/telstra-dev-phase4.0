/***********************************************************************************************
 * File Name        : GpsInventoryExport.java
 *
 * Description      :The Below Code is invoked by the Process File Agent with Criteria ID as INVENTORY_EXPORT to get the data for the
 * 					 excel export for Inventory from the Sterling Portal. A view table is implemented to fetch
 * 					 the relevant data based on the search criteria passed from the input. Every Inventory Key and Ship Node combination would have a unique
 * 					 row in the Excel.
 *
 * Modification Log :
 * ---------------------------------------------------------------------------------------------
 * Ver #  	Date              Author				Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0		09-08-2017      Keerthi Yadav           Initial Version 
 * ---------------------------------------------------------------------------------------------
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
 **********************************************************************************************/

package com.gps.hubble.inventory.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.json.JSONException;
import org.apache.commons.json.JSONObject;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.utils.GpsJSONXMLUtil;
import com.sterlingcommerce.woodstock.util.Base64;
import com.yantra.custom.dbi.GPS_Inventory_Export_Vw;
import com.yantra.shared.dbclasses.GPS_Inventory_Export_VwDBHome;
import com.yantra.shared.ycp.YFSContext;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dblayer.PLTQueryBuilder;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSException;

/**
 * The class is executed to send the inventory data back to the Inventory Agent
 * @author Keerthi
 *
 */
public class GpsInventoryExport extends AbstractCustomApi {
	private static YFCLogCategory logger = YFCLogCategory.instance(GpsInventoryExport.class);

	@Override
	public YFCDocument invoke(YFCDocument inXml) {

		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke",inXml);

		YFCDocument outXml = getinventoryUpdate(inXml);

		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke",inXml);

		return outXml;
	}

	/**
	 * The below method will return the Column and Row data Xml Document
	 * in the execute jobs of the agent.
	 * @param inXml
	 * @return outXml
	 */
	private YFCDocument getinventoryUpdate(YFCDocument inXml) {
		YFCElement inElem = inXml.getDocumentElement();
		String sExportType = inElem.getAttribute("ExportType","");
		String searchCriteria =	inElem.getAttribute("ExportCriteria");
		String decodedCriteria = new String(Base64.decode(searchCriteria.getBytes()));
		YFCDocument outDoc = YFCDocument.createDocument("ExportData");
		YFCElement rootElem = outDoc.getDocumentElement();
		YFCElement eleColumnList = rootElem.createChild("ColumnList");
		YFCElement eleDataList = rootElem.createChild("DataList");
		YFCDocument templateDoc = YFCDocument.createDocument(GPS_Inventory_Export_VwDBHome.getInstance().getXMLName());
		YFCElement templateElem = templateDoc.getDocumentElement();

		try{	
			YFSContext ctx = getServiceInvoker().getContext();
			PLTQueryBuilder pltQry = new PLTQueryBuilder();
			JSONObject criteriaJSON = new JSONObject(decodedCriteria);
            JSONObject searchCriteriaObj = (JSONObject)criteriaJSON.get("SearchCriteria");
            YFCDocument criteriaDoc = GpsJSONXMLUtil.getXmlFromJSON(searchCriteriaObj,TelstraConstants.INVENTORY_ITEM);

			/*This Hash Map will contain InventoryItemKey and the Data element combination which the element from
			 * the getInventoryItemList Api added with the supply and demand Qtys*/

			HashMap<String, YFCElement>InventoryItemKeyDataList= new HashMap<String, YFCElement>();

			/*This list is used to populate the column names and also the Quantity Attributes in the Data Row Element*/
			List<String> lcolumnNames = new ArrayList<>();
			List<String> linventoryItemKeys = new ArrayList<>();


			lcolumnNames = getCommonCodeForInvColumn(eleColumnList,lcolumnNames); 

			/* The below will take inputs of ItemID , Product Line and will do a like search and return the get Inventory List*/
			YFCDocument inventoryItemListOutDoc = getServiceInvoker().invokeYantraService("GpsInventoryList", criteriaDoc);
			YFCElement eleInventoryItemList = inventoryItemListOutDoc.getDocumentElement();

			/*If the Document from eleInventoryItemList is blank then return the outdoc  */

			if(YFCObject.isVoid(eleInventoryItemList)){
				return outDoc;
			}

			/**
			 * The required Data Attributes are added into a single Element and the Primary Information Tag is removed
			 * This element will be used to construct the required attributes
			 */
			for(YFCElement eleInventoryItem : eleInventoryItemList.getElementsByTagName(TelstraConstants.INVENTORY_ITEM) ){
				YFCElement eleItem = eleInventoryItem.getElementsByTagName(TelstraConstants.ITEM).item(0);
				YFCElement elePrimaryInfo = eleItem.getElementsByTagName(TelstraConstants.PRIMARY_INFORMATION).item(0);
				eleInventoryItem.setAttribute("ProductLine", elePrimaryInfo.getAttribute("ProductLine",""));
				eleInventoryItem.setAttribute("ItemDescription", elePrimaryInfo.getAttribute("ShortDescription"));

				if(!YFCObject.isVoid(eleItem.getAttribute("LinkedItemID",""))){
					String sLinkedItemID = eleItem.getAttribute("LinkedItemID","");
					String sItemID = eleInventoryItem.getAttribute("ItemID","");
					String sLinkedInventoryOrganizationCode = eleItem.getAttribute("LinkedInventoryOrganizationCode","");
					String sLinkedUOM = eleItem.getAttribute("LinkedUOM","");

					eleInventoryItem.setAttribute("ItemID", sLinkedItemID+" ("+sItemID+")");
					//eleInventoryItem.setAttribute("UnitOfMeasure", sLinkedUOM);

					String sInventoryItemKey = getInventoryKeyforIntegral(sLinkedItemID,sLinkedInventoryOrganizationCode,sLinkedUOM);
					if(!YFCObject.isVoid(sInventoryItemKey)){
						eleInventoryItem.setAttribute(TelstraConstants.INVENTORY_ITEM_KEY, sInventoryItemKey);
					}
				}
				eleInventoryItem.removeAttribute(TelstraConstants.INVENTORY_ORGANIZATION_CODE);
				eleInventoryItem.removeChild(eleItem);
			}

			/*Renamed to 'Data' from 'InventoryItem' in the next for loop */
			for(YFCElement eleInventoryItem : eleInventoryItemList.getElementsByTagName(TelstraConstants.INVENTORY_ITEM) ){
				if(!YFCObject.isVoid(eleInventoryItem.getAttribute(TelstraConstants.INVENTORY_ITEM_KEY,""))){
					inventoryItemListOutDoc.getDocument().renameNode(eleInventoryItem.getDOMNode(),null, "Data");
					InventoryItemKeyDataList.put(eleInventoryItem.getAttribute(TelstraConstants.INVENTORY_ITEM_KEY,""), (YFCElement) eleInventoryItem);
					linventoryItemKeys.add(eleInventoryItem.getAttribute(TelstraConstants.INVENTORY_ITEM_KEY,""));
				}
			}

			/*If the Document from InventoryItemKeyDataList Map is blank then return the outdoc  */
			if(InventoryItemKeyDataList.isEmpty()) {
				return outDoc;
			}	

			String [] ohArr=InventoryItemKeyDataList.keySet().toArray(new String[0]);

			/*The below logic will return a result from the view with a where clause of all the inventory item keys from the array*/
			if(ohArr.length > 0) {
				pltQry.appendWHERE();
				pltQry.appendStringForIN(GPS_Inventory_Export_VwDBHome.getInstance(), "INVENTORY_ITEM_KEY", ohArr);
				logger.debug("The Generated Query is "+pltQry.getReadableWhereClause());				
			}

			YFCDocument docInventoryResult = YFCDocument.getDocumentFor("<InventoryResultList />");
			YFCElement eleInventory = docInventoryResult.getDocumentElement();
			@SuppressWarnings("unchecked")
			List<GPS_Inventory_Export_Vw> resultList = GPS_Inventory_Export_VwDBHome.getInstance().listWithWhereUnCommited(ctx, pltQry, Integer.MAX_VALUE);
			for(Iterator<GPS_Inventory_Export_Vw> iter=resultList.iterator();iter.hasNext();) {
				GPS_Inventory_Export_Vw row = iter.next();
				YFCElement rowElem = row.getXML(outDoc, templateElem,"InventoryResultRow");
				eleInventory.importNode(rowElem);
			}

			/*The below method will do all the Quantity Computation and create Data Rows for every Inventory Item Key and Ship Node combination */
			createDataRows(eleInventory,eleDataList,InventoryItemKeyDataList,lcolumnNames,linventoryItemKeys);

		}catch(JSONException je) {
			je.printStackTrace();
			throw new YFSException("JSON Parse Error");
		}
		return outDoc;
	}

	private String getInventoryKeyforIntegral(String sLinkedItemID,
			String sLinkedInventoryOrganizationCode, String sLinkedUOM) {
		String sInventoryItemKey="";
		if(!YFCObject.isNull(sInventoryItemKey) || !YFCObject.isNull(sLinkedUOM) || !YFCObject.isNull(sLinkedItemID)){
			YFCDocument inventoryLisInDoc = YFCDocument.getDocumentFor("<InventoryItem  InventoryOrganizationCode='"+sLinkedInventoryOrganizationCode+"' ItemID='"+sLinkedItemID+"' UnitOfMeasure='"+sLinkedUOM+"' />");
			YFCDocument inventoryListTempDoc = YFCDocument.getDocumentFor("<InventoryList TotalNumberOfRecords=''><InventoryItem InventoryItemKey=''/></InventoryList>");
			YFCDocument inventoryListoutDoc = invokeYantraApi("getInventoryItemList", inventoryLisInDoc,inventoryListTempDoc);
			YFCElement eleInventoryList =  inventoryListoutDoc.getDocumentElement();
			if(eleInventoryList.getDoubleAttribute(TelstraConstants.TOTAL_NUMBER_OF_RECORDS,0.0) == 1){
				YFCElement eleInventory = eleInventoryList.getElementsByTagName(TelstraConstants.INVENTORY_ITEM).item(0);
				sInventoryItemKey = eleInventory.getAttribute(TelstraConstants.INVENTORY_ITEM_KEY,"");
				return sInventoryItemKey;
			}
		}
		return sInventoryItemKey;
	}


	/**
	 * The Supply and demand Qtys are computed below along with the Availability Qty
	 * @param eleInventoryResultList
	 * @param eleDataList
	 * @param inventoryItemKeyDataList
	 * @param lcolumnNames
	 */
	private void createDataRows(YFCElement eleInventoryResultList, YFCElement eleDataList, HashMap<String, YFCElement> inventoryItemKeyDataList, List<String> lcolumnNames,List<String> linventoryItemKeys) {


		HashMap<String, YFCElement>InventoryItemKeyShipNodeDataList= new HashMap<String, YFCElement>();

		/*From the Query result the iteration starts with each record */
		for(YFCElement eleInventoryResultRow : eleInventoryResultList.getElementsByTagName("InventoryResultRow")){
			String sInventoryItemKey = eleInventoryResultRow.getAttribute(TelstraConstants.INVENTORY_ITEM_KEY,"");
			String sShipNode = eleInventoryResultRow.getAttribute("ShipNodeKey");

			if(InventoryItemKeyShipNodeDataList.containsKey(sInventoryItemKey+"||"+sShipNode)){
				YFCElement eleInventoryItem = InventoryItemKeyShipNodeDataList.get(sInventoryItemKey+"||"+sShipNode);
				if(!eleInventoryItem.hasAttribute("ONHAND_QTY")){
					setDefaultQtyToDataRow(eleInventoryItem,lcolumnNames); 	
				}
				String sInventoryStatus = eleInventoryResultRow.getAttribute("InventoryStatus");
				String sRecordType = eleInventoryResultRow.getAttribute("RecordType");
				double dRowResultQuantity = eleInventoryResultRow.getDoubleAttribute("Quantity",0.0);
				double dAvbQuantity = eleInventoryItem.getDoubleAttribute(TelstraConstants.AVAILABILITY_QTY,0.0);

				/*The Quantity for SUPPLY Type are all supply types are added into the element and for ONHAND the availability qty is also updated*/
				if(YFCObject.equals(sRecordType, "SUPPLY")){
					if(dRowResultQuantity>0.0){
						double dDataRowQuantity = eleInventoryItem.getDoubleAttribute(sInventoryStatus+"_QTY",0.0);
						double dQty = dRowResultQuantity + dDataRowQuantity; 
						eleInventoryItem.setDoubleAttribute(sInventoryStatus+"_QTY",dQty);
					}
					if(YFCObject.equals(sInventoryStatus, "ONHAND")){
						if(dRowResultQuantity>0.0){
							double dQty = dRowResultQuantity + dAvbQuantity; 
							eleInventoryItem.setDoubleAttribute(TelstraConstants.AVAILABILITY_QTY,dQty);
						}
					}
					/*The Quantity for DEMAND Type are all demand types are all added and stamped as Total demand qty and for demand type
					 *  the availability qty is also updated*/

				}else if(YFCObject.equals(sRecordType, "DEMAND")){
					if(dRowResultQuantity>0.0){
						double dTotalDemandQuantity = eleInventoryItem.getDoubleAttribute("TOTAL_"+sRecordType+"_QTY",0.0);
						double dTotalQty = dRowResultQuantity + dTotalDemandQuantity; 
						eleInventoryItem.setDoubleAttribute("TOTAL_"+sRecordType+"_QTY",dTotalQty);
						double dQty = dAvbQuantity - dRowResultQuantity; 
						eleInventoryItem.setDoubleAttribute(TelstraConstants.AVAILABILITY_QTY,dQty);
					}
				}
			}else if(inventoryItemKeyDataList.containsKey(sInventoryItemKey)){
				YFCElement eleInventoryItemList = inventoryItemKeyDataList.get(sInventoryItemKey);
				YFCElement eleInventoryItem = YFCDocument.getDocumentFor(eleInventoryItemList.toString()).getDocumentElement();
				if(!eleInventoryItem.hasAttribute("ONHAND_QTY")){
					setDefaultQtyToDataRow(eleInventoryItem,lcolumnNames); 	
				}
				String sInventoryStatus = eleInventoryResultRow.getAttribute("InventoryStatus");
				String sRecordType = eleInventoryResultRow.getAttribute("RecordType");
				double dRowResultQuantity = eleInventoryResultRow.getDoubleAttribute("Quantity",0.0);
				double dAvbQuantity = eleInventoryItem.getDoubleAttribute(TelstraConstants.AVAILABILITY_QTY,0.0);
				eleInventoryItem.setAttribute("SHIP_NODE",sShipNode);

				/*The Quantity for SUPPLY Type are all supply types are added into the element and for ONHAND the availability qty is also updated*/

				if(YFCObject.equals(sRecordType, "SUPPLY")){
					if(dRowResultQuantity>0.0){
						double dDataRowQuantity = eleInventoryItem.getDoubleAttribute(sInventoryStatus+"_QTY",0.0);
						double dQty = dRowResultQuantity + dDataRowQuantity; 
						eleInventoryItem.setDoubleAttribute(sInventoryStatus+"_QTY",dQty);
					}
					if(YFCObject.equals(sInventoryStatus, "ONHAND")){
						if(dRowResultQuantity>0.0){
							double dQty = dRowResultQuantity + dAvbQuantity; 
							eleInventoryItem.setDoubleAttribute(TelstraConstants.AVAILABILITY_QTY,dQty);
						}
					}

					/*The Quantity for DEMAND Type are all demand types are all added and stamped as Total demand qty and for demand type
					 *  the availability qty is also updated*/

				}else if(YFCObject.equals(sRecordType, "DEMAND")){
					if(dRowResultQuantity>0.0){
						double dTotalDemandQuantity = eleInventoryItem.getDoubleAttribute("TOTAL_"+sRecordType+"_QTY",0.0);
						double dTotalQty = dRowResultQuantity + dTotalDemandQuantity; 
						eleInventoryItem.setDoubleAttribute("TOTAL_"+sRecordType+"_QTY",dTotalQty);
						double dQty = dAvbQuantity - dRowResultQuantity; 
						eleInventoryItem.setDoubleAttribute(TelstraConstants.AVAILABILITY_QTY,dQty);
					}
				}
				InventoryItemKeyShipNodeDataList.put(sInventoryItemKey+"||"+sShipNode, eleInventoryItem);
				if(!linventoryItemKeys.isEmpty()) {
					linventoryItemKeys.remove(eleInventoryItem.getAttribute(TelstraConstants.INVENTORY_ITEM_KEY,""));
				}	
			}
		}


		if(!linventoryItemKeys.isEmpty()) {
			for (String sInventoryItemKey : linventoryItemKeys) {
				if(inventoryItemKeyDataList.containsKey(sInventoryItemKey)){
					YFCElement eleInventoryItem = inventoryItemKeyDataList.get(sInventoryItemKey);
					if(!eleInventoryItem.hasAttribute("ONHAND_QTY")){
						setDefaultQtyToDataRow(eleInventoryItem,lcolumnNames); 
						eleInventoryItem.setDoubleAttribute(TelstraConstants.AVAILABILITY_QTY,0.0);
						eleInventoryItem.setDoubleAttribute("TOTAL_DEMAND_QTY",0.0);
						eleInventoryItem.setAttribute("SHIP_NODE","");
					}
					InventoryItemKeyShipNodeDataList.put(sInventoryItemKey, eleInventoryItem);
				}
			}
		}
		
		/*The below for loop is to create the Data element in the DataList*/
		for (YFCElement eleData : InventoryItemKeyShipNodeDataList.values()) {
			double dAvailabilityQty = eleData.getDoubleAttribute(TelstraConstants.AVAILABILITY_QTY,0.0);
			if(dAvailabilityQty<0.0){
				eleData.setDoubleAttribute(TelstraConstants.AVAILABILITY_QTY,0.0);
			}
			eleDataList.importNode(eleData);
		}
	}

	/**
	 * The below method will stamp all the Quantity values as 0.0 as the default value
	 * @param eleinventoryItemListRow
	 * @param lcolumnNames
	 */
	private void setDefaultQtyToDataRow(YFCElement eleinventoryItemListRow,
			List<String> lcolumnNames) {
		for (String sInventoryStatus : lcolumnNames) {
			/** The below check is done to make sure non qty attributes such as ItemId, Description or Product Line are not stamped with 
			 * the Quantity**/
			if(sInventoryStatus.contains("_QTY")){
				eleinventoryItemListRow.setDoubleAttribute(sInventoryStatus, 0.0);
			}
		}
	}

	/**
	 * The below common codes are used to stamp the column values in the Column List for INVENTORY EXPORT common code type
	 * @param eleDataList
	 * @param lcolumnNames
	 * @return
	 */
	private List<String> getCommonCodeForInvColumn(YFCElement eleColumnList, List<String> lcolumnNames) {
		YFCDocument docTemplateCommonCodeList = YFCDocument.getDocumentFor("<CommonCodeList><CommonCode CodeValue='' CodeShortDescription='' /></CommonCodeList>");
		YFCDocument docGetCommonCodeListOutput = invokeYantraApi(TelstraConstants.API_GET_COMMON_CODE_LIST,
				YFCDocument.getDocumentFor("<CommonCode CodeType='INVENTORY_EXPORT'><OrderBy><Attribute Desc='N' Name='CodeLongDescription' /></OrderBy></CommonCode>"),docTemplateCommonCodeList);
		for (YFCElement eleCommonCode : docGetCommonCodeListOutput.getElementsByTagName(TelstraConstants.COMMON_CODE)) {
			YFCElement eleData = eleColumnList.createChild("Column");
			eleData.setAttribute("Name", eleCommonCode.getAttribute(TelstraConstants.CODE_VALUE,""));
			eleData.setAttribute("Label", eleCommonCode.getAttribute(TelstraConstants.CODE_SHORT_DESCRIPTION,""));
			lcolumnNames.add(eleCommonCode.getAttribute(TelstraConstants.CODE_VALUE,""));
			if(!eleCommonCode.getAttribute(TelstraConstants.CODE_VALUE,"").contains("_QTY")){
				eleData.setAttribute("EscapeChars", "Y");
			}
		}
		return lcolumnNames;
	}
}
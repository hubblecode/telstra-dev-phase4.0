package com.gps.hubble.ds;

import org.w3c.dom.Document;

import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;
import com.yantra.yfs.japi.YFSUserExitException;
import com.yantra.ysc.japi.ue.YSCGetExternalDataAccessPolicyOnReadUE;

public class DataAccessPolicyOnReadUE extends AbstractDataAccessCustomApi implements YSCGetExternalDataAccessPolicyOnReadUE{
	private static YFCLogCategory logger = YFCLogCategory.instance(DataAccessPolicyOnReadUE.class);
	 
	@Override
	public YFCDocument invoke(YFCDocument inXml) throws YFSException {
		if(logger.isDebugEnabled()){
			logger.debug("DataAccessPolicyOnReadUE is getting invoked for input : "+inXml);
		}
		if(!isDataAccessPolicyBypassRequired(inXml)){
			if(logger.isDebugEnabled()){
				logger.debug("Data access policy not required..default access policy will be applied");
			}
			return getDataAccessPolicyDocument(true);
		}
		YFCDocument docUser = getUser(inXml);
		if(docUser == null){
			if(logger.isDebugEnabled()){
				logger.debug("Unable to obtain user..default access policy will be applied");
			}
			return getDataAccessPolicyDocument(true);
		}
		if(!isValidVendorUser(docUser)){
			if(logger.isDebugEnabled()){
				logger.debug("Not a valid vendor user..default access policy will be applied");
			}
			return getDataAccessPolicyDocument(true);
		}
		if(logger.isDebugEnabled()){
			logger.debug("Valid vendor user..disabling data access policy");
		}
		return getDataAccessPolicyDocument(false);
	}


	@Override
	public Document getExternalDataAccessPolicyOnRead(
			YFSEnvironment paramYFSEnvironment, Document paramDocument)
			throws YFSUserExitException {
		return invoke(paramYFSEnvironment, paramDocument);
	}
	
	

}

package com.gps.hubble.alerts.api;

import java.util.ArrayList;
import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;

/**
 * 
 * @author Prateek
 *
 */
public class GpsResolveAlertOnOrderCancellation extends AbstractCustomApi{

	private static YFCLogCategory logger = YFCLogCategory.instance(GpsResolveAlertOnOrderCancellation.class);


	/*
	 * @see com.bridge.sterling.framework.api.AbstractCustomApi#invoke(com.yantra.yfc.dom.YFCDocument)
	 */
	@Override
	public YFCDocument invoke(YFCDocument inDoc) {

		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", inDoc);
		YFCElement eleOrder = inDoc.getDocumentElement();
		String sMinOrderStatus = eleOrder.getAttribute(TelstraConstants.MIN_ORDER_STATUS);
		if(TelstraConstants.STATUS_CANCELLED.equalsIgnoreCase(sMinOrderStatus)){

			String sOrderHeaderKey = eleOrder.getAttribute(TelstraConstants.ORDER_HEADER_KEY);
			YFCDocument docGetExceptionListIp = YFCDocument.getDocumentFor("<Inbox Status='CLOSED' StatusQryType='NE' OrderHeaderKey='"+sOrderHeaderKey+"'/>");
			YFCDocument docGetExceptionListTemp = YFCDocument.getDocumentFor("<InboxList> <Inbox ExceptionType=''  Status='' OrderNo='' OrderHeaderKey='' InboxKey=''/> </InboxList>");
			LoggerUtil.verboseLog(this.getClass().getName()+" :: docGetExceptionListIp", logger, docGetExceptionListIp);

			YFCDocument docGetExceptionListOp = invokeYantraApi(TelstraConstants.API_GET_EXCEPTION_LIST, docGetExceptionListIp, docGetExceptionListTemp);
			LoggerUtil.verboseLog(this.getClass().getName()+" :: docGetExceptionListOp", logger, docGetExceptionListOp);

			YFCNodeList<YFCElement> nlInbox = docGetExceptionListOp.getElementsByTagName(TelstraConstants.INBOX);

			//Looping through the excepitonList

			String sDoNotCloseException = getProperty("DoNotCloseExceptions");
			String[] serviceDoNotCloseExceptionList = sDoNotCloseException.split(",");
			ArrayList<String> doNotCloseExceptionArrayList = new ArrayList<String>();
		     for (int i = 0; i < serviceDoNotCloseExceptionList.length; i++) {
		    	 doNotCloseExceptionArrayList.add(serviceDoNotCloseExceptionList[i].trim());
		        }
			

			for(YFCElement eleInbox : nlInbox){
				String sExceptionType=eleInbox.getAttribute("ExceptionType");		
				// Looping through the ExceptionList from arguments that are not to be closed
				if(doNotCloseExceptionArrayList.contains(sExceptionType)){
					// continue if incoming interface is configured
					continue;
				}

				String sInboxKey = eleInbox.getAttribute(TelstraConstants.INBOX_KEY);				
				YFCDocument docResolveExceptionIp = YFCDocument.getDocumentFor("<ResolutionDetails> <Inbox InboxKey='"+sInboxKey+"' Status='OPEN' /> </ResolutionDetails>");
				LoggerUtil.verboseLog(this.getClass().getName()+" :: docResolveExceptionIp", logger, docResolveExceptionIp);
				invokeYantraApi(TelstraConstants.API_RESOLVE_EXCEPTION, docResolveExceptionIp);	

			}
		}
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", inDoc);
		return inDoc;		
	}
}
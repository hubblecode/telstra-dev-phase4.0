/**
 * 
 */
package com.gps.hubble.status.api;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCException;


/**
 * Custom API to construct statusList for multiple process types removing duplicates
 * @author Sambeet Mohanty
 *
 */
public class StatusAggregatorApi extends AbstractCustomApi {

	private YFCDocument retDoc;
	private static YFCLogCategory logger = YFCLogCategory.instance(StatusAggregatorApi.class);
	/* (non-Javadoc)
	 * @see com.bridge.sterling.framework.api.AbstractCustomApi#invoke(com.yantra.yfc.dom.YFCDocument)
	 */
	@Override
	public YFCDocument invoke(YFCDocument inpDoc) throws YFCException  {
		try {
			LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke",inpDoc);
			logger.verbose("inpDoc"+ inpDoc);
			String attri=inpDoc.getDocumentElement().getAttribute("ProcessTypeAttributes");

			ArrayList<String> pipeLineKeyList=new ArrayList<>();

			if(!XmlUtils.isVoid(attri)){
				String[] s= attri.split(",");
				for(String  str:s){
					String pipelineInStr="<DeterminePipelineInput ProcessType='"+str+"'>"+
							"<ConditionVariables EnterpriseCode='"+TelstraConstants.ORG_TELSTRA_SCLSA+"' /></DeterminePipelineInput>";
					YFCDocument pipelineInDoc=YFCDocument.getDocumentFor(pipelineInStr);

					YFCDocument pipelineOutDoc=invokeYantraApi(TelstraConstants.API_DETERMINE_PIPELINE, pipelineInDoc);

					pipeLineKeyList.add(pipelineOutDoc.getDocumentElement().getAttribute("PipelineKey"));

				}

				StringBuilder peocessKeyBuilder=new StringBuilder("(");


				for(int i=0; i< pipeLineKeyList.size() ;i++){
					peocessKeyBuilder.append("?");
					if(i!=pipeLineKeyList.size()-1){
						peocessKeyBuilder.append(",");
					}
					else{
						peocessKeyBuilder.append(")");
					}
					logger.verbose("peocessKeyBuilder"+ peocessKeyBuilder.toString());
				}

				Connection connection = getServiceInvoker().getDBConnection();
				try (PreparedStatement ps = createQuery(connection, peocessKeyBuilder,pipeLineKeyList);
						ResultSet rs = ps.executeQuery()) {
					retDoc = constructDocument(rs);
					ps.close();
					rs.close();

				}catch(SQLException sqlEx){
					logger.error("SQLException while executing query ",sqlEx);
					throw new YFCException(sqlEx);
				}
				
			}
			else {
				throw new YFCException("ProcessTypeKey List is not set");
			}

			return retDoc;
		}finally {
			LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke",inpDoc);
		}
	}

	public PreparedStatement createQuery(Connection connection, StringBuilder processKeyBuilder, ArrayList<String> pipeLineKeyList ) throws SQLException{
		String query= "SELECT DISTINCT YS.STATUS AS STATUS, YS.STATUS_NAME AS STATUS_NAME"
				+" FROM YFS_PIPELINE_PICKUP_TRAN  YPPT JOIN YFS_STATUS YS"
				+" ON YPPT.PROCESS_TYPE_KEY=YS.PROCESS_TYPE_KEY and YPPT.STATUS=YS.STATUS"
				+" WHERE PIPELINE_KEY in "+ processKeyBuilder.toString()
				+" ORDER BY STATUS";

		logger.verbose("pipeLineKeyList size:"+ pipeLineKeyList.size());
		PreparedStatement ps= connection.prepareStatement(query);
		for (int ipsIndex=0; ipsIndex<pipeLineKeyList.size();ipsIndex++){

			logger.verbose("pipeLineKeyList.get(ipsIndex):"+ pipeLineKeyList.get(ipsIndex) +" ipsIndex:"+ ipsIndex);
			ps.setString((ipsIndex+1), pipeLineKeyList.get(ipsIndex));

		}		

		return ps;
	}
	public YFCDocument constructDocument(ResultSet rs) throws SQLException
	{

		YFCDocument statusListDoc= YFCDocument.createDocument("StatusList");
		YFCElement elem=statusListDoc.getDocumentElement();

		while(rs.next()){
			YFCElement statusElem=elem.createChild("Status");

			statusElem.setAttribute("Status", rs.getString("STATUS").trim());
			statusElem.setAttribute("StatusName", rs.getString("STATUS_NAME").trim());
		}
		logger.verbose("statusListDoc"+ statusListDoc);

		return statusListDoc;

	}

}
/***********************************************************************************************
 * File	Name		: IsDuplicateOrderCondition.java
 *
 * Description		: This class is called from the the dynamic condition.
 * 					  It will validate if the order is a duplicate order of an already existing order created 
 * 					  in the last configured days with the configured department codes and order types.
 * 
 * Modification	Log	:
 * ---------------------------------------------------------------------------------------------
 * Ver #	Date			Author					Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0					  	Murali Pachiappan 		Initial	Version
 * 1.1		June 16,2017    Manish Kumar			Changed to code to read the configured OrderTypes 
 * 													to call getOrderList API
 * 1.2		Jun19,2017		Prateek Kumar			HUB-9305: Checking address through item ship to key combination.
 * 													
 * ---------------------------------------------------------------------------------------------
 * 
 */
package com.gps.hubble.condition;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Document;

import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.ycp.japi.YCPDynamicConditionEx;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;

/**
 * Custom validation class to check duplicate orders
 * 
 * @author Murali Pachiappan
 * @version 1.0
 * 
 *          Implements YCPDynamicCondition class
 */
public class IsDuplicateOrderCondition implements YCPDynamicConditionEx {

	private Map<String, String> propertyMap = new HashMap<>();
	private Map<String, List<String>> inputOrderDtlMap = new HashMap<>();
	private static YFCLogCategory logger = YFCLogCategory.instance(IsDuplicateOrderCondition.class);

	public Map<String, List<String>> getInputOrderDtlMap() {
		return inputOrderDtlMap;
	}

	public void setInputOrderDtlMap(Map<String, List<String>> inputOrderDtlMap) {
		this.inputOrderDtlMap = inputOrderDtlMap;
	}


	/**
	 * 
	 * Base method initial execution starts here.
	 * 
	 * Gets input XML from the service and validates for duplicate order by
	 * calling get order list with specific conditions
	 */
	@Override
	public boolean evaluateCondition(YFSEnvironment env, String arg1, Map arg2, Document inXml) {

		YFCDocument outDoc = null;
		Boolean isOrderDuplicate = false;
		try {
			YFCDocument inDoc = prepareInputForGetOrderList(YFCDocument.getDocumentFor(inXml).getDocumentElement());
			LoggerUtil.verboseLog("IsDuplicateOrderCondition :: OrderListIp", logger, inDoc);
			
			YFCDocument inTemp = YFCDocument.getDocumentFor("<OrderList TotalNumberOfRecords='' TotalOrderList=''>"
					+ "<Order BillToID='' DocumentType='' EnterpriseCode='' EntryType='' OrderDate='' OrderHeaderKey='' OrderNo='' OrderType='' ShipNode='' ShipToID='' OrderName=''>"
					+ "<OrderLines TotalNumberOfRecords=''><OrderLine OrderHeaderKey='' OrderLineKey='' OrderedQty='' ShipNode=''   ShipToID=''>"
					+ "<Item ItemID='' ProductClass='' UnitOfMeasure=''>"
					+ "</Item><PersonInfoShipTo PersonInfoKey=''/></OrderLine></OrderLines></Order></OrderList>");

			env.setApiTemplate(TelstraConstants.GET_ORDER_LIST, inTemp.getDocument());
			
			outDoc = YFCDocument.getDocumentFor(YIFClientFactory.getInstance().getApi().invoke(env,
					TelstraConstants.GET_ORDER_LIST, inDoc.getDocument()));
			
			LoggerUtil.endComponentLog(logger, this.getClass().getName(), "OrderListOutput", outDoc);
			env.clearApiTemplate(TelstraConstants.GET_ORDER_LIST);
			String orderListCount = outDoc.getDocumentElement().getAttribute(TelstraConstants.TOTAL_NUMBER_OF_RECORDS);
			if (!"0".equals(orderListCount)) {
				isOrderDuplicate = validateDuplicateOrder(outDoc, YFCDocument.getDocumentFor(inXml));
			}
		} catch (Exception exp) {
			LoggerUtil.endComponentLog(logger, this.getClass().getName(), "evaluateCondition", exp);
		}
		return isOrderDuplicate;
	}

	/**
	 * Method prepares input for Get order list with specific conditions and
	 * complex query
	 * 
	 * @param inEle
	 * @return
	 * @throws ParseException
	 * 
	 *             returns YFCDocument
	 */
	private YFCDocument prepareInputForGetOrderList(YFCElement inEle) throws ParseException {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "prepareInputForGetOrderList", inEle);

		String sDeptCodes = propertyMap.get(TelstraConstants.DEPT_CODES);
		String[] sDeptCodeList = sDeptCodes.split(TelstraConstants.COMMA_SEPARATOR);
		
		String sDayCount = propertyMap.get(TelstraConstants.DAY_COUNT).toString();
		
		String sOrderTypes = propertyMap.get("ORDER_TYPES").toString();
		String[] sOrderTypeList = sOrderTypes.split(TelstraConstants.COMMA_SEPARATOR);

		StringBuilder sbGetOrderListCompleQry = new StringBuilder(
				"<Order EnterpriseCode='' DocumentType='' FromStatus='1100' ToStatus='3700.10000' StatusQryType='BETWEEN' FromOrderDate='' ToOrderDate=''"
				+ " OrderDateQryType='DATERANGE' ShipToID=''> <ComplexQuery Operator='AND'><And><Or>");
		for (String sDeptCode : sDeptCodeList) {
			sbGetOrderListCompleQry
					.append("<Exp Name='DepartmentCode' QryType='EQ' Value='" + sDeptCode.trim() + "' />");
		}
		sbGetOrderListCompleQry.append("</Or><And><Or>");
		for (String sOrderType : sOrderTypeList) {
			sbGetOrderListCompleQry
					.append("<Exp Name='OrderType' QryType='EQ' Value='" + sOrderType.trim() + "' />");
		}
		sbGetOrderListCompleQry.append(
				"</Or></And></And> </ComplexQuery> <OrderLine ShipNode='' ShipNodeQryType='EQ'/><OrderBy><Attribute Desc='Y' Name='OrderHeaderKey' /></OrderBy></Order>");

		YFCDocument getItemListIn = YFCDocument.getDocumentFor(sbGetOrderListCompleQry.toString());

		YFCElement getItemListEle = getItemListIn.getDocumentElement();
		getItemListEle.setAttribute(TelstraConstants.ENTERPRISE_CODE,
				inEle.getAttribute(TelstraConstants.ENTERPRISE_CODE));
		getItemListEle.setAttribute(TelstraConstants.DOCUMENT_TYPE, inEle.getAttribute(TelstraConstants.DOCUMENT_TYPE));
		getItemListEle.setAttribute(TelstraConstants.SHIP_TO_ID, inEle.getAttribute(TelstraConstants.SHIP_TO_ID));
		getItemListEle.setAttribute(TelstraConstants.FROM_ORDER_DATE, computeOrderDate(inEle.getAttribute(TelstraConstants.ORDER_DATE),Integer.parseInt("-"+sDayCount)));
		getItemListEle.setAttribute(TelstraConstants.TO_ORDER_DATE, computeOrderDate(inEle.getAttribute(TelstraConstants.ORDER_DATE), Integer.parseInt(sDayCount)));
		getItemListEle.getChildElement(TelstraConstants.ORDER_LINE).setAttribute(TelstraConstants.SHIP_NODE,
				inEle.getChildElement(TelstraConstants.ORDER_LINES).getChildElement(TelstraConstants.ORDER_LINE)
						.getAttribute(TelstraConstants.SHIP_NODE));
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "prepareInputForGetOrderList", getItemListIn);
		return getItemListIn;

	}

	/**
	 * 
	 * Method to calculate date based by adding the no.of days from the @param
	 * @param sOrderDate 
	 * 
	 * @param daycount
	 * @return
	 * @throws ParseException
	 * 
	 *             returns computed String date
	 */
	public String computeOrderDate(String sOrderDate, int daycount) throws ParseException {

		SimpleDateFormat sdf = new SimpleDateFormat(TelstraConstants.DATE_TIME_FORMAT_2);
		Calendar c = Calendar.getInstance();
		c.setTime(sdf.parse(sOrderDate));
		c.add(Calendar.DATE, daycount);  // number of days to add
		return sdf.format(c.getTime());

	}

	/**
	 * Method to get current date and time
	 * 
	 * @return
	 */
	public static String getCurrentTimeStamp() {
		SimpleDateFormat sdfDate = new SimpleDateFormat(TelstraConstants.DATE_TIME_FORMAT);
		Date now = new Date();
		return sdfDate.format(now);
	}

	/**
	 * Method to validate the duplicate orders from the orderList and newly
	 * created Order
	 * 
	 * @param orderListXml
	 * @param inXml
	 * @return
	 */
	private boolean validateDuplicateOrder(YFCDocument orderListXml, YFCDocument inXml) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "validateDuplicateOrder", orderListXml);
		int inputOrderlineLength = XPathUtil.getXpathNodeList(inXml, "//OrderLine").getLength();

		String inputOrderHeaderKey = inXml.getDocumentElement().getAttribute(TelstraConstants.ORDER_HEADER_KEY);
		if (inputOrderlineLength == 0) {
			return false;
		}
		List<String> lIpItemIdShipToKey = prepareItemIdShipToKeyList(inXml);
		getOrderLineDtls(inXml);
		YFCElement orderListEle = orderListXml.getDocumentElement();
		YFCIterable<YFCElement> orderList = orderListEle.getChildren(TelstraConstants.ORDER);
		for (YFCElement order : orderList) {
			int orderLineMatchCount = 0;
			int orderListLineLength = order.getChildElement(TelstraConstants.ORDER_LINES)
					.getIntAttribute(TelstraConstants.TOTAL_NUMBER_OF_RECORDS);
			String listOrderHeaderKey = order.getAttribute(TelstraConstants.ORDER_HEADER_KEY);
			String originalOrderNo = order.getAttribute(TelstraConstants.ORDER_NO);
			if (validateOrderHeaderKey(listOrderHeaderKey, inputOrderHeaderKey)
					&& validateNoOfLinesCount(orderListLineLength, inputOrderlineLength)) {
				YFCElement orderLineListEle = order.getChildElement(TelstraConstants.ORDER_LINES);
				YFCIterable<YFCElement> orderLineList = orderLineListEle.getChildren(TelstraConstants.ORDER_LINE);
				for (YFCElement orderLine : orderLineList) {
					orderLineMatchCount = getLineMatchCount(orderLine, orderLineMatchCount);
				}
				if (validateLineMatchCount(orderLineMatchCount, inputOrderlineLength)) {
					/*
					 * HUB-9305
					 */
					boolean bIsAddressSame =  bValidateShipToAddress(order, lIpItemIdShipToKey);
					if(bIsAddressSame){
					inXml.getDocumentElement().setAttribute(TelstraConstants.GPS_ORIGINAL_ORDER_NO, originalOrderNo);
					inXml.getDocumentElement().setAttribute(TelstraConstants.GPS_ORIGINAL_SOURCE_ID, order.getAttribute(TelstraConstants.ORDER_NAME));

					return true;
					}
				}
			}
		}

		return false;
	}

	/**
	 * 
	 * @param yfcEleOrder
	 * @param lIpItemIdShipToKey
	 * @return
	 */
	private boolean bValidateShipToAddress(YFCElement yfcEleOrder, List<String> lIpItemIdShipToKey) {
		boolean bIsAddressSame = true;
		YFCNodeList<YFCElement> yfcNlOrderLine = yfcEleOrder.getElementsByTagName(TelstraConstants.ORDER_LINE);
		for (YFCElement yfcEleOrderLine : yfcNlOrderLine) {
			YFCElement yfcEleItem = yfcEleOrderLine.getChildElement(TelstraConstants.ITEM);			
			String sItemId = yfcEleItem.getAttribute(TelstraConstants.ITEM_ID);
			
			YFCElement yfcEleShipTo = yfcEleOrderLine.getChildElement(TelstraConstants.PERSON_INFO_SHIP_TO);
			String sPersonInfoKey = yfcEleShipTo.getAttribute(TelstraConstants.PERSON_INFO_KEY);
			if(lIpItemIdShipToKey.contains(sItemId+"_"+sPersonInfoKey)){
				lIpItemIdShipToKey.remove(sItemId+"_"+sPersonInfoKey);
			}
			else{
				bIsAddressSame = false;
				break;
			}
		}
		
		return bIsAddressSame;
	}

	/**
	 * 
	 * @param inXml
	 * @return
	 */
	private List<String> prepareItemIdShipToKeyList(YFCDocument inXml) {

		List<String> lItemIdShipToKey = new ArrayList<>();
		YFCNodeList<YFCElement> yfcNlOrderLine = inXml.getElementsByTagName(TelstraConstants.ORDER_LINE);
		for (YFCElement yfcEleOrderLine : yfcNlOrderLine) {

			YFCElement yfcEleItem = yfcEleOrderLine.getChildElement(TelstraConstants.ITEM);
			String sItemId = yfcEleItem.getAttribute(TelstraConstants.ITEM_ID);
			YFCElement yfcEleShipTo = yfcEleOrderLine.getChildElement(TelstraConstants.PERSON_INFO_SHIP_TO);
			if (!YFCCommon.isVoid(yfcEleShipTo)) {

				String sShipToKey = yfcEleShipTo.getAttribute(TelstraConstants.PERSON_INFO_KEY);
				lItemIdShipToKey.add(sItemId + "_" + sShipToKey);

			}
		}
		return lItemIdShipToKey;
	}

	/**
	 * Method to get Item and quantity details for validation from the new order
	 * document
	 * 
	 * @param inXml
	 */
	private void getOrderLineDtls(YFCDocument inXml) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "getOrderLineDtls", inXml);
		Map<String, List<String>> setOrderDtlMap = new HashMap<>();
		YFCNodeList<YFCElement> orderLineListEle = inXml.getDocumentElement()
				.getElementsByTagName(TelstraConstants.ORDER_LINE);
		for (YFCElement orderLine : orderLineListEle) {
			String itemId = orderLine.getChildElement(TelstraConstants.ITEM, true)
					.getAttribute(TelstraConstants.ITEM_ID, "");
			String qty = orderLine.getAttribute(TelstraConstants.ORDERED_QTY, "0");
			if (!"0".equals(qty)) {
				if (setOrderDtlMap.size() == 0 || XmlUtils.isVoid(setOrderDtlMap.get(itemId))) {
					List<String> qtyList = new ArrayList<>();
					qtyList.add(qty);
					setOrderDtlMap.put(itemId, qtyList);
				} else {
					List<String> qtyList = setOrderDtlMap.get(itemId);
					qtyList.add(qty);
					setOrderDtlMap.put(itemId, qtyList);
				}
			}
		}
		setInputOrderDtlMap(setOrderDtlMap);
	}

	/**
	 * Method to validate Order Item and quantity matches with existing order
	 * line from the Order List
	 * 
	 * @param orderLine
	 * @return
	 */
	private boolean validateOrderLine(YFCElement orderLine) {

		Map<String, List<String>> tempOrderDtlMap;
		tempOrderDtlMap = getInputOrderDtlMap();
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "validateOrderLine", orderLine);
		String itemId = orderLine.getChildElement(TelstraConstants.ITEM, true).getAttribute(TelstraConstants.ITEM_ID,
				"");
		String qty = orderLine.getAttribute(TelstraConstants.ORDERED_QTY, "0");
		if (!"0".equals(qty)) {
			List<String> qtyList = getInputOrderDtlMap().get(itemId);
			if (XmlUtils.isVoid(qtyList)) {
				return false;
			}
			for (int i = 0; i < qtyList.size(); i++) {
				LoggerUtil.startComponentLog(logger, this.getClass().getName(), "validateOrderLine",
						qty + "and" + qtyList.get(i));
				if (qty.equals(qtyList.get(i))) {
					qtyList.add(i, "0");
					return true;
				}
			}
		}
		setInputOrderDtlMap(tempOrderDtlMap);
		return false;
	}

	/**
	 * Method to validate final count of lines with actual item and quantity
	 * that matches with new order and existing order
	 * 
	 * @param orderLineMatchCount
	 * @param inputOrderlineLength
	 * @return
	 */
	private boolean validateLineMatchCount(int orderLineMatchCount, int inputOrderlineLength) {
		if (orderLineMatchCount == inputOrderlineLength) {
			LoggerUtil.startComponentLog(logger, this.getClass().getName(), "validateLineMatchCount",
					orderLineMatchCount + "and" + inputOrderlineLength);
			return true;
		}
		return false;
	}

	/**
	 * Method to check order_header_key of new order with existing order
	 * 
	 * @param listOrderHeaderKey
	 * @param inputOrderHeaderKey
	 * @return
	 */
	private boolean validateOrderHeaderKey(String listOrderHeaderKey, String inputOrderHeaderKey) {
		if (!listOrderHeaderKey.equals(inputOrderHeaderKey)) {
			return true;
		}
		return false;
	}

	/**
	 * Method to validate Line matches and If line matches increment
	 * orderLineMatchCount by 1
	 * 
	 * @param orderLine
	 * @param orderLineMatchCount
	 * @return
	 */
	private int getLineMatchCount(YFCElement orderLine, int orderLineMatchCount) {
		if (validateOrderLine(orderLine)) {
			return orderLineMatchCount + 1;
		}
		return orderLineMatchCount;
	}

	/**
	 * Method to validate no.Of order lines matches with existing order. If
	 * count doesn't match validation for that existing order will be skipped.
	 * 
	 * @param orderListLineLength
	 * @param inputOrderlineLength
	 * @return
	 */
	private boolean validateNoOfLinesCount(int orderListLineLength, int inputOrderlineLength) {
		if (orderListLineLength == inputOrderlineLength) {
			return true;
		}
		return false;
	}

	/**
	 * Default Method
	 */
	@Override
	public void setProperties(Map mProperties) {
		propertyMap.putAll(mProperties);
	}

}
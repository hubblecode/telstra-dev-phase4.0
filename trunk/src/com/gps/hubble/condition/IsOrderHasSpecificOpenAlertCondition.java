/***********************************************************************************************
 * File	Name		: GpsManageVectorOrder.java
 *
 * Description		: This class is called from ManageOrder.java
 * 					The purpose of this class is to create and manage Vector order
 * 
 * Modification	Log	:
 * -------------------------------------------------------------------------------------------------------------------------------------
 * Ver #	Date			Author					Modification
 * 
 * 
 * -------------------------------------------------------------------------------------------------------------------------------------
 * 1.0					  	Prateek Kumar 		   	Initial	Version 
 * 1.1		May 05,2017		Prateek Kumar			HUB-8771: Fetching Exception type and order header key from Alert if it is null
 * -------------------------------------------------------------------------------------------------------------------------------------
 * 
 */
package com.gps.hubble.condition;

import java.rmi.RemoteException;
import java.util.Map;

import org.w3c.dom.Document;

import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.ycp.japi.YCPDynamicConditionEx;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

/**
 * 
 * @author Prateek
 *
 */
public class IsOrderHasSpecificOpenAlertCondition implements YCPDynamicConditionEx {

	private static YFCLogCategory logger = YFCLogCategory.instance(IsOrderHasSpecificOpenAlertCondition.class);

	@Override
	public boolean evaluateCondition(YFSEnvironment env, String paramString, Map paramMap, Document inDoc) {

		YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "conslidateAlertCount", yfcInDoc);

		String sExceptionType = XPathUtil.getXpathAttribute(yfcInDoc, "//MonitorConsolidation/@ExceptionType");
		String sOrderHeaderKey = XPathUtil.getXpathAttribute(yfcInDoc, "//Order/@OrderHeaderKey");

		//HUB-8771 - Begin
		/* Alert consolidation for PO_CANCEL_ALERT*/ 
		if(YFCObject.isNull(sOrderHeaderKey) && YFCObject.isNull(sExceptionType)){
			sExceptionType = XPathUtil.getXpathAttribute(yfcInDoc, "//Alert/@ExceptionType");
			sOrderHeaderKey = XPathUtil.getXpathAttribute(yfcInDoc, "//Alert/@OrderHeaderKey");
		}
		//HUB-8771 - End

		YFCDocument docGetExceptionListIp = YFCDocument.getDocumentFor("<Inbox OrderHeaderKey='" + sOrderHeaderKey
				+ "' ExceptionType='" + sExceptionType + "' Status='OPEN' />");
		LoggerUtil.verboseLog(this.getClass().getName() + " :: docGetExceptionListIp", logger, docGetExceptionListIp);

		YFCDocument docGetExceptionListTemp = YFCDocument.getDocumentFor(
				"<InboxList> <Inbox OrderHeaderKey='' ExceptionType='' ConsolidationCount='' InboxKey=''  Status='' OrderNo=''/> </InboxList>");

		env.setApiTemplate(TelstraConstants.API_GET_EXCEPTION_LIST, docGetExceptionListTemp.getDocument());

		YFCDocument docGetExceptionListOp = null;
		try {
			docGetExceptionListOp = YFCDocument.getDocumentFor(YIFClientFactory.getInstance().getApi().invoke(env,
					TelstraConstants.API_GET_EXCEPTION_LIST, docGetExceptionListIp.getDocument()));
			LoggerUtil.verboseLog(this.getClass().getName() + " :: docGetExceptionListOp", logger,
					docGetExceptionListOp);

		} catch (YFSException | RemoteException | YIFClientCreationException e) {
			e.printStackTrace();
			LoggerUtil.errorLog(this.getClass().getName() + " :: Exception while calling getExceptionList ", logger,
					e.getMessage());
		}
		env.clearApiTemplate(TelstraConstants.API_GET_EXCEPTION_LIST);
		boolean bReturn = true;
		if (YFCCommon.isVoid(docGetExceptionListOp)
				|| docGetExceptionListOp.getDocumentElement().getChildNodes().getLength() == 0) {
			bReturn = false;
		}
		/*
		 * This template is getting used in GpsConsolidateAlertCount.java code to increment the alert count
		 */
		env.setTxnObject("GpsGetSpecificExceptionOp", docGetExceptionListOp);

		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "return value", bReturn);
		return bReturn;
	}

	@Override
	public void setProperties(Map mapProperties) {
	}

}

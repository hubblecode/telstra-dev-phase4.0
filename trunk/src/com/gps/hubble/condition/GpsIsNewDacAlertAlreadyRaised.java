/***********************************************************************************************
 * File Name        : GpsIsNewDacAlertAlreadyRaised.java
 *
 * Description      : Dynamic condition to check whether the new DAC alert has already been raised for the incoming customer ID or not
 * 
 * Modification Log :
 * ---------------------------------------------------------------------------------------------
 * Ver #    Date            Author						Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0      Feb 22,2017     Prateek Kumar				Initial Version (HUB-8451 - New DAC alert was raised for an Old DAC)
 * 
 * ---------------------------------------------------------------------------------------------
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
 **********************************************************************************************/
package com.gps.hubble.condition;

import java.rmi.RemoteException;
import java.util.Map;

import org.w3c.dom.Document;

import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.ycp.japi.YCPDynamicConditionEx;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

/**
 * 
 * @author Prateek
 *
 */
public class GpsIsNewDacAlertAlreadyRaised implements YCPDynamicConditionEx {

	private static YFCLogCategory logger = YFCLogCategory.instance(GpsIsNewDacAlertAlreadyRaised.class);

	@Override
	public boolean evaluateCondition(YFSEnvironment env, String arg1, Map arg2, Document inDoc) {

		YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
		try {
			return isNewDacAlertAlreadyRaised(env, yfcInDoc);
		}

		catch (YFSException | RemoteException | YIFClientCreationException e) {
			e.printStackTrace();
			LoggerUtil.errorLog(this.getClass().getName() + " :: Exception while calling getExceptionList ", logger,
					e.getMessage());
		}
		return false;
	}

	/**
	 * This method checks whether the new DAC alert has already been raised for the incoming DAC or not and based on that
	 * it returns the boolean
	 * @param yfcInDoc
	 * @return
	 * @throws YIFClientCreationException
	 * @throws RemoteException
	 * @throws YFSException
	 */
	private boolean isNewDacAlertAlreadyRaised(YFSEnvironment env, YFCDocument yfcInDoc)
			throws YFSException, RemoteException, YIFClientCreationException {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "isNewDacAlertAlreadyRaised", yfcInDoc);
		/*
		 * Preparing the get exception list api input.
		 */
		String sCustomerID = XPathUtil.getXpathAttribute(yfcInDoc, "//Customer/@CustomerID");
		YFCDocument yfcDocGetExceptionListIp = YFCDocument.getDocumentFor("<Inbox/>");
		YFCElement yfcEleGetExceptionListRoot = yfcDocGetExceptionListIp.getDocumentElement();
		yfcEleGetExceptionListRoot.setAttribute(TelstraConstants.EXCEPTION_TYPE, TelstraConstants.GPS_NEW_DAC_ALERT);
		yfcEleGetExceptionListRoot.setAttribute(TelstraConstants.BILL_TO_ID, sCustomerID);
		/*
		 * Setting specific template as only total number of records is required
		 */
		env.setApiTemplate(TelstraConstants.API_GET_EXCEPTION_LIST,
				YFCDocument.getDocumentFor("<InboxList TotalNumberOfRecords=''/>").getDocument());

		LoggerUtil.verboseLog(this.getClass().getName() + " :: yfcDocGetExceptionListIp", logger,
				yfcDocGetExceptionListIp);
		/*
		 * Calling get exception list api
		 */
		YFCDocument docGetExceptionListOp = YFCDocument.getDocumentFor(YIFClientFactory.getInstance().getApi()
				.invoke(env, TelstraConstants.API_GET_EXCEPTION_LIST, yfcDocGetExceptionListIp.getDocument()));
		LoggerUtil.verboseLog(this.getClass().getName() + " :: docGetExceptionListOp", logger, docGetExceptionListOp);
		/*
		 * clearing the template from the environment
		 */
		env.clearApiTemplate(TelstraConstants.API_GET_EXCEPTION_LIST);

		boolean bReturn = true;
		/*
		 * If there is no alert raised for this DAC earlier, total number of records would be 0
		 */
		if ("0".equals(
				docGetExceptionListOp.getDocumentElement().getAttribute(TelstraConstants.TOTAL_NUMBER_OF_RECORDS))) {
			bReturn = false;
		}

		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "isNewDacAlertAlreadyRaised", yfcInDoc);
		return bReturn;
	}

	@Override
	public void setProperties(Map arg0) {
		// TODO Auto-generated method stub

	}

}

package com.gps.hubble.condition;

import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Document;

import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.ycp.japi.YCPDynamicConditionEx;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;


public class IsVendorSendsASNCondition implements YCPDynamicConditionEx {

	private Map<String, String> propertyMap = new HashMap<>();
	private static YFCLogCategory logger = YFCLogCategory.instance(IsVendorSendsASNCondition.class);


	@Override
	public boolean evaluateCondition(YFSEnvironment env, String arg1, Map arg2, Document inXml) {

		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "evaluateCondition", inXml);
		Boolean isVendorSendsASN = false;
		try {
			YFCDocument inDoc = YFCDocument.getDocumentFor(inXml);
			//call getVendorList with template = <VendorList><Vendor  OrganizationCode='' SellerOrganizationCode=''  SendsAsn=''  VendorID='' VendorKey=''/></VendorList>
			YFCElement eleOrder = inDoc.getElementsByTagName(TelstraConstants.ORDER).item(0);
			if(!YFCObject.isVoid(eleOrder)) {
				String sOrganizationCode = 	eleOrder.getAttribute(TelstraConstants.ENTERPRISE_CODE,"");
				String sSellerOrganizationCode = eleOrder.getAttribute(TelstraConstants.SELLER_ORGANIZATION_CODE,"");
				
				if(!YFCObject.isNull(sOrganizationCode) && !YFCObject.isNull(sSellerOrganizationCode)) {
					YFCDocument docGetVendorListInput = YFCDocument.getDocumentFor("<Vendor OrganizationCode='"+sOrganizationCode+"' SellerOrganizationCode='"+sSellerOrganizationCode+"'/>");
					YFCDocument docGetVendorListTemplate = YFCDocument.getDocumentFor("<VendorList><Vendor  OrganizationCode='' SellerOrganizationCode=''  SendsAsn=''  VendorID='' VendorKey=''/></VendorList>");
					env.setApiTemplate("getVendorList", docGetVendorListTemplate.getDocument());
					YFCDocument docGetVendorListOutput = YFCDocument.getDocumentFor(YIFClientFactory.getInstance().getApi().invoke(env,"getVendorList", docGetVendorListInput.getDocument()));
					env.clearApiTemplate("getVendorList");
					
					YFCElement eleVendor = docGetVendorListOutput.getElementsByTagName("Vendor").item(0);
					if(!YFCObject.isVoid(eleVendor)) {
						isVendorSendsASN = eleVendor.getBooleanAttribute("SendsAsn",false);
					}
				}
			}			
		} catch (Exception exp) {
			exp.printStackTrace();
			
		}finally {
			LoggerUtil.endComponentLog(logger, this.getClass().getName(), "evaluateCondition", inXml);
		}
		
		return isVendorSendsASN;
	}
	/**
	 * Default Method
	 */
	@Override
	public void setProperties(Map mProperties) {
		propertyMap.putAll(mProperties);
	}

}
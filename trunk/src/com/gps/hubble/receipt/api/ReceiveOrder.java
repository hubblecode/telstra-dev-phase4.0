/***********************************************************************************************
 * File Name        : ReceiveOrder.java
 *
 * Description      : This class is called when we consume Receive Order Message from Queue.
 *                      1. From input xml get OrderName(OrderNo) and ReceivingNode and call 
 *                          getShipmentList to get Shipped Shipments
 *                      2. Then call receiveOrder against those Shipments.
 * 
 * Modification Log :
 * ---------------------------------------------------------------------------------------------
 * Ver #    Date            Author                  Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0      Jul 17,2016     Tushar Dhaka            Initial Version
 * 1.1		Dec 30,2016		Santosh Nagaraj			Receive if the PO is still not delivered 
 * 1.2		Jan 01,2017		Keerthi Yadav			HUB-8119: Exception must be thrown when Invalid receiving Node or Invalid Item Or 
 * 													Invalid Quantity is passed in the receipt message.
 * 1.3		Jan 20,2017		Keerthi Yadav			HUB-8081: If the ReceivingNode is ZX, dont try to receive the order.
 * 1.4		Jan 20,2017		Keerthi Yadav			HUB-8317: Receive Order fails with null pointer exception.
 * 1.5		Mar 14,2017		Keerthi Yadav			HUB-8079: Avoid Over Receipt against Shipment while Receiving for some scenarios.
 * 1.6		May 03,2017		Prateek Kumar			HUB-8919: If the msg comes for RO, create and deliver shipment before proceeding further
 * 1.7		May 31,2017		Prateek Kumar			HUB-9208: removing the line with 0 qty.
 * 1.8 		Jun 09,2017		Keerthi Yadav			HUB-9259: Order No is getting blanked out for some shipments
 * 1.9		Jun 21,2017		Prateek Kumar			HUB-9384: If the receving messages is comin for the line which is not delivered, invoke carrrier update service
 * 2.0		Jun 27,2017		Keerthi Yadav			HUB-9415: Adding Reason Text while calling Adjust Inventory for Audit
 * ---------------------------------------------------------------------------------------------
 **********************************************************************************************/

package com.gps.hubble.receipt.api;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.constants.TelstraErrorCodeConstants;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.date.YDate;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSException;

/**
 * Custom code to call receiveOrder
 * 
 * @author Tushar Dhaka
 * @version 1.0
 * 
 *          Extends AbstractCustomApi class
 */
public class ReceiveOrder extends AbstractCustomApi {

	private static YFCLogCategory logger = YFCLogCategory
			.instance(ReceiveOrder.class);
	private boolean errored = false;
	private static String errorMessage = "";
	private static String errorCode = "";
	private boolean isReceivingNodePresentInShipment = true;
	private String sourceSystem = "";
	private YFCDocument inXml;
	private String sVectorId="";
	public YFCDocument invoke(YFCDocument inXml) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(),
				"invoke", inXml);

		getOrderLineList(inXml);

		/*
		 * HUB-9208: removing the line with 0 qty.
		 */
		removelineWithZeroQty(inXml);
		this.inXml = inXml;
		//

		/*
		 * Input XML:- <ReceiptList> <Receipt ReceivingNode="" OrderName="" >
		 * <ReceiptLines> <ReceiptLine ItemID="" PrimeLineNo=""
		 * Quantity="ReceivedQuantity" SubLineNo="1" UnitOfMeasure="">
		 * </ReceiptLine> </ReceiptLines> </Receipt> <ReceiptList>
		 */

		YFCElement eleReceipt = inXml.getElementsByTagName(
				TelstraConstants.RECEIPT).item(0);
		if (!YFCObject.isVoid(eleReceipt)) {
			// Begin - IF Transfer order is not in delivered status throw an
			// error
			// This is because, it is expected that the shipment for the TO will
			// delivered,
			// before the receipt message for the same is received.

			// HUB-7713 [Start]
			String sOrderName = inXml.getElementsByTagName("Receipt").item(0)
					.getAttribute(TelstraConstants.ORDER_NAME);

			String sReceivingNode = inXml.getElementsByTagName("Receipt")
					.item(0).getAttribute(TelstraConstants.RECEIVING_NODE,"");

			//			boolean isTODelivered = checkTOStatus(inXml, sOrderName);
			// HUB-7713 [End]

			/*if (!isTODelivered) {
				throw ExceptionUtil
				.getYFSException(
						TelstraErrorCodeConstants.RECEIVE_ORDER_ORDER_STATUS_INVALID_ERROR_CODE,
						new YFSException());
			}*/

			// End - IF Transfer order is not in delivered status throw an error

			//HUB-8919 - Begin
			YFCDocument yfcDocGetOrderListOp = callGetOrderList(sOrderName);
			String sDocumentType = XPathUtil.getXpathAttribute(yfcDocGetOrderListOp, "//Order/@DocumentType");
			/*if(TelstraConstants.DOCUMENT_TYPE_0003.equals(sDocumentType)){
				String sOrderHeaderKey = XPathUtil.getXpathAttribute(yfcDocGetOrderListOp, "//Order/@OrderHeaderKey");
				createAndProcessROShipment(sOrderHeaderKey);
			}*/
			//HUB-8919 - End			
			/*
			 * If an exception is thrown when processing the Receipt message,
			 * the user can create a shipment and stamp the shipment no in the
			 * input message (both at the header or line level). When the
			 * exception is re-processed, instead of trying to identify which
			 * shipment to receive against, the system will directly try to
			 * receive the line against the passed shipment no. If the
			 * ShipmentNo is passed at the line level, the same will be used to
			 * receive the line. Else, the shipmentNo passed at the header will
			 * be used to receive the line.
			 */

			boolean isShipmentpresentatLine = isShipmentPresentatLineLevel(eleReceipt);
			if (isShipmentpresentatLine) {
				/*
				 * If the shipment is present in the input messaged, receive the
				 * line against the passed ShipmentNo.
				 */

				for (YFCElement eleReceiptLine : eleReceipt
						.getElementsByTagName(TelstraConstants.RECEIPT_LINE)) {
					String sShipmentNo = eleReceiptLine.getAttribute(
							TelstraConstants.SHIPMENT_NO, "");
					if (sShipmentNo.equals("")) {
						sShipmentNo = eleReceipt.getAttribute(
								TelstraConstants.SHIPMENT_NO, "");
					}
					String sItemID = eleReceiptLine.getAttribute(
							TelstraConstants.ITEM_ID, "");
					String sPrimeLineNo = eleReceiptLine.getAttribute(
							TelstraConstants.PRIME_LINE_NO, "");


					//HUB-8119[START]

					double toBeReceivedQuantity = eleReceiptLine
							.getDoubleAttribute(TelstraConstants.QUANTITY, 0.0);

					//					YFCDocument yfcDocGetOrderListOp = callGetOrderList(sOrderName);
					YFCElement eleOrderOut = yfcDocGetOrderListOp
							.getElementsByTagName(TelstraConstants.ORDER).item(
									0);
					// sPrimeLineNo =
					// eleReceiptLine.getAttribute(TelstraConstants.PRIME_LINE_NO,
					// "");

					YFCElement eleTempOrderLine = null;
					for (YFCElement eleOrderListLine : eleOrderOut.getElementsByTagName(TelstraConstants.ORDER_LINE)) {
						String sOrderListPrimeLineNo = eleOrderListLine.getAttribute(TelstraConstants.PRIME_LINE_NO,"");
						if (sOrderListPrimeLineNo.equals(sPrimeLineNo)) {
							eleTempOrderLine = eleOrderListLine;
							break;
						}
					}

					//Validate the receiptLine for Item,Quantity and Receiving node

					boolean bValidationFailed = validateReceiptLine(eleTempOrderLine,sItemID,toBeReceivedQuantity,sReceivingNode,eleReceipt,eleReceiptLine,sOrderName);
					if(bValidationFailed){
						continue;
						/*
						 * if the receipt message (only for the failed line)
						 * is dropped back to the queue, go to the next line
						 * that is to be received.
						 */
					}
					sItemID = eleReceiptLine.getAttribute(TelstraConstants.ITEM_ID, "");
					//HUB-8119[END]


					YFCDocument docGetShipmentListOutput = getShipmentListForItem(
							inXml, sItemID, sPrimeLineNo, sShipmentNo, true);
					YFCNodeList<YFCElement> nlShipment = docGetShipmentListOutput
							.getElementsByTagName(TelstraConstants.SHIPMENT);
					if (!nlShipment.iterator().hasNext()) {
						String processingFailed = eleReceipt
								.getAttribute("ProcessingFailed");
						YFCDocument exceptionDoc = getExceptionDocument(
								eleReceiptLine, sOrderName, sReceivingNode);
						if (!YFCObject.isVoid(processingFailed)
								&& "Y".equals(processingFailed)) {
							LoggerUtil
							.verboseLog(
									"Shipment does not exist or is not in a valid status to receive",
									logger, " throwing exception");
							throw ExceptionUtil.getYFSException(
									"TEL_ERR_1181_007", new YFSException());
						} else {
							invokeYantraService("DropReceiptOrderQ",
									exceptionDoc);
						}
						continue;
					}
					YFCElement eleShipment = nlShipment.item(0);
					moveToDeliveredAndReceiveShipment(eleReceiptLine,
							eleShipment, sOrderName, sPrimeLineNo);
				}
			} else {
				/*
				 * If the ShipmentNo is not passed in the input message, the
				 * system will have to identify the Shipment to receive against.
				 * For POs, the shipment may not even be present. In such a
				 * case, a shipment will be created, shipped, delivered, and
				 * received against.
				 */
				for (YFCElement eleReceiptLine : eleReceipt
						.getElementsByTagName(TelstraConstants.RECEIPT_LINE)) {
					String sItemID = eleReceiptLine.getAttribute(
							TelstraConstants.ITEM_ID, "");
					double toBeReceivedQuantity = eleReceiptLine
							.getDoubleAttribute(TelstraConstants.QUANTITY, 0.0);
					String sPrimeLineNo = eleReceiptLine.getAttribute(
							TelstraConstants.PRIME_LINE_NO, "");

					//					YFCDocument yfcDocGetOrderListOp = callGetOrderList(sOrderName);
					YFCElement eleOrderOut = yfcDocGetOrderListOp
							.getElementsByTagName(TelstraConstants.ORDER).item(
									0);
					sourceSystem = eleOrderOut.getAttribute("EntryType");
					// sPrimeLineNo =
					// eleReceiptLine.getAttribute(TelstraConstants.PRIME_LINE_NO,
					// "");
					YFCElement eleTempOrderLine = null;
					for (YFCElement eleOrderListLine : eleOrderOut
							.getElementsByTagName(TelstraConstants.ORDER_LINE)) {
						String sOrderListPrimeLineNo = eleOrderListLine
								.getAttribute(TelstraConstants.PRIME_LINE_NO,
										"");
						if (sOrderListPrimeLineNo.equals(sPrimeLineNo)) {
							eleTempOrderLine = eleOrderListLine;
							break;
						}
					}

					if (!YFCObject.isVoid(eleTempOrderLine)) {
						double orderedQuantity = eleTempOrderLine
								.getDoubleAttribute("OrderedQty");

						//Validate the receiptLine for Item,Quantity and Receiving node

						//HUB-8119[START]

						boolean bValidationFailed = validateReceiptLine(eleTempOrderLine,sItemID,toBeReceivedQuantity,sReceivingNode,eleReceipt,eleReceiptLine,sOrderName);
						if(bValidationFailed){
							continue;
							/*
							 * if the receipt message (only for the failed line)
							 * is dropped back to the queue, go to the next line
							 * that is to be received.
							 */
						}

						sItemID = eleReceiptLine.getAttribute(TelstraConstants.ITEM_ID, "");

						YFCDocument docGetShipmentListOutput = getShipmentListForItem(
								inXml, sItemID, sPrimeLineNo, "", false);

						YFCNodeList<YFCElement> nlShipment = docGetShipmentListOutput
								.getElementsByTagName(TelstraConstants.SHIPMENT);

						//HUB-8119[END]



						// Get the total quantity that has already been received
						// in the order line
						double orderLineStatusReceivedQuantity = getOrderLineStatusReceivedQuantity(
								yfcDocGetOrderListOp, sPrimeLineNo);
						if (orderLineStatusReceivedQuantity >= orderedQuantity) {
							/*
							 * if the total received quantity in the order line
							 * is greater than or equal to the ordered quantity,
							 * then an exception is thrown. This is because, the
							 * system cannot determine which shipment to receive
							 * against. This is an unlikely scenario.
							 */
							String processingFailed = eleReceipt
									.getAttribute("ProcessingFailed");
							YFCDocument exceptionDoc = getExceptionDocument(
									eleReceiptLine, sOrderName, sReceivingNode);
							if (!YFCObject.isVoid(processingFailed)
									&& "Y".equals(processingFailed)) {
								LoggerUtil.verboseLog("Order already received",
										logger, " throwing exception");
								throw ExceptionUtil.getYFSException(
										"TEL_ERR_1181_004", new YFSException());
							} else {
								invokeYantraService("DropReceiptOrderQ",
										exceptionDoc);
							}
							/*
							 * if the receipt message (only for the failed line)
							 * is dropped back to the queue, go to the next line
							 * that is to be received.
							 */
							continue;
						}
						/*
						 * If the total received quantity in the order line is
						 * less than rhe order quantity, then this part of the
						 * code will be executed.
						 */
						if (nlShipment.getLength() == 0) {
							// if there are no shipments for the passed order
							// line
							eleTempOrderLine.setAttribute("StatusQuantity",
									eleReceiptLine.getAttribute("Quantity"));
							//							String sDocumentType = (yfcDocGetOrderListOp.getElementsByTagName(TelstraConstants.ORDER).item(0)).getAttribute(TelstraConstants.DOCUMENT_TYPE);
							if (!YFCObject.isVoid(yfcDocGetOrderListOp)
									&& "0005"
									.equalsIgnoreCase(yfcDocGetOrderListOp
											.getElementsByTagName(
													TelstraConstants.ORDER)
											.item(0)
											.getAttribute(
													TelstraConstants.DOCUMENT_TYPE))) {
								/*
								 * As the number of shipments for the order line
								 * is 0, and if its a PO, create and confirm a
								 * shipment. Move the shipment to delivered and
								 * receive against the shipment.
								 */
								moveToDeliveredAndReceiveShipment(
										eleReceiptLine, eleTempOrderLine,
										sOrderName, sPrimeLineNo);
							} else if (("0006".equalsIgnoreCase(sDocumentType)
									|| "0003".equalsIgnoreCase(sDocumentType))
									&& !"Y".equalsIgnoreCase(
											inXml.getDocumentElement().getAttribute("CarrierUpdateInvoked"))) {

								/*
								 * HUB-9205 moved line to delivered status and then reprocess the receipt message.
								 * Added CarrierUpdateInvoked flag, in case of carrier update fails and message is consumed, this will go in infinite loop
								 */
								String sShipmentNo = movelineToShippedStatus(inXml, eleReceiptLine);
								movelineToDeliveveredStatus(inXml, eleReceiptLine, sShipmentNo);

								YFCDocument yfcDocReceiveOrderIp = YFCDocument
										.getDocumentFor("<Receipt ReceivingNode='" + sReceivingNode
												+ "' OrderName='" + sOrderName
												+ "' CarrierUpdateInvoked='Y'/>");
								YFCElement receiptLinesEle = yfcDocReceiveOrderIp.getDocumentElement()
										.createChild("ReceiptLines");
								receiptLinesEle.importNode(eleReceiptLine);								
								invokeYantraService("DropReceiptOrderQ", yfcDocReceiveOrderIp);
							}
							else{
								String processingFailed = eleReceipt
										.getAttribute("ProcessingFailed");
								YFCDocument exceptionDoc = getExceptionDocument(
										eleReceiptLine, sOrderName,
										sReceivingNode);
								if (!YFCObject.isVoid(processingFailed)
										&& "Y".equals(processingFailed)) {
									LoggerUtil.verboseLog(
											"No Valid Shipment Found ", logger,
											" throwing exception");
									throw ExceptionUtil.getYFSException(
											"TEL_ERR_1181_001",
											new YFSException());
								} else {
									invokeYantraService("DropReceiptOrderQ",
											exceptionDoc);
								}
								continue;
							}
						} else if (nlShipment.getLength() == 1) {
							YFCElement eleShipment = nlShipment.item(0);
							// get the shipment line element for the line that
							// is being received
							YFCElement eleShipmentLine = getShipmentLineForPrimeLineNo(
									eleShipment, sPrimeLineNo);
							String shipmentStatus = eleShipment
									.getAttribute("Status");
							if (YFCObject.isVoid(eleShipmentLine)) {
								String processingFailed = eleReceipt
										.getAttribute("ProcessingFailed");
								YFCDocument exceptionDoc = getExceptionDocument(
										eleReceiptLine, sOrderName,
										sReceivingNode);
								if (!YFCObject.isVoid(processingFailed)
										&& "Y".equals(processingFailed)) {
									LoggerUtil
									.verboseLog(
											"No Valid Prime Line found to receive in the shipment",
											logger,
											" throwing exception");
									throw ExceptionUtil.getYFSException(
											"TEL_ERR_1181_005",
											new YFSException());
								} else {
									invokeYantraService("DropReceiptOrderQ",
											exceptionDoc);
								}
								continue;
							}
							String sShipmentLineItem = eleShipmentLine
									.getAttribute("ItemID", "");
							double dShipmentLineQuantity = eleShipmentLine
									.getDoubleAttribute("Quantity", 0.0);
							double dShipmentLineReceivedQuantity = eleShipmentLine
									.getDoubleAttribute("ReceivedQuantity", 0.0);
							double receivableQuantity = dShipmentLineQuantity
									- dShipmentLineReceivedQuantity;
							YFCDocument docReceiveOrderInput = null;
							if (sShipmentLineItem.equals(sItemID)
									&& receivableQuantity > 0.0) {
								if ("1200.10000".equals(shipmentStatus)
										|| "1400".equals(shipmentStatus)
										|| "1400.10000".equals(shipmentStatus)) {
									if (TelstraConstants.DOCUMENT_TYPE_0006
											.equals(eleShipment
													.getAttribute("DocumentType"))
											&& "1200.10000"
											.equals(shipmentStatus)) {
										YFCDocument confirmShipmentDoc = YFCDocument
												.getDocumentFor("<Shipment ShipmentKey='"
														+ eleShipment
														.getAttribute(
																"ShipmentKey",
																"")
														+ "' />");
										invokeYantraApi("confirmShipment",
												confirmShipmentDoc);
									}
									YFCDocument carrierUpdateInp = inputToCarrierUpdate(
											sOrderName,
											eleShipment
											.getAttribute("ShipmentNo"));
									invokeYantraService("GpsCarrierUpdate",
											carrierUpdateInp);
								}

								docReceiveOrderInput = getReceiveOrderInput(
										eleReceiptLine, eleShipment,
										sPrimeLineNo);
								/*
								 * If receiving node is not present in the shipment, then skip the receive order call. For now this scenario will happen for ZX 
								 * node, where B2Bi don't pass the ZX receiving node in the manage order xml.
								 */
								if(isReceivingNodePresentInShipment){
									callReceiveOrderAndAdjustInventoryForVector(docReceiveOrderInput,sOrderName,sPrimeLineNo);
								}
							} else {
								double orderLineStatusQuantity = getOrderLineStatusQuantity(
										yfcDocGetOrderListOp, sPrimeLineNo);
								// ???The orderLineStatusQuantity is not the sum
								// of all the status quantities???
								// ???It is the status quantity of the highest
								// status after 3350 (Included In Shipment)???
								if (errored
										&& orderLineStatusQuantity == 0.0) {
									String processingFailed = eleReceipt
											.getAttribute("ProcessingFailed");
									YFCDocument exceptionDoc = getExceptionDocument(
											eleReceiptLine, sOrderName,
											sReceivingNode);
									if (!YFCObject.isVoid(processingFailed)
											&& "Y".equals(processingFailed)) {
										LoggerUtil.verboseLog(errorMessage,
												logger, " throwing exception");
										throw ExceptionUtil.getYFSException(
												errorCode, new YFSException());
									} else {
										invokeYantraService(
												"DropReceiptOrderQ",
												exceptionDoc);
									}
									continue;
								}
								if (orderLineStatusQuantity >= toBeReceivedQuantity) {
									eleTempOrderLine.setAttribute(
											"StatusQuantity",
											toBeReceivedQuantity + "");
								} else {
									eleTempOrderLine.setAttribute(
											"StatusQuantity",
											orderLineStatusQuantity + "");
								}
								/*moveToDeliveredAndReceiveShipment(
										eleReceiptLine, eleTempOrderLine,
										sOrderName, sPrimeLineNo);*/
								if (!YFCObject.isVoid(yfcDocGetOrderListOp)
										&& "0005"
										.equalsIgnoreCase(yfcDocGetOrderListOp
												.getElementsByTagName(
														TelstraConstants.ORDER)
												.item(0)
												.getAttribute(
														TelstraConstants.DOCUMENT_TYPE))) {
									/*
									 * As the number of shipments for the order line
									 * is 0, and if its a PO, create and confirm a
									 * shipment. Move the shipment to delivered and
									 * receive against the shipment.
									 */
									moveToDeliveredAndReceiveShipment(
											eleReceiptLine, eleTempOrderLine,
											sOrderName, sPrimeLineNo);
								} else if (("0006".equalsIgnoreCase(sDocumentType)
										|| "0003".equalsIgnoreCase(sDocumentType))
										&& !"Y".equalsIgnoreCase(
												inXml.getDocumentElement().getAttribute("CarrierUpdateInvoked"))) {

									/*
									 * HUB-9205 moved line to delivered status and then reprocess the receipt message.
									 * Added CarrierUpdateInvoked flag, in case of carrier update fails and message is consumed, this will go in infinite loop
									 */
									String sShipmentNo = movelineToShippedStatus(inXml, eleReceiptLine);

									movelineToDeliveveredStatus(inXml, eleReceiptLine, sShipmentNo);

									YFCDocument yfcDocReceiveOrderIp = YFCDocument
											.getDocumentFor("<Receipt ReceivingNode='" + sReceivingNode
													+ "' OrderName='" + sOrderName
													+ "' CarrierUpdateInvoked='Y'/>");
									YFCElement receiptLinesEle = yfcDocReceiveOrderIp.getDocumentElement()
											.createChild("ReceiptLines");
									receiptLinesEle.importNode(eleReceiptLine);								
									invokeYantraService("DropReceiptOrderQ", yfcDocReceiveOrderIp);
								}
							}
						} else if (nlShipment.getLength() > 1) {
							//HUB-8079[START]

							List<YFCElement> eleShipments = chooseBestShipment(
									nlShipment, sPrimeLineNo, toBeReceivedQuantity);

							//HUB-8079[END]

							if (YFCObject.isVoid(eleShipments)) {
								double orderLineStatusQuantity = getOrderLineStatusQuantity(
										yfcDocGetOrderListOp, sPrimeLineNo);
								if (errored
										&& orderLineStatusQuantity == 0.0) {
									String processingFailed = eleReceipt
											.getAttribute("ProcessingFailed");
									YFCDocument exceptionDoc = getExceptionDocument(
											eleReceiptLine, sOrderName,
											sReceivingNode);
									if (!YFCObject.isVoid(processingFailed)
											&& "Y".equals(processingFailed)) {
										LoggerUtil.verboseLog(errorMessage,
												logger, " throwing exception");
										throw ExceptionUtil.getYFSException(
												errorCode, new YFSException());
									} else {
										invokeYantraService(
												"DropReceiptOrderQ",
												exceptionDoc);
									}
									continue;
								}
								if (orderLineStatusQuantity >= toBeReceivedQuantity) {
									eleTempOrderLine.setAttribute(
											"StatusQuantity",
											toBeReceivedQuantity);
								} else {
									eleTempOrderLine.setAttribute(
											"StatusQuantity",
											orderLineStatusQuantity);
								}
								/*moveToDeliveredAndReceiveShipment(
										eleReceiptLine, eleTempOrderLine,
										sOrderName, sPrimeLineNo);*/


								if (!YFCObject.isVoid(yfcDocGetOrderListOp)
										&& "0005"
										.equalsIgnoreCase(yfcDocGetOrderListOp
												.getElementsByTagName(
														TelstraConstants.ORDER)
												.item(0)
												.getAttribute(
														TelstraConstants.DOCUMENT_TYPE))) {
									/*
									 * As the number of shipments for the order line
									 * is 0, and if its a PO, create and confirm a
									 * shipment. Move the shipment to delivered and
									 * receive against the shipment.
									 */
									moveToDeliveredAndReceiveShipment(
											eleReceiptLine, eleTempOrderLine,
											sOrderName, sPrimeLineNo);
								} else if (("0006".equalsIgnoreCase(sDocumentType)
										|| "0003".equalsIgnoreCase(sDocumentType))
										&& !"Y".equalsIgnoreCase(
												inXml.getDocumentElement().getAttribute("CarrierUpdateInvoked"))) {

									/*
									 * HUB-9205 moved line to delivered status and then reprocess the receipt message.
									 * Added CarrierUpdateInvoked flag, in case of carrier update fails and message is consumed, this will go in infinite loop
									 */
									String sShipmentNo = movelineToShippedStatus(inXml, eleReceiptLine);
									movelineToDeliveveredStatus(inXml, eleReceiptLine, sShipmentNo);

									YFCDocument yfcDocReceiveOrderIp = YFCDocument
											.getDocumentFor("<Receipt ReceivingNode='" + sReceivingNode
													+ "' OrderName='" + sOrderName
													+ "' CarrierUpdateInvoked='Y'/>");
									YFCElement receiptLinesEle = yfcDocReceiveOrderIp.getDocumentElement()
											.createChild("ReceiptLines");
									receiptLinesEle.importNode(eleReceiptLine);								
									invokeYantraService("DropReceiptOrderQ", yfcDocReceiveOrderIp);
								}

							} else {
								//HUB-8079[START]

								for (YFCElement eleShipment : eleShipments) {
									//HUB-8079[END]

									String shipmentStatus = eleShipment
											.getAttribute("Status");
									if ("1200.10000".equals(shipmentStatus)
											|| "1400".equals(shipmentStatus)
											|| "1400.10000".equals(shipmentStatus)) {
										if (TelstraConstants.DOCUMENT_TYPE_0006
												.equals(eleShipment
														.getAttribute("DocumentType"))
												&& "1200.10000"
												.equals(shipmentStatus)) {
											YFCDocument confirmShipmentDoc = YFCDocument
													.getDocumentFor("<Shipment ShipmentKey='"
															+ eleShipment
															.getAttribute(
																	"ShipmentKey",
																	"")
															+ "' />");
											invokeYantraApi("confirmShipment",
													confirmShipmentDoc);
										}
										YFCDocument carrierUpdateInp = inputToCarrierUpdate(
												sOrderName,
												eleShipment
												.getAttribute("ShipmentNo"));
										invokeYantraService("GpsCarrierUpdate",
												carrierUpdateInp);
									}

									YFCDocument docReceiveOrderInput = getReceiveOrderInput(
											eleReceiptLine, eleShipment,
											sPrimeLineNo);
									/*
									 * If receiving node is not present in the shipment, then skip the receive order call. For now this scenario will happen for ZX 
									 * node, where B2Bi don't pass the ZX receiving node in the manage order xml.
									 */
									if(isReceivingNodePresentInShipment){
										callReceiveOrderAndAdjustInventoryForVector(docReceiveOrderInput,sOrderName,sPrimeLineNo);
									}
								}
							}
						}
					} else {
						String processingFailed = eleReceipt
								.getAttribute("ProcessingFailed");
						YFCDocument exceptionDoc = getExceptionDocument(
								eleReceiptLine, sOrderName, sReceivingNode);
						if (!YFCObject.isVoid(processingFailed)
								&& "Y".equals(processingFailed)) {
							LoggerUtil
							.verboseLog(
									"No matching order line found for the prime line number passed",
									logger, " throwing exception");
							throw ExceptionUtil.getYFSException(
									"TEL_ERR_1181_006", new YFSException());
						} else {
							invokeYantraService("DropReceiptOrderQ",
									exceptionDoc);
						}
						continue;
					}
				}
			}
		}
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke",
				inXml);
		return inXml;
	}

	/**
	 * 
	 * @param inXml
	 * @param eleReceiptLine
	 * @return
	 */
	private String movelineToShippedStatus(YFCDocument inXml, YFCElement eleReceiptLine) {

		String sOrderName = inXml.getElementsByTagName(TelstraConstants.RECEIPT).item(0).getAttribute(TelstraConstants.ORDER_NAME);
		String sPrimeLineNo = eleReceiptLine.getAttribute(TelstraConstants.PRIME_LINE_NO);
		String sItemId = eleReceiptLine.getAttribute(TelstraConstants.ITEM_ID);
		String sQuantity = eleReceiptLine.getAttribute(TelstraConstants.QUANTITY);
		String sSubLineNo = eleReceiptLine.getAttribute(TelstraConstants.SUB_LINE_NO);

		YFCDocument yfcDocShipOrderIp = YFCDocument.createDocument(TelstraConstants.SHIPMENT);
		YFCElement yfcEleRoot = yfcDocShipOrderIp.getDocumentElement();
		yfcEleRoot.setAttribute(TelstraConstants.ORDER_NAME, sOrderName);

		YFCElement yfcEleShipmentLines = yfcEleRoot.createChild(TelstraConstants.SHIPMENT_LINES);
		YFCElement yfcEleShipmentLine = yfcEleShipmentLines.createChild(TelstraConstants.SHIPMENT_LINE);
		yfcEleShipmentLine.setAttribute(TelstraConstants.ACTION, "BACKORDER");
		yfcEleShipmentLine.setAttribute(TelstraConstants.PRIME_LINE_NO, sPrimeLineNo);
		yfcEleShipmentLine.setAttribute(TelstraConstants.SUB_LINE_NO, sSubLineNo);
		yfcEleShipmentLine.setAttribute(TelstraConstants.ITEM_ID, sItemId);
		yfcEleShipmentLine.setAttribute(TelstraConstants.STATUS_QUANTITY, sQuantity);

		YFCDocument yfcDocShipOrderOp = invokeYantraService(TelstraConstants.GPS_SHIP_OR_REJECT_ORDER, yfcDocShipOrderIp);

		String sShipmnetNo = yfcDocShipOrderOp.getDocumentElement().getAttribute(TelstraConstants.SHIPMENT_NO);
		return sShipmnetNo;
	}

	/**
	 * 
	 * @param inXml
	 * @param eleReceiptLine
	 * @param yfcDocShipOrderOp 
	 */
	private void movelineToDeliveveredStatus(YFCDocument inXml, YFCElement eleReceiptLine, String sShipmentNo) {

		String sOrderName = inXml.getElementsByTagName(TelstraConstants.RECEIPT).item(0).getAttribute(TelstraConstants.ORDER_NAME);
		String sPrimeLineNo = eleReceiptLine.getAttribute(TelstraConstants.PRIME_LINE_NO);
		String sItemId = eleReceiptLine.getAttribute(TelstraConstants.ITEM_ID);
		String sQuantity = eleReceiptLine.getAttribute(TelstraConstants.QUANTITY);
		String sSubLineNo = eleReceiptLine.getAttribute(TelstraConstants.SUB_LINE_NO);

		YFCDocument yfcDocCarrierUpdateIp = YFCDocument.getDocumentFor("<Shipment/>");
		YFCElement yfcEleRoot = yfcDocCarrierUpdateIp.getDocumentElement();
		if(YFCCommon.isStringVoid(sVectorId)){
			yfcEleRoot.setAttribute(TelstraConstants.ORDER_NAME, sOrderName);
		}
		else{
			yfcEleRoot.setAttribute(TelstraConstants.ORDER_NAME, sVectorId);
		}

		if(!YFCCommon.isStringVoid(sShipmentNo)){
			yfcEleRoot.setAttribute(TelstraConstants.SHIPMENT_NO, sShipmentNo);
		}

		YFCElement yfcEleShipmentLines = yfcEleRoot.createChild(TelstraConstants.SHIPMENT_LINES);
		YFCElement yfcEleShipmentLine = yfcEleShipmentLines.createChild(TelstraConstants.SHIPMENT_LINE);
		yfcEleShipmentLine.setAttribute(TelstraConstants.PRIME_LINE_NO, sPrimeLineNo);
		yfcEleShipmentLine.setAttribute(TelstraConstants.ITEM_ID, sItemId);
		yfcEleShipmentLine.setAttribute(TelstraConstants.QUANTITY, sQuantity);
		yfcEleShipmentLine.setAttribute(TelstraConstants.SUB_LINE_NO, sSubLineNo);

		YFCElement yfcEleExtn = yfcEleRoot.createChild(TelstraConstants.EXTN);
		YFCElement yfcEleCarrierUpdateList = yfcEleExtn.createChild(TelstraConstants.CARRIER_UPDATE_LIST);
		YFCElement yfcEleCarrierUpdate = yfcEleCarrierUpdateList.createChild(TelstraConstants.CARRIER_UPDATE);
		yfcEleCarrierUpdate.setAttribute(TelstraConstants.TRANSPORT_STATUS, TelstraConstants.DELIVERED);
		yfcEleCarrierUpdate.setDateTimeAttribute(TelstraConstants.TRANSPORT_STATUS_DATE, new YDate(false));
		yfcEleCarrierUpdate.setAttribute(TelstraConstants.TRANSPORT_STATUS_TXT, "This was delivered through receipt message");

		LoggerUtil.verboseLog("ReceiveOrder :: movelineToDeliveveredStatus :: yfcDocCarrierUpdateIp", logger, yfcDocCarrierUpdateIp);
		invokeYantraService("GpsCarrierUpdate", yfcDocCarrierUpdateIp);


	}

	/**
	 * This method remove the line with 0 qty, provided line no 
	 * @param inXml
	 */

	private void removelineWithZeroQty(YFCDocument inXml) {

		List<String> lLineNoWithZeroQty = new ArrayList<>();

		for(YFCElement yfcEleReceiptLine : inXml.getElementsByTagName(TelstraConstants.RECEIPT_LINE)){

			double dQty = yfcEleReceiptLine.getDoubleAttribute(TelstraConstants.QUANTITY, 0);
			if(dQty==0){
				String sPrimeLineNo = yfcEleReceiptLine.getAttribute(TelstraConstants.PRIME_LINE_NO);
				if(!YFCCommon.isStringVoid(sPrimeLineNo)){
					lLineNoWithZeroQty.add(sPrimeLineNo);
				}
			}
		}

		if(!lLineNoWithZeroQty.isEmpty()){
			for (String sLineNo : lLineNoWithZeroQty){

				YFCElement yfcEleReceiptLine = XPathUtil.getXPathElement(inXml, "//ReceiptLine[@PrimeLineNo='"+sLineNo+"']");
				if(!YFCCommon.isVoid(yfcEleReceiptLine)){
					yfcEleReceiptLine.getParentElement().removeChild(yfcEleReceiptLine);
				}
			}
		}		
	}
	/**
	 * 
	 * @param sOrderHeaderKey
	 */

	private void createAndProcessROShipment(String sOrderHeaderKey) {

		YFCDocument yfcDocCreateShipmentOp = createROShipment(sOrderHeaderKey);
		invokeYantraApi(TelstraConstants.API_CONFIRM_SHIPMENT, yfcDocCreateShipmentOp);
		invokeYantraApi(TelstraConstants.API_DELIVER_SHIPMENT, yfcDocCreateShipmentOp);




	}

	/**
	 * This method creates the shipment for the RO.
	 * @param sOrderHeaderKey
	 * @return
	 */

	private YFCDocument createROShipment(String sOrderHeaderKey) {
		YFCDocument docCreateShipmentIp = YFCDocument.createDocument(TelstraConstants.SHIPMENT);
		YFCElement eleShipment = docCreateShipmentIp.getDocumentElement();
		eleShipment.setAttribute(TelstraConstants.DOCUMENT_TYPE, TelstraConstants.DOCUMENT_TYPE_0003);
		String sReceivingNode = inXml.getElementsByTagName(TelstraConstants.RECEIPT).item(0)
				.getAttribute(TelstraConstants.RECEIVING_NODE);
		eleShipment.setAttribute(TelstraConstants.RECEIVING_NODE, sReceivingNode);

		//HUB-9259[START]
		eleShipment.setAttribute(TelstraConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
		//HUB-9259[END]

		YFCElement eleShipmentLines = docCreateShipmentIp.createElement(TelstraConstants.SHIPMENT_LINES);
		eleShipment.appendChild(eleShipmentLines);

		YFCNodeList<YFCElement> nlReceiptLine = inXml.getElementsByTagName(TelstraConstants.RECEIPT_LINE);
		for (YFCElement eleReceiptLine : nlReceiptLine) {

			YFCElement eleShipmentLine = docCreateShipmentIp.createElement(TelstraConstants.SHIPMENT_LINE);
			eleShipmentLines.appendChild(eleShipmentLine);
			eleShipmentLine.setAttribute(TelstraConstants.RECEIVING_NODE,sReceivingNode);
			eleShipmentLine.setAttribute(TelstraConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
			eleShipmentLine.setAttribute(TelstraConstants.PRIME_LINE_NO,
					eleReceiptLine.getAttribute(TelstraConstants.PRIME_LINE_NO));
			eleShipmentLine.setAttribute(TelstraConstants.SUB_LINE_NO,"1");
			eleShipmentLine.setAttribute(TelstraConstants.QUANTITY,
					eleReceiptLine.getAttribute(TelstraConstants.QUANTITY));

		}
		LoggerUtil.verboseLog("Receive Order::createROShipment::docCreateShipmentIp ", logger, docCreateShipmentIp);
		YFCDocument docCreateShipmentOp = invokeYantraApi(TelstraConstants.API_CREATE_SHIPMENT, docCreateShipmentIp);
		LoggerUtil.verboseLog("Receive Order::createROShipment::docCreateShipmentOp ", logger, docCreateShipmentOp);
		return docCreateShipmentOp;
	}


	private void getOrderLineList(YFCDocument inXml) {
		YFCElement receiptEle = inXml.getElementsByTagName("Receipt").item(0);
		YFCDocument inDocgetOrdLineList = YFCDocument.getDocumentFor("<OrderLine><Extn VectorID='"+receiptEle.getAttribute("OrderName")+"' /></OrderLine>");
		YFCDocument templateForGetOrdLineList = YFCDocument.getDocumentFor("<OrderLineList TotalLineList=''>"
				+ "<OrderLine PrimeLineNo=''><Order OrderName='' /></OrderLine>"
				+ "</OrderLineList>");
		YFCDocument outputGetOrderLineList = invokeYantraApi("getOrderLineList", inDocgetOrdLineList, templateForGetOrdLineList);
		if(!YFCObject.isVoid(outputGetOrderLineList) 
				&& outputGetOrderLineList.getDocumentElement().getDoubleAttribute("TotalLineList") > 0) {
			if(outputGetOrderLineList.getDocumentElement().getDoubleAttribute("TotalLineList") > 1) {
				throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.SHIP_ORDER_MULTIPLE_ORDER_LINES_WITH_SAME_VECTOR_ID, new YFSException());
			}
			String primeLineNo = outputGetOrderLineList.getElementsByTagName("OrderLine").item(0).getAttribute("PrimeLineNo");
			String orderName = outputGetOrderLineList.getElementsByTagName("Order").item(0).getAttribute("OrderName");
			sVectorId = receiptEle.getAttribute("OrderName");
			receiptEle.setAttribute("OrderName", orderName);
			receiptEle.getElementsByTagName("ReceiptLine").item(0).setAttribute("PrimeLineNo", primeLineNo);
		}
	}

	//HUB-8119[START]

	private void callReceiveOrderAndAdjustInventoryForVector(YFCDocument docReceiveOrderInput, String sOrderName, String sPrimeLineNo) {
		YFCDocument receiveOrderOutput = invokeYantraApi(TelstraConstants.RECEIVE_ORDER_API,docReceiveOrderInput, getReceiveOrderTemplate());
		insertSerialNumInCustomTable(receiveOrderOutput);
		String shipmentNo = receiveOrderOutput.getElementsByTagName(TelstraConstants.SHIPMENT).item(0).getAttribute("ShipmentNo");

		if(YFCObject.equals(sourceSystem, TelstraConstants.VECTOR)) {
			if(!YFCObject.isVoid(receiveOrderOutput) && receiveOrderOutput.getDocumentElement().hasChildNodes()) {
				String receivingNode = receiveOrderOutput.getDocumentElement().getAttribute("ReceivingNode");
				YFCDocument adjustInventoryInDoc = YFCDocument.getDocumentFor("<Items />");
				YFCNodeList<YFCElement> receiptLines = receiveOrderOutput.getElementsByTagName("ReceiptLine");
				for(YFCElement receiptLine : receiptLines) {
					YFCElement itemEle = adjustInventoryInDoc.getDocumentElement().createChild("Item");
					itemEle.setAttribute("AdjustmentType", "ADJUSTMENT");
					itemEle.setAttribute("ItemID", receiptLine.getAttribute("ItemID"));
					itemEle.setAttribute("Quantity", "-"+receiptLine.getAttribute("Quantity"));
					itemEle.setAttribute("UnitOfMeasure", receiptLine.getAttribute("UnitOfMeasure",""));
					itemEle.setAttribute("ShipNode", receivingNode);
					itemEle.setAttribute("SupplyType", "ONHAND");

					//HUB-9415[START]
					/*Stamping the Prime Line No and Order Name for Inventory Audit*/

					itemEle.setAttribute("ReasonText", "Decreasing the supply for Receipt Confirmation : : Line No = "
							+sPrimeLineNo +" : : Source ID = "+ sOrderName);
					//HUB-9415[END]

					itemEle.setAttribute("Reference_1", "ShipmentNo:"+shipmentNo);
					itemEle.setAttribute("Reference_2", "ShipmentLineKey:"+receiptLine.getAttribute("ShipmentLineKey"));
					itemEle.setAttribute("Reference_3", "ReceiptLineKey:"+receiptLine.getAttribute("ReceiptLineKey"));
					adjustInventoryInDoc.getDocumentElement().appendChild(itemEle);
				}
				invokeYantraApi("adjustInventory", adjustInventoryInDoc);
			}
		}
	}


	private YFCDocument getReceiveOrderTemplate() {
		YFCDocument receiveOrderTemplate = YFCDocument.getDocumentFor("<Receipt ReceivingNode=''><Shipment ShipmentKey='' /><ReceiptLines><ReceiptLine ItemID='' ShipmentLineKey='' PrimeLineNo='' ReceiptLineKey='' Quantity='' UnitOfMeasure='' /></ReceiptLines></Receipt>");
		return receiveOrderTemplate;
	}


	/**
	 * Throw Exception if Invalid Item or Receiving Node or Qty
	 * @param eleTempOrderLine
	 * @param sItemID
	 * @param toBeReceivedQuantity
	 * @param sReceivingNode
	 * @param sOrderName 
	 * @param eleReceiptLine 
	 * @param eleReceipt 
	 * @param orderedQuantity 
	 * @return bValidationFailed
	 */
	private boolean validateReceiptLine(YFCElement eleTempOrderLine,String sItemID, double toBeReceivedQuantity, String sReceivingNode, YFCElement eleReceipt, YFCElement eleReceiptLine, String sOrderName) {

		boolean bValidationFailed= false;
		if(!YFCObject.isNull(eleTempOrderLine)){
			String sOrderLineReceivingNode = eleTempOrderLine.getAttribute(TelstraConstants.RECEIVING_NODE,"");
			String sOrderLineItemID = eleTempOrderLine.getElementsByTagName("Item").item(0).getAttribute(TelstraConstants.ITEM_ID,"");

			/* Method to remove the leading zeroes for a String */

			String strimmedOrderLineItemID = removeLeadingZeroes(sOrderLineItemID);
			if(YFCObject.isVoid(sItemID)) {
				sItemID = sOrderLineItemID;
			}
			String strimmedItemID = removeLeadingZeroes(sItemID);

			//HUB-8081-[START]	

			/*If the OrderLine Receiving Node is Blank do not validate the receivingNode Otherwise validate with the receiving node 
			 * passed in the message. If the receiving Node in the input message is blank do not throw an exception. If orderLine receiving Node is
			 * override receiving node to blank so that getShipmentList returns an output and delivers the shipment if not already delivered.
			 */

			if(!YFCObject.isVoid(sOrderLineReceivingNode)) {
				if(!YFCObject.equals(sOrderLineReceivingNode,sReceivingNode)){
					bValidationFailed = true;
				}
			}else{
				eleReceipt.setAttribute(TelstraConstants.RECEIVING_NODE, "");
			}

			if(YFCObject.equals(toBeReceivedQuantity , 0.0) || !YFCObject.equals(strimmedItemID,strimmedOrderLineItemID) 
					//	|| !YFCObject.equals(sOrderLineReceivingNode,sReceivingNode) 
					//  || YFCObject.isVoid(sReceivingNode) 	
					//HUB-8081-[END]	

					|| YFCObject.isVoid(sItemID)){
				bValidationFailed = true;
			}else{
				eleReceiptLine.setAttribute(TelstraConstants.ITEM_ID,sOrderLineItemID);
			}
		}else{
			bValidationFailed = true;
		}

		if(bValidationFailed){
			//throw exception

			String processingFailed = eleReceipt.getAttribute("ProcessingFailed");
			YFCDocument exceptionDoc = getExceptionDocument(eleReceiptLine, sOrderName, sReceivingNode);
			if (!YFCObject.isVoid(processingFailed) && "Y".equals(processingFailed)) {
				LoggerUtil.verboseLog("Invalid ItemID or Receiving Node or Quantity Or PrimeLineNo has been passed", logger, " throwing exception");
				throw ExceptionUtil.getYFSException("TEL_ERR_1181_009",new YFSException());
			} else {
				invokeYantraService("DropReceiptOrderQ",exceptionDoc);
			}
		}
		return bValidationFailed;
	}

	/**
	 * Method to remove the leading zeroes for a String
	 * @param sItemID
	 * @return sItemID
	 */
	public static String removeLeadingZeroes(String sItemID) {
		while (sItemID.indexOf("0")==0)
			sItemID = sItemID.substring(1);
		return sItemID;
	}

	//HUB-8119[END]

	private YFCDocument getExceptionDocument(YFCElement eleReceiptLine,
			String sOrderName, String sReceivingNode) {
		YFCDocument exceptionDocument = YFCDocument
				.getDocumentFor("<Receipt ReceivingNode='" + sReceivingNode
						+ "' OrderName='" + sOrderName
						+ "' ProcessingFailed='Y'/>");
		YFCElement receiptLinesEle = exceptionDocument.getDocumentElement()
				.createChild("ReceiptLines");
		receiptLinesEle.importNode(eleReceiptLine);
		return exceptionDocument;
	}

	/**
	 * This method checks if the ShipmentNo is passed in the receipt input
	 * message (at header or line level).
	 * 
	 * @param eleReceiptLines
	 * @return
	 */
	private boolean isShipmentPresentatLineLevel(YFCElement eleReceipt) {
		/*
		 * <Receipt OrderName="50963844-5" ReceivingNode="DC13"
		 * ShipmentNo="ShipmentNo-1"><ReceiptLines><ReceiptLine
		 * ItemID="91900015" PrimeLineNo="7" Quantity="2" SubLineNo="1"
		 * UnitOfMeasure="EA"/><ReceiptLine ItemID="91900018" PrimeLineNo="6"
		 * Quantity="4" SubLineNo="1" UnitOfMeasure="EA"/><ReceiptLine
		 * ItemID="91900019" PrimeLineNo="5" Quantity="2" SubLineNo="1"
		 * UnitOfMeasure="EA"/><ReceiptLine ItemID="91900029" PrimeLineNo="4"
		 * Quantity="5" SubLineNo="1" UnitOfMeasure="EA"/></ReceiptLines>
		 * </Receipt>
		 */
		if (!YFCObject.isVoid(eleReceipt
				.getAttribute(TelstraConstants.SHIPMENT_NO))) {
			return true;
		}
		YFCElement eleReceiptLines = eleReceipt.getElementsByTagName(
				TelstraConstants.RECEIPT_LINES).item(0);

		for (YFCElement eleReceiptLine : eleReceiptLines
				.getElementsByTagName(TelstraConstants.RECEIPT_LINE)) {
			if (!YFCObject.isVoid(eleReceiptLine
					.getAttribute(TelstraConstants.SHIPMENT_NO))) {
				return true;
			}
		}
		return false;
	}

	// HUB-7713 - PO should be able to be received in created status - Start

	private YFCElement getShipmentLineForPrimeLineNo(YFCElement eleShipment,
			String sPrimeLineNo) {
		YFCNodeList<YFCElement> shipmentLines = eleShipment
				.getElementsByTagName("ShipmentLine");
		for (YFCElement shipmentLine : shipmentLines) {
			if (sPrimeLineNo.equals(shipmentLine.getAttribute("PrimeLineNo"))) {
				return shipmentLine;
			}
		}
		return null;
	}

	/**
	 * This method will return the total received quantity for the passed
	 * PrimeLineNo. The received quantity is computed by summing status quantity
	 * of the order line status breakup in 3900 (Received) status.
	 * 
	 * @param yfcDocGetOrderListOp
	 * @param sPrimeLineNo
	 * @return
	 */
	private Double getOrderLineStatusReceivedQuantity(
			YFCDocument yfcDocGetOrderListOp, String sPrimeLineNo) {
		boolean orderPrimeLineNumberMatched = false;
		YFCNodeList<YFCElement> orderLines = yfcDocGetOrderListOp
				.getElementsByTagName("OrderLine");
		for (YFCElement orderLine : orderLines) {
			if (sPrimeLineNo.equals(orderLine.getAttribute("PrimeLineNo"))) {
				YFCNodeList<YFCElement> orderLineStatuses = orderLine
						.getElementsByTagName("OrderStatus");
				double statusReceivedQuantity = 0.0;
				for (YFCElement orderLineStatus : orderLineStatuses) {
					String status = orderLineStatus.getAttribute("Status");
					double statusQty = orderLineStatus
							.getDoubleAttribute("StatusQty");
					if ("3900".equals(status)) {
						statusReceivedQuantity = statusQty
								+ statusReceivedQuantity;
					}
				}
				orderPrimeLineNumberMatched = true;
				return statusReceivedQuantity;
			}
		}
		if (!orderPrimeLineNumberMatched) {
			LoggerUtil
			.verboseLog(
					"No matching order line found for the prime line number passed",
					logger, " throwing exception");
			throw ExceptionUtil.getYFSException("TEL_ERR_1181_006",
					new YFSException());
		}
		return 0.0;
	}

	private Double getOrderLineStatusQuantity(YFCDocument yfcDocGetOrderListOp, String sPrimeLineNo) {
		errored = false;
		errorMessage = "";
		errorCode = "";
		boolean orderPrimeLineNumberMatched = false;
		YFCNodeList<YFCElement> orderLines = yfcDocGetOrderListOp.getElementsByTagName("OrderLine");
		double totalQuantity = 0;
		for (YFCElement orderLine : orderLines) {
			if (sPrimeLineNo.equals(orderLine.getAttribute("PrimeLineNo"))) {
				YFCNodeList<YFCElement> orderLineStatuses = orderLine.getElementsByTagName("OrderStatus");
				// Map<String, Double> statusMap = new HashMap<String, Double>();
				for (YFCElement orderLineStatus : orderLineStatuses) {
					double status = orderLineStatus.getDoubleAttribute("Status");
					double statusQty = orderLineStatus.getDoubleAttribute("StatusQty");
					if (status >= 3350) {
						continue;
					} else {
						totalQuantity = totalQuantity + statusQty;
					}
				}
				if (totalQuantity > 0) {
					orderPrimeLineNumberMatched = true;
				}
			}
		}
		if (!orderPrimeLineNumberMatched) {
			errored = true;
			errorMessage = "No matching order line found for the prime line number passed";
			errorCode = "TEL_ERR_1181_006";
			// HUB-8317[START]
			return 0.0;
			// HUB-8317[END]
		}
		return totalQuantity;
	}

	/**
	 * Call deliverOrderLineQuantity method to move PO in created or PO Sent to
	 * Vendor Status to Delivered
	 * 
	 * @param inXml
	 * @param yfcDocGetOrderListOp
	 * @param sOrderName
	 */

	private void moveToDeliveredAndReceiveShipment(YFCElement eleReceiptLine,
			YFCElement eleTempShipmentOrOrderLine, String sOrderName,
			String sPrimeLineNo) {
		if ("Shipment".equals(eleTempShipmentOrOrderLine.getNodeName())) {
			String shipmentStatus = eleTempShipmentOrOrderLine
					.getAttribute("Status");
			if ("1200.10000".equals(shipmentStatus)
					|| "1400".equals(shipmentStatus)
					|| "1400.10000".equals(shipmentStatus)) {
				if (TelstraConstants.DOCUMENT_TYPE_0006
						.equals(eleTempShipmentOrOrderLine
								.getAttribute("DocumentType"))
						&& "1200.10000".equals(shipmentStatus)) {
					YFCDocument confirmShipmentDoc = YFCDocument
							.getDocumentFor("<Shipment ShipmentKey='"
									+ eleTempShipmentOrOrderLine.getAttribute(
											"ShipmentKey", "") + "' />");
					invokeYantraApi("confirmShipment", confirmShipmentDoc);
				}
				YFCDocument carrierUpdateInp = inputToCarrierUpdate(sOrderName,
						eleTempShipmentOrOrderLine.getAttribute("ShipmentNo"));
				invokeYantraService("GpsCarrierUpdate", carrierUpdateInp);
			}

			YFCDocument docReceiveOrderInput = getReceiveOrderInput(
					eleReceiptLine, eleTempShipmentOrOrderLine, sPrimeLineNo);
			/*
			 * If receiving node is not present in the shipment, then skip the receive order call. For now this scenario will happen for ZX 
			 * node, where B2Bi don't pass the ZX receiving node in the manage order xml.
			 */
			if(isReceivingNodePresentInShipment){
				callReceiveOrderAndAdjustInventoryForVector(docReceiveOrderInput,sOrderName,sPrimeLineNo);
			}
		} else if (!YFCObject.isNull(eleTempShipmentOrOrderLine)) {
			// Using the MinLineStatus to check the OrderLine status to
			// createShipment for the PO
			String sOrderLineStatus = eleTempShipmentOrOrderLine.getAttribute(
					"MinLineStatus", "");
			if ("1100.10000".equalsIgnoreCase(sOrderLineStatus)
					|| "1100".equalsIgnoreCase(sOrderLineStatus)
					|| "1100.05000".equals(sOrderLineStatus)
					|| "1260.10000".equals(sOrderLineStatus)) {
				YFCDocument docInputCreatePOASN = inputToCreateASN(
						eleTempShipmentOrOrderLine, sOrderName);
				// Creating shipment in shipped status
				YFCDocument docOutputCreatePOASN = invokeYantraService(
						"GpsProcessPOASNMsg", docInputCreatePOASN);
				YFCElement eleShipment = docOutputCreatePOASN
						.getDocumentElement();
				String sShipmentNo = docOutputCreatePOASN.getDocumentElement()
						.getAttribute(TelstraConstants.SHIPMENT_NO);
				YFCDocument docInputCarrierUpdate = inputToCarrierUpdate(
						sOrderName, sShipmentNo);
				// Moving shipment in Delivered status
				invokeYantraService("GpsCarrierUpdate", docInputCarrierUpdate);

				YFCDocument docReceiveOrderInput = getReceiveOrderInput(
						eleReceiptLine, eleShipment, sPrimeLineNo);
				/*
				 * If receiving node is not present in the shipment, then skip the receive order call. For now this scenario will happen for ZX 
				 * node, where B2Bi don't pass the ZX receiving node in the manage order xml.
				 */
				if(isReceivingNodePresentInShipment){
					callReceiveOrderAndAdjustInventoryForVector(docReceiveOrderInput,sOrderName,sPrimeLineNo);
				}
			}
		}
	}

	/*****
	 * The method creates the Input Xml to GpsProcessPoAsnMsg Service
	 * 
	 * @param eleOrderLine
	 * @param sOrderName
	 * @return docinputToCreateASN
	 */

	private YFCDocument inputToCreateASN(YFCElement eleOrderLine,
			String sOrderName) {
		YFCDocument docinputToCreateASN = YFCDocument
				.getDocumentFor("<Shipment OrderName='"
						+ sOrderName
						+ "'>"
						+ "<ShipmentLines><ShipmentLine /></ShipmentLines></Shipment>");
		YFCElement eleShipmentline = docinputToCreateASN.getElementsByTagName(
				TelstraConstants.SHIPMENT_LINE).item(0);
		YFCElement itemEle = eleOrderLine.getElementsByTagName(
				TelstraConstants.ITEM).item(0);
		if (!YFCObject.isVoid(itemEle)) {
			eleShipmentline.setAttribute(TelstraConstants.ITEM_ID,
					itemEle.getAttribute(TelstraConstants.ITEM_ID, ""));
			eleShipmentline.setAttribute(TelstraConstants.UNIT_OF_MEASURE,
					itemEle.getAttribute(TelstraConstants.UNIT_OF_MEASURE, ""));
			eleShipmentline.setAttribute(TelstraConstants.QUANTITY,
					eleOrderLine.getAttribute(TelstraConstants.STATUS_QUANTITY, ""));
			eleShipmentline.setAttribute(TelstraConstants.PRIME_LINE_NO,
					eleOrderLine.getAttribute(TelstraConstants.PRIME_LINE_NO,
							""));
			return docinputToCreateASN;
		} else
			return null;

	}

	/*****
	 * The method creates the Input Xml to GpsCarrierUpdate Service to move the
	 * order to delivered
	 * 
	 * @param sOrderName
	 * @param sShipmentNo
	 * @return docinputToCarrierUpdate
	 */

	private YFCDocument inputToCarrierUpdate(String sOrderName,
			String sShipmentNo) {
		YFCDocument docinputToCarrierUpdate = YFCDocument
				.getDocumentFor("<Shipment OrderName='"
						+ sOrderName
						+ "' ShipmentNo='"
						+ sShipmentNo
						+ "'><Extn><CarrierUpdateList><CarrierUpdate TransportStatus='Delivered' /></CarrierUpdateList></Extn></Shipment>");
		return docinputToCarrierUpdate;
	}

	// HUB-7713 - PO should be able to be received in created status - End

	/**
	 * This method checks whether the incoming lines ,if for a TO, is in
	 * delivered status or not.
	 * 
	 * @param inXml
	 * @return
	 */
	private boolean checkTOStatus(YFCDocument inXml, String sOrderName) {

		YFCDocument yfcDocGetOrderListOp = callGetOrderList(sOrderName);
		boolean isTODelivered = true;
		YFCNodeList<YFCElement> yfcNlReceiptLine = inXml
				.getElementsByTagName(TelstraConstants.RECEIPT_LINE);
		for (YFCElement yfcEleReceiptLine : yfcNlReceiptLine) {
			String sPrimeLineNo = yfcEleReceiptLine
					.getAttribute(TelstraConstants.PRIME_LINE_NO);

			String sMaxLineStatus = XPathUtil.getXpathAttribute(
					yfcDocGetOrderListOp, "//OrderLine[@PrimeLineNo='"
							+ sPrimeLineNo + "']/@MaxLineStatus");
			if ("3350.10000".compareTo(sMaxLineStatus) > 0
					&& TelstraConstants.DOCUMENT_TYPE_0006
					.equals(yfcDocGetOrderListOp
							.getElementsByTagName(
									TelstraConstants.ORDER)
							.item(0)
							.getAttribute(
									TelstraConstants.DOCUMENT_TYPE))) {
				isTODelivered = false;
				break;
			}
		}
		LoggerUtil.verboseLog("ReceiveOrder::checkTOStatus::isTODelivered :: ",
				logger, isTODelivered);

		return isTODelivered;
	}

	/**
	 * 
	 * @param sOrderName
	 * @return
	 */
	private YFCDocument callGetOrderList(String sOrderName) {

		YFCDocument yfcDocGetOrderListOp = null;
		if (!YFCCommon.isStringVoid(sOrderName)) {
			YFCDocument yfcDocGetOrderListIp = YFCDocument
					.getDocumentFor("<Order OrderName='" + sOrderName + "'/>");
			LoggerUtil.verboseLog(
					"ReceiveOrder::callGetOrderList::yfcDocGetOrderListIp\n",
					logger, yfcDocGetOrderListIp);
			YFCDocument yfcDocGetOrderListTemp = YFCDocument
					.getDocumentFor("<OrderList><Order OrderName='' OrderNo='' DocumentType='' EntryType='' OrderHeaderKey=''> "
							+ "<OrderLines> <OrderLine OrderLineKey='' OrderedQty='' PrimeLineNo='' "
							+ "OrderHeaderKey='' MaxLineStatus='' MinLineStatus='' Status='' ShipNode='' "
							+ "StatusQuantity='' ReceivingNode='' ><Item ItemID='' UnitOfMeasure='' />"
							+ "<OrderStatuses ><OrderStatus StatusQty='' TotalQuantity='' Status='' OrderReleaseKey=''/>"
							+ "</OrderStatuses></OrderLine> </OrderLines></Order></OrderList>");
			LoggerUtil.verboseLog(
					"ReceiveOrder::callGetOrderList::yfcDocGetOrderListTemp\n",
					logger, yfcDocGetOrderListTemp);
			yfcDocGetOrderListOp = invokeYantraApi(
					TelstraConstants.API_GET_ORDER_LIST, yfcDocGetOrderListIp,
					yfcDocGetOrderListTemp);
			LoggerUtil.verboseLog(
					"ReceiveOrder::callGetOrderList::yfcDocGetOrderListOp\n",
					logger, yfcDocGetOrderListOp);

			//HUB-8082 [Start]
			//if the invalid order name is passed i.e. it does not exist in the system. Below piece of code
			//will throw an error

			YFCElement orderListEle = yfcDocGetOrderListOp.getDocumentElement();
			if(!orderListEle.hasChildNodes()) {
				LoggerUtil.verboseLog("Order does not exist", logger, " throwing exception");
				throw ExceptionUtil.getYFSException("TEL_ERR_1171_005", new YFSException());
			}
			//HUB-8082 [End]
		}
		return yfcDocGetOrderListOp;
	}

	/**
	 * This method will return the best match Shipment/Shipments 
	 * @param nlShipment
	 * @param sPrimeLineNo
	 * @param dQuantity
	 * @return ShipmentList
	 */
	private List<YFCElement> chooseBestShipment(YFCNodeList<YFCElement> nlShipment,
			String sPrimeLineNo, double dQuantity) {

		Map<YFCElement, Double> shipMap = new HashMap<YFCElement, Double>();
		for (YFCElement eleShipment : nlShipment) {
			// iterate through all the lines of Shipment and get the quantity
			// for the item.
			for (YFCElement eleShipmentLine : eleShipment
					.getElementsByTagName("ShipmentLine")) {

				//HUB-8317 [START]
				/* Replacing comparing Items with the Prime Line No */
				YFCElement eleOrderLine = eleShipmentLine.getElementsByTagName(TelstraConstants.ORDER_LINE).item(0);
				String sShipmentsOrderLineNo = "";
				if(!YFCObject.isVoid(eleOrderLine)){
					sShipmentsOrderLineNo = eleOrderLine.getAttribute(TelstraConstants.PRIME_LINE_NO,"");
				}
				//HUB-8317 [END]

				double dShipmentLineQuantity = eleShipmentLine
						.getDoubleAttribute("Quantity", 0.0);
				double dShipmentLineReceivedQuantity = eleShipmentLine
						.getDoubleAttribute("ReceivedQuantity", 0.0);
				if (sShipmentsOrderLineNo.equals(sPrimeLineNo)) {
					double availableToReceiveQuantity = dShipmentLineQuantity
							- dShipmentLineReceivedQuantity;
					if (availableToReceiveQuantity > 0.0) {
						shipMap.put(eleShipment, availableToReceiveQuantity);

						//HUB-8317 [START]
						/*Exit out of shipmentLine to next shipment if a availableToReceiveQuantity is found after adding the shipment to the map. */
						break;
					}
					//break;

					//HUB-8317 [END]
				}
			}

		}
		if (shipMap.size() == 0) {
			return null;
		}
		Set<Entry<YFCElement, Double>> set = shipMap.entrySet();
		List<Entry<YFCElement, Double>> list = new ArrayList<Entry<YFCElement, Double>>(
				set);
		Collections.sort(list, new Comparator<Map.Entry<YFCElement, Double>>() {
			//HUB-8315[START]

			/* Sort Shipment From higher Status to lower status and if status are equal then sort them from lower quantity to higher quantity. This is done so that 
			 * the shipment the shipment with the highest status get received first. */

			public int compare(Map.Entry<YFCElement, Double> o1,
					Map.Entry<YFCElement, Double> o2) {
				int iQtyCompartorValue = (o2.getKey().getAttribute("Status")).compareTo(o1.getKey().getAttribute("Status"));

				if (iQtyCompartorValue==0){
					iQtyCompartorValue = (o1.getValue()).compareTo(o2.getValue());
				}

				return iQtyCompartorValue;
			}           
		});
		//HUB-8315[END]

		/* iterate through map and check if we have record matching line qty, if not then get to splitShipmentToReceive method and return the shipmentList. */

		List<YFCElement> lShipmentlist = new ArrayList<YFCElement>();

		for (Map.Entry<YFCElement, Double> entry : list) {
			if (entry.getValue() == dQuantity) {
				lShipmentlist.add(entry.getKey());
				return lShipmentlist;
			}
		}

		//return list.iterator().next().getKey();

		//HUB-8079 [START]
		//The method will receive shipments based on the Qty passed from higher shipment status to lower.
		return splitShipmentToReceive(list,dQuantity);
	}

	/**
	 * The method will receive shipments based on the Qty passed from higher shipment status to lower.
	 * @param dQuantity 
	 * @param list 
	 * @param list
	 * @param dQuantity
	 * @return
	 */
	private List<YFCElement> splitShipmentToReceive(List<Entry<YFCElement, Double>> list, double dQuantity) {

		List<YFCElement> lShipmentlist = new ArrayList<YFCElement>();
		List<Entry<YFCElement, Double>> lMaxShipmentlist = new ArrayList<Entry<YFCElement, Double>>();

		Map.Entry<YFCElement, Double> entryShipment = list.get(0);

		/*From the sorted List find the highest status*/
		double dMaxStatus = entryShipment.getKey().getDoubleAttribute(TelstraConstants.STATUS);
		double dTobeReceivedQty= dQuantity;

		/*Group Included in receipt and delivered together in a List.*/
		if(YFCObject.equals(dMaxStatus, 1600)){
			for(Map.Entry<YFCElement, Double> entry : list) {
				double dShipmentStatus =  entry.getKey().getDoubleAttribute(TelstraConstants.STATUS);
				if(YFCObject.equals(dShipmentStatus, 1600) || YFCObject.equals(dShipmentStatus, 1500)){
					lMaxShipmentlist.add(entry);

				}
			}
		}else{
			/*Else Group the highest Shipment Status in a List.*/
			for(Map.Entry<YFCElement, Double> entry : list) {
				double dShipmentStatus =  entry.getKey().getDoubleAttribute(TelstraConstants.STATUS);
				if(YFCObject.equals(dShipmentStatus, dMaxStatus)){
					lMaxShipmentlist.add(entry);

				}
			}
		}

		Integer dNoOfShipment = lMaxShipmentlist.size();

		//Counter to identify the the index of the Shipment List
		Integer counter = 0;

		/*Iterate through the grouped Max Status Shipment List and Add the Shipment/Shipments to a List which will later be used Receive. The Qty 
		 * to receive against the shipment is set at the Shipment header level with attribute Shipment Quantity for each shipment,
		 * this quantity will be used will creating the receive order Input.If last shipment or only shipment the entire Quantity to be received will be 
		 * received against the shipment, if not the the available Quantity for the Shipment will be set
		 * */
		if(!lMaxShipmentlist.isEmpty()){
			for (Map.Entry<YFCElement, Double> entry : lMaxShipmentlist) {
				counter++;
				double dShipmentQty = entry.getValue();
				if(dTobeReceivedQty <= 0.0){
					return lShipmentlist;
				}
				if(dTobeReceivedQty<=dShipmentQty ){
					//set dTobeReceivedQty in shipment
					entry.getKey().setAttribute(TelstraConstants.SHIPMENT_QUANTITY,dTobeReceivedQty);			
					dTobeReceivedQty = dTobeReceivedQty - dShipmentQty;					
					lShipmentlist.add(entry.getKey());

				}else{

					if(dNoOfShipment == counter){
						//set dTobeReceivedQty in shipment if last shipment or only shipment
						lShipmentlist.add(entry.getKey());
						entry.getKey().setAttribute(TelstraConstants.SHIPMENT_QUANTITY,dTobeReceivedQty);
						return lShipmentlist;

					}else{

						//set dShipmentQty in shipment if not last shipment
						entry.getKey().setAttribute(TelstraConstants.SHIPMENT_QUANTITY,dShipmentQty);
						dTobeReceivedQty = dTobeReceivedQty - dShipmentQty;
						lShipmentlist.add(entry.getKey());

					}
				}
			}
		}
		return lShipmentlist;
	}

	//HUB-8079 [END]

	private YFCDocument getReceiveOrderInput(YFCElement eleReceiptLine,
			YFCElement eleShipment, String sPrimeLineNo) {
		/*
		 * Input to receiveOrder:- <Receipt DocumentType="0005"
		 * ReceivingNode="DC1" EnterpriseCode="XYZ-CORP"> <Shipment
		 * ShipmentKey="S0000001" ReceivingNode="DC1"/> <ReceiptLines>
		 * <ReceiptLine ItemID="NOR-00001" Quantity="50"/> </ReceiptLines>
		 * </Receipt>
		 */

		/*
		 * If receiving node is not present in the shipment, then skip the receive order call. For now this scenario will happen for ZX 
		 * node, where B2Bi don't pass the ZX receiving node in the manage order xml.
		 */
		if(YFCCommon.isStringVoid(eleShipment.getAttribute("ReceivingNode", ""))){
			isReceivingNodePresentInShipment = false;
			return null;
		}
		YFCDocument docReceiveOrderInput = YFCDocument
				.getDocumentFor("<Receipt DocumentType='"
						+ eleShipment.getAttribute("DocumentType", "")
						+ "' ReceivingNode='"
						+ eleShipment.getAttribute("ReceivingNode", "")
						+ "' EnterpriseCode='"
						+ eleShipment.getAttribute("EnterpriseCode", "")
						+ "'><Shipment ShipmentKey='"
						+ eleShipment.getAttribute("ShipmentKey", "")
						+ "' ReceivingNode='"
						+ eleShipment.getAttribute("ReceivingNode", "")
						+ "'/><ReceiptLines/></Receipt>");

		String sItemID = eleReceiptLine.getAttribute(TelstraConstants.ITEM_ID,
				"");

		//HUB-8079[START]

		/*This value has been set in the chooseBestShipment Method these values could be equal or lower than the Quantity (to be received) passed in the input */

		double dQuantity = eleShipment.getDoubleAttribute(TelstraConstants.SHIPMENT_QUANTITY, 0.0);
		if(dQuantity <= 0.0){
			dQuantity = eleReceiptLine.getDoubleAttribute(
					TelstraConstants.QUANTITY, 0.0);
		}

		//HUB-8079[END]

		// HUB-5292 change
		String sShipmentLineKey = null;
		for (YFCElement eleShipmentLine : eleShipment
				.getElementsByTagName("ShipmentLine")) {
			for (YFCElement eleOrderLine : eleShipmentLine
					.getElementsByTagName("OrderLine")) {
				String sOrderPrimeLineNo = eleOrderLine.getAttribute(
						TelstraConstants.PRIME_LINE_NO, "");
				if (YFCObject.equals(sOrderPrimeLineNo, sPrimeLineNo)) {
					sShipmentLineKey = eleShipmentLine
							.getAttribute("ShipmentLineKey");
					break;
				}
			}
			if (!YFCObject.isNull(sShipmentLineKey)) {
				break;
			}
		}

		if (!YFCObject.isNull(sItemID)) {
			YFCElement eleLine = docReceiveOrderInput
					.createElement(TelstraConstants.RECEIPT_LINE);
			eleLine.setAttribute(TelstraConstants.ITEM_ID, sItemID);
			eleLine.setAttribute("ShipmentLineKey", sShipmentLineKey);
			eleLine.setDoubleAttribute(TelstraConstants.QUANTITY, dQuantity);
			docReceiveOrderInput
			.getElementsByTagName(TelstraConstants.RECEIPT_LINES)
			.item(0).appendChild(eleLine);
		}
		return docReceiveOrderInput;
	}

	/**
	 * The method finds the shipment for the passed orderName, ReceivingNode,
	 * ItemId, PrimeLineNo and ShipmentNo (if passed in the input) between 1200
	 * (Shipment Being Picked) and 1600 (Included In Receipt) status.
	 * 
	 * @param inXml
	 * @param sItemID
	 * @param sPrimeLineNo
	 * @param sShipmentNo
	 * @param isShipmentBasedReceiving
	 * @return
	 */
	private YFCDocument getShipmentListForItem(YFCDocument inXml,
			String sItemID, String sPrimeLineNo, String sShipmentNo,
			boolean isShipmentBasedReceiving) {

		YFCElement eleReceipt = inXml.getElementsByTagName(
				TelstraConstants.RECEIPT).item(0);

		String sOrderName = eleReceipt.getAttribute(
				TelstraConstants.ORDER_NAME, "");
		String sReceivingNode = eleReceipt.getAttribute(
				TelstraConstants.RECEIVING_NODE, "");
		/*
		 * Input to getShipmentList:- <Shipment ReceivingNode="TestOrg5"
		 * StatusQryType="BETWEEN" FromStatus="1200" ToStatus="1599">
		 * <ShipmentLines> <ShipmentLine ItemID="testitem1"> <Order
		 * OrderName="3003PO10"/> <OrderLine PrimeLineNo="1"/> </ShipmentLine>
		 * </ShipmentLines> </Shipment>
		 */

		YFCDocument docGetShipmentListInput = null;
		if (isShipmentBasedReceiving) {
			if (!YFCObject.isVoid(sShipmentNo)) {
				docGetShipmentListInput = YFCDocument
						.getDocumentFor("<Shipment ReceivingNode='"
								+ sReceivingNode + "' " + "ShipmentNo='"
								+ sShipmentNo + "'><ShipmentLines/></Shipment>");
			} else {
				return YFCDocument.getDocumentFor("<Shipments />");
			}
		} else if (YFCObject.isVoid(sShipmentNo) && !isShipmentBasedReceiving) {
			docGetShipmentListInput = YFCDocument
					.getDocumentFor("<Shipment ReceivingNode='"
							+ sReceivingNode
							+ "' "
							+ "StatusQryType='BETWEEN' FromStatus='1200' ToStatus='1600'><ShipmentLines/></Shipment>");
		}

		if (!YFCObject.isNull(sItemID)) {
			YFCElement eleShipmentLine = docGetShipmentListInput
					.createElement(TelstraConstants.SHIPMENT_LINE);
			eleShipmentLine.setAttribute(TelstraConstants.ITEM_ID, sItemID);
			docGetShipmentListInput
			.getElementsByTagName(TelstraConstants.SHIPMENT_LINES)
			.item(0).appendChild(eleShipmentLine);

			YFCElement eleShipmentLineOrder = docGetShipmentListInput
					.createElement(TelstraConstants.ORDER);
			eleShipmentLineOrder.setAttribute(TelstraConstants.ORDER_NAME,
					sOrderName);
			eleShipmentLine.appendChild(eleShipmentLineOrder);

			if (!YFCObject.isNull(sPrimeLineNo)) {
				YFCElement eleShipmentLineOrderLine = docGetShipmentListInput
						.createElement(TelstraConstants.ORDER_LINE);
				eleShipmentLineOrderLine.setAttribute(
						TelstraConstants.PRIME_LINE_NO, sPrimeLineNo);
				eleShipmentLine.appendChild(eleShipmentLineOrderLine);
			}
		} else {
			// throw exception
			LoggerUtil.verboseLog("ItemID is mandatory ", logger,
					" throwing exception");
			throw ExceptionUtil.getYFSException("TEL_ERR_1181_002",
					new YFSException());
		}

		YFCDocument docGetShipmentListOutput = null;
		if (docGetShipmentListInput.getElementsByTagName(
				TelstraConstants.SHIPMENT_LINE).getLength() > 0) {
			docGetShipmentListOutput = invokeYantraApi(
					TelstraConstants.GET_SHIPMENT_LIST_API,
					docGetShipmentListInput, getShipmentListTempate());
		}
		return docGetShipmentListOutput;
	}

	private YFCDocument getShipmentListTempate() {
		YFCDocument template = YFCDocument
				.getDocumentFor("<Shipments>"
						+ "<Shipment EnterpriseCode='' DocumentType='' ReceivingNode='' ShipmentNo='' "
						+ "ShipmentKey='' Status=''>"
						+ "<ShipmentLines>"
						+ "<ShipmentLine Quantity='' ShipmentLineKey='' ItemID='' ReceivedQuantity='' "
						+ "PrimeLineNo=''>" + "<OrderLine PrimeLineNo=''/>"
						+ "</ShipmentLine>" + "</ShipmentLines>"
						+ "</Shipment></Shipments>");
		return template;
	}

	/**
	 * This method copy the incoming shipment line to confirm shipment input. On top of that it appends order header and shipment line key to each line
	 * @param inputApiDoc
	 * @param outputGetShipmentList
	 */
	private void insertSerialNumInCustomTable (YFCDocument receiveOrderOutput) {

		YFCNodeList<YFCElement> nlShipmentSerialNumber = inXml.getElementsByTagName(TelstraConstants.SHIPMENT_SERIAL_NUMBER);
		if(nlShipmentSerialNumber.getLength()>0){
			setTxnObject("yfcDocShipmentSerialNum", inXml);
			YFCElement yfcEleShipemnt = receiveOrderOutput.getElementsByTagName(TelstraConstants.SHIPMENT).item(0);
			if(!YFCCommon.isVoid(yfcEleShipemnt)){
				String sShipmentKey = yfcEleShipemnt.getAttribute(TelstraConstants.SHIPMENT_KEY);
				for(YFCElement yfcEleReceiptLineIp : inXml.getElementsByTagName(TelstraConstants.RECEIPT_LINE)){

					String sPrimeLineNo = yfcEleReceiptLineIp.getAttribute(TelstraConstants.PRIME_LINE_NO);
					String sShipmentLineKey = XPathUtil.getXpathAttribute(receiveOrderOutput, "//ReceiptLine[@PrimeLineNo='"+sPrimeLineNo+"']/@ShipmentLineKey");
					if(!YFCCommon.isStringVoid(sShipmentLineKey)){
						for(YFCElement yfcEleShipmentSerialNum : yfcEleReceiptLineIp.getElementsByTagName(TelstraConstants.SHIPMENT_SERIAL_NUMBER)){
							String sSerialNumber = yfcEleShipmentSerialNum.getAttribute(TelstraConstants.SERIAL_NO);

							YFCDocument yfcDocCreateShipmentSerialNum = YFCDocument.getDocumentFor("<ShipmentSerialNumber/>");
							YFCElement yfcEleCreateShipmentSerialNumRoot = yfcDocCreateShipmentSerialNum.getDocumentElement();
							yfcEleCreateShipmentSerialNumRoot.setAttribute(TelstraConstants.SHIPMENT_KEY, sShipmentKey);
							yfcEleCreateShipmentSerialNumRoot.setAttribute(TelstraConstants.SHIPMENT_LINE_KEY, sShipmentLineKey);
							yfcEleCreateShipmentSerialNumRoot.setAttribute(TelstraConstants.SERIAL_NO, sSerialNumber);
							yfcEleCreateShipmentSerialNumRoot.setAttribute(TelstraConstants.TRANSACTION_TYPE, TelstraConstants.RECEIPT);
							invokeYantraService(TelstraConstants.API_CREATE_SHIPMENT_SERIAL_NUMBER, yfcDocCreateShipmentSerialNum);
						}
					}
				}
			}
		}
	}

}
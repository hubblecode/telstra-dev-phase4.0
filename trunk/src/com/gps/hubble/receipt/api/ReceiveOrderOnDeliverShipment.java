/***********************************************************************************************
 * File	Name		: ReceiveOrderOnDeliverShipment.java *
 * Description		: This class is called from deliverShipment On Success Event. Will check 
 * 						if receiving Node NodeType belongs to particular list then call 
 * 						receiveOrder.
 * 
 * Modification	Log	:
 * ---------------------------------------------------------------------------------------------
 * Ver #	Date			Author					Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0		Jul 17,2016	  	Tushar Dhaka 		   	Initial	Version 
 * ---------------------------------------------------------------------------------------------
 **********************************************************************************************/

package com.gps.hubble.receipt.api;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.gps.hubble.constants.TelstraConstants;


/**
 * Custom code to call receiveOrder API
 * 
 * @author Tushar Dhaka
 * @version 1.0
 * 
 * Extends AbstractCustomApi class
 */
public class ReceiveOrderOnDeliverShipment extends AbstractCustomApi{
	
	private static YFCLogCategory logger = YFCLogCategory.instance(ReceiveOrderOnDeliverShipment.class);
	
	public YFCDocument invoke(YFCDocument inXml) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(),"invoke", inXml);
		/*
		 * Input XML:-
			<Shipment BuyerOrganizationCode="" 	DocumentType="" EnterpriseCode="" OrderHeaderKey="" 	OrderNo="" ReceivingNode="" SellerOrganizationCode="" 	
			ShipNode="" ShipmentKey="" ShipmentNo="" Status="" StatusDate="" StationId=" " StationType=" ">
				<ShipmentLines>
					<ShipmentLine DocumentType="" ItemDesc=""	OrderNo="" PrimeLineNo="" ReleaseNo="" ShipmentLineNo=""	ShipmentSubLineNo="" SubLineNo="" 
					ItemID="" UnitOfMeasure="" ProductClass="">
					</OrderLine>
				</ShipmentLine>
				</ShipmentLines>
				<ShipmentStatusAudits>
					<ShipmentStatusAudit ShipmentStatusAuditKey="" OldStatus="" NewStatus="" OldStatusDate="" NewStatusDate="" ReasonCode="" ReasonText=""/>
				</ShipmentStatusAudits>
			</Shipment>

		 */
				
		YFCElement eleShipment = inXml.getDocumentElement();
		String sReceivingNode = eleShipment.getAttribute(TelstraConstants.RECEIVING_NODE,"");
		if(!YFCObject.isNull(sReceivingNode)) {
			if(callReceiveOrder(sReceivingNode)) {
				YFCDocument docReceiveOrderInput = YFCDocument.getDocumentFor("<Receipt DocumentType='"+eleShipment.getAttribute("DocumentType","")+
						"' ReceivingNode='"+eleShipment.getAttribute("ReceivingNode","")+"' EnterpriseCode='"+eleShipment.getAttribute("EnterpriseCode","")+
						"'><Shipment ShipmentKey='"+eleShipment.getAttribute("ShipmentKey","")+"' ReceivingNode='"+eleShipment.getAttribute("ReceivingNode","")+
						"'/><ReceiptLines/></Receipt>");
				
				for(YFCElement eleShipmentLine : eleShipment.getElementsByTagName(TelstraConstants.SHIPMENT_LINE)) {
					String sItemID = eleShipmentLine.getAttribute(TelstraConstants.ITEM_ID,"");
					double dQuantity = eleShipmentLine.getDoubleAttribute(TelstraConstants.QUANTITY,0.0);
					String sShipmentLineKey = eleShipmentLine.getAttribute("ShipmentLineKey","");
					if(!YFCObject.isNull(sItemID)) {
						YFCElement eleLine = docReceiveOrderInput.createElement(TelstraConstants.RECEIPT_LINE);
						eleLine.setAttribute(TelstraConstants.ITEM_ID, sItemID);
						eleLine.setDoubleAttribute(TelstraConstants.QUANTITY, dQuantity);
						eleLine.setAttribute("ShipmentLineKey", sShipmentLineKey);
						docReceiveOrderInput.getElementsByTagName(TelstraConstants.RECEIPT_LINES).item(0).appendChild(eleLine);
					}
				}
				String sReceiveOrderService = getProperty("Service.receiveOrder", true);
				invokeYantraService(sReceiveOrderService, docReceiveOrderInput);
			}
		}
				
		LoggerUtil.endComponentLog(logger, this.getClass().getName(),"invoke", inXml);
		return inXml;
	}

	private boolean callReceiveOrder(String sReceivingNode) {
		boolean bCallReceiveOrder = false;
		YFCDocument docNodeListinXml= YFCDocument.getDocumentFor("<Shipment ShipNode='"+sReceivingNode+"' />");
		YFCDocument doctempgetNodeListinXml = YFCDocument.getDocumentFor("<ShipNodeList><ShipNode NodeType=''/></ShipNodeList>");
		YFCDocument outXml = invokeYantraApi(TelstraConstants.API_GET_SHIP_NODE_LIST, docNodeListinXml, doctempgetNodeListinXml);	
		YFCElement eleShipNode=outXml.getDocumentElement().getChildElement(TelstraConstants.SHIP_NODE);
		if(!YFCElement.isNull(eleShipNode)){
			String sNodeType=outXml.getDocumentElement().getChildElement(TelstraConstants.SHIP_NODE).getAttribute(TelstraConstants.NODE_TYPE,"");
			String sNodeTypeList = getProperty("NodeTypeList", true);
			
			String[] serviceNodeTypeList = sNodeTypeList.split(",");
            for (String serviceNodeType : serviceNodeTypeList) {
            	if(YFCObject.equals(sNodeType, serviceNodeType)) {
            		bCallReceiveOrder = true;
            		break;
            	}
            }
		}
		return bCallReceiveOrder;
	}
}
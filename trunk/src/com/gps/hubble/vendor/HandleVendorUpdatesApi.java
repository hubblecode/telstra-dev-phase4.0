package com.gps.hubble.vendor;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.constants.TelstraErrorCodeConstants;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.date.YDate;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCDate;
import com.yantra.yfs.japi.YFSException;

public class HandleVendorUpdatesApi extends AbstractCustomApi{
	private static YFCLogCategory logger = YFCLogCategory.instance(HandleVendorUpdatesApi.class);


	enum ActionType{
		ACCEPT,REJECT,COMMIT,COMMIT_ALL, REJECT_ALL;
	}

	private Map<String, LineCommitmentsInfo> lineCommitmentsInfo;
	private boolean isCommitmentMismatch = false;
	private Map<String, Double> commitmentsMap = null;
	@Override
	public YFCDocument invoke(YFCDocument inXml) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", inXml);
		
		String sOrderName = inXml.getDocumentElement().getAttribute("OrderName");
		YFCDocument yfcDocGetOrderListOp = callGetOrderList(sOrderName);
		YFCNodeList<YFCElement> orderLines = inXml.getElementsByTagName("OrderLine");
		for(YFCElement orderLine : orderLines) {
		  String sPrimeLineNo = orderLine.getAttribute("PrimeLineNo");
	      YFCElement inOrderLineCommitments = orderLine.getElementsByTagName("OrderLineCommitmentList").item(0);
	      
	      //orderLine.setAttribute("IsActive", "Y");
		  
	      YFCElement yfcEleOrderListOrderLine = XPathUtil.getXPathElement(yfcDocGetOrderListOp, "//OrderLine[@PrimeLineNo='"+sPrimeLineNo+"']");
		  YFCDate reqDeliveryDate =  yfcEleOrderListOrderLine.getDateAttribute("ReqDeliveryDate");
		  YFCNodeList<YFCElement> yfcNlOrderListOrderStatuses = yfcEleOrderListOrderLine.getElementsByTagName("OrderStatus");
		  double committedQty = 0.0;
		  for(YFCElement orderStatus : yfcNlOrderListOrderStatuses) {
		    double statusQty = orderStatus.getDoubleAttribute("StatusQty");
		    if(orderStatus.getDoubleAttribute("Status") >= 1260 ) {
		      committedQty = committedQty + statusQty;
		    }
		  }
		  
		  YFCNodeList<YFCElement> inOrderLineCommitmentList = orderLine.getElementsByTagName("OrderLineCommitment");
          double totalIncomingCommittedQty = 0.0;
          for(YFCElement inOrderLineCommitment : inOrderLineCommitmentList) {
        	  inOrderLineCommitment.setAttribute("IsActive", "Y");
        	  if(inOrderLineCommitment.hasAttribute("CommitRequestDate")){
        		  inOrderLineCommitment.removeAttribute("CommitRequestDate");
        	  }
        	  double inCommittedQty = inOrderLineCommitment.getDoubleAttribute("CommittedQuantity");
        	  totalIncomingCommittedQty = totalIncomingCommittedQty + inCommittedQty;
        	  YFCDate commitDeliveryDate = inOrderLineCommitment.getDateAttribute("CommitDeliveryDate");
        	  if(YFCObject.isVoid(reqDeliveryDate)){
        		  reqDeliveryDate = commitDeliveryDate;
        	  }
        	  else if(!YFCObject.isVoid(commitDeliveryDate) && commitDeliveryDate.after(reqDeliveryDate)) {
        		  reqDeliveryDate = commitDeliveryDate;
        	  }
          }
          YFCElement dateTypes = orderLine.createChild("OrderDates");
          YFCElement dateType = dateTypes.createChild("OrderDate");
          dateType.setAttribute("DateTypeId", "GPS_COMMIT_REQUESTED_DATE");
          dateType.setDateAttribute("RequestedDate", reqDeliveryDate);
          double committableQty =  totalIncomingCommittedQty - committedQty;
          if(YFCObject.isVoid(commitmentsMap)) {
            commitmentsMap = new HashMap<String, Double>();
          }
          commitmentsMap.put(sPrimeLineNo, committableQty);
		  YFCNodeList<YFCElement> yfcNlOrderListOrderLineCommitment = yfcEleOrderListOrderLine.getElementsByTagName(TelstraConstants.ORDER_LINE_COMMITMENT);
		  for(YFCElement orderLineCommitment : yfcNlOrderListOrderLineCommitment) {
		    YFCElement inOrderLineCommitment = inXml.importNode(orderLineCommitment, true);
		    inOrderLineCommitment.setAttribute("IsActive", "N");
		    inOrderLineCommitments.appendChild(inOrderLineCommitment);
		  }
		}
		
		lineCommitmentsInfo = new HashMap<>();
		ActionType actionType = getActionType(inXml);
		boolean rejected = false;
		YFCDocument output = null;
		if(actionType == ActionType.ACCEPT){
			output = acceptOrder(inXml);
		}else if(actionType == ActionType.REJECT_ALL){
			output = rejectEntireOrder(inXml);
			rejected = true;
		}else if(actionType == ActionType.COMMIT){
			output = commitOrderLine(inXml);
		}else if(actionType == ActionType.COMMIT_ALL){
			output = commitEntireOrder(inXml);
		}
		else if(actionType == ActionType.REJECT){
			output = rejectOrderLines(inXml);
			rejected = true;
		}
		if(rejected || isCommitmentMismatch){
			YFCDocument docOrderDetails = getOrderDetails(inXml);
			if(rejected){
				LoggerUtil.verboseLog("raising vendor commitment mismatch alert as vendor has rejected the order", logger, inXml);
				docOrderDetails.getDocumentElement().setAttribute(TelstraConstants.VENDOR_REJECTED, "true");
			}else{
				LoggerUtil.verboseLog("raising vendor commitment mismatch alert as there is a mismatch", logger, inXml);
				docOrderDetails.getDocumentElement().setAttribute(TelstraConstants.IS_COMMITMENT_MISMATCH,"true");
			}
			raiseAlert(docOrderDetails);
		}else{
			//No mismatch resolve any open alert
			YFCDocument docExceptionList = getOpenAlerts(inXml);
			if(hasOpenAlert(docExceptionList)){
				LoggerUtil.verboseLog("As there is an open alert will try to resolve the alert", logger, docExceptionList);
				resolveAlerts(docExceptionList);
			}
		}
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", inXml);
		return output;
	}

	/**
	 * 
	 * @param inXml
	 * @return
	 */
	private YFCDocument rejectOrderLines(YFCDocument inXml) {

		LoggerUtil.debugLog("HandleVendorUpdatesApi:input to rejectOrderLines ", logger, inXml);
		YFCElement elem = inXml.getDocumentElement();
		YFCDocument inDoc = YFCDocument.createDocument(TelstraConstants.ORDER_STATUS_CHANGE);
		YFCElement inElem = inDoc.getDocumentElement();
		inElem.setAttribute(TelstraConstants.ORDER_HEADER_KEY, elem.getAttribute(TelstraConstants.ORDER_HEADER_KEY));
		inElem.setAttribute("TransactionId", TelstraConstants.TRANSACTION_VENDOR_UPDATE);

		YFCElement yfcEleOrderLines = inElem.createChild(TelstraConstants.ORDER_LINES);


		for(YFCElement yfcEleIpOrderLine : inXml.getElementsByTagName(TelstraConstants.ORDER_LINE)){			

			YFCElement yfcEleOrderLine = yfcEleOrderLines.createChild(TelstraConstants.ORDER_LINE);		
			yfcEleOrderLine.setAttribute(TelstraConstants.ORDER_LINE_KEY, yfcEleIpOrderLine.getAttribute(TelstraConstants.ORDER_LINE_KEY));
			yfcEleOrderLine.setAttribute("BaseDropStatus", "9000.10000");
			yfcEleOrderLine.setAttribute("ChangeForAllAvailableQty", "Y");		
		}
		LoggerUtil.debugLog("rejectOrderLines:input to changeOrderStatus api", logger,inDoc);
		invokeYantraApi(TelstraConstants.API_CHANGE_ORDER_STATUS, inDoc);
		//input to change order api 
		YFCDocument changeOrderInDoc = YFCDocument.createDocument("Order");
		YFCElement changeOrderInElem = changeOrderInDoc.getDocumentElement();		
		changeOrderInElem.setAttribute(TelstraConstants.ORDER_HEADER_KEY, elem.getAttribute(TelstraConstants.ORDER_HEADER_KEY));

		YFCElement yfcEleOrderLinesChangeOrder = changeOrderInElem.createChild(TelstraConstants.ORDER_LINES);

		for(YFCElement yfcEleIpOrderLine : inXml.getElementsByTagName(TelstraConstants.ORDER_LINE)){

			YFCElement yfcEleOrderLine = yfcEleOrderLinesChangeOrder.createChild(TelstraConstants.ORDER_LINE);		
			yfcEleOrderLine.setAttribute(TelstraConstants.ORDER_LINE_KEY, yfcEleIpOrderLine.getAttribute(TelstraConstants.ORDER_LINE_KEY));
			YFCElement notesElem = changeOrderInElem.createChild(TelstraConstants.NOTES);
			YFCElement noteElem = notesElem.createChild(TelstraConstants.NOTE);
			noteElem.setAttribute(TelstraConstants.NOTE_TEXT, TelstraConstants.ORDER_REJECTED_NOTE_TEXT);
			noteElem.setAttribute(TelstraConstants.CONTACT_USER, getUserId());
		}

		LoggerUtil.debugLog("rejectOrderLines:input to changeOrder api", logger,inDoc);
		YFCDocument template=createTemplate();

		return invokeYantraApi(TelstraConstants.API_CHANGE_ORDER, changeOrderInDoc,template);
	}

	private YFCDocument acceptOrder(YFCDocument inXml){
		LoggerUtil.debugLog("HandleVendorUpdatesApi:input to acceptOrder ", logger, inXml);
		YFCElement elem = inXml.getDocumentElement();
		//to check whether the order is less than po sent to vendor status
		String poSentToVendor = elem.getAttribute(TelstraConstants.PO_SENT_TO_VENDOR);
		String checkStatus ="N";
		YFCDocument inDoc = YFCDocument.createDocument("Order");
		YFCElement inElem = inDoc.getDocumentElement();
		inElem.setAttribute(TelstraConstants.ACTION, "MODIFY");
		inElem.setAttribute(TelstraConstants.ORDER_NO, elem.getAttribute(TelstraConstants.ORDER_NO));
		inElem.setAttribute(TelstraConstants.ORDER_HEADER_KEY, elem.getAttribute(TelstraConstants.ORDER_HEADER_KEY));
		inElem.setAttribute(TelstraConstants.ENTERPRISE_CODE, elem.getAttribute(TelstraConstants.ENTERPRISE_CODE));
		YFCElement datesElem = inElem.createChild(TelstraConstants.ORDER_DATES);
		YFCElement dateElem = datesElem.createChild(TelstraConstants.ORDER_DATE);
		dateElem.setAttribute(TelstraConstants.DATE_TYPE_ID, "ORDER_ACCEPTED");
		YFCElement notesElem = inElem.createChild(TelstraConstants.NOTES);
		YFCElement noteElem = notesElem.createChild(TelstraConstants.NOTE);
		noteElem.setAttribute(TelstraConstants.NOTE_TEXT, TelstraConstants.ORDER_ACCEPTED_NOTE_TEXT);
		noteElem.setAttribute(TelstraConstants.CONTACT_USER, getUserId());
		YFCElement extn = inElem.createChild(TelstraConstants.EXTN);
		extn.setAttribute(TelstraConstants.HAS_VENDOR_ACCEPTED, TelstraConstants.YES);
		//TODO Call changeOrderStatus for all quantity with Base Drop Status as 1100.10000 if the Current Order Status is  less than or equal to 1100.10000
		//TODO if PO is already in PO Sent to Vendor do not invoke changeOrderStatus api
		if(checkStatus.equalsIgnoreCase(poSentToVendor)){
			YFCDocument changeStatusInDoc = YFCDocument.createDocument(TelstraConstants.ORDER_STATUS_CHANGE);
			YFCElement inElement = changeStatusInDoc.getDocumentElement();
			inElement.setAttribute(TelstraConstants.ORDER_HEADER_KEY, elem.getAttribute(TelstraConstants.ORDER_HEADER_KEY));
			inElement.setAttribute("TransactionId", TelstraConstants.TRANSACTION_VENDOR_ACCEPT_UPDATE);
			inElement.setAttribute("BaseDropStatus", "1100.10000");
			inElement.setAttribute("ChangeForAllAvailableQty", "Y");
			LoggerUtil.debugLog("acceptOrder:input to changeOrderStatus api", logger,changeStatusInDoc);
			invokeYantraApi(TelstraConstants.API_CHANGE_ORDER_STATUS, changeStatusInDoc);
		}
		LoggerUtil.debugLog("acceptOrder:input to changeOrder api", logger,inDoc);
		//adding template
		YFCDocument template=createTemplate();
		return invokeYantraApi(TelstraConstants.API_CHANGE_ORDER, inDoc,template);
	}


	private YFCDocument rejectEntireOrder(YFCDocument inXml){
		LoggerUtil.debugLog("HandleVendorUpdatesApi:input to rejectOrder ", logger, inXml);
		YFCElement elem = inXml.getDocumentElement();
		YFCDocument inDoc = YFCDocument.createDocument(TelstraConstants.ORDER_STATUS_CHANGE);
		YFCElement inElem = inDoc.getDocumentElement();
		inElem.setAttribute(TelstraConstants.ORDER_HEADER_KEY, elem.getAttribute(TelstraConstants.ORDER_HEADER_KEY));
		inElem.setAttribute("TransactionId", TelstraConstants.TRANSACTION_VENDOR_UPDATE);
		inElem.setAttribute("BaseDropStatus", "9000.10000");
		inElem.setAttribute("ChangeForAllAvailableQty", "Y");
		LoggerUtil.debugLog("rejectOrder:input to changeOrderStatus api", logger,inDoc);
		invokeYantraApi(TelstraConstants.API_CHANGE_ORDER_STATUS, inDoc);
		//input to change order api 
		YFCDocument changeOrderInDoc = YFCDocument.createDocument("Order");
		YFCElement changeOrderInElem = changeOrderInDoc.getDocumentElement();
		changeOrderInElem.setAttribute(TelstraConstants.ACTION, "MODIFY");
		changeOrderInElem.setAttribute(TelstraConstants.ORDER_NO, elem.getAttribute(TelstraConstants.ORDER_NO));
		changeOrderInElem.setAttribute(TelstraConstants.ORDER_HEADER_KEY, elem.getAttribute(TelstraConstants.ORDER_HEADER_KEY));
		changeOrderInElem.setAttribute(TelstraConstants.ENTERPRISE_CODE, elem.getAttribute(TelstraConstants.ENTERPRISE_CODE));
		YFCElement notesElem = changeOrderInElem.createChild(TelstraConstants.NOTES);
		YFCElement noteElem = notesElem.createChild(TelstraConstants.NOTE);
		noteElem.setAttribute(TelstraConstants.NOTE_TEXT, TelstraConstants.ORDER_REJECTED_NOTE_TEXT);
		noteElem.setAttribute(TelstraConstants.CONTACT_USER, getUserId());
		LoggerUtil.debugLog("rejectOrder:input to changeOrder api", logger,changeOrderInDoc);
		YFCDocument template=createTemplate();
		return invokeYantraApi(TelstraConstants.API_CHANGE_ORDER, changeOrderInDoc,template);
	}
	/**
	 * template for change order
	 * @return
	 */
	private YFCDocument createTemplate(){
		String templateString = "<Order OrderHeaderKey=\"\" OrderNo=\"\" EnterpriseCode=\"\"><Notes NumberOfNotes=\"\"><Note  ContactUser=\"\"  NoteText=\"\" Createts=\"\" Createuserid=\"\"></Note></Notes> </Order>";
		return YFCDocument.getDocumentFor(templateString);
	}

	private YFCDocument commitOrderLine(YFCDocument inXml){
		LoggerUtil.debugLog("HandleVendorUpdatesApi:input to commitOrder ", logger, inXml);
		YFCDocument coDoc = getCopy(inXml, TelstraConstants.ORDER);
		coDoc.getDocumentElement().setAttribute(TelstraConstants.ACTION, "MODIFY");
		handleOrderLineCommitments(coDoc);
		LoggerUtil.debugLog("commitOrder:input to changeOrder api", logger,coDoc);
		YFCDocument coOut = invokeYantraApi(TelstraConstants.API_CHANGE_ORDER, coDoc);
		//Change order successful..create input for changeOrderStatus
		YFCDocument inDoc = YFCDocument.createDocument(TelstraConstants.ORDER_STATUS_CHANGE);
		YFCElement inElem = inDoc.getDocumentElement();
		YFCElement elem = coDoc.getDocumentElement();
		inElem.setAttribute(TelstraConstants.ORDER_HEADER_KEY, elem.getAttribute(TelstraConstants.ORDER_HEADER_KEY));
		inElem.setAttribute(TelstraConstants.TRANSACTION_ID, TelstraConstants.TRANSACTION_VENDOR_UPDATE);
		YFCElement orderLinesElem = elem.getChildElement(TelstraConstants.ORDER_LINES);
		YFCElement inputOrderLinesElem = inElem.createChild(TelstraConstants.ORDER_LINES);
		boolean callChangeOrderStatus = false;
		for(Iterator<YFCElement> itr = orderLinesElem.getChildren(TelstraConstants.ORDER_LINE);itr.hasNext();){
			YFCElement orderLineElem = itr.next();
			YFCElement inputOrderLineElem = inputOrderLinesElem.createChild(TelstraConstants.ORDER_LINE);
			inputOrderLineElem.setAttribute(TelstraConstants.ORDER_LINE_KEY, orderLineElem.getAttribute(TelstraConstants.ORDER_LINE_KEY));
			inputOrderLineElem.setAttribute("BaseDropStatus", "1260.10000");
			LineCommitmentsInfo lineCommitmentInfo = lineCommitmentsInfo.get(orderLineElem.getAttribute(TelstraConstants.ORDER_LINE_KEY));
			if(lineCommitmentInfo != null){
				double diffCommit = lineCommitmentInfo.getDifferentialCommittedQuantity();
				if(Double.compare(diffCommit, 0d) != 0 && commitmentsMap.get(orderLineElem.getAttribute("PrimeLineNo"))!=0 ){
				    double commitableQty = commitmentsMap.get(orderLineElem.getAttribute("PrimeLineNo"));
					callChangeOrderStatus = true;
					inputOrderLineElem.setAttribute("Quantity", commitableQty);
				}else{
					inputOrderLinesElem.removeChild(inputOrderLineElem);
				}
			}else{
				inputOrderLinesElem.removeChild(inputOrderLineElem);
			}
		}
		if(callChangeOrderStatus){
			LoggerUtil.debugLog("commitOrder:input to changeOrderStatus api", logger, inDoc);
			return invokeYantraApi(TelstraConstants.API_CHANGE_ORDER_STATUS, inDoc);
		}
		return coOut;
	}

	private YFCDocument commitEntireOrder(YFCDocument inXml){
		LoggerUtil.debugLog("HandleVendorUpdatesApi:input to commitEntireOrder ", logger, inXml);
		YFCDocument coDoc = getCopy(inXml, TelstraConstants.ORDER);
		coDoc.getDocumentElement().setAttribute(TelstraConstants.ACTION, "MODIFY");
		handleOrderLineCommitmentsForEntireOrder(coDoc);
		invokeYantraApi(TelstraConstants.API_CHANGE_ORDER, coDoc);
		//Change order successful..create input for changeOrderStatus
		YFCDocument inDoc = YFCDocument.createDocument(TelstraConstants.ORDER_STATUS_CHANGE);
		YFCElement inElem = inDoc.getDocumentElement();
		YFCElement elem = coDoc.getDocumentElement();
		inElem.setAttribute(TelstraConstants.ORDER_HEADER_KEY, elem.getAttribute(TelstraConstants.ORDER_HEADER_KEY));
		inElem.setAttribute(TelstraConstants.TRANSACTION_ID, TelstraConstants.TRANSACTION_VENDOR_UPDATE);
		YFCElement orderLinesElem = elem.getChildElement(TelstraConstants.ORDER_LINES);
		YFCElement inputOrderLinesElem = inElem.createChild(TelstraConstants.ORDER_LINES);
		for(Iterator<YFCElement> itr = orderLinesElem.getChildren(TelstraConstants.ORDER_LINE);itr.hasNext();){
			YFCElement orderLineElem = itr.next();
			YFCElement inputOrderLineElem = inputOrderLinesElem.createChild(TelstraConstants.ORDER_LINE);
			inputOrderLineElem.setAttribute(TelstraConstants.ORDER_LINE_KEY, orderLineElem.getAttribute(TelstraConstants.ORDER_LINE_KEY));
			inputOrderLineElem.setAttribute("BaseDropStatus", "1260.10000");
			inputOrderLineElem.setAttribute("Quantity", orderLineElem.getAttribute(TelstraConstants.ORDERED_QTY));            
		}
		LoggerUtil.debugLog("commiting entire order. Input to changeOrderStatus api", logger, inDoc);
		return invokeYantraApi(TelstraConstants.API_CHANGE_ORDER_STATUS, inDoc);
	}


	private void handleOrderLineCommitments(YFCDocument doc){
		YFCElement elem = doc.getDocumentElement();
		YFCElement orderLines = elem.getChildElement(TelstraConstants.ORDER_LINES);
		if(orderLines == null){
			return;
		}
		YFCDocument orderDetails = getOrderDetails(doc);
		for(Iterator<YFCElement> itr = orderLines.getChildren(TelstraConstants.ORDER_LINE);itr.hasNext();){
			YFCElement orderLine = itr.next();
			YFCElement extn = orderLine.getChildElement(TelstraConstants.EXTN);
			YFCElement commitmentList = null;
			if(extn != null){
				commitmentList = extn.getChildElement(TelstraConstants.ORDER_LINE_COMMITMENT_LIST);
			}
			if(commitmentList != null){
				LineCommitmentsInfo lineCommitmentInfo = getLineCommitmentsInfo(orderLine,orderDetails.getDocumentElement());
				long currentScheduleNo = lineCommitmentInfo.getScheduleNo();

				double orderedQuantity = lineCommitmentInfo.getOrderedQuantity();
				double totalCommittedQuantity = processCommitments(lineCommitmentInfo, commitmentList, currentScheduleNo);
				//double totalCommittedQuantity = lineCommitmentInfo.getTotalCommittedQuantity();
				if(totalCommittedQuantity < orderedQuantity){
					isCommitmentMismatch = true;
				}
				double diffCommitted = lineCommitmentInfo.getNewCommittedQuantity() + lineCommitmentInfo.getChangedCommittedQuantity();
				lineCommitmentInfo.setDifferentialCommittedQuantity(diffCommitted);
				orderLine.setAttribute(TelstraConstants.COMMITTED_QUANTITY, totalCommittedQuantity);
				lineCommitmentsInfo.put(orderLine.getAttribute(TelstraConstants.ORDER_LINE_KEY), lineCommitmentInfo);
			}else{
				isCommitmentMismatch = true;
			}
		}
	}

	private double processCommitments(LineCommitmentsInfo lineCommitmentInfo,YFCElement commitmentList, long currentScheduleNo){
		double totalCommittedQuantity = 0;
		YDate reqDeliveryDate = lineCommitmentInfo.getReqDeliveryDate();
		long startScheduleNo = currentScheduleNo;
		for(Iterator<YFCElement> citr = commitmentList.getChildren();citr.hasNext();){
			YFCElement commitment = citr.next();
			YDate commitmentDeliveryDate = commitment.getYDateAttribute(TelstraConstants.COMMIT_DELIVERY_DATE);
			if(commitment.hasAttribute(TelstraConstants.IS_ACTIVE) && TelstraConstants.YES.equals(commitment.getAttribute(TelstraConstants.IS_ACTIVE))) {
				if(commitmentDeliveryDate == null){
					LoggerUtil.verboseLog("Delivery date is not passed for processCommitment", logger, commitmentList);
					throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.VENDOR_COMMITMENT_DELIVERY_DATE_NOT_PRESENT_ERROR_CODE, new YFSException());
				}
				if(reqDeliveryDate != null && commitmentDeliveryDate.after(reqDeliveryDate)){
					isCommitmentMismatch = true;
				}
				double lineCommittedQuantity = commitment.getDoubleAttribute(TelstraConstants.COMMITTED_QUANTITY);
				totalCommittedQuantity = totalCommittedQuantity + lineCommittedQuantity;
				String scheduleNoStr = commitment.getAttribute(TelstraConstants.SCHEDULE_NO);
				if(YFCObject.isVoid(scheduleNoStr)){
					//If schedule no is passed its plain update scenariow	
					startScheduleNo = startScheduleNo + 1;
					commitment.setAttribute(TelstraConstants.SCHEDULE_NO, startScheduleNo);
				}
			}
		}
		return totalCommittedQuantity;
	}


	private void handleOrderLineCommitmentsForEntireOrder(YFCDocument doc){
		YFCElement elem = doc.getDocumentElement();
		YDate reqShipDateAtOrder = elem.getYDateAttribute(TelstraConstants.REQUIRED_SHIP_DATE);
		YDate reqDeliveryDateAtOrder = elem.getYDateAttribute(TelstraConstants.REQUIRED_DELIVERY_DATE);
		YFCElement orderLines = elem.getChildElement(TelstraConstants.ORDER_LINES);
		if(orderLines != null){
			elem.removeChild(orderLines);
		}
		orderLines = elem.createChild(TelstraConstants.ORDER_LINES);
		YFCDocument orderDetails = getOrderDetails(doc);
		YFCElement elemExistingOrderLines = orderDetails.getDocumentElement().getChildElement(TelstraConstants.ORDER_LINES);
		if(elemExistingOrderLines == null || elemExistingOrderLines.getChildren() == null){
			LoggerUtil.verboseLog("invalid input for commit entire order as no order lines found", logger, orderDetails);

			throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.VENDOR_COMMITMENT_INVALID_INPUT_ERROR_CODE, new YFSException());
		}
		for(Iterator<YFCElement> itr = elemExistingOrderLines.getChildren(TelstraConstants.ORDER_LINE);itr.hasNext();){
			YFCElement orderLine = itr.next();
			YFCElement extn = orderLine.getChildElement(TelstraConstants.EXTN,true);
			YFCElement commitmentList = extn.getChildElement(TelstraConstants.ORDER_LINE_COMMITMENT_LIST);
			if(commitmentList != null){
				for(Iterator<YFCElement> itr2 = commitmentList.getChildren().iterator();itr2.hasNext();){
					LoggerUtil.verboseLog("Cannot entirely commit an order which has commitments", logger, orderDetails);
					throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.VENDOR_COMMITMENT_EXISTING_COMMITMENT_ERROR_CODE, new YFSException());
				}
			}else{
				commitmentList = extn.createChild(TelstraConstants.ORDER_LINE_COMMITMENT_LIST);
			}
			YFCElement elemCommitment = commitmentList.createChild(TelstraConstants.ORDER_LINE_COMMITMENT);
			YDate reqShipDate = orderLine.getYDateAttribute(TelstraConstants.REQUIRED_SHIP_DATE);
			if(reqShipDate != null){
				elemCommitment.setAttribute(TelstraConstants.COMMIT_SHIP_DATE, reqShipDate);
			}else{
				elemCommitment.setAttribute(TelstraConstants.COMMIT_SHIP_DATE, reqShipDateAtOrder);
			}
			YDate reqDeliveryDate = orderLine.getYDateAttribute(TelstraConstants.REQUIRED_DELIVERY_DATE);
			if(reqDeliveryDate != null){
				elemCommitment.setAttribute(TelstraConstants.COMMIT_DELIVERY_DATE, reqDeliveryDate);
			}else{
				elemCommitment.setAttribute(TelstraConstants.COMMIT_DELIVERY_DATE, reqDeliveryDateAtOrder);
			}
			elemCommitment.setAttribute(TelstraConstants.SCHEDULE_NO, 1);
			elemCommitment.setAttribute(TelstraConstants.IS_ACTIVE, TelstraConstants.YES);
			elemCommitment.setAttribute(TelstraConstants.COMMITTED_QUANTITY, orderLine.getDoubleAttribute(TelstraConstants.ORDERED_QTY));
			elemCommitment.setAttribute(TelstraConstants.ORDER_LINE_KEY, orderLine.getAttribute(TelstraConstants.ORDER_LINE_KEY));
			orderLine.setAttribute(TelstraConstants.COMMITTED_QUANTITY, orderLine.getDoubleAttribute(TelstraConstants.ORDERED_QTY));
			orderLines.importNode(orderLine);

		}
	}

	private YFCDocument getOrderDetails(YFCDocument doc){
		YFCDocument newTemplate = createTemplateForGetOrderDetails();
		YFCDocument inputDoc = YFCDocument.createDocument(TelstraConstants.ORDER);
		YFCElement input = inputDoc.getDocumentElement();
		input.setAttribute(TelstraConstants.DOCUMENT_TYPE, TelstraConstants.DOCUMENT_TYPE_PURCHACE_ORDER);
		input.setAttribute(TelstraConstants.ENTERPRISE_CODE, doc.getDocumentElement().getAttribute(TelstraConstants.ENTERPRISE_CODE));
		input.setAttribute(TelstraConstants.ORDER_HEADER_KEY, doc.getDocumentElement().getAttribute(TelstraConstants.ORDER_HEADER_KEY));
		input.setAttribute(TelstraConstants.ORDER_NO, doc.getDocumentElement().getAttribute(TelstraConstants.ORDER_NO));
		LoggerUtil.verboseLog("Getting order details", logger, inputDoc);
		return invokeYantraApi(TelstraConstants.API_GET_ORDER_DETAILS, inputDoc, newTemplate);
	}

	private YFCDocument createTemplateForGetOrderDetails(){
		String templateString = "<Order OrderHeaderKey=\"\" OrderNo=\"\" EnterpriseCode=\"\" EntryType=\"\" SellerOrganizationCode=\"\" DepartmentCode=\"\" Status=\"\" OrderName=\"\" OrderType=\"\" ReqDeliveryDate=\"\" ReqShipDate=\"\" ><Extn HasVendorAccepted=\"\" SellerName=\"\" /> <OrderLines>  <OrderLine OrderLineKey=\"\" PrimeLineNo=\"\" SubLineNo=\"\" ReqDeliveryDate=\"\" CommittedQuantity=\"\" OrderedQty=\"\" Status=\"\"><Extn><OrderLineCommitmentList><OrderLineCommitment OrderLineCommitKey=\"\" ScheduleNo=\"\" OrderLineKey=\"\" CommitShipDate=\"\" CommitDeliveryDate=\"\" CommittedQuantity=\"\" IsActive=\"\" /> </OrderLineCommitmentList></Extn></OrderLine> </OrderLines> </Order>";
		return YFCDocument.getDocumentFor(templateString);
	}

	private LineCommitmentsInfo getLineCommitmentsInfo(YFCElement elemInputOrderLine, YFCElement orderDetails){
		long maxScheduleNo = 0;
		double existingCommittedQuantity = 0;
		LineCommitmentsInfo lineCommitmentInfo = new LineCommitmentsInfo();
		YFCElement existingOrderLine = getExistingOrderLine(elemInputOrderLine.getAttribute(TelstraConstants.ORDER_LINE_KEY), orderDetails);
		double orderedQty = existingOrderLine.getDoubleAttribute(TelstraConstants.ORDERED_QTY);
		YDate reqDeliveryDate = existingOrderLine.getYDateAttribute(TelstraConstants.REQUIRED_DELIVERY_DATE);
		if(reqDeliveryDate == null){
			reqDeliveryDate = orderDetails.getYDateAttribute(TelstraConstants.REQUIRED_DELIVERY_DATE);
		}
		lineCommitmentInfo.setOrderedQuantity(orderedQty);
		lineCommitmentInfo.setReqDeliveryDate(reqDeliveryDate);
		YFCElement extn = existingOrderLine.getChildElement(TelstraConstants.EXTN);
		if(extn == null){
			return lineCommitmentInfo;
		}
		YFCElement lineCommitmentList = extn.getChildElement(TelstraConstants.ORDER_LINE_COMMITMENT_LIST);
		if(lineCommitmentList == null){
			return lineCommitmentInfo;
		}
		Map<String, Double> existingQuantityMap = new HashMap<>();
		double newQuantity = 0;
		double changedQuantity = 0;
		double totalQuantity = 0;
		for(Iterator<YFCElement> itr = lineCommitmentList.getChildren(TelstraConstants.ORDER_LINE_COMMITMENT);itr.hasNext();){
			YFCElement lineCommitmentElem = itr.next();
			if(TelstraConstants.YES.equals(lineCommitmentElem.getAttribute(TelstraConstants.IS_ACTIVE))) {
				double committedQty = lineCommitmentElem.getDoubleAttribute(TelstraConstants.COMMITTED_QUANTITY);
				String commitKey=makeCommitmentKey(lineCommitmentElem);
				//existingQuantityMap.put(lineCommitmentElem.getAttribute("OrderLineCommitKey"), committedQty);
				existingQuantityMap.put(commitKey,committedQty);
				existingCommittedQuantity = existingCommittedQuantity + committedQty;
				long scheduleNo = lineCommitmentElem.getLongAttribute(TelstraConstants.SCHEDULE_NO);
				if(scheduleNo > maxScheduleNo){
					maxScheduleNo = scheduleNo;
				}
			}
		}

		lineCommitmentInfo.setScheduleNo(maxScheduleNo);
		//Now iterate over input order line commitment
		YFCElement elemInputOrderLineExtn = elemInputOrderLine.getChildElement(TelstraConstants.EXTN);
		if(elemInputOrderLineExtn != null){
			YFCElement elemInputLineCommitmentList = elemInputOrderLineExtn.getChildElement(TelstraConstants.ORDER_LINE_COMMITMENT_LIST);
			if(elemInputLineCommitmentList != null){
				for(Iterator<YFCElement> itr = elemInputLineCommitmentList.getChildren(TelstraConstants.ORDER_LINE_COMMITMENT);itr.hasNext();){
					YFCElement lineCommitmentElem = itr.next();
					if(TelstraConstants.YES.equals(lineCommitmentElem.getAttribute(TelstraConstants.IS_ACTIVE))) {
						double committedQty = lineCommitmentElem.getDoubleAttribute(TelstraConstants.COMMITTED_QUANTITY);
						//String lineCommitmentKey = lineCommitmentElem.getAttribute("OrderLineCommitKey");
						String lineCommitmentKey = makeCommitmentKey(lineCommitmentElem);
						if(existingQuantityMap.containsKey(lineCommitmentKey)) {
							double existingQuantity = existingQuantityMap.get(lineCommitmentKey);
							changedQuantity = changedQuantity + (committedQty - existingQuantity);
							existingQuantityMap.remove(lineCommitmentKey);
						} else {
							newQuantity = newQuantity + committedQty;
						}
						totalQuantity = totalQuantity + committedQty;
						
					}
				}
				lineCommitmentInfo.setChangedCommittedQuantity(changedQuantity);
				lineCommitmentInfo.setNewCommittedQuantity(newQuantity);
				if(!existingQuantityMap.isEmpty()){
					for(Entry<String, Double> entry : existingQuantityMap.entrySet()){
						totalQuantity = totalQuantity + entry.getValue();
					}
				}
				lineCommitmentInfo.setTotalCommittedQuantity(totalQuantity);
			}
		}

		return lineCommitmentInfo;
	}
	
	private String makeCommitmentKey(YFCElement commitmentElem) {
		StringBuffer commitmentKey=new StringBuffer();
		if(!YFCObject.isVoid(commitmentElem.getAttribute(TelstraConstants.ORDER_LINE_KEY))) {
			commitmentKey.append(commitmentElem.getAttribute(TelstraConstants.ORDER_LINE_KEY)).append(":");
		}
		if(!YFCObject.isVoid(commitmentElem.getAttribute(TelstraConstants.SCHEDULE_NO))) {
			commitmentKey.append(commitmentElem.getAttribute(TelstraConstants.SCHEDULE_NO));
		}		
		
		return commitmentKey.toString();
	}

	private YFCElement getExistingOrderLine(String orderLineKey, YFCElement orderDetails){
		YFCElement orderLines = orderDetails.getChildElement(TelstraConstants.ORDER_LINES);
		for(Iterator<YFCElement> itr = orderLines.getChildren(TelstraConstants.ORDER_LINE);itr.hasNext();){
			YFCElement orderLine = itr.next();
			String key = orderLine.getAttribute(TelstraConstants.ORDER_LINE_KEY);
			if(key.equals(orderLineKey)){
				return orderLine;
			}
		}
		LoggerUtil.errorLog("No existing order line found, throwing exception"+orderLineKey, logger, orderDetails);
		LoggerUtil.verboseLog("invalid input for commit entire order as no order lines found", logger, orderDetails);
		throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.VENDOR_COMMITMENT_INVALID_INPUT_ERROR_CODE, new YFSException());
	}


	private static YFCDocument getCopy(YFCDocument inXml, String rootNode){
		YFCDocument copyDoc = YFCDocument.createDocument(rootNode);
		copyDoc.getDocumentElement().setAttributes(inXml.getDocumentElement().getAttributes());
		if(inXml.getDocumentElement().getChildren() != null){
			for(Iterator<YFCElement> itr = inXml.getDocumentElement().getChildren().iterator();itr.hasNext();){
				copyDoc.getDocumentElement().importNode(itr.next());
			}
		}
		return copyDoc;
	}


	private class LineCommitmentsInfo{
		long scheduleNo;
		double differentialCommittedQuantity;
		double newCommittedQuantity;
		double changedCommittedQuantity;
		double orderedQuantity;
		YDate reqDeliveryDate;
		double totalCommittedQuantity;
		public long getScheduleNo() {
			return scheduleNo;
		}
		public void setScheduleNo(long scheduleNo) {
			this.scheduleNo = scheduleNo;
		}
		public double getDifferentialCommittedQuantity() {
			return differentialCommittedQuantity;
		}
		public void setDifferentialCommittedQuantity(
				double differentialCommittedQuantity) {
			this.differentialCommittedQuantity = differentialCommittedQuantity;
		}
		public double getOrderedQuantity() {
			return orderedQuantity;
		}
		public void setOrderedQuantity(double orderedQuantity) {
			this.orderedQuantity = orderedQuantity;
		}
		public YDate getReqDeliveryDate() {
			return reqDeliveryDate;
		}
		public void setReqDeliveryDate(YDate reqDeliveryDate) {
			this.reqDeliveryDate = reqDeliveryDate;
		}
		public double getNewCommittedQuantity() {
			return newCommittedQuantity;
		}
		public void setNewCommittedQuantity(double newCommittedQuantity) {
			this.newCommittedQuantity = newCommittedQuantity;
		}
		public double getChangedCommittedQuantity() {
			return changedCommittedQuantity;
		}
		public void setChangedCommittedQuantity(double changedCommittedQuantity) {
			this.changedCommittedQuantity = changedCommittedQuantity;
		}
		public double getTotalCommittedQuantity() {
			return totalCommittedQuantity;
		}
		public void setTotalCommittedQuantity(double totalCommittedQuantity) {
			this.totalCommittedQuantity = totalCommittedQuantity;
		}


	}

	private ActionType getActionType(YFCDocument doc){
		YFCElement elem = doc.getDocumentElement();
		String action = elem.getAttribute(TelstraConstants.ACTION);
		if(YFCObject.isVoid(action)){
			LoggerUtil.verboseLog("No action found in document", logger, doc);
			throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.VENDOR_COMMITMENT_INVALID_ACTION_ERROR_CODE, new YFSException());
		}
		try{
			ActionType actionType = ActionType.valueOf(action);
			return actionType;
		}catch(Exception e){
			LoggerUtil.verboseLog("Invalid action found in document", logger, doc);
			throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.VENDOR_COMMITMENT_INVALID_ACTION_ERROR_CODE, new YFSException());
		}

	}


	private YFCDocument getOpenAlerts(YFCDocument inXml){
		YFCDocument docGetOpenAlerts = YFCDocument.getDocumentFor("<Inbox ActiveFlag='Y' OrderHeaderKey='"+inXml.getDocumentElement().getAttribute(TelstraConstants.ORDER_HEADER_KEY)+"' ExceptionType='GPS_PO_COMMIT_MISMATCH_ALERT' />");
		return invokeYantraApi("getExceptionList", docGetOpenAlerts); 
	}

	private void resolveAlerts(YFCDocument docExceptionList){
		YFCElement elemExceptionList = docExceptionList.getDocumentElement();
		for(Iterator<YFCElement> itr = elemExceptionList.getChildren().iterator();itr.hasNext();){
			YFCElement elemException = itr.next();
			resolveAlert(elemException);
		}
	}

	private void resolveAlert(YFCElement elemException){
		YFCDocument docResolveException = YFCDocument.getDocumentFor("<ResolutionDetails AutoResolvedFlag='Y' />");
		docResolveException.getDocumentElement().importNode(elemException);
		invokeYantraApi("resolveException", docResolveException);
	}

	private boolean hasOpenAlert(YFCDocument docExceptionList){
		if(docExceptionList == null || docExceptionList.getDocumentElement() == null){
			return false;
		}
		YFCElement elemExceptionList = docExceptionList.getDocumentElement();
		if(elemExceptionList.getChildren() == null || elemExceptionList.getFirstChild() == null){
			return false;
		}
		return true;
	}

	private void raiseAlert(YFCDocument inXml){
		invokeYantraService(TelstraConstants.SERVICE_VENDOR_COMMITMENT_PROCESS, inXml);
	}
	
	   /**
     * 
     * @param sOrderName
     * @return
     */
    private YFCDocument callGetOrderList(String sOrderName) {

        YFCDocument yfcDocGetOrderListIp = YFCDocument.getDocumentFor("<Order OrderName='"+sOrderName+"'/>");
        LoggerUtil.verboseLog(this.getClass().getName() + " :: callGetOrderList ::  yfcDocGetOrderListIp", logger, yfcDocGetOrderListIp);

        YFCDocument yfcDocGetOrderListTemp = YFCDocument.getDocumentFor(
            "<OrderList>"
                + "<Order OrderHeaderKey='' OrderNo='' DocumentType=''>"
                  + "<OrderLines>"
                    + "<OrderLine OrderLineKey='' PrimeLineNo='' ReqDeliveryDate='' >"
                      + "<OrderStatuses>"
                        + "<OrderStatus  StatusQty='' Status='' />"
                      + "</OrderStatuses>"
                      + "<Extn>"
                        + "<OrderLineCommitmentList>"
                          + "<OrderLineCommitment ScheduleNo='' CommittedQuantity='' CommitRequestDate='' CommitShipDate='' CommitDeliveryDate='' IsActive='' />"
                        + "</OrderLineCommitmentList>"
                      + "</Extn>"
                      + "<OrderDates>"
                        + "<OrderDate DateTypeId='' RequestedDate='' />"
                      + "</OrderDates>"
                    + "</OrderLine>"
                  + "</OrderLines>"
                + "</Order>"
                + "</OrderList>"); 

        YFCDocument yfcDocGetOrderListOp = invokeYantraApi(TelstraConstants.API_GET_ORDER_LIST, yfcDocGetOrderListIp, yfcDocGetOrderListTemp); 
        LoggerUtil.verboseLog(this.getClass().getName() + " :: callGetOrderList ::  yfcDocGetOrderListOp", logger, yfcDocGetOrderListOp);

        return yfcDocGetOrderListOp;
    }
	//	@Override
	//	public Map<String, List<String>> getDataAccessPolicyBypassMap() {
	//		Map<String, List<String>> bypassMap = new HashMap<>();
	//		//Change order status
	//		String changeOrderStatus = "changeOrderStatus";
	//		List<String> changeOrderStatusEntities = new ArrayList<>();
	//		changeOrderStatusEntities.add("YFS_ORDER_HEADER");
	//		bypassMap.put(changeOrderStatus, changeOrderStatusEntities);
	//		String getExceptionList = "getExceptionList";
	//		List<String> getExceptionListEntities = new ArrayList<>();
	//		getExceptionListEntities.add("YFS_INBOX");
	//		bypassMap.put(getExceptionList, getExceptionListEntities);
	//		return bypassMap;
	//	}
	//	
	//	
	//	@Override
	//	public List<String> getBypassEntities() {
	//		List<String> bypassEntities = new ArrayList<>();
	//		bypassEntities.add("YFS_INBOX");
	//		return bypassEntities;
	//	}
}

package com.gps.hubble.vendor;

import java.util.List;
import java.util.Map;

import org.w3c.dom.Document;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

public abstract class BaseVendorApi extends AbstractCustomApi{
	
	@Override
	public Document invoke(YFSEnvironment env, Document inXml)
			throws YFSException {
		env.setTxnObject("IsCustomDataAccessPolicyRequired", new Boolean(true));
		env.setTxnObject("DSVendorOrgCode", getVendorOrganizationCode(YFCDocument.getDocumentFor(inXml)));
		env.setTxnObject("DSBypassMap", getDataAccessPolicyBypassMap());
		env.setTxnObject("DSBypassEntities", getBypassEntities());
		return super.invoke(env, inXml);
	}
	
	/*
	 * Override this method if vendor organization is not present as SellerOrganizationCode of root element of input document
	 */
	protected String getVendorOrganizationCode(YFCDocument inXml){
		if(inXml == null || inXml.getDocumentElement() == null){
			return null;
		}
		return inXml.getDocumentElement().getAttribute(TelstraConstants.SELLER_ORGANIZATION_CODE);
	}
	
	/*
	 * All vendor apis should implement this method and provide an map of apis and the entities where access policy needs to 
	 */
	public abstract Map<String, List<String>> getDataAccessPolicyBypassMap();
	
	/*
	 * All entities that needs to be bypassed if not invoked as part of any api 
	 */
	public abstract List<String> getBypassEntities();
}

package com.gps.hubble.agent.purge;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.ycp.agent.server.YCPAbstractAgent;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

/**
 * 
 * @author Vinay
 *
 */

public class GpsExportRequestPurgeAgent extends YCPAbstractAgent {

	private static YFCLogCategory logger = YFCLogCategory.instance(GpsExportRequestPurgeAgent.class);

	/**
	 * 
	 */
	@Override
	public void executeJob(YFSEnvironment env, Document inDoc) throws Exception {

		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "executeJob", inDoc);
		LoggerUtil.verboseLog("GpsExportRequestPurgeAgent::executeJob::inDoc\n", logger,
				YFCDocument.getDocumentFor(inDoc));
		// invoke delete api
		YIFClientFactory.getInstance().getApi().executeFlow(env, TelstraConstants.GPS_DELETE_EXPORT_REQUEST, inDoc);
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "executeJob", inDoc);

	}

	/**
	 * 
	 */
	@Override
	public List<Document> getJobs(YFSEnvironment env, Document inDoc, Document docLastMsg) throws Exception {

		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "getJobs", inDoc);
		LoggerUtil.verboseLog("GpsExportRequestPurgeAgent::getJobs::InputDocument\n", logger,
				YFCDocument.getDocumentFor(inDoc));

		Element eleIpRoot = inDoc.getDocumentElement();
		String sRetentionDays = eleIpRoot.getAttribute(TelstraConstants.RETENTION_DAYS);

		Document docGetExportReqListOp = getExportRequestListOp(env, sRetentionDays);
		List<Document> jobList = prepareReturnList(docGetExportReqListOp);

		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "getJobs", inDoc);
		return jobList;

	}

	

	/**
	 * 
	 * @param docGetFileUploadListOp
	 * @return
	 */
	private List<Document> prepareReturnList(Document docGetFileUploadListOp) {
		Element eleRoot = docGetFileUploadListOp.getDocumentElement();
		NodeList nlChildNode = eleRoot.getChildNodes();

		List<Document> jobList = new ArrayList<>();
		for (int i = 0; i < nlChildNode.getLength(); i++) {

			Element eleChild = (Element) nlChildNode.item(i);
			Document docExecuteJobsIp = (YFCDocument.createDocument()).getDocument();
			Element eleImportedElement = (Element) docExecuteJobsIp.importNode(eleChild, true);
			docExecuteJobsIp.appendChild(eleImportedElement);

			jobList.add(docExecuteJobsIp);
		}
		return jobList;
	}

	/**
	 * 
	 * @param env
	 * @param sRetentionDays
	 * @return
	 * @throws YFSException
	 * @throws RemoteException
	 * @throws YIFClientCreationException
	 */
	private Document getExportRequestListOp(YFSEnvironment env, String sRetentionDays)
			throws YFSException, RemoteException, YIFClientCreationException {

		String sToModifyTS = getToModifyTS(Integer.parseInt("-" + sRetentionDays));

		Document docGetExportRequestIp = (YFCDocument.createDocument(TelstraConstants.GPS_EXPORT_REQUEST)).getDocument();
		docGetExportRequestIp.getDocumentElement().setAttribute(TelstraConstants.TO_MODIFY_TS, sToModifyTS);
		docGetExportRequestIp.getDocumentElement().setAttribute(TelstraConstants.MODIFY_TS_QRY_TYPE,
				TelstraConstants.DATE_RANGE);
		docGetExportRequestIp.getDocumentElement().setAttribute(TelstraConstants.PROCESSED_FLAG, TelstraConstants.YES);

		LoggerUtil.verboseLog("GpsExportRequestPurgeAgent::getJobs::docGetExportRequestIp\n", logger,
				YFCDocument.getDocumentFor(docGetExportRequestIp));

		Document docGetExportReqListOp = YIFClientFactory.getInstance().getApi().executeFlow(env,
				TelstraConstants.GPS_GET_EXPORT_REQUEST_LIST, docGetExportRequestIp);
		LoggerUtil.verboseLog("GpsExportRequestPurgeAgent::getJobs::getExportRequestListOp\n", logger,
				YFCDocument.getDocumentFor(docGetExportReqListOp));

		return docGetExportReqListOp;
	}

	/**
	 * 
	 * @param iRetentionDays
	 * @return
	 */
	private String getToModifyTS(int iRetentionDays) {

		Date currentDate = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat(TelstraConstants.DATE_TIME_FORMAT_2);
		Calendar c = Calendar.getInstance();
		c.setTime(currentDate);
		c.add(Calendar.DATE, iRetentionDays); // number of days to add
		String sToModifyTS = sdf.format(c.getTime());
		LoggerUtil.verboseLog("GpsExportRequestPurgeAgent::getToCreateTS::sToCreateTS\n", logger, sToModifyTS);
		return sToModifyTS;
	}

}


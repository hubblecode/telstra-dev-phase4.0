package com.gps.hubble.costcenter.api;


//Java imports - NONE

//Project imports - NONE

//Sterling imports
import com.sterlingcommerce.baseutil.SCUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSException;

//Misc imports 
import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.ExceptionUtil;
import com.gps.hubble.constants.TelstraErrorCodeConstants;

/**
 * This class manages the cost center upload for new and existing cost data.
 * @version 1.0 (Jun 2016)
 * @author Sambeet Mohanty
 *
 * Functionality
 * @see method invoke()
 *
 * Known Bugs
 * Revision History
 */

public class ManageCostCenter extends AbstractCustomApi{

	private static YFCLogCategory logger =
			YFCLogCategory.instance(ManageCostCenter.class);
	
	@Override
	public YFCDocument invoke(YFCDocument inXml) {
		/**
		 * Method name: invoke
		 * arguments: YFCDocument
		 * This Method validates if any cost center data to be process is already present or not.
		 * If already present, it updates the existing data, else creates new entry. 
		 */	

		String sOrgCode=inXml.getDocumentElement().getAttribute("OrgCode");
		logger.verbose("sOrgCode: "+ sOrgCode );

		if(SCUtil.isVoid(sOrgCode))
		{
			//throw new YFSException("Cost center code is null");
			throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.COST_CENTER_NULL_ERROR_CODE, new YFSException());
		}

		YFCDocument docGetCostCenterReq= YFCDocument.createDocument("CostCenter");
		docGetCostCenterReq.getDocumentElement().setAttribute("OrgCode", sOrgCode);

		logger.verbose("docGetCostCenterReq"+ docGetCostCenterReq);
		YFCDocument	getCostCenterRespDoc=invokeGetCostCenterList(docGetCostCenterReq);		

		YFCNodeList<YFCElement> listCostCenterResp= getCostCenterRespDoc.getElementsByTagName("CostCenter");
		if(listCostCenterResp.getLength()>0)
		{
			String sCostCenterKey=listCostCenterResp.item(0).getAttribute("CostCenterKey");
			inXml.getDocumentElement().setAttribute("CostCenterKey", sCostCenterKey);

			return invokeChangeCostCenter(inXml);

		}
		else
		{
			return invokeCreateCostCenter(inXml);
		}

	}

	/**
	 * Method name: invokeChangeCostCenter
	 * @param inXml
	 * @return YFCDocument
	 * This method invokes ChangeCostCenter api to modify any existing record of cost center.
	 */
	public YFCDocument invokeChangeCostCenter(YFCDocument inXml){
		String sChangeCostCenterService=getProperty("ChangeCostCenterService", true);
		logger.verbose("Invoking sChangeCostCenterService");
		return invokeYantraService(sChangeCostCenterService, inXml);

	}

	/**
	 * Method name: invokeCreateCostCenter
	 * @param inXml
	 * @return YFCDocument
	 * This method invokes createCostCenter api to modify any existing record of cost center.
	 */

	public YFCDocument invokeCreateCostCenter(YFCDocument inXml)
	{
		String sCreateCostCenterService=getProperty("CreateCostCenterService", true);
		logger.verbose("Invoking sCreateCostCenterService");
		return invokeYantraService(sCreateCostCenterService, inXml);
	}

	/**
	 * Method name: invokeGetCostCenterList
	 * @param inXml
	 * @return docGetCostCenterResp
	 * This method invokes getCostCenterList api to modify any existing record of cost center.
	 */ 
	public YFCDocument invokeGetCostCenterList(YFCDocument inXml){

		String sGetCostCenterListService=getProperty("GetCostCenterListService", true);
		YFCDocument docGetCostCenterResp= invokeYantraService(sGetCostCenterListService, inXml);
		logger.verbose("getCostCenterResponse"+ docGetCostCenterResp);
		return docGetCostCenterResp;
	}

}

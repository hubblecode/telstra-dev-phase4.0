/***********************************************************************************************
 * File Name        : GpsLdapProcessor.java
 *
 * Description      : Custom API for managing teams for the LDAP Users
 * 
 * Modification Log :
 * ---------------------------------------------------------------------------------------------
 * Ver #    Date            Author                              Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0      16/02/2017      Keerthi Yadav                     Initial Version
 * ---------------------------------------------------------------------------------------------
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
 **********************************************************************************************/
package com.gps.hubble.ldap.api;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;

/**Sample Input xml:
 * <Employee FirstName=" EmployeeID='' AccountName='' EMailID='' ManagerAccountName='' PreviousManagerAccountName=''/>
 */
public class GpsLdapProcessor extends AbstractCustomApi {

	private static YFCLogCategory logger = YFCLogCategory.instance(GpsLdapProcessor.class);

	/**
	 *  Base Method:  The LDAP data would be stored in the GPS_LDAP custom table which contains EmployeeId, ManagerAccountName
	 * FirstName, PreviousManagerAccountName, EmailId and with CTFlag defaulted as Y.
	 * 
	 * @param yfcInDoc
	 * @return yfcInDoc
	 */

	@Override
	public YFCDocument invoke(YFCDocument yfcInDoc) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(),"invoke", yfcInDoc);

		/*This class is invoked from GpsLdapLoad service after loading the data into the custom table */

		YFCElement yfcEleEmployee =yfcInDoc.getDocumentElement();

		String strEmployeeID = yfcEleEmployee.getAttribute(TelstraConstants.EMPLOYEE_ID, "");
		String strEmployeeFirstName = yfcEleEmployee.getAttribute(TelstraConstants.FIRST_NAME, "");
		String strManagerAccountName = yfcEleEmployee.getAttribute(TelstraConstants.MANAGER_ACCOUNT_NAME, "");
		String strAccountName = yfcEleEmployee.getAttribute("AccountName", "");
		String strPrevManagerAccountName = yfcEleEmployee.getAttribute(TelstraConstants.PREVIOUS_MANAGER_ACCOUNT_NAME, "");
		String strEmailID = yfcEleEmployee.getAttribute(TelstraConstants.EMAIL_ID, "");
		String strDacCreated = yfcEleEmployee.getAttribute("DacCreated", "");



		/*The assumption is that the EmployeeID when prefixed with 'V' is the CustomerID or the CT Node
		 *  and to check if the DAC exists or not, call getShipNode List Api with this DAC.
		 */
		String strShipNodeKey = 'V'+strEmployeeID;
		boolean isCommunicationTech = isCommunicationTechnician(strShipNodeKey);

		if(isCommunicationTech){

			/*
			 * create the CT user
			 * logic to create or update the CT manager team
			 * validate of the CT manager record exists in the GPS_LDAP table.
			 * If yes, create the CT manager user with the team and the ManagerAccountName_TEAM
			 * If not, don't do anything. The user will be created for the CT Manager, 
			 * when the CT Manager record is inserted in the GPS_LDAP table. 
			 */


			boolean bIsUser = checkUserExists(strEmailID);

			/*Create user if user does not exists */
			if(!bIsUser){

				String strTeam = getProperty("DefaultCTTeam", true);// Default CT Team
				String strOrganizationKey="V"+strEmployeeID;

				/* Create CT User*/
				String sUserGroups = getProperty("GroupCTUser");


				createUser(strEmployeeFirstName, strEmailID, strTeam, strOrganizationKey, sUserGroups);
			}

			//Manger Teams are created under TELSTRA
			String strOrganizationCode="TELSTRA";

			//Create Manager Teams if not present and subscribe the CT Node to those teams.
			createAndSubcribeCTToTeam(strManagerAccountName,strShipNodeKey,strOrganizationCode,strEmailID);

			//Remove CT Nodes from the old manager Team if there is any change in Manger for the CT
			removeCTFromPreviousManagerTeam(strPrevManagerAccountName,strOrganizationCode,strShipNodeKey);

			//Update flag from N to Y
			if(!YFCObject.isVoid(strDacCreated)){
				updateCTFlag(yfcEleEmployee,TelstraConstants.YES);
			}

		}else{
			//update the CT Flag from Y to N if no DAC associated with CT
			updateCTFlag(yfcEleEmployee,TelstraConstants.NO);

			/* 
			 * Validate if the employee record is a CT's manager
			 * If yes, create the user for this employee record with the team and the EmployeeID_TEAM
			 */

			//new
			boolean isCTManager = validateIfCTManager(strAccountName);
			if(isCTManager){
				boolean bIsUser = checkUserExists(strEmailID);

				//If User not exist for the manger create the User
				if(!bIsUser){
					String strTeam=strAccountName+"_TEAM";
					String strOrganizationKey = getProperty("MangerOrg", true);// Organisation Code for Creating Manager User
					String sUserGroups = getProperty("GroupCTLead");

					createUser(strEmployeeFirstName, strEmailID, strTeam, strOrganizationKey, sUserGroups);
				}
			}
		}

		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", yfcInDoc);

		return yfcInDoc;
	}

	/**
	 * The method will create manager Team if not created and subscribe the CT Node to that team
	 * @param strManagerAccountName
	 * @param strShipNodeKey
	 * @param strOrganizationCode
	 */
	private void createAndSubcribeCTToTeam(String strManagerAccountName,
			String strShipNodeKey,String strOrganizationCode,String strEmailID) {

		//call getTeamList
		YFCDocument outputManagerTeamList = getTeamList(strManagerAccountName);
		YFCElement eleManagerTeam = outputManagerTeamList.getDocumentElement().getChildElement(TelstraConstants.TEAM);
		String strCTManagerTeam=strManagerAccountName+"_TEAM";

		//Validate if the Team is present for the manager
		if(!YFCObject.isVoid(strManagerAccountName)){
			if(YFCObject.isNull(eleManagerTeam)) {
				// If CT Manager team is not created, create the CT Manager team and subscribe the CT Node to that team
				createTeam(strManagerAccountName);
				subscribeCTToTeam(strCTManagerTeam,strOrganizationCode,strShipNodeKey);
			}else{
				//check if CT Node is already subscribed
				boolean bIsEmployeeSubscribed = checkIsEmployeeSubscribed(strCTManagerTeam,strOrganizationCode,strShipNodeKey);

				//If not subscribed, subscribe the CT Node to the team
				if(!bIsEmployeeSubscribed){
					subscribeCTToTeam(strCTManagerTeam,strOrganizationCode,strShipNodeKey);
				}
			}	

			//Create manager User if already uploaded in custom table
			YFCDocument inXmlEmployeeList = YFCDocument.getDocumentFor("<Employee AccountName='"+ strManagerAccountName+"' />");
			YFCDocument outXmlEmployeeList = invokeYantraService("GpsGetEmployeeList", inXmlEmployeeList);

			YFCElement eleoutEmployeeList = outXmlEmployeeList.getDocumentElement();
			YFCElement eleoutEmployee = eleoutEmployeeList.getElementsByTagName(TelstraConstants.EMPLOYEE).item(0);
			if(!YFCObject.isNull(eleoutEmployee)){
				String strManagerEmailID = eleoutEmployee.getAttribute(TelstraConstants.EMAIL_ID,"");
				if(!YFCObject.isNull(strManagerEmailID)){
					boolean bUserExists = checkUserExists(strManagerEmailID);
					if(!bUserExists){
						createMangerUserInput(strManagerAccountName,eleoutEmployee);
					}
				}
			}
		}
	}
	/**
	 * This Method will create manger if inserted in the ldap custom table and if they are already not created in Sterling
	 * @param strManagerAccountName
	 */
	private void createMangerUserInput(String strManagerAccountName, YFCElement eleoutEmployee) {
		String strTeam=strManagerAccountName+"_TEAM";
		String strOrganizationKey = getProperty("MangerOrg", true);// Organisation Code for Creating Manager User


		logger.verbose("Creating Manager User fetched from the custom table while CT User is uploaded");

		String strEmployeeFirstName = eleoutEmployee.getAttribute(TelstraConstants.FIRST_NAME,"");
		String strEmailID = eleoutEmployee.getAttribute(TelstraConstants.EMAIL_ID,"");
		String sUserGroups = getProperty("GroupCTLead");

		createUser(strEmployeeFirstName, strEmailID, strTeam, strOrganizationKey, sUserGroups);		
	}


	/**
	 * The method will check Employee is still subscribed to the previous manager and if true will
	 * Unsubscribe the CT org from the previous manager
	 * @param strPrevManagerAccountName
	 * @param strOrganizationCode
	 * @param strShipNodeKey
	 */
	private void removeCTFromPreviousManagerTeam(String strPrevManagerAccountName, String strOrganizationCode,
			String strShipNodeKey) {
		/*
		 * Validate if the CT org is in the team or not
		 * If not, do nothing
		 * Else, remove the CT org from the Previous CT Manager's team
		 */

		if(!YFCObject.isNull(strPrevManagerAccountName)) {

			//get the team of the Previous manager  ??
			YFCDocument outputTeamList = getTeamList(strPrevManagerAccountName);
			YFCElement eleTeam = outputTeamList.getDocumentElement().getChildElement(TelstraConstants.TEAM);
			if(!YFCObject.isNull(eleTeam)) {								
				String sPreviousManagerTeam=eleTeam.getAttribute("TeamId","");

				//The method checkIsEmployeeSubscribed will check if the Employee is still subscribed to the previous manager

				boolean bIsEmployeeSubscribed = checkIsEmployeeSubscribed(sPreviousManagerTeam,strOrganizationCode,strShipNodeKey);
				if(bIsEmployeeSubscribed){
					// Unsubscribe the CT org from the previous manager
					unSubscribeCTToTeam(sPreviousManagerTeam,strOrganizationCode,strShipNodeKey);
				}
			}
		}

	}

	/**
	 * The Method will unsubcribe the communication technician(CT) from the previous manager.
	 * @param sTeamId
	 * @param sOrganizationCode
	 * @param sShipNodeKey
	 */
	private void unSubscribeCTToTeam(String sTeamId, String sOrganizationCode,String sShipNodeKey) {
		YFCDocument docInputmanageTeam  = YFCDocument.getDocumentFor("<Team TeamId='"+sTeamId +"' OrganizationCode='"+sOrganizationCode +"'><TeamNodesList><TeamNodes Operation='Delete' ShipnodeKey='"+sShipNodeKey +"'/></TeamNodesList></Team>");
		logger.verbose("Unsubscribe the CT from the previous manager : : manageTeam Input : : "+docInputmanageTeam.toString());
		invokeYantraApi(TelstraConstants.MANAGE_TEAM, docInputmanageTeam);
	}

	/**
	 *Will return boolean true if CT org is subscribed to the Team
	 * @param sTeamId
	 * @param sOrganizationCode
	 * @param sShipNodeKey
	 * @return
	 */
	private boolean checkIsEmployeeSubscribed(String sTeamId, String sOrganizationCode,String sShipNodeKey) {

		YFCDocument docInputmanageTeam  = YFCDocument.getDocumentFor("<Team TeamId='"+sTeamId +"' OrganizationCode='"+sOrganizationCode +"' ><TeamNodesList>"
				+ "<TeamNodes ShipnodeKey='"+sShipNodeKey+"'/></TeamNodesList></Team>");
		YFCDocument docTempmanageTeam  = YFCDocument.getDocumentFor("<TeamList><Team TeamId='' /></TeamList>");
		YFCDocument docOutputmanageTeam  = invokeYantraApi(TelstraConstants.GET_TEAM_LIST, docInputmanageTeam, docTempmanageTeam);
		YFCElement eleTeam = docOutputmanageTeam.getDocumentElement().getElementsByTagName("Team").item(0);

		if(!YFCObject.isNull(eleTeam)){
			logger.verbose("Employee is subscribed to CT");
			return true;
		}
		return false;
	}

	/**
	 * Creating New team for the Manager with team ID as <ManagerEmployeeId>_TEAM
	 * @param sManagerEmployeeID
	 */
	private void createTeam(String strManagerAccountName) {
		/*
		 * <Team TeamId="M2" OrganizationCode="TELSTRA"  DocumentTypeAccessMode="02" EnterpriseAccessMode="10" ShipNodeAccessMode="03" />
		 * DocumentTypeAccessMode = 02  (Restricted access mode. Access is restricted to the list configured in the Data security document types entity.)
		 * EnterpriseAccessMode   = 10  (Use parent data security group access mode)
		 * ShipNodeAccessMode     = 03 	(Access allowed to restricted nodes in the data security nodes entity)
		 */
		logger.verbose("Creating Team for the Manager");

		YFCDocument docInputManageTeam = YFCDocument.getDocumentFor("<Team TeamId='"+strManagerAccountName+"_TEAM' OrganizationCode='TELSTRA'  DocumentTypeAccessMode='02' EnterpriseAccessMode='10' ShipNodeAccessMode='03' />");
		invokeYantraApi(TelstraConstants.MANAGE_TEAM, docInputManageTeam);

	}

	/**
	 * The method returns getTeamList output document
	 * @param sPreviousManagerEmployeeID
	 * @return
	 */
	private YFCDocument getTeamList(String strManagerAccountName) {
		YFCDocument docInputmanageTeam  = YFCDocument.getDocumentFor("<Team TeamId='"+strManagerAccountName +"_TEAM' />");
		return invokeYantraApi(TelstraConstants.GET_TEAM_LIST, docInputmanageTeam);	
	}
	/**
	 * The Method will subscribe the communication technician(CT) to the team.
	 * @param sTeamId
	 * @param sOrganizationCode
	 * @param sShipNodeKey
	 */
	private void subscribeCTToTeam(String sTeamId, String sOrganizationCode,String sShipNodeKey) {
		YFCDocument docInputmanageTeam  = YFCDocument.getDocumentFor("<Team TeamId='"+sTeamId +"' OrganizationCode='"+sOrganizationCode +"'><TeamNodesList><TeamNodes Operation='Create' ShipnodeKey='"+sShipNodeKey +"'/></TeamNodesList></Team>");
		logger.verbose("Subscribing CT to Team"+docInputmanageTeam.toString());
		invokeYantraApi(TelstraConstants.MANAGE_TEAM, docInputmanageTeam);
	}

	/**
	 * The method returns boolean true if a User exists in Sterling
	 * @param sManagerEmployeeID
	 * @return GetUserList output
	 */
	private boolean checkUserExists(String strEmailID) {
		YFCDocument docUserListinXml = YFCDocument
				.getDocumentFor("<User Loginid='" + strEmailID
						+ "' />");
		YFCDocument doctempgetUserListXml = YFCDocument
				.getDocumentFor("<UserList><User DataSecurityGroupId='' Loginid='' Username='' /></UserList>");
		YFCDocument docOutgetUserListXml = invokeYantraApi("getUserList",
				docUserListinXml, doctempgetUserListXml);
		YFCElement eleUser = docOutgetUserListXml.getDocumentElement().getChildElement("User");

		//Create user if user does not exist in Sterling 
		if(!YFCObject.isNull(eleUser)) {
			logger.verbose("User Already Exists : : "+eleUser.toString());
			return true;
		}
		return false;
	}

	/**
	 * The Method is used to create Input document for creating the user both CT and CT manager Users in Sterling
	 * @param sEmployeeName
	 * @param sEmployeeID
	 * @param sManagerEmployeeID
	 * @param sEmailID 
	 * @param strOrganizationKey 
	 * @param strTeam 
	 * @return docInputcreateUserHierarchy
	 */
	private void createUser(String strEmployeeFirstName, String sEmailID, String strTeam, String strOrganizationKey,String strUserGroups) {

		/*
		 * Sample Input:
		 * <User Loginid="sEmailID" Username="strEmployeeFirstName"  DataSecurityGroupId="IBM_TEAM" Password="password" 
		 * Localecode="en_AU_ACT" OrganizationKey="V25002"><ContactPersonInfo sEmailID='xyz@yahoo.in' 
		 * FirstName='Keerthi'' /></User>
		 */

		logger.verbose("Creating CT User");

		YFCDocument docInputcreateUserHierarchy  = YFCDocument.getDocumentFor("<User Activateflag='Y' Theme='sapphire' Localecode='en_AU_VIC' MenuId='DEFAULT_MENU'  />");
		YFCElement eleUser = docInputcreateUserHierarchy.getDocumentElement();
		eleUser.setAttribute("Password", "Password");
		eleUser.setAttribute(TelstraConstants.LOGIN_ID, sEmailID);
		eleUser.setAttribute("Username", strEmployeeFirstName);
		eleUser.setAttribute(TelstraConstants.DATA_SECURITY_GROUP_ID, strTeam);
		eleUser.setAttribute(TelstraConstants.ORGANIZATION_KEY, strOrganizationKey);

		YFCElement elContactPersonInfo = eleUser.createChild("ContactPersonInfo");
		elContactPersonInfo.setAttribute(TelstraConstants.EMAIL_ID, sEmailID);

		YFCElement eleUserGroupLists = eleUser.createChild("UserGroupLists");		
		String[] servicesUserGroups= strUserGroups.split(",");


		for (int i = 0; i < servicesUserGroups.length; i++) {
			eleUserGroupLists.createChild("UserGroupList").setAttribute("UsergroupId", servicesUserGroups[i].trim());
		}


		logger.verbose("Input Document for CreateUserHireachy : : "+docInputcreateUserHierarchy.toString());
		invokeYantraApi("createUserHierarchy", docInputcreateUserHierarchy);
	}

	/**
	 * The method is to check if the employeeID is a CT Manager
	 * @param strEmployeeID
	 * @return
	 */
	private boolean validateIfCTManager(String strAccountName) {
		YFCDocument docEmployeeListinXml = YFCDocument.getDocumentFor("<Employee ManagerAccountName='"+strAccountName+"' CTFlag='Y' />");
		YFCDocument outXmlEmployeeList = invokeYantraService("GpsGetEmployeeList", docEmployeeListinXml);
		YFCElement eleinEmployeetag = outXmlEmployeeList.getElementsByTagName(TelstraConstants.EMPLOYEE).item(0);
		if(!YFCObject.isVoid(eleinEmployeetag)){
			logger.verbose("The CT is a manager");
			return true;
		}
		return false;
	}

	/**
	 * The method is to Update the CT Flag to N/Y depending on the DAC existing 
	 * @param yfcEleEmployee
	 */
	private void updateCTFlag(YFCElement yfcEleEmployee,String CTFlag) {
		logger.verbose("Updating the CT Flag of the Employee to : :" +CTFlag);

		yfcEleEmployee.setAttribute(TelstraConstants.CT_FLAG, CTFlag);
		String sEmployeeInputEle = yfcEleEmployee.toString();
		YFCDocument docInChangeEmployee = YFCDocument.getDocumentFor(sEmployeeInputEle);
		invokeYantraService("GpsChangeEmployee", docInChangeEmployee);		
	}

	/**
	 * This method is to return true if the Communication Technician has any CT Node associated with him.
	 * @param sEmployeeID
	 * @return externalCustomerID/DAC of the Employee
	 */
	private boolean isCommunicationTechnician(String sShipNodeKey) {

		/*The DAC associated with the employee would be prefixed with V with the employeeID
		 *  and to check if the DAC exists or not call getShipNode List Api
		 */

		YFCDocument docNodeListinXml = YFCDocument
				.getDocumentFor("<ShipNode ShipNode='"+sShipNodeKey+"' />");
		YFCDocument doctempgetNodeListinXml = YFCDocument
				.getDocumentFor("<ShipNodeList><ShipNode ShipNode='' NodeType=''/></ShipNodeList>");
		YFCDocument outXml = invokeYantraApi("getShipNodeList",
				docNodeListinXml, doctempgetNodeListinXml);

		YFCElement eleShipNode = outXml.getDocumentElement().getChildElement(
				"ShipNode");

		if(!YFCObject.isVoid(eleShipNode)){
			logger.verbose("Attribute Value of the eleShipNode is : : "+eleShipNode.toString());
			return true;
		}
		return false;
	}

}

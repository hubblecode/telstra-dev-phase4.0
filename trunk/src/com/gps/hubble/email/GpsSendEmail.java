package com.gps.hubble.email;

import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.catalog.api.ManageCatalog;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSException;

/**
 * 
 * @author Prateek
 *
 */
public class GpsSendEmail extends AbstractCustomApi{

	private static YFCLogCategory logger = YFCLogCategory.instance(ManageCatalog.class);

	/**
	 * Sample input
	 * <SendMail>
        <MessageHeader FromAddress='noreply@au1.ibm.com'
            MailHost='146.89.213.203' MailPort='25' SubjectText='Can you provide the Proof of Deliver (POD) of the PO ORDER61'  FailureEmailAddress='' >
            <ToAddresses>
                <ToAddress EmailID=''/>
            </ToAddresses>
        </MessageHeader>
       <MessageDetail>Hi, &#xa;Kindly confirm the Proof of Delivery (POD) and POD Date of the PO, by replying this email.</MessageDetail>
    </SendMail>

	 */

	@Override
	public YFCDocument invoke(YFCDocument yfcDoc) throws YFSException {

		try {
			sendEmail (yfcDoc);
		} catch (AddressException e) {
			e.printStackTrace();
			LoggerUtil.errorLog("AddressException ", logger, e.getMessage());
		} catch (MessagingException e) {
			e.printStackTrace();
			LoggerUtil.errorLog("MessagingException ", logger, e.getMessage());
		}
		return yfcDoc;
	}

	/**
	 * 
	 * @param yfcDoc
	 * @throws AddressException
	 * @throws MessagingException
	 */
	private void sendEmail(YFCDocument yfcDoc) throws AddressException, MessagingException {

		LoggerUtil.startComponentLog(logger, this.getClass().getName(),"sendEmail", yfcDoc);

		YFCElement yfcEleMessageHeader = yfcDoc.getElementsByTagName("MessageHeader").item(0);
		String sFromAddress = yfcEleMessageHeader.getAttribute("FromAddress");
		String sMailHost = yfcEleMessageHeader.getAttribute("MailHost");
		String sMailPort = yfcEleMessageHeader.getAttribute("MailPort");
		String sSubject = yfcEleMessageHeader.getAttribute("SubjectText");
		String sFailureEmailAddress = yfcEleMessageHeader.getAttribute("FailureEmailAddress");

		YFCNode yfcNodeMessageDetail = yfcDoc.getElementsByTagName("MessageDetail").item(0);		
		String sMessageDetail = yfcNodeMessageDetail.getNodeValue();

		YFCNodeList<YFCNode> yfcnNlToAddress = XPathUtil.getXpathNodeList(yfcDoc, "//MessageHeader/ToAddresses/ToAddress");
		InternetAddress[] toAddress = new InternetAddress[yfcnNlToAddress.getLength()];

		for (int i = 0; i < yfcnNlToAddress.getLength(); i++) {

			YFCElement yfcEleToAddress = (YFCElement) yfcnNlToAddress.item(i);
			String sToEmailId = yfcEleToAddress.getAttribute("EmailID");
			LoggerUtil.verboseLog("GpsSendEmail::sendEmail:: To Email Id ", logger, sToEmailId);
			toAddress[i] = new InternetAddress(sToEmailId);
		}


		Properties props = System.getProperties();
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.port", sMailPort);
		props.put("mail.smtp.host", sMailHost);
		props.put("mail.smtp.auth", "false");
		props.put("mail.smtp.from", sFailureEmailAddress);

		Session session = Session.getInstance(props, null);
		MimeMessage message = new MimeMessage(session);
		message.setFrom(new InternetAddress(sFromAddress, true));
		message.setSentDate(new Date());
		message.setRecipients(Message.RecipientType.TO, toAddress);
		message.setSubject(sSubject);	
		message.setContent(sMessageDetail, "text/plain");
		LoggerUtil.verboseLog("GpsSendEmail::sendEmail:: ", logger, "Sending ....");
		
		Transport.send(message);
		
		LoggerUtil.verboseLog("GpsSendEmail::sendEmail:: ", logger, "Sending done ...");
		LoggerUtil.endComponentLog(logger, this.getClass().getName(),"sendEmail", yfcDoc);


	}
}

package com.gps.hubble.file.service;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.gps.hubble.file.Row;
import com.gps.hubble.utils.ExcelUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;

public class RowDataConsolidationServiceExtender extends RowDataConsolidationService {
  
  /**
   * This method will override the processRow method in RowDataConsolidationService since the behaviour of preparing the SPS_INVENTORY_REPORT is different from
   * all other uploads.
   * 
   * @param workbook
   * @param fileUploadRow
   * @param sheet
   * @param errorCellPosition
   * 
   */
  public void processRow(Workbook workbook, Sheet sheet, YFCElement fileUploadRow, int errorCellPosition) {
    String rowObject = fileUploadRow.getAttribute("RowObject");
    YFCDocument rowObjectDoc = YFCDocument.getDocumentFor(rowObject);
    Row row = new Row(rowObjectDoc);
    moveAnalysisRow(workbook, row);
  }

  /**
   * This method will move the row to the analysis sheet.
   * @param workbook
   * @param row
   */
  public static void moveAnalysisRow(Workbook workbook, com.gps.hubble.file.Row row) {
    Sheet sheet = workbook.getSheet("Analysis Sheet");
    if (sheet == null) {
      sheet = workbook.createSheet("Analysis Sheet");
      createHeaderForAnalysis(sheet);
      return;
    }

    ExcelUtil.createApacheRow(sheet, row);
  }
  
  /**
   * This method will create the header for the analysis sheet.
   * @param sheet
   */
  private static void createHeaderForAnalysis(Sheet sheet) {
    YFCDocument spsInvReportHeaderDoc =
        YFCDocument.getDocumentFor("<Row RowNumber='0'> <Cells> "
            + "<Cell CellIndex='0' DataType='String' Value='Material Id'/> "
            + "<Cell CellIndex='1' DataType='String' Value='Material Name'/> "
            + "<Cell CellIndex='2' DataType='String' Value='Serial Number'/> "
            + "<Cell CellIndex='3' DataType='String' Value='MITS Barcode'/> "
            + "<Cell CellIndex='4' DataType='String' Value='Equipment'/> "
            + "<Cell CellIndex='5' DataType='String' Value='System Status'/> "
            + "<Cell CellIndex='6' DataType='String' Value='Plant'/> "
            + "<Cell CellIndex='7' DataType='String' Value='Stor. Location'/> "
            + "<Cell CellIndex='8' DataType='String' Value='Object Type'/> "
            + "<Cell CellIndex='9' DataType='String' Value='Superord. Equip.'/> "
            + "<Cell CellIndex='10' DataType='String' Value='Functional Loc.'/> "
            + "<Cell CellIndex='11' DataType='String' Value='Description'/> "
            + "<Cell CellIndex='12' DataType='String' Value='ITEC Serial'/> "
            + "<Cell CellIndex='13' DataType='String' Value='ITEC Barcode'/> "
            + "<Cell CellIndex='14' DataType='String' Value='ITEC Location'/> "
            + "<Cell CellIndex='15' DataType='String' Value='ITEC Status'/> "
            + "<Cell CellIndex='16' DataType='String' Value='ITEC Revision No'/> " + "</Cells> </Row>");
    Row row = new Row(spsInvReportHeaderDoc);
    ExcelUtil.createApacheRow(sheet, row);
  }

}

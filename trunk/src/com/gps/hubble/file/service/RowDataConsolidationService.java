package com.gps.hubble.file.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import javax.mail.MessagingException;

import org.apache.commons.codec.binary.Base64;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.constants.TelstraErrorCodeConstants;
import com.gps.hubble.file.FileProcessor;
import com.gps.hubble.file.FileProcessorFactory;
import com.gps.hubble.file.RowInsertService;
import com.gps.hubble.utils.ExcelUtil;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.date.YDate;
import com.yantra.yfc.date.YTimestamp;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCConfigurator;
import com.yantra.yfs.japi.YFSException;

@SuppressWarnings({ "unused", "unused" })
public class RowDataConsolidationService extends AbstractCustomApi{
	private static YFCLogCategory logger = YFCLogCategory.instance(RowInsertService.class);
	private static final int MAX_RECORD_TO_FETCH = 1000;
	private boolean bIsExcelHasError = false;
	@Override
	public YFCDocument invoke(YFCDocument inXml) throws YFSException {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", inXml);
		String transactionType = inXml.getDocumentElement().getAttribute("TRANSACTION_TYPE");
		String fileUploadKey = inXml.getDocumentElement().getAttribute("FILE_UPLOAD_KEY");
		if(YFCObject.isVoid(transactionType) || YFCObject.isVoid(fileUploadKey)){
			throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.NULL_FILE_KEY_AND_TRANSACTION_TYPE, new YFSException());
		}
		
		YFCElement elemFileUpload = getFileUploadElement(fileUploadKey, transactionType);
		boolean isProcessed = elemFileUpload.getBooleanAttribute("ProcessedFlag");
		if(isProcessed){
			//This is already processed
			return inXml;
		}
		if(!isAllRowProcessed(fileUploadKey, transactionType)){
			boolean leaveService = true;
			long maxSleepCount = 10;
			long sleepInterval = 1000;
			for(int i=0;i<maxSleepCount;i++){
				try{
					Thread.sleep(sleepInterval);
					if(isAllRowProcessed(fileUploadKey, transactionType)){
						leaveService = false;
						break;
					}
				}catch(Exception e){
					break;
				}
			}
			if(leaveService){
				logger.warn("As all records for "+transactionType+" and file upload key "+fileUploadKey+ " are not processed..exiting the RowDataConsolidationService.");
				return inXml;
			}
		}
		/*
		 * Below method will update the file upload table with status as process file 100%
		 */
		updateFileUploadRecord(fileUploadKey, TelstraConstants.PROCESSING_FILE, "100");
		
		processFileUpload(elemFileUpload, fileUploadKey, transactionType);
		/*
		 * Below alert is raised so that user is notified about the completion of the excel processing
		 */
		raiseNotification(elemFileUpload, fileUploadKey);
		invokeUploadedFileProcessorAgent(transactionType);
		updateFileUploadRecord(fileUploadKey, TelstraConstants.EXCEL_UPLOAD_COMPLETED, "-1");
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", inXml);
		return inXml;
	}
	
	/**
	 * Below method will update the file upload status with the passed parameter
	 * @param sFileUploadKey
	 * @param sStatus
	 * @param sFileProcessed
	 */
	private void updateFileUploadRecord(String sFileUploadKey, String sStatus, String sFileProcessed) {
		YFCDocument yfcDocChangeFileUploadIp = YFCDocument.createDocument(TelstraConstants.FILE_UPLOAD);
		YFCElement yfcEleChangeFileUploadIp = yfcDocChangeFileUploadIp.getDocumentElement();
		yfcEleChangeFileUploadIp.setAttribute(TelstraConstants.FILE_UPLOAD_KEY, sFileUploadKey);
		yfcEleChangeFileUploadIp.setAttribute(TelstraConstants.FILE_PROGRESSED, sFileProcessed);
		yfcEleChangeFileUploadIp.setAttribute(TelstraConstants.STATUS, sStatus);
		String sHasError = "N";
		if(bIsExcelHasError){
			sHasError = "Y";
		}
		yfcEleChangeFileUploadIp.setAttribute(TelstraConstants.HAS_ERROR, sHasError);
		LoggerUtil.verboseLog("RowDataConsolidationService :: updateFileUploadRecord :: yfcDocChangeFileUploadIp", logger, yfcDocChangeFileUploadIp);
		try{
			invokeYantraService(TelstraConstants.SERVICE_GPS_CHANGE_FILE_UPLOAD, yfcDocChangeFileUploadIp);
		}
		catch(Exception e){
			LoggerUtil.errorLog("Something went wrong while updating the status percent", logger, e);
		}		
		
	}

	/**
	 * 
	 * @param elemFileUpload
	 * @param fileUploadKey 
	 */
	private void raiseNotification(YFCElement elemFileUpload, String fileUploadKey) {

		String sCreateUserId = elemFileUpload.getAttribute(TelstraConstants.CREATE_USER_ID);
		String sTransactionType = getTransactionTypeLabel(elemFileUpload.getAttribute(TelstraConstants.TRANSACTION_TYPE));
		YTimestamp yDateCreateTS = elemFileUpload.getYTimestampAttribute(TelstraConstants.CREATETS);
		String sUserName = elemFileUpload.getAttribute(TelstraConstants.USER_NAME);
		String sCreateTS = yDateCreateTS.getString(TelstraConstants.DATE_FORMAT_DD_MMM_YY_TIME);
		
		String sCodeValue = "WITHOUT_ERROR";
		if(bIsExcelHasError){
			sCodeValue = "WITH_ERROR";
		}
		String sAlertDescription = callCommonCodeList(TelstraConstants.EXCEL_ALERT_TEMPLATE, sCodeValue);
		sAlertDescription = sAlertDescription.replace("${date}", sCreateTS);
		sAlertDescription = sAlertDescription.replace("${Key}", fileUploadKey);
		sAlertDescription = sAlertDescription.replace("${Upload}", sTransactionType);
		sAlertDescription = sAlertDescription.replace("${user}", sUserName);

		YFCDocument yfcDocCreateExceptionIp = YFCDocument
				.getDocumentFor("<Inbox AssignedToUserId='"+sCreateUserId+"' Description='" + sAlertDescription
						+ "'  ExceptionType='GPS_EXCEL_UPLOAD_ALERT'  QueueId='EXCEL_UPLOAD_NOTIFICATION'><InboxReferencesList><InboxReferences Name='Upload ID' ReferenceType='TEXT' Value='"
						+ fileUploadKey + "'/></InboxReferencesList></Inbox>");
		invokeYantraApi(TelstraConstants.API_CREATE_EXCEPTION, yfcDocCreateExceptionIp);
	}


	private String getTransactionTypeLabel(String sTransactionType) {

		String sTransactionTypeLabel = "";
		switch (sTransactionType) {
		case "ORG_LOAD":
			sTransactionTypeLabel = "Organization load";
			break;

		case "SPS_INVENTORY_REPORT":
			sTransactionTypeLabel = "SPS inventory report";
			break;

		case "INVENTORY_UPLOAD":
			sTransactionTypeLabel = "Inventory Upload";
			break;
		case "WEEK_NUMBER":
			sTransactionTypeLabel = "Week number";
			break;

		case "MATERIAL_TO_TRANSPORT_MAP":
			sTransactionTypeLabel = "Material to transport map";
			break;

		case "TRANSPORT_MATRIX":
			sTransactionTypeLabel = "Transport matrix";
			break;
		case "DAC_SCHEDULE":
			sTransactionTypeLabel = "DAC schedule";
			break;

		case "MILK_RUN_SCHEDULE":
			sTransactionTypeLabel = "Milk run";
			break;

		case "ASN_LOAD":
			sTransactionTypeLabel = "ASN load";
			break;
		case "STOCK_MOVEMENT_REPORT":
			sTransactionTypeLabel = "Stock movement report";
			break;
		
		default:
			sTransactionTypeLabel = "Invalid transaction";
			break;
		}

		return sTransactionTypeLabel;
	}

	private String callCommonCodeList(String sCodeType , String sCodeValue) {
		YFCDocument yfcDocCommonCodeIp = YFCDocument.getDocumentFor("<CommonCode CodeType='"+sCodeType+"' CodeValue='"+sCodeValue+"'/>");
		YFCDocument yfcDocCommonCodeTemp = YFCDocument.getDocumentFor("<CommonCodeList><CommonCode CodeLongDescription='' CodeType='' CodeValue='' /></CommonCodeList>");
		YFCDocument yfcDocCommonCodeOp = invokeYantraApi(TelstraConstants.API_GET_COMMON_CODE_LIST, yfcDocCommonCodeIp, yfcDocCommonCodeTemp);
		
		String sLongDesc = XPathUtil.getXpathAttribute(yfcDocCommonCodeOp, "//CommonCode/@CodeLongDescription");
		if(YFCCommon.isStringVoid(sLongDesc)){
			sLongDesc = "${Upload} was completed successfully on ${date}";
		}
		
		return sLongDesc;
	}

	private void processFileUpload(YFCElement elemFileUpload, String fileUploadKey, String transactionType){
		logger.debug("RowDataConsolidation for fileUploadKey"+fileUploadKey+" and transactionType "+transactionType);
		String strEncodedExcel = elemFileUpload.getAttribute("FileObject");
		byte[] decodedBytes = Base64.decodeBase64(strEncodedExcel.getBytes());
		try{
			logger.debug("Creating workbook");
			Workbook workbook = createWorkbook(decodedBytes);
			logger.debug("workbook created");
			consolidateRows(workbook, transactionType, fileUploadKey, -1);
			byte[] encodedBytes = getEncodedBytes(workbook);
			updateFileUpload(elemFileUpload, encodedBytes);
			sendEmail(elemFileUpload, transactionType,fileUploadKey, workbook, encodedBytes);
		}catch(IOException ioe){
			throw new  YFSException(ioe.getMessage());
		}catch(InvalidFormatException ife){
			throw new  YFSException(ife.getMessage());
		}catch(MessagingException me){
			throw new  YFSException(me.getMessage());
		}
	}
	
  private YFCElement getFileUploadElemFromList(YFCDocument docFileUploadList){
		if(docFileUploadList == null){
			return null;
		}
		return docFileUploadList.getDocumentElement().getChildElement("FileUpload");
	}
	
	
	private void consolidateRows(Workbook workbook, String transactionType, String fileUploadKey, int rowNo){
		List<YFCElement> fileUploadRows = getRowsToConsolidate(transactionType, fileUploadKey, rowNo);
		rowNo = rowNo + MAX_RECORD_TO_FETCH;
		consolidateRows(workbook, transactionType, fileUploadKey, rowNo, fileUploadRows);
	}
	
	private void consolidateRows(Workbook workbook, String transactionType, String fileUploadKey, int rowNo, List<YFCElement> fileUploadRows){
		logger.debug("consolidateRows is getting called");
		Sheet sheet = workbook.getSheetAt(0);
		Row row = sheet.getRow(0);
		FileProcessor processor = FileProcessorFactory.create(transactionType, getServiceInvoker());
		int errorCellPosition = processor.getNumberOfCells(transactionType);
		if(errorCellPosition == 0){
			errorCellPosition = row.getPhysicalNumberOfCells();
		}
		for(YFCElement fileUploadRow : fileUploadRows){
			processRow(workbook, sheet, fileUploadRow,errorCellPosition);
		}
		List<YFCElement> nextFileUploadRows = getRowsToConsolidate(transactionType, fileUploadKey, rowNo);
		if(nextFileUploadRows.isEmpty()){
			return;
		}
		rowNo = rowNo + MAX_RECORD_TO_FETCH;
		consolidateRows(workbook, transactionType, fileUploadKey, rowNo,nextFileUploadRows);
	}
	
	public void processRow(Workbook workbook,Sheet sheet, YFCElement fileUploadRow,int errorCellPosition){
		int rowNo = fileUploadRow.getIntAttribute("RowNumber");
		Row row = sheet.getRow(rowNo);
		if(rowNo == 0){
			ExcelUtil.moveRow(workbook, row, true,"Error Message",errorCellPosition);
			ExcelUtil.moveRow(workbook, row, false,"");
			return;
		}
		boolean hasError = fileUploadRow.getBooleanAttribute("HasError");
		if(hasError){
			String errorMessageDoc = fileUploadRow.getAttribute("ErrorMessage");
			String errorMessage = getReadableDescription(errorMessageDoc);
			ExcelUtil.moveRow(workbook, row, true, errorMessage,errorCellPosition);
			/*
			 * Setting bIsExcelHasError so that proper template is used for sending notification to the user.
			 */
			bIsExcelHasError = true;
		}else{
			ExcelUtil.moveRow(workbook, row, false, "");
		}
		
	}
	
	private List<YFCElement> getRowsToConsolidate(String transactionType, String fileUploadKey, int rowNo){
	    YFCDocument docGetFileUploadRows = getFileUploadRowDocument(transactionType, fileUploadKey, rowNo);
	    YFCDocument docFileUploadRowOutput = invokeYantraService("GpsGetFileUploadRowList", docGetFileUploadRows);
	    List<YFCElement> fileUploadRows = new ArrayList<>();
	    if(docFileUploadRowOutput == null || docFileUploadRowOutput.getDocumentElement() == null){
	    	return fileUploadRows;
	    }
	    YFCElement elemFileUploadRows = docFileUploadRowOutput.getDocumentElement();
	    if(elemFileUploadRows.getChildren() == null){
	    	return fileUploadRows;
	    }
	    for(Iterator<YFCElement> itr = elemFileUploadRows.getChildren().iterator();itr.hasNext();){
	    	fileUploadRows.add(itr.next());
	    }
	    sort(fileUploadRows);
		return fileUploadRows;
	}
	
	private void sort(List<YFCElement> list){
		Collections.sort(list, new Comparator<YFCElement>(){
			@Override
			public int compare(YFCElement o1, YFCElement o2) {
				if(o1 == null){
					return -1;
				}
				if(o2 == null){
					return 1;
				}
				int o1RowNo = o1.getIntAttribute("RowNumber");
				int o2RowNo = o2.getIntAttribute("RowNumber");
				if(o1RowNo > o2RowNo){
					return 1;
				}else if(o1RowNo == o2RowNo){
					return 0;
				}else{
					return -1;
				}
			}
			
		});
	}
	
	private YFCDocument getFileUploadRowDocument(String transactionType, String fileUploadKey, int rowNo){
		return YFCDocument.getDocumentFor("<FileUploadRow FileUploadKey='"+fileUploadKey+"' TransactionType='"+transactionType+"' RowNumberQryType='GT' RowNumber='"+rowNo+"' MaximumRecords='"+MAX_RECORD_TO_FETCH+"' />");
	}
	
	
	
	private byte[] getEncodedBytes(Workbook workbook) throws IOException{
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		workbook.write(out);
		byte[] processByteArray = out.toByteArray();
		out.close();
		byte[] encodedBytes = Base64.encodeBase64(processByteArray);
		return encodedBytes;
	}
	
	
	/**
	 * @param yfcRootEle
	 * @param processExcel
	 * @return
	 * @throws IOException
	 */
	private void updateFileUpload(YFCElement yfcRootEle, byte[] encodedBytes) throws IOException {
		logger.debug("Updating file upload");
		YFCDocument yfcChangeInDoc = YFCDocument.getDocumentFor("<UploadedFile/>");
		YFCElement yfcUploadedFileEle = yfcChangeInDoc.getDocumentElement();
		String strUploadedFileKey = yfcRootEle.getAttribute("FileUploadKey");
		yfcUploadedFileEle.setAttribute("FileUploadKey", strUploadedFileKey);
		yfcUploadedFileEle.setAttribute("FileObject", new String(encodedBytes));
		yfcUploadedFileEle.setAttribute("ProcessedFlag", "Y");
		invokeYantraService("GpsChangeFileUpload", yfcChangeInDoc);
		logger.debug("Updating file upload finished");
	}
	
	
	/**
	 * @param yfcRootEle
	 * @param strTransactionType
	 * @param processExcel
	 * @param encodedBytes
	 * @throws MessagingException
	 * @throws IOException
	 */
	private void sendEmail(YFCElement yfcRootEle, String strTransactionType,String fileUploadKey, Workbook processExcel,
			byte[] encodedBytes) throws MessagingException, IOException {
		String strToEmailId = yfcRootEle.getAttribute("NotifyEmail");
		if(!YFCCommon.isVoid(strToEmailId)){
			
			String strFromEmailId = YFCConfigurator.getInstance().getProperty("gps.email.from.id", TelstraConstants.EMAIL_FROM_ID);
			String strPort =		YFCConfigurator.getInstance().getProperty("gps.email.port", TelstraConstants.EMAIL_PORT);
			String strHost =		YFCConfigurator.getInstance().getProperty("gps.email.hostname", TelstraConstants.EMAIL_HOST);
			String sFileExtension = ".xls";
			if(processExcel instanceof org.apache.poi.xssf.usermodel.XSSFWorkbook){
				sFileExtension = ".xlsx";
			}
			String fileName = strTransactionType + "_" + fileUploadKey;
			ExcelUtil.sendExcelAttachmentEmail(encodedBytes, strToEmailId, strFromEmailId, fileName, sFileExtension, strPort, strHost);
		}
	}
	
	private Workbook createWorkbook(byte[] decodedBytes) throws IOException,InvalidFormatException{
		InputStream file = null;
		try {
			file = new ByteArrayInputStream(decodedBytes);
			Workbook workbook = WorkbookFactory.create(file);
			return workbook;
		} finally{
			if(null!=file){
				file.close();
			}
		}
	}
	
	private boolean isAllRowProcessed(String fileUploadKey, String transactionType){
		YFCDocument docGetUnprocessedRows = YFCDocument.getDocumentFor("<FileUploadRow FileUploadKey='"+fileUploadKey+"' TransactionType='"+transactionType+"' ProcessedFlag='N' />");
		YFCDocument docUnprocessedRowList = invokeYantraService("GpsGetFileUploadRowList", docGetUnprocessedRows);
		if(docUnprocessedRowList != null && docUnprocessedRowList.getDocumentElement() != null && docUnprocessedRowList.getDocumentElement().getChildren() != null && docUnprocessedRowList.getDocumentElement().getChildren().hasNext()){
			int count = 0;
			for(Iterator<YFCElement> itr = docUnprocessedRowList.getDocumentElement().getChildren();itr.hasNext();){
				count = count + 1;
			}
			return false;
		}
		return true;
	}
	
	private YFCElement getFileUploadElement(String fileUploadKey, String transactionType){
		YFCDocument docGetFileUpload = YFCDocument.getDocumentFor("<FileUpload FileUploadKey='"+fileUploadKey+"' />");
		YFCDocument docFileUploadList = invokeYantraService("GpsGetFileUploadList", docGetFileUpload);
		return getFileUploadElemFromList(docFileUploadList);
	}
	
	private void invokeUploadedFileProcessorAgent(String transactionType){
		String criteriaId = transactionType;
		YFCDocument docTriggerAgent = YFCDocument.createDocument("TriggerAgent");
		YFCElement elemTriggerAgent = docTriggerAgent.getDocumentElement();
		elemTriggerAgent.setAttribute("CriteriaId", criteriaId); //This will be based on transactionType
		YFCElement elemCriteriaAttributes = elemTriggerAgent.createChild("CriteriaAttributes");
		YFCElement elemCriteriaAttributeTransaction = elemCriteriaAttributes.createChild("Attribute");
		elemCriteriaAttributeTransaction.setAttribute("Name","TRANSACTION_TYPE");
		elemCriteriaAttributeTransaction.setAttribute("Value",transactionType);
		invokeYantraApi("triggerAgent", docTriggerAgent);
	}
	
	private String getReadableDescription(String errorDesc){
		if(YFCObject.isVoid(errorDesc)){
			return errorDesc;
		}
		if(!isDocument(errorDesc)){
			return errorDesc;
		}
		YFCDocument errorDoc = getExceptionDocument(errorDesc);
		if(errorDoc == null || errorDoc.getDocumentElement() == null){
			return errorDesc;
		}
		YFCElement elemError = errorDoc.getDocumentElement();
		String internalDesc = elemError.getAttribute("ErrorDescription");
		if(YFCObject.isVoid(internalDesc)){
			String elemErrorTagName = elemError.getTagName();
			if(elemErrorTagName.equals("Errors")){
				YFCElement elemInternalError = getErrorElement(elemError);
				if(elemInternalError != null){
					internalDesc = elemInternalError.getAttribute("ErrorDescription");
					if(!YFCCommon.isVoid(internalDesc)){
						return internalDesc;
					}
				}
			}
			return errorDesc;
		}
		return internalDesc;
	}
	
	private YFCElement getErrorElement(YFCElement elemErrors){
		if(elemErrors == null || elemErrors.getChildren("Error") == null){
			return null;
		}
		for(Iterator<YFCElement> itr = elemErrors.getChildren("Error");itr.hasNext();){
			return itr.next();
		}
		return null;
	}
	
	private YFCDocument getExceptionDocument(String errorDesc){
		try{
			return YFCDocument.getDocumentFor(errorDesc);
		}catch(Exception e){
			//The error message is normal string
		}
		return null;
	}
	
	private boolean isDocument(String errorDesc){
		return errorDesc.startsWith("<");
	}

}

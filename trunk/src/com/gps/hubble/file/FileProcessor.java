package com.gps.hubble.file;


import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.bridge.sterling.framework.service.ServiceInvoker;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
/**
* Class description goes here.
*
* @version 1.0 30 June 2016
* @author VGupta
*
*/

public abstract class FileProcessor {
	private static final String EXCEL_CONFIG_FOLDER_PATH = "/global/template/resource/excelconfig/";
	private ServiceInvoker serviceInvoker = null;
	private static YFCLogCategory cat = YFCLogCategory.instance(FileProcessor.class);

	public abstract void processRow(Row row);
		
	protected void setServiceInvoker(ServiceInvoker serviceInvoker){
		this.serviceInvoker=serviceInvoker;
	}
	
	protected ServiceInvoker getServiceInvoker() {
		return serviceInvoker;
	}
	
	public boolean isRowEmpty(Row row){
		String value = row.getCellValue(0);
		if(YFCObject.isVoid(value)){
			return true;
		}
		return false;
	}
	
	public boolean isValidHeader(String transactionType,Row row){
		Map<Integer, String> headerCells = getHeaderCells(transactionType);
		if(headerCells == null || headerCells.isEmpty()){
			cat.error("Invalid Header as no configuration for header found for transaction type "+transactionType);
			return false;
		}
		for(Integer cellNo : headerCells.keySet()){
			String currentHeaderCellValue = row.getCellValue(cellNo);
			if(YFCObject.isVoid(currentHeaderCellValue)){
				cat.error("Processing excel contains an empty header at cell index"+cellNo+" for transaction type "+transactionType);
				return false;
			}
			String expHeaderCellValue = headerCells.get(cellNo);
			if(YFCObject.isVoid(expHeaderCellValue)){
				cat.error("excel configuration contains an empty header at cell index"+cellNo+" for transaction type "+transactionType);
				return false;
			}
			if(!expHeaderCellValue.equalsIgnoreCase(currentHeaderCellValue)){
				cat.error("excel configuration and cell and processing excel cell mismatch for cell index"+cellNo+" . Processing excel cell value "+currentHeaderCellValue+", and configuration cell value:"+expHeaderCellValue+" for transaction type "+transactionType);
				return false;
			}
		}
		return true;
	}
	
	protected Map<Integer, String> getHeaderCells(String transactionType){
		String fileName = EXCEL_CONFIG_FOLDER_PATH + transactionType + ".xml";
		InputStream is = null;
		try{
			is = FileProcessor.class.getResourceAsStream(fileName);
			YFCDocument excelConfig = YFCDocument.parse(is);
			return getHeaderCells(excelConfig);
		}catch(Exception e){
			cat.error("Exception while reading excel config for transaction type "+transactionType, e);
			return null;
		}finally{
			if(is != null){
				try{
					is.close();
				}catch(Exception e){
					cat.error("Exception while closing output stream", e);
				}
			}
		}
	}
	
	protected Map<Integer, String> getHeaderCells(YFCDocument excelConfig){
		YFCElement elemExcelConfig = excelConfig.getDocumentElement();
		if(elemExcelConfig == null){
			return null;
		}
		YFCElement elemHeaderConfig = elemExcelConfig.getChildElement("HeaderConfig");
		if(elemHeaderConfig == null){
			return null;
		}
		YFCElement elemCells = elemHeaderConfig.getChildElement("Cells");
		if(elemCells == null || elemCells.getChildren("Cell") == null){
			return null;
		}
		Map<Integer, String> headerCells = new HashMap<Integer, String>();
		for(Iterator<YFCElement> itr = elemCells.getChildren("Cell").iterator();itr.hasNext();){
			YFCElement elemCell = itr.next();
			int cellIndex = elemCell.getIntAttribute("CellIndex");
			String headerCellName = elemCell.getAttribute("Value");
			headerCells.put(cellIndex, headerCellName);
		}
		return headerCells;
	}
	
	public int getNumberOfCells(String transactionType){
		String fileName = EXCEL_CONFIG_FOLDER_PATH + transactionType + ".xml";
		InputStream is = null;
		try{
			is = FileProcessor.class.getResourceAsStream(fileName);
			YFCDocument excelConfig = YFCDocument.parse(is);
			if(excelConfig == null || excelConfig.getDocumentElement() == null){
				return 0;
			}
			String noOfCellsStr =  excelConfig.getDocumentElement().getAttribute("NoOfCells");
			if(YFCObject.isVoid(noOfCellsStr)){
				return 0;
			}
			return Integer.parseInt(noOfCellsStr);
		}catch(Exception e){
			cat.error("Exception while reading excel config for transaction type "+transactionType, e);
			return 0;
		}finally{
			if(is != null){
				try{
					is.close();
				}catch(Exception e){
					cat.error("Exception while closing output stream", e);
				}
			}
		}
	}	
}

/***********************************************************************************************
 * File	Name		: OrgLoadFileProcessor.java
 *
 * Description		: This class is called for creating organization through excel file 
 * 
 * Modification	Log	:
 * ---------------------------------------------------------------------------------------------
 * Ver #	Date			Author					Modification


 * ---------------------------------------------------------------------------------------------
 * 1.0		30/06/16		Varun Gupta				Initial	Version
 * 1.1		21/04/17		Prateek Kumar			HUB-8727: Append the node type for the DAC 
 * ---------------------------------------------------------------------------------------------
 * 
 */
package com.gps.hubble.file.agent.orgload;

import com.bridge.sterling.utils.ExceptionUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.file.FileProcessor;
import com.gps.hubble.file.Row;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSException;
/**
* Class description goes here.
*
* @version 1.0 30 June 2016
* @author VGupta
*
*/
public class OrgLoadFileProcessor extends FileProcessor {
	
	private static YFCLogCategory cat = YFCLogCategory.instance(OrgLoadFileProcessor.class);
	private static final String ORG_ROLE_LIST_ORG_ROLE_ROLE_KEY = "OrgRoleList/OrgRole@RoleKey";
	@Override	
	
	public void processRow(Row row) {
		manageOrganizationHierarchy(row);
	}

	private void manageOrganizationHierarchy(Row row) {
		String sOrganizationCode = row.getCellValue(0);
		String sParentOrganizationCode = row.getCellValue(2);
		debug("Processing : " + sOrganizationCode);
		if (YFCCommon.isVoid(sOrganizationCode)) {
			throw ExceptionUtil.getYFSException("ERR9900016",new YFSException());
		}

		YFCDocument yfcDocManageOrg = YFCDocument.getDocumentFor("<Organization />");
		YFCElement inputEle = yfcDocManageOrg.getDocumentElement();

		setOrgHeaderAttributes(row, sOrganizationCode, sParentOrganizationCode, inputEle);

		boolean isVendor = prepareManageOrganizationHierarchyInput(row, sOrganizationCode, inputEle);

		debug("manageOrganizationHierarchy input Doc :: " + yfcDocManageOrg);
		getServiceInvoker().invokeYantraApi("manageOrganizationHierarchy", yfcDocManageOrg);

		if (isVendor) {
			applyVendorLogic(row, inputEle);
		}

		String dac = row.getCellValue(4);
		if (!YFCCommon.isVoid(dac)) {
						
			YFCDocument yfcDocManageCustomerIp = YFCDocument.getDocumentFor("<Customer CustomerKey='" + dac + "' ExternalCustomerID='" + sOrganizationCode	+ "' />");
			//HUB-8727 - Begin
			String sNodeType = row.getCellValue(11);
			if(!YFCCommon.isStringVoid(sNodeType)){
			
				YFCElement yfcEleExtn = yfcDocManageCustomerIp.getDocumentElement().createChild(TelstraConstants.EXTN);
				yfcEleExtn.setAttribute(TelstraConstants.CUSTOMER_NODE_TYPE, sNodeType);
			}
			//HUB-8727 - End
			getServiceInvoker().invokeYantraService("GpsDropToDacQ", yfcDocManageCustomerIp);
		}

		cat.info("Successfully read the Organization: " + sOrganizationCode);

	}

	/**
	 * @param row
	 * @param sOrganizationCode
	 * @param isVendor
	 * @param inputEle
	 * @return
	 */
	private boolean prepareManageOrganizationHierarchyInput(Row row, String sOrganizationCode, 
			YFCElement inputEle) {
		boolean isVendor = false;
		if("Y".equalsIgnoreCase(row.getCellValue(5))){
			setBuyerAttributes(inputEle);
			
		}
		if("Y".equalsIgnoreCase(row.getCellValue(6))){
			setSellerAttributes(inputEle);
		}
		
		if("Y".equalsIgnoreCase(row.getCellValue(7))){
			setEnterpriseAttributes(row, sOrganizationCode, inputEle);
			isVendor =  true;
		}
		
		if("Y".equalsIgnoreCase(row.getCellValue(10))){
			setNodeAttributes(row, inputEle);
		}
		
		if("Y".equalsIgnoreCase(row.getCellValue(13))){
			setAttribute(inputEle, "CARRIER", ORG_ROLE_LIST_ORG_ROLE_ROLE_KEY,true);
		}
		
		setAddressAttributes(row, inputEle);

		if(YFCCommon.isVoid(inputEle.getAttribute(TelstraConstants.LOCALE_CODE))){
			inputEle.setAttribute(TelstraConstants.LOCALE_CODE, TelstraConstants.LOCALE_EN_AU_ACT);
		}
		return isVendor;
	}

	/**
	 * @param row
	 * @param inputEle
	 */
	private void applyVendorLogic(Row row, YFCElement inputEle) {
		String vSendsCommitment = row.getCellValue(8);
		String vSendsAsn = row.getCellValue(9);
		
		//HUB 6801 -[START]
		String sOrganizationName = row.getCellValue(1).toUpperCase();
		//HUB 6801 -[END]

		createDefaultVendorNode(inputEle,vSendsCommitment,vSendsAsn,sOrganizationName);
		if("Y".equalsIgnoreCase(inputEle.getAttribute(TelstraConstants.INVENTORY_PUBLISHED))){
			markConsumableOrganization(inputEle.getAttribute(TelstraConstants.ORGANIZATION_CODE));
			createAtpRules(inputEle.getAttribute(TelstraConstants.ORGANIZATION_CODE));
		}
		
		//create Vendor alert queue by calling manageQueue
		createVendorAlertQueue(inputEle.getAttribute(TelstraConstants.ORGANIZATION_CODE));
		
		//create Vendor Exception routing rule by calling manageExceptionRoutingRule
		createVendorExceptionRoutingRule(inputEle.getAttribute(TelstraConstants.ORGANIZATION_CODE));
	}
	
	private void createVendorAlertQueue(String sVendorOrg) {
		//input to manageQueue api
		//<Queue OwnerKey="Vendor1" Priority="1" QueueDescription="Test Queue" QueueId="TEST_Q"  QueueName="TEST_Q"/>
		YFCDocument docManageQueueInput = YFCDocument.getDocumentFor("<Queue OwnerKey='"+sVendorOrg+"' Priority='1' QueueDescription='"+sVendorOrg+" default alert queue' "
				+ "QueueId='"+sVendorOrg+"_QUEUE'  QueueName='"+sVendorOrg+"_QUEUE'/>");
		
		getServiceInvoker().invokeYantraApi("manageQueue", docManageQueueInput);	
	}
	
	private void createVendorExceptionRoutingRule(String sVendorOrg) {
		
		//call getExceptionRoutingRuleList to get list of Exception Types for which Route To Vendor is enabled.
		YFCDocument docGetExceptionRoutingRuleListOutput = getServiceInvoker().invokeYantraApi("getExceptionRoutingRuleList", 
				YFCDocument.getDocumentFor("<ExceptionRoutingRule OrganizationCode='TELSTRA_SCLSA' RoutingRole='SELLER' RoutingType='04'/>"));	
		
		for(YFCElement eleExceptionRoutingRule : docGetExceptionRoutingRuleListOutput.getElementsByTagName("ExceptionRoutingRule") ) {
			String sExceptionType = eleExceptionRoutingRule.getAttribute("ExceptionType");
			
			// call manageOrgExceptionType 
			// input = <OrgExceptionType Active='Y' ExceptionType='PO_NOT_DELIVERED_ALERT' OrganizationCode='578001' Priority='1'/>
			YFCDocument docManageOrgExceptionType = YFCDocument.getDocumentFor("<OrgExceptionType Active='Y' "
					+ "ExceptionType='"+sExceptionType+"' OrganizationCode='"+sVendorOrg+"' Priority='1'/>");
			
			getServiceInvoker().invokeYantraApi("manageOrgExceptionType", docManageOrgExceptionType);
			
			
			//call manageExceptionRoutingRule
			//input = <ExceptionRoutingRule ExceptionRoutingSeq='1' ExceptionType='SHIPPED_PO_CANCEL_ALERT' OrganizationCode='Vendor1' QueueId='Vendor1_Queue' 
			//RoutingType='02'/>
			YFCDocument docManageExceptionRoutingRule = YFCDocument.getDocumentFor("<ExceptionRoutingRule ExceptionRoutingSeq='1' "
					+ "ExceptionType='"+sExceptionType+"' OrganizationCode='"+sVendorOrg+"' QueueId='"+sVendorOrg+"_QUEUE' "
					+ "RoutingType='02'/>");
			
			getServiceInvoker().invokeYantraApi("manageExceptionRoutingRule", docManageExceptionRoutingRule);
			
		}
	
	}
	

	/**
	 * @param row
	 * @param sOrganizationCode
	 * @param sParentOrganizationCode
	 * @param inputEle
	 */
	private void setOrgHeaderAttributes(Row row, String sOrganizationCode, String sParentOrganizationCode,
			YFCElement inputEle) {
		setAttribute(inputEle, sOrganizationCode, TelstraConstants.ORGANIZATION_CODE);
		setAttribute(inputEle, sOrganizationCode, TelstraConstants.ORGANIZATION_KEY);
		setAttribute(inputEle, row.getCellValue(1), TelstraConstants.ORGANIZATION_NAME);
		setAttribute(inputEle, sParentOrganizationCode, TelstraConstants.PARENT_ORGANIZATION_CODE);
		setAttribute(inputEle, row.getCellValue(3), TelstraConstants.PRIMARY_ENTERPRISE_KEY);
	}

	/**
	 * @param row
	 * @param inputEle
	 */
	private void setAddressAttributes(Row row, YFCElement inputEle) {
		setAttribute(inputEle, row.getCellValue(14), "CorporatePersonInfo@FirstName");
		setAttribute(inputEle, row.getCellValue(15), "CorporatePersonInfo@AddressLine1");
		setAttribute(inputEle, row.getCellValue(16), "CorporatePersonInfo@AddressLine2");
		setAttribute(inputEle, row.getCellValue(17), "CorporatePersonInfo@AddressLine3");
		setAttribute(inputEle, row.getCellValue(18), "CorporatePersonInfo@City");
		setAttribute(inputEle, row.getCellValue(19), "CorporatePersonInfo@State");
		setAttribute(inputEle, row.getCellValue(20), "CorporatePersonInfo@ZipCode");
		setAttribute(inputEle, row.getCellValue(21), "CorporatePersonInfo@Country");
		setAttribute(inputEle, row.getCellValue(22), "CorporatePersonInfo@DayPhone");
		setAttribute(inputEle, row.getCellValue(23), "CorporatePersonInfo@HttpUrl");
		
		setAttribute(inputEle, row.getCellValue(14), "BillingPersonInfo@FirstName");
		setAttribute(inputEle, row.getCellValue(15), "BillingPersonInfo@AddressLine1");
		setAttribute(inputEle, row.getCellValue(16), "BillingPersonInfo@AddressLine2");
		setAttribute(inputEle, row.getCellValue(17), "BillingPersonInfo@AddressLine3");
		setAttribute(inputEle, row.getCellValue(18), "BillingPersonInfo@City");
		setAttribute(inputEle, row.getCellValue(19), "BillingPersonInfo@State");
		setAttribute(inputEle, row.getCellValue(20), "BillingPersonInfo@ZipCode");
		setAttribute(inputEle, row.getCellValue(21), "BillingPersonInfo@Country");
		setAttribute(inputEle, row.getCellValue(22), "BillingPersonInfo@DayPhone");
		setAttribute(inputEle, row.getCellValue(23), "BillingPersonInfo@HttpUrl");
		
		setAttribute(inputEle, row.getCellValue(24), TelstraConstants.LOCALE_CODE);
	}

	/**
	 * @param row
	 * @param inputEle
	 */
	private void setNodeAttributes(Row row, YFCElement inputEle) {
		setAttribute(inputEle, "NODE", ORG_ROLE_LIST_ORG_ROLE_ROLE_KEY,true);
		setAttribute(inputEle, row.getCellValue(11), "Node@NodeType");
		setAttribute(inputEle, "LOW", "Node@AgentCriteriaGroup");
		if("Y".equalsIgnoreCase(row.getCellValue(12))){
			setAttribute(inputEle, "Y", "Node@InventoryTracked");
			setAttribute(inputEle, "Y", TelstraConstants.INVENTORY_PUBLISHED);
			setAttribute(inputEle, TelstraConstants.ORG_TELSTRA, "InventoryOrganizationCode");
		}
		  
		setAttribute(inputEle, "1", "Node@MaxDaysToScheduleBefore");
		setAttribute(inputEle, TelstraConstants.ORG_TELSTRA_SCLSA, TelstraConstants.STORE_CONFIG_ORGANIZATION_CODE);
	}

	/**
	 * @param inputEle
	 */
	private void setBuyerAttributes(YFCElement inputEle) {
		setAttribute(inputEle, "BUYER", ORG_ROLE_LIST_ORG_ROLE_ROLE_KEY,true);
		
		setAttribute(inputEle, TelstraConstants.ORG_TELSTRA_SCLSA, TelstraConstants.CATALOG_ORGANIZATION_CODE);
		setAttribute(inputEle, TelstraConstants.ORG_TELSTRA_SCLSA, TelstraConstants.CAPACITY_ORGANIZATION_CODE);
		setAttribute(inputEle, TelstraConstants.ORG_TELSTRA_SCLSA, TelstraConstants.PRICING_ORGANIZATION_CODE);
		setAttribute(inputEle, TelstraConstants.ORG_TELSTRA_SCLSA, TelstraConstants.STORE_CONFIG_ORGANIZATION_CODE);
	}

	/**
	 * @param inputEle
	 */
	private void setSellerAttributes(YFCElement inputEle) {
		setAttribute(inputEle, "SELLER", ORG_ROLE_LIST_ORG_ROLE_ROLE_KEY,true);
		
		setAttribute(inputEle, TelstraConstants.ORG_TELSTRA_SCLSA, TelstraConstants.CATALOG_ORGANIZATION_CODE);
		setAttribute(inputEle, TelstraConstants.ORG_TELSTRA_SCLSA, TelstraConstants.CAPACITY_ORGANIZATION_CODE);
		setAttribute(inputEle, TelstraConstants.ORG_TELSTRA_SCLSA, TelstraConstants.PRICING_ORGANIZATION_CODE);
		setAttribute(inputEle, TelstraConstants.ORG_TELSTRA_SCLSA, TelstraConstants.STORE_CONFIG_ORGANIZATION_CODE);
	}

	/**
	 * @param row
	 * @param sOrganizationCode
	 * @param inputEle
	 * @return
	 */
	private void setEnterpriseAttributes(Row row, String sOrganizationCode, YFCElement inputEle) {
		setAttribute(inputEle, "ENTERPRISE", ORG_ROLE_LIST_ORG_ROLE_ROLE_KEY,true);
		setAttribute(inputEle, "Y", "IsLegalEntity");
		setAttribute(inputEle, "Y", "RequiresChainedOrder");
		setAttribute(inputEle, sOrganizationCode, TelstraConstants.PRIMARY_ENTERPRISE_KEY);

		if("Y".equalsIgnoreCase(row.getCellValue(12))){
			setAttribute(inputEle, "Y", TelstraConstants.INVENTORY_PUBLISHED);
			setAttribute(inputEle, "Y", "HasOwnInventory");
			setAttribute(inputEle, sOrganizationCode, "InventoryOrganizationCode");
		}else {
			setAttribute(inputEle, "N", TelstraConstants.INVENTORY_PUBLISHED);
			setAttribute(inputEle, "N", "HasOwnInventory");
		}
		
		
		setAttribute(inputEle, TelstraConstants.ORG_TELSTRA_SCLSA, TelstraConstants.CATALOG_ORGANIZATION_CODE);
		setAttribute(inputEle, TelstraConstants.ORG_TELSTRA_SCLSA, TelstraConstants.CAPACITY_ORGANIZATION_CODE);
		setAttribute(inputEle, TelstraConstants.ORG_TELSTRA_SCLSA, TelstraConstants.CUSTOMER_MASTER_ORGANIZATION_CODE);
		setAttribute(inputEle, TelstraConstants.ORG_TELSTRA_SCLSA, TelstraConstants.PRICING_ORGANIZATION_CODE);
		setAttribute(inputEle, TelstraConstants.ORG_TELSTRA_SCLSA, TelstraConstants.STORE_CONFIG_ORGANIZATION_CODE);
		setAttribute(inputEle, TelstraConstants.ORG_TELSTRA_SCLSA, TelstraConstants.INHERIT_CONFIG_FROM_ENTERPRISE);
	}

	private void createAtpRules(String orgCode) {
		YFCDocument createAtpRulesInputDoc = YFCDocument.getDocumentFor("<AtpRules />");
		YFCElement createAtpRulesInputEle = createAtpRulesInputDoc.getDocumentElement();
		
		String defaultAtpDays = "730";
		String orgATPPostFix = "_ATP";
		
		createAtpRulesInputEle.setAttribute("AccumulationDays", defaultAtpDays);
		createAtpRulesInputEle.setAttribute("AdvanceNotificationTime", "0");
		createAtpRulesInputEle.setAttribute("AtpRule", orgCode+orgATPPostFix);
		createAtpRulesInputEle.setAttribute("AtpRuleKey", orgCode+orgATPPostFix);
		createAtpRulesInputEle.setAttribute("AtpRuleName", orgCode+orgATPPostFix);
		createAtpRulesInputEle.setAttribute("BackwardConsumptionDays", defaultAtpDays);
		createAtpRulesInputEle.setAttribute("CallingOrganizationCode", orgCode);
		createAtpRulesInputEle.setAttribute("ConsiderPoForAlloc", "N");
		createAtpRulesInputEle.setAttribute("ForwardConsumptionDays", defaultAtpDays);
		createAtpRulesInputEle.setAttribute("MaxInventoryHoldDays", defaultAtpDays);
		createAtpRulesInputEle.setAttribute("ModifyFlag", "Y");
		createAtpRulesInputEle.setAttribute(TelstraConstants.ORGANIZATION_CODE, orgCode);
		createAtpRulesInputEle.setAttribute("PastDueDemandDays", defaultAtpDays);
		createAtpRulesInputEle.setAttribute("PastDueSupplyDays", defaultAtpDays);
		createAtpRulesInputEle.setAttribute("ProcessingTime", "0");
		
		debug("createAtpRulesInputDoc : " + createAtpRulesInputDoc);
		getServiceInvoker().invokeEntityApi("manageAtpRules", createAtpRulesInputDoc, null);	
		
		manageRuleForDefaultATP(orgCode,orgCode+orgATPPostFix);
		
	}

	private void manageRuleForDefaultATP(String orgCode, String atpRuleName) {
		YFCDocument manageRulesDoc = YFCDocument.getDocumentFor("<Rules />");
		YFCElement eRules = manageRulesDoc.getDocumentElement();

		eRules.setAttribute(TelstraConstants.ORGANIZATION_CODE, orgCode);
		eRules.setAttribute("RuleSetFieldDescription", orgCode+"_ATP_RULE");
		eRules.setAttribute("RuleSetFieldName", "ATP_RULE");
		eRules.setAttribute("RuleSetValue", atpRuleName);
		
		debug("manageRule API input : " + manageRulesDoc);
		getServiceInvoker().invokeYantraApi("manageRule", manageRulesDoc);
		
	}

	private void markConsumableOrganization(String sOrganizationKey) {
		YFCDocument inputForMakeConsumableInventoryOrg = YFCDocument.getDocumentFor("<Organization />");
		YFCElement inputForMakeConsumableInventoryOrgEle = inputForMakeConsumableInventoryOrg.getDocumentElement();
		inputForMakeConsumableInventoryOrgEle.setAttribute(TelstraConstants.ORGANIZATION_CODE, TelstraConstants.ORG_TELSTRA);
		setAttribute(inputForMakeConsumableInventoryOrgEle,  sOrganizationKey, "ConsumableOrganizationList/InventoryOrgRelationship@ConsumableInventoryOrg");
		if(!isConsumableInventoryOrgForTELSTRA(sOrganizationKey)){
			debug("inputForMakeConsumableInventoryOrg: " + inputForMakeConsumableInventoryOrgEle);
			getServiceInvoker().invokeYantraApi("manageOrganizationHierarchy", inputForMakeConsumableInventoryOrg);
		}
		
	}

	private boolean isConsumableInventoryOrgForTELSTRA(String sOrganizationKey) {
		String inputStringForGetOrganizationList = ""
				+ "<Organization OrganizationCode='"+TelstraConstants.ORG_TELSTRA+"'>"
				+ "		<ConsumableOrganizationList>"
				+ "			<InventoryOrgRelationship ConsumableInventoryOrg='"+sOrganizationKey+"'/>"
				+ "		</ConsumableOrganizationList>"
				+ "</Organization>";
		
		String templateForgetOrgList=""
				+ "<OrganizationList>"
				+ "		<Organization OrganizationCode=''>"
				+ "			<ConsumableOrganizationList>"
				+ "				<InventoryOrgRelationship ConsumableInventoryOrg=''/>"
				+ "			</ConsumableOrganizationList>"
				+ "		</Organization>"
				+ "</OrganizationList>";

		YFCDocument inDocGetOrganizationList = YFCDocument.getDocumentFor(inputStringForGetOrganizationList);
		YFCDocument outputGetOrganizationList = getServiceInvoker().invokeYantraApi("getOrganizationList", inDocGetOrganizationList, YFCDocument.getDocumentFor(templateForgetOrgList));
		YFCElement sOrganization = outputGetOrganizationList.getDocumentElement().getChildElement("Organization");
		if(!YFCElement.isNull(sOrganization)){
			return true;
		}
		return false;
	}


	private void createDefaultVendorNode(YFCElement inputEle, String vSendsCommitment, String vSendsAsn,String sOrganizationName) {
		createVendorOrg(inputEle);
		manageVendor(inputEle.getAttribute(TelstraConstants.ORGANIZATION_CODE),vSendsCommitment,vSendsAsn,sOrganizationName);
	}

	private void createVendorOrg(YFCElement orgInputEle) {
		cat.beginTimer("createVendorOrg");
		YFCDocument inDocForCreateVendorOrg = YFCDocument.getDocumentFor("<Organization />");
		YFCElement inEleForCreateVendorOrg = inDocForCreateVendorOrg.getDocumentElement();
		
		inEleForCreateVendorOrg.setAttribute(TelstraConstants.ORGANIZATION_KEY,  orgInputEle.getAttribute(TelstraConstants.ORGANIZATION_KEY)+"_N1");
		inEleForCreateVendorOrg.setAttribute(TelstraConstants.ORGANIZATION_CODE, orgInputEle.getAttribute(TelstraConstants.ORGANIZATION_CODE)+"_N1");
		inEleForCreateVendorOrg.setAttribute(TelstraConstants.ORGANIZATION_NAME, orgInputEle.getAttribute(TelstraConstants.ORGANIZATION_NAME).trim()+" Default Node");
		inEleForCreateVendorOrg.setAttribute(TelstraConstants.LOCALE_CODE, 		 orgInputEle.getAttribute(TelstraConstants.LOCALE_CODE));
		
		setAttribute(inEleForCreateVendorOrg, "NODE", ORG_ROLE_LIST_ORG_ROLE_ROLE_KEY,true);
		
		setAttribute(inEleForCreateVendorOrg, "VN", "Node@NodeType");
		setAttribute(inEleForCreateVendorOrg, "LOW", "Node@AgentCriteriaGroup");
		setAttribute(inEleForCreateVendorOrg, "N", "Node@InventoryTracked");
		setAttribute(inEleForCreateVendorOrg, orgInputEle.getAttribute(TelstraConstants.ORGANIZATION_CODE), TelstraConstants.PARENT_ORGANIZATION_CODE);
		setAttribute(inEleForCreateVendorOrg, orgInputEle.getAttribute(TelstraConstants.ORGANIZATION_KEY), TelstraConstants.PRIMARY_ENTERPRISE_KEY);
		
		inEleForCreateVendorOrg.importNode(orgInputEle.getChildElement("BillingPersonInfo",true).cloneNode(true));
		inEleForCreateVendorOrg.importNode(orgInputEle.getChildElement("CorporatePersonInfo").cloneNode(true));
		
		debug("inEleForCreateVendorOrg : \n " + inEleForCreateVendorOrg);
		getServiceInvoker().invokeYantraApi("manageOrganizationHierarchy", inDocForCreateVendorOrg);	
		cat.endTimer("createVendorOrg");
	}

	private void manageVendor(String sVendorID, String vSendsCommitment, String vSendsAsn,String sOrganizationName) {
		cat.beginTimer("manageVendor");
		YFCDocument manageVendorInputDoc = YFCDocument.getDocumentFor("<Vendor />");
		YFCElement manageVendorInputEle = manageVendorInputDoc.getDocumentElement();
		manageVendorInputEle.setAttribute("CallingOrganizationCode", TelstraConstants.ORG_TELSTRA_SCLSA);
		manageVendorInputEle.setAttribute("CommitmentTime", "24");
		manageVendorInputEle.setAttribute(TelstraConstants.ORGANIZATION_CODE, TelstraConstants.ORG_TELSTRA_SCLSA);
		manageVendorInputEle.setAttribute("SellerOrganization", "Y");
		manageVendorInputEle.setAttribute("SellerOrganizationCode", sVendorID);
		manageVendorInputEle.setAttribute("SendsCommitment", vSendsCommitment);
		manageVendorInputEle.setAttribute("SendsAsn", vSendsAsn);
		manageVendorInputEle.setAttribute("SendsFuncAck", "N");
		manageVendorInputEle.setAttribute("VendorID", sVendorID);	
		
		//HUB 7347 -[START]
		//Adding the ExtnVendorName in the extended column in YFS_VENDOR table;
		
		if(!YFCObject.isNull(sOrganizationName)){
		manageVendorInputEle.createChild(TelstraConstants.EXTN).setAttribute("ExtnVendorName", sOrganizationName);
		}
		
		//HUB 7347 -[END]
		
		debug("manageVendorInput : \n " + manageVendorInputEle);
		getServiceInvoker().invokeYantraApi("manageVendor", manageVendorInputDoc);
		cat.endTimer("manageVendor");
	}

	private  void setAttribute(YFCElement inputEle, String cellvalue,String xpath) {
		setAttribute( inputEle,  cellvalue, xpath,false);
	}
	
	private  void setAttribute(YFCElement inputEle, String cellvalue,String xpath,boolean alwaysCreateNewElement) {
		
		if(xpath.contains("/")){
			int firstEleIndex = xpath.indexOf('/');
			String firstEle=xpath.substring(0, firstEleIndex);
			YFCElement childElement = inputEle.getChildElement(firstEle, true);
			setAttribute(childElement, cellvalue, xpath.substring(firstEleIndex+1),alwaysCreateNewElement);
			
		}else if (xpath.contains("@"))
		{ 
			String[] xpathlast = xpath.split("@");
			String ele = xpathlast[0];			
			String atribute = xpathlast[1];
			YFCElement childElement = null;
			if(alwaysCreateNewElement){
				childElement = inputEle.createChild(ele);
			}
			else{
				childElement = inputEle.getChildElement(ele, true);
			}
			
			childElement.setAttribute(atribute, cellvalue);
		}
		else
		{
			inputEle.setAttribute(xpath, cellvalue);
		}
	}
	
	private void debug(String string) {
		cat.debug(string);
	}

}

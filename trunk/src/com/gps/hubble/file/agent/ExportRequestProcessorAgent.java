package com.gps.hubble.file.agent;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Document;

import com.bridge.sterling.framework.agent.AbstractCustomBaseAgent;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.sterlingcommerce.woodstock.util.Base64;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.date.YDate;
import com.yantra.yfc.date.YTimestamp;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;


/**
 * Class description goes here.
 *
 * @version 1.0 14 June 2016
 * @author VGupta
 *
 */

public class ExportRequestProcessorAgent extends AbstractCustomBaseAgent
{

	private static YFCLogCategory cat = YFCLogCategory.instance(ExportRequestProcessorAgent.class);

	public List<YFCDocument> getJobs(YFCDocument inXML, YFCDocument lastMessage) 
	{
		LoggerUtil.startComponentLog(cat, this.getClass().getName(), "getJobs", inXML);
		debug("inXML : " + inXML);
		List<YFCDocument> aList = new ArrayList<YFCDocument>();

		YFCElement eleMessageXML = inXML.getDocumentElement();
		String sExportType = eleMessageXML.getAttribute("EXPORT_TYPE");
		String serviceName = eleMessageXML.getAttribute(TelstraConstants.SERVICE_NAME);
		String sExportTypeDesc = "";

		if (YFCUtils.isVoid(sExportType))
		{
			sExportType = eleMessageXML.getAttribute(TelstraConstants.EXPORT_TYPE);
			if (YFCUtils.isVoid(sExportType))
			{
				throw new YFSException("Agent Params Configuration Missing",
						"Agent Params Configuration Missing",
						"EXPORT_TYPE or ExportType configuration is required");
			}
		}
		String lastExportReqKey = null;
		if(lastMessage != null){
			lastExportReqKey = lastMessage.getDocumentElement().getAttribute("ExportRequestKey");
		}
		YFCDocument commonCodeListInpDoc = YFCDocument.createDocument(TelstraConstants.COMMON_CODE);
		commonCodeListInpDoc.getDocumentElement().setAttribute(TelstraConstants.CODE_TYPE, "EXPORT_TYPE");
		commonCodeListInpDoc.getDocumentElement().setAttribute(TelstraConstants.CODE_VALUE,sExportType);
		
		YFCDocument commonCodeListOutput = invokeYantraApi(TelstraConstants.API_GET_COMMON_CODE_LIST, commonCodeListInpDoc);
		YFCNodeList<YFCElement> commonCodesOutput = commonCodeListOutput.getElementsByTagName(TelstraConstants.COMMON_CODE);
		
		if(commonCodesOutput != null && commonCodesOutput.getLength() > 0) {
			YFCElement commonCodeElem = commonCodesOutput.item(0);
			if(YFCUtils.isVoid(serviceName)) {
				
				serviceName = commonCodeElem.getAttribute(TelstraConstants.CODE_LONG_DESCRIPTION);
			}
			sExportTypeDesc = commonCodeElem.getAttribute(TelstraConstants.CODE_SHORT_DESCRIPTION);
			
		} else {
			throw new YFSException("Invalid Export Type",
					"Agent Params Configuration Missing",
					"EXPORT_TYPE or ExportType configuration is required");			
		}
		
		YFCDocument yfcFileListDoc = getRequestsToProcess(sExportType,lastExportReqKey);
		YFCIterable<YFCElement> yfcFileListEle = yfcFileListDoc.getDocumentElement().getChildren();
		for(YFCElement eExportReq:yfcFileListEle){
			eExportReq.setAttribute(TelstraConstants.EXPORT_TYPE, sExportType);
			eExportReq.setAttribute(TelstraConstants.SERVICE_NAME, serviceName);
			eExportReq.setAttribute("ExportTypeDescription", sExportTypeDesc);
			aList.add((YFCDocument.getDocumentFor(eExportReq.getString())));
		}
		debug("getJobs size is : " + aList.size());
		LoggerUtil.endComponentLog(cat, this.getClass().getName(), "getJobs", aList);
		return aList;
	}

	@Override
	public void executeJob(YFCDocument yfcIndoc)  {
		LoggerUtil.startComponentLog(cat, this.getClass().getName(), "executeJob", yfcIndoc);
		try 
		{
			YFCElement yfcRootEle = yfcIndoc.getDocumentElement();
			startProcess(yfcRootEle);
			debug("Input to execute jobs : " + yfcRootEle.toString());
			String strExportCrit = yfcRootEle.getAttribute(TelstraConstants.EXPORT_CRITERIA);
			String strExportRequestKey = yfcRootEle.getAttribute("ExportRequestKey");
			String strExportType = yfcRootEle.getAttribute(TelstraConstants.EXPORT_TYPE);
			
			String strServiceName = yfcRootEle.getAttribute(TelstraConstants.SERVICE_NAME);
			YFCDocument serviceInDoc = YFCDocument.createDocument("ExportRequest");
			serviceInDoc.getDocumentElement().setAttribute(TelstraConstants.EXPORT_TYPE,strExportType);
			serviceInDoc.getDocumentElement().setAttribute(TelstraConstants.EXPORT_CRITERIA, strExportCrit);
			YFCDocument outDoc = invokeYantraService(strServiceName, serviceInDoc);
			updateExportRequest(yfcRootEle,outDoc);
			//updateFileUpload(yfcRootEle,excelValidationFailed);
		}
		catch (Exception exp)
		{
			cat.error(exp);
			YFCException yfcException = new YFCException(exp);
			throw yfcException;
		}
		LoggerUtil.endComponentLog(cat, this.getClass().getName(), "executeJob", yfcIndoc);

	}

	// HUB-5951 Start


	

	@Override
	protected String getLockParameters(Document inMessage) {
		YFCDocument yfcDoc = YFCDocument.getDocumentFor(inMessage);
		String transactionType = yfcDoc.getDocumentElement().getAttribute("TRANSACTION_TYPE");
		return "TRANSACTION_TYPE="+transactionType;
	}
	
	private void startProcess(YFCElement yfcRootEle) throws IOException {
		YFCDocument yfcChangeInDoc = YFCDocument.getDocumentFor("<ExportRequest/>");
		YFCElement yfcExportRequestEle = yfcChangeInDoc.getDocumentElement();
		
		String strExportReqKey = yfcRootEle.getAttribute("ExportRequestKey");
		yfcExportRequestEle.setAttribute("ExportRequestKey", strExportReqKey);
		yfcExportRequestEle.setAttribute("Status", "Started Processing");
		yfcExportRequestEle.setAttribute("FileProgressed", "-1");			
		invokeYantraService("GpsChangeExportRequest", yfcChangeInDoc);
	}	

	/**
	 * @param yfcRootEle	
	 * @param processExcel
	 * @return
	 * @throws IOException
	 */
	private void updateExportRequest(YFCElement yfcRootEle,YFCDocument outDoc) {
		YFCDocument yfcChangeInDoc = YFCDocument.getDocumentFor("<ExportRequest/>");
		YFCElement yfcExportRequestEle = yfcChangeInDoc.getDocumentElement();
		
		String csvfile = convertXMLtoCSV(outDoc.getDocumentElement());
		String strExportReqKey = yfcRootEle.getAttribute("ExportRequestKey");
		byte[] encodedCSV = null;
		yfcExportRequestEle.setAttribute("ExportRequestKey", strExportReqKey);
			yfcExportRequestEle.setAttribute("ProcessedFlag", "Y");
			yfcExportRequestEle.setAttribute("Status", "Completed");
			yfcExportRequestEle.setAttribute("FileProgressed", "-1");
			if(YFCUtils.isVoid(csvfile)) {
				csvfile = new String(" No Results Found");
			}
			encodedCSV = Base64.encode(csvfile.getBytes());
			yfcExportRequestEle.setAttribute("ExportFile", new String(encodedCSV));
			invokeYantraService("GpsChangeExportRequest", yfcChangeInDoc);
			raiseNotification(yfcRootEle,strExportReqKey);
	}
	
	private void raiseNotification(YFCElement elemExportRequest, String exportReqKey) {

		String sCreateUserId = elemExportRequest.getAttribute(TelstraConstants.CREATE_USER_ID);
		String sTransactionType = elemExportRequest.getAttribute("ExportType");
		YTimestamp yDateCreateTS = elemExportRequest.getYTimestampAttribute(TelstraConstants.CREATETS);
		String sUserName = elemExportRequest.getAttribute(TelstraConstants.USER_NAME);
		String sCreateTS = yDateCreateTS.getString(TelstraConstants.DATE_FORMAT_DD_MMM_YY_TIME);
		String strExportTypeDesc = elemExportRequest.getAttribute("ExportTypeDescription");
		String sCodeValue = "SUCCESS";
		/*if(bIsExcelHasError){
			sCodeValue = "FAILURE";
		}*/
		if(YFCUtils.isVoid(sUserName)) {
			sUserName = sCreateUserId;
		}
		String sAlertDescription = callCommonCodeList(TelstraConstants.EXPORT_ALERT_TEMPLATE, sCodeValue);
		sAlertDescription = sAlertDescription.replace("${date}", sCreateTS);
		sAlertDescription = sAlertDescription.replace("${Export}", strExportTypeDesc);
		sAlertDescription = sAlertDescription.replace("${Key}", exportReqKey);		
		sAlertDescription = sAlertDescription.replace("${user}", sUserName);
		//sAlertDescription = sAlertDescription.replace("${}", newChar)

		YFCDocument yfcDocCreateExceptionIp = YFCDocument
				.getDocumentFor("<Inbox AssignedToUserId='"+sCreateUserId+"' Description='" + sAlertDescription
						+ "'  ExceptionType='GPS_EXPORT_ALERT'  QueueId='EXPORT_NOTIFICATION'><InboxReferencesList><InboxReferences Name='Export ID' ReferenceType='TEXT' Value='"
						+ exportReqKey + "'/></InboxReferencesList></Inbox>");
		invokeYantraApi(TelstraConstants.API_CREATE_EXCEPTION, yfcDocCreateExceptionIp);
	}
	
	private String callCommonCodeList(String sCodeType , String sCodeValue) {
		YFCDocument yfcDocCommonCodeIp = YFCDocument.getDocumentFor("<CommonCode CodeType='"+sCodeType+"' CodeValue='"+sCodeValue+"'/>");
		YFCDocument yfcDocCommonCodeTemp = YFCDocument.getDocumentFor("<CommonCodeList><CommonCode CodeLongDescription='' CodeType='' CodeValue='' /></CommonCodeList>");
		YFCDocument yfcDocCommonCodeOp = invokeYantraApi(TelstraConstants.API_GET_COMMON_CODE_LIST, yfcDocCommonCodeIp, yfcDocCommonCodeTemp);
		
		String sLongDesc = XPathUtil.getXpathAttribute(yfcDocCommonCodeOp, "//CommonCode/@CodeLongDescription");
		if(YFCCommon.isStringVoid(sLongDesc)){
			sLongDesc = "${Export} was completed successfully on ${date}";
		}
		
		return sLongDesc;
	}


	private String convertXMLtoCSV(YFCElement rootElem) {
		 StringBuilder csvBuilder= new StringBuilder();
		 char newline='\n';
		 StringBuilder title = new StringBuilder();
		 StringBuilder data = new StringBuilder();
		 YFCElement columnListElem = rootElem.getChildElement("ColumnList");
		 Map<String,YFCElement> colArr = new LinkedHashMap<>();
		 for(Iterator<YFCElement> colItr=columnListElem.getChildren();colItr.hasNext();) {
			 YFCElement columnElem = colItr.next();
			 String attrName = columnElem.getAttribute("Name");
			 String attrLabel = columnElem.getAttribute("Label");
			 title.append(attrLabel);
			 //colArr.add(attrName);
			 colArr.put(attrName, columnElem);
			 if(colItr.hasNext()) {
				 title.append(",");
			 }else {
				 title.append(newline);
			 }
		 }
		 YFCElement dataListElem = rootElem.getChildElement("DataList");
		 for(Iterator<YFCElement> itr=dataListElem.getChildren();itr.hasNext();) {
			 YFCElement childElem = itr.next();
			 for(Iterator<String> colItr=colArr.keySet().iterator();colItr.hasNext();) {
				 String columnName = colItr.next();
				 YFCElement columnElem = colArr.get(columnName);
				 if(childElem.hasAttribute(columnName)) {
					 if(columnElem.hasAttribute("EscapeChars") && "Y".equals(columnElem.getAttribute("EscapeChars"))) {
						 data.append("\""+childElem.getAttribute(columnName)+"\"");
					 }else if(columnElem.hasAttribute("IsDate") && "Y".equals(columnElem.getAttribute("IsDate"))) {
						 YDate dateAttr = childElem.getYDateAttribute(columnName);
						 data.append( dateAttr.getString(TelstraConstants.DATE_FORMAT_DD_MM_YY));
					 }else {
						 data.append(childElem.getAttribute(columnName));
					 }
					 
				 }else {
					 data.append("NA");
				 }
				 if(colItr.hasNext()) {
					 data.append(",");
				 }else {
					 data.append(newline);
				 }
				 
			 }
			 /*Map <String,String> attrMap = childElem.getAttributes(false);
			 for(Iterator<Map.Entry<String, String>> entryItr=attrMap.entrySet().iterator();entryItr.hasNext();) {
				 Map.Entry<String, String> attr = entryItr.next();
				 data.append(attr.getValue());
				 if(entryItr.hasNext()) {
					 data.append(",");
				 }else {
					 data.append(newline);
				 }
			 }*/
		 }
		 if(!YFCUtils.isVoid(data.toString())) {
			 csvBuilder.append(title.toString());
			 csvBuilder.append(data.toString());
		 }
		 return csvBuilder.toString();
	}
	


	/**
	 * The method calls the extended database Get List API to get a list of
	 * all the files that are yet to be processed for a particular process
	 * passed in the argument
	 */
	protected YFCDocument getRequestsToProcess(String strProcess, String exportReqKey){
		YFCDocument yfcGetFileListInDoc = YFCDocument
				.getDocumentFor("<ExportRequest ExportType=\"" + strProcess + "\" ProcessedFlag=\"N\"/>");
		if(!YFCObject.isVoid(exportReqKey)){
			yfcGetFileListInDoc.getDocumentElement().setAttribute("ExportRequestKey", exportReqKey);
			yfcGetFileListInDoc.getDocumentElement().setAttribute("ExportRequestKeyQryType", "GT");
		}
		debug("GpsGetExportRequestList : " + yfcGetFileListInDoc.getString());
		return invokeYantraService("GpsGetExportRequestList", yfcGetFileListInDoc);
	}


	private void setTemplates(YFSEnvironment env){
		env.setApiTemplate("createFileUploadRow", getTemplateForCreateFileUploadRow().getDocument());
		env.setApiTemplate("getFileUploadRow", getTemplateForGetFileUploadRow().getDocument());
	}



	private YFCDocument getTemplateForGetFileUploadRow(){
		return YFCDocument.getDocumentFor("<FileUploadRow FileUploadRowKey='' FileUploadKey='' TransactionType='' RowNumber='' />");
	}

	private YFCDocument getTemplateForCreateFileUploadRow(){
		return YFCDocument.getDocumentFor("<FileUploadRow FileUploadRowKey=''/>");
	}

	private void debug(String string) {
		cat.debug(string);
	}

}

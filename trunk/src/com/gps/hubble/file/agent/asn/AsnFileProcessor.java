package com.gps.hubble.file.agent.asn;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.constants.TelstraErrorCodeConstants;
import com.gps.hubble.file.FileProcessor;
import com.gps.hubble.file.Row;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.date.YDate;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSException;

/**
 * Custom API for ASN upload for PO shipments (Excel upload)
 * 
 * @author Karthikeyan Suruliappan
 * @version 1.0
 *
 * Extends AbstractCustomApi Class
 * 
 */


public class AsnFileProcessor extends FileProcessor {

	private static YFCLogCategory logger = YFCLogCategory.instance(AsnFileProcessor.class);

	/*
	 *@row 
	 * */
	@Override
	public void processRow(Row row) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", row);
		uploadASN(row);
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", row);
	}

	/*
	 *This method feeds GpsProcessPOASNMsg service with xml formed from excel row. 
	 * */
	private void uploadASN(Row row) {
		List<YFCDocument> inXmlList = new ArrayList<YFCDocument>();
		inXmlList = formInputXml(row);
		for(YFCDocument inXml : inXmlList) {
		  YFCDocument serviceOutputDoc = getServiceInvoker().invokeYantraService("GpsProcessPOASNMsg", inXml);
	        YFCElement sOutEle = serviceOutputDoc.getDocumentElement();    
	        if("Y".equalsIgnoreCase(sOutEle.getAttribute("ConfirmShip",""))){
	            createAsynRequest(serviceOutputDoc);
	        }
		}
	}

	/*
	 *This method form xml from excel row. 
	 * */
	private List<YFCDocument> formInputXml(Row row) {
	    List<YFCDocument> processPoAsnList = new ArrayList<YFCDocument>();
		String sNo = row.getCellValue(1);		
		String pNo = row.getCellValue(0);
		String cNo = row.getCellValue(2);
		String eDelDate = row.getCellValue(3);
		String sFromLoc = row.getCellValue(4);
		String aShipDate = row.getCellValue(5);
		String pOrdLineNo = row.getCellValue(6);
		String tMetNo = row.getCellValue(7);
		String qty = row.getCellValue(8);

		YFCDocument inXml = YFCDocument.getDocumentFor("<Shipment/>");
		YFCElement sEle = inXml.getDocumentElement();
		sEle.setAttribute("OrderName", pNo);
		sEle.setAttribute("ShipmentNo", sNo);
		//HUB-6566 [Begin]
		if(!YFCCommon.isStringVoid(aShipDate)){
			try{
				sEle.setDateAttribute("ActualShipmentDate", new YDate(aShipDate, "dd/MM/yyyy", true));
			}
			catch (YFCException ex){
				throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.ASN_LOAD_INVALID_DATE_FORMAT_ERROR_CODE,
						new YFSException());
			}
		}
		//HUB-6566 [End]
		sEle.setAttribute("TrackingNo", cNo);
		sEle.createChild("Extn").setAttribute("VendorShipNode", sFromLoc);
		//HUB-6566 [Begin]
		if(!YFCCommon.isStringVoid(eDelDate)){
			try{
				sEle.setDateAttribute("ExpectedShipmentDate", new YDate(eDelDate, "dd/MM/yyyy", true));
			}
			catch (YFCException ex){
				throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.ASN_LOAD_INVALID_DATE_FORMAT_ERROR_CODE,
						new YFSException());
			}
		}
		//HUB-6566 [End]
		
		String shipCompleteFlag = row.getCellValue(9);
        /*
         * If the ShipCompleteFlag passed in the Excel is Y for the row then the entire order must be shipped.
         */
        if(YFCObject.equals(shipCompleteFlag, "Y")) {
          if(!YFCObject.isVoid(tMetNo) || !YFCObject.isVoid(qty) || !YFCObject.isVoid(pOrdLineNo)) {
            throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.ASN_LOAD_QUANTITY_ITEMID_NOT_REQUIRED,
                new YFSException());
          }
          YFCDocument getOrderListOutputDoc = callGetOrderList(pNo);
          processPoAsnList = formInputToCallProcessPoAsnForOrder(sEle, getOrderListOutputDoc);
        } else {
          sEle.setAttribute("ConfirmShip", "N");
          YFCElement sLineEle = sEle.getChildElement("ShipmentLines",true).getChildElement("ShipmentLine",true);
          sLineEle.setAttribute("PrimeLineNo", pOrdLineNo);
          sLineEle.setAttribute("Quantity", qty);
          sLineEle.setAttribute("ItemID", tMetNo);
          processPoAsnList.add(inXml);
        }
		return processPoAsnList;
	}
	
	private List<YFCDocument> formInputToCallProcessPoAsnForOrder(YFCElement sEle, YFCDocument getOrderListOutputDoc) {
	  sEle.setAttribute("ConfirmShip", "Y");
      Map<String, YFCDocument> processPoAsnMap = new HashMap<String, YFCDocument>();
      YFCElement orderEle = getOrderListOutputDoc.getElementsByTagName("Order").item(0);
      YFCNodeList<YFCElement> orderLines = orderEle.getElementsByTagName("OrderLine");
      for(YFCElement orderLine : orderLines) {
          if(processPoAsnMap.containsKey(orderLine.getAttribute("ReceivingNode"))) {
            YFCDocument processPoAsnDoc = processPoAsnMap.get(orderLine.getAttribute("ReceivingNode"));
            YFCNodeList<YFCElement> orderLineStatuses = orderLine.getElementsByTagName("OrderStatus");
            double shipmentLineQty = 0.0;
            for(YFCElement orderLineStatus : orderLineStatuses) {
              if(orderLineStatus.getDoubleAttribute("Status") < 3350 && orderLineStatus.getDoubleAttribute("Status") != 9000) {
                shipmentLineQty = shipmentLineQty + orderLineStatus.getDoubleAttribute("StatusQty");
              }
            }
            if(0.0 == shipmentLineQty) {
              continue;
            }
            YFCElement sLines = processPoAsnDoc.getElementsByTagName("ShipmentLines").item(0);
            YFCElement shipmentLine = sLines.createChild("ShipmentLine");
            YFCElement itemEle = orderLine.getElementsByTagName("Item").item(0);
            shipmentLine.setAttribute("PrimeLineNo", orderLine.getAttribute("PrimeLineNo"));
            shipmentLine.setAttribute("Quantity", shipmentLineQty);
            shipmentLine.setAttribute("ItemID", itemEle.getAttribute("ItemID"));
            shipmentLine.setAttribute("UnitOfMeasure", itemEle.getAttribute("UnitOfMeasure"));
            shipmentLine.setAttribute("ItemDesc", itemEle.getAttribute("ItemDesc"));
            processPoAsnMap.remove(orderLine.getAttribute("ReceivingNode"));
            processPoAsnMap.put(orderLine.getAttribute("ReceivingNode"), processPoAsnDoc);
          } else {
            YFCDocument processPoAsnDoc = YFCDocument.getDocumentFor(sEle.toString());
            YFCElement processPoAsnDocEle = processPoAsnDoc.getDocumentElement();
            YFCElement sLines = processPoAsnDocEle.createChild("ShipmentLines");
            YFCNodeList<YFCElement> orderLineStatuses = orderLine.getElementsByTagName("OrderStatus");
            double shipmentLineQty = 0.0;
            for(YFCElement orderLineStatus : orderLineStatuses) {
              if(orderLineStatus.getDoubleAttribute("Status") < 3350 && orderLineStatus.getDoubleAttribute("Status") != 9000) {
                shipmentLineQty = shipmentLineQty + orderLineStatus.getDoubleAttribute("StatusQty");
              }
            }
            if(0.0 == shipmentLineQty) {
              continue;
            }
            YFCElement itemEle = orderLine.getElementsByTagName("Item").item(0);
            YFCElement shipmentLine = sLines.createChild("ShipmentLine");
            shipmentLine.setAttribute("PrimeLineNo", orderLine.getAttribute("PrimeLineNo"));
            shipmentLine.setAttribute("Quantity", shipmentLineQty);
            shipmentLine.setAttribute("ItemID", itemEle.getAttribute("ItemID"));
            shipmentLine.setAttribute("UnitOfMeasure", itemEle.getAttribute("UnitOfMeasure"));
            shipmentLine.setAttribute("ItemDesc", itemEle.getAttribute("ItemDesc"));
            processPoAsnMap.put(orderLine.getAttribute("ReceivingNode"), processPoAsnDoc);
          }
        }
      
      List<YFCDocument> processPoAsnList = new ArrayList<YFCDocument>();
      if(!YFCObject.isNull(processPoAsnMap) && processPoAsnMap.size() > 0) {
        Set<Entry<String, YFCDocument>> processPoAsnMapSet = processPoAsnMap.entrySet();
        Iterator<Entry<String, YFCDocument>> processPoAsnMapSetItr = processPoAsnMapSet.iterator();
        while (processPoAsnMapSetItr.hasNext()) {
          Map.Entry<String, YFCDocument> processPoAsnMapSetItrEntry = (Map.Entry<String, YFCDocument>) processPoAsnMapSetItr.next();
          YFCDocument processPoAsnDoc = processPoAsnMapSetItrEntry.getValue();
          processPoAsnList.add(processPoAsnDoc);
        }
      }
      return processPoAsnList;
      /*if(!YFCObject.isVoid(getOrderListOutputDoc)) {
        YFCNodeList<YFCElement> orderLines = getOrderListOutputDoc.getElementsByTagName("OrderLine");
       for(YFCElement orderLine : orderLines) {
         YFCNodeList<YFCElement> orderLineStatuses = orderLine.getElementsByTagName("OrderStatus");
         double shipmentLineQty = 0.0;
         for(YFCElement orderLineStatus : orderLineStatuses) {
           if(orderLineStatus.getDoubleAttribute("Status") < 3350 && orderLineStatus.getDoubleAttribute("Status") != 9000) {
             shipmentLineQty = shipmentLineQty + orderLineStatus.getDoubleAttribute("StatusQty");
           }
         }
         YFCElement itemEle = orderLine.getElementsByTagName("Item").item(0);
         YFCElement shipmentLine = sLines.createChild("ShipmentLine");
         shipmentLine.setAttribute("PrimeLineNo", orderLine.getAttribute("PrimeLineNo"));
         shipmentLine.setAttribute("Quantity", shipmentLineQty);
         shipmentLine.setAttribute("ItemID", itemEle.getAttribute("ItemID"));
         shipmentLine.setAttribute("UnitOfMeasure", itemEle.getAttribute("UnitOfMeasure"));
         shipmentLine.setAttribute("ItemDesc", itemEle.getAttribute("ItemDesc"));
       }
      }*/
    }

  /**
	 * 
	 * @param pNo
	 * @return
	 */
	private YFCDocument callGetOrderList(String poNo) {
	  YFCDocument inDocgetOrdList =
	        YFCDocument.getDocumentFor("<Order>" + "<ComplexQuery Operator='OR'><And>"
	            + "<Exp Name='DocumentType' Value='0005' QryType='EQ'/>"
	            + "<Exp Name='OrderName' Value='"+poNo+"' QryType='EQ'/>" 
	            + "</And>" + "</ComplexQuery>" + "</Order>");
	    YFCDocument templateForGetOrdList =
	        YFCDocument.getDocumentFor("<OrderList>"
	            + "<Order DocumentType='' OrderHeaderKey='' OrderNo='' EnterpriseCode='' >" + "<OrderLines>"
	            + "<OrderLine PrimeLineNo='' SubLineNo='' OrderedQty='' ReceivingNode='' >"
	            + "<Item  ItemID='' UnitOfMeasure='' ItemDesc='' /> "
	            + "<OrderStatuses>" + "<OrderStatus OrderReleaseKey='' Status='' StatusQty=''/>"
	            + "</OrderStatuses> " + "</OrderLine>" + "</OrderLines>" + "</Order>" + "</OrderList>");
	    YFCDocument outputGetOrdListForComQry =
	        getServiceInvoker().invokeYantraApi("getOrderList", inDocgetOrdList, templateForGetOrdList);
      return outputGetOrdListForComQry;
    }

  /*
	 *This method createAsynRequest to newly created shipment xml with ConfirmShip flag Y. 
	 * */
	private void createAsynRequest(YFCDocument inXml) {
		YFCDocument asynRequestXml = YFCDocument.getDocumentFor("<CreateAsyncRequest />");
		String message = inXml.getDocumentElement().getString().replace("\\<\\?xml(.+?)\\?\\>", "").trim();
		asynRequestXml.getDocumentElement().setAttribute("ServiceName", "confirmShipment");
		asynRequestXml.getDocumentElement().setAttribute("Message", message);
		asynRequestXml.getDocumentElement().setAttribute(TelstraConstants.INTERFACE_NO, TelstraConstants.ASN_PO_UPLOAD);
		asynRequestXml.getDocumentElement().setAttribute("IsFlow", "N");
		getServiceInvoker().invokeYantraService("GpsCreateAsyncRequest", asynRequestXml);
	}
	
	public static void main(String[] args) {
       YFCDocument test = YFCDocument.getDocumentFor("<Test TestAttr='' />");
       YFCDocument test2 = YFCDocument.getDocumentFor("<Test TestAttr='' />");
       List<YFCDocument> testList = new ArrayList<YFCDocument>();
       testList.add(test);
       testList.add(test2);
       System.out.println("testList: "+testList);
    }
	
}

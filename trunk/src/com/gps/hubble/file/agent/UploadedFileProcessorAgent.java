package com.gps.hubble.file.agent;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.poi.hssf.OldExcelFormatException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.w3c.dom.Document;

import com.bridge.sterling.framework.agent.AbstractCustomBaseAgent;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.file.FileProcessor;
import com.gps.hubble.file.FileProcessorFactory;
import com.gps.hubble.utils.ExcelUtil;
import com.yantra.interop.japi.YIFApi;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.date.YDate;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCConfigurator;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;


/**
 * Class description goes here.
 *
 * @version 1.0 14 June 2016
 * @author VGupta
 *
 */

public class UploadedFileProcessorAgent extends AbstractCustomBaseAgent
{

	private static YFCLogCategory cat = YFCLogCategory.instance(UploadedFileProcessorAgent.class);
	private boolean isExcelFormatValid = true;

	public List<YFCDocument> getJobs(YFCDocument inXML, YFCDocument lastMessage) 
	{
		LoggerUtil.startComponentLog(cat, this.getClass().getName(), "getJobs", inXML);
		debug("inXML : " + inXML);
		List<YFCDocument> aList = new ArrayList<YFCDocument>();

		YFCElement eleMessageXML = inXML.getDocumentElement();
		String sTransactionType = eleMessageXML.getAttribute("TRANSACTION_TYPE");

		if (YFCUtils.isVoid(sTransactionType))
		{
			sTransactionType = eleMessageXML.getAttribute("TransactionType");
			if (YFCUtils.isVoid(sTransactionType))
			{
				throw new YFSException("Agent Params Configuration Missing",
						"Agent Params Configuration Missing",
						"TRANSACTION_TYPE or TransactionType configuration is required");
			}
		}
		String lastFileUploadKey = null;
		if(lastMessage != null){
			lastFileUploadKey = lastMessage.getDocumentElement().getAttribute("FileUploadKey");
		}
		YFCDocument yfcFileListDoc = getFilesToProcess(sTransactionType,lastFileUploadKey);
		YFCIterable<YFCElement> yfcFileListEle = yfcFileListDoc.getDocumentElement().getChildren();
		for(YFCElement eFileupload:yfcFileListEle){
			eFileupload.setAttribute("TransactionType", sTransactionType);
			aList.add((YFCDocument.getDocumentFor(eFileupload.getString())));
		}
		debug("getJobs size is : " + aList.size());
		LoggerUtil.endComponentLog(cat, this.getClass().getName(), "getJobs", aList);
		return aList;
	}

	@Override
	public void executeJob(YFCDocument yfcIndoc)  {
		LoggerUtil.startComponentLog(cat, this.getClass().getName(), "executeJob", yfcIndoc);
		try 
		{
			String sOrganisationKey=null;
			YFCElement yfcRootEle = yfcIndoc.getDocumentElement();
			debug("Input to execute jobs : " + yfcRootEle.toString());
			String strEncodedExcel = yfcRootEle.getAttribute("FileObject");
			String strFileUploadKey = yfcRootEle.getAttribute("FileUploadKey");
			String strTransactionType = yfcRootEle.getAttribute("TransactionType");
			boolean isRowUploaded = yfcRootEle.getBooleanAttribute("RowUploadFlag");
			boolean excelValidationFailed = false;

			// HUB-5951 Start
			if(YFCObject.equals(strTransactionType,"INVENTORY_UPLOAD")){
				String sCreateUserId = yfcRootEle.getAttribute("CreateUserId");
				sOrganisationKey=getOrganizationKey(sCreateUserId);
			}
			// HUB-5951 End

			ExcelInfo excelInfo = new ExcelInfo();
			if (!YFCCommon.isStringVoid(strEncodedExcel))
			{
				if(!isRowUploaded){
					byte[] decodedBytes = Base64.decodeBase64(strEncodedExcel.getBytes());
					List<YFCDocument> fileUploadRows = new ArrayList<>();
					long startExcelProcessTime = System.currentTimeMillis();
					excelValidationFailed = processExcel(decodedBytes, strTransactionType,strFileUploadKey,fileUploadRows,excelInfo,sOrganisationKey);
					long endExcelProcessTime = System.currentTimeMillis();
					debug("Time took to process the excel and populating file upload rows object is "+(endExcelProcessTime - startExcelProcessTime));
					decodedBytes = null;
					yfcRootEle.setAttribute("FileObject", ""); //Setting the file object as emtpy string so that it gets garbage collected
					if(!excelValidationFailed&& isExcelFormatValid){
						strEncodedExcel = null;
						processUploadRowList(fileUploadRows);
					}
				}
				if(!excelValidationFailed && isExcelFormatValid){
					invokeRowProcessorAgent(strTransactionType,strFileUploadKey);
				}
				if(excelValidationFailed || !isExcelFormatValid){
					/*
					 * Below method will update the file upload table with status as invalid tempate
					 */
					updateFileUploadRecord(strFileUploadKey,  "-1",TelstraConstants.INVALID_TEMPLATE, "Y");
					
					String strToEmailIDs = yfcRootEle.getAttribute("NotifyEmail");
					if(!YFCObject.isVoid(strToEmailIDs)){
						String strFromEmailId = YFCConfigurator.getInstance().getProperty("gps.email.from.id", TelstraConstants.EMAIL_FROM_ID);
						String strPort =		YFCConfigurator.getInstance().getProperty("gps.email.port", TelstraConstants.EMAIL_PORT);
						String strHost =		YFCConfigurator.getInstance().getProperty("gps.email.hostname", TelstraConstants.EMAIL_HOST);
						String fileName = strTransactionType + "_" + strFileUploadKey;
						String sHelpdeskContact = getHelpDeskContact();
						ExcelUtil.sendExcelAttachmentEmail(strEncodedExcel.getBytes(), strToEmailIDs, strFromEmailId, fileName, excelInfo.getFileExtension(), strPort, strHost, sHelpdeskContact, true, isExcelFormatValid);
					}else{
						cat.error("No notifiy email id found.");
					}
				}
			}
			else
			{
				cat.info("strEncodedExcel is Null");
			}
			updateFileUpload(yfcRootEle,excelValidationFailed);
		}
		catch (Exception exp)
		{
			cat.error(exp);
			YFCException yfcException = new YFCException(exp);
			throw yfcException;
		}
		LoggerUtil.endComponentLog(cat, this.getClass().getName(), "executeJob", yfcIndoc);

	}

	/*
	 * Below method will update the file upload status with the passed parameter
	 */
	private void updateFileUploadRecord(String sFileUploadKey, String sFileProcessed, String sStatus, String sHasError) {

		YFCDocument yfcDocChangeFileUploadIp = YFCDocument.createDocument(TelstraConstants.FILE_UPLOAD);
		YFCElement yfcEleChangeFileUploadIp = yfcDocChangeFileUploadIp.getDocumentElement();
		yfcEleChangeFileUploadIp.setAttribute(TelstraConstants.FILE_UPLOAD_KEY, sFileUploadKey);
		yfcEleChangeFileUploadIp.setAttribute(TelstraConstants.FILE_PROGRESSED, sFileProcessed);
		yfcEleChangeFileUploadIp.setAttribute(TelstraConstants.STATUS, sStatus);
		yfcEleChangeFileUploadIp.setAttribute(TelstraConstants.HAS_ERROR, sHasError);

		LoggerUtil.verboseLog("RowDataConsolidationService :: updateFileUploadRecord :: yfcDocChangeFileUploadIp", cat, yfcDocChangeFileUploadIp);
		try{
			invokeYantraService(TelstraConstants.SERVICE_GPS_CHANGE_FILE_UPLOAD, yfcDocChangeFileUploadIp);
		}
		catch(Exception e){
			LoggerUtil.errorLog("Something went wrong while updating the status percent", cat, e);
		}	
		
	}

	// HUB-5951 Start
	//Determining the Organisation to whom the user belongs to.
	private String getOrganizationKey(String sCreateUserId) {
		String sOrganizationKey=null;
		if(!YFCObject.isVoid(sCreateUserId)){
			//Determining the Organisation to whom the user belongs to.

			YFCDocument docUserListInput=YFCDocument.getDocumentFor("<User Loginid='" +sCreateUserId+"'/>");
			YFCDocument docUserListTemp=YFCDocument.getDocumentFor("<UserList TotalNumberOfRecords=''><User  OrganizationKey=''  UserKey='' Username='' Loginid='' /></UserList>");
			YFCDocument docUserListOutput = getServiceInvoker().invokeYantraApi("getUserList", docUserListInput, docUserListTemp);
			YFCElement eleUser = docUserListOutput.getElementsByTagName("User").item(0);

			if (!YFCCommon.isVoid(eleUser)) {
				//Determining the ShipNode of the organisation to which the user belonged to.

				sOrganizationKey=eleUser.getAttribute(TelstraConstants.ORGANIZATION_KEY,"");
			}
		}
		return sOrganizationKey;
	}
	// HUB-5951 End


	private String getHelpDeskContact() 
	{
		String strHelpDeskContact = "";
		YFCDocument yfcDocCommonCodeTemplate = YFCDocument.getDocumentFor("<CommonCodeList><CommonCode/></CommonCodeList>");
		YFCDocument yfcDocCommonCodeListIn = YFCDocument.getDocumentFor("<CommonCode CodeType='CONTACT'/>");
		YFCDocument yfcDocCommonCodeListOut = invokeYantraApi(TelstraConstants.API_GET_COMMON_CODE_LIST, 
				yfcDocCommonCodeListIn, yfcDocCommonCodeTemplate); 
		YFCElement yfcEleCommonCodeList = yfcDocCommonCodeListOut.getDocumentElement();
		YFCNodeList<YFCElement> yfcNLCommonCode = yfcEleCommonCodeList.getElementsByTagName("CommonCode");
		for(int i=0; i<yfcNLCommonCode.getLength(); i++)
		{
			YFCElement yfcEleCommonCode = (YFCElement)yfcNLCommonCode.item(i);
			String strCodeValue = yfcEleCommonCode.getAttribute("CodeValue", "");
			String strCode = yfcEleCommonCode.getAttribute("CodeLongDescription", "");
			if(strCodeValue.equalsIgnoreCase("HELP_DESK_EMAIL"))
			{
				if(!strCode.equals(""))
				{
					strHelpDeskContact = strHelpDeskContact + "Email: " + strCode + " ";
				}
			}
			else if(strCodeValue.equalsIgnoreCase("HELP_DESK_PHONE"))
			{
				if(!strCode.equals(""))
				{
					strHelpDeskContact = strHelpDeskContact + "Phone: " + strCode;
				}
			}
		}
		return strHelpDeskContact;
	}

	@Override
	protected String getLockParameters(Document inMessage) {
		YFCDocument yfcDoc = YFCDocument.getDocumentFor(inMessage);
		String transactionType = yfcDoc.getDocumentElement().getAttribute("TRANSACTION_TYPE");
		return "TRANSACTION_TYPE="+transactionType;
	}

	/**
	 * @param yfcRootEle
	 * @param processExcel
	 * @return
	 * @throws IOException
	 */
	private void updateFileUpload(YFCElement yfcRootEle, boolean headerValidationFailed) throws IOException {
		YFCDocument yfcChangeInDoc = YFCDocument.getDocumentFor("<UploadedFile/>");
		YFCElement yfcUploadedFileEle = yfcChangeInDoc.getDocumentElement();
		String strUploadedFileKey = yfcRootEle.getAttribute("FileUploadKey");
		yfcUploadedFileEle.setAttribute("FileUploadKey", strUploadedFileKey);
		if(headerValidationFailed){
			yfcUploadedFileEle.setAttribute("ProcessedFlag", "Y");
		}else{
			yfcUploadedFileEle.setAttribute("RowUploadFlag", "Y");
		}
		invokeYantraService("GpsChangeFileUpload", yfcChangeInDoc);
	}



	/**
	 * The method calls the extended database Get List API to get a list of
	 * all the files that are yet to be processed for a particular process
	 * passed in the argument
	 */
	protected YFCDocument getFilesToProcess(String strProcess, String fileUploadKey){
		YFCDocument yfcGetFileListInDoc = YFCDocument
				.getDocumentFor("<UploadedFile TransactionType=\"" + strProcess + "\" ProcessedFlag=\"N\"/>");
		if(!YFCObject.isVoid(fileUploadKey)){
			yfcGetFileListInDoc.getDocumentElement().setAttribute("FileUploadKey", fileUploadKey);
			yfcGetFileListInDoc.getDocumentElement().setAttribute("FileUploadKeyQryType", "GT");
		}
		debug("GpsGetFileUploadList : " + yfcGetFileListInDoc.getString());
		return invokeYantraService("GpsGetFileUploadList", yfcGetFileListInDoc);
	}

	private boolean processExcel(byte[] decodedBytes, String strTransactionType, String strFileUploadKey, List<YFCDocument> fileUploadRows, ExcelInfo excelInfo, String sOrganisationKey) throws InvalidFormatException, IOException,YIFClientCreationException,SQLException
	{
		cat.debug("creating workbook for "+strTransactionType);
		InputStream is = null;
		boolean validationFailed = false;
		try {
			is = new ByteArrayInputStream(decodedBytes);
			Workbook workbook = WorkbookFactory.create(is);
			debug("Invoking : " + strTransactionType);
			String fileExtension = ".xls";
			if(workbook instanceof org.apache.poi.xssf.usermodel.XSSFWorkbook){
				fileExtension = ".xlsx";
			}
			excelInfo.setFileExtension(fileExtension);
			validationFailed = processExcel(workbook, strTransactionType, strFileUploadKey,fileUploadRows,sOrganisationKey);
			workbook = null;
		} 
		catch (OldExcelFormatException ex){
			cat.error(ex.getMessage());
			isExcelFormatValid = false;
		}
		finally{
			if(is != null){
				is.close();
			}
		}
		return validationFailed;
	}


	private boolean processExcel(Workbook workbook, String strTransactionType, String strFileUploadKey,List<YFCDocument> fileUploadRows, String sOrganisationKey) throws InvalidFormatException, IOException,SQLException{
		cat.debug("processing workbook for transaction type "+strTransactionType);
		Sheet sheet = workbook.getSheetAt(0);
		if(sheet == null){
			cat.error("No sheet found in the excel.");
			return false;
		}
		int count = 0;
		int nullcount = 0;
		FileProcessor processor = FileProcessorFactory.create(strTransactionType, getServiceInvoker());

		int noOfCells = processor.getNumberOfCells(strTransactionType);
		boolean headerValidationFailed = false;
		while(true){
			Row row = sheet.getRow(count);
			if(count == 0){
				if(!isValidateHeader(strTransactionType,row,sOrganisationKey)){
					headerValidationFailed = true;
					break;
				}
			}
			if(row == null){
				if(nullcount > 10){
					break;
				}
				nullcount = nullcount + 1;
			}else if(!ExcelUtil.isEmptyRow(row,noOfCells)){
				if(count == 0 && noOfCells == 0){
					noOfCells = row.getPhysicalNumberOfCells();
				}
				nullcount = 0;
				YFCDocument docFileUploadRow = YFCDocument.createDocument("FileUploadRow"); 
				YFCElement elemFileUploadRow = docFileUploadRow.getDocumentElement();
				elemFileUploadRow.setAttribute("TransactionType", strTransactionType);
                elemFileUploadRow.setAttribute("FileUploadKey", strFileUploadKey);
                elemFileUploadRow.setAttribute("RowNumber", count);
                String rowData = getRowDataObject(row, count,noOfCells,sOrganisationKey,strTransactionType);
                elemFileUploadRow.setAttribute("RowObject", rowData);
                fileUploadRows.add(docFileUploadRow);
			}else{
			}
			count = count + 1;
		}
		return headerValidationFailed;
	}

  private void processUploadRowList(List<YFCDocument> uploadRowList) throws YIFClientCreationException,RemoteException{
		int commitCount = 0;
		//ISCPropertyManagerFactory factory = SCPropertyManagerFactoryService.getInstance().getFactory();
		//ISCPropertyManager manager = factory.getPropertyManager(SCIPropertyManager.class.getName());
		boolean tokenEnabled = YFCConfigurator.getInstance().getBooleanProperty("api.security.token.enabled", true);
		YFCConfigurator.getInstance().setProperty("api.security.token.enabled", "N");
		boolean apiSecurityEnabled = YFCConfigurator.getInstance().getBooleanProperty("api.security.enabled", true);
		YFCConfigurator.getInstance().setProperty("api.security.enabled", "N");
		YFSEnvironment envForInsert = getServiceInvoker().createNewEnvironment();
		envForInsert.setRollbackOnly(false);
		setTemplates(envForInsert);
		YIFApi oApi = YIFClientFactory.getInstance().getLocalApi();
		YFCDocument docFileUploadRows = YFCDocument.createDocument("FileUploadRowList");
		long startTime = System.currentTimeMillis();
		for(YFCDocument uploadRow : uploadRowList){
			if(commitCount == 1000){
				//cat.debug("As commit reached 1000..committing the context and recreating one");
				long endTime = System.currentTimeMillis();
				oApi.executeFlow(envForInsert, "GpsInsertFileUploadRowsService", docFileUploadRows.getDocument());
				debug("it took "+(endTime - startTime)+"ms for inserting 1000 records");
				startTime = System.currentTimeMillis();
				docFileUploadRows = null;
				docFileUploadRows = YFCDocument.createDocument("FileUploadRowList");
				oApi.releaseEnvironment(envForInsert);
				envForInsert = getServiceInvoker().createNewEnvironment();
				envForInsert.setRollbackOnly(false);
				setTemplates(envForInsert);
				commitCount = 0;
			}
			docFileUploadRows.getDocumentElement().importNode(uploadRow.getDocumentElement());
			commitCount = commitCount + 1;
		}
		if(envForInsert != null){
			oApi.executeFlow(envForInsert, "GpsInsertFileUploadRowsService", docFileUploadRows.getDocument());
			oApi.releaseEnvironment(envForInsert);
		}
		if(tokenEnabled){
			YFCConfigurator.getInstance().setProperty("api.security.token.enabled", "Y");
		}else{
			YFCConfigurator.getInstance().setProperty("api.security.token.enabled", "N");
		}
		if(apiSecurityEnabled){
			YFCConfigurator.getInstance().setProperty("api.security.enabled", "Y");
		}else{
			YFCConfigurator.getInstance().setProperty("api.security.enabled", "N");
		}
	}


	private void setTemplates(YFSEnvironment env){
		env.setApiTemplate("createFileUploadRow", getTemplateForCreateFileUploadRow().getDocument());
		env.setApiTemplate("getFileUploadRow", getTemplateForGetFileUploadRow().getDocument());
	}

	private String getRowDataObject(Row row, int rowNumber, int noOfCells ,String sOrganisationKey, String strTransactionType){
      YFCDocument docRowData = YFCDocument.createDocument("Row");
      YFCElement elemRowData = docRowData.getDocumentElement();
      YFCElement elemSheets = elemRowData.createChild("Cells");
      elemRowData.setAttribute("RowNumber", rowNumber);
      //      int noOfCells = row.getPhysicalNumberOfCells();
      for(int i=0;i<noOfCells;i++){
          YFCElement elemSheet = elemSheets.createChild("Cell");
          elemSheet.setAttribute("CellIndex", i);
          Cell cell = ExcelUtil.getCell(row, i);
          Object obj = ExcelUtil.getObjectOfCell(cell);
          if(obj == null){
              continue;
          }
          if(obj instanceof String){
              elemSheet.setAttribute("DataType", "String");
              elemSheet.setAttribute("Value", ((String)obj).toUpperCase());
          }else if(obj instanceof Date){
              elemSheet.setAttribute("DataType", "Date");
              YDate ydate = new YDate(((Date)obj).getTime(), false);
              elemSheet.setAttribute("Value", ydate.getString(TelstraConstants.TELSTRA_DATE_TIME_FORMAT));
          }else{
              elemSheet.setAttribute("DataType", "String");
              elemSheet.setAttribute("Value", ((String)obj).toUpperCase());
          }

      }

      // HUB-5951 Start
      if(YFCObject.equals(strTransactionType,"INVENTORY_UPLOAD")){
          YFCElement elemSheet = elemSheets.createChild("Cell");
          elemSheet.setAttribute("CellIndex", noOfCells);
          elemSheet.setAttribute("DataType", "OrganisationKey");
          elemSheet.setAttribute("Value", sOrganisationKey);
      }
      // HUB-5951 End

      return docRowData.getDocumentElement().toString();
  }

	private void invokeRowProcessorAgent(String transactionType, String fileUploadKey){
		String criteriaId = transactionType + "_ROW";
		debug("invoking row processor agent for criteria id "+criteriaId);
		YFCDocument docTriggerAgent = YFCDocument.createDocument("TriggerAgent");
		YFCElement elemTriggerAgent = docTriggerAgent.getDocumentElement();
		elemTriggerAgent.setAttribute("CriteriaId", criteriaId); //This will be based on transactionType
		YFCElement elemCriteriaAttributes = elemTriggerAgent.createChild("CriteriaAttributes");
		YFCElement elemCriteriaAttributeFileUpload = elemCriteriaAttributes.createChild("Attribute");
		elemCriteriaAttributeFileUpload.setAttribute("Name","FILE_UPLOAD_KEY");
		elemCriteriaAttributeFileUpload.setAttribute("Value",fileUploadKey);
		YFCElement elemCriteriaAttributeTransaction = elemCriteriaAttributes.createChild("Attribute");
		elemCriteriaAttributeTransaction.setAttribute("Name","TRANSACTION_TYPE");
		elemCriteriaAttributeTransaction.setAttribute("Value",transactionType);
		invokeYantraApi("triggerAgent", docTriggerAgent);
	}

	private YFCDocument getTemplateForGetFileUploadRow(){
		return YFCDocument.getDocumentFor("<FileUploadRow FileUploadRowKey='' FileUploadKey='' TransactionType='' RowNumber='' />");
	}

	private YFCDocument getTemplateForCreateFileUploadRow(){
		return YFCDocument.getDocumentFor("<FileUploadRow FileUploadRowKey=''/>");
	}

	private void debug(String string) {
		cat.debug(string);
	}

	private boolean isValidateHeader(String transactionType,Row row, String sOrganisationKey){
		String rowDataObj = getRowDataObject(row, 0, row.getPhysicalNumberOfCells(), sOrganisationKey, transactionType);
		if(YFCObject.isVoid(rowDataObj)){
			return false;
		}
		com.gps.hubble.file.Row excelRowObj = new com.gps.hubble.file.Row(YFCDocument.getDocumentFor(rowDataObj));
		FileProcessor fileProcessor = FileProcessorFactory.create(transactionType, this.getServiceInvoker());
		return fileProcessor.isValidHeader(transactionType, excelRowObj);
	}

	private class ExcelInfo{
		private String fileExtension;

		public String getFileExtension() {
			return fileExtension;
		}

		public void setFileExtension(String fileExtension) {
			this.fileExtension = fileExtension;
		}
	}
}

package com.gps.hubble.file.agent;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.bridge.sterling.framework.agent.AbstractCustomBaseAgent;
import com.bridge.sterling.utils.LoggerUtil;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSException;

public class CloseOrderAgent extends AbstractCustomBaseAgent {
	private static YFCLogCategory logger = YFCLogCategory.instance(CloseOrderAgent.class);

	public List<YFCDocument> getJobs(YFCDocument msgXml, YFCDocument lastMsgXml) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "getJobs", msgXml);
		
		List<YFCDocument> fileList = new ArrayList<>();
		YFCElement eleMsgXml = msgXml.getDocumentElement();
		String sInputDir = eleMsgXml.getAttribute("InputDir");
		String sFunction = eleMsgXml.getAttribute("Function");
		
		if (YFCUtils.isVoid(sInputDir)) {
			throw new YFSException("Agent Params Configuration Missing",
					"Agent Params Configuration Missing",
					"Directories configuration is required");
		}
		
		File fileToBeProcessed = getFileToProcess(sInputDir);
		if (!YFCObject.isNull(fileToBeProcessed)) {
			String sCompletePathWithFileName = fileToBeProcessed.getAbsolutePath();
			logger.info("File Being Read: " + sCompletePathWithFileName);
			//String sFileName = file.getName().substring(0,file.getName().lastIndexOf("."));
			Iterator<Row> rowIterator = null;
			FileInputStream fileIS = null;
			try {
				fileIS = new FileInputStream(fileToBeProcessed);

				if (sCompletePathWithFileName.toUpperCase().endsWith(".XLS")) {
					// Get the workbook instance for XLS file
					HSSFWorkbook workbook = new HSSFWorkbook(fileIS);
					// Get first sheet from the workbook
					HSSFSheet sheet = workbook.getSheetAt(0);
					rowIterator = sheet.iterator();
				} else if (sCompletePathWithFileName.toUpperCase().endsWith(".XLSX")) {
					// Get the workbook instance for XLSX file
					XSSFWorkbook workbook = new XSSFWorkbook(fileIS);
					// Get first sheet from the workbook
					XSSFSheet sheet = workbook.getSheetAt(0);
					rowIterator = sheet.iterator();
				}
				
				while(rowIterator.hasNext()){
					Row row = rowIterator.next();
					YFCDocument yfcRowDoc = parseExcelRow(sFunction, row);
					
					if(!YFCObject.isNull(yfcRowDoc)) {
						fileList.add(yfcRowDoc);
					}
				}
			}
			catch (Exception exp) {
				logger.error(exp);
			} finally {
				if (null != fileIS)
					try {
						fileIS.close();
						if(fileToBeProcessed.delete())
			        		logger.info("file deleted : " + sCompletePathWithFileName);
			        	else 
			        		logger.info("couldnt delete the file : " + sCompletePathWithFileName);
					} catch (IOException ioEx) {
						// TODO Auto-generated catch block
						logger.error(ioEx);
					}
			}
		}
		
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "getJobs", msgXml);
		return fileList;
	}

	private YFCDocument parseExcelRow(String strFunction, Row row) {
		DecimalFormat df = new DecimalFormat("#");
		
		Iterator<Cell> cellIterator;
		ArrayList<String> colValue = new ArrayList<String>();
		
		//For the passed row, iterate through each columns
		cellIterator = row.cellIterator();
		try {
			while(cellIterator.hasNext()) {
				Cell cell = cellIterator.next();
				if(cell == null) {
					//_cat.verbose("The cell is null");
					colValue.add("");
				}
				else {
					switch(cell.getCellType()) {
						case Cell.CELL_TYPE_NUMERIC:
							//_cat.verbose("The cell is numeric");
							colValue.add(String.valueOf(df.format(cell.getNumericCellValue())));
							break;
						case Cell.CELL_TYPE_STRING:
							//_cat.verbose("The cell is string");
							colValue.add(cell.getStringCellValue());
							break;
						case Cell.CELL_TYPE_BLANK:
							//_cat.verbose("The cell is blank");
							colValue.add("");
							break;
					}
				}
			}

			YFCDocument yfcDocCloseOrder = YFCDocument.getDocumentFor("<Order/>");
			YFCElement yfcEleOrder = yfcDocCloseOrder.getDocumentElement();
			
			if(strFunction.equalsIgnoreCase("CLOSE_ORDER")){
				if(colValue.size() == 1) {
					String str1stCol = (colValue.get(0)).trim();
					yfcEleOrder.setAttribute("OrderName", str1stCol);
					yfcEleOrder.setAttribute("Function", strFunction);
				}
				else {
					String str1stCol = (colValue.get(0)).trim();
					String str2ndCol = (colValue.get(1)).trim();
					String str3rdCol = (colValue.get(2)).trim();
					
					yfcEleOrder.setAttribute("OrderName", str1stCol);
					yfcEleOrder.setAttribute("PrimeLineNo", str2ndCol);
					yfcEleOrder.setAttribute("DeliveredQty", str3rdCol);
					yfcEleOrder.setAttribute("Function", strFunction);
				}
			}
			else if(strFunction.equalsIgnoreCase("CHANGE_ORDER")){
				String str1stCol = (colValue.get(0)).trim();
				String str2ndCol = (colValue.get(1)).trim();
				String str3rdCol = (colValue.get(2)).trim();
				
				yfcEleOrder.setAttribute("OrderName", str1stCol);
				yfcEleOrder.setAttribute("PrimeLineNo", str2ndCol);
				yfcEleOrder.setAttribute("OrderedQty", str3rdCol);
				yfcEleOrder.setAttribute("Function", strFunction);
			}
			
			return yfcDocCloseOrder;
		}
		catch(Exception e) {
			return null;
		}
	}

	protected File getFileToProcess(String directory) {
		File dir = new File(directory);
		File[] files = dir.listFiles();
		
		for (File file : files) {
			String sCompletePathWithFileName = file.getAbsolutePath();
			logger.info("File Being Read: " + sCompletePathWithFileName);
			if (sCompletePathWithFileName.toUpperCase().endsWith(".XLS")
					|| sCompletePathWithFileName.toUpperCase().endsWith(".XLSX")) {
				return file;
			}
		}
		return null;
	}
	
	public void executeJob(YFCDocument executeJobXml) {
		try {
			YFCElement executeJobRootEle = executeJobXml.getDocumentElement();
			String strFunction = executeJobRootEle.getAttribute("Function", "CLOSE_ORDER");
			
			if(strFunction.equalsIgnoreCase("CLOSE_ORDER")){
				//invoke service to close the order
				invokeYantraService("GpsCloseOrderDropQ", executeJobXml);
			}
			else if(strFunction.equalsIgnoreCase("CHANGE_ORDER")){
				String strOrderName = executeJobRootEle.getAttribute("OrderName", "");
				String strPrimeLineNo = executeJobRootEle.getAttribute("PrimeLineNo", "0");
				String strOrderedQty = executeJobRootEle.getAttribute("OrderedQty", "");
				
				//find order header key and order line key for the passed order name and line number.
				/*
				 * call get order line list
				 * <OrderLine PrimeLineNo=""><Order OrderName=""/></OrderLine>
				 * 
				 * Output Template:
				 * <OrderLineList TotalNumberOfRecords=""> <OrderLine OrderHeaderKey="" OrderLineKey="" /> </OrderLineList>
				 */
				
				YFCDocument yfcGetOrderLineListInDoc = YFCDocument.getDocumentFor("<OrderLine/>");
				YFCElement yfcOrderLineEle = yfcGetOrderLineListInDoc.getDocumentElement();
				yfcOrderLineEle.setAttribute("PrimeLineNo", strPrimeLineNo);
				YFCElement yfcOrderEle = yfcOrderLineEle.createChild("Order");
				yfcOrderEle.setAttribute("OrderName", strOrderName);
				
				YFCDocument yfcGetOrderLineListTemp = YFCDocument.getDocumentFor("<OrderLineList TotalNumberOfRecords=''>"
						+ "<OrderLine OrderHeaderKey='' OrderLineKey='' /> </OrderLineList>");
				
				YFCDocument yfcGetOrderLineListOutDoc = invokeYantraApi("GetOrderLineList", yfcGetOrderLineListInDoc, yfcGetOrderLineListTemp);
				YFCElement yfcOrderLineListOutDoc = yfcGetOrderLineListOutDoc.getDocumentElement();
				String strTotalRecords = yfcOrderLineListOutDoc.getAttribute("TotalNumberOfRecords");
				if(strTotalRecords.equals("1")){
					YFCElement yfcOrderLineOutDoc = yfcOrderLineListOutDoc.getChildElement("OrderLine");
					String strOrderHeaderKey = yfcOrderLineOutDoc.getAttribute("OrderHeaderKey");
					String strOrderLineKey = yfcOrderLineOutDoc.getAttribute("OrderLineKey");
					
					//call changeOrder
					/*
					 * <Order OrderHeaderKey="">
					 * 	<OrderLines>
					 * 		<OrderLine OrderLineKey="" OrderedQty=""/>
					 * 	</OrderLines>
					 * </Order>
					 */
					YFCDocument yfcChangeOrderInDoc = YFCDocument.getDocumentFor("<Order/>");
					YFCElement yfcChangeOrderRootEle = yfcChangeOrderInDoc.getDocumentElement();
					yfcChangeOrderRootEle.setAttribute("OrderHeaderKey", strOrderHeaderKey);
					YFCElement yfcChangeOrderLinesEle = yfcChangeOrderRootEle.createChild("OrderLines");
					YFCElement yfcChangeOrderLineEle = yfcChangeOrderLinesEle.createChild("OrderLine");
					yfcChangeOrderLineEle.setAttribute("OrderLineKey", strOrderLineKey);
					yfcChangeOrderLineEle.setAttribute("OrderedQty", strOrderedQty);
					
					invokeYantraApi("changeOrder", yfcChangeOrderInDoc);
				}
				else if(strTotalRecords.equals("0")){
					//do nothing
					logger.debug("No lines found for OrderName: " + strOrderName + " PrimeLineNo " + strPrimeLineNo);
				}
				else{
					logger.info("More than 1 lines found for OrderName: " + strOrderName + " PrimeLineNo " + strPrimeLineNo);
				}
			}
		}
		catch (Exception exp) {
			logger.error(exp);
		}
		
	}
}

/***********************************************************************************************
 * File Name        : StockMovementFileProcessor.java
 *
 * Description      : Custom API for validating the field in the Stock movement excel upload
 * 
 * Modification Log :
 * ---------------------------------------------------------------------------------------------
 * Ver #    Date        	  Author                 			 Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0						Prateek Kumar    					Initial Version
 * 1.1		Jan 30,2017		Prateek Kumar						HUB-8372 : picking the unit of measure from order details for the movement type RECEIPT  
 * ---------------------------------------------------------------------------------------------
 * 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
 **********************************************************************************************/
package com.gps.hubble.file.agent.inventory;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.constants.TelstraErrorCodeConstants;
import com.gps.hubble.file.FileProcessor;
import com.gps.hubble.file.Row;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSException;

/**
 * Custom code to upload stock movement report from excel
 * 
 * @author Prateek
 * @version 1.0
 * 
 *          Extends FileProcessor class
 */
public class StockMovementFileProcessor extends FileProcessor {

	private static YFCLogCategory logger = YFCLogCategory.instance(StockMovementFileProcessor.class);
	private String movementType;
	private String orderNo = "";
	private String shipNode = "";
	private String receivingNode = "";
	private String itemId;
	private String unitOfMeasure;
	private String quantity;
	private String costCenter;
	private String activityType;
	private String pmOrder;
	private String pmOperation;
	private YFCDocument docGetOrderListOp = null;
	@Override
	public void processRow(Row row) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", row);
		validateStockMovementReport(row);
		StockMovementOrderCreation obj = new StockMovementOrderCreation();
		obj.manageStockMovementOrder(this, getServiceInvoker());
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", row);

	}

	/**
	 * 
	 * @param row
	 */
	private void validateStockMovementReport(Row row) {

		// fetch and validate the field from the excel row
		validateMovementType(row);
		validateOrderNo(row);
		validateAndGetShipNode(row);
		validateAndGetReceivingNode(row);
		getItemId(row);
		//[HUB-8372] Begin
		callGetOrderList();
		/*
		 * For movement type receipt, pick the unit of measure from order line (as order is already created) instead of from the item details as item may or may not exist in Sterling.
		 */
		if(TelstraConstants.MOVEMENT_TYPE_RECEIPT.equalsIgnoreCase(movementType)){			
			getUnitOfMeasureFromOrder();
		}
		/*
		 * For the other movement type, since the order is not created yet picking the unit of measure from item details
		 */
		else{
			validateItemIdAndGetUnitOfMeasure();
		}//[HUB-8372] End
		validateAndGetQuantity(row);
		validateCostCenter(row);
		validateActivityType(row);
		validatePmOrder(row);
		validatePmOperation(row);
		if (shipNode.equalsIgnoreCase(receivingNode)) {
			throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.STOCK_MOVEMENT_BOTH_NODE_SAME_ERROR_CODE,
					new YFSException());
		}
		if(TelstraConstants.MOVEMENT_TYPE_RECEIPT.equalsIgnoreCase(movementType)){
			validateExistingOrderForReceipt();
		}
		else{
			IsRecordIsUniqueForAGivenOrderNo();
		}
	}
	
	/**
	 * 
	 */
	private void validateExistingOrderForReceipt() {

		if (TelstraConstants.ZERO_STRING
				.equals(docGetOrderListOp.getDocumentElement().getAttribute(TelstraConstants.TOTAL_ORDER_LIST))) {
			throw ExceptionUtil.getYFSException(
					TelstraErrorCodeConstants.STOCK_MOVEMENT_ORDER_NAME_DOESNT_EXIST_ERROR_CODE, new YFSException());
		}		
		
		YFCElement yfcEleOrderLine = XPathUtil.getXPathElement(docGetOrderListOp, "//OrderLine[Item[@ItemID='"+itemId+"'] and @ReceivingNode='"+receivingNode+"' and Extn[@Division='"+costCenter+"']]");

		if(YFCCommon.isVoid(yfcEleOrderLine)){
			throw ExceptionUtil.getYFSException(
					TelstraErrorCodeConstants.STOCK_MOVEMENT_LINE_NOT_FOUND, new YFSException());			
		}
		
		String sActivityType = XPathUtil.getXpathAttribute(docGetOrderListOp,"//Reference[@Name='ACTIVITY_TYPE']/@Value");
		if(!(YFCCommon.isStringVoid(sActivityType) || getActivityType().equals(sActivityType))){
			throw ExceptionUtil.getYFSException(
					TelstraErrorCodeConstants.STOCK_MOVEMENT_ACTIVITY_TYPE_MISMATCH, new YFSException());
		}
		
		String sPmOperation = XPathUtil.getXpathAttribute(docGetOrderListOp,"//Reference[@Name='ACTIVITY_NUMBER']/@Value");
		if(!(YFCCommon.isStringVoid(sPmOperation) || getPmOperation().equals(sPmOperation))){
			throw ExceptionUtil.getYFSException(
					TelstraErrorCodeConstants.STOCK_MOVEMENT_PM_OPERATION_MISMATCH, new YFSException());
		}
		
		String sPmOrder = XPathUtil.getXpathAttribute(docGetOrderListOp, "//Reference[@Name='PM_ORDER']/@Value");
		if(!(YFCCommon.isStringVoid(sPmOrder) || getPmOrder().equals(sPmOrder))){
			throw ExceptionUtil.getYFSException(
					TelstraErrorCodeConstants.STOCK_MOVEMENT_PM_ORDER_MISMATCH, new YFSException());
		}		
	}		
	

	/**
	 * This method is invoke only for movement type receipt. It returns the unit of measure from the order line
	 */
	private void getUnitOfMeasureFromOrder() {
		if (!TelstraConstants.ZERO_STRING
				.equals(docGetOrderListOp.getDocumentElement().getAttribute(TelstraConstants.TOTAL_ORDER_LIST))) {			
			String sUnitOfMeasure = XPathUtil.getXpathAttribute(docGetOrderListOp, "//OrderLine/Item[@ItemID='"+itemId+"']/@UnitOfMeasure");
			setUnitOfMeasure(sUnitOfMeasure);
		}
	}

	/**
	 * 
	 */
	private void IsRecordIsUniqueForAGivenOrderNo() {

		if (!TelstraConstants.ZERO_STRING
				.equals(docGetOrderListOp.getDocumentElement().getAttribute(TelstraConstants.TOTAL_ORDER_LIST))) {
			//HUB-6000 [Begin]
			String sMaxOrderStatus = docGetOrderListOp.getElementsByTagName(TelstraConstants.ORDER).item(0).getAttribute(TelstraConstants.MAX_ORDER_STATUS);

			if(1000 < Double.valueOf(sMaxOrderStatus) && !TelstraConstants.MOVEMENT_TYPE_RECEIPT.equalsIgnoreCase(movementType)){
				throw ExceptionUtil.getYFSException(
						TelstraErrorCodeConstants.STOCK_MOVEMENT_ORDER_NAME_ALREADY_EXIST_ERROR_CODE, new YFSException());
			}
			//HUB-6000 [End]
			String sShipNode = XPathUtil.getXpathAttribute(docGetOrderListOp, "//OrderLine/@ShipNode");
			String sReceivingNode = XPathUtil.getXpathAttribute(docGetOrderListOp, "//OrderLine/@ReceivingNode");
			String sActivityType = XPathUtil.getXpathAttribute(docGetOrderListOp,
					"//Reference[@Name='ACTIVITY_TYPE']/@Value");
			String sPmOperation = XPathUtil.getXpathAttribute(docGetOrderListOp,
					"//Reference[@Name='ACTIVITY_NUMBER']/@Value");
			String sPmOrder = XPathUtil.getXpathAttribute(docGetOrderListOp, "//Reference[@Name='PM_ORDER']/@Value");
//			String sDivision = XPathUtil.getXpathAttribute(docGetOrderListOp, "//Order/@Division");
			String sDivision = XPathUtil.getXpathAttribute(docGetOrderListOp, "//OrderLine/Extn/@Division");

			boolean bThrowException = false;

			if ((TelstraConstants.MOVEMENT_TYPE_ISSUE.equalsIgnoreCase(movementType)
					|| TelstraConstants.MOVEMENT_TYPE_TRANSFER.equalsIgnoreCase(movementType))
					&& !getShipNode().equals(sShipNode)) {
				bThrowException=true;

			} else if ((TelstraConstants.MOVEMENT_TYPE_RECOVERY.equalsIgnoreCase(movementType)
					|| TelstraConstants.MOVEMENT_TYPE_TRANSFER.equalsIgnoreCase(movementType)
					|| TelstraConstants.MOVEMENT_TYPE_RECEIPT.equalsIgnoreCase(movementType))
					&& !getReceivingNode().equals(sReceivingNode)) {
				bThrowException=true;
			}
			if(!bThrowException){

				if(!(YFCCommon.isStringVoid(sActivityType) || getActivityType().equals(sActivityType))){
					bThrowException=true;
				}
				else if(!(YFCCommon.isStringVoid(sPmOperation) || getPmOperation().equals(sPmOperation))){
					bThrowException=true;
				}
				else if(!(YFCCommon.isStringVoid(sPmOrder) || getPmOrder().equals(sPmOrder))){
					bThrowException=true;
				}
				else if(!(YFCCommon.isStringVoid(sDivision) || getCostCenter().equals(sDivision))){
					bThrowException=true;
				}
			}

			if(bThrowException){
				throw ExceptionUtil.getYFSException(
						TelstraErrorCodeConstants.STOCK_MOVEMENT_NON_UNIQUE_RECORD_ERROR_CODE, new YFSException());
			}
		}
		else if(TelstraConstants.MOVEMENT_TYPE_RECEIPT.equalsIgnoreCase(movementType)){
			throw ExceptionUtil.getYFSException(
					TelstraErrorCodeConstants.STOCK_MOVEMENT_ORDER_NAME_DOESNT_EXIST_ERROR_CODE, new YFSException());

		}
	}


	/**
	 * This method calls get order list api with order name as the input and with a specific template.
	 */
	private void callGetOrderList() {

		if(!YFCCommon.isVoid(orderNo)){
			YFCDocument docGetOrderListIp = YFCDocument.getDocumentFor("<Order OrderName='" + orderNo + "'/>");
			LoggerUtil.verboseLog("StockMovementOrder::callGetOrderList:: getOrderListIp", logger,
					docGetOrderListIp);
			YFCDocument docGetOrderListTemp = YFCDocument.getDocumentFor(
					"<OrderList> <Order OrderName='' Division='' MaxOrderStatus=''> <References /> <OrderLines> <OrderLine ShipNode='' ReceivingNode=''> <Item ItemID='' UnitOfMeasure=''> </Item> <Extn/> </OrderLine></OrderLines> </Order> </OrderList>");

			LoggerUtil.verboseLog("StockMovementOrder::callGetOrderList:: getOrderListTemp", logger,
					docGetOrderListTemp);

			docGetOrderListOp = getServiceInvoker().invokeYantraApi(TelstraConstants.API_GET_ORDER_LIST,
					docGetOrderListIp, docGetOrderListTemp);
			LoggerUtil.verboseLog("StockMovementOrder::callGetOrderList:: getOrderListOp", logger,
					docGetOrderListOp);
		}
		else{
			docGetOrderListOp = YFCDocument.getDocumentFor("<OrderList TotalOrderList='0'/>");
		}
	}

	/**
	 * 
	 * @param row
	 * @return
	 */
	private void validatePmOperation(Row row) {
		setPmOperation(row.getCellValue(9));
		if (YFCCommon.isStringVoid(pmOperation)) {
			LoggerUtil.verboseLog("PM operation missing ", logger, null);
			throw ExceptionUtil.getYFSException(
					TelstraErrorCodeConstants.STOCK_MOVEMENT_PM_OPERATION_MISSING_ERROR_CODE, new YFSException());

		}
	}

	/**
	 * 
	 * @param row
	 * @return
	 */
	private void validatePmOrder(Row row) {
		setPmOrder(row.getCellValue(8));
		if (YFCCommon.isStringVoid(pmOrder)) {
			LoggerUtil.verboseLog("PM order missing ", logger, null);
			throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.STOCK_MOVEMENT_PM_ORDER_MISSING_ERROR_CODE,
					new YFSException());

		}
	}

	/**
	 * 
	 * @param row
	 * @return
	 */
	private void validateActivityType(Row row) {
		setActivityType(row.getCellValue(7));
		if (YFCCommon.isStringVoid(activityType)) {
			LoggerUtil.verboseLog("Activity type missing ", logger, null);
			throw ExceptionUtil.getYFSException(
					TelstraErrorCodeConstants.STOCK_MOVEMENT_ACTIVITY_TYPE_MISSING_ERROR_CODE, new YFSException());

		}
	}

	/**
	 * 
	 * @param row
	 * @return
	 */
	private void validateCostCenter(Row row) {
		setCostCenter(row.getCellValue(6));
		if (YFCCommon.isStringVoid(costCenter)) {
			LoggerUtil.verboseLog("Cost center missing ", logger, null);
			throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.STOCK_MOVEMENT_COST_CENTER_MISSING_ERROR_CODE,
					new YFSException());

		}
	}

	/**
	 * 
	 * @param sMovementType
	 * @param row
	 * @return
	 */
	private void validateAndGetShipNode(Row row) {
		setShipNode(row.getCellValue(2));
		if (YFCCommon.isVoid(shipNode) && (TelstraConstants.MOVEMENT_TYPE_ISSUE.equalsIgnoreCase(movementType)
				|| TelstraConstants.MOVEMENT_TYPE_TRANSFER.equalsIgnoreCase(movementType))) {
			LoggerUtil.verboseLog("Ship node is mandatory for given movement type", logger, null);
			throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.STOCK_MOVEMENT_SHIP_NODE_MISSING_ERROR_CODE,
					new YFSException());
		} else if (!YFCCommon.isVoid(shipNode)
				&& (TelstraConstants.MOVEMENT_TYPE_RECOVERY.equalsIgnoreCase(movementType)
						|| TelstraConstants.MOVEMENT_TYPE_RECEIPT.equalsIgnoreCase(movementType))) {
			throw ExceptionUtil.getYFSException(
					TelstraErrorCodeConstants.STOCK_MOVEMENT_ISSUING_NODE_INVALID_FOR_PASSED_MOVEMENT_TYPE_ERROR_CODE,
					new YFSException());
		}
		if (!YFCCommon.isStringVoid(shipNode)) {
			validateNode(shipNode, TelstraConstants.SHIP_NODE);
		}
	}

	/**
	 * 
	 * @param row
	 * @return
	 */
	private void validateAndGetReceivingNode(Row row) {

		setReceivingNode(row.getCellValue(3));
		if (YFCCommon.isVoid(receivingNode) && !TelstraConstants.MOVEMENT_TYPE_ISSUE.equalsIgnoreCase(movementType)) {
			LoggerUtil.verboseLog("Reciving node is mandatory for ", logger, movementType);
			throw ExceptionUtil.getYFSException(
					TelstraErrorCodeConstants.STOCK_MOVEMENT_RECEIVING_NODE_MISSING_ERROR_CODE, new YFSException());
		} else if (!YFCCommon.isVoid(receivingNode)
				&& TelstraConstants.MOVEMENT_TYPE_ISSUE.equalsIgnoreCase(movementType)) {
			throw ExceptionUtil.getYFSException(
					TelstraErrorCodeConstants.STOCK_MOVEMENT_RECEIVING_NODE_NOT_VALID_FOR_ISSUE_ERROR_CODE,
					new YFSException());
		}
		if (!YFCCommon.isStringVoid(receivingNode)) {
			validateNode(receivingNode, TelstraConstants.RECEIVING_NODE);
		}

	}

	/**
	 * 
	 * @param row
	 * @return
	 */
	private void validateOrderNo(Row row) {

		setOrderNo(row.getCellValue(1));
		if (YFCCommon.isStringVoid(orderNo) && (TelstraConstants.MOVEMENT_TYPE_RECEIPT.equalsIgnoreCase(movementType)
				|| TelstraConstants.MOVEMENT_TYPE_ISSUE.equalsIgnoreCase(movementType))) {
			LoggerUtil.verboseLog("order no missing for movement type receipt/issue ", logger, null);
			throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.STOCK_MOVEMENT_ORDER_NO_MISSING_ERROR_CODE,
					new YFSException());

		}
	}

	/**
	 * 
	 * @param row
	 * @return
	 */
	private void validateMovementType(Row row) {

		setMovementType(row.getCellValue(0));
		if (YFCCommon.isStringVoid(movementType)) {
			throw ExceptionUtil.getYFSException(
					TelstraErrorCodeConstants.STOCK_MOVEMENT_MOVEMENT_TYPE_MISSING_ERROR_CODE, new YFSException());
		} else if (!(TelstraConstants.MOVEMENT_TYPE_ISSUE.equalsIgnoreCase(movementType)
				|| TelstraConstants.MOVEMENT_TYPE_TRANSFER.equalsIgnoreCase(movementType)
				|| TelstraConstants.MOVEMENT_TYPE_RECOVERY.equalsIgnoreCase(movementType)
				|| TelstraConstants.MOVEMENT_TYPE_RECEIPT.equalsIgnoreCase(movementType))) {

			throw ExceptionUtil.getYFSException(
					TelstraErrorCodeConstants.STOCK_MOVEMENT_MOVEMENT_TYPE_INVALID_ERROR_CODE, new YFSException());
		}

	}

	/**
	 * 
	 * @return
	 */
	private void validateItemIdAndGetUnitOfMeasure() {

		YFCDocument docGetItemListIp = YFCDocument.getDocumentFor("<Item ItemID='" + itemId + "'/>");
		LoggerUtil.verboseLog(
				"StockMovementReportFileProcessor :: validateItemIdAndGetUnitOfMeasure :: getItemList input doc\n",
				logger, docGetItemListIp);
		YFCDocument docGetItemListTemp = YFCDocument
				.getDocumentFor("<ItemList><Item ItemID=\"\" OrganizationCode=\"\" UnitOfMeasure=\"\" /></ItemList>");
		LoggerUtil.verboseLog(
				"StockMovementReportFileProcessor :: validateItemIdAndGetUnitOfMeasure :: getItemList template doc\n",
				logger, docGetItemListTemp);
		YFCDocument docGetItemListOp = getServiceInvoker().invokeYantraApi(TelstraConstants.API_GET_ITEM_LIST,
				docGetItemListIp, docGetItemListTemp);
		LoggerUtil.verboseLog(
				"StockMovementReportFileProcessor :: validateItemIdAndGetUnitOfMeasure :: getItemList output doc\n",
				logger, docGetItemListOp);

		YFCElement eleItem = docGetItemListOp.getElementsByTagName(TelstraConstants.ITEM).item(0);
		if (YFCCommon.isVoid(eleItem)) {
			LoggerUtil.verboseLog("Invalid item id ", logger, itemId);
			throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.STOCK_MOVEMENT_INVALID_ITEM_ID_ERROR_CODE,
					new YFSException());
		}
		unitOfMeasure = eleItem.getAttribute(TelstraConstants.UNIT_OF_MEASURE);
	}

	/**
	 * 
	 * @param row
	 * @return
	 */
	private void validateAndGetQuantity(Row row) {

		quantity = row.getCellValue(5);
		if (YFCCommon.isVoid(quantity)) {
			LoggerUtil.verboseLog("Inventory quantity missing", logger, " throwing exception");
			throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.STOCK_MOVEMENT_QUANTITY_MISSING_ERROR_CODE,
					new YFSException());
		}
		if (!StringUtils.isNumeric(quantity)) {

			LoggerUtil.verboseLog("Inventory quantity invalid", logger, quantity);
			throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.STOCK_MOVEMENT_INVALID_QUANTITY_ERROR_CODE,
					new YFSException());
		}
	}

	/**
	 * 
	 * @param row
	 * @return
	 */
	private void getItemId(Row row) {
		itemId = row.getCellValue(4);
		if (YFCCommon.isVoid(itemId)) {
			LoggerUtil.verboseLog("Item ID missing", logger, " throwing exception");
			throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.STOCK_MOVEMENT_ITEM_ID_MISSING_ERROR_CODE,
					new YFSException());
		}
	}

	/**
	 * 
	 * @param sNode
	 * @param sSterlingNodeType
	 *            (ShipNode or ReceivingNode)
	 * @return
	 */
	private void validateNode(String sNode, String sSterlingNodeType) {

		YFCDocument docGetShipNodeListIp = YFCDocument.getDocumentFor("<ShipNode ShipNode='" + sNode + "'/>");
		LoggerUtil.verboseLog("StockMovementReportFileProcessor :: validateShipNode :: getShipNodeList input doc\n",
				logger, docGetShipNodeListIp);
		YFCDocument docGetShipNodeListTemp = YFCDocument
				.getDocumentFor("<ShipNodeList><ShipNode ShipNode='' NodeType='' InventoryTracked=''/></ShipNodeList>");
		LoggerUtil.verboseLog("StockMovementReportFileProcessor :: validateShipNode :: getShipNodeList template doc\n",
				logger, docGetShipNodeListTemp);
		YFCDocument docGetShipNodeListOp = getServiceInvoker().invokeYantraApi(TelstraConstants.API_GET_SHIP_NODE_LIST,
				docGetShipNodeListIp, docGetShipNodeListTemp);
		LoggerUtil.verboseLog("StockMovementReportFileProcessor :: validateShipNode :: getShipNodeList output doc\n",
				logger, docGetShipNodeListOp);

		YFCElement eleShipNode = docGetShipNodeListOp.getElementsByTagName(TelstraConstants.SHIP_NODE).item(0);

		if (YFCCommon.isVoid(eleShipNode)) {
			LoggerUtil.verboseLog("Invalid ship node ", logger, sNode);

			if (TelstraConstants.SHIP_NODE.equalsIgnoreCase(sSterlingNodeType)) {
				throw ExceptionUtil.getYFSException(
						TelstraErrorCodeConstants.STOCK_MOVEMENT_INVALID_SHIP_NODE_ERROR_CODE, new YFSException());
			} else {
				throw ExceptionUtil.getYFSException(
						TelstraErrorCodeConstants.STOCK_MOVEMENT_INVALID_RECEIVING_NODE_ERROR_CODE, new YFSException());
			}
		} else {
			String sTrackedInventory = eleShipNode.getAttribute(TelstraConstants.INVENTORY_TRACKED);
			if (TelstraConstants.NO.equalsIgnoreCase(sTrackedInventory)) {
				LoggerUtil.verboseLog("Inventory is not tracked in this ship node ", logger, sNode);

				if (TelstraConstants.SHIP_NODE.equalsIgnoreCase(sSterlingNodeType)) {
					throw ExceptionUtil.getYFSException(
							TelstraErrorCodeConstants.STOCK_MOVEMENT_UPLOAD_INV_NOT_TRACKABLE_ERROR_CODE,
							new YFSException());
				} else {
					throw ExceptionUtil.getYFSException(
							TelstraErrorCodeConstants.STOCK_MOVEMENT_UPLOAD_REC_NODE_INV_NOT_TRACKABLE_ERROR_CODE,
							new YFSException());
				}
			}
		}

		String sNodeType = eleShipNode.getAttribute(TelstraConstants.NODE_TYPE);
		List<String> lCodeValue = getCommonCodeCodeValueList(TelstraConstants.STOCK_MOVEMNT_UPLOAD_NODE_TYPE);

		if (!lCodeValue.contains(sNodeType)) {
			LoggerUtil.verboseLog("Invalid node type", logger, sNodeType);
			if (TelstraConstants.SHIP_NODE.equalsIgnoreCase(sSterlingNodeType)) {
				throw ExceptionUtil.getYFSException(
						TelstraErrorCodeConstants.STOCK_MOVEMENT_INVALID_SHIP_NODE_TYPE_ERROR_CODE, new YFSException());
			} else {
				throw ExceptionUtil.getYFSException(
						TelstraErrorCodeConstants.STOCK_MOVEMENT_INVALID_RECEIVING_NODE_TYPE_ERROR_CODE,
						new YFSException());
			}
		}
	}

	/**
	 * 
	 * @param sCodeType
	 * @return
	 */
	private List<String> getCommonCodeCodeValueList(String sCodeType) {

		YFCDocument docGetCommonCodeListIp = YFCDocument.getDocumentFor("<CommonCode CodeType='" + sCodeType + "'/>");
		LoggerUtil.verboseLog(
				"StockMovementReportFileProcessor :: getCommonCodeCodeValueList :: getCommonCode input doc\n", logger,
				docGetCommonCodeListIp);
		YFCDocument docGetCommonCodeListTemp = YFCDocument.getDocumentFor(
				"<CommonCodeList><CommonCode CodeType='' CodeValue='' CodeShortDescription=''/></CommonCodeList>");
		LoggerUtil.verboseLog(
				"StockMovementReportFileProcessor :: getCommonCodeCodeValueList :: getCommonCode template doc\n",
				logger, docGetCommonCodeListTemp);
		YFCDocument docGetCommonCodeListOp = getServiceInvoker().invokeYantraApi(
				TelstraConstants.API_GET_COMMON_CODE_LIST, docGetCommonCodeListIp, docGetCommonCodeListTemp);
		LoggerUtil.verboseLog(
				"StockMovementReportFileProcessor :: getCommonCodeCodeValueList :: getCommonCode output doc\n", logger,
				docGetCommonCodeListOp);

		List<String> lCodeValue = new ArrayList<>();

		YFCNodeList<YFCElement> nlCommonCode = docGetCommonCodeListOp
				.getElementsByTagName(TelstraConstants.COMMON_CODE);
		for (int i = 0; i < nlCommonCode.getLength(); i++) {
			YFCElement eleCommonCode = nlCommonCode.item(i);
			String sCodeValue = eleCommonCode.getAttribute(TelstraConstants.CODE_VALUE);
			lCodeValue.add(sCodeValue);
		}

		return lCodeValue;
	}

	protected String getMovementType() {
		return movementType;
	}

	protected void setMovementType(String movementType) {
		this.movementType = movementType;
	}

	protected String getOrderNo() {
		return orderNo;
	}

	protected void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	protected String getShipNode() {
		return shipNode;
	}

	protected void setShipNode(String shipNode) {
		this.shipNode = shipNode;
	}

	protected String getReceivingNode() {
		return receivingNode;
	}

	protected void setReceivingNode(String receivingNode) {
		this.receivingNode = receivingNode;
	}

	protected String getItemId() {
		return itemId;
	}

	protected void setItemId(String itemId) {
		this.itemId = itemId;
	}

	protected String getUnitOfMeasure() {
		return unitOfMeasure;
	}

	protected void setUnitOfMeasure(String unitOfMeasure) {
		this.unitOfMeasure = unitOfMeasure;
	}

	protected String getQuantity() {
		return quantity;
	}

	protected void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	protected String getCostCenter() {
		return costCenter;
	}

	protected void setCostCenter(String costCenter) {
		this.costCenter = costCenter;
	}

	protected String getActivityType() {
		return activityType;
	}

	protected void setActivityType(String activityType) {
		this.activityType = activityType;
	}

	protected String getPmOrder() {
		return pmOrder;
	}

	protected void setPmOrder(String pmOrder) {
		this.pmOrder = pmOrder;
	}

	protected String getPmOperation() {
		return pmOperation;
	}

	protected void setPmOperation(String pmOperation) {
		this.pmOperation = pmOperation;
	}

	@Override
	public String toString() {
		return "StockMovementFileProcessor [movementType=" + movementType + ", orderNo=" + orderNo + ", shipNode="
				+ shipNode + ", receivingNode=" + receivingNode + ", itemId=" + itemId + ", unitOfMeasure="
				+ unitOfMeasure + ", quantity=" + quantity + ", costCenter=" + costCenter + ", activityType="
				+ activityType + ", pmOrder=" + pmOrder + ", pmOperation=" + pmOperation + "]";
	}

}

/***********************************************************************************************
 * File Name        : StockMovementOrderCreation.java
 *
 * Description      : Custom API for creating Stock movement draft order
 * 
 * Modification Log :
 * ---------------------------------------------------------------------------------------------
 * Ver #    Date        	  Author                 			 Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0						Prateek Kumar    					Initial Version
 * 1.1		Jan 11,2017		Prateek Kumar						Added custom template while calling create order api
 * 1.2		Jan 30,2017		Prateek Kumar						HUB-8372 : picking the unit of measure from order details for the movement type RECEIPT 
 * 1.3      May 30,2017     Keerthi Yadav  						HUB-9176 : SMR: Storing incorrect Source System (Entry Type).
 * ---------------------------------------------------------------------------------------------
 * 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
 **********************************************************************************************/
package com.gps.hubble.file.agent.inventory;

import com.bridge.sterling.framework.service.ServiceInvoker;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;

/**
 * 
 * @author Prateek
 *
 */
public class StockMovementOrderCreation {

	private static YFCLogCategory logger = YFCLogCategory.instance(StockMovementOrderCreation.class);
	private StockMovementFileProcessor oStockMovementFP;
	private ServiceInvoker serviceInvoker;

	/**
	 * 
	 * @param oStockMovementFP
	 * @param serviceInvoker
	 */
	public void manageStockMovementOrder(StockMovementFileProcessor oStockMovementFP, ServiceInvoker serviceInvoker) {

		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "StockMovementOrderCreation ",
				oStockMovementFP.toString());
		this.oStockMovementFP = oStockMovementFP;
		this.serviceInvoker = serviceInvoker;
		YFCDocument yfcDocOrderListOp = callGetOrderListApi();
		
		if (YFCCommon.isVoid(yfcDocOrderListOp)) {

			String sMovementType = oStockMovementFP.getMovementType();
			YFCDocument docCreateAsyncReqMsg = null;

			if (TelstraConstants.MOVEMENT_TYPE_ISSUE.equalsIgnoreCase(sMovementType)) {
				docCreateAsyncReqMsg = createDraftOrder(TelstraConstants.DOCUMENT_TYPE_0001);
			}
			else if (TelstraConstants.MOVEMENT_TYPE_RECOVERY.equalsIgnoreCase(sMovementType)) {
				oStockMovementFP.setShipNode(oStockMovementFP.getReceivingNode());
				docCreateAsyncReqMsg = createDraftOrder(TelstraConstants.DOCUMENT_TYPE_0003);
			}
			else if (TelstraConstants.MOVEMENT_TYPE_TRANSFER.equalsIgnoreCase(sMovementType)) {
				docCreateAsyncReqMsg = createDraftOrder(TelstraConstants.DOCUMENT_TYPE_0006);
			}
			
			callAsyncRequestService(docCreateAsyncReqMsg);

		}
		else if(TelstraConstants.MOVEMENT_TYPE_RECEIPT.equalsIgnoreCase(oStockMovementFP.getMovementType())){
			String sPrimeLineNo = XPathUtil.getXpathAttribute(yfcDocOrderListOp,
					"//OrderLine[Item[@ItemID='" + oStockMovementFP.getItemId() + "'] and @ReceivingNode='"
							+ oStockMovementFP.getReceivingNode() + "' and Extn[@Division='"
							+ oStockMovementFP.getCostCenter() + "']]/@PrimeLineNo");
			
			LoggerUtil.verboseLog("StockMovementOrderCreation:: movement type receipt :: primeLineNo ", logger,
					sPrimeLineNo);
			YFCDocument docCreateAsyncReqMsg = receiveOrder(sPrimeLineNo);
			updateAsyncReqMsgForReceipt(docCreateAsyncReqMsg, yfcDocOrderListOp, sPrimeLineNo);
			callAsyncRequestService(docCreateAsyncReqMsg);
		}		
		else {
			changeOrder(yfcDocOrderListOp);
		}
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "StockMovementOrderCreation ",
				oStockMovementFP.toString());

	}

	/**
	 * 
	 * @param docCreateAsyncReqMsg
	 * @param yfcDocOrderListOp
	 * @param sPrimeLineNo
	 */
	private void updateAsyncReqMsgForReceipt(YFCDocument docCreateAsyncReqMsg, YFCDocument yfcDocOrderListOp, String sPrimeLineNo) {

		YFCElement yfcEleOrder = yfcDocOrderListOp.getDocumentElement();
		String sSellerOrgCode = yfcEleOrder.getAttribute(TelstraConstants.SELLER_ORGANIZATION_CODE);
		String sEntryType = yfcEleOrder.getAttribute(TelstraConstants.ENTRY_TYPE);
		String sOrderType = yfcEleOrder.getAttribute(TelstraConstants.ORDER_TYPE);
		String sOrderDate = yfcEleOrder.getAttribute(TelstraConstants.ORDER_DATE);
		String sEnteredBy = yfcEleOrder.getAttribute(TelstraConstants.ENTERED_BY);
		String sOrderedQty = "";
		String sShipNode = "";
		String sDivision = "";
		YFCElement yfcEleOrderLine = XPathUtil.getXPathElement(yfcDocOrderListOp, "//OrderLine[@PrimeLineNo='"+sPrimeLineNo+"']");
		if(!YFCCommon.isVoid(yfcEleOrderLine)){
			sOrderedQty = yfcEleOrderLine.getAttribute(TelstraConstants.ORDERED_QTY);
			sShipNode = yfcEleOrderLine.getAttribute(TelstraConstants.SHIP_NODE);
			YFCElement yfcEleLineExtn = yfcEleOrderLine.getChildElement(TelstraConstants.EXTN);
			if(!YFCCommon.isVoid(yfcEleLineExtn)){
				sDivision = yfcEleLineExtn.getAttribute(TelstraConstants.DIVISION);
			}
		}
		
		YFCElement yfcEleReceipt = docCreateAsyncReqMsg.getElementsByTagName(TelstraConstants.RECEIPT).item(0);
		if(!YFCCommon.isVoid(yfcEleReceipt)){
			yfcEleReceipt.setAttribute(TelstraConstants.SELLER_ORGANIZATION_CODE, sSellerOrgCode);
			yfcEleReceipt.setAttribute(TelstraConstants.ENTRY_TYPE, sEntryType);
			yfcEleReceipt.setAttribute(TelstraConstants.ORDER_TYPE, sOrderType);
			yfcEleReceipt.setAttribute(TelstraConstants.ORDER_DATE, sOrderDate);
			yfcEleReceipt.setAttribute(TelstraConstants.ENTERED_BY, sEnteredBy);
			
			YFCElement yfcEleReceiptLine = yfcEleReceipt.getElementsByTagName(TelstraConstants.RECEIPT_LINE).item(0);
			yfcEleReceiptLine.setAttribute(TelstraConstants.ORDERED_QTY, sOrderedQty);
			yfcEleReceiptLine.setAttribute(TelstraConstants.SHIP_NODE, sShipNode);
			yfcEleReceiptLine.setAttribute(TelstraConstants.DIVISION, sDivision);
		}		
	}

	/**
	 * @param docGetOrderListOp
	 * @return
	 * 
	 */
	private YFCDocument receiveOrder(String sPrimeLineNo) {

		YFCDocument docReceiveOrderIp = YFCDocument.getDocumentFor("<ReceiptList> <Receipt ReceivingNode='"
				+ oStockMovementFP.getReceivingNode() + "' OrderName='" + oStockMovementFP.getOrderNo()
				+ "' > <ReceiptLines> <ReceiptLine ItemID='" + oStockMovementFP.getItemId() + "' PrimeLineNo='"
				+ sPrimeLineNo + "' Quantity='" + oStockMovementFP.getQuantity() + "' SubLineNo='1' UnitOfMeasure='" + oStockMovementFP.getUnitOfMeasure() + "'>"
						+ " </ReceiptLine> </ReceiptLines> </Receipt> </ReceiptList>");
		LoggerUtil.verboseLog("StockMovementOrderCreation::receiveOrder :: docReceiveOrderIp\n", logger, docReceiveOrderIp);
		serviceInvoker.invokeYantraService(TelstraConstants.GPS_RECEIVE_ORDER, docReceiveOrderIp);
		return docReceiveOrderIp;

	}

	/**
	 * 
	 * @param docGetOrderListOp
	 */
	private void changeOrder(YFCDocument docOrder) {

			String sOrderHeaderKey = docOrder.getDocumentElement().getAttribute(TelstraConstants.ORDER_HEADER_KEY);

			YFCDocument docChangeOrderIp = YFCDocument.getDocumentFor("<Order OrderHeaderKey='" + sOrderHeaderKey
					+ "'><OrderLines><OrderLine ShipNode='" + oStockMovementFP.getShipNode() + "' OrderedQty='"
					+ oStockMovementFP.getQuantity() + "' ReceivingNode='" + oStockMovementFP.getReceivingNode()
					+ "'><Item ItemID='" + oStockMovementFP.getItemId() + "' UnitOfMeasure='"
					+ oStockMovementFP.getUnitOfMeasure() + "'/> <Extn Division='"+oStockMovementFP.getCostCenter()+"'/></OrderLine></OrderLines></Order>");

			LoggerUtil.verboseLog("StockMovementOrder::changeOrder::docChangeOrderIp\n", logger, docChangeOrderIp);
			serviceInvoker.invokeYantraApi(TelstraConstants.API_CHANGE_ORDER, docChangeOrderIp);
		
	}

	/**
	 * 
	 * @param docCreateAsyncReqMsg
	 */
	private void callAsyncRequestService(YFCDocument docCreateAsyncReqMsg) {

		YFCDocument docAsynRequestIp = YFCDocument.getDocumentFor("<CreateAsyncRequest />");
		String sMessage = docCreateAsyncReqMsg.getDocumentElement().getString().replace("\\<\\?xml(.+?)\\?\\>", "").trim();
		docAsynRequestIp.getDocumentElement().setAttribute(TelstraConstants.SERVICE_NAME,
				TelstraConstants.GPS_PROCESS_STOCK_MOVEMENT);
		docAsynRequestIp.getDocumentElement().setAttribute(TelstraConstants.MESSAGE, sMessage);
		docAsynRequestIp.getDocumentElement().setAttribute(TelstraConstants.INTERFACE_NO,
				TelstraConstants.STOCK_MOVEMENT_UPLOAD);
		docAsynRequestIp.getDocumentElement().setAttribute(TelstraConstants.IS_FLOW, TelstraConstants.YES);
		docAsynRequestIp.getDocumentElement().setAttribute(TelstraConstants.TRANSACTION_REFERENCE_NO, getTransactionReferenceNo(docCreateAsyncReqMsg));
		LoggerUtil.verboseLog("StockMovementOrder::callAsyncRequestService::docAsynRequestIp\n", logger,
				docAsynRequestIp);
		serviceInvoker.invokeYantraService(TelstraConstants.GPS_CREATE_ASYNC_REQUEST, docAsynRequestIp);
	}

	/**
	 * 
	 * @param docCreateOrderOp
	 * @return
	 */
	private String getTransactionReferenceNo(YFCDocument docCreateAsyncReqMsg) {

		String sReturnValue = (TelstraConstants.MOVEMENT_TYPE_RECEIPT
				.equalsIgnoreCase(oStockMovementFP.getMovementType()))
						? XPathUtil.getXpathAttribute(docCreateAsyncReqMsg, "//Receipt/@OrderName")
						: XPathUtil.getXpathAttribute(docCreateAsyncReqMsg, "//Order/@OrderNo");
		LoggerUtil.verboseLog("StockMovementOrder::getTransactionReferenceNo:: TransReferenceNo ", logger,
				sReturnValue);
		return sReturnValue;
	}

	/**
	 * @return
	 * 
	 */
	private YFCDocument createDraftOrder(String sDocumentType) {

		//HUB-9176 - [START]
		
		/* The below changes are done to replace the entry type from  STERLING_OMS to STERLING 
		 * and moving the PersonInfoBillTo to PersonInfoShipTo at the Order line level. */
		
		YFCDocument docCreateOrderIp = YFCDocument.getDocumentFor(
				"<Order EnterpriseCode='TELSTRA_SCLSA' EntryType='STERLING' BuyerOrganizationCode='TELSTRA' SellerOrganizationCode='TELSTRA' DocumentType='"
						+ sDocumentType + "' OrderName='" + oStockMovementFP.getOrderNo() + "' "
						+ " DraftOrderFlag='Y' OrderType='STOCK_MOVEMENT'><References> "
						+ "<Reference Name='PM_ORDER' Value='" + oStockMovementFP.getPmOrder()
						+ "' /> <Reference Name='ACTIVITY_NUMBER' Value='" + oStockMovementFP.getPmOperation() + "' /> "
						+ "<Reference Name='ACTIVITY_TYPE' Value='" + oStockMovementFP.getActivityType()
						+ "' /> </References> <OrderLines> " + "<OrderLine ShipNode='" + oStockMovementFP.getShipNode()
						+ "' OrderedQty='" + oStockMovementFP.getQuantity() + "' ReceivingNode='" + oStockMovementFP.getReceivingNode()
						+ "'> <Item ItemID='"
						+ oStockMovementFP.getItemId() + "' " + "UnitOfMeasure='" + oStockMovementFP.getUnitOfMeasure()
						+ "' /> <Extn Division='"+ oStockMovementFP.getCostCenter()+"'/> <PersonInfoShipTo Country='AU' /> </OrderLine> </OrderLines></Order>");
		LoggerUtil.verboseLog("StockMovementOrder::createSalesOrder::CreateOrderIp\n", logger, docCreateOrderIp);
		YFCDocument docCreateOrderOp = serviceInvoker.invokeYantraApi(TelstraConstants.API_CREATE_ORDER,
				docCreateOrderIp, YFCDocument.getDocumentFor("<Order OrderHeaderKey='' DocumentType='' OrderName='' OrderNo=''/>"));
		
		//HUB-9176 - [END]
		
		LoggerUtil.verboseLog("StockMovementOrder::createSalesOrder::docCreateOrderOp\n", logger, docCreateOrderOp);
		return docCreateOrderOp;

	}

	/**
	 * 
	 * @return docGetOrderListOp
	 */
	private YFCDocument callGetOrderListApi() {

		String sDocumentType;
		String sStatus="1000";
		String sOrderType="STOCK_MOVEMENT";
		switch (oStockMovementFP.getMovementType()) {

		case TelstraConstants.MOVEMENT_TYPE_ISSUE:
			sDocumentType = TelstraConstants.DOCUMENT_TYPE_0001;			
			break;
		case TelstraConstants.MOVEMENT_TYPE_RECOVERY:
			sDocumentType = TelstraConstants.DOCUMENT_TYPE_0003;				
			oStockMovementFP.setShipNode(oStockMovementFP.getReceivingNode());			
			break;
		case TelstraConstants.MOVEMENT_TYPE_TRANSFER:
			sDocumentType = TelstraConstants.DOCUMENT_TYPE_0006;			
			break;
		default:
			sDocumentType = TelstraConstants.BLANK;
			sStatus = TelstraConstants.BLANK;
			sOrderType = TelstraConstants.BLANK;
			break;
		}

		YFCDocument docGetOrderListIp = YFCDocument.getDocumentFor("<Order DocumentType='" + sDocumentType
				+ "' OrderType='"+sOrderType+"' OrderName='" + oStockMovementFP.getOrderNo() + "'   Status='"+sStatus+"'  "
				+ "StatusQryType='EQ' OrderNameQryType='EQ' DivisionQryType='EQ'> <OrderLine ShipNode='"
				+ oStockMovementFP.getShipNode() + "' ShipNodeQryType='EQ'" + " ReceivingNode='"
				+ oStockMovementFP.getReceivingNode() + "' ReceivingNodeQryType='EQ'><Extn Division='"
				+ oStockMovementFP.getCostCenter() + "'/> </OrderLine> </Order>");
		LoggerUtil.verboseLog("StockMovementOrder::getOrderList:: getOrderListIp", logger, docGetOrderListIp);
		YFCDocument docGetOrderListTemp = YFCDocument.getDocumentFor(
				"<OrderList> <Order OrderName='' Division='' OrderNo='' OrderType='' DocumentType='' OrderHeaderKey='' SellerOrganizationCode='' EntryType=''  "
				+ "OrderDate='' EnteredBy=''> <References /> <OrderLines> <OrderLine ShipNode='' ReceivingNode='' PrimeLineNo='' SubLineNo='' OrderedQty=''> "
				+ "<Item ItemID='' UnitOfMeasure=''/> <Extn Division=''/> </OrderLine></OrderLines> </Order> </OrderList>");
		LoggerUtil.verboseLog("StockMovementOrder::getOrderList:: getOrderListTemp", logger, docGetOrderListTemp);
		
		YFCDocument docGetOrderListOp = serviceInvoker.invokeYantraApi(TelstraConstants.API_GET_ORDER_LIST,
				docGetOrderListIp, docGetOrderListTemp);
		LoggerUtil.verboseLog("StockMovementOrder::getOrderList:: getOrderListOp", logger, docGetOrderListOp);
		
		
		YFCNodeList<YFCElement> nlOrder = docGetOrderListOp.getElementsByTagName(TelstraConstants.ORDER);
		YFCDocument retDocOrder = null;
		for (YFCElement eleOrder : nlOrder) {

			YFCDocument yfcDocOrder = YFCDocument.getDocumentFor(eleOrder.toString());
			String sActivityType = XPathUtil.getXpathAttribute(yfcDocOrder, "//Reference[@Name='ACTIVITY_TYPE']/@Value");						
			String sPmOperation = XPathUtil.getXpathAttribute(yfcDocOrder, "//Reference[@Name='ACTIVITY_NUMBER']/@Value");				
			String sPmOrder = XPathUtil.getXpathAttribute(yfcDocOrder, "//Reference[@Name='PM_ORDER']/@Value");
			
			for(YFCElement yfcEleOrderLine : yfcDocOrder.getElementsByTagName(TelstraConstants.ORDER_LINE)){
				String sShipNode = XPathUtil.getXpathAttribute(YFCDocument.getDocumentFor(yfcEleOrderLine.toString()), "//OrderLine/@ShipNode");
				String sReceivingNode = XPathUtil.getXpathAttribute(YFCDocument.getDocumentFor(yfcEleOrderLine.toString()), "//OrderLine/@ReceivingNode");
				
				// if activity type or pm operation or pm order is null in the already created order (only possible in case of receipt), assigning the passed value so that validation doesn't fail
				if(YFCCommon.isStringVoid(sActivityType)){
					sActivityType = oStockMovementFP.getActivityType();
				}
				if(YFCCommon.isStringVoid(sPmOperation)){
					sPmOperation = oStockMovementFP.getPmOperation();
				}
				if(YFCCommon.isStringVoid(sPmOrder)){
					sPmOrder = oStockMovementFP.getPmOrder();
				}	
				//[HUB-7100] Begin
				// In case of receipt, user wont be sending shipping plant in the input even though it is there in the order. So making it blank to pass the ship node validation for receipt
				if(TelstraConstants.MOVEMENT_TYPE_RECEIPT.equalsIgnoreCase(oStockMovementFP.getMovementType())){
					sShipNode = TelstraConstants.BLANK;
				}
				//[HUB-7100] End			
				if (oStockMovementFP.getShipNode().equals(sShipNode)
						&& oStockMovementFP.getReceivingNode().equals(sReceivingNode)
						&& oStockMovementFP.getActivityType().equals(sActivityType)
						&& oStockMovementFP.getPmOperation().equals(sPmOperation)
						&& oStockMovementFP.getPmOrder().equals(sPmOrder)) {

					retDocOrder = YFCDocument.createDocument();
					YFCElement eleOrderImp = retDocOrder.importNode(eleOrder, true);
					retDocOrder.appendChild(eleOrderImp);
					LoggerUtil.verboseLog("StockMovementOrder::callGetOrderListApi::retDocOrder\n", logger, retDocOrder);
					break;
				}				
			}
		}
		
		return retDocOrder;
	}
}

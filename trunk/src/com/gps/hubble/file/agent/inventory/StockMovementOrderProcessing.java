/***********************************************************************************************
 * File Name        : StockMovementOrderProcessing.java
 *
 * Description      : Custom API for Stock movement order processing
 * 
 * Modification Log :
 * ------------------------------------------------------------------------------------------------------------------------------------
 * Ver #    Date        	  Author            Modification
 * ------------------------------------------------------------------------------------------------------------------------------------
 * 1.0						Prateek Kumar    	Initial Version
 * 1.1		Jan 11,2017		Prateek Kumar		Updating order name field with order number for movement type transfer if it is not coming in the input
 * 1.2		Apr 25,2017		Prateek Kumar		HUB-8803: Append sequence number in MA01, MA02 and MA03 messages  
 * 1.3      May 30,2017     Keerthi Yadav  		HUB-9185 : The Order_header_extension table is not populated for the Stock Movement Orders 
 * ------------------------------------------------------------------------------------------------------------------------------------
 * 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
 *************************************************************************************************************************************/
package com.gps.hubble.file.agent.inventory;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.DateTimeUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.order.api.GpsUpdateOrderExtnFields;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSException;

/**
 * 
 * @author Prateek
 *
 */
public class StockMovementOrderProcessing extends AbstractCustomApi {

	private static YFCLogCategory logger = YFCLogCategory.instance(StockMovementOrderProcessing.class);
	private String sOrderHeaderKey;
	private YFCDocument docConfirmDraftOrderOp = null;
	private YFCDocument docGpsPickOrderIp;

	/**
	 * 
	 */
	@Override
	public YFCDocument invoke(YFCDocument inDoc) throws YFSException {

		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", inDoc);
				
		sOrderHeaderKey = inDoc.getDocumentElement().getAttribute(TelstraConstants.ORDER_HEADER_KEY,"");
		String sDocumentType = inDoc.getDocumentElement().getAttribute(TelstraConstants.DOCUMENT_TYPE);
		
		//HUB-9185 [START]
		
		/* The changes below invoke a class to update the Order header extension table with the boolean values only 
		 * for Order Creation and not for movement type Receipt*/
		if(!YFCObject.isVoid(sOrderHeaderKey)){
			GpsUpdateOrderExtnFields obj = new GpsUpdateOrderExtnFields();
			obj.updateOrderExtnFields(inDoc,getServiceInvoker());
		}

		//HUB-9185 [END]

		if (TelstraConstants.DOCUMENT_TYPE_0001.equals(sDocumentType)) {
			processSalesOrder();
			prepareAndSendMA02Msg();

		} else if (TelstraConstants.DOCUMENT_TYPE_0003.equals(sDocumentType)) {
			processReturnOrder(inDoc);
			String sReceivingNode = XPathUtil.getXpathAttribute(docConfirmDraftOrderOp, "//OrderLine/@ReceivingNode");
			prepareAndSendMA03Msg(TelstraConstants.STOCK_MOVEMENT_RECOVERY_ORDER_TYPE, sReceivingNode);

		} else if (TelstraConstants.DOCUMENT_TYPE_0006.equals(sDocumentType)) {
			processTransferOrder(inDoc);

			String sReceivingNode = XPathUtil.getXpathAttribute(docConfirmDraftOrderOp, "//OrderLine/@ReceivingNode");
			if (!YFCCommon.isStringVoid(sReceivingNode)) {
				prepareAndSendMA03Msg(TelstraConstants.STOCK_MOVEMENT_RECOVERY_ORDER_TYPE, sReceivingNode);
			}
			String sShipNode = XPathUtil.getXpathAttribute(docConfirmDraftOrderOp, "//OrderLine/@ShipNode");
			if (!YFCCommon.isStringVoid(sShipNode)) {
				prepareAndSendMA03Msg(TelstraConstants.STOCK_MOVEMENT_ISSUE_ORDER_TYPE, sShipNode);
			}
		} else {
			prepareAndSendMA01Msg(TelstraConstants.STOCK_MOVEMENT_ISSUE_ORDER_TYPE, inDoc);
		}

		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", inDoc);

		return inDoc;

	}



	/**
	 * 
	 */
	private void processSalesOrder() {

		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "processSalesOrder", null);
		confirmDraftOrder();
		scheduleAndRelease();
		pickOrder();
		shipOrder();
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "processSalesOrder", null);

	}

	/**
	 * 
	 */
	private void processReturnOrder(YFCDocument inDoc) {

		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "processReturnOrder", null);
		updateOrderName(inDoc);
		confirmDraftOrder();
		YFCDocument docCreateShipmentOp = createReturnShipment();
		confirmShipment(docCreateShipmentOp);
		updateCarrierData();
		receiveOrder(docCreateShipmentOp, TelstraConstants.DOCUMENT_TYPE_0003);
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "processReturnOrder", null);

	}

	/**
	 * @param inDoc 
	 * 
	 */

	private void processTransferOrder(YFCDocument inDoc) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "processTransferOrder", null);
		/*
		 * Order name is optional field for processing movement type Transfer but it is mandatory field for processing Transfer order.
		 * If it is not passed in the input, below method will copy order number generated by Sterling to order name field by calling change order ip
		 */
		updateOrderName(inDoc);
		confirmDraftOrder();
		scheduleAndRelease();

		//Pick order method will move the order from released to Order picked status
		YFCDocument yfcDocGpsPickOrderOp = pickOrder();
		// Once the order is picked, ship order method will get called which will internally call GpsShipOrRejectOrder service to move the order
		//status to shipped
		shipOrder();
		//After shipment, order will be moved to delivered status by leveraging existing carrier update service (GpsCarrierUpdate)
		updateCarrierData();
		// Receive order code takes care of moving the order from Picked to Received status by calling Ship order and carrier update code internally
		receiveOrder(yfcDocGpsPickOrderOp, TelstraConstants.DOCUMENT_TYPE_0006);
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "processTransferOrder", null);

	}

	/**
	 * Order name is optional field for processing movement type Transfer but it is mandatory field for processing Transfer order.
	 * If it is not passed in the input, below method will copy order number generated by Sterling to order name field by calling change order ip
	 * @param inDoc
	 */
	private void updateOrderName(YFCDocument inDoc) {

		String sOrderName = inDoc.getDocumentElement().getAttribute(TelstraConstants.ORDER_NAME);
		if(YFCCommon.isStringVoid(sOrderName)){
			YFCDocument yfcDocChangeOrderIp = YFCDocument.getDocumentFor("<Order OrderHeaderKey='" + sOrderHeaderKey
					+ "' OrderName='" + inDoc.getDocumentElement().getAttribute(TelstraConstants.ORDER_NO) + "'/>");
			invokeYantraApi(TelstraConstants.API_CHANGE_ORDER, yfcDocChangeOrderIp);
		}
	}



	/**
	 * Below method will invoke GpsCarrierUpdate service with Transport status as delivered and will order to Order Delivered status
	 */
	private void updateCarrierData() {

		YFCDocument yfcDocCarrierUpdateIp = YFCDocument.createDocument(TelstraConstants.SHIPMENT);
		YFCElement yfcEleRoot = yfcDocCarrierUpdateIp.getDocumentElement();		
		yfcEleRoot.setAttribute(TelstraConstants.ORDER_NAME, docConfirmDraftOrderOp.getDocumentElement().getAttribute(TelstraConstants.ORDER_NAME));

		YFCElement yfcEleExtn = yfcDocCarrierUpdateIp.createElement(TelstraConstants.EXTN);
		yfcEleRoot.appendChild(yfcEleExtn);

		YFCElement yfcEleCarrierUpdateList = yfcDocCarrierUpdateIp.createElement(TelstraConstants.CARRIER_UPDATE_LIST);
		yfcEleExtn.appendChild(yfcEleCarrierUpdateList);

		YFCElement yfcEleCarrierUpdate = yfcDocCarrierUpdateIp.createElement(TelstraConstants.CARRIER_UPDATE);
		yfcEleCarrierUpdateList.appendChild(yfcEleCarrierUpdate);
		yfcEleCarrierUpdate.setAttribute(TelstraConstants.TRANSPORT_STATUS, TelstraConstants.DELIVERED);
		yfcEleCarrierUpdate.setAttribute(TelstraConstants.TRANSPORT_STATUS_DATE, DateTimeUtil.getFormattedCurrentDate("yyyy-MM-dd'T'HH:mm:ss"));

		LoggerUtil.verboseLog("StockMovementOrderProcessing::updateCarrierData:: yfcDocCarrierUpdateIp", logger, yfcDocCarrierUpdateIp);
		invokeYantraService(TelstraConstants.GPS_CARRIER_UPDATE, yfcDocCarrierUpdateIp);
	}



	/**
	 * 
	 */
	private void confirmDraftOrder() {

		YFCDocument docConfirmDraftOrderTemp = YFCDocument.getDocumentFor(
				"<Order OrderName='' OrderNo='' OrderHeaderKey='' Division=''> <OrderLines> <OrderLine PrimeLineNo='' SubLineNo='' OrderedQty='' "
						+ "ShipNode='' ReceivingNode=''> <Item ItemID='' UnitOfMeasure=''/> <Extn/> </OrderLine> </OrderLines> <References/> </Order>");

		YFCDocument docConfirmDraftOrderIp = YFCDocument
				.getDocumentFor("<ConfirmDraftOrder OrderHeaderKey='" + sOrderHeaderKey + "'/>");
		docConfirmDraftOrderOp = invokeYantraApi(TelstraConstants.API_CONFIRM_DRAFT_ORDER, docConfirmDraftOrderIp,
				docConfirmDraftOrderTemp);
		LoggerUtil.verboseLog("StockMovementOrderProcessing::confirmDraftOrder:: confirmDraftOrder Op", logger,
				docConfirmDraftOrderOp);
	}

	/**
	 * 
	 */
	private void scheduleAndRelease() {

		YFCDocument docScheduleAndReleaseIp = YFCDocument.getDocumentFor(
				"<ScheduleOrder CheckInventory='N' ScheduleAndRelease='Y' IgnoreReleaseDate='Y' OrderHeaderKey='" + sOrderHeaderKey + "'/>");
		LoggerUtil.verboseLog("StockMovementOrderProcessing::ScheduleAndRelease:: docScheduleAndReleaseIp", logger,
				docScheduleAndReleaseIp);
		invokeYantraApi(TelstraConstants.API_SCHEDULE_ORDER, docScheduleAndReleaseIp);
	}

	/**
	 * 
	 */
	private YFCDocument pickOrder() {

		docGpsPickOrderIp = YFCDocument.createDocument(TelstraConstants.ORDER_RELEASE);
		YFCElement eleRootPickOrder = docGpsPickOrderIp.getDocumentElement();
		eleRootPickOrder.setAttribute(TelstraConstants.ORDER_NAME,
				docConfirmDraftOrderOp.getDocumentElement().getAttribute(TelstraConstants.ORDER_NAME));
		eleRootPickOrder.setAttribute(TelstraConstants.SHIP_NODE,
				(docConfirmDraftOrderOp.getElementsByTagName(TelstraConstants.ORDER_LINE).item(0))
				.getAttribute(TelstraConstants.SHIP_NODE));

		YFCElement eleOrderLinesPickOrder = docGpsPickOrderIp.createElement(TelstraConstants.ORDER_LINES);
		eleRootPickOrder.appendChild(eleOrderLinesPickOrder);

		YFCNodeList<YFCElement> nlOrderLine = docConfirmDraftOrderOp.getElementsByTagName(TelstraConstants.ORDER_LINE);
		for (YFCElement eleOrderLine : nlOrderLine) {

			String sOrderedQty = eleOrderLine.getAttribute(TelstraConstants.ORDERED_QTY);
			String sPrimeLineNo = eleOrderLine.getAttribute(TelstraConstants.PRIME_LINE_NO);
			String sSubLineNo = eleOrderLine.getAttribute(TelstraConstants.SUB_LINE_NO);

			YFCElement eleItem = eleOrderLine.getChildElement(TelstraConstants.ITEM);
			String sItemID = eleItem.getAttribute(TelstraConstants.ITEM_ID);
			String sUOM = eleItem.getAttribute(TelstraConstants.UNIT_OF_MEASURE);

			YFCElement eleOrderLinePickOrder = docGpsPickOrderIp.createElement(TelstraConstants.ORDER_LINE);
			eleOrderLinesPickOrder.appendChild(eleOrderLinePickOrder);
			eleOrderLinePickOrder.setAttribute(TelstraConstants.ACTION, TelstraConstants.BACKORDER);
			eleOrderLinePickOrder.setAttribute(TelstraConstants.PRIME_LINE_NO, sPrimeLineNo);
			eleOrderLinePickOrder.setAttribute(TelstraConstants.SUB_LINE_NO, sSubLineNo);
			eleOrderLinePickOrder.setAttribute(TelstraConstants.STATUS_QUANTITY, sOrderedQty);
			eleOrderLinePickOrder.setAttribute(TelstraConstants.ITEM_ID, sItemID);
			eleOrderLinePickOrder.setAttribute(TelstraConstants.UNIT_OF_MEASURE, sUOM);
		}
		LoggerUtil.verboseLog("StockMovementOrderProcessing::PickOrder:: docGpsPickOrderIp", logger, docGpsPickOrderIp);
		YFCDocument yfcDocGpsPickOrderOp = invokeYantraService(TelstraConstants.GPS_PICK_ORDER, docGpsPickOrderIp);
		LoggerUtil.verboseLog("StockMovementOrderProcessing::PickOrder:: yfcDocGpsPickOrderOp", logger, yfcDocGpsPickOrderOp);

		return yfcDocGpsPickOrderOp;
	}

	/**
	 * Below method prepare the input for GpsShipOrRejectOrder from the confirm draft order op xml
	 */
	private void shipOrder() {

		YFCDocument yfcDocShipOrderIp = YFCDocument.createDocument(TelstraConstants.SHIPMENT);
		YFCElement yfcEleRoot = yfcDocShipOrderIp.getDocumentElement();		
		yfcEleRoot.setAttribute(TelstraConstants.ORDER_NAME,
				docConfirmDraftOrderOp.getDocumentElement().getAttribute(TelstraConstants.ORDER_NAME));
		YFCElement yfcEleShipmentLines = yfcDocShipOrderIp.createElement(TelstraConstants.SHIPMENT_LINES);
		yfcEleRoot.appendChild(yfcEleShipmentLines);

		YFCNodeList<YFCElement> nlOrderLine = docConfirmDraftOrderOp.getElementsByTagName(TelstraConstants.ORDER_LINE);
		for (YFCElement eleOrderLine : nlOrderLine) {

			YFCElement yfcEleShipmentLine = yfcDocShipOrderIp.createElement(TelstraConstants.SHIPMENT_LINE);
			yfcEleShipmentLines.appendChild(yfcEleShipmentLine);			
			yfcEleShipmentLine.setAttribute(TelstraConstants.ACTION, TelstraConstants.BACKORDER);
			yfcEleShipmentLine.setAttribute(TelstraConstants.PRIME_LINE_NO, eleOrderLine.getAttribute(TelstraConstants.PRIME_LINE_NO));
			yfcEleShipmentLine.setAttribute(TelstraConstants.SUB_LINE_NO, eleOrderLine.getAttribute(TelstraConstants.SUB_LINE_NO));
			yfcEleShipmentLine.setAttribute(TelstraConstants.STATUS_QUANTITY, eleOrderLine.getAttribute(TelstraConstants.ORDERED_QTY));

			YFCElement eleItem = eleOrderLine.getChildElement(TelstraConstants.ITEM);
			yfcEleShipmentLine.setAttribute(TelstraConstants.ITEM_ID, eleItem.getAttribute(TelstraConstants.ITEM_ID));
		}


		LoggerUtil.verboseLog("StockMovementOrderProcessing::ShipOrder::yfcDocShipOrderIp", logger,
				yfcDocShipOrderIp);

		invokeYantraService("GpsShipOrRejectOrder",yfcDocShipOrderIp);

	}

	/**
	 * 
	 * @param docCreateShipmentOp
	 */
	private void receiveOrder(YFCDocument docCreateShipmentOp, String sDocumentType) {

		YFCDocument docReceiveOrderIp = YFCDocument.createDocument(TelstraConstants.RECEIPT);
		YFCElement eleReceipt = docReceiveOrderIp.getDocumentElement();
		eleReceipt.setAttribute(TelstraConstants.DOCUMENT_TYPE, sDocumentType);
		eleReceipt.setAttribute(TelstraConstants.RECEIVING_NODE,
				(docConfirmDraftOrderOp.getElementsByTagName(TelstraConstants.ORDER_LINE).item(0))
				.getAttribute(TelstraConstants.RECEIVING_NODE));
		// shipment tag
		YFCElement eleShipment = docReceiveOrderIp.createElement(TelstraConstants.SHIPMENT);
		eleReceipt.appendChild(eleShipment);
		eleShipment.setAttribute(TelstraConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
		eleShipment.setAttribute(TelstraConstants.SHIPMENT_KEY,
				docCreateShipmentOp.getDocumentElement().getAttribute(TelstraConstants.SHIPMENT_KEY));
		eleShipment.setAttribute(TelstraConstants.RECEIVING_NODE,
				(docConfirmDraftOrderOp.getElementsByTagName(TelstraConstants.ORDER_LINE).item(0))
				.getAttribute(TelstraConstants.RECEIVING_NODE));
		// receipt lines
		YFCElement eleReceiptLines = docReceiveOrderIp.createElement(TelstraConstants.RECEIPT_LINES);
		eleReceipt.appendChild(eleReceiptLines);
		// receipt line
		YFCNodeList<YFCElement> nlOrderLine = docConfirmDraftOrderOp.getElementsByTagName(TelstraConstants.ORDER_LINE);
		for (YFCElement eleOrderLine : nlOrderLine) {
			YFCElement eleReceiptLine = docReceiveOrderIp.createElement(TelstraConstants.RECEIPT_LINE);
			eleReceiptLines.appendChild(eleReceiptLine);
			eleReceiptLine.setAttribute(TelstraConstants.PRIME_LINE_NO,
					eleOrderLine.getAttribute(TelstraConstants.PRIME_LINE_NO));
			eleReceiptLine.setAttribute(TelstraConstants.SUB_LINE_NO,
					eleOrderLine.getAttribute(TelstraConstants.SUB_LINE_NO));
			eleReceiptLine.setAttribute(TelstraConstants.QUANTITY,
					eleOrderLine.getAttribute(TelstraConstants.ORDERED_QTY));
		}

		LoggerUtil.verboseLog("StockMovementOrderProcessing::receiveReturnOrder::docReceiveOrderIp", logger,
				docReceiveOrderIp);

		invokeYantraApi(TelstraConstants.API_RECEIVE_ORDER, docReceiveOrderIp);

	}

	/**
	 * 
	 * @return
	 */
	private YFCDocument createReturnShipment() {

		YFCDocument docCreateShipmentIp = YFCDocument.createDocument(TelstraConstants.SHIPMENT);
		YFCElement eleShipment = docCreateShipmentIp.getDocumentElement();
		eleShipment.setAttribute(TelstraConstants.DOCUMENT_TYPE, TelstraConstants.DOCUMENT_TYPE_0003);
		eleShipment.setAttribute(TelstraConstants.RECEIVING_NODE,
				(docConfirmDraftOrderOp.getElementsByTagName(TelstraConstants.ORDER_LINE).item(0))
				.getAttribute(TelstraConstants.RECEIVING_NODE));
		
		//HUB-9259[START]
		eleShipment.setAttribute(TelstraConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
		//HUB-9259[END]		

		YFCElement eleShipmentLines = docCreateShipmentIp.createElement(TelstraConstants.SHIPMENT_LINES);
		eleShipment.appendChild(eleShipmentLines);

		YFCNodeList<YFCElement> nlOrderLine = docConfirmDraftOrderOp.getElementsByTagName(TelstraConstants.ORDER_LINE);
		for (YFCElement eleOrderLine : nlOrderLine) {

			YFCElement eleShipmentLine = docCreateShipmentIp.createElement(TelstraConstants.SHIPMENT_LINE);
			eleShipmentLines.appendChild(eleShipmentLine);
			eleShipmentLine.setAttribute(TelstraConstants.RECEIVING_NODE,
					eleOrderLine.getAttribute(TelstraConstants.RECEIVING_NODE));
			eleShipmentLine.setAttribute(TelstraConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
			eleShipmentLine.setAttribute(TelstraConstants.PRIME_LINE_NO,
					eleOrderLine.getAttribute(TelstraConstants.PRIME_LINE_NO));
			eleShipmentLine.setAttribute(TelstraConstants.SUB_LINE_NO,
					eleOrderLine.getAttribute(TelstraConstants.SUB_LINE_NO));
			eleShipmentLine.setAttribute(TelstraConstants.QUANTITY,
					eleOrderLine.getAttribute(TelstraConstants.ORDERED_QTY));

		}
		LoggerUtil.verboseLog("StockMovementOrderProcessing::createReturnShipment::docCreateShipmentIp ", logger,
				docCreateShipmentIp);
		YFCDocument docCreateShipmentOp = invokeYantraApi(TelstraConstants.API_CREATE_SHIPMENT, docCreateShipmentIp);
		return docCreateShipmentOp;
	}

	/**
	 * 
	 * @param docCreateShipmentOp
	 */

	private void confirmShipment(YFCDocument docCreateShipmentOp) {
		LoggerUtil.verboseLog("StockMovementOrderProcessing::receiveReturnOrder::confirmROShipment", logger,
				docCreateShipmentOp);
		invokeYantraApi(TelstraConstants.API_CONFIRM_SHIPMENT, docCreateShipmentOp);

	}

	/**
	 * 
	 * @param stockMovementIssueOrderType
	 * @param inDoc
	 */
	private void prepareAndSendMA01Msg(String stockMovementIssueOrderType, YFCDocument inDoc) {
		YFCDocument docMA01Ip = YFCDocument.createDocument(TelstraConstants.STOCK_MOVEMENT);
		YFCElement eleRoot = docMA01Ip.getDocumentElement();
		eleRoot.setAttribute(TelstraConstants.MESSAGE_TYPE, TelstraConstants.INTERFACE_NO_MA_01);
		//HUB-8803 - Begin
		long lNextSequenceNo = getServiceInvoker().getNextSequenceNumber(TelstraConstants.GPS_SEQUENCE_MA01);
		String sSequenceNo = String.valueOf(lNextSequenceNo);
		eleRoot.setAttribute(TelstraConstants.SEQUENCE_NO, sSequenceNo);
		//HUB-8803 - End
		YFCElement eleChildEle = docMA01Ip.importNode(inDoc.getDocumentElement(), true);
		eleRoot.appendChild(eleChildEle);

		//HUB-8803 - Begin
		YFCElement yfcEleReceipt = docMA01Ip.getElementsByTagName("Receipt").item(0);				
		String sNode = yfcEleReceipt.getAttribute(TelstraConstants.RECEIVING_NODE);
		YFCDocument docGetCustomerListOp = callGetCustomerList(sNode);
		YFCElement eleCustomer = docGetCustomerListOp.getElementsByTagName(TelstraConstants.CUSTOMER).item(0);

		if (!YFCCommon.isVoid(eleCustomer)) {			
			YFCElement eleBillingPersonInfo = docGetCustomerListOp
					.getElementsByTagName(TelstraConstants.BILLING_PERSON_INFO).item(0);

			if (!YFCCommon.isVoid(eleBillingPersonInfo)) {
				yfcEleReceipt.setAttribute(TelstraConstants.VIRTUAL_STORE_STATE,
						eleBillingPersonInfo.getAttribute(TelstraConstants.STATE));
			}
		}
		LoggerUtil.verboseLog("StockMovementOrderProcessing::prepareAndSendMA01Msg::MA01Msg ", logger, docMA01Ip);

		invokeYantraService(TelstraConstants.GPS_SEND_STOCK_MOVEMENT_MESSAGE, docMA01Ip);

	}

	/**
	 * 
	 */
	private void prepareAndSendMA02Msg() {

		YFCDocument docMA02Ip = YFCDocument.createDocument(TelstraConstants.STOCK_MOVEMENT);
		YFCElement eleRoot = docMA02Ip.getDocumentElement();
		eleRoot.setAttribute(TelstraConstants.MESSAGE_TYPE, TelstraConstants.INTERFACE_NO_MA_02);
		//HUB-8803 - Begin
		long lNextSequenceNo = getServiceInvoker().getNextSequenceNumber(TelstraConstants.GPS_SEQUENCE_MA02);
		String sSequenceNo = String.valueOf(lNextSequenceNo);
		eleRoot.setAttribute(TelstraConstants.SEQUENCE_NO, sSequenceNo);
		//HUB-8803 - End
		YFCElement yfcElePickOrderRoot = docGpsPickOrderIp.getDocumentElement();
		YFCElement eleChildEle = docMA02Ip.importNode(yfcElePickOrderRoot, true);
		eleRoot.appendChild(eleChildEle);
		//HUB-8803 - Begin
		String sNode = yfcElePickOrderRoot.getAttribute(TelstraConstants.SHIP_NODE);
		YFCDocument docGetCustomerListOp = callGetCustomerList(sNode);
		YFCElement eleCustomer = docGetCustomerListOp.getElementsByTagName(TelstraConstants.CUSTOMER).item(0);

		if (!YFCCommon.isVoid(eleCustomer)) {			
			YFCElement eleBillingPersonInfo = docGetCustomerListOp
					.getElementsByTagName(TelstraConstants.BILLING_PERSON_INFO).item(0);
			
			YFCElement yfcEleOrderLine = docConfirmDraftOrderOp.getElementsByTagName(TelstraConstants.ORDER_LINE).item(0);
			String sCostCenter = yfcEleOrderLine.getChildElement(TelstraConstants.EXTN).getAttribute(TelstraConstants.DIVISION);
			if (!YFCCommon.isVoid(eleBillingPersonInfo)) {
				eleChildEle.setAttribute(TelstraConstants.VIRTUAL_STORE_STATE,
						eleBillingPersonInfo.getAttribute(TelstraConstants.STATE));
				eleChildEle.setAttribute(TelstraConstants.STORE_DAC_CODE,
						eleBillingPersonInfo.getAttribute(TelstraConstants.PERSON_ID));
				eleChildEle.setAttribute(TelstraConstants.COST_CENTER,sCostCenter);
				eleChildEle.setAttribute(TelstraConstants.STORE_ADDRESS_LINE_1,
						eleBillingPersonInfo.getAttribute(TelstraConstants.ADDRESS_LINE_1));
				eleChildEle.setAttribute(TelstraConstants.STORE_ADDRESS_LINE_2,
						eleBillingPersonInfo.getAttribute(TelstraConstants.ADDRESS_LINE_2));
				eleChildEle.setAttribute(TelstraConstants.STORE_ADDRESS_LINE_3,
						eleBillingPersonInfo.getAttribute(TelstraConstants.ADDRESS_LINE_3));
				eleChildEle.setAttribute(TelstraConstants.STORE_CITY,
						eleBillingPersonInfo.getAttribute(TelstraConstants.CITY));
				eleChildEle.setAttribute(TelstraConstants.STORE_COUNTRY,
						eleBillingPersonInfo.getAttribute(TelstraConstants.COUNTRY));
				eleChildEle.setAttribute(TelstraConstants.STORE_NAME,
						eleBillingPersonInfo.getAttribute(TelstraConstants.FIRST_NAME));
				eleChildEle.setAttribute(TelstraConstants.STORE_ZIP_CODE,
						eleBillingPersonInfo.getAttribute(TelstraConstants.ZIP_CODE));
			}
		}
		//HUB-8803 - End
		LoggerUtil.verboseLog("StockMovementOrderProcessing::prepareAndSendMA02Msg::MA02Msg ", logger, docMA02Ip);

		invokeYantraService(TelstraConstants.GPS_SEND_STOCK_MOVEMENT_MESSAGE, docMA02Ip);

	}

	/**
	 * 
	 * @param sOrderType
	 * @param sNode
	 */
	private void prepareAndSendMA03Msg(String sOrderType, String sNode) {

		YFCDocument docMA03Msg = YFCDocument.createDocument(TelstraConstants.STOCK_MOVEMENT);
		YFCElement eleRoot = docMA03Msg.getDocumentElement();
		eleRoot.setAttribute(TelstraConstants.MESSAGE_TYPE, TelstraConstants.INTERFACE_NO_MA_03);
		//HUB-8803 - Begin
		long lNextSequenceNo = getServiceInvoker().getNextSequenceNumber(TelstraConstants.GPS_SEQUENCE_MA03);
		String sSequenceNo = String.valueOf(lNextSequenceNo);
		eleRoot.setAttribute(TelstraConstants.SEQUENCE_NO, sSequenceNo);
		//HUB-8803 - End
		YFCElement eleItems = docMA03Msg.createElement(TelstraConstants.ITEMS);
		eleRoot.appendChild(eleItems);

		YFCDocument docGetCustomerListOp = callGetCustomerList(sNode);
		YFCElement eleCustomer = docGetCustomerListOp.getElementsByTagName(TelstraConstants.CUSTOMER).item(0);
		if (!YFCCommon.isVoid(eleCustomer)) {
			updateItemsElement(docGetCustomerListOp, sNode, eleItems, sOrderType);
			YFCNodeList<YFCElement> nlOrderLine = docConfirmDraftOrderOp
					.getElementsByTagName(TelstraConstants.ORDER_LINE);
			for (YFCElement eleOrderLine : nlOrderLine) {

				YFCElement eleItem = docMA03Msg.createElement(TelstraConstants.ITEM);
				eleItems.appendChild(eleItem);

				eleItem.setAttribute(TelstraConstants.QUANTITY,
						eleOrderLine.getAttribute(TelstraConstants.ORDERED_QTY));

				YFCElement eleItemOrder = eleOrderLine.getElementsByTagName(TelstraConstants.ITEM).item(0);
				eleItem.setAttribute(TelstraConstants.ITEM_ID, eleItemOrder.getAttribute(TelstraConstants.ITEM_ID));
				eleItem.setAttribute(TelstraConstants.UNIT_OF_MEASURE,
						eleItemOrder.getAttribute(TelstraConstants.UNIT_OF_MEASURE));
			}

		}
		LoggerUtil.verboseLog("StockMovementOrderProcessing::prepareAndSendMA03Msg::MA03Msg ", logger, docMA03Msg);
		invokeYantraService(TelstraConstants.GPS_SEND_STOCK_MOVEMENT_MESSAGE, docMA03Msg);

	}

	/**
	 * 
	 * @param sShipNode
	 * @return
	 */
	private YFCDocument callGetCustomerList(String sShipNode) {

		YFCDocument docGetCustomerListIp = YFCDocument
				.getDocumentFor("<Customer ExternalCustomerID='" + sShipNode + "'/>");
		LoggerUtil.verboseLog("StockMovementOrderProcessing::callGetCustomerList::docGetCustomerListIp ", logger,
				docGetCustomerListIp);
		YFCDocument docGetCustomerListTemp = YFCDocument.getDocumentFor(
				"<Customer ExternalCustomerID=\"\"> <Consumer BillingAddressKey=\"\"> <BillingPersonInfo AddressLine1=\"\" AddressLine2=\"\" AddressLine3=\"\" State=\"\" PersonID=\"\" City=\"\" Country=\"\" FirstName=\"\" ZipCode=\"\" /> </Consumer> </Customer>");
		LoggerUtil.verboseLog("FinancialAdjustmentMessage :: updateItemsElement :: getCustomerList template doc\n",
				logger, docGetCustomerListTemp);
		YFCDocument docGetCustomerListOp = getServiceInvoker().invokeYantraApi(TelstraConstants.API_GET_CUSTOMER_LIST,
				docGetCustomerListIp, docGetCustomerListTemp);

		LoggerUtil.verboseLog("StockMovementOrderProcessing::callGetCustomerList::docGetCustomerListOp ", logger,
				docGetCustomerListOp);
		return docGetCustomerListOp;

	}

	/**
	 * 
	 * @param docGetCustomerListOp
	 * @param sNode
	 * @param eleItems
	 * @param sOrderType
	 */
	private void updateItemsElement(YFCDocument docGetCustomerListOp, String sNode, YFCElement eleItems,
			String sOrderType) {

		YFCElement eleBillingPersonInfo = docGetCustomerListOp
				.getElementsByTagName(TelstraConstants.BILLING_PERSON_INFO).item(0);
		if (!YFCCommon.isVoid(eleBillingPersonInfo)) {
			eleItems.setAttribute(TelstraConstants.VIRTUAL_STORE_STATE,
					eleBillingPersonInfo.getAttribute(TelstraConstants.STATE));
			eleItems.setAttribute(TelstraConstants.STORE_DAC_CODE,
					eleBillingPersonInfo.getAttribute(TelstraConstants.PERSON_ID));
			eleItems.setAttribute(TelstraConstants.STORE_ADDRESS_LINE_1,
					eleBillingPersonInfo.getAttribute(TelstraConstants.ADDRESS_LINE_1));
			eleItems.setAttribute(TelstraConstants.STORE_CITY,
					eleBillingPersonInfo.getAttribute(TelstraConstants.CITY));
			eleItems.setAttribute(TelstraConstants.STORE_COUNTRY,
					eleBillingPersonInfo.getAttribute(TelstraConstants.COUNTRY));
			eleItems.setAttribute(TelstraConstants.STORE_NAME,
					eleBillingPersonInfo.getAttribute(TelstraConstants.FIRST_NAME));
			eleItems.setAttribute(TelstraConstants.STORE_ZIP_CODE,
					eleBillingPersonInfo.getAttribute(TelstraConstants.ZIP_CODE));
			
			YFCElement yfcEleOrderLine = docConfirmDraftOrderOp.getElementsByTagName(TelstraConstants.ORDER_LINE).item(0);
			String sCostCenter = yfcEleOrderLine.getChildElement(TelstraConstants.EXTN).getAttribute(TelstraConstants.DIVISION);
			
			eleItems.setAttribute(TelstraConstants.COST_CENTER,sCostCenter);
			eleItems.setAttribute(TelstraConstants.NETWORK_ID,
					XPathUtil.getXpathAttribute(docConfirmDraftOrderOp, "//Reference[@Name='PM_ORDER']/@Value"));
			eleItems.setAttribute(TelstraConstants.ACTIVITY_CODE,
					XPathUtil.getXpathAttribute(docConfirmDraftOrderOp, "//Reference[@Name='ACTIVITY_TYPE']/@Value"));
			eleItems.setAttribute(TelstraConstants.ORDER_TYPE, sOrderType);
			eleItems.setAttribute(TelstraConstants.NODE, sNode);
		}
	}
}
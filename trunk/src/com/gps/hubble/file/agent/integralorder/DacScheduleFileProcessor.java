package com.gps.hubble.file.agent.integralorder;


import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.constants.TelstraErrorCodeConstants;
import com.gps.hubble.file.FileProcessor;
import com.gps.hubble.file.Row;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSException;

/**
 * 
 * @author Prateek
 *
 */
public class DacScheduleFileProcessor extends FileProcessor{
	private static YFCLogCategory logger = YFCLogCategory.instance(DacScheduleFileProcessor.class);
	
	/**
	 * 
	 */
	public void processRow(Row row) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "processRow", row);
		String node = row.getCellValue( 0);
		String dac = row.getCellValue( 1);
		if(YFCObject.isVoid(node) || YFCObject.isVoid(dac)){
			LoggerUtil.verboseLog("Either node or dac missing", logger, " .This row will be moved to error");
//			YFCDocument erroDoc = ExceptionUtil
//					.getYFSExceptionDocument(TelstraErrorCodeConstants.DAC_SCHEDULE_NODE_OR_DAC_MISSING_ERROR_CODE);
//			throw new YFSException(erroDoc.toString());
			throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.DAC_SCHEDULE_NODE_OR_DAC_MISSING_ERROR_CODE, new YFSException());
		}
		String dc = getDCFromCommonCode(node);
		if(YFCObject.isVoid(dc)){
			LoggerUtil.verboseLog("Either node to dc mapping is not present for node"+node, logger, " .This row will be moved to error");
//			YFCDocument erroDoc = ExceptionUtil
//					.getYFSExceptionDocument(TelstraErrorCodeConstants.DAC_SCHEDULE_NODE_DC_MAPPING_MISSING_ERROR_CODE);
//			throw new YFSException(erroDoc.toString());
			throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.DAC_SCHEDULE_NODE_DC_MAPPING_MISSING_ERROR_CODE, new YFSException());
		}
		YFCDocument dacSchedule = YFCDocument.createDocument(TelstraConstants.DAC_SCHEDULE);
		YFCElement dacScheduleElem = dacSchedule.getDocumentElement();
		dacScheduleElem.setAttribute("Node", dc);
		dacScheduleElem.setAttribute(TelstraConstants.DELIVERY_ADDRESS_CODE, dac.toUpperCase());
		
		YFCDocument dacScheduleOutput = getServiceInvoker().invokeYantraService(TelstraConstants.API_GET_DAC_SCHEDULE, dacSchedule);
		String schedule = row.getCellValue( 2);
		String routeId = row.getCellValue( 3);
		String destId = row.getCellValue( 4);
		if(!YFCObject.isVoid(schedule)){
			schedule = schedule.toUpperCase();
		}
		if(!YFCObject.isVoid(routeId)){
			routeId = routeId.toUpperCase();
		}
		if(!YFCObject.isVoid(destId)){
			destId = destId.toUpperCase();
		}
		dacScheduleElem.setAttribute(TelstraConstants.SCHEDULE, schedule);
		dacScheduleElem.setAttribute(TelstraConstants.ROUTE_ID, routeId);
		dacScheduleElem.setAttribute(TelstraConstants.DEST_ID, destId);
		if(isRecordExist(dacScheduleOutput)){
			getServiceInvoker().invokeYantraService(TelstraConstants.API_CHANGE_DAC_SCHEDULE, dacSchedule);
		}else{
			getServiceInvoker().invokeYantraService(TelstraConstants.API_CREATE_DAC_SCHEDULE, dacSchedule);
		}
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "processRow", row);
		
	}
	
	private boolean isRecordExist(YFCDocument dacSchdule){
		if(dacSchdule == null || dacSchdule.getDocumentElement() == null  || dacSchdule.getDocumentElement().getAttributes() == null || dacSchdule.getDocumentElement().getAttributes().isEmpty()){
			return false;
		}
		return true;
	}
	
	private String getDCFromCommonCode(String node){
		YFCDocument docCommonCode = YFCDocument.getDocumentFor("<CommonCode SystemDefinedCode='N' CodeType='DC_CITY_MAP' CodeValue='"+node+"' />");
		YFCDocument docCommonCodeOutput = getServiceInvoker().invokeYantraApi(TelstraConstants.API_GET_COMMON_CODE_LIST, docCommonCode);
		if(docCommonCodeOutput == null || docCommonCodeOutput.getDocumentElement() == null || docCommonCodeOutput.getDocumentElement().getFirstChildElement() == null){
			return null;
		}
		return docCommonCodeOutput.getDocumentElement().getFirstChildElement().getAttribute(TelstraConstants.CODE_SHORT_DESCRIPTION);
	}

}

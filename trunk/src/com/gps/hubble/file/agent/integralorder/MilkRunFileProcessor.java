package com.gps.hubble.file.agent.integralorder;

import java.text.SimpleDateFormat;



import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.constants.TelstraErrorCodeConstants;
import com.gps.hubble.file.FileProcessor;
import com.gps.hubble.file.Row;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.date.YDate;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSException;
/**
 * 
 * @author Prateek
 *
 */
public class MilkRunFileProcessor extends FileProcessor{
	private static YFCLogCategory logger = YFCLogCategory.instance(MilkRunFileProcessor.class);

	/**
	 * 
	 */
	public void processRow(Row row) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "processRow", row);
		String routeId = row.getCellValue( 0);
		if(YFCObject.isVoid(routeId)){
			LoggerUtil.verboseLog("Mandatory parameter for milk run is missing", logger, " .This row will be moved to error");
//			YFCDocument erroDoc = ExceptionUtil
//					.getYFSExceptionDocument(TelstraErrorCodeConstants.MILK_RUN_ROUTE_ID_MISSING_ERROR_CODE);
//			throw new YFSException(erroDoc.toString());
			throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.MILK_RUN_ROUTE_ID_MISSING_ERROR_CODE, new YFSException());
		}
		YFCDocument milkRunSchedule = YFCDocument.createDocument("MilkRunSchedule");
		YFCElement milkRunScheduleElem = milkRunSchedule.getDocumentElement();
		milkRunScheduleElem.setAttribute(TelstraConstants.ROUTE_ID, routeId);
		YFCDocument milkRunScheduleOutput = getServiceInvoker().invokeYantraService(TelstraConstants.API_GET_MILK_RUN_SCHEDULE, milkRunSchedule);
		String milkRunName = row.getCellValue( 1);
		if(!YFCObject.isVoid(milkRunName)){
			milkRunName = milkRunName.toUpperCase();
		}
		String weekDay = row.getCellValue( 2);
		if(!YFCObject.isVoid(weekDay)){
			weekDay = weekDay.toUpperCase();
		}
		String frequency = row.getCellValue( 3);
		if(!YFCObject.isVoid(frequency)){
			frequency = frequency.toUpperCase();
		}
		String comments = row.getCellValue( 4);
		SimpleDateFormat sdf = new SimpleDateFormat(TelstraConstants.TELSTRA_DATE_TIME_FORMAT);
		String effectiveFromStr = row.getCellValue( 5,sdf);
		String effectiveToStr = row.getCellValue( 6,sdf);
		milkRunScheduleElem.setAttribute("MilkRunName", milkRunName);
		milkRunScheduleElem.setAttribute("Weekday", weekDay);
		milkRunScheduleElem.setAttribute("Frequency", frequency);
		milkRunScheduleElem.setAttribute("Comments", comments);
		if(!YFCObject.isVoid(effectiveFromStr)){
			milkRunScheduleElem.setAttribute("EffectiveFrom", convertToYDate(effectiveFromStr));
		}
		if(!YFCObject.isVoid(effectiveToStr)){
			milkRunScheduleElem.setAttribute("EffectiveTo", convertToYDate(effectiveToStr));
		}
		if(isRecordExist(milkRunScheduleOutput)){
			getServiceInvoker().invokeYantraService(TelstraConstants.API_CHANGE_MILK_RUN_SCHEDULE, milkRunSchedule);
		}else{
			getServiceInvoker().invokeYantraService(TelstraConstants.API_CREATE_MILK_RUN_SCHEDULE, milkRunSchedule);
		}
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "processRow", row);
	}
	
	
	private static YDate convertToYDate(String dateTimeString){
		return new YDate(dateTimeString, TelstraConstants.TELSTRA_DATE_TIME_FORMAT, true);
	}

	
	private boolean isRecordExist(YFCDocument milkRunScheduleOutput){
		if(milkRunScheduleOutput == null || milkRunScheduleOutput.getDocumentElement() == null || milkRunScheduleOutput.getDocumentElement().getAttributes() == null || milkRunScheduleOutput.getDocumentElement().getAttributes().isEmpty()){
			return false;
		}
		return true;
	}

}

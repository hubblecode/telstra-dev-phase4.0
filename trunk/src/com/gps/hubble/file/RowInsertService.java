package com.gps.hubble.file;

import org.apache.commons.lang.StringUtils;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.StringUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;

public class RowInsertService extends AbstractCustomApi{
	private static YFCLogCategory logger = YFCLogCategory.instance(RowInsertService.class);
	
	public YFCDocument invoke(YFCDocument docRowList){
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", docRowList);
		if(docRowList == null || docRowList.getDocumentElement() == null || docRowList.getDocumentElement().getChildren() == null){
			return docRowList;
		}
		
		YFCElement elemRowList = docRowList.getDocumentElement();
		String sFileUploadKey = ((YFCElement)elemRowList.getFirstChild()).getAttribute(TelstraConstants.FILE_UPLOAD_KEY);
		/*
		 * Setting the status to Reading File and percentage completion as 0 which tell us that the reading file has not started yet
		 */
		updateFileUploadWithReadingFileStatus(sFileUploadKey,TelstraConstants.READING_FILE, "0");
		int iRowLength = elemRowList.getChildNodes().getLength() - 1;
		double dPerCentageBreakup = getPercentageBreakupFromCommonCode(TelstraConstants.EXCEL_PERCENTAGE_INCREMENT, TelstraConstants.PERCENTAGE);		
		double dNoOfRowPerUpdateReq = (iRowLength*dPerCentageBreakup)/100;
		    	
    	if(dNoOfRowPerUpdateReq<1){
    		    		
    		double dIncrementFactor = dPerCentageBreakup;
    		
    		while(dNoOfRowPerUpdateReq < 1){
    			dPerCentageBreakup = dPerCentageBreakup+dIncrementFactor;
    			dNoOfRowPerUpdateReq = (iRowLength*dPerCentageBreakup)/100;			
    			
    		}
    	}
    	LoggerUtil.verboseLog("RowInsertService :: invoke :: RowsPerUpdate before rouding off", logger, dNoOfRowPerUpdateReq);
    	int iNoOfRowPerUpdateReq = (int) Math.ceil(dNoOfRowPerUpdateReq);
    	LoggerUtil.verboseLog("RowInsertService :: invoke :: RowsPerUpdate after rouding off", logger, iNoOfRowPerUpdateReq);
		int i=0;
		for(YFCElement elemRow : elemRowList.getChildren()){
			i++;
			YFCDocument docGetFileUploadRow = getExistingFileUploadRow(elemRow);
			if(!isRecordExist(docGetFileUploadRow)){
				YFCDocument docCreateFileUploadRow = YFCDocument.getDocumentFor(elemRow.getString());
				/*
				 * if the
				 */
				if(i==iNoOfRowPerUpdateReq){
					i=0;
					/*
					 * Set the percent increment to in the file upload row. This will help to tell how much percent of the row has be inserted.
					 * This will also help in determining percentage completion of the excel sheet
					 */
					String sPercentIncrement = String.valueOf(dPerCentageBreakup);					
					docCreateFileUploadRow.getDocumentElement().setAttribute(TelstraConstants.PERCENT_INCREMENT, sPercentIncrement);										
					if(YFCCommon.isStringVoid(sFileUploadKey)){
					updateFileUploadRecord(sFileUploadKey, sPercentIncrement);
					}
				}
			    invokeYantraService("GpsCreateFileUploadRow", docCreateFileUploadRow);
			}
		}
		/*
		 * Setting the status to Reading File and percentage completion as 100 which tell us that the reading file process has been completed
		 */
		updateFileUploadWithReadingFileStatus(sFileUploadKey, TelstraConstants.PROCESSING_FILE,"0");
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", docRowList);
		return docRowList;
	}
	
	/**
	 * This method will update the status of the file upload to Reading file and percentage as 0
	 * @param sFileUploadKey
	 * @param sPercentCompleted 
	 */
	private void updateFileUploadWithReadingFileStatus(String sFileUploadKey, String sStatus, String sPercentCompleted) {
		
		YFCDocument yfcDocChangeFileUploadIp = YFCDocument.createDocument(TelstraConstants.FILE_UPLOAD);
		YFCElement yfcEleChangeFileUploadIp = yfcDocChangeFileUploadIp.getDocumentElement();
		yfcEleChangeFileUploadIp.setAttribute(TelstraConstants.FILE_UPLOAD_KEY, sFileUploadKey);
		yfcEleChangeFileUploadIp.setAttribute(TelstraConstants.FILE_PROGRESSED, sPercentCompleted);
		yfcEleChangeFileUploadIp.setAttribute(TelstraConstants.STATUS, sStatus);
		LoggerUtil.verboseLog("RowInsertService :: updateFileUploadWithReadingFileStatus :: yfcDocChangeFileUploadIp", logger, yfcDocChangeFileUploadIp);
		try{
			invokeYantraService(TelstraConstants.SERVICE_GPS_CHANGE_FILE_UPLOAD, yfcDocChangeFileUploadIp);
		}
		catch(Exception e){
			LoggerUtil.errorLog("Something went wrong while updating the status percent", logger, e);
		}		
	}

	/**
	 * This method will check the percentage completion of the process. If it is less than 100, then it will increment it by the passed number
	 * @param sFileUploadKey
	 * @param sPercentIncrement
	 */
	private void updateFileUploadRecord(String sFileUploadKey, String sPercentIncrement) {

		LoggerUtil.verboseLog("RowInsertService :: updateFileUploadRecord :: sFileUploadKey", logger, sFileUploadKey);
		YFCDocument yfcDocFileUpload = YFCDocument.createDocument(TelstraConstants.FILE_UPLOAD);
		yfcDocFileUpload.getDocumentElement().setAttribute(TelstraConstants.FILE_UPLOAD_KEY, sFileUploadKey);				
		YFCDocument yfcDocGetFileUploadListOp = invokeYantraService(TelstraConstants.SERVICE_GPS_GET_FILE_UPLOAD_LIST, yfcDocFileUpload);
		LoggerUtil.verboseLog("RowInsertService :: updateFileUploadRecord :: yfcDocGetFileUploadListOp", logger, yfcDocGetFileUploadListOp);
		YFCElement yfcEleFileUpload = yfcDocGetFileUploadListOp.getDocumentElement().getChildElement(TelstraConstants.FILE_UPLOAD);
		if(!YFCCommon.isVoid(yfcEleFileUpload)){
			String sFileProgressed = yfcEleFileUpload.getAttribute(TelstraConstants.FILE_PROGRESSED);
			int iFileProgressed = Integer.parseInt(sFileProgressed);
			
			if(iFileProgressed < 100){
				iFileProgressed = iFileProgressed + Integer.valueOf(sPercentIncrement);				
				YFCDocument yfcDocChangeFileUploadIp = YFCDocument.createDocument(TelstraConstants.FILE_UPLOAD);
				YFCElement yfcEleChangeFileUploadIp = yfcDocChangeFileUploadIp.getDocumentElement();
				yfcEleChangeFileUploadIp.setAttribute(TelstraConstants.FILE_UPLOAD_KEY, sFileUploadKey);
				yfcEleChangeFileUploadIp.setAttribute(TelstraConstants.FILE_PROGRESSED, String.valueOf(iFileProgressed));
				LoggerUtil.verboseLog("RowInsertService :: updateFileUploadRecord :: yfcDocChangeFileUploadIp", logger, yfcDocChangeFileUploadIp);
				try{
					invokeYantraService(TelstraConstants.SERVICE_GPS_CHANGE_FILE_UPLOAD, yfcDocChangeFileUploadIp);
				}
				catch(Exception e){
					LoggerUtil.errorLog("Something went wrong while updating the status percent", logger, e);
				}
			}
		}
	}

	/**
	 * 
	 * @param sCodeType
	 * @param sCodeValue
	 * @return
	 */
	private int getPercentageBreakupFromCommonCode(String sCodeType , String sCodeValue) {
		YFCDocument yfcDocCommonCodeIp = YFCDocument.getDocumentFor("<CommonCode CodeType='"+sCodeType+"' CodeValue='"+sCodeValue+"'/>");
		YFCDocument yfcDocCommonCodeTemp = YFCDocument.getDocumentFor("<CommonCodeList><CommonCode CodeShortDescription='' CodeType='' CodeValue='' /></CommonCodeList>");
		YFCDocument yfcDocCommonCodeOp = invokeYantraApi(TelstraConstants.API_GET_COMMON_CODE_LIST, yfcDocCommonCodeIp, yfcDocCommonCodeTemp);
		
		String sShortDesc = XPathUtil.getXpathAttribute(yfcDocCommonCodeOp, "//CommonCode/@CodeShortDescription");
		if(YFCCommon.isStringVoid(sShortDesc)|| !StringUtils.isNumeric(sShortDesc)){
			sShortDesc = "5";
		}
		
		return Integer.valueOf(sShortDesc);
	}

	
	
	private YFCDocument getExistingFileUploadRow(YFCElement elemRow){
		YFCDocument docFileUploadRows = YFCDocument.createDocument("FileUploadRow");
		YFCElement elemFileUploadRows = docFileUploadRows.getDocumentElement();
		elemFileUploadRows.setAttribute("TransactionType", elemRow.getAttribute("TransactionType"));
		elemFileUploadRows.setAttribute("FileUploadKey", elemRow.getAttribute("FileUploadKey"));
		elemFileUploadRows.setAttribute("RowNumber", elemRow.getIntAttribute("RowNumber"));
		return invokeYantraService("GpsGetFileUploadRow", docFileUploadRows);
	}
	
	private boolean isRecordExist(YFCDocument output){
		if(output == null || output.getDocumentElement() == null || output.getDocumentElement().getAttributes() == null || output.getDocumentElement().getAttributes().isEmpty()){
			return false;
		}
		return true;
	}
}

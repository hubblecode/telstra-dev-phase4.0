package com.gps.hubble.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.date.YDate;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSException;

/**
 * 
 * @author Prateek
 *
 */
public class GpsCompleteOrder extends AbstractCustomApi {

	private static YFCLogCategory logger = YFCLogCategory.instance(GpsCompleteOrder.class);

	private int iRetentionDays = 0;
	private YFCDocument yfcDocGetOrderListOp = null;

	/**
	 * ServiceName : GpsCompleteOrder Input Xml
	 * <Order OrderName='' OrderNo='' RetentionDays='' ToDate=
	 * '2017-06-28T23:59:59-04:00' PrimeLineNo='' DeliveredQty=''/>
	 */
	@Override
	public YFCDocument invoke(YFCDocument yfcInDoc) throws YFSException {

		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", yfcInDoc);

		yfcDocGetOrderListOp = callGetOrderListApi(yfcInDoc);

		YFCElement yfcEleRoot = yfcInDoc.getDocumentElement();
		String sOrderNameIp = yfcEleRoot.getAttribute(TelstraConstants.ORDER_NAME);
		String sOrderNoIp = yfcEleRoot.getAttribute(TelstraConstants.ORDER_NO);
		String sPrimeLineNoIp = "";		
		double dDeliveredQty = 0.00;

		if(!YFCCommon.isStringVoid(sOrderNoIp) || !YFCCommon.isStringVoid(sOrderNameIp)){
			sPrimeLineNoIp = yfcEleRoot.getAttribute(TelstraConstants.PRIME_LINE_NO);
			dDeliveredQty = yfcEleRoot.getDoubleAttribute(TelstraConstants.DELIVERED_QTY,0.00);
		}

		for (YFCElement yfcEleOrder : yfcDocGetOrderListOp.getElementsByTagName(TelstraConstants.ORDER)) {

			String sOrderName = yfcEleOrder.getAttribute(TelstraConstants.ORDER_NAME);
			String sDocumentType = yfcEleOrder.getAttribute(TelstraConstants.DOCUMENT_TYPE);
			if (!TelstraConstants.DOCUMENT_TYPE_0003.equalsIgnoreCase(sDocumentType)) {

				for (YFCElement yfcEleOrderLine : yfcEleOrder.getElementsByTagName(TelstraConstants.ORDER_LINE)) {

					/* if the prime line no is passed either with order name or order no, then processed only that line*/
					String sPrimeLineNo = yfcEleOrderLine.getAttribute(TelstraConstants.PRIME_LINE_NO);
					if(!YFCCommon.isStringVoid(sPrimeLineNoIp) && !sPrimeLineNoIp.equalsIgnoreCase(sPrimeLineNo)){
						continue;
					}

					TreeMap<String, Double> mapStatusQty = new TreeMap<>(Collections.reverseOrder());

					boolean bCloseLine = prepareStatusQtyMap(mapStatusQty, yfcEleOrderLine);

					if (bCloseLine) {
						closeOrderLine(mapStatusQty,yfcEleOrderLine,sOrderName,sDocumentType, dDeliveredQty);
					}
				}
			}
		}
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", yfcInDoc);
		return yfcInDoc;
	}

	/**
	 * 
	 * @param mapStatusQty
	 * @param yfcEleOrderLine
	 * @param sOrderName
	 * @param sDocumentType
	 * @param dDeliveredQty 
	 */
	private void closeOrderLine(TreeMap<String, Double> mapStatusQty, YFCElement yfcEleOrderLine, String sOrderName, String sDocumentType, double dDeliveredQty) {

		boolean bConsiderDeliveredQty = false;
		if(dDeliveredQty>0){
			bConsiderDeliveredQty = true;
		}
		for (Map.Entry<String, Double> entry : mapStatusQty.entrySet()) {
			String sStatus = entry.getKey();
			String[] parts = sStatus.split("-");
			sStatus = parts[0];

			/*if the status qty is cancelled or futile or rejected or back ordered from node (it will be taken care in the back ordered qty), ignore it*/
			if ("9000".equalsIgnoreCase(sStatus) || "9000.10000".equalsIgnoreCase(sStatus)
					|| "9000.30000".equalsIgnoreCase(sStatus) || "1400".equalsIgnoreCase(sStatus)) {
				continue;
			}

			double dQty = entry.getValue();
			YFCDocument yfcDoc = null;

			/* if some of the qty of SO is delivered or received for the other order, consider this qty part of the qty to delivered */
			if ((TelstraConstants.DOCUMENT_TYPE_0001.equalsIgnoreCase(sDocumentType)
					&& "3700.7777".equalsIgnoreCase(sStatus)) || ("3900".equalsIgnoreCase(sStatus))) {
				if(bConsiderDeliveredQty){
					dDeliveredQty = dDeliveredQty - dQty;
				}
				continue;
			}
			/* if qty to delivered is zero which means all the qty which was supposed to be delivered has completed its life cycle */
			if(bConsiderDeliveredQty && dDeliveredQty<=0){
				break;	
			}

			if(bConsiderDeliveredQty){	
				/* if remaining qty to delivered is less than the qty which can be delivered, delivered only remaining qty*/
				if(dDeliveredQty<dQty){
					dQty = dDeliveredQty;
				}
				else{
					dDeliveredQty = dDeliveredQty - dQty;
				}				
			}	

			if (TelstraConstants.DOCUMENT_TYPE_0001.equalsIgnoreCase(sDocumentType)) {						
				yfcDoc = prepareCarrierUpdateMessage(yfcEleOrderLine, sOrderName, dQty);
				double dStatus = Double.parseDouble(sStatus);
				if(dStatus>=3700){
					updateCarrierMsgWithTrackingNo(yfcDoc, sStatus, entry.getValue(), yfcEleOrderLine.getAttribute(TelstraConstants.PRIME_LINE_NO));
				}
			}			
			else {			
				yfcDoc = prepareReceiptMessage(yfcEleOrderLine, sOrderName, dQty);			
			}
			yfcDoc.getDocumentElement().setAttribute(TelstraConstants.DOCUMENT_TYPE, sDocumentType);

			LoggerUtil.verboseLog("GpsProcessCompleteOrderCycleDropQ :: Input document", logger, yfcDoc);
			invokeYantraService("GpsProcessCompleteOrderCycleDropQ", yfcDoc);

		}		
	}

	/**
	 * 
	 * @param yfcDoc
	 * @param sStatus
	 * @param sPrimeLineNo 
	 * @param dQty 
	 */
	private void updateCarrierMsgWithTrackingNo(YFCDocument yfcDoc, String sStatus, Double dQty, String sPrimeLineNo) {

		String sOrderHeaderKey = XPathUtil.getXpathAttribute(yfcDocGetOrderListOp, "//Order/@OrderHeaderKey");
		String sQty = String.valueOf(dQty)+"0";
		if (!YFCCommon.isStringVoid(sOrderHeaderKey)) {

			YFCDocument yfcDocGetShipmentListIp = YFCDocument
					.getDocumentFor("<Shipment Status='" + getShipmentStatus(sStatus) + "' OrderHeaderKey='" + sOrderHeaderKey + "'/>");
			YFCDocument yfcDocGetShipmentListTemp = YFCDocument.getDocumentFor(
					"<Shipments><Shipment ShipmentNo='' Status='' TrackingNo=''><ShipmentLines><ShipmentLine OrderHeaderKey='' OrderNo='' PrimeLineNo='' Quantity=''/></ShipmentLines></Shipment></Shipments>");

			YFCDocument yfcDocGetShipmentListOp = invokeYantraApi(TelstraConstants.API_GET_SHIPMENT_LIST,
					yfcDocGetShipmentListIp, yfcDocGetShipmentListTemp);
			
			
			YFCElement yfcEleShipment = XPathUtil.getXPathElement(yfcDocGetShipmentListOp, "//Shipment[ShipmentLines/ShipmentLine[@PrimeLineNo='"+sPrimeLineNo+"' and @Quantity='"+sQty+"']]");
			
			if(!YFCCommon.isVoid(yfcEleShipment)){				
				yfcDoc.getDocumentElement().setAttribute(TelstraConstants.TRACKING_NO, yfcEleShipment.getAttribute(TelstraConstants.TRACKING_NO));
				yfcDoc.getDocumentElement().setAttribute(TelstraConstants.SHIPMENT_NO, yfcEleShipment.getAttribute(TelstraConstants.SHIPMENT_NO));
			}			
		}
	}
	
	/**
	 * 
	 * @param sStatus
	 * @return
	 */
	private String getShipmentStatus(String sStatus) {
		
		String sShipmentStatus = "";
		switch (sStatus) {
		case "3700":  sShipmentStatus = "1400";
        break;
        
		case "3700.10000":  sShipmentStatus = "1400.10000";
        break;
		
		case "3700.7777":  sShipmentStatus = "1500";
        break;
		
		default: sShipmentStatus = "1500";
        break;
        
		}
		return sShipmentStatus;
	}

	/**
	 * 
	 * @param mapStatusQty
	 * @param yfcEleOrderLine 
	 * @return
	 */
	private boolean prepareStatusQtyMap(TreeMap<String, Double> mapStatusQty, YFCElement yfcEleOrderLine) {

		boolean bCloseLine = true;
		List<YDate> lStatusDate = new ArrayList<>();
		int i = 0;
		for (YFCElement yfcEleOrderStatus : yfcEleOrderLine.getElementsByTagName(TelstraConstants.ORDER_STATUS)) {
			i++;
			String sStatus = yfcEleOrderStatus.getAttribute(TelstraConstants.STATUS);
			double dStatusQty = yfcEleOrderStatus.getDoubleAttribute(TelstraConstants.STATUS_QTY);
			mapStatusQty.put(sStatus + "-" + i, dStatusQty);

			YDate ydStatusDate = yfcEleOrderStatus.getYDateAttribute(TelstraConstants.STATUS_DATE);
			lStatusDate.add(ydStatusDate);
		}		
		/*
		 * Sorting the status date from max to min .
		 */		
		if (iRetentionDays > 0) {
			Comparator<YDate> comparator = Collections.reverseOrder();
			Collections.sort(lStatusDate, comparator);

			YDate ydLatestStatusDate = lStatusDate.get(0);

			YDate yDateToConsider = new YDate(false);
			yDateToConsider.changeDate(-iRetentionDays);
			if (yDateToConsider.before(ydLatestStatusDate)) {
				bCloseLine = false;
			}
		}		
		return bCloseLine;
	}

	/**
	 * 
	 * @param yfcEleOrderLine
	 * @param sOrderName
	 * @param sQty
	 * @return
	 */
	private YFCDocument prepareCarrierUpdateMessage(YFCElement yfcEleOrderLine, String sOrderName, double dQty) {

		String sPrimeLineNo = yfcEleOrderLine.getAttribute(TelstraConstants.PRIME_LINE_NO);
		String sSubLineNo = yfcEleOrderLine.getAttribute(TelstraConstants.SUB_LINE_NO);
		String sItemID = yfcEleOrderLine.getChildElement(TelstraConstants.ITEM).getAttribute(TelstraConstants.ITEM_ID);

		YFCDocument yfcDocCarrierUpdateIp = YFCDocument.createDocument(TelstraConstants.SHIPMENT);
		YFCElement yfcEleRoot = yfcDocCarrierUpdateIp.getDocumentElement();
		yfcEleRoot.setAttribute(TelstraConstants.ORDER_NAME, sOrderName);
		YFCElement yfcEleShipmentLines = yfcEleRoot.createChild(TelstraConstants.SHIPMENT_LINES);
		YFCElement yfcEleShipmentLine = yfcEleShipmentLines.createChild(TelstraConstants.SHIPMENT_LINE);
		yfcEleShipmentLine.setAttribute(TelstraConstants.PRIME_LINE_NO, sPrimeLineNo);
		yfcEleShipmentLine.setAttribute(TelstraConstants.SUB_LINE_NO, sSubLineNo);
		yfcEleShipmentLine.setAttribute(TelstraConstants.ITEM_ID, sItemID);
		yfcEleShipmentLine.setDoubleAttribute(TelstraConstants.QUANTITY, dQty);

		YFCElement yfcEleExtn = yfcEleRoot.createChild(TelstraConstants.EXTN);
		YFCElement yfcEleCarrierUpdateList = yfcEleExtn.createChild(TelstraConstants.CARRIER_UPDATE_LIST);
		YFCElement yfcEleCarrierUpdate = yfcEleCarrierUpdateList.createChild(TelstraConstants.CARRIER_UPDATE);
		yfcEleCarrierUpdate.setAttribute(TelstraConstants.TRANSPORT_STATUS, TelstraConstants.DELIVERED);
		yfcEleCarrierUpdate.setAttribute(TelstraConstants.TRANSPORT_STATUS_DATE, new YDate(true));
		yfcEleCarrierUpdate.setAttribute(TelstraConstants.TRANSPORT_STATUS_TXT,
				"Delivered through GpsGetOrdersToBeClosed");

		LoggerUtil.verboseLog("GpsProcessCompleteOrderCycleDropQ :: prepareCarrierUpdateMessage :: Input document",
				logger, yfcDocCarrierUpdateIp);

		return yfcDocCarrierUpdateIp;
	}

	/**
	 * 
	 * @param yfcEleOrderLine
	 * @param sOrderName
	 * @param sQty
	 * @return
	 */
	private YFCDocument prepareReceiptMessage(YFCElement yfcEleOrderLine, String sOrderName, double dQty) {

		String sPrimeLineNo = yfcEleOrderLine.getAttribute(TelstraConstants.PRIME_LINE_NO);
		String sSubLineNo = yfcEleOrderLine.getAttribute(TelstraConstants.SUB_LINE_NO);
		String sReceivingNode = yfcEleOrderLine.getAttribute(TelstraConstants.RECEIVING_NODE);
		if(YFCCommon.isStringVoid(sReceivingNode)){
			sReceivingNode = yfcEleOrderLine.getAttribute(TelstraConstants.SHIP_NODE);
		}
		String sItemID = yfcEleOrderLine.getChildElement(TelstraConstants.ITEM).getAttribute(TelstraConstants.ITEM_ID);
		String sUnitOfMeasure = yfcEleOrderLine.getChildElement(TelstraConstants.ITEM)
				.getAttribute(TelstraConstants.UNIT_OF_MEASURE);

		YFCDocument yfcDocReceiveOrderIp = YFCDocument.createDocument(TelstraConstants.RECEIPT);
		YFCElement yfcEleRoot = yfcDocReceiveOrderIp.getDocumentElement();
		yfcEleRoot.setAttribute(TelstraConstants.ORDER_NAME, sOrderName);
		yfcEleRoot.setAttribute(TelstraConstants.RECEIVING_NODE, sReceivingNode);
		/*
		 * Adding processing failed flag, so that exception doesn't go to
		 * receive order queue
		 */
		yfcEleRoot.setAttribute(TelstraConstants.PROCESSING_FAILED, TelstraConstants.YES);

		YFCElement yfcEleReceiptLines = yfcEleRoot.createChild(TelstraConstants.RECEIPT_LINES);
		YFCElement yfcEleReceiptLine = yfcEleReceiptLines.createChild(TelstraConstants.RECEIPT_LINE);
		yfcEleReceiptLine.setAttribute(TelstraConstants.ITEM_ID, sItemID);
		yfcEleReceiptLine.setAttribute(TelstraConstants.PRIME_LINE_NO, sPrimeLineNo);
		yfcEleReceiptLine.setDoubleAttribute(TelstraConstants.QUANTITY, dQty);
		yfcEleReceiptLine.setAttribute(TelstraConstants.SUB_LINE_NO, sSubLineNo);
		yfcEleReceiptLine.setAttribute(TelstraConstants.UNIT_OF_MEASURE, sUnitOfMeasure);

		LoggerUtil.verboseLog("GpsProcessCompleteOrderCycleDropQ :: prepareReceiptMessage :: Input document", logger,
				yfcDocReceiveOrderIp);

		return yfcDocReceiveOrderIp;
	}

	/**
	 * 
	 * @param yfcInDoc
	 * @return
	 */
	private YFCDocument callGetOrderListApi(YFCDocument yfcInDoc) {
		YFCElement yfcEleRoot = yfcInDoc.getDocumentElement();
		String sOrderName = yfcEleRoot.getAttribute(TelstraConstants.ORDER_NAME);
		String sOrderNo = yfcEleRoot.getAttribute(TelstraConstants.ORDER_NO);
		YFCDocument yfcDocGetOrderListIp = null;

		if (!YFCCommon.isStringVoid(sOrderName)) {
			yfcDocGetOrderListIp = YFCDocument.getDocumentFor("<Order OrderName='" + sOrderName + "' />");
		} else if (!YFCCommon.isStringVoid(sOrderNo)) {
			yfcDocGetOrderListIp = YFCDocument.getDocumentFor("<Order OrderNo='" + sOrderNo + "'/>");
		} else {
			String sDate = yfcEleRoot.getAttribute(TelstraConstants.TO_DATE);
			if (!YFCCommon.isStringVoid(sDate)) {
				if (YFCCommon.isStringVoid(yfcEleRoot.getAttribute(TelstraConstants.RETENTION_DAYS))) {
					throw new YFSException("Retention days is mandatory with To Date Field");
				}
				iRetentionDays = Integer.valueOf(yfcEleRoot.getAttribute(TelstraConstants.RETENTION_DAYS));
				yfcDocGetOrderListIp = YFCDocument
						.getDocumentFor("<Order OrderDate='" + sDate + "' OrderDateQryType='LE' />");
			}
		}

		YFCDocument yfcDocgetOrderListTemp = YFCDocument.getDocumentFor(
				"<OrderList><Order OrderDate='' DocumentType='' OrderName='' OrderHeaderKey=''><OrderLines><OrderLine PrimeLineNo='' SubLineNo='' OrderLineKey='' ShipNode='' ReceivingNode=''><Item ItemID='' UnitOfMeasure=''/><OrderStatuses><OrderStatus Status='' StatusDate='' StatusQty=''/></OrderStatuses></OrderLine></OrderLines></Order></OrderList>");

		LoggerUtil.verboseLog("GpsProcessCompleteOrderCycleDropQ :: callGetOrderListApi :: Input document", logger,
				yfcDocGetOrderListIp);

		YFCDocument yfcDocGetOrderListOp = invokeYantraApi(TelstraConstants.API_GET_ORDER_LIST, yfcDocGetOrderListIp,
				yfcDocgetOrderListTemp);

		LoggerUtil.verboseLog("GpsProcessCompleteOrderCycleDropQ :: callGetOrderListApi :: Ouput document", logger,
				yfcDocGetOrderListOp);

		return yfcDocGetOrderListOp;
	}

}

package com.gps.hubble.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ListIterator;

import org.apache.commons.json.JSONArray;
import org.apache.commons.json.JSONException;
import org.apache.commons.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import com.sterlingcommerce.baseutil.SCXmlUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;

public class GpsJSONXMLUtil {
	
	public static  YFCDocument getXmlFromJSON(JSONObject jsonObject,String rootElement) throws JSONException {
		YFCDocument outDoc = YFCDocument.createDocument(rootElement);
		YFCElement rootElem = outDoc.getDocumentElement();
		getXmlFromJSONObject(jsonObject,rootElem);
		
		return outDoc;
	}
	/**
	 * populates passed the XML element with data form jsonObject. If uiConext
	 * passed is not null, datatype validation will be done on the data.
	 * 
	 * @param jsonObject
	 * @param outElement
	 */
	public static  void getXmlFromJSONObject(JSONObject jsonObject, YFCElement outElement) throws JSONException {
		Iterator iterator = jsonObject.keys();
		while (iterator.hasNext()) {
			String key = (String) iterator.next();
			Object child = jsonObject.opt(key);
			if (child instanceof JSONArray) {
				getXmlFromJSONArray((JSONArray) child, outElement, key);
			} else if (child instanceof JSONObject) {
				//Element childEl = SCXmlUtil.getChildElement(outElement, key, true);
				YFCElement childEl = outElement.getChildElement(key, true);
				getXmlFromJSONObject((JSONObject) child, childEl);
			} else {
				Object str = jsonObject.get(key);
				if(null==str){
					str = "";
				}
				key = formatKey(key);
				outElement.setAttribute(key,  str+"");
			}
		}
	}


	private static Object convertElementToJSON(Element el,  String xpath) throws JSONException {

		JSONObject jsonObj = new JSONObject();
		boolean ignoreTextNode = false;
		xpath = "".equals(xpath) ? el.getNodeName() : xpath + "/" + el.getNodeName();

		// First handle all the child elements
		Iterator it = SCXmlUtil.getChildren(el);
		HashMap tempMap = new HashMap();
		while (it.hasNext()) {
			ignoreTextNode = true;
			Element childEl = (Element) it.next();
			Object retObj = convertElementToJSON(childEl, xpath);
			String childName = childEl.getNodeName();
			if (tempMap.containsKey(childName)) {
				ArrayList namedList = (ArrayList) tempMap.get(childName);
				namedList.add(retObj);
			} else {
				ArrayList namedList = new ArrayList();
				namedList.add(retObj);
				tempMap.put(childName, namedList);
			}
		}
		Iterator mapIt = tempMap.keySet().iterator();
		while (mapIt.hasNext()) {
			String childName = (String) mapIt.next();
			ArrayList namedList = (ArrayList) tempMap.get(childName);
			// If there are more child elements with the same name
			if (namedList.size() > 1) {
				JSONArray jsonArray = new JSONArray();
				ListIterator li = namedList.listIterator();
				while (li.hasNext()) {
					jsonArray.add(li.next());
				}
				jsonObj.put(childName, jsonArray);
			} else {
				boolean isArray = confirmArray(childName, el, xpath);
				if (isArray) {
					JSONArray jsonArray = new JSONArray();
					jsonArray.add(namedList.get(0));
					jsonObj.put(childName, jsonArray);
				} else {
					jsonObj.put(childName, namedList.get(0));
				}

			}
		}
		NamedNodeMap map = el.getAttributes();
		for (int i = 0; i < map.getLength(); i++) {
			Node attrNode = map.item(i);
			String attrName = attrNode.getNodeName();
			if (jsonObj.has(attrName))
				attrName = attrName + "_scattr";
			String attrXpath = xpath + "/" + attrName;
			
			jsonObj.put(attrName, processAttrValue(attrName, attrNode,  el, attrXpath));
			ignoreTextNode = true;
		}

		if (!ignoreTextNode) {
			Node node = el.getFirstChild();
			if (node != null)
				if (Node.TEXT_NODE == node.getNodeType())
					return processAttrValue(node.getNodeName(), node,  el, xpath + "/" + node.getNodeName());
		}
		return jsonObj;
	}

	private static  String processAttrValue(String attrName, Node attrNode, Node el, String xpath) {
		String retVal = getConvertedAttributeValueWrapper(attrName, attrNode,  el, xpath);
		return retVal;
	}

	private static  String getConvertedAttributeValueWrapper(String attrName, Node attrNode, Node el, String xpath) {
		// Check for null context
		if (true)
			return attrNode.getTextContent();

		String xPathStr = el.getNodeName() + "/" + attrName;
		while ((el.getParentNode() != null) && (!(el.getParentNode() instanceof Document))) {
			el = el.getParentNode();
			xPathStr = el.getNodeName() + "/" + xPathStr;
		}

		boolean convertorRegistered = false;
		String name;
		if (convertorRegistered) {
			name = xpath;
		} else {
			name = attrName;
		}
		return name;
	}
	
	private static String formatKey(String key) {
		StringBuffer sb = new StringBuffer("");
		char[] charArray = key.toCharArray();
		for(char c:charArray){
			if(Character.isLetterOrDigit(c) || "_".equalsIgnoreCase(c+"")){
				sb.append(c);
			} 
		}
		return sb.toString();
	}

	public static  JSONObject getJSONObjectFromXML(Element element) throws JSONException {
		String xpath = "";
		Node el = element;
		while ((el.getParentNode() != null) && (!(el.getParentNode() instanceof Document))) {
			el = el.getParentNode();
			xpath = el.getNodeName() + "/" + xpath;
		}

		Object obj = convertElementToJSON(element,  xpath);
		JSONObject jsonObj = new JSONObject();
		jsonObj.put(element.getNodeName(), obj);
		return jsonObj;
	}
	private static boolean confirmArray(String childName, Element parentEl, String xpath) {
		String parentName = parentEl.getNodeName();
		return parentName.startsWith(childName);
	}
	
	private static void getXmlFromJSONArray(JSONArray jsonArray, YFCElement outElement, String key) throws JSONException {

		for (int i = 0; i < jsonArray.size(); i++) {
			//Element childEl = SCXmlUtil.createChild(outElement, key);
			YFCElement childEl = outElement.createChild(key);
			Object child = jsonArray.get(i);

			// Element ele = getElement(key, false, null);
			if (child instanceof JSONArray) {
				// UITODO Throw Exception
			} else if (child instanceof JSONObject) {
				// element.appendChild(ele);
				getXmlFromJSONObject((JSONObject) child, childEl);
			} else {
				String str = child.toString();
				if(null==str){
					str = "";
				}
				childEl.setNodeValue(str);

			}
		}
	}

}


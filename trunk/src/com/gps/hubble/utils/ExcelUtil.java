package com.gps.hubble.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.NumberToTextConverter;

import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCConfigurator;


public final class ExcelUtil {
	private static YFCLogCategory cat = YFCLogCategory.instance(ExcelUtil.class);
	public static final SimpleDateFormat SDF_DDMMYYYY = new SimpleDateFormat("dd/MM/yyyy");
	public static final SimpleDateFormat SDF_YYYYMMDDHHMMSSSSS =new SimpleDateFormat("yyyyMMddHHmmssSSS");
	public static final DecimalFormat DF = new DecimalFormat("#");
	private static final String PROCESSED_SUCCESS_HTML_FILE = "/global/template/email/html/ExcelProcessed.html";
	private static final String PROCESSED_ERROR_HTML_FILE = "/global/template/email/html/ExcelError.html";
	
	private ExcelUtil(){
		
	}
	
	public static List<File> getFilesToProcess(String directory) {
		/**
		 * The method picks a file from directory with extension as fileExtn
		 * .xls and .xslx and returns the file's List
		 */
		File dir = new File(directory);
		File[] files = dir.listFiles(new XLSFileNameFilter(".xls", ".xlsx"));
		if (files == null) {
			return new ArrayList<File>();
		}
		return Arrays.asList(files);
	}
	
	public static Workbook moveRow(Workbook workbook, Row moveRow, Boolean bErrored, String strErrorDesc){
		return moveRow(workbook, moveRow, bErrored, strErrorDesc,-1);
	}
	
	public static Workbook moveRow(Workbook workbook, Row moveRow, Boolean bErrored,String strErrorDesc,int errorCellPosition)
	{
		int iSize = moveRow.getPhysicalNumberOfCells();
		if(errorCellPosition > 0){
			iSize = errorCellPosition;
		}
		Sheet sheet;
		if (bErrored)
		{
			sheet = workbook.getSheet("Error Sheet");
			if (sheet == null)
			{
				cat.debug("Creating the Error Sheet");
				sheet =  workbook.createSheet("Error Sheet");
			}
		} 
		else
		{
			sheet =  workbook.getSheet(TelstraConstants.DONE_SHEET);
			if (sheet == null)
			{
				cat.debug("Creating the Successful Upload Sheet");
				sheet = workbook.createSheet(TelstraConstants.DONE_SHEET);
			}
		}
		int intNumOfRows = sheet.getPhysicalNumberOfRows();
		cat.debug("intNumOfRows" + intNumOfRows);
		Row row = sheet.createRow(intNumOfRows);
		
		Sheet fromSheet = workbook.getSheetAt(0);
		Row fromHeaderRow = fromSheet.getRow(0);
		Cell fromCell = getCell(fromHeaderRow, 0);
		CellStyle fromStyle = fromCell.getCellStyle();
		
		for (int j = 0; j < iSize; j++)
		{
			cat.debug("Writing To Sheet : " + ExcelUtil.getCellvalue(moveRow, j));
			Cell cell = row.createCell(j);
			cell.setCellValue(ExcelUtil.getCellvalue(moveRow, j));
			if(intNumOfRows==0)
			{
				//If its the header row
				cell.setCellStyle(fromStyle);
		        sheet.setColumnWidth(j, fromSheet.getColumnWidth(j));
			}
		}
		
		if(bErrored){
			Cell cell = row.createCell(iSize);
			cell.setCellValue(strErrorDesc);
			if(intNumOfRows==0)
			{
				cell.setCellStyle(fromStyle);
				sheet.setColumnWidth(iSize, 5000);
			}
		}
		if (bErrored){
			cat.debug("Error Row Wriiten Successfully");
		}
		else {
			cat.debug("Done Row Wriiten Successfully");
		}

		return workbook;
	}
	
	
	public static void sendExcelAttachmentEmail(byte[] encodedExcelBytes, String strToEmailIDs, String strFromEmailID, String sFileName, String sFileExtension ,String sPort, String sHost) throws MessagingException, IOException{
		sendExcelAttachmentEmail(encodedExcelBytes, strToEmailIDs, strFromEmailID, sFileName, sFileExtension, sPort, sHost, "", false, true);
	}
	
	public static void sendExcelAttachmentEmail(byte[] encodedExcelBytes, String strToEmailIDs, String strFromEmailID, String sFileName, String sFileExtension ,String sPort, String sHost, String sHelpdeskContact, boolean validationFailed, boolean isExcelFormatValid) throws MessagingException, IOException
	{
		byte[] decodedExcelBytes = Base64.decodeBase64(encodedExcelBytes);
		double attachmentSize  = decodedExcelBytes.length;
		
		String sAttachmentFilename = sFileName + sFileExtension ;
		String sTemplateName = sFileName.substring(0, sFileName.lastIndexOf("_"));
		String sTemplateFilename = sTemplateName + "_TEMPLATE.xlsx";
		String sFileUploadKey="";
		if(!YFCCommon.isVoid(sFileName)){
			sFileUploadKey = sFileName.substring(sFileName.lastIndexOf("_")+1);
		}
		
		boolean isAttachemntWithInLimitSize = false;
		attachmentSize = attachmentSize/1048576;
		int maxAttachmentSize = YFCConfigurator.getInstance().getIntProperty("gps.email.maxsize", 6);
		
		if(attachmentSize <= maxAttachmentSize){
			isAttachemntWithInLimitSize = true;
		}
				
		DataSource fds = new ByteArrayDataSource(decodedExcelBytes,	"application/vnd.ms-excel");

		String sEmailSubject = "Processed: " + sFileName;
		
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("maxSizeExceeded", new Boolean(!isAttachemntWithInLimitSize));
		model.put("fileUploadKey", sFileUploadKey);
		model.put("attachmentSize", ""+attachmentSize);
		model.put("maxAttachmentSize", ""+maxAttachmentSize);
		model.put("attachmentFileName", sAttachmentFilename);
		model.put("attachmentTemplateName", sTemplateFilename);
		model.put("helpdeskContact", sHelpdeskContact);
		boolean isHelpdeskContactNotBlank = true;
		if(YFCCommon.isStringVoid(sHelpdeskContact)){
			isHelpdeskContactNotBlank = false;
		}
		model.put("helpdeskContactNotBlank", new Boolean(isHelpdeskContactNotBlank));
		TemplateProcessor tpl = new TemplateProcessor();
		String fileName = PROCESSED_SUCCESS_HTML_FILE;
		if(validationFailed || !isExcelFormatValid){
			fileName = PROCESSED_ERROR_HTML_FILE;
			sEmailSubject = "Invalid Excel: " + sFileName;
			model.put("excelValid", new Boolean(isExcelFormatValid));

		}
		
		String sEmailBodyText = tpl.process(fileName, model);
		if(YFCObject.isVoid(sEmailBodyText)){
			cat.error("Empty html body found for fileName "+fileName);
		}
		final String sFromEmailID = strFromEmailID;

		InternetAddress[] toAddress = null;
		cat.debug("From Email Id" + sFromEmailID);
		cat.debug("To Email Ids" + strToEmailIDs);
		StringTokenizer stToEmailId = new StringTokenizer(strToEmailIDs, ";");
		int iNumOfEmailIds = stToEmailId.countTokens();
		cat.debug("No Of To Email Ids" + iNumOfEmailIds);
		toAddress = new InternetAddress[iNumOfEmailIds];

		for (int i = 0; i < iNumOfEmailIds; i++) {
			cat.debug(i + "th iterator");
			String strNextEmailId = stToEmailId.nextToken();
			cat.debug("To Email Id " + strNextEmailId);
			toAddress[i] = new InternetAddress(strNextEmailId);
		}

		Properties props = System.getProperties();
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.port", sPort);
		props.put("mail.smtp.host", sHost);
		props.put("mail.smtp.auth", "false");

		Session session = Session.getInstance(props, null);
		Message message = new MimeMessage(session);
		message.setFrom(new InternetAddress(sFromEmailID, true));
		message.setSentDate(new Date());
		message.setRecipients(Message.RecipientType.TO, toAddress);
		message.setSubject(sEmailSubject);
		BodyPart messageBodyPart = new MimeBodyPart();
		
		DataSource bodySource = new ByteArrayDataSource(sEmailBodyText, "text/html");
		messageBodyPart.setDataHandler(new DataHandler(bodySource));
		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(messageBodyPart);
		
		if(isAttachemntWithInLimitSize){
			addAttachment(multipart, fds, sAttachmentFilename);
		}
		if(validationFailed || !isExcelFormatValid)
		{
			//the template excel too needs to be attached.
			DataSource templateDataSource = getTemplate(sTemplateName);
			if(templateDataSource!=null)
				addAttachment(multipart, templateDataSource, sTemplateFilename);
		}
		
		message.setContent(multipart);
		Transport.send(message);
	}

	private static void addAttachment(Multipart multipart, DataSource source,
			String sFilename) throws MessagingException {
		BodyPart messageBodyPart = new MimeBodyPart();        
	    messageBodyPart.setDataHandler(new DataHandler(source));
	    messageBodyPart.setFileName(sFilename);
	    multipart.addBodyPart(messageBodyPart);	
	}
	
	private static DataSource getTemplate(String sTemplateName){
		String fileName = TelstraConstants.EXCEL_TEMPLATE_FOLDER_PATH + sTemplateName + ".xlsx";
		InputStream templateInputStream = null;
		
		try{
			templateInputStream = ExcelUtil.class.getResourceAsStream(fileName);
			byte[] templateByteArray = IOUtils.toByteArray(templateInputStream);
			DataSource templateDataSource = new ByteArrayDataSource(templateByteArray,	"application/vnd.ms-excel");
			return templateDataSource;
		}catch(Exception e){
			cat.error("Exception while reading excel template for transaction type "+ sTemplateName, e);
			return null;
		}finally{
			if(templateInputStream != null){
				try{
					templateInputStream.close();
				}catch(Exception e){
					cat.error("Exception while closing output stream", e);
				}
			}
		}
	}

	public static String getCellvalue(Row row, int cellno, DateFormat dateFormat) {
		Cell cell = getCell(row, cellno);
		return getValueOfCell(cell,dateFormat);
	}
	
	public static String getCellvalue(Row row, int cellno) {
		return getCellvalue(row, cellno, SDF_DDMMYYYY);
	}
	
	public static void moveFile(String filesAbsPath, String srcDir, String destDir) {
		File fileSource = new File(filesAbsPath);
		new File(destDir).mkdirs();
		File fileDestination = new File(filesAbsPath.replace(srcDir, destDir));
		if(fileDestination.exists()){
			String extn = fileDestination.getName().substring(fileDestination.getName().lastIndexOf("."));
			String filename = fileDestination.getName().substring(0,fileDestination.getName().lastIndexOf("."));
			String date = SDF_YYYYMMDDHHMMSSSSS.format(new Date());
			filename=filename+"_"+date+extn;
			fileDestination.renameTo(new File(destDir+File.separator+filename));
			
		}
		fileSource.renameTo(fileDestination);
	}

	public static Cell getCell(Row row, int cellno) {
		Cell cell;
		if (row.getCell(cellno) == null) {
			cell = row.createCell(cellno);
		} else {
			cell = row.getCell(cellno);
		}
		return cell;
	}

	
	public static String getValueOfCell(Cell cell) { 
		return getValueOfCell(cell,SDF_DDMMYYYY);
	}
	
	public static String getValueOfCell(Cell cell,DateFormat dateFormat) {
		if (cell == null) {
			return "";
		}
		String cellValue = "";
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_NUMERIC:
			if (DateUtil.isCellDateFormatted(cell)) {
				String date = dateFormat.format(cell.getDateCellValue());
				cellValue = date;
			} else {
				cellValue = NumberToTextConverter.toText(cell.getNumericCellValue());
			}
			break;
		case Cell.CELL_TYPE_STRING:
			cellValue = cell.getStringCellValue();
			break;
		case Cell.CELL_TYPE_BLANK:
			cellValue = "";
			break;
		}

		return cellValue;
	}
	
	/*
	 * Can return either string or date object
	 */
	public static Object getObjectOfCell(Cell cell){
		if (cell == null) {
			return "";
		}
		String cellValue = "";
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_NUMERIC:
			if (DateUtil.isCellDateFormatted(cell)) {
				return cell.getDateCellValue();
			} else {
				cellValue = NumberToTextConverter.toText(cell.getNumericCellValue());
			}
			break;
		case Cell.CELL_TYPE_STRING:
			cellValue = cell.getStringCellValue();
			break;
		case Cell.CELL_TYPE_BLANK:
			cellValue = "";
			break;
		}
		return cellValue;
	}

	static class XLSFileNameFilter implements FilenameFilter {

		private String ext;

		private String ext2;

		public XLSFileNameFilter(String ext, String ext2) {
			this.ext = ext.toLowerCase();
			this.ext2 = ext2.toLowerCase();
		}

		@Override
		public boolean accept(File dir, String name) {
			return name.toLowerCase().endsWith(ext)
					|| name.toUpperCase().endsWith(ext.toUpperCase())
					|| name.toLowerCase().endsWith(ext2)
					|| name.toUpperCase().endsWith(ext2.toUpperCase());
		}

	}
	
	public static Iterator<Row> getRowsFromExcellFile(File f) throws IOException, InvalidFormatException {
		Iterator<Row> rowIterator = null;
		InputStream file = new FileInputStream(f);
		Workbook workbook = WorkbookFactory.create(file);
		Sheet sheet = workbook.getSheetAt(0);
		rowIterator = sheet.rowIterator();
		return rowIterator;
	}
	
	
	public static boolean isEmptyRow(Row row, int noOfCells){
		if(noOfCells == 0){
			noOfCells = row.getPhysicalNumberOfCells();
		}
		if(noOfCells <= 0){
			return true;
		}
		boolean isEmpty = true;
		for(int i=0;i<noOfCells;i++){
			Cell cell = row.getCell(i);
			Object obj = getObjectOfCell(cell);
			if(!YFCObject.isVoid(obj)){
				isEmpty = false;
				break;
			}
		}
		return isEmpty;
	}
	
	// HUB-7088 - SPS and ITEC Inventoy - Begin
    public static org.apache.poi.ss.usermodel.Row createApacheRow(Sheet sheet, com.gps.hubble.file.Row row) {
      org.apache.poi.ss.usermodel.Row createRow = sheet.createRow(row.getRowNumber());
      
      for(int i=0;i<row.getNumberOfCells();i++){
        Cell cell = createRow.createCell(i);
        cell.setCellValue(row.getCellValue(i));
      }
      return createRow;
    }
    // HUB-7088 - SPS and ITEC Inventoy - End
} 
package com.gps.hubble.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Properties;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.w3c.dom.Document;

import com.yantra.interop.japi.YIFApi;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

/**
 * The code is written to configure Regions within Sterling.
 * The Region information is fetched from an excel sheet in Excel 97-2003 Workbook format.
 * As a prerequisite, the following configurations have to present in Sterling:
 *          Region Levels - The following Region Levels have to be created in Sterling:
 *              EASTERN_REGION
 *              WESTERN_REGION
 *              NORTHERN_REGION
 *              SOUTHERN_REGION
 * If creating a Region is successful, the Region info is written to Done Sheet.
 * If creating a Region is unsuccessful, the Region info is written to Error Sheet.
 * 
 * @author Santosh Nagaraj
 * 
 * Bridge Solutions Group
 */

public class CreateRegionsFromExcel implements YIFCustomApi
{   
    private Properties properties = null;
    private static YFCLogCategory _cat = YFCLogCategory.instance("com.yantra.CustomCode");
    
    public void setProperties(final Properties prop) throws Exception
    {    
        properties = prop;
    }
 
    public void readRegionData(final YFSEnvironment env, final Document ipDoc) throws Exception
    {
        String strInDir = null;
        String strCompleteDir = null;
        String strFileExtension = null;
        String strRegionSchemaName = null;
        String strCountry = null;
        String errorCode = null;
        String errorDescription = null;
        FileInputStream file = null;
        DecimalFormat df = new DecimalFormat("#");
        YIFApi _Api = YIFClientFactory.getInstance().getLocalApi();
        
        strInDir = ipDoc.getDocumentElement().getAttribute("IncomingDirectory");
        strCompleteDir = ipDoc.getDocumentElement().getAttribute("CompletionDirectory");
        
        if(properties != null)
        {
            //This has to be xls
            strFileExtension = properties.getProperty("FileExtension");
            strRegionSchemaName = properties.getProperty("RegionSchemaName");
            strCountry = properties.getProperty("Country");
        }
        else
        {
            errorCode = "Error1";
            errorDescription = "SDF arguments are not passed correctly";
            YFSException exeception = new YFSException(errorDescription, errorCode, errorDescription);
            throw exeception;
        }
                
        String strFilesAbsPath = getFileToRead(strInDir, strFileExtension);
        
        
        if (!strFilesAbsPath.equals(""))
        {
            _cat.verbose("File Being Read: " + strFilesAbsPath);

            file = new FileInputStream(new File(strFilesAbsPath));
                
            //Get the workbook instance for XLS file
            HSSFWorkbook workbook = new HSSFWorkbook(file);
            //Get first sheet from the workbook
            HSSFSheet sheet = workbook.getSheetAt(0);
            //Iterate through each rows from first sheet
            Iterator<Row> rowIterator = sheet.iterator();
            boolean first=true;
            while(rowIterator.hasNext())
            {
                Iterator<Cell> cellIterator;
                ArrayList<String> colValue = new ArrayList<String>();
                Row row = rowIterator.next();
                if(first){
                    first=false;
                    continue;
                }
                //For each row, iterate through each columns
                cellIterator = row.cellIterator();
                try
                {
                    while(cellIterator.hasNext())
                    {
                        Cell cell = cellIterator.next();
                        if(cell == null)
                        {
                            //_cat.verbose("The cell is null");
                            colValue.add("");
                        }
                        else
                        {
                            switch(cell.getCellType())
                            {
                                case Cell.CELL_TYPE_NUMERIC:
                                    //_cat.verbose("The cell is numeric");
                                    colValue.add(String.valueOf(df.format(cell.getNumericCellValue())));
                                    break;
                                case Cell.CELL_TYPE_STRING:
                                    //_cat.verbose("The cell is string");
                                    colValue.add(cell.getStringCellValue());
                                    break;
                                case Cell.CELL_TYPE_BLANK:
                                    //_cat.verbose("The cell is blank");
                                    colValue.add("");
                                    break;
                            }
                        }
                    }
                
                    _cat.verbose("colValue.size : " + colValue.size());
                    String strPostalCode = (colValue.get(0)).trim();
                    while(strPostalCode.length() < 4) {
                      strPostalCode = "0"+strPostalCode;
                    }
                    _cat.verbose("str1stCol : " + strPostalCode);
                    String strRegionName = (colValue.get(5)).trim();
                    _cat.verbose("str2ndCol : " + strRegionName);
                    
                    /*
                     * <Region Action="MODIFY" Country="AU" RegionLevelName="EASTERN_REGION" RegionName="EASTERN">
                          <RegionSchema OrganizationCode="DEFAULT" RegionSchemaName="Australlia"/>
                          <ZipCodeRanges>
                            <ZipCodeRange Country="AU" FromZip="0100" ToZip="0100"/>
                          </ZipCodeRanges>
                        </Region>
                     */
                        YFCDocument yfcDocChangeRegion = YFCDocument.getDocumentFor("<Region Action='MODIFY' />");
                        YFCElement yfcEleRegion = yfcDocChangeRegion.getDocumentElement();
                        
                        yfcEleRegion.setAttribute("RegionLevelName", strRegionName+"_REGION");
                        yfcEleRegion.setAttribute("RegionName", strRegionName);
                        yfcEleRegion.setAttribute("Country", strCountry);
                        
                        YFCElement yfcEleRegionSchema = yfcEleRegion.createChild("RegionSchema");
                        yfcEleRegionSchema.setAttribute("OrganizationCode", "DEFAULT");
                        yfcEleRegionSchema.setAttribute("RegionSchemaName", strRegionSchemaName);
                        
                        YFCElement yfcEleZipCodeRanges = yfcEleRegion.createChild("ZipCodeRanges");
                        YFCElement YFCEleZipCodeRange = yfcEleZipCodeRanges.createChild("ZipCodeRange");
                        YFCEleZipCodeRange.setAttribute("Country", strCountry);
                        YFCEleZipCodeRange.setAttribute("FromZip", strPostalCode);
                        YFCEleZipCodeRange.setAttribute("ToZip", strPostalCode);
                        
                        _cat.verbose("Inpt being passed to create Country:" + yfcDocChangeRegion.toString());
                        Document docChangeRegionOutput = _Api.invoke(env, "changeRegion", yfcDocChangeRegion.getDocument());
                        
                    FileOutputStream fileDoneDestination = new FileOutputStream(new File(strFilesAbsPath));
                    moveRow(workbook, colValue, fileDoneDestination, false);
                }
                catch(Exception e)
                {
                    _cat.verbose("Problem while reading the " + (colValue.get(0)).trim() + " Region: " + (colValue.get(1)).trim());
                    FileOutputStream fileErrDestination = new FileOutputStream(new File(strFilesAbsPath));
                    moveRow(workbook, colValue, fileErrDestination, true);
                }
            }
            file.close();
            
            moveFile(strFilesAbsPath, strInDir, strCompleteDir);
        }
    }
    
    public static String getFileToRead(String directory, String fileExtn)
    {
        /** 
         * The method picks a file from directory with extension as fileExtn
         * and returns the file's absolute path
         */
        File dir = new File(directory);
        File[] children = dir.listFiles();
        
        if (children.length == 0)
        {
            return "";
        }
        else
        {
            int i=0;
            File fileName = children[i];
            while ((i < ((children.length)-1)) && !(fileName.toString()).endsWith(fileExtn))
            {
                i++;
                if(children[i].isFile())
                    fileName = children[i];
            }
            
            if((fileName.toString()).endsWith(fileExtn) && fileName.isFile())
                return (fileName.toString());
            else
                return "";
        }
    }
    
    public static void moveRow(HSSFWorkbook workbook, ArrayList<String> colValue, FileOutputStream outputFile, Boolean bErrored) throws Exception
    {
        HSSFSheet sheet;
        if(bErrored)
        {
            sheet = workbook.getSheet("Error Sheet");
            if(sheet == null)
            {
                _cat.debug("Creating the Error Sheet");
                sheet = workbook.createSheet("Error Sheet");
            }
        }
        else
        {
            sheet = workbook.getSheet("Done Sheet");
            if(sheet == null)
            {
                _cat.debug("Creating the Done Sheet");
                sheet = workbook.createSheet("Done Sheet");
            }
        }
        int intNumOfRows = sheet.getPhysicalNumberOfRows();
        _cat.debug("intNumOfRows" + intNumOfRows);
        Row row = sheet.createRow(intNumOfRows);
        int intColValueSize = colValue.size();
        for (int j = 0; j < intColValueSize; j++)
        {
            _cat.debug("Writing To Sheet : " + colValue.get(j));
            Cell cell = row.createCell(j);
            cell.setCellValue(colValue.get(j));
        }
        
        try
        {
            workbook.write(outputFile);
            if(bErrored)
            {
                _cat.debug("Error Row Wriiten Successfully");
            }
            else
            {
                _cat.debug("Done Row Wriiten Successfully");
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        finally
        {
            outputFile.close();
        }
    }
    
    public static void moveFile(String filesAbsPath, String inDir, String completionDir)
    {
        File fileSource = new File(filesAbsPath);
        File fileErrDestination = new File(filesAbsPath.replace(inDir, completionDir));
        fileSource.renameTo(fileErrDestination);
    }
}
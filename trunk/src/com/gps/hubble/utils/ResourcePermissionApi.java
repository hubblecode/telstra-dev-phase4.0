package com.gps.hubble.utils;

import java.rmi.RemoteException;

import org.w3c.dom.Document;

import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.yantra.api.ycp.util.getProperty;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;


/**
 * Method to get the list of Resource for log in User.
 * 
 * @author Murali
 *
 */
public class ResourcePermissionApi {

  /**
   * 
   * @param env
   * @param inDoc
   * @return
   * @throws YFSException
   * @throws RemoteException
   * @throws YIFClientCreationException
   */
  public Document getResourceList(YFSEnvironment env, Document inDoc ) throws RemoteException, YIFClientCreationException {
	//TODO resolve security issue
	//The below line seems incorrect  
    String userId = env.getUserId();
    userId =  inDoc.getDocumentElement().getAttribute("Loginid");
    YFCDocument inXml = YFCDocument.getDocumentFor("<getResourcesForUser />");
    YFCElement inEle = inXml.getDocumentElement();
    inEle.setAttribute("Loginid", userId);
    YFCDocument outXml = YFCDocument.getDocumentFor(YIFClientFactory.getInstance().getApi()
        .invoke(env,"getResourcesForUser", inXml.getDocument()));
    return getValidResources(outXml).getDocument();
  }

  
  /**
   * 
   * @param resourceList
   * @return
   */
  private YFCDocument getValidResources(YFCDocument resourceList) {
    YFCElement resourceListEle =  resourceList.getDocumentElement();
    YFCIterable<YFCElement> resources = resourceListEle.getChildren("Resource");
    YFCDocument outputDocXml = YFCDocument.getDocumentFor("<Resources />");
    YFCElement outputDocEle = outputDocXml.getDocumentElement();
    for(YFCElement resource: resources) {
      String applicationName = resource.getAttribute("ApplicationName");
      YFCElement subEle = resource.getChildElement("SubResource");
      if(!XmlUtils.isVoid(subEle)) {
        YFCIterable<YFCElement> subResources = subEle.getChildren("Resource");
        if("YFSSYS00004".equals(applicationName)) {
          getSubResourceId(subResources,outputDocEle);
        }
      }
    }
    return outputDocXml;
  }
  
  private void getSubResourceId(YFCIterable<YFCElement> subResources, YFCElement  outputDocEle) {
    for(YFCElement subResource: subResources ) {
      String resourceId = subResource.getAttribute("ResourceId");
      String isActive = subResource.getAttribute("Active");
      if(( resourceId.contains("GPSSP") || resourceId.contains("GPSVP") || resourceId.contains("GPSTP")) && "Y".equals(isActive)) {
        outputDocEle.createChild("Resource").setAttribute("ResourceId",subResource.getAttribute("ResourceId"));
      }
    }
  }
}
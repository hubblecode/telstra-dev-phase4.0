/***********************************************************************************************
 * File	Name		: TrackConsignment.java
 *
 * Description		: This class is called from the UI as a service to paint the consignment tracking page. 
 * 						It fetches the carrier updates for a shipment, 
 * 						and formats it so that it can be easily used to paint the UI.
 * 
 * Modification	Log	:
 * ---------------------------------------------------------------------------------------------
 * Ver #	Date			Author					Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0		Aug 17,2016	  	Manish Kumar 		   	Initial	Version 
 * ---------------------------------------------------------------------------------------------
 * 
 */

package com.gps.hubble.carrier.api;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.date.YTimestamp;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;

/**
 * Custom API to track a consignment
 * 
 * @author Manish Kumar
 * @version 1.0
 *
 * Extends AbstractCustomApi Class
 */

public class TrackConsignment extends AbstractCustomApi
{
	private static YFCLogCategory logger = YFCLogCategory.instance(TrackConsignment.class);
	
	/*
	 * yfInDoc : Input to the service 
	 * Sample Input: 	<CarrierUpdate ShipmentKey="2016072913091874863"/>
	 * Sample Output:	<Shipment POD="data:image/jpeg;base64,/9j/4AZ" ReceiverName="Manish Kumar" 
	 * 							ReceiverPhone="991655991655" Status="Delivered">
	 * 						<CarrierUpdateGrp TransportStatusDate="2016-08-17" TransportStatusDayOfWeek="2016-08-17">
	 * 							<CarrierUpdate TransportStatus="Delivered" 
	 * 								TransportStatusTime="19:32:41" TransportStatusTxt="Delivered"/>
	 * 							<CarrierUpdate TransportStatus="Intransit"
	 * 								TransportStatusTime="09:32:41" TransportStatusTxt="UUpdate 12"/>
	 * 							<CarrierUpdate TransportStatus="Intransit"
	 * 								TransportStatusTime="04:42:41" TransportStatusTxt="Update 11"/>
	 * 						</CarrierUpdateGrp>
	 * 						<CarrierUpdateGrp TransportStatusDate="2016-08-16" TransportStatusDayOfWeek="2016-08-16">
	 * 							<CarrierUpdate TransportStatus="Intransit"
	 * 								TransportStatusTime="23:21:34" TransportStatusTxt="Update 10"/>
	 * 						</CarrierUpdateGrp>
	 * 						<CarrierUpdateGrp TransportStatusDate="2016-08-10" TransportStatusDayOfWeek="2016-08-10">
	 * 							<CarrierUpdate TransportStatus="Intransit"
	 * 								TransportStatusTime="13:12:54" TransportStatusTxt="PPicked Up from Warehouse"/>
	 * 						</CarrierUpdateGrp>
	 * 					</Shipment>
	 */
	public YFCDocument invoke(YFCDocument yfcInDoc)
	{
		LoggerUtil.startComponentLog(logger, this.getClass().getName(),"invoke", yfcInDoc);
		
		//Get the carrier updates for the Shipment
		YFCDocument yfcDocCarrierUpdateIn = getcarrierUpdateList(yfcInDoc);
		YFCElement yfcEleCarrierUpdateListIn = yfcDocCarrierUpdateIn.getDocumentElement();
		YFCNodeList<YFCElement> yfcNLCarrierUpdateIn = yfcEleCarrierUpdateListIn.getElementsByTagName("CarrierUpdate");
		
		TreeMap<String, TreeMap<String, YFCElement>> mapDateElement = new TreeMap<>(Collections.reverseOrder());
		TreeMap<String, YFCElement> mapTimeElement;
		Map<String, String> mapDateToDay = new HashMap<>();
		
		for(int i=0; i<yfcNLCarrierUpdateIn.getLength(); i++)
		{
			YFCElement yfcCarrierUpdateEle = yfcNLCarrierUpdateIn.item(i);
			YTimestamp transportStatusTimeStamp = yfcCarrierUpdateEle.getYTimestampAttribute("TransportStatusDate");
			if(transportStatusTimeStamp==null)
			{
				logger.verbose("TransportStatusDate is null; getting the TransportStatusDate value as the CreateTS");
				transportStatusTimeStamp = (yfcCarrierUpdateEle.getYTimestampAttribute("CreateTS"));
			}
			logger.verbose("dateTimeTransportStatusDate=> " + transportStatusTimeStamp);
			
			/*Changed the constant format*/
			String strTransportStatusTimeStamp = transportStatusTimeStamp.getString(TelstraConstants.TELSTRA_YEAR_DATE_TIME_FORMAT);
			logger.verbose("strDateTime=> " + strTransportStatusTimeStamp);
			
			String strDate = strTransportStatusTimeStamp.substring(0, 10);
			logger.verbose("strDate=> " + strDate);
			String strTime = strTransportStatusTimeStamp.substring(11, 19);
			logger.verbose("strTime=> " + strTime);
			
			if(!mapDateToDay.containsKey(strDate))
			{
				String strDayOfWeek;
				switch(transportStatusTimeStamp.getDayOfWeek())
				{
					case 1: strDayOfWeek = "Sunday";
							break;
					case 2: strDayOfWeek = "Monday";
							break;
					case 3: strDayOfWeek = "Tuesday";
							break;
					case 4: strDayOfWeek = "Wednesday";
							break;
					case 5: strDayOfWeek = "Thursday";
							break;
					case 6: strDayOfWeek = "Friday";
							break;
					case 7: strDayOfWeek = "Saturday";
							break;
					default: strDayOfWeek = "";
							break;
				}
				mapDateToDay.put(strDate, strDayOfWeek);	
			}
		
			if(mapDateElement.containsKey(strDate))
			{
				mapTimeElement = mapDateElement.get(strDate);
				mapTimeElement.put(strTime, yfcCarrierUpdateEle);	
			}
			else
			{
				mapTimeElement = new TreeMap<>(Collections.reverseOrder());
				mapTimeElement.put(strTime, yfcCarrierUpdateEle);
				mapDateElement.put(strDate, mapTimeElement);
			}			
		}
		logger.verbose("map contains" + mapDateElement.toString());
		
		String strLastCarrierUpdateDate;
		YFCDocument yfcDocOut = YFCDocument.getDocumentFor("<Shipment/>");
		YFCElement yfcEleRoot = yfcDocOut.getDocumentElement();
		
		if(!mapDateElement.isEmpty()) {
			strLastCarrierUpdateDate = mapDateElement.firstKey();
			TreeMap<String, YFCElement> mapLastCarrierUpdate = mapDateElement.get(strLastCarrierUpdateDate);
			if(mapLastCarrierUpdate != null && !mapLastCarrierUpdate.isEmpty()) {
				String strLastCarrierUpdateTime = mapLastCarrierUpdate.firstKey();
				YFCElement yfcLastCarrierUpdate = mapLastCarrierUpdate.get(strLastCarrierUpdateTime);
				yfcEleRoot.setAttribute("Status", yfcLastCarrierUpdate.getAttribute("TransportStatus", ""));
				yfcEleRoot.setAttribute("POD", yfcLastCarrierUpdate.getAttribute("Pod", ""));
				yfcEleRoot.setAttribute("ReceiverName", yfcLastCarrierUpdate.getAttribute("ReceiverName", ""));
				yfcEleRoot.setAttribute("ReceiverPhone", yfcLastCarrierUpdate.getAttribute("ReceiverPhone", ""));
			}
			
		}
		
		Set setDate = mapDateElement.entrySet();
		if(setDate != null) {
			Iterator itrDate = setDate.iterator();
			TreeMap<String, YFCElement> mapCarrierUpdateTime = null;
			while(itrDate.hasNext())
			{
				logger.verbose("Iterator============");
				/*Changed the constant format*/

				Map.Entry mapEntryDate = (Map.Entry)itrDate.next();
				String strCarrierUpdateDate = (String) mapEntryDate.getKey();
				
				//Changes to stamp the right date format
				/*START*/
				  DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd"); 
				  Date date = null;
				try {
					date = (Date)formatter.parse(strCarrierUpdateDate);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				  SimpleDateFormat newFormat = new SimpleDateFormat("dd/MM/yyyy");
				  String strFormatCarrierUpdateDate = newFormat.format(date);
				  
					/*END*/

				mapCarrierUpdateTime = (TreeMap<String, YFCElement>) mapEntryDate.getValue();
				logger.verbose(strCarrierUpdateDate + ": ");
				logger.verbose(mapCarrierUpdateTime.toString());
				
				YFCElement yfcCarrierUpdateGrpEleOut = yfcEleRoot.createChild("CarrierUpdateGrp");
				yfcCarrierUpdateGrpEleOut.setAttribute("TransportStatusDate", strFormatCarrierUpdateDate);
				yfcCarrierUpdateGrpEleOut.setAttribute("TransportStatusDayOfWeek", mapDateToDay.get(strCarrierUpdateDate));
				
				Set setTime = mapCarrierUpdateTime.entrySet();
				if(setTime != null) {
					Iterator itrTime = setTime.iterator();
					while(itrTime.hasNext())
					{
						logger.verbose("Child Iterator+++++++++++");
						Map.Entry mapEntryTime = (Map.Entry)itrTime.next();
						String strCarrierUpdateTime = (String) mapEntryTime.getKey();
						YFCElement yfcEleCarrierUpdateMap = (YFCElement) mapEntryTime.getValue();
						
						logger.verbose(strCarrierUpdateTime);
						logger.verbose(yfcEleCarrierUpdateMap.toString());
						
						YFCElement yfcCarrierUpdateEleOut = yfcCarrierUpdateGrpEleOut.createChild("CarrierUpdate");
						yfcCarrierUpdateEleOut.setAttribute("TransportStatusTime", strCarrierUpdateTime);
						yfcCarrierUpdateEleOut.setAttribute("TransportStatus", yfcEleCarrierUpdateMap.getAttribute("TransportStatus", ""));
						yfcCarrierUpdateEleOut.setAttribute("TransportStatusTxt", yfcEleCarrierUpdateMap.getAttribute("TransportStatusTxt", ""));
					}
					
				}
				
			}

			
		}
		
		logger.verbose("Output Element is => " + yfcEleRoot.toString());
		
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", yfcInDoc);
		return yfcDocOut;	
	}

	private YFCDocument getcarrierUpdateList(YFCDocument yfcInDoc)
	{
		//YFCDocument yfcDocCarrierUpdateIn = YFCDocument.getDocumentForXMLFile("C:\\Users\\Manish\\Desktop\\Test\\TestCarrierUpdate.xml");
		YFCDocument yfcDocCarrierUpdateListOut = getServiceInvoker().invokeYantraService(getProperty("GetCarrierUpdateListService", true), yfcInDoc);
		return yfcDocCarrierUpdateListOut;
	}

}

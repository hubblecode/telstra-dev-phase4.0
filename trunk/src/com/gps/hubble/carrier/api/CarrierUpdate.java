/***********************************************************************************************
 * File Name : CarrierUpdate.java
 * 
 * Description : Custom API for Carrier updates to update the shipment to Intransit (along with the
 * intransit updates), Delivered (along with Proof Of Delivery) or Futile.
 * 
 * Modification Log :
 * --------------------------------------------------------------------------------------------- Ver
 * # Date Author Modification
 * --------------------------------------------------------------------------------------------- 1.0
 * Murali Initial Version 1.1 Jan 13,2017 Manish Kumar Fixed defects HUB-8120, HUB-8121 1.2 Jan
 * 18,2017 Prateek Kumar Fixed HUB-8301 : Updating the carrier update table if the shipment is
 * already delivered/futile and carrier update comes for it. 1.3 May 23,2017 Prateek Kumar Fixed
 * defect HUB-9093 1.4 Jun 4,2017 Gladston Xavier Fixed defect HUB-9090 1.5 Jun 26,2017 Gladston
 * Xavier Consignment Ready, Service Completed and Return Order (Intransit and Delivered) changes.
 * Aug 25,2017 Gladston Xavier - HUB-9461  and exception related fixes.
 * ---------------------------------------------------------------------------------------------
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
 **********************************************************************************************/
package com.gps.hubble.carrier.api;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XMLUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.constants.TelstraErrorCodeConstants;
import com.sterlingcommerce.baseutil.SCUtil;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSException;

public class CarrierUpdate extends AbstractCustomApi {

  private static YFCLogCategory logger = YFCLogCategory.instance(CarrierUpdate.class);

  /**
   * Base Method execution starts here
   * 
   */
  private boolean isPrimeAvl = false;
  private boolean isQtyAvl = false;
  private boolean isTrackAvl = false;
  private boolean isIntegral = false;
  private boolean isVector = false;
  private boolean isAdhoc = false;
  private boolean isNotificationOnly = false;
  private boolean isStockMovement = false;
  private boolean isShipNoPassed = false;
  private boolean isUpdtZeroQtyPO = false;
  private String strTransportStatus = null;
  private String strTransportText = null;
  private String strTrackingNo = null;
  private String strOrderNo = null;
  private String strDocType = null;
  private String strOrderName = null;
  private String strVectorId = null;
  private String strShipNo = null;
  // private String strShipKey = null;
  private String strOrderHeaderKey = null;
  private String strVectorPrimeLineNo = null;
  // private String strQty = null;
  private YFCDocument adhocShipListDoc = null;
  private Set<String> statusSet = null;

  @Override
  public YFCDocument invoke(YFCDocument inXml) {
    LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", inXml);
    try {
      validateInputTracking(inXml);
      validateInputOrder(inXml);
      if (isNotificationOnly) {
        processNotification(inXml);
      }
      if (isIntegral || isStockMovement) {
        processIntegral(inXml);
      }
      if (isVector) {
        processVector(inXml);
      }
      if (isAdhoc) {
        processAdhoc(inXml);
      }
		} catch (YFSException exp) {
			String strErrCode = exp.getErrorCode();

			if (strErrCode.equalsIgnoreCase("TEL_ERR_1171_027")
					|| (strErrCode.equalsIgnoreCase("TEL_ERR_1171_013")
							&& "N".equalsIgnoreCase(getProperty("THROW_ERR_1171_013", false)))
					|| strErrCode.equalsIgnoreCase("TEL_ERR_1151_003")) {
				return inXml;
			}
			LoggerUtil.verboseLog("Carrier Update Failure ", logger, exp);
			throw exp;
		}
    LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", inXml);
    return inXml;
  }

  private void processNotification(YFCDocument inXml) {
    YFCDocument changeOrderIPDoc =
        YFCDocument.getDocumentFor("<Order OrderHeaderKey='" + strOrderHeaderKey
            + "' Override='Y'><Notes> <Note/> </Notes> </Order>");
    YFCElement noteElem =
        changeOrderIPDoc.getDocumentElement().getElementsByTagName("Note").item(0);
    if (isNotificationOnly) {
      noteElem.setAttribute("ContactUser", "Unsccessfull (Notification Only)");
    } else {
      noteElem.setAttribute("ContactUser", strTransportStatus);
    }
    noteElem.setAttribute("NoteText", strTransportText + " TrackingNo:" + strTrackingNo);
    invokeYantraApi("changeOrder", changeOrderIPDoc);
  }

  private void processAdhoc(YFCDocument inXml) {

    if (!SCUtil.isVoid(adhocShipListDoc)) {

      YFCElement shipElem =
          adhocShipListDoc.getDocumentElement().getElementsByTagName("Shipment").item(0);
      updateCarrierStatus(inXml, shipElem);

    } else {
      // will not reach here
      throw ExceptionUtil.getYFSException("TEL_ERR_1171_009",
          "There is no Shipment available in the System for the passed AdHoc Carrier Message"
              + inXml, new YFSException());
    }

  }

  private void updateCarrierStatus(YFCDocument inXml, YFCElement shipElem) {
    LoggerUtil
        .startComponentLog(logger, this.getClass().getName(), "updateCarrierStatus", shipElem);

    if (!strDocType.equalsIgnoreCase(TelstraConstants.DOCUMENT_TYPE_0003)) {
      updateCarrierStatusNonRO(inXml, shipElem);
    } else {
      updateCarrierStatusRO(inXml, shipElem);
    }

    LoggerUtil.endComponentLog(logger, this.getClass().getName(), "updateCarrierStatus",
        "Carrier Update Success");

  }

  private void updateCarrierStatusRO(YFCDocument inXml, YFCElement shipElem) {
    String inTransit = getProperty("GPS_IN_TRANSIT", true);
    String carrierInTransit = getProperty("INTRANSIT_STATUS", true);
    String carrierDelivered = getProperty("DELIVERED_STATUS", true);
    String carrierFailed = getProperty("FAILED_STATUS", true);
    String serComp = getProperty("GPS_SER_COMP", true);
    Double shipmentStatus = shipElem.getDoubleAttribute(TelstraConstants.STATUS);
    String documentType = shipElem.getAttribute(TelstraConstants.DOCUMENT_TYPE);
    String carrierStatus = strTransportStatus;
    String shipmentKey = shipElem.getAttribute(TelstraConstants.SHIPMENT_KEY);
    String shipTrackNo = shipElem.getAttribute(TelstraConstants.TRACKING_NO);
    String serviceCompleted = getProperty("SER_COMP_STATUS", true);
    YFCDocument gpsReceiveIPDoc = getGpsReceiptIPDoc(shipElem, inXml);

    if (shipmentStatus < 1400) {
      changeShipmentTranStatus(shipmentKey, TelstraConstants.CONFIRM_SHIPMENT_API);
      shipmentStatus = (double) 1400;
    }
    if (carrierInTransit.equals(carrierStatus) && shipmentStatus != 1400.10000) {
      if (shipmentStatus < 1400.10000) {
        changeShipmentStatus(shipmentKey, "GPS_IN_TRANSIT." + documentType + ".ex", inTransit);
        changeOrderStatus(shipmentKey, "GPS_INTRANSIT." + documentType + ".ex", "3700.10000");
      }
    } else if (carrierFailed.equals(carrierStatus)) {
      processNotification(inXml);
      // throw
      // ExceptionUtil.getYFSException("TEL_ERR_1171_010","Futile for Return - Not valid Scenario - Input XML"
      // + inXml, new YFSException());
    } else if (carrierDelivered.equals(carrierStatus) && shipmentStatus < 1500) {
      if (shipmentStatus < 1400.10000) {
        changeShipmentStatus(shipmentKey, "GPS_IN_TRANSIT." + documentType + ".ex", inTransit);
        changeOrderStatus(shipmentKey, "GPS_INTRANSIT." + documentType + ".ex", "3700.10000");
      }
      changeShipmentTranStatus(shipmentKey, TelstraConstants.DELIVER_SHIPMENT_API);
      changeOrderStatus(shipmentKey, "GPS_DELIVER." + documentType + ".ex", "3700.20000");
    } else if (serviceCompleted.equalsIgnoreCase(carrierStatus) && shipmentStatus < 1600.00001) {
      if (shipmentStatus < 1400.10000) {
        changeShipmentStatus(shipmentKey, "GPS_IN_TRANSIT." + documentType + ".ex", inTransit);
        changeOrderStatus(shipmentKey, "GPS_INTRANSIT." + documentType + ".ex", "3700.10000");
        changeShipmentTranStatus(shipmentKey, TelstraConstants.DELIVER_SHIPMENT_API);
        changeOrderStatus(shipmentKey, "GPS_DELIVER." + documentType + ".ex", "3700.20000");
        invokeYantraService("GpsReceiveOrder", gpsReceiveIPDoc);
      } else if (shipmentStatus < 1500) {
        changeShipmentTranStatus(shipmentKey, TelstraConstants.DELIVER_SHIPMENT_API);
        changeOrderStatus(shipmentKey, "GPS_DELIVER." + documentType + ".ex", "3700.20000");
        invokeYantraService("GpsReceiveOrder", gpsReceiveIPDoc);
      } else if (shipmentStatus < 1600) {
        invokeYantraService("GpsReceiveOrder", gpsReceiveIPDoc);
      }
      changeShipmentStatus(shipmentKey, "GPS_SHIP_SER_COMP." + documentType + ".ex", serComp);
      changeOrderStatus(shipmentKey, "GPS_SER_COMP." + documentType + ".ex", "3950.9999");
    }
    if (isTrackAvl && (strTrackingNo.equalsIgnoreCase(shipTrackNo) || SCUtil.isVoid(shipTrackNo))) {
      modifyShipment(inXml, shipmentKey, strTrackingNo, true);
    } else {
      modifyShipment(inXml, shipmentKey, strTrackingNo, false);
    }
  }

  private YFCDocument getGpsReceiptIPDoc(YFCElement shipElem, YFCDocument inXml) {
    YFCDocument outputGetOrderLineList = getOrderLineList();
    YFCNodeList<YFCElement> orderLineNL = outputGetOrderLineList.getElementsByTagName("OrderLine");
    String shipmentNo = shipElem.getAttribute(TelstraConstants.SHIPMENT_NO);
    String receivingNode = shipElem.getAttribute(TelstraConstants.RECEIVING_NODE);
    YFCDocument receiveOrderIPDoc =
        YFCDocument.getDocumentFor("<Receipt ShipmentNo='" + shipmentNo + "' OrderName='"
            + strOrderName + "' ReceivingNode='" + receivingNode + "'> </Receipt>");
    YFCElement recptElem = receiveOrderIPDoc.getDocumentElement();
    YFCElement recptLinesElm = recptElem.createChild("ReceiptLines");
    if (isPrimeAvl) {
      YFCDocument cloneIPDoc = XMLUtil.getDocumentFor(inXml.getDocumentElement().cloneNode(true));
      YFCNodeList<YFCElement> shipmentLineNL = cloneIPDoc.getElementsByTagName("ShipmentLine");
      for (YFCElement shipmentLineEle : shipmentLineNL) {
        String strPLno = shipmentLineEle.getAttribute(TelstraConstants.PRIME_LINE_NO);
        for (YFCElement orderLineElem : orderLineNL) {
          String strLineNo = orderLineElem.getAttribute(TelstraConstants.PRIME_LINE_NO);
          if (strPLno.equalsIgnoreCase(strLineNo)) {
            shipmentLineEle.setAttribute(TelstraConstants.QUANTITY,
                orderLineElem.getAttribute("OrderedQty"));
            String sItemID =
                orderLineElem.getElementsByTagName("Item").item(0)
                    .getAttribute(TelstraConstants.ITEM_ID);
            shipmentLineEle.setAttribute(TelstraConstants.ITEM_ID, sItemID);
            String sUnitOfMeasure =
                orderLineElem.getElementsByTagName("Item").item(0)
                    .getAttribute(TelstraConstants.UNIT_OF_MEASURE);
            shipmentLineEle.setAttribute(TelstraConstants.UNIT_OF_MEASURE, sUnitOfMeasure);
            shipmentLineEle.setAttribute(TelstraConstants.SUB_LINE_NO, "1");
          }
        }

        YFCElement shipLineElem = receiveOrderIPDoc.importNode(shipmentLineEle, true);
        recptLinesElm.appendChild(shipLineElem);
        receiveOrderIPDoc.getDocument().renameNode(shipLineElem.getDOMNode(), null, "ReceiptLine");
      }
    } else {
      for (YFCElement orderLineElem : orderLineNL) {
        YFCElement receiptLineElem = recptLinesElm.createChild("ReceiptLine");
        receiptLineElem.setAttribute(TelstraConstants.PRIME_LINE_NO,
            orderLineElem.getAttribute(TelstraConstants.PRIME_LINE_NO));
        receiptLineElem.setAttribute(TelstraConstants.QUANTITY,
            orderLineElem.getAttribute("OrderedQty"));
        String sItemID =
            orderLineElem.getElementsByTagName("Item").item(0)
                .getAttribute(TelstraConstants.ITEM_ID);
        receiptLineElem.setAttribute(TelstraConstants.ITEM_ID, sItemID);
        String sUnitOfMeasure =
            orderLineElem.getElementsByTagName("Item").item(0)
                .getAttribute(TelstraConstants.UNIT_OF_MEASURE);
        receiptLineElem.setAttribute(TelstraConstants.UNIT_OF_MEASURE, sUnitOfMeasure);
        receiptLineElem.setAttribute(TelstraConstants.SUB_LINE_NO, "1");
        recptLinesElm.appendChild(receiptLineElem);
      }
    }
    return receiveOrderIPDoc;

  }

  private void updateCarrierStatusNonRO(YFCDocument inXml, YFCElement shipElem) {
    String inTransit = getProperty("GPS_IN_TRANSIT", true);
    String futileDle = getProperty("GPS_FUTILE_DEL", true);
    String conReady = getProperty("GPS_CON_READY", true);
    String serComp = getProperty("GPS_SER_COMP", true);
    String carrierInTransit = getProperty("INTRANSIT_STATUS", true);
    String carrierDelivered = getProperty("DELIVERED_STATUS", true);
    String consignmentReady = getProperty("CON_READY_STATUS", true);
    String carrierFailed = getProperty("FAILED_STATUS", true);
    String serviceCompleted = getProperty("SER_COMP_STATUS", true);
    Double shipmentStatus = shipElem.getDoubleAttribute(TelstraConstants.STATUS);
    String documentType = shipElem.getAttribute(TelstraConstants.DOCUMENT_TYPE);
    String carrierStatus = strTransportStatus;
    String shipmentKey = shipElem.getAttribute(TelstraConstants.SHIPMENT_KEY);
    String shipTrackNo = shipElem.getAttribute(TelstraConstants.TRACKING_NO);

    if (shipmentStatus < 1400) {
      if (!strDocType.equalsIgnoreCase("0005")) {
        YFCDocument shipConfirmDocument =
            YFCDocument.getDocumentFor("<Shipment OrderName='" + strOrderName + "' ShipmentKey='"
                + shipmentKey + "' TrackingNo='" + strTrackingNo + "' />");
        invokeYantraService("GpsShipOrRejectOrder", shipConfirmDocument);
        // changeShipmentTranStatus(shipmentKey, TelstraConstants.CONFIRM_SHIPMENT_API);
      } else {
        changeShipmentTranStatus(shipmentKey, TelstraConstants.CONFIRM_SHIPMENT_API);
      }
      shipmentStatus = (double) 1400;
    }
    if (consignmentReady.equalsIgnoreCase(carrierStatus) && shipmentStatus != 1400.09999) {
      if (shipmentStatus < 1400.09999) {
        changeShipmentStatus(shipmentKey, "GPS_SHIP_CON_READY." + documentType + ".ex", conReady);
        changeOrderStatus(shipmentKey, "GPS_CONS_READY." + documentType + ".ex", "3700.8888");
      }
    } else if (carrierInTransit.equals(carrierStatus) && shipmentStatus != 1400.10000) {
      if (shipmentStatus < 1400.10000) {
        changeShipmentStatus(shipmentKey, "GPS_IN_TRANSIT." + documentType + ".ex", inTransit);
        changeOrderStatus(shipmentKey, "GPS_INTRANSIT." + documentType + ".ex", "3700.10000");
      }
    } else if (carrierFailed.equals(carrierStatus)) {
      // HUB-5242 [Begin]
      if (shipmentStatus < 1400.10000) {
        changeShipmentStatus(shipmentKey, "GPS_IN_TRANSIT." + documentType + ".ex", inTransit);
        changeOrderStatus(shipmentKey, "GPS_INTRANSIT." + documentType + ".ex", "3700.10000");
      }
      // HUB-5242 [End]
      changeShipmentTranStatus(shipmentKey, TelstraConstants.UN_CONFIRM_SHIPMENT);
      changeShipmentStatus(shipmentKey, "GPS_FUTILE_DEL." + documentType + ".ex", futileDle);
      changeOrderStatus(shipmentKey, "GPS_FUTILE_DELIVERY." + documentType + ".ex", "9000.30000");

    } else if (carrierDelivered.equals(carrierStatus) && shipmentStatus < 1500) {
      if (shipmentStatus < 1400.10000) {
        changeShipmentStatus(shipmentKey, "GPS_IN_TRANSIT." + documentType + ".ex", inTransit);
        changeOrderStatus(shipmentKey, "GPS_INTRANSIT." + documentType + ".ex", "3700.10000");
      }
      changeShipmentTranStatus(shipmentKey, TelstraConstants.DELIVER_SHIPMENT_API);
      changeOrderStatus(shipmentKey, "GPS_DELIVER." + documentType + ".ex", "3700.7777");
    } else if (serviceCompleted.equalsIgnoreCase(carrierStatus) && shipmentStatus < 1600.00001) {
      if (shipmentStatus < 1400.10000) {
        changeShipmentStatus(shipmentKey, "GPS_IN_TRANSIT." + documentType + ".ex", inTransit);
        changeOrderStatus(shipmentKey, "GPS_INTRANSIT." + documentType + ".ex", "3700.10000");
        changeShipmentTranStatus(shipmentKey, TelstraConstants.DELIVER_SHIPMENT_API);
        changeOrderStatus(shipmentKey, "GPS_DELIVER." + documentType + ".ex", "3700.7777");
      } else if (shipmentStatus < 1500) {
        changeShipmentTranStatus(shipmentKey, TelstraConstants.DELIVER_SHIPMENT_API);
        changeOrderStatus(shipmentKey, "GPS_DELIVER." + documentType + ".ex", "3700.7777");
      }
      changeShipmentStatus(shipmentKey, "GPS_SHIP_SER_COMP." + documentType + ".ex", serComp);
      changeOrderStatus(shipmentKey, "GPS_SER_COMP." + documentType + ".ex", "3950.9999");
    }
    if (isTrackAvl && (strTrackingNo.equalsIgnoreCase(shipTrackNo) || SCUtil.isVoid(shipTrackNo))) {
      modifyShipment(inXml, shipmentKey, strTrackingNo, true);
    } else {
      modifyShipment(inXml, shipmentKey, strTrackingNo, false);
    }

  }

  /**
   * Method to add the carrier update information in HangOff Table for Shipment.
   * 
   * @param inXml
   * @param shipmentKey
   */
  private void modifyShipment(YFCDocument inXml, String shipmentKey, String strTrackingNo,
      boolean bUpdateTrackingNo) {
    LoggerUtil.startComponentLog(logger, this.getClass().getName(), "modifyShipment", inXml);
    YFCElement extnEle = inXml.getDocumentElement().getChildElement(TelstraConstants.EXTN);
    YFCDocument modifyShipXml =
        YFCDocument.getDocumentFor("<Shipment ShipmentKey='" + shipmentKey + "' />");
    YFCElement modifyShipEle = modifyShipXml.getDocumentElement();
    if (bUpdateTrackingNo) {
      if (!SCUtil.isVoid(strTrackingNo)) {
        modifyShipEle.setAttribute(TelstraConstants.TRACKING_NO, strTrackingNo);
      }
    }
    // modifyShipEle.importNode(extnEle);
    // HUB-8301 : [Begin]
    /*
     * If the carrier update message is coming for the shipment which is already delivered or in
     * futile status, update the carrier update table instead of creating the new entry.
     */
    String sTransportStatus = strTransportStatus;
    String sCarrierUpdateKeyForDelivered = "";
    if (TelstraConstants.DELIVERED.equalsIgnoreCase(sTransportStatus)
        || TelstraConstants.FUTILE.equalsIgnoreCase(sTransportStatus)) {
      sCarrierUpdateKeyForDelivered = getCarrierUpdateKeyForDelivered(shipmentKey);
    }
    if (!YFCCommon.isStringVoid(sCarrierUpdateKeyForDelivered)) {
      invokeYantraApi(TelstraConstants.CHANGE_SHIPMENT, modifyShipXml);
      modifyShipEle.importNode(extnEle);
      updateCarrierUpdateStatus(inXml, sCarrierUpdateKeyForDelivered);
    } else {
      modifyShipEle.importNode(extnEle);
      invokeYantraApi(TelstraConstants.CHANGE_SHIPMENT, modifyShipXml);
    }
    // HUB-8301 : [End]
    LoggerUtil.endComponentLog(logger, this.getClass().getName(), "modifyShipment",
        "Shipment Modification Success");
  }

  /**
   * 
   * @param inXml
   * @param sCarrierUpdateKeyForDelivered
   */
  private void updateCarrierUpdateStatus(YFCDocument inXml, String sCarrierUpdateKeyForDelivered) {

    YFCElement yfcEleCarrierUpdate =
        inXml.getElementsByTagName(TelstraConstants.CARRIER_UPDATE).item(0);
    Map<String, String> mapCarrierUpdateAttri = yfcEleCarrierUpdate.getAttributes();

    YFCDocument yfcDocChangeCarrierUpdateIp = YFCDocument.getDocumentFor("<CarrierUpdate/>");
    YFCElement yfcEleChangeCarrierUpdateRoot = yfcDocChangeCarrierUpdateIp.getDocumentElement();
    yfcEleChangeCarrierUpdateRoot.setAttributes(mapCarrierUpdateAttri);
    yfcEleChangeCarrierUpdateRoot.setAttribute(TelstraConstants.CARRIER_UPDATE_KEY,
        sCarrierUpdateKeyForDelivered);
    LoggerUtil.verboseLog(
        "CarrierUpdate :: checkCarrierUpdateTable :: yfcDocChangeCarrierUpdateIp ::", logger,
        yfcDocChangeCarrierUpdateIp);
    invokeYantraService(TelstraConstants.GPS_CHANGE_CARRIER_UPDATE, yfcDocChangeCarrierUpdateIp);

  }

  /**
   * 
   * @param shipmentKey
   * @return
   */
  private String getCarrierUpdateKeyForDelivered(String shipmentKey) {

    String sCarrierUpdateKeyForDelivered = "";

    YFCDocument yfcDocGetCarrierUpdateListIp =
        YFCDocument.getDocumentFor("<CarrierUpdate ShipmentKey='" + shipmentKey
            + "' TransportStatus='Delivered' />");

    LoggerUtil.verboseLog(
        "CarrierUpdate :: checkCarrierUpdateTable :: yfcDocGetCarrierUpdateListIp ::", logger,
        yfcDocGetCarrierUpdateListIp);
    YFCDocument yfcDocGetCarrierUpdateListOp =
        invokeYantraService(TelstraConstants.GET_CARRIER_UPDATE_LIST, yfcDocGetCarrierUpdateListIp);

    YFCNodeList<YFCNode> yfcNodeCarrierUpdate =
        yfcDocGetCarrierUpdateListOp.getDocumentElement().getChildNodes();
    if (yfcNodeCarrierUpdate.getLength() > 0) {
      sCarrierUpdateKeyForDelivered =
          ((YFCElement) yfcNodeCarrierUpdate.item(0))
              .getAttribute(TelstraConstants.CARRIER_UPDATE_KEY);
    }
    return sCarrierUpdateKeyForDelivered;
  }


  /**
   * Method to change Shipment status based on the Carrier Information
   * 
   * If Carrier Status is Consignment Picked Up then Shipment status is In transit. If Carrier
   * Status is Unsuccessful then Shipment status is Futile Delivery
   * 
   * @param shipmentKey
   * @param transactionId
   * @param baseDropStatus
   */
  private void changeShipmentStatus(String shipmentKey, String transactionId, String baseDropStatus) {
    LoggerUtil.startComponentLog(logger, this.getClass().getName(), "changeShipmentStatus",
        "ShipmentKey :" + shipmentKey + "BaseDropStatus :" + baseDropStatus);
    YFCDocument changeShipStatXml = YFCDocument.getDocumentFor("<Shipment />");
    changeShipStatXml.getDocumentElement().setAttribute(TelstraConstants.SHIPMENT_KEY, shipmentKey);
    changeShipStatXml.getDocumentElement().setAttribute("TransactionId", transactionId);
    changeShipStatXml.getDocumentElement().setAttribute("BaseDropStatus", baseDropStatus);
    invokeYantraApi(TelstraConstants.CHANGE_SHIPMENT_STATUS_API, changeShipStatXml);
    LoggerUtil.endComponentLog(logger, this.getClass().getName(), "changeShipmentStatus",
        "Change Shipment Status Success");
  }

  /**
   * Method to change Order status based on the Carrier Information
   * 
   * If Carrier Status is Consignment Picked Up then Order status is In transit. If Carrier Status
   * is Unsuccessful then Order status is Futile Delivery If Carrier Status is Successful then Order
   * status is Delivered
   * 
   * @param shipmentKey
   * @param transactionId
   * @param baseDropStatus
   */
  private void changeOrderStatus(String shipmentKey, String transactionId, String baseDropStatus) {
    LoggerUtil.startComponentLog(logger, this.getClass().getName(), "changeOrderStatus",
        "ShipmentKey :" + shipmentKey + "BaseDropStatus :" + baseDropStatus);

    YFCDocument changeOrderStatusInDoc =
        YFCDocument.getDocumentFor("<OrderStatusChange " + "BaseDropStatus='" + baseDropStatus
            + "' TransactionId='" + transactionId + "'>" + "<OrderLines/></OrderStatusChange>");
    YFCElement orderStatusChangeELe = changeOrderStatusInDoc.getDocumentElement();
    YFCElement orderLinesEle = orderStatusChangeELe.getChildElement("OrderLines");

    YFCDocument getShipmentLineListInDoc =
        YFCDocument.getDocumentFor("<ShipmentLine ShipmentKey='" + shipmentKey + "'/>");
    YFCDocument getShipmentLineListTemp =
        YFCDocument
            .getDocumentFor("<ShipmentLines>"
                + "<ShipmentLine OrderHeaderKey='' OrderLineKey='' OrderReleaseKey='' Quantity=''/></ShipmentLines>");
    YFCDocument getShipmentLineListOutDoc =
        invokeYantraApi("getShipmentLineList", getShipmentLineListInDoc, getShipmentLineListTemp);
    YFCElement shipmentLinesEle = getShipmentLineListOutDoc.getDocumentElement();
    YFCNodeList<YFCElement> shipmentLineNL = shipmentLinesEle.getElementsByTagName("ShipmentLine");
    for (int i = 0; i < shipmentLineNL.getLength(); i++) {
      YFCElement shipmentLineEle = (YFCElement) shipmentLineNL.item(i);
      String strOrderLineKey = shipmentLineEle.getAttribute("OrderLineKey");
      String strOrderReleaseKey = shipmentLineEle.getAttribute("OrderReleaseKey", "");
      String strQuantity = shipmentLineEle.getAttribute("Quantity");

      if (i == 0) {
        String strOrderHeaderKey = shipmentLineEle.getAttribute("OrderHeaderKey");
        orderStatusChangeELe.setAttribute("OrderHeaderKey", strOrderHeaderKey);
      }

      YFCElement orderLineEle = orderLinesEle.createChild("OrderLine");
      orderLineEle.setAttribute("OrderLineKey", strOrderLineKey);
      if (!strOrderReleaseKey.equals("")) {
        orderLineEle.setAttribute("OrderReleaseKey", strOrderReleaseKey);
      }
      orderLineEle.setAttribute("Quantity", strQuantity);
    }

    invokeYantraApi("changeOrderStatus", changeOrderStatusInDoc);
    LoggerUtil.endComponentLog(logger, this.getClass().getName(), "changeOrderStatus",
        "Change Order Status Success");
  }

  /**
   * 
   * Method to confirm / un confirm / deliver Status based on carrier status and shipment status
   * 
   * @param shipmentStatus
   * @param shipmentKey
   * @param api
   */
  private void changeShipmentTranStatus(String shipmentKey, String api) {
    LoggerUtil.startComponentLog(logger, this.getClass().getName(), "validateConfirmShipment",
        "ShipmentKey :" + shipmentKey + "API :" + api);
    YFCDocument shipConfirmXml =
        YFCDocument.getDocumentFor("<Shipment ShipmentKey='" + shipmentKey + "' />");
    invokeYantraApi(api, shipConfirmXml);
    LoggerUtil.endComponentLog(logger, this.getClass().getName(), "validateConfirmShipment",
        "API Success");
  }


  private void processVector(YFCDocument inXml) {

    YFCDocument shipListVecDoc = getVectorShipmentList(inXml);
    double dNoOfShipment =
        shipListVecDoc.getDocumentElement().getDoubleAttribute("TotalNumberOfRecords");
    if (dNoOfShipment > 0.0) {
      processVectorShipmentPresent(shipListVecDoc, inXml);
    } else {
      processShipmentNotPresent(inXml);
    }
  }

  private YFCDocument getVectorShipmentList(YFCDocument inXml) {
    YFCDocument getShipListIPDoc =
        YFCDocument.getDocumentFor("<Shipment> <ShipmentLines> <ShipmentLine> <Order OrderNo='"
            + strOrderNo + "' OrderName='" + strOrderName + "'> </Order> <OrderLine PrimeLineNo='"
            + strVectorPrimeLineNo
            + "'> </OrderLine>  </ShipmentLine> </ShipmentLines> </Shipment>");
    YFCElement shipElem = getShipListIPDoc.getDocumentElement();
    if (isShipNoPassed) {
      shipElem.setAttribute("ShipmentNo", strShipNo);
    }
    YFCDocument getShipListTempDoc =
        YFCDocument
            .getDocumentFor("<Shipments TotalNumberOfRecords=''>"
                + "<Shipment DocumentType='' ShipmentNo='' ShipmentKey='' Status='' TrackingNo='' ReceivingNode='' ><ShipmentLines><ShipmentLine PrimeLineNo='' OrderNo='' Quantity='' ><Order OrderName=''/></ShipmentLine> </ShipmentLines></Shipment></Shipments>");
    YFCDocument getShipListOPDoc =
        invokeYantraApi(TelstraConstants.GET_SHIPMENT_LIST_API, getShipListIPDoc,
            getShipListTempDoc);
    return getShipListOPDoc;
  }

  private void processIntegral(YFCDocument inXml) {

    YFCDocument shipListIntegDoc = getShipmentList(inXml);
    double dNoOfShipment =
        shipListIntegDoc.getDocumentElement().getDoubleAttribute("TotalNumberOfRecords");
    if (dNoOfShipment > 0.0) {
      processShipmentPresent(shipListIntegDoc, inXml);
    } else {
      processShipmentNotPresent(inXml);
    }
  }


  private void processShipmentNotPresent(YFCDocument inXml) {
    if (isPrimeAvl) {
      if (strDocType.equalsIgnoreCase("0003")) {
        YFCDocument yfcDocCreateShipmentIP = createROShipmentLinePassed(inXml);
        invokeYantraApi(TelstraConstants.API_CREATE_SHIPMENT, yfcDocCreateShipmentIP);
        findShipAgain(inXml);
      } else if (strDocType.equalsIgnoreCase("0005")) {
        YFCDocument poASNInDoc = preparePOASN(inXml);
        invokeYantraService("GpsProcessPOASNMsg", poASNInDoc);
        findShipAgain(inXml);
      } else {
        YFCDocument shipConfirmDocument = prepareInputForShipConfirm(inXml, strTrackingNo);
        invokeYantraService("GpsShipOrRejectOrder", shipConfirmDocument);
        findShipAgain(inXml);
      }
    } else {
      if (strDocType.equalsIgnoreCase("0003")) {
        YFCDocument yfcDocCreateShipmentIP = createROShipment(inXml);
        invokeYantraApi(TelstraConstants.API_CREATE_SHIPMENT, yfcDocCreateShipmentIP);
        findShipAgain(inXml);
      } else {
        throw ExceptionUtil.getYFSException("TEL_ERR_1171_011",
            "Prime Line no Not available in the Carrier Message and no shipments available in the System"
                + inXml, new YFSException());
      }
    }
  }

  private void processPOASN(YFCDocument poASNInDoc, YFCDocument inXml) {
    YFCElement eleShipmentToAddress =
        poASNInDoc.getDocumentElement().getElementsByTagName(TelstraConstants.PERSON_INFO_SHIP_TO)
            .item(0);
    poASNInDoc.getDocument().renameNode(eleShipmentToAddress.getDOMNode(), null, "ToAddress");
    YFCDocument shipOutDoc =
        invokeYantraApi("createShipment", poASNInDoc, getCreateShipmentTemplate());
    YFCElement shipElem = shipOutDoc.getDocumentElement();
    YFCNodeList<YFCElement> shipLineElems = shipElem.getElementsByTagName("ShipmentLine");
    for (YFCElement shipLineElem : shipLineElems) {
      shipLineElem.setAttribute("Quantity", "0");
    }
    YFCDocument shipDoc =
        invokeYantraApi("changeShipment", shipOutDoc, getCreateShipmentTemplate());
    updateCarrierStatusZeroPO(inXml, shipDoc.getDocumentElement());
  }

  private void updateCarrierStatusZeroPO(YFCDocument inXml, YFCElement shipElem) {
    String inTransit = getProperty("GPS_IN_TRANSIT", true);
    String futileDle = getProperty("GPS_FUTILE_DEL", true);
    String conReady = getProperty("GPS_CON_READY", true);
    String carrierInTransit = getProperty("INTRANSIT_STATUS", true);
    String carrierDelivered = getProperty("DELIVERED_STATUS", true);
    String consignmentReady = getProperty("CON_READY_STATUS", true);
    String carrierFailed = getProperty("FAILED_STATUS", true);
    Double shipmentStatus = shipElem.getDoubleAttribute(TelstraConstants.STATUS);
    String documentType = shipElem.getAttribute(TelstraConstants.DOCUMENT_TYPE);
    String carrierStatus = strTransportStatus;
    String shipmentKey = shipElem.getAttribute(TelstraConstants.SHIPMENT_KEY);
    String shipTrackNo = shipElem.getAttribute(TelstraConstants.TRACKING_NO);

    if (shipmentStatus < 1400) {
      changeShipmentTranStatus(shipmentKey, TelstraConstants.CONFIRM_SHIPMENT_API);
      shipmentStatus = (double) 1400;
    }
    if (carrierInTransit.equals(carrierStatus) && shipmentStatus != 1400.10000) {
      if (shipmentStatus < 1400.10000) {
        changeShipmentStatus(shipmentKey, "GPS_IN_TRANSIT." + documentType + ".ex", inTransit);
        // changeOrderStatus(shipmentKey, "GPS_INTRANSIT." + documentType + ".ex", "3700.10000");
      }
    } else if (carrierFailed.equals(carrierStatus)) {
      // HUB-5242 [Begin]
      if (shipmentStatus < 1400.10000) {
        changeShipmentStatus(shipmentKey, "GPS_IN_TRANSIT." + documentType + ".ex", inTransit);
        // changeOrderStatus(shipmentKey, "GPS_INTRANSIT." + documentType + ".ex", "3700.10000");
      }
      // HUB-5242 [End]
      changeShipmentTranStatus(shipmentKey, TelstraConstants.UN_CONFIRM_SHIPMENT);
      changeShipmentStatus(shipmentKey, "GPS_FUTILE_DEL." + documentType + ".ex", futileDle);
      // changeOrderStatus(shipmentKey, "GPS_FUTILE_DELIVERY." + documentType + ".ex",
      // "9000.30000");

    } else if (carrierDelivered.equals(carrierStatus) && shipmentStatus < 1500) {
      if (shipmentStatus < 1400.10000) {
        changeShipmentStatus(shipmentKey, "GPS_IN_TRANSIT." + documentType + ".ex", inTransit);
        // changeOrderStatus(shipmentKey, "GPS_INTRANSIT." + documentType + ".ex", "3700.10000");
      }
      changeShipmentTranStatus(shipmentKey, TelstraConstants.DELIVER_SHIPMENT_API);
      // changeOrderStatus(shipmentKey, "GPS_DELIVER." + documentType + ".ex", "3700.7777");
    } else if (consignmentReady.equalsIgnoreCase(carrierStatus) && shipmentStatus < 1500.1000) {
      if (shipmentStatus < 1400.10000) {
        changeShipmentStatus(shipmentKey, "GPS_IN_TRANSIT." + documentType + ".ex", inTransit);
        // changeOrderStatus(shipmentKey, "GPS_INTRANSIT." + documentType + ".ex", "3700.10000");
        changeShipmentTranStatus(shipmentKey, TelstraConstants.DELIVER_SHIPMENT_API);
        // changeOrderStatus(shipmentKey, "GPS_DELIVER." + documentType + ".ex", "3700.7777");
      } else if (shipmentStatus < 1500) {
        changeShipmentTranStatus(shipmentKey, TelstraConstants.DELIVER_SHIPMENT_API);
        // changeOrderStatus(shipmentKey, "GPS_DELIVER." + documentType + ".ex", "3700.7777");
      }
      changeShipmentStatus(shipmentKey, "GPS_SHIP_CON_READY." + documentType + ".ex", conReady);
      // changeOrderStatus(shipmentKey, "GPS_CONS_READY." + documentType + ".ex", "3700.8888");
    }
    if (isTrackAvl && (strTrackingNo.equalsIgnoreCase(shipTrackNo) || SCUtil.isVoid(shipTrackNo))) {
      modifyShipment(inXml, shipmentKey, strTrackingNo, true);
    } else {
      modifyShipment(inXml, shipmentKey, strTrackingNo, false);
    }
  }

  private YFCDocument getCreateShipmentTemplate() {
    YFCDocument template =
        YFCDocument
            .getDocumentFor("<Shipment EnterpriseCode='' DocumentType='' ReceivingNode='' ShipmentKey='' Status='' ShipmentNo='' TrackingNo='' >"
                + "<ShipmentLines><ShipmentLine Quantity='' ShipmentLineKey='' ItemID='' ReceivedQuantity='' PrimeLineNo=''><OrderLine PrimeLineNo=''/></ShipmentLine></ShipmentLines></Shipment>");
    return template;
  }

  private YFCDocument createROShipmentLinePassed(YFCDocument inXml) {
    YFCDocument outputGetOrderLineList = getOrderLineList();
    YFCNodeList<YFCElement> orderLineNL = outputGetOrderLineList.getElementsByTagName("OrderLine");
    YFCDocument docCreateShipmentIp =
        YFCDocument.getDocumentFor("<Shipment OrderHeaderKey='" + strOrderHeaderKey
            + "' OrderName='" + strOrderName + "' " + "TrackingNo='" + strTrackingNo
            + "' DocumentType='" + strDocType + "' />");
    YFCElement shipElem = docCreateShipmentIp.getDocumentElement();
    YFCElement shipmentLinesEle = shipElem.createChild("ShipmentLines");
    YFCDocument cloneIPDoc = XMLUtil.getDocumentFor(inXml.getDocumentElement().cloneNode(true));
    YFCNodeList<YFCElement> shipmentLineNL = cloneIPDoc.getElementsByTagName("ShipmentLine");
    String updateZeroQtyRO = getProperty("GPS_PROCESS_RO_ZERO_QTY", true);
    statusAndLineCheck(orderLineNL, shipmentLineNL, inXml, updateZeroQtyRO);
    for (YFCElement shipmentLineEle : shipmentLineNL) {
      shipmentLineEle.setAttribute(TelstraConstants.ORDER_HEADER_KEY, strOrderHeaderKey);
      String strPLno = shipmentLineEle.getAttribute(TelstraConstants.PRIME_LINE_NO);
      double dqty = shipmentLineEle.getDoubleAttribute(TelstraConstants.QUANTITY, 0.0);
      if (dqty > 0.0) {
        shipmentLineEle.setAttribute(TelstraConstants.QUANTITY,
            shipmentLineEle.getAttribute(TelstraConstants.QUANTITY));
      } else {
        // String updateZeroQtyRO = getProperty("GPS_PROCESS_RO_ZERO_QTY", true);
        for (YFCElement orderLineElem : orderLineNL) {
          String strLineNo = orderLineElem.getAttribute(TelstraConstants.PRIME_LINE_NO);
          if (strPLno.equalsIgnoreCase(strLineNo)) {
            if (dqty == 0.0 && "Y".equalsIgnoreCase(updateZeroQtyRO)) {
              shipmentLineEle.setAttribute(TelstraConstants.QUANTITY,
                  orderLineElem.getAttribute("AvailableQty"));
            } else {
              throw ExceptionUtil.getYFSException("TEL_ERR_1171_016",
                  "Quantity attribute is not passed or is zero in the Carrier Message for SO or TO or RO"
                      + inXml, new YFSException());
            }
          }
        }
      }
      // shipmentLineEle.removeAttribute(TelstraConstants.QUANTITY);
      YFCElement shipLineElem = docCreateShipmentIp.importNode(shipmentLineEle, true);
      // shipLineConfirmEle.setAttribute("Action", "BACKORDER");
      shipmentLinesEle.appendChild(shipLineElem);
    }
    return docCreateShipmentIp;


  }

  private YFCDocument createROShipment(YFCDocument inXml) {
    String updateNoLineRO = getProperty("GPS_PROCESS_RO_NO_LINE", true);
    if ("Y".equalsIgnoreCase(updateNoLineRO)) {
      YFCDocument outputGetOrderLineList = getOrderLineList();
      YFCNodeList<YFCElement> orderLineNL =
          outputGetOrderLineList.getElementsByTagName("OrderLine");
      YFCDocument docCreateShipmentIp =
          YFCDocument.getDocumentFor("<Shipment OrderHeaderKey='" + strOrderHeaderKey
              + "' OrderName='" + strOrderName + "' " + "TrackingNo='" + strTrackingNo
              + "' DocumentType='" + strDocType + "' />");
      YFCElement shipConfirmEle = docCreateShipmentIp.getDocumentElement();
      YFCElement shipmentLinesEle = shipConfirmEle.createChild("ShipmentLines");
      statusAndLineCheck(orderLineNL, inXml);
      for (YFCElement orderLineElem : orderLineNL) {
        YFCElement shipmentLineEle = shipmentLinesEle.createChild("ShipmentLine");
        shipmentLineEle.setAttribute(TelstraConstants.PRIME_LINE_NO,
            orderLineElem.getAttribute(TelstraConstants.PRIME_LINE_NO));
        shipmentLineEle.setAttribute(TelstraConstants.RECEIVING_NODE,
            orderLineElem.getAttribute(TelstraConstants.SHIP_NODE));
        shipmentLineEle.setAttribute(TelstraConstants.SUB_LINE_NO,
            orderLineElem.getAttribute(TelstraConstants.SUB_LINE_NO));
        shipmentLineEle.setAttribute(TelstraConstants.QUANTITY,
            orderLineElem.getAttribute("AvailableQty"));
        shipmentLineEle.setAttribute(TelstraConstants.ORDER_HEADER_KEY, strOrderHeaderKey);
        shipConfirmEle.setAttribute(TelstraConstants.RECEIVING_NODE,
            orderLineElem.getAttribute(TelstraConstants.SHIP_NODE));
      }
      return docCreateShipmentIp;
    } else {
      throw ExceptionUtil.getYFSException("TEL_ERR_1171_026",
          "Line details not available in the Carrier Message for RO" + inXml, new YFSException());
    }

  }

  private void statusAndLineCheck(YFCNodeList<YFCElement> orderLineNL, YFCDocument inXml) {
    for (YFCElement orderLineElem : orderLineNL) {
      setAvlQty(orderLineElem, inXml);
    }
  }

  private YFCDocument preparePOASN(YFCDocument inXml) {
    YFCDocument outputGetOrderLineList = getOrderLineList();
    YFCNodeList<YFCElement> orderLineNL = outputGetOrderLineList.getElementsByTagName("OrderLine");
    YFCDocument asnInDoc =
        YFCDocument.getDocumentFor("<Shipment OrderHeaderKey='" + strOrderHeaderKey
            + "' OrderName='" + strOrderName + "' " + "TrackingNo='" + strTrackingNo + "' />");
    YFCElement asnShipEle = asnInDoc.getDocumentElement();
    YFCElement asnLinesEle = asnShipEle.createChild("ShipmentLines");
    YFCDocument cloneIPDoc = XMLUtil.getDocumentFor(inXml.getDocumentElement().cloneNode(true));
    YFCNodeList<YFCElement> shipmentLineNL = cloneIPDoc.getElementsByTagName("ShipmentLine");
    // lineCheck(orderLineNL, shipmentLineNL, inXml);
    for (YFCElement shipmentLineEle : shipmentLineNL) {

      if (YFCObject.isVoid(shipmentLineEle.getAttribute(TelstraConstants.PRIME_LINE_NO))) {
        LoggerUtil.verboseLog("Invalid Input XML", logger, " throwing exception");
        throw ExceptionUtil.getYFSException("TEL_ERR_1171_012",
            "PrimeLineNo not passed in Input XML" + inXml, new YFSException());
      }
      String strPLno = shipmentLineEle.getAttribute(TelstraConstants.PRIME_LINE_NO);
      double dqty = shipmentLineEle.getDoubleAttribute(TelstraConstants.QUANTITY, 0.0);
      shipmentLineEle.setAttribute("OrderHeaderKey", strOrderHeaderKey);
      // add item id attribute or add quantity if qty is zero
      String updateZeroQtyPO = getProperty("GPS_PROCESS_PO_ZERO_QTY", true);
      if (dqty == 0.0) {
        if (dqty == 0.0 && "Y".equalsIgnoreCase(updateZeroQtyPO)) {
          // shipmentLineEle.setAttribute(TelstraConstants.QUANTITY,
          // orderLineElem.getAttribute("OrderedQty"));
          throw ExceptionUtil.getYFSException("TEL_ERR_1171_013",
              "Quantity attribute is not passed or is zero in the Carrier Message for PO" + inXml,
              new YFSException());
        } else {
          throw ExceptionUtil.getYFSException("TEL_ERR_1171_013",
              "Quantity attribute is not passed or is zero in the Carrier Message for PO" + inXml,
              new YFSException());
        }
      }
      for (YFCElement orderLineElem : orderLineNL) {
        String strLineNo = orderLineElem.getAttribute(TelstraConstants.PRIME_LINE_NO);
        if (strPLno.equalsIgnoreCase(strLineNo)) {
          String sItemID =
              orderLineElem.getElementsByTagName("Item").item(0)
                  .getAttribute(TelstraConstants.ITEM_ID);
          shipmentLineEle.setAttribute(TelstraConstants.ITEM_ID, sItemID);
          if (dqty == 0.0) {
            if (dqty == 0.0 && "Y".equalsIgnoreCase(updateZeroQtyPO)) {
              // shipmentLineEle.setAttribute(TelstraConstants.QUANTITY,
              // orderLineElem.getAttribute("OrderedQty"));
              throw ExceptionUtil.getYFSException("TEL_ERR_1171_013",
                  "Quantity attribute is not passed or is zero in the Carrier Message for PO"
                      + inXml, new YFSException());
            } else {
              throw ExceptionUtil.getYFSException("TEL_ERR_1171_013",
                  "Quantity attribute is not passed or is zero in the Carrier Message for PO"
                      + inXml, new YFSException());
            }
          }
        }
      }
      // add the shipment element to the noTrackingNoShipmentsEle
      YFCElement shipLineConfirmEle = asnInDoc.importNode(shipmentLineEle, true);
      asnLinesEle.appendChild(shipLineConfirmEle);
    }
    lineCheck(orderLineNL, shipmentLineNL, inXml);
    return asnInDoc;
  }

  private void lineCheck(YFCNodeList<YFCElement> orderLineNL,
      YFCNodeList<YFCElement> shipmentLineNL, YFCDocument inXml) {
    for (YFCElement shipmentLineEle : shipmentLineNL) {
      boolean isOdrLineAvl = false;
      if (YFCObject.isVoid(shipmentLineEle.getAttribute(TelstraConstants.PRIME_LINE_NO))) {
        LoggerUtil.verboseLog("Invalid Input XML", logger, " throwing exception");
        throw ExceptionUtil.getYFSException("TEL_ERR_1171_015",
            "PrimeLineNo not passed in Input XML" + inXml, new YFSException());
      }
      String strPLno = shipmentLineEle.getAttribute(TelstraConstants.PRIME_LINE_NO);
      for (YFCElement orderLineElem : orderLineNL) {
        String strLineNo = orderLineElem.getAttribute(TelstraConstants.PRIME_LINE_NO);
        if (strPLno.equalsIgnoreCase(strLineNo)) {
          isOdrLineAvl = true;
          break;
        }
      }
      if (!isOdrLineAvl) {

        throw ExceptionUtil.getYFSException("TEL_ERR_1171_023",
            "Order does not have the PrimeLineNo:" + strPLno + "  passed in Input XML" + inXml,
            new YFSException());
      }
    }

  }

  /**
   * The preparePOASN(YFCDocument inXml, String strOrderNo2) Old method can be used if for 0 Qty
   * workaround
   */
  private YFCDocument preparePOASN(YFCDocument inXml, String strOrderNo2) {
    YFCDocument outputGetOrderLineList = getOrderLineList();
    YFCNodeList<YFCElement> orderLineNL = outputGetOrderLineList.getElementsByTagName("OrderLine");
    YFCDocument asnInDoc =
        YFCDocument.getDocumentFor("<Shipment OrderHeaderKey='" + strOrderHeaderKey
            + "' OrderName='" + strOrderName + "' " + "TrackingNo='" + strTrackingNo + "' />");
    YFCElement asnShipEle = asnInDoc.getDocumentElement();
    YFCElement asnLinesEle = asnShipEle.createChild("ShipmentLines");
    YFCDocument cloneIPDoc = XMLUtil.getDocumentFor(inXml.getDocumentElement().cloneNode(true));
    YFCNodeList<YFCElement> shipmentLineNL = cloneIPDoc.getElementsByTagName("ShipmentLine");
    for (YFCElement shipmentLineEle : shipmentLineNL) {

      if (YFCObject.isVoid(shipmentLineEle.getAttribute(TelstraConstants.PRIME_LINE_NO))) {
        LoggerUtil.verboseLog("Invalid Input XML", logger, " throwing exception");
        throw ExceptionUtil.getYFSException("TEL_ERR_1171_012",
            "PrimeLineNo not passed in Input XML" + inXml, new YFSException());
      }
      String strPLno = shipmentLineEle.getAttribute(TelstraConstants.PRIME_LINE_NO);
      double dqty = shipmentLineEle.getDoubleAttribute(TelstraConstants.QUANTITY, 0.0);
      shipmentLineEle.setAttribute("OrderHeaderKey", strOrderHeaderKey);
      // add item id attribute or add quantity if qty is zero
      for (YFCElement orderLineElem : orderLineNL) {
        String strLineNo = orderLineElem.getAttribute(TelstraConstants.PRIME_LINE_NO);
        if (strPLno.equalsIgnoreCase(strLineNo)) {
          String sItemID =
              orderLineElem.getElementsByTagName("Item").item(0)
                  .getAttribute(TelstraConstants.ITEM_ID);
          shipmentLineEle.setAttribute(TelstraConstants.ITEM_ID, sItemID);
          if (!(dqty > 0.0)) {
            if (dqty == 0.0 && isUpdtZeroQtyPO) {
              double dOpenQty = orderLineElem.getDoubleAttribute("OpenQty");
              double dOrderedQty = orderLineElem.getDoubleAttribute("OrderedQty");
              if (dOpenQty > 0.0) {
                shipmentLineEle.setAttribute(TelstraConstants.QUANTITY,
                    orderLineElem.getAttribute("OpenQty"));
              } else {
                throw ExceptionUtil.getYFSException("TEL_ERR_1171_022",
                    "There are no pending quantity that needs to be shipped for this PO PrimeLineNo:"
                        + strLineNo + " OpenQty:" + dOpenQty + " OrderedQty:" + dOrderedQty
                        + " InPutXML: " + inXml, new YFSException());
              }
            } else {
              isUpdtZeroQtyPO = false;
              shipmentLineEle.setAttribute(TelstraConstants.QUANTITY, "1");
              YFCElement personInfoElem = orderLineElem.getChildElement("PersonInfoShipTo");
              YFCElement addressElem = asnShipEle.getChildElement("PersonInfoShipTo");
              if (addressElem == null) {
                asnShipEle.importNode(personInfoElem);
              }
              /*
               * throw ExceptionUtil.getYFSException( "TEL_ERR_1171_013",
               * "Quantity passed as zero for in the Carrier Message for PO " + inXml, new
               * YFSException());
               */
            }
          }
        }
      }
      // add the shipment element to the noTrackingNoShipmentsEle
      YFCElement shipLineConfirmEle = asnInDoc.importNode(shipmentLineEle, true);
      asnLinesEle.appendChild(shipLineConfirmEle);
    }

    return asnInDoc;
  }

  private YFCDocument getOrderLineList() {
    YFCDocument inDocgetOrdLineList =
        YFCDocument.getDocumentFor("<OrderLine><Order OrderNo='" + strOrderNo + "'/></OrderLine>");
    YFCDocument templateForGetOrdLineList =
        YFCDocument
            .getDocumentFor("<OrderLineList TotalLineList=''>"
                + "<OrderLine PrimeLineNo='' OrderedQty='' OpenQty='' ShipNode='' SubLineNo='' > <Item ItemID='' UnitOfMeasure='' />"
                + "<OrderStatuses> <OrderStatus Status='' StatusQty='' /> </OrderStatuses>"
                + "<PersonInfoShipTo AddressLine1='' AddressLine2='' AddressLine3='' AddressLine4='' AddressLine5='' AddressLine6='' City='' Company='' Country='' EMailID='' State='' ZipCode='' DayPhone='' LastName='' FirstName='' />"
                + " </OrderLine>" + "</OrderLineList>");
    YFCDocument outputGetOrderLineList =
        invokeYantraApi("getOrderLineList", inDocgetOrdLineList, templateForGetOrdLineList);
    return outputGetOrderLineList;
  }

  private void findShipAgain(YFCDocument inXml) {

    if (isIntegral) {
      YFCDocument shipListIntegDoc = getShipmentList(inXml);
      double dNoOfShipment =
          shipListIntegDoc.getDocumentElement().getDoubleAttribute("TotalNumberOfRecords");
      if (dNoOfShipment > 0.0) {
        processShipmentPresent(shipListIntegDoc, inXml);
      } else {
        // will not reach here
        throw ExceptionUtil.getYFSException("TEL_ERR_1171_014",
            "Shipment Not Present Scenario - get Shipment failed after creating a new shipment"
                + inXml, new YFSException());
      }
    } else if (isVector) {
      YFCDocument shipListVecDoc = getVectorShipmentList(inXml);
      double dNoOfShipment =
          shipListVecDoc.getDocumentElement().getDoubleAttribute("TotalNumberOfRecords");
      if (dNoOfShipment > 0.0) {
        processVectorShipmentPresent(shipListVecDoc, inXml);
      } else {
        // will not reach here
        throw ExceptionUtil.getYFSException("TEL_ERR_1171_014",
            "Shipment Not Present Scenario - get Shipment failed after creating a new shipment"
                + inXml, new YFSException());
      }
    }
  }

  /**
   * This method prepares the input to ship confirm logic by removing the Extn element and adding
   * Action="BACKORDER" for all the lines
   * 
   * @param inXml
   * @return
   */
  private YFCDocument prepareInputForShipConfirm(YFCDocument inXml, String strTrackingNo) {
    YFCDocument outputGetOrderLineList = getOrderLineList();
    YFCNodeList<YFCElement> orderLineNL = outputGetOrderLineList.getElementsByTagName("OrderLine");
    YFCDocument shipConfirmInDoc =
        YFCDocument.getDocumentFor("<Shipment OrderName='" + strOrderName + "' " + "TrackingNo='"
            + strTrackingNo + "' />");
    YFCElement shipConfirmEle = shipConfirmInDoc.getDocumentElement();
    YFCElement shipmentLinesEle = shipConfirmEle.createChild("ShipmentLines");
    YFCDocument cloneIPDoc = XMLUtil.getDocumentFor(inXml.getDocumentElement().cloneNode(true));
    YFCNodeList<YFCElement> shipmentLineNL = cloneIPDoc.getElementsByTagName("ShipmentLine");
    String updateZeroQtyPO = getProperty("GPS_PROCESS_SOTO_ZERO_QTY", true);
    statusAndLineCheck(orderLineNL, shipmentLineNL, inXml, updateZeroQtyPO);
    for (YFCElement shipmentLineEle : shipmentLineNL) {
      String strPLno = shipmentLineEle.getAttribute(TelstraConstants.PRIME_LINE_NO);
      double dqty = shipmentLineEle.getDoubleAttribute(TelstraConstants.QUANTITY, 0.0);
      if (dqty > 0.0) {
        shipmentLineEle.setAttribute(TelstraConstants.STATUS_QUANTITY,
            shipmentLineEle.getAttribute(TelstraConstants.QUANTITY));
      } else {
        // String updateZeroQtyPO = getProperty("GPS_PROCESS_SOTO_ZERO_QTY", true);
        for (YFCElement orderLineElem : orderLineNL) {
          String strLineNo = orderLineElem.getAttribute(TelstraConstants.PRIME_LINE_NO);
          if (strPLno.equalsIgnoreCase(strLineNo)) {
            if (dqty == 0.0 && "Y".equalsIgnoreCase(updateZeroQtyPO)) {
              shipmentLineEle.setAttribute(TelstraConstants.STATUS_QUANTITY,
                  orderLineElem.getAttribute("AvailableQty"));
            } else {
              throw ExceptionUtil.getYFSException("TEL_ERR_1171_016",
                  "Quantity attribute is not passed or is zero in the Carrier Message for SO or TO or RO"
                      + inXml, new YFSException());
            }
          }
        }
      }
      shipmentLineEle.removeAttribute(TelstraConstants.QUANTITY);
      YFCElement shipLineConfirmEle = shipConfirmInDoc.importNode(shipmentLineEle, true);
      shipLineConfirmEle.setAttribute("Action", "BACKORDER");
      shipmentLinesEle.appendChild(shipLineConfirmEle);
    }
    return shipConfirmInDoc;
  }

  private void statusAndLineCheck(YFCNodeList<YFCElement> orderLineNL,
      YFCNodeList<YFCElement> shipmentLineNL, YFCDocument inXml, String updateZeroQty) {
    for (YFCElement shipmentLineEle : shipmentLineNL) {
      boolean isOdrLineAvl = false;
      if (YFCObject.isVoid(shipmentLineEle.getAttribute(TelstraConstants.PRIME_LINE_NO))) {
        LoggerUtil.verboseLog("Invalid Input XML", logger, " throwing exception");
        throw ExceptionUtil.getYFSException("TEL_ERR_1171_015",
            "PrimeLineNo not passed in Input XML" + inXml, new YFSException());
      }
      String strPLno = shipmentLineEle.getAttribute(TelstraConstants.PRIME_LINE_NO);
      double dIPQty = shipmentLineEle.getDoubleAttribute(TelstraConstants.QUANTITY);
      for (YFCElement orderLineElem : orderLineNL) {
        String strLineNo = orderLineElem.getAttribute(TelstraConstants.PRIME_LINE_NO);
        if (strPLno.equalsIgnoreCase(strLineNo)) {
          if (dIPQty == 0.0 && "Y".equalsIgnoreCase(updateZeroQty)) {
            setAvlQty(orderLineElem, inXml);
          } else if (dIPQty == 0.0) {
            throw ExceptionUtil.getYFSException("TEL_ERR_1171_016",
                "Quantity attribute is not passed or is zero in the Carrier Message for SO or TO or RO"
                    + inXml, new YFSException());
          }
          isOdrLineAvl = true;
          break;
        }
      }
      if (!isOdrLineAvl) {
        if ((dIPQty == 0.0)) {
          throw ExceptionUtil.getYFSException("TEL_ERR_1171_016",
              "Quantity attribute is not passed or is zero in the Carrier Message for SO or TO or RO"
                  + inXml, new YFSException());
        }
        throw ExceptionUtil.getYFSException("TEL_ERR_1171_023",
            "Order does not have the PrimeLineNo:" + strPLno + "  passed in Input XML" + inXml,
            new YFSException());
      }
    }
  }

  private void setAvlQty(YFCElement orderLineElem, YFCDocument inXml) {
    double dAvlQty = 0.0;
    String strPLno = orderLineElem.getAttribute(TelstraConstants.PRIME_LINE_NO);
    YFCNodeList<YFCElement> orderStatusNL = orderLineElem.getElementsByTagName("OrderStatus");
    for (YFCElement orderStatusElem : orderStatusNL) {
      double dStatusQty = orderStatusElem.getDoubleAttribute("StatusQty");
      String strStatus = orderStatusElem.getAttribute("Status");
      if (statusSet.contains(strStatus)) {
        dAvlQty = dAvlQty + dStatusQty;
      }
    }
    if (dAvlQty == 0.0) {
      throw ExceptionUtil.getYFSException("TEL_ERR_1171_024",
          "Order does not have the enough Available quantity for the PrimeLineNo:" + strPLno
              + "  passed in Input XML" + inXml, new YFSException());
    }
    orderLineElem.setDoubleAttribute("AvailableQty", dAvlQty);
  }

  private void processShipmentPresent(YFCDocument shipListDoc, YFCDocument inXml) {
    boolean isTrackingMactchFound = false;
    int intShipCounter = 0;
    YFCElement shipElem =
        shipListDoc.getDocumentElement().getElementsByTagName(TelstraConstants.SHIPMENT).item(0);
    YFCElement shipLineOdrElem = shipElem.getElementsByTagName("Order").item(0);
    String shipOdrName = shipLineOdrElem.getAttribute("OrderName");
    YFCNodeList<YFCElement> yfcNlshipment =
        shipListDoc.getElementsByTagName(TelstraConstants.SHIPMENT);
    intShipCounter = yfcNlshipment.getLength();
    if (isShipNoPassed) {
      updateCarrierStatus(inXml, shipElem);
    } else if (isTrackAvl) {
      for (YFCElement shipmentEle : yfcNlshipment) {
        String strShipTrackNo = shipmentEle.getAttribute(TelstraConstants.TRACKING_NO);
        if (strTrackingNo.equalsIgnoreCase(strShipTrackNo)) {
          isTrackingMactchFound = true;
          updateCarrierStatus(inXml, shipmentEle);
        }
      }
      if (!isTrackingMactchFound) {
        if (!isQtyAvl) {
          if (intShipCounter == 1) {
            String strShipTrackNo = shipElem.getAttribute(TelstraConstants.TRACKING_NO);
            if ((SCUtil.isVoid(strShipTrackNo)) && strOrderName.equalsIgnoreCase(shipOdrName)) {
              updateCarrierStatus(inXml, shipElem);
            } else {
              throw ExceptionUtil.getYFSException("TEL_ERR_1171_017",
                  "Cannot find a matching shipment available in the System" + inXml,
                  new YFSException());
            }
          } else {
            throw ExceptionUtil.getYFSException("TEL_ERR_1171_018",
                "Cannot update since qty not avl and more than one shipment without trackingNo found"
                    + inXml, new YFSException());
          }
        } else {
          findMatchAndProcess(yfcNlshipment, inXml);
        }
      }
    } else {
      if (!isQtyAvl) {
        if (intShipCounter == 1) {
          String strShipTrackNo = shipElem.getAttribute(TelstraConstants.TRACKING_NO);
          if ((SCUtil.isVoid(strShipTrackNo)) && strOrderName.equalsIgnoreCase(shipOdrName)) {
            updateCarrierStatus(inXml, shipElem);
          } else {
            throw ExceptionUtil.getYFSException("TEL_ERR_1171_017",
                "Cannot find a matching shipment available in the System" + inXml,
                new YFSException());
          }
        } else {
          throw ExceptionUtil.getYFSException("TEL_ERR_1171_018",
              "Cannot update since qty not avl and more than one shipment without trackingNo found"
                  + inXml, new YFSException());
        }
      } else {
        findMatchAndProcess(yfcNlshipment, inXml);
      }
    }
  }

  private void processVectorShipmentPresent(YFCDocument shipListDoc, YFCDocument inXml) {
    int intShipCounter = 0;
    YFCElement shipElem =
        shipListDoc.getDocumentElement().getElementsByTagName(TelstraConstants.SHIPMENT).item(0);
    YFCElement shipLineOdrElem = shipElem.getElementsByTagName("Order").item(0);
    String shipOdrName = shipLineOdrElem.getAttribute("OrderName");
    YFCNodeList<YFCElement> yfcNlshipment =
        shipListDoc.getElementsByTagName(TelstraConstants.SHIPMENT);
    intShipCounter = yfcNlshipment.getLength();
    if (isShipNoPassed) {
      updateCarrierStatus(inXml, shipElem);
    } else if (isTrackAvl) {
      for (YFCElement shipmentEle : yfcNlshipment) {
        String strShipTrackNo = shipmentEle.getAttribute(TelstraConstants.TRACKING_NO);
        if (strTrackingNo.equalsIgnoreCase(strShipTrackNo)) {
          updateCarrierStatus(inXml, shipmentEle);
        } else {
          if (!SCUtil.isVoid(strShipTrackNo)) {
            isTrackAvl = false;
          }
          updateCarrierStatus(inXml, shipmentEle);
        }
      }
    } else {
      if (!isQtyAvl) {
        if (intShipCounter == 1) {
          String strShipTrackNo = shipElem.getAttribute(TelstraConstants.TRACKING_NO);
          if ((SCUtil.isVoid(strShipTrackNo)) && strOrderName.equalsIgnoreCase(shipOdrName)) {
            updateCarrierStatus(inXml, shipElem);
          } else {
            throw ExceptionUtil.getYFSException("TEL_ERR_1171_017",
                "Cannot find a matching shipment available in the System" + inXml,
                new YFSException());
          }
        } else {
          throw ExceptionUtil.getYFSException("TEL_ERR_1171_018",
              "Cannot update since qty not avl and more than one shipment without trackingNo found"
                  + inXml, new YFSException());
        }
      } else {
        findMatchAndProcess(yfcNlshipment, inXml);
      }
    }
  }

  private void findMatchAndProcess(YFCNodeList<YFCElement> yfcNlshipment, YFCDocument inXml) {

    YFCNodeList<YFCElement> yfcNlIPshipLines =
        inXml.getDocumentElement().getElementsByTagName(TelstraConstants.SHIPMENT_LINE);
    Map<YFCElement, ArrayList<String>> matchMap = new HashMap<YFCElement, ArrayList<String>>();
    ArrayList<String> primeLineList = null;
    ArrayList<String> inputPrmLnLst = new ArrayList<String>();
    // boolean isMatched = true;
    boolean isCompleteMatch = false;
    for (YFCElement shipLineIPElem : yfcNlIPshipLines) {
      String strIPPrimeLineNo = shipLineIPElem.getAttribute("PrimeLineNo");
      inputPrmLnLst.add(strIPPrimeLineNo);
    }
    for (YFCElement shipmentEle : yfcNlshipment) {
      String shipTrackNo = null;
      shipTrackNo = shipmentEle.getAttribute(TelstraConstants.TRACKING_NO);
      int iShipLineCount = 0;
      int iMatchCount = 0;
      if (SCUtil.isVoid(shipTrackNo)) {
        YFCNodeList<YFCElement> yfcNlshipLines =
            shipmentEle.getElementsByTagName(TelstraConstants.SHIPMENT_LINE);
        primeLineList = new ArrayList<String>();
        for (YFCElement shipLineEle : yfcNlshipLines) {
          iShipLineCount++;
          String strPrimeLineNo = shipLineEle.getAttribute("PrimeLineNo");
          Double dQuantity = shipLineEle.getDoubleAttribute("Quantity");

          for (YFCElement shipLineIPElem : yfcNlIPshipLines) {
            String strIPPrimeLineNo = shipLineIPElem.getAttribute("PrimeLineNo");
            Double dIPQuantity = shipLineIPElem.getDoubleAttribute("Quantity");
            // inputPrmLnLst.add(strIPPrimeLineNo);
            if (strPrimeLineNo.equalsIgnoreCase(strIPPrimeLineNo)) {
              if (dIPQuantity == 0.0) {
                primeLineList.add(strPrimeLineNo);
                iMatchCount++;
              } else if (dQuantity.doubleValue() == dIPQuantity.doubleValue()) {
                primeLineList.add(strPrimeLineNo);
                iMatchCount++;
              }
            } else {
              // isMatched = false;
            }
          }
        }
        // if (isMatched) {
        Collections.sort(inputPrmLnLst);
        Collections.sort(primeLineList);
        if (inputPrmLnLst.equals(primeLineList)) {
          updateCarrierStatus(inXml, shipmentEle);
          isCompleteMatch = true;
          break;
        } else {
          if (inputPrmLnLst.containsAll(primeLineList) && iShipLineCount == iMatchCount) {
            matchMap.put(shipmentEle, primeLineList);
          }
        }
        // }
      }
    }
    if (!isCompleteMatch) {
      if (matchMap != null && !matchMap.isEmpty()) {
        processMatch(matchMap, matchMap, inXml, inputPrmLnLst);
      } else {
        // throw ExceptionUtil.getYFSException("No Match Found",
        // "Cannot cannot find a matching shipment to update" + inXml, new YFSException());
        processShipmentNotPresent(inXml);
      }
    }

  }

  // call a seperate method when doing RO
  private void processMatch(Map<YFCElement, ArrayList<String>> matchMap1,
      Map<YFCElement, ArrayList<String>> matchMap2, YFCDocument inXml,
      ArrayList<String> inputPrmLnLst) {
    ArrayList<YFCElement> finalList = new ArrayList<YFCElement>();
    Collections.sort(inputPrmLnLst);
    Map<String, Boolean> carrierMap = new HashMap<String, Boolean>();
    for (String primeLineNo : inputPrmLnLst) {
      carrierMap.put(primeLineNo, false);
    }
    boolean isFinalMatch = false;
    for (YFCElement shipElem1 : matchMap1.keySet()) {
      if (!isFinalMatch) {
        String strShipKey1 = shipElem1.getAttribute("ShipmentKey");
        if (matchMap1.get(shipElem1) != null && !matchMap1.get(shipElem1).isEmpty()) {
          ArrayList<String> primeLineList1 = matchMap1.get(shipElem1);
          for (YFCElement shipElem2 : matchMap2.keySet()) {
            if (!isFinalMatch) {
              String strShipKey2 = shipElem2.getAttribute("ShipmentKey");
              if (!strShipKey1.equalsIgnoreCase(strShipKey2)) {
                ArrayList<String> primeLineList2 = matchMap2.get(shipElem2);
                for (String primeLineNo : primeLineList2) {
                  primeLineList1.add(primeLineNo);
                }
                if (primeLineList1.size() == inputPrmLnLst.size()) {
                  Collections.sort(primeLineList1);
                  if (primeLineList1.equals(inputPrmLnLst)) {
                    finalList.add(shipElem2);
                    finalList.add(shipElem1);
                    isFinalMatch = true;
                    break;
                  }
                } else {
                  for (String primeLineNo : primeLineList2) {
                    primeLineList1.remove(primeLineList1.size() - 1);
                  }
                }
              }
            }
          }
        }
      }
    }
    if (isFinalMatch) {
      for (YFCElement shipElem : finalList) {
        updateCarrierStatus(inXml, shipElem);
      }
    } else {
      if (!isFinalMatch) {
        Set<YFCElement> tempSet = new HashSet<YFCElement>();
        for (YFCElement shipElem1 : matchMap1.keySet()) {
          if (!isFinalMatch) {
            String strShipKey1 = shipElem1.getAttribute("ShipmentKey");
            tempSet.add(shipElem1);
            if (matchMap1.get(shipElem1) != null && !matchMap1.get(shipElem1).isEmpty()) {
              ArrayList<String> primeLineList1 = matchMap1.get(shipElem1);
              for (YFCElement shipElem2 : matchMap2.keySet()) {
                if (!isFinalMatch) {
                  String strShipKey2 = shipElem2.getAttribute("ShipmentKey");
                  if (!strShipKey1.equalsIgnoreCase(strShipKey2)) {
                    ArrayList<String> primeLineList2 = matchMap2.get(shipElem2);
                    for (String primeLineNo : primeLineList2) {
                      if (!primeLineList1.contains(primeLineNo)) {
                        primeLineList1.add(primeLineNo);
                      } else {
                        break;
                      }
                    }
                    if (primeLineList1.size() == inputPrmLnLst.size()) {
                      Collections.sort(primeLineList1);
                      if (primeLineList1.equals(inputPrmLnLst)) {
                        for (YFCElement tempShipElem : tempSet) {
                          finalList.add(tempShipElem);
                        }
                        finalList.add(shipElem2);
                        isFinalMatch = true;
                        break;
                      } else {

                      }
                    } else if (primeLineList1.size() <= inputPrmLnLst.size()) {
                      if (!tempSet.contains(shipElem1)) {
                        tempSet.add(shipElem1);
                      }
                      if (!tempSet.contains(shipElem2)) {
                        tempSet.add(shipElem2);
                      }
                    }
                  }
                }
              }
            }
          }
          tempSet.clear();// goes into multiple depth // use break; // for single depth
        }
      }
      if (isFinalMatch) {
        for (YFCElement shipElem : finalList) {
          updateCarrierStatus(inXml, shipElem);
        }
      } else {
        // throw ExceptionUtil.getYFSException("No Match Found",
        // "Cannot cannot find a matching shipment to update" + inXml, new YFSException());
        processShipmentNotPresent(inXml);
      }
    }
  }

  private YFCDocument getShipmentList(YFCDocument inXml) {

    YFCDocument getShipListIPDoc =
        YFCDocument.getDocumentFor("<Shipment> <ShipmentLines> <ShipmentLine> <Order OrderNo='"
            + strOrderNo + "' OrderName='" + strOrderName
            + "'/> </ShipmentLine> </ShipmentLines> </Shipment>");
    YFCElement shipElem = getShipListIPDoc.getDocumentElement();
    if (isShipNoPassed) {
      shipElem.setAttribute("ShipmentNo", strShipNo);
    }
    YFCDocument getShipListTempDoc =
        YFCDocument
            .getDocumentFor("<Shipments TotalNumberOfRecords=''>"
                + "<Shipment DocumentType='' ShipmentNo='' ShipmentKey='' Status='' TrackingNo='' ReceivingNode='' ><ShipmentLines><ShipmentLine PrimeLineNo='' OrderNo='' Quantity='' ><Order OrderName=''/></ShipmentLine> </ShipmentLines></Shipment></Shipments>");
    YFCDocument getShipListOPDoc =
        invokeYantraApi(TelstraConstants.GET_SHIPMENT_LIST_API, getShipListIPDoc,
            getShipListTempDoc);
    return getShipListOPDoc;
  }

  /**
   * This method will validate the order details
   * 
   * @param inXml
   * @return
   */

  private void validateInputOrder(YFCDocument inXml) {
    String strOrderName = inXml.getDocumentElement().getAttribute("OrderName");
    if (!SCUtil.isVoid(strOrderName)) {
      isVectorOrder(inXml, strOrderName);
      if (!isVector) {
        isIntegralOrder(inXml, strOrderName);
        if (!isIntegral) {
          // HUB-9416
          isStockMovement(inXml, strOrderName);
          if (!isStockMovement) {
            isAdhocShipment(inXml, strOrderName);
          }
        }
      }
    } else {
      LoggerUtil.verboseLog("OrderName not present in the input", logger, " throwing exception");
      // String strErrorCode = getProperty("BlankOrderName", true);
      // throw ExceptionUtil.getYFSException(strErrorCode, new YFSException());
      throw ExceptionUtil.getYFSException("TEL_ERR_1171_008", "OrderName not present in the input"
          + inXml, new YFSException());
    }
  }

  /**
   * 
   * @param inXml
   * @param sOrderName
   */
  private void isStockMovement(YFCDocument inXml, String sOrderName) {

    YFCDocument getOrdListIPDoc =
        YFCDocument.getDocumentFor("<Order EntryType='STERLING' OrderName='" + sOrderName + "'/>");
    YFCDocument getOrdListTempDoc =
        YFCDocument
            .getDocumentFor("<OrderList  TotalOrderList=''> <Order OrderHeaderKey='' OrderNo='' OrderName='' DocumentType='' > </Order> </OrderList> ");
    YFCDocument getOrdListOPDoc =
        invokeYantraApi("getOrderList", getOrdListIPDoc, getOrdListTempDoc);
    if (!SCUtil.isVoid(getOrdListOPDoc)
        && getOrdListOPDoc.getDocumentElement().getDoubleAttribute("TotalOrderList") > 0) {
      isStockMovement = true;
      if (getOrdListOPDoc.getDocumentElement().getDoubleAttribute("TotalOrderList") > 1) {
        throw ExceptionUtil.getYFSException("TEL_ERR_1171_021",
            "Multiple Orders found with the same Order Name for Stock movement  Order" + inXml,
            new YFSException());
      }
      YFCElement orderListElem = getOrdListOPDoc.getDocumentElement();
      YFCElement orderElem = orderListElem.getElementsByTagName("Order").item(0);
      strOrderNo = orderElem.getAttribute("OrderNo");
      strDocType = orderElem.getAttribute("DocumentType");
      strOrderName = orderElem.getAttribute("OrderName");
      strOrderHeaderKey = orderElem.getAttribute("OrderHeaderKey");
    }

  }

  private void isAdhocShipment(YFCDocument inXml, String strOrderName) {

    YFCDocument getShipListIPDoc =
        YFCDocument
            .getDocumentFor("<Shipment ShipmentNo= '"
                + strOrderName
                + "'><ShipmentLines><ShipmentLine><Order OrderType='ADHOC_LOGISTICS'/></ShipmentLine></ShipmentLines></Shipment>");
    YFCElement ipShipElem = getShipListIPDoc.getDocumentElement();
    if (isShipNoPassed) {
      ipShipElem.setAttribute("ShipmentNo", strShipNo);
    }
    YFCDocument getShipListTempDoc =
        YFCDocument
            .getDocumentFor("<Shipments TotalNumberOfRecords = ''><Shipment DocumentType='' ShipmentNo='' ShipmentKey='' Status='' TrackingNo='' > <ShipmentLines> <ShipmentLine> <Order/> </ShipmentLine> </ShipmentLines> </Shipment> </Shipments>");
    YFCDocument getShipListOPDoc =
        invokeYantraApi(TelstraConstants.GET_SHIPMENT_LIST_API, getShipListIPDoc,
            getShipListTempDoc);

    if (!SCUtil.isVoid(getShipListOPDoc)
        && getShipListOPDoc.getDocumentElement().getDoubleAttribute("TotalNumberOfRecords") > 0) {
      isAdhoc = true;
      if (getShipListOPDoc.getDocumentElement().getDoubleAttribute("TotalNumberOfRecords") > 1) {
        throw ExceptionUtil.getYFSException("TEL_ERR_1171_019",
            "Multiple Shipment with same Shipment No" + inXml, new YFSException());
      }
      YFCElement shipElem =
          getShipListOPDoc.getDocumentElement().getElementsByTagName("Shipment").item(0);
      // strShipNo = shipElem.getAttribute("ShipmentNo");
      // strShipKey = shipElem.getAttribute("ShipmentKey");
      strDocType = shipElem.getAttribute("DocumentType");
      adhocShipListDoc = getShipListOPDoc;
    } else {
      throw ExceptionUtil.getYFSException("TEL_ERR_1171_020",
          "There is no Order or Shipment available in the System for the passed Carrier Message"
              + inXml, new YFSException());
    }

  }

  private void isIntegralOrder(YFCDocument inXml, String strIPOrderName) {
    YFCDocument getOrdListIPDoc =
        YFCDocument.getDocumentFor("<Order EntryType='INTEGRAL_PLUS' OrderName='" + strIPOrderName
            + "'/>");
    YFCDocument getOrdListTempDoc =
        YFCDocument
            .getDocumentFor("<OrderList  TotalOrderList=''> <Order OrderHeaderKey='' OrderNo='' OrderName='' DocumentType='' > </Order> </OrderList> ");
    YFCDocument getOrdListOPDoc =
        invokeYantraApi("getOrderList", getOrdListIPDoc, getOrdListTempDoc);
    if (!SCUtil.isVoid(getOrdListOPDoc)
        && getOrdListOPDoc.getDocumentElement().getDoubleAttribute("TotalOrderList") > 0) {
      isIntegral = true;
      if (getOrdListOPDoc.getDocumentElement().getDoubleAttribute("TotalOrderList") > 1) {
        throw ExceptionUtil.getYFSException("TEL_ERR_1171_021",
            "Multiple Orders found with the same Order Name for Integral Order" + inXml,
            new YFSException());
      }
      YFCElement orderListElem = getOrdListOPDoc.getDocumentElement();
      YFCElement orderElem = orderListElem.getElementsByTagName("Order").item(0);
      strOrderNo = orderElem.getAttribute("OrderNo");
      strDocType = orderElem.getAttribute("DocumentType");
      strOrderName = orderElem.getAttribute("OrderName");
      strOrderHeaderKey = orderElem.getAttribute("OrderHeaderKey");
    }

  }

  private void isVectorOrder(YFCDocument inXml, String strIPOrderName) {
    isVectorIDPassed(inXml, strIPOrderName);
    if (!isVector) {
      isVecOdrNamePassed(inXml, strIPOrderName);
    }
  }

  private void isVecOdrNamePassed(YFCDocument inXml, String strIPOrderName) {
    YFCDocument inDocgetOrdLineList =
        YFCDocument.getDocumentFor("<OrderLine><Order EntryType='VECTOR' OrderName='"
            + strIPOrderName + "' /></OrderLine>");
    YFCDocument templateForGetOrdLineList =
        YFCDocument
            .getDocumentFor("<OrderLineList TotalLineList=''>"
                + "<OrderLine PrimeLineNo=''><Order OrderName='' OrderNo='' DocumentType='' OrderHeaderKey='' /></OrderLine>"
                + "</OrderLineList>");
    YFCDocument outputGetOrderLineList =
        invokeYantraApi("getOrderLineList", inDocgetOrdLineList, templateForGetOrdLineList);
    if (!YFCObject.isVoid(outputGetOrderLineList)
        && outputGetOrderLineList.getDocumentElement().getDoubleAttribute("TotalLineList") > 0) {
      isVector = true;
      if (outputGetOrderLineList.getDocumentElement().getDoubleAttribute("TotalLineList") > 1) {

        throw ExceptionUtil.getYFSException("TEL_ERR_1171_027",
            "Multiple Order Lines found with Same Vector Source ID.", new YFSException());
      }
      String primeLineNo =
          outputGetOrderLineList.getElementsByTagName("OrderLine").item(0)
              .getAttribute("PrimeLineNo");
      // strVectorId = strIPOrderName;
      strOrderNo =
          outputGetOrderLineList.getElementsByTagName("Order").item(0).getAttribute("OrderNo");
      strDocType =
          outputGetOrderLineList.getElementsByTagName("Order").item(0).getAttribute("DocumentType");

      strOrderName =
          outputGetOrderLineList.getElementsByTagName("Order").item(0).getAttribute("OrderName");
      inXml.getDocumentElement().setAttribute("OrderName", strOrderName);
      strOrderHeaderKey =
          outputGetOrderLineList.getElementsByTagName("Order").item(0)
              .getAttribute("OrderHeaderKey");
      strVectorPrimeLineNo = primeLineNo;
      if (isPrimeAvl) {
        inXml.getElementsByTagName("ShipmentLine").item(0).setAttribute("PrimeLineNo", primeLineNo);
      } else {
        updatePrimeLineNo(inXml, primeLineNo);
        isPrimeAvl = true;
      }
    }

  }

  private void isVectorIDPassed(YFCDocument inXml, String strIPOrderName) {
    YFCDocument inDocgetOrdLineList =
        YFCDocument.getDocumentFor("<OrderLine><Order EntryType='VECTOR' /><Extn VectorID='"
            + strIPOrderName + "' /></OrderLine>");
    YFCDocument templateForGetOrdLineList =
        YFCDocument
            .getDocumentFor("<OrderLineList TotalLineList=''>"
                + "<OrderLine PrimeLineNo=''><Order OrderName='' OrderNo='' DocumentType='' OrderHeaderKey='' /></OrderLine>"
                + "</OrderLineList>");
    YFCDocument outputGetOrderLineList =
        invokeYantraApi("getOrderLineList", inDocgetOrdLineList, templateForGetOrdLineList);
    if (!YFCObject.isVoid(outputGetOrderLineList)
        && outputGetOrderLineList.getDocumentElement().getDoubleAttribute("TotalLineList") > 0) {
      isVector = true;
      if (outputGetOrderLineList.getDocumentElement().getDoubleAttribute("TotalLineList") > 1) {
        throw ExceptionUtil.getYFSException(
            TelstraErrorCodeConstants.SHIP_ORDER_MULTIPLE_ORDER_LINES_WITH_SAME_VECTOR_ID,
            new YFSException());
      }
      String primeLineNo =
          outputGetOrderLineList.getElementsByTagName("OrderLine").item(0)
              .getAttribute("PrimeLineNo");
      strVectorId = strIPOrderName;
      strOrderNo =
          outputGetOrderLineList.getElementsByTagName("Order").item(0).getAttribute("OrderNo");
      strDocType =
          outputGetOrderLineList.getElementsByTagName("Order").item(0).getAttribute("DocumentType");

      strOrderName =
          outputGetOrderLineList.getElementsByTagName("Order").item(0).getAttribute("OrderName");
      inXml.getDocumentElement().setAttribute("OrderName", strOrderName);

      strOrderHeaderKey =
          outputGetOrderLineList.getElementsByTagName("Order").item(0)
              .getAttribute("OrderHeaderKey");
      strVectorPrimeLineNo = primeLineNo;
      if (isPrimeAvl) {
        inXml.getElementsByTagName("ShipmentLine").item(0).setAttribute("PrimeLineNo", primeLineNo);
      } else {
        updatePrimeLineNo(inXml, primeLineNo);
        isPrimeAvl = true;
      }
    }
  }


  private void updatePrimeLineNo(YFCDocument inXml, String primeLineNo) {
    YFCElement rootElem = inXml.getDocumentElement();
    YFCElement shipLinesElem = rootElem.createChild("ShipmentLines");
    YFCElement shipLineElem = shipLinesElem.createChild("ShipmentLine");
    shipLineElem.setAttribute(TelstraConstants.PRIME_LINE_NO, primeLineNo);
    shipLineElem.setAttribute(TelstraConstants.SUB_LINE_NO, "1");
    shipLineElem.setAttribute(TelstraConstants.QUANTITY, "0");
  }

  /**
   * This method will validate of the carrier status received is correct or not. If it is not
   * correct, an exception will be thrown before any other api is called. If it is valid, it will
   * return true if the carrier update has shipment lines. Else, it will return false. Based on this
   * flag (if true), and if the document type is sales order or transfer order, the ship confirm
   * logic will be called.
   * 
   * @param inXml
   * @return
   */
  private void validateInputTracking(YFCDocument inXml) {
    String strStatusList = getProperty("STATUS_LIST", true);
    String[] statusAry = strStatusList.split("\\|");
    statusSet = new HashSet<String>();
    for (String strStatus : statusAry) {
      statusSet.add(strStatus);
    }
    String updateZeroQtyPO = getProperty("GPS_PROCESS_PO_ZERO_QTY", true);
    if ("Y".equalsIgnoreCase(updateZeroQtyPO)) {
      isUpdtZeroQtyPO = true;
    }
    String carrierInTransit = getProperty("INTRANSIT_STATUS", true);
    String carrierDelivered = getProperty("DELIVERED_STATUS", true);
    String carrierFailed = getProperty("FAILED_STATUS", true);
    String carrierConReady = getProperty("CON_READY_STATUS", true);
    String carrierServiceComp = getProperty("SER_COMP_STATUS", true);
    String carrierNotification = getProperty("NOTIFY_STATUS", true);
    // Notification

    strTransportStatus = XPathUtil.getXpathAttribute(inXml, "//CarrierUpdate/@TransportStatus");
    strTransportText = XPathUtil.getXpathAttribute(inXml, "//CarrierUpdate/@TransportStatusTxt");

    // throw an exception if invalid transport status is passed
    if (!(carrierInTransit.equals(strTransportStatus) || carrierFailed.equals(strTransportStatus)
        || carrierDelivered.equals(strTransportStatus)
        || carrierConReady.equals(strTransportStatus)
        || carrierServiceComp.equals(strTransportStatus) || carrierNotification
          .equals(strTransportStatus))) {
      LoggerUtil.verboseLog("Shipment Status was not Found", logger, " throwing exception");
      String strErrorCode = getProperty("InvalidStatusFailureCode", true);
      throw ExceptionUtil.getYFSException(strErrorCode, new YFSException());
    }
    if (carrierNotification.equals(strTransportStatus)) {
      isNotificationOnly = true;
    }
    YFCElement shipElem = inXml.getDocumentElement();
    strTrackingNo = shipElem.getAttribute("TrackingNo", "");
    strOrderName = shipElem.getAttribute("OrderName", "");
    strShipNo = shipElem.getAttribute("ShipmentNo", "");
    if (!SCUtil.isVoid(strShipNo)) {
      isShipNoPassed = true;
    }
    if (!SCUtil.isVoid(strTrackingNo)) {
      isTrackAvl = true;
    }
    YFCElement firstShipmentLineEle = shipElem.getElementsByTagName("ShipmentLine").item(0);
    if (!SCUtil.isVoid(firstShipmentLineEle)) {
      String strPrimeLineNo = firstShipmentLineEle.getAttribute("PrimeLineNo", "");
      String strQty = firstShipmentLineEle.getAttribute("Quantity", "");
      if (!SCUtil.isVoid(strPrimeLineNo)) {
        isPrimeAvl = true;
      }
      if (!SCUtil.isVoid(strQty)) {
        isQtyAvl = true;
      }
    }
  }
}

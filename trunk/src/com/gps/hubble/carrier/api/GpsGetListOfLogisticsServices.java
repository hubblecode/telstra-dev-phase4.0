/***********************************************************************************************
 * File	Name		: GpsGetListOfLogisticsServices.java
 *
 * Description		: This class will return the list of Logistics service which satisfy the input criteria
 * 
 * Modification	Log	:
 * --------------------------------------------------------------------------------------------------------------------------------------------------------------
 * Ver #		Date				Author					Modification
 * --------------------------------------------------------------------------------------------------------------------------------------------------------------
 * 1.0			Sep 10,2016	  		Prateek Kumar 		   	Initial	Version 
 * ---------------------------------------------------------------------------------------------
 * Copyright (c) IBM Australia Limited 2017. All rights reserved.
 **********************************************************************************************/
package com.gps.hubble.carrier.api;

import java.util.ArrayList;
import java.util.List;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSException;


/**
 * 
 * @author Prateek
 *
 */
public class GpsGetListOfLogisticsServices extends AbstractCustomApi {

	private static YFCLogCategory logger = YFCLogCategory.instance(CarrierUpdate.class);
	private static enum Moves {
		WS, I, A
	}

	@Override
	public YFCDocument invoke(YFCDocument yfcInDoc) throws YFSException {

		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", yfcInDoc);

		YFCElement yfcEleIpRoot = yfcInDoc.getDocumentElement();

		YFCDocument yfcDocGpsGetLogisticsServiceIp = prepareGetLogisticsServiceIp(yfcEleIpRoot);

		LoggerUtil.verboseLog("GpsGetListOfLogisticsServices :: invoke :: yfcDocGpsGetLogisticsServiceIp", logger, yfcDocGpsGetLogisticsServiceIp);
		YFCDocument yfcDocGetLogisticsServiceOp = invokeYantraService(TelstraConstants.SERVICE_GPS_GET_LOGISTICS_SERVICE_LIST, yfcDocGpsGetLogisticsServiceIp);
		LoggerUtil.verboseLog("GpsGetListOfLogisticsServices :: invoke :: yfcDocGetLogisticsServiceOp", logger, yfcDocGetLogisticsServiceOp);

		Moves logisticsMoves =  getMoves(yfcInDoc);

		YFCDocument yfcDocValidLogisticsService = validateLogisticsService (yfcEleIpRoot, yfcDocGetLogisticsServiceOp, logisticsMoves);


		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", yfcEleIpRoot);
		return yfcDocValidLogisticsService;
	}

	/**
	 * 
	 * @param yfcEleIpRoot
	 * @return
	 */
	private Moves getMoves(YFCDocument yfcInDoc) {

		Moves logisticsMoves = Moves.A;
		YFCElement yfcEleFromAddress = XPathUtil.getXPathElement(yfcInDoc, "//AdditionalAddress[@AddressType='SHIP_FROM']/PersonInfo");
		YFCElement yfcEleToAddress = yfcInDoc.getElementsByTagName(TelstraConstants.PERSON_INFO_BILL_TO).item(0);
		if(!YFCCommon.isVoid(yfcEleFromAddress) && !YFCCommon.isVoid(yfcEleToAddress)){

			String sFromCountry = yfcEleFromAddress.getAttribute(TelstraConstants.COUNTRY);
			String sToCountry = yfcEleToAddress.getAttribute(TelstraConstants.COUNTRY);		

			if((!YFCCommon.isStringVoid(sFromCountry) && sFromCountry.equalsIgnoreCase(sToCountry))){
				String sFromState = yfcEleFromAddress.getAttribute(TelstraConstants.STATE);
				String sToState = yfcEleToAddress.getAttribute(TelstraConstants.STATE);		

				if((!YFCCommon.isStringVoid(sFromState) && sFromState.equalsIgnoreCase(sToState))){
					logisticsMoves = Moves.WS;
				}
				else{
					logisticsMoves = Moves.I;
				}	
			}
		}
		return logisticsMoves;
	}

	/**
	 * 
	 * @param inXml
	 * @return
	 */
	private YFCDocument prepareGetLogisticsServiceIp(YFCElement yfcEleIpRoot) {

		YFCDocument yfcDocGetLogisticsServiceIp = YFCDocument.createDocument(TelstraConstants.LOGISTICS_SERVICE);
		YFCElement yfcEleGetLogisticsServiceRoot = yfcDocGetLogisticsServiceIp.getDocumentElement();

		YFCElement yfcEleLogisticsServiceDetail = yfcEleIpRoot.getElementsByTagName(TelstraConstants.LOGISTICS_SERVICE_DETAIL).item(0);
		if(!YFCCommon.isVoid(yfcEleLogisticsServiceDetail)){
			yfcEleGetLogisticsServiceRoot.setAttribute(TelstraConstants.DANGEROUS_GOODS,
					yfcEleLogisticsServiceDetail.getAttribute(TelstraConstants.DANGEROUS_GOODS, ""));
			yfcEleGetLogisticsServiceRoot.setAttribute(TelstraConstants.FOOD_STUFF,
					yfcEleLogisticsServiceDetail.getAttribute(TelstraConstants.FOOD_STUFF, ""));
			yfcEleGetLogisticsServiceRoot.setAttribute(TelstraConstants.FRAGILE,
					yfcEleLogisticsServiceDetail.getAttribute(TelstraConstants.FRAGILE, ""));
			yfcEleGetLogisticsServiceRoot.setAttribute(TelstraConstants.LITHIUM_BATTERIES,
					yfcEleLogisticsServiceDetail.getAttribute(TelstraConstants.LITHIUM_BATTERIES, ""));
			yfcEleGetLogisticsServiceRoot.setAttribute(TelstraConstants.STAR_TRACK_SATCHEL,
					yfcEleLogisticsServiceDetail.getAttribute(TelstraConstants.STAR_TRACK_SATCHEL, ""));
			yfcEleGetLogisticsServiceRoot.setAttribute(TelstraConstants.UNSPECIFIED,
					yfcEleLogisticsServiceDetail.getAttribute(TelstraConstants.UNSPECIFIED, ""));
			yfcEleGetLogisticsServiceRoot.setAttribute(TelstraConstants.PACKS,
					yfcEleLogisticsServiceDetail.getAttribute(TelstraConstants.PACKS, ""));
			yfcEleGetLogisticsServiceRoot.setAttribute(TelstraConstants.RUBBISH_REMOVAL,
					yfcEleLogisticsServiceDetail.getAttribute(TelstraConstants.RUBBISH_REMOVAL, ""));
			yfcEleGetLogisticsServiceRoot.setAttribute(TelstraConstants.SUPPLY_LABELS,
					yfcEleLogisticsServiceDetail.getAttribute(TelstraConstants.SUPPLY_LABELS, ""));
			yfcEleGetLogisticsServiceRoot.setAttribute(TelstraConstants.SUPPLY_PACKAGING,
					yfcEleLogisticsServiceDetail.getAttribute(TelstraConstants.SUPPLY_PACKAGING, ""));
			yfcEleGetLogisticsServiceRoot.setAttribute(TelstraConstants.SUPPLY_STAR_TRACK_SATCHEL,
					yfcEleLogisticsServiceDetail.getAttribute(TelstraConstants.SUPPLY_STAR_TRACK_SATCHEL, ""));
			yfcEleGetLogisticsServiceRoot.setAttribute(TelstraConstants.UNPACK,
					yfcEleLogisticsServiceDetail.getAttribute(TelstraConstants.UNPACK, ""));
			yfcEleGetLogisticsServiceRoot.setAttribute(TelstraConstants.STATUS, TelstraConstants.LOGISTICS_SERVICE_ACTIVE);
		}
		return yfcDocGetLogisticsServiceIp;
	}

	/**
	 * 
	 * @param inXml
	 * @param yfcDocGetLogisticsServiceOp
	 * @param logisticsMoves 
	 * @return
	 */
	private YFCDocument validateLogisticsService(YFCElement yfcEleIpRoot, YFCDocument yfcDocGetLogisticsServiceOp, Moves logisticsMoves) {

		double dTotalContainers = 0.0;
		double dTotalLength = 0.0;
		double dTotalVol = 0.0;
		double dTotalWeight = 0.0;
		for(YFCElement yfcEleContainer : yfcEleIpRoot.getElementsByTagName(TelstraConstants.CONTAINER)){

			dTotalContainers = dTotalContainers + yfcEleContainer.getDoubleAttribute(TelstraConstants.CONTAINER_NO, 0.0);
			dTotalLength = dTotalLength + yfcEleContainer.getDoubleAttribute(TelstraConstants.CONTAINER_LENGTH, 0.0);
			dTotalVol = dTotalVol + yfcEleContainer.getDoubleAttribute(TelstraConstants.CONTAINER_VOLUME, 0.0);
			dTotalWeight = dTotalWeight + yfcEleContainer.getDoubleAttribute(TelstraConstants.CONTAINER_GROSS_WEIGHT, 0.0);
		}

		YFCElement yfcEleLogisticServiceDetail = yfcEleIpRoot.getElementsByTagName(TelstraConstants.LOGISTICS_SERVICE_DETAIL).item(0);

		double dInsuredValueIp = 0.0;

		if(!YFCCommon.isVoid(yfcEleLogisticServiceDetail)){
			dInsuredValueIp = yfcEleLogisticServiceDetail.getDoubleAttribute(TelstraConstants.INSURED_VALUE, 0.0);
		}

		List<YFCElement> lLogisticsServiceDetail = new ArrayList<>();

		for(YFCElement yfcEleLogisticsService : yfcDocGetLogisticsServiceOp.getElementsByTagName(TelstraConstants.LOGISTICS_SERVICE)){

			double dInsuredValue = yfcEleLogisticsService.getDoubleAttribute(TelstraConstants.INSURED_VALUE, 0.0);

			double dMaxContainers = yfcEleLogisticsService.getDoubleAttribute(TelstraConstants.MAX_CONTAINERS, 0.0);
			double dMinWt = yfcEleLogisticsService.getDoubleAttribute(TelstraConstants.MIN_WEIGHT, 0.0);
			double dMaxWt = yfcEleLogisticsService.getDoubleAttribute(TelstraConstants.MAX_WEIGHT, 0.0);
			double dMaxLength = yfcEleLogisticsService.getDoubleAttribute(TelstraConstants.MAX_LENGTH, 0.0);
			double dMaxValue = yfcEleLogisticsService.getDoubleAttribute(TelstraConstants.MAX_VALUE, 0.0);
			String sCarrierMoves = yfcEleLogisticsService.getAttribute(TelstraConstants.MOVES);

			/*checking container level constraint*/
			if ((dMaxContainers == 0 || dTotalContainers <= dMaxContainers)
					&& (dMinWt == 0 || dTotalWeight == 0 || dMinWt >= dTotalWeight)
					&& (dMaxWt == 0 || dTotalWeight == 0 || dTotalWeight <= dMaxWt)
					&& (dTotalLength == 0 || dMaxLength == 0 || dTotalLength <= dMaxLength)
					&& (dTotalVol == 0 || dMaxValue == 0 || dTotalVol <= dMaxValue)
					&& (dInsuredValue == 0 || dInsuredValueIp <= dInsuredValue)) {

				/*checking moves*/
				if(Moves.A.toString().equals(sCarrierMoves)
						|| (Moves.I.toString().equals(sCarrierMoves)
								&& (Moves.I.equals(logisticsMoves) || Moves.WS.equals(logisticsMoves)))
						|| (Moves.WS.toString().equals(sCarrierMoves) && Moves.WS.equals(logisticsMoves))) {

					lLogisticsServiceDetail.add(yfcEleLogisticsService);	
				}				
			}			
		}

		YFCDocument yfcDocLogisticsServiceList = YFCDocument.createDocument(TelstraConstants.LOGISTICS_SERVICE_LIST);
		YFCElement yfcEleRoot = yfcDocLogisticsServiceList.getDocumentElement();

		for(YFCElement yfcEleLogisticService : lLogisticsServiceDetail){
			yfcEleRoot.appendChild(yfcDocLogisticsServiceList.importNode(yfcEleLogisticService, true));
		}

		return yfcDocLogisticsServiceList;
	}


}

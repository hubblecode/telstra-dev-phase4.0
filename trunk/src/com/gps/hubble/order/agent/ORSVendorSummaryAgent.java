package com.gps.hubble.order.agent;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.bridge.sterling.framework.agent.AbstractCustomBaseAgent;
import com.bridge.sterling.utils.LoggerUtil;
import com.yantra.yfc.date.YDate;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCException;

public class ORSVendorSummaryAgent extends AbstractCustomBaseAgent{
	private static YFCLogCategory logger = YFCLogCategory.instance(ORSVendorSummaryAgent.class);
	
	@Override
	public List<YFCDocument> getJobs(YFCDocument msgXml, YFCDocument lastMsgXml) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "getJobs", msgXml);
		boolean getJobsRequired = true;
		int refreshInterval = msgXml.getDocumentElement().getIntAttribute("RefreshInterval",24);
		String enterpriseCode = msgXml.getDocumentElement().getAttribute("EnterpriseCode", "TELSTRA_SCLSA");
		String vendorClassification = msgXml.getDocumentElement().getAttribute("VendorClassification", "VENDOR_PORTAL");
		//We do not want to call getJobs twice in a run
		if(lastMsgXml != null){
			getJobsRequired = false;
		}
		// Check if the summary table was refreshed before the last interval and if so no need to proceed
		if(hasLatestRecords(refreshInterval)){
			getJobsRequired = false;
		}
		
		List<YFCDocument> list = new ArrayList<>();
		if(!getJobsRequired){
			return list;
		}
		refreshTable();
		logger.debug("GPS_ORS_MQT table refreshed");
		//Truncating the table
		deleteOldRecords();
		logger.debug("old records of GPS_ORS_VENDOR_SUMMARY");
		YFCDocument getVendorListOutput = getVendorList(enterpriseCode,vendorClassification);
		for(Iterator<YFCElement> itr = getVendorListOutput.getDocumentElement().getChildren();itr.hasNext();){
			YFCElement elemGetJob = itr.next();
			list.add(YFCDocument.getDocumentFor(elemGetJob.getString()));
		}
        //list.add(getVendorListOutput);
		/*try{*/
			/*String query = createQuery();
			Connection connection = getServiceInvoker().getDBConnection();
			PreparedStatement ps = connection.prepareStatement(query);
			ResultSet rs = ps.executeQuery();
			YFCDocument doc = constructDocument(rs);*/
		/*}catch(SQLException sqlEx){
			logger.error("SQLException while executing query for getJobs", sqlEx);
			throw new YFCException(sqlEx);
		}*/
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "getJobs",list);
		return list;
	}

	private YFCDocument getVendorList(String enterpriseCode, String vendorClassification) {
		String apiInput="<Vendor OrganizationCode='"+enterpriseCode+"' VendorClassificationCode='"+vendorClassification+"'/>";
      YFCDocument getVendorListInput = YFCDocument.getDocumentFor(apiInput);
      return invokeYantraApi("getVendorList", getVendorListInput);
    }

  @Override
	public void executeJob(YFCDocument executeJobXml) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "executeJob", executeJobXml);
		if(executeJobXml == null || executeJobXml.getDocumentElement() == null){
			return;
		}
		//deleteOldRecords();
		YFCElement vendor = executeJobXml.getDocumentElement();
        Connection connection = getServiceInvoker().getDBConnection();
        String sellerOrg= vendor.getAttribute("SellerOrganizationCode");
        String documentType=vendor.getAttribute("DocumentType","0005");
        String processType=vendor.getAttribute("ProcessType","PO_FULFILLMENT");        
        //try with resources auto closes the prepared statement and resultset
		try ( PreparedStatement ps = createQuery(connection,sellerOrg,processType,documentType); ResultSet rs = ps.executeQuery()) {		  
	            	            
	            YFCDocument doc = constructDocument(rs);
	            for(Iterator<YFCElement> itr = doc.getDocumentElement().getChildren("OrsVendorSummary").iterator();itr.hasNext();){
	              YFCElement elemOrsVendorSummary = itr.next();
	              YFCDocument docOrsVendorSummary = YFCDocument.getDocumentFor(elemOrsVendorSummary.getString());
	              invokeYantraService("GpsCreateOrsVendorSummary", docOrsVendorSummary);
	            }
        }catch(SQLException sqlEx){
          logger.error("SQLException while executing query for getJobs", sqlEx);
          throw new YFCException(sqlEx);
        }
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "executeJob",executeJobXml);
	}
	
	
	private PreparedStatement createQuery(Connection connection,String sellerOrganizationCode,String processType,String documentType) throws SQLException{
		String query= "SELECT OH.ORDER_TYPE AS ORDER_TYPE,OH.DOCUMENT_TYPE DOCUMENT_TYPE,OH.SELLER_ORGANIZATION_CODE AS SELLER_ORGANIZATION_CODE, ORS.STATUS AS STATUS, YFS_STATUS.DESCRIPTION AS STATUS_DESCRIPTION,"
				+ " COUNT(DISTINCT ORS.ORDER_HEADER_KEY) AS NO_OF_ORDERS, SUM(OH.TOTAL_AMOUNT) AS TOTAL_AMOUNT 	"
				+ " FROM YFS_ORDER_HEADER OH, GPS_ORS_MQT ORS, YFS_STATUS YFS_STATUS, YFS_PROCESS_TYPE YFS_PROCESS_TYPE "
				+ " WHERE OH.DOCUMENT_TYPE = ? AND OH.HOLD_FLAG <> 'Y' AND ORS.ORDER_HEADER_KEY = OH.ORDER_HEADER_KEY AND ORS.STATUS= YFS_STATUS.STATUS AND YFS_STATUS.PROCESS_TYPE_KEY = ?"
				+ " AND YFS_STATUS.PROCESS_TYPE_KEY=YFS_PROCESS_TYPE.PROCESS_TYPE_KEY "
				+ " AND YFS_PROCESS_TYPE.BASE_PROCESS_TYPE='ORDER_FULFILLMENT' "
				+ " AND YFS_PROCESS_TYPE.DOCUMENT_TYPE=OH.DOCUMENT_TYPE "
				+ " AND OH.SELLER_ORGANIZATION_CODE = ? "
				+ " GROUP BY OH.ORDER_TYPE, OH.DOCUMENT_TYPE, OH.SELLER_ORGANIZATION_CODE, ORS.STATUS, YFS_STATUS.DESCRIPTION ";
		PreparedStatement ps = connection.prepareStatement(query);
		ps.setString(1,documentType);
		ps.setString(2,processType);
		ps.setString(3,sellerOrganizationCode);
		return ps;
		
	}
	
	/*
	 * This method will check whether records inserted in GPR_ORS_VENDOR_SUMMARY is latest, i.e. inserted withing last one hour 
	 */
	private boolean hasLatestRecords(int refreshInterval){
		YFCDocument inDoc = YFCDocument.getDocumentFor("<OrsVendorSummary MaximumRecords='1' />");
		//YFCDocument template = YFCDocument.getDocumentFor("<OrsVendorSummaryList><OrsVendorSummary  /></OrsVendorSummaryList>");
		YFCDocument outDoc = invokeYantraService("GpsGetOrsVendorSummaryList", inDoc);
		if(outDoc == null || outDoc.getDocumentElement() == null || outDoc.getDocumentElement().getChildren() == null){
			return false;
		}
		YFCElement vendorSummary = outDoc.getDocumentElement().getChildElement("OrsVendorSummary");
		if(vendorSummary == null){
			return false;
		}
		YDate createTs = vendorSummary.getYDateAttribute("CreateTS");
		YDate currentTime = new YDate(false);
		long hour = refreshInterval * 60 * 60 * 1000L;
		if(createTs == null || currentTime.getTime() - createTs.getTime() > hour){
			return false;
		}
		return true;
	}
	
	private YFCDocument constructDocument(ResultSet rs) throws SQLException{
		YFCDocument doc = YFCDocument.getDocumentFor("<OrsVendorSummaryList />");
		YFCElement elem = doc.getDocumentElement();
		while(rs.next()){
			YFCElement elemOrsVendorSummary = elem.createChild("OrsVendorSummary");
			String orderType = rs.getString("ORDER_TYPE");
			elemOrsVendorSummary.setAttribute("OrderType", orderType);
			String docType = rs.getString("DOCUMENT_TYPE");
			elemOrsVendorSummary.setAttribute("DocumentType", docType);			
			String sellerOrganizationCode = rs.getString("SELLER_ORGANIZATION_CODE");
			elemOrsVendorSummary.setAttribute("SellerOrganizationCode", sellerOrganizationCode);
			String status = rs.getString("STATUS");
			elemOrsVendorSummary.setAttribute("Status", status);
			String statusDesc = rs.getString("STATUS_DESCRIPTION");
			elemOrsVendorSummary.setAttribute("StatusDescription", statusDesc);
			long noOfOrders = rs.getLong("NO_OF_ORDERS");
			elemOrsVendorSummary.setAttribute("NoOfOrders", noOfOrders);
			Double totalAmount = rs.getDouble("TOTAL_AMOUNT");
			elemOrsVendorSummary.setAttribute("TotalAmount", totalAmount);
		}
		return doc;
	}
	
	private void refreshTable(){
		Connection connection = getServiceInvoker().getDBConnection();
		String refreshQuery = "refresh table GPS_ORS_MQT";
		try (PreparedStatement ps = connection.prepareStatement(refreshQuery)) {
			ps.execute();
		}catch(SQLException sqlEx){
			logger.error("Exception while refreshing GPS_ORS_MQT", sqlEx);
			throw new YFCException(sqlEx);
		}
	}
	
	private void deleteOldRecords(){
		String deleteQuery = "DELETE FROM GPS_ORS_VENDOR_SUMMARY";
		Connection connection = getServiceInvoker().getDBConnection();

		try (PreparedStatement ps = connection.prepareStatement(deleteQuery)) {
			ps.execute();
		}catch(SQLException sqlEx){
			logger.error("Error while deleting old records ",sqlEx);
			throw new YFCException(sqlEx);
		}
	}
}

/***********************************************************************************************
 * File	Name		: BeforeCreateOrderUE.java
 *
 * Description		: This class is called from YFSBeforeCreateOrder UE as a service. 
 * 						It do Cost Center, Zip Code and Item Round Validation. 
 * 						It also updates BillToId if BillToKey is present.
 * 
 * Modification	Log	:
 * ---------------------------------------------------------------------------------------------
 * Ver #	Date				Author					Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0		Jul 04,2016	  		Tushar Dhaka 		   	Initial	Version 
 * 1.1		Aug 02,2016			Prateek Kumar			HUB-3885: copying order name from SO to chained PO
 * 1.2		Jan 06,2017			Santosh Nagaraj			HUB-8088: The requested delivery date stamped for an urgent order is incorrect when a transfer order is created between two nodes situated in two different states
 * 1.3      Feb 22,2017			Keerthi Yadav			HUB-8462: Entry Type STERLING change for order creation in UI
 * 1.4		Mar 24,2017			Prateek Kumar			HUB-8596: Added null check in getTransportMatrix method
 * 1.5 		Mar 24,2017			Prateek Kumar			HUB-8604: Moved zip code and cost center validation outside new order line creation check
 * 1.6		Mar 30,2017			Prateek Kumar			HUB-8610: if interfaceNo is void (which means request is coming from Sterling), do order date computation
 * 1.7		Apr 05,2017			Prateek Kumar			HUB-8525: Fixed null pointer while calling DAC schedule list if DAC is null
 * 1.8		May 17,2017			Prateek Kumar			HUB-9083: Removing receiving node field if any for the sales order in before create/change order ue.
 * 1.9		May 17,2017			Santosh					HUB-9068: Invalid Date Format during load order
 * 2.0		Jun 15,2017			Keerthi Yadav			HUB-9333: Requested Delivery Date before Requested ship Date. Also same as above the date does not match against PMC LRA.
 * ---------------------------------------------------------------------------------------------
 **********************************************************************************************/

package com.gps.hubble.order.ue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.constants.TelstraErrorCodeConstants;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.date.YDate;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCDate;
import com.yantra.yfc.util.YFCDateUtils;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfc.util.YFCLocale;
import com.yantra.yfs.japi.YFSException;


/**
 * Custom call to do order validations
 * 
 * @author Tushar Dhaka
 * @version 1.0
 * 
 * Extends AbstractCustomApi class
 */
public class BeforeCreateOrderUE extends AbstractCustomApi{

	private static YFCLogCategory logger = YFCLogCategory.instance(BeforeCreateOrderUE.class);

	private String departmentCode = "";
	Map<String, String> datesMap = new HashMap<String, String>();

	/*
	 * For validation and order date calcuation
	 * (non-Javadoc)
	 * @see com.bridge.sterling.framework.api.AbstractCustomApi#invoke(com.yantra.yfc.dom.YFCDocument)
	 */
	public YFCDocument invoke(YFCDocument inXml) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(),"invoke", inXml);

		/*
		 * This transaction is set in GpsUpdateOrderExtnFields.java and is getting invoked at the end of MangeOrder code. In this 
		 * code change order api is getting called and this object will prevent the unnecessary execution of this code.		 
		 */
		if(YFCCommon.isVoid(getTxnObject("ORDER_ALREADY_UPDATED"))){
			// HUB-7096: Processing orders having Multiple ShipNodes and multi-DACs with V&C at order line level  - Start
			departmentCode = inXml.getDocumentElement().getAttribute(TelstraConstants.DEPARTMENT_CODE);
			// HUB-7096: Processing orders having Multiple ShipNodes and multi-DACs with V&C at order line level  - End

			//stockMovement [Begin]
			String sOrderType = inXml.getDocumentElement().getAttribute(TelstraConstants.ORDER_TYPE);
			if(TelstraConstants.STOCK_MOVEMENT_UPPER_CASE.equals(sOrderType) || TelstraConstants.ADHOC_LOGISTICS_UPPER_CASE.equals(sOrderType)){
				return inXml;
			}
			//stockMovement [End]


			// update required attributes from SO to chained PO
			if(TelstraConstants.DOCUMENT_TYPE_0005.equalsIgnoreCase(inXml.getDocumentElement().getAttribute(TelstraConstants.DOCUMENT_TYPE))){
				//HUB-4762: Begin
				appendMrpControllerinOrderLine(inXml);
				//HUB-4762: End
				setGPSCommitDateTypeForOrderLines(inXml);

			}
			//get Common Codes associated with Order Validation
			YFCDocument docGetCommonCodeListOutput = invokeYantraApi(TelstraConstants.API_GET_COMMON_CODE_LIST, YFCDocument.getDocumentFor("<CommonCode CodeType=\"ORDER_VALIDATION\"/>"));
			LoggerUtil.verboseLog("BeforeCreateOrderUE :: invoke :: docGetCommonCodeListOutput", logger, docGetCommonCodeListOutput);

			//validate Item Round
			itemRoundValidation(inXml, docGetCommonCodeListOutput);

			YDate dateToWham = OrderDateHelper.getOrderDate(inXml.getDocumentElement(), TelstraConstants.DATE_TO_WHAM);
			// HUB-7096: Processing orders having Multiple ShipNodes and multi-DACs with V&C at order line level  - Start
			//Order date calculations
			if(requireOrderDateCalculations(inXml)){
				docGetCommonCodeListOutput = invokeYantraApi(TelstraConstants.API_GET_COMMON_CODE_LIST, YFCDocument.getDocumentFor("<CommonCode SystemDefinedCode='N' />"));
				LoggerUtil.verboseLog("BeforeCreateOrderUE :: invoke :: docGetCommonCodeListOutput2", logger, docGetCommonCodeListOutput);

				boolean linePriorityPresent = false;
				YFCNodeList<YFCElement> orderLines = inXml.getElementsByTagName(TelstraConstants.ORDER_LINE);
				linePriorityPresent = checkIfLinePriorityPresent(orderLines);
				Priority priority = null;

				for(YFCElement orderLine : orderLines) {
					//[HUB-8604] - Begin
					//validate zip code
					zipCodeValidation(orderLine, docGetCommonCodeListOutput);

					//validate Cost Center
					costCenterValidation(inXml, docGetCommonCodeListOutput, orderLine);
					//[HUB-8604] - End
					/*
					 * Adding interface number check so that date computation works for the new line as well as it is recalculated on the 
					 * ship node and address change
					 */
					String sInterfaceNo = inXml.getDocumentElement().getAttribute(TelstraConstants.INTERFACE_NO);
					if (TelstraConstants.ACTION_CREATE.equalsIgnoreCase(orderLine.getAttribute(TelstraConstants.ACTION))
							|| TelstraConstants.ACTION_CREATE
							.equalsIgnoreCase(inXml.getDocumentElement().getAttribute(TelstraConstants.ACTION))
							|| YFCCommon.isVoid(sInterfaceNo)) {

						//update BillToId if BillToKey is passed in input
						updateShipToIdOrKey(orderLine);

						if(!linePriorityPresent && YFCObject.isVoid(priority)) {
							priority = getOrderOrOrderLinePriority(inXml.getDocumentElement());
						} else if (linePriorityPresent) {
							priority = getOrderOrOrderLinePriority(orderLine);
						}

						String datesMapKey = getShipNode(orderLine)+getReceivingState(orderLine)+orderLine.getAttribute(TelstraConstants.SHIP_TO_ID); 
						logger.verbose("Calculated priority is "+priority);
						if(datesMap.containsKey(datesMapKey)) {
							String dates = datesMap.get(datesMapKey);
							String[] datesArray = dates.split(":");
							String shipDate = datesArray[0];
							String deliveryDate = datesArray[1];
							if(!(YFCObject.isVoid(shipDate) || shipDate.equalsIgnoreCase("null"))) {
								YDate yShipDate = YDate.newMutableDate(shipDate);
								orderLine.setAttribute("ReqShipDate", yShipDate);
							}
							if(!(YFCObject.isVoid(shipDate) || deliveryDate.equalsIgnoreCase("null"))) {
								YDate yDeliveryDate = YDate.newMutableDate(deliveryDate);
								orderLine.setAttribute("ReqDeliveryDate", yDeliveryDate);
							}

						} else {
							if(isIntegralOrder(inXml)){
								calculateDatesForIntegralOrder(orderLine,docGetCommonCodeListOutput, priority, dateToWham);
							}else if(isVectorOrder(inXml)){
								calculateDatesForVectorOrder(orderLine, docGetCommonCodeListOutput, priority);
							}
						}
					} else {
						continue;
					}
				}
			}
			// HUB-7096: Processing orders having Multiple ShipNodes and multi-DACs with V&C at order line level  - End
			/*
			 * Commenting out below method as objective of below one is achieved in GpsUpdateOrderExtnFields.java
			 */
			//			copyDateAttributesToLine(inXml);

			updateSellerNameAndCode(inXml);
			LoggerUtil.verboseLog("BeforeCreateOrderUE :: Return Doc", logger, inXml);
			/*
			 * HUB-9083 Begin
			 * Removing receiving node field if any for the sales order in before create/change order ue.
			 */			
			String sDocumentType = inXml.getDocumentElement().getAttribute(TelstraConstants.DOCUMENT_TYPE);
			if(TelstraConstants.DOCUMENT_TYPE_0001.equals(sDocumentType)){
				YFCNodeList <YFCElement> yfcNlOrderLine = inXml.getElementsByTagName(TelstraConstants.ORDER_LINE);
				for(YFCElement yfcEleOrderLine : yfcNlOrderLine){			
					yfcEleOrderLine.removeAttribute(TelstraConstants.RECEIVING_NODE);
				}	
			}
			//HUB-9083 End
			LoggerUtil.endComponentLog(logger, this.getClass().getName(),"invoke", inXml);
		}else{
			LoggerUtil.verboseLog("Skipping Before change order ue call as this has been invoked from GpsUpdateOrderExtnFields.java ", logger, inXml);
		}
		return inXml;
	}

	private void setGPSCommitDateTypeForOrderLines(YFCDocument inXml) {
      YFCNodeList<YFCElement> orderLines = inXml.getElementsByTagName("OrderLine");
      for(YFCElement orderLine : orderLines) {
        YFCDate deliveryDate = orderLine.getDateAttribute("ReqDeliveryDate");
        YFCElement orderDates = orderLine.createChild("OrderDates");
        YFCElement orderDate = orderDates.createChild("OrderDate");
        orderDate.setAttribute("DateTypeId", "GPS_COMMIT_REQUESTED_DATE");
        orderDate.setDateAttribute("RequestedDate", deliveryDate);
      }
    }

  //HUB-6785 Vendor Unknown 
	private void updateSellerNameAndCode(YFCDocument inXml) {
		String sSellerOrgCode = inXml.getDocumentElement().getAttribute("SellerOrganizationCode");
		String sSellerName = "";
		if(!YFCObject.isVoid(sSellerOrgCode)) {
			sSellerName = sSellerOrgCode + "||";
			String organizationName = getSellerNameFromSellerCode(sSellerOrgCode);
			sSellerName = sSellerName + organizationName;

			YFCElement extn = inXml.getDocumentElement().getChildElement("Extn");
			if(YFCObject.isVoid(extn)) {
				extn = inXml.getDocumentElement().createChild("Extn");
			}
			extn.setAttribute("SellerName", sSellerName);
		}
	}

	private String getSellerNameFromSellerCode(String sSellerOrgCode) {
		YFCDocument getOrganizationHirearchyInDoc = YFCDocument.getDocumentFor("<Organization OrganizationCode='"+sSellerOrgCode+"'  />");
		YFCDocument getOrganizationHirearchyOutDoc = invokeYantraApi("getOrganizationHierarchy", getOrganizationHirearchyInDoc, getOrganizationHierarchyTemplate());
		if(!YFCObject.isVoid(getOrganizationHirearchyOutDoc)) {
			return getOrganizationHirearchyOutDoc.getDocumentElement().getAttribute("OrganizationName");
		}
		return "";
	}

	private YFCDocument getOrganizationHierarchyTemplate() {
		YFCDocument getOrganizationHierarchyTemplate = YFCDocument.getDocumentFor("<Organization OrganizationName='' OrganizationCode='' />");
		return getOrganizationHierarchyTemplate;
	}

	private boolean checkIfLinePriorityPresent(YFCNodeList<YFCElement> orderLines) {
		for(YFCElement orderLine : orderLines) {
			YFCElement extnEle = orderLine.getChildElement(TelstraConstants.EXTN);
			if(!YFCObject.isVoid(extnEle) && extnEle.hasAttribute(TelstraConstants.PRIORITY_CODE)) {
				return true;
			}
		}
		return false;
	}

	private void appendMrpControllerinOrderLine(YFCDocument inXml) {
		boolean receivingNodePassedAtOrderLine = determineIfReceivingNodePassedAtLine(inXml);
		String sReceivingNode = "";

		YFCNodeList<YFCElement> nlOrderLine = inXml.getElementsByTagName(TelstraConstants.ORDER_LINE);
		for(YFCElement eleOrderLine: nlOrderLine){

			YFCElement eleExtn = eleOrderLine.getChildElement(TelstraConstants.EXTN);
			if(YFCCommon.isVoid(eleExtn)){
				eleExtn = inXml.createElement(TelstraConstants.EXTN);
				eleOrderLine.appendChild(eleExtn);
			}
			if(receivingNodePassedAtOrderLine) {
				sReceivingNode = eleOrderLine.getAttribute(TelstraConstants.RECEIVING_NODE);
			} else {
				sReceivingNode = inXml.getDocumentElement().getAttribute(TelstraConstants.RECEIVING_NODE);
			}
			if(!YFCCommon.isStringVoid(sReceivingNode)){
				String sItemID = eleOrderLine.getChildElement(TelstraConstants.ITEM).getAttribute(TelstraConstants.ITEM_ID);
				eleExtn.setAttribute(TelstraConstants.MRP_CONTROLLER, getMrpController(sItemID, sReceivingNode));			
			}
		}
	}

	private boolean determineIfReceivingNodePassedAtLine(YFCDocument inXml) {
		YFCNodeList<YFCElement> nlOrderLine = inXml.getElementsByTagName(TelstraConstants.ORDER_LINE);
		for(YFCElement eleOrderLine: nlOrderLine){
			String receivingNode=eleOrderLine.getAttribute("ReceivingNode");
			if(!YFCObject.isVoid(receivingNode)) {
				return true;
			}
		}
		return false;
	}
	private String getMrpController(String sItemID, String sReceivingNode) {

		YFCDocument docItemNodeDefnIp = YFCDocument.getDocumentFor("<ItemNodeDefn ItemID='' Node=''/>");
		docItemNodeDefnIp.getDocumentElement().setAttribute("ItemID", sItemID);
		docItemNodeDefnIp.getDocumentElement().setAttribute("Node", sReceivingNode);
		YFCDocument docItemNodeDefnTemp = YFCDocument.getDocumentFor("<ItemNodeDefnList><ItemNodeDefn ItemID='' Node=''><Extn MrpController=''/></ItemNodeDefn></ItemNodeDefnList>");
		YFCDocument docItemNodeDefnOp = invokeYantraApi(TelstraConstants.GET_ITEM_NODE_DEFN_LIST, docItemNodeDefnIp, docItemNodeDefnTemp);

		LoggerUtil.verboseLog("BeforeCreateOrderUE :: getMrpController :: docItemNodeDefnOp\n", logger, docItemNodeDefnOp);

		return XPathUtil.getXpathAttribute(docItemNodeDefnOp, "//ItemNodeDefn/Extn/@MrpController");

	}

	/**
	 * HUB-7096: Processing orders having Multiple ShipNodes and multi-DACs with V&C at order line level  - Start
	 * @param orderLine
	 */
	private void updateShipToIdOrKey(YFCElement orderLine) {
		String sShipToID = orderLine.getAttribute(TelstraConstants.SHIP_TO_ID,"");
		if(YFCObject.isNull(sShipToID)) {
			String sShipToKey = orderLine.getAttribute(TelstraConstants.SHIP_TO_KEY,"");
			if(!YFCObject.isNull(sShipToKey)) {
				//ShipToID is blank and ShipToKey exist, will call getCustomerList and get CustomerID and update in input xml
				YFCDocument docInput = YFCDocument.getDocumentFor("<Customer CustomerKey ='"+sShipToKey+"'/>");
				YFCDocument docTemplate = YFCDocument.getDocumentFor("<CustomerList><Customer CustomerKey=\"\" CustomerID=\"\"/></CustomerList>");
				YFCDocument docOutput = invokeYantraApi(TelstraConstants.API_GET_CUSTOMER_LIST, docInput, docTemplate);
				if(!YFCObject.isVoid(docOutput.getDocumentElement().getChildElement(TelstraConstants.CUSTOMER))) {
					String sCustomerID = docOutput.getDocumentElement().getChildElement(TelstraConstants.CUSTOMER).getAttribute(TelstraConstants.CUSTOMER_ID,"");
					if(!YFCObject.isNull(sCustomerID)) {
						orderLine.setAttribute(TelstraConstants.SHIP_TO_ID, sCustomerID);

					}
				}
			}
		}
		orderLine.removeAttribute(TelstraConstants.SHIP_TO_KEY);
	}

	/**
	 * HUB-7096: Processing orders having Multiple ShipNodes and multi-DACs with V&C at order line level  - End
	 * @param docOrderXml
	 * @param docGetCommonCodeListOutput
	 */

	private void itemRoundValidation(YFCDocument docOrderXml, YFCDocument docGetCommonCodeListOutput) {
		//Check Item Round Validation settings from common code
		if(isValidationOn(docGetCommonCodeListOutput,"ITEM_ROUND")) {
			String sOrganizationCode = docOrderXml.getDocumentElement().getAttribute(TelstraConstants.ENTERPRISE_CODE);
			//iterate through all the order lines
			for(YFCElement eleOrderLine : docOrderXml.getElementsByTagName(TelstraConstants.ORDER_LINE)) {
				String sShipNode = eleOrderLine.getAttribute(TelstraConstants.SHIP_NODE,"");
				//Return If shipNode is null as the getItemNodeDefnList will return me all the records for the ItemID
				
				if(YFCObject.isVoid(sShipNode)){
					continue;
				}
				
				double dOrderLineQty = eleOrderLine.getDoubleAttribute(TelstraConstants.ORDERED_QTY,0.0);
				double dNewOrderLineQty = dOrderLineQty;

				YFCElement eleItem = eleOrderLine.getChildElement(TelstraConstants.ITEM);
				if(!YFCCommon.isVoid(eleItem)){
					String sItemID = eleItem.getAttribute(TelstraConstants.ITEM_ID);
					String sUnitOfMeasure = eleItem.getAttribute(TelstraConstants.UNIT_OF_MEASURE,"");

					//call getItemNodeDefnList with input <ItemNodeDefn UnitOfMeasure="" OrganizationCode="" Node="" ItemID=""/>
					YFCDocument docInput = YFCDocument.getDocumentFor("<ItemNodeDefn UnitOfMeasure='' OrganizationCode='' Node='' ItemID=''/>");
					YFCElement eleInputRoot = docInput.getDocumentElement();
					eleInputRoot.setAttribute("UnitOfMeasure", sUnitOfMeasure);
					eleInputRoot.setAttribute("OrganizationCode", sOrganizationCode);
					eleInputRoot.setAttribute("Node", sShipNode);
					eleInputRoot.setAttribute("ItemID", sItemID);
					YFCDocument docTemplate = YFCDocument.getDocumentFor("<ItemNodeDefnList><ItemNodeDefn><Extn/></ItemNodeDefn></ItemNodeDefnList>");
					YFCDocument docOutput = invokeYantraApi(TelstraConstants.API_GET_ITEM_NODE_DEFN_LIST, docInput, docTemplate);

					YFCElement eleItemNodeDefn = docOutput.getElementsByTagName(TelstraConstants.ITEM_NODE_DEFN).item(0);
					if(!YFCObject.isVoid(eleItemNodeDefn)) {
						YFCElement eleExtn = eleItemNodeDefn.getChildElement(TelstraConstants.EXTN);
						if(!YFCObject.isVoid(eleExtn)) {
							double dRunQuantity = eleExtn.getDoubleAttribute(TelstraConstants.RUN_QUANTITY,0.0);
							if(dRunQuantity>0) {
								if(dOrderLineQty<=dRunQuantity) {
									dNewOrderLineQty = dRunQuantity;
								}
								else {
									dNewOrderLineQty = (Math.ceil(dOrderLineQty/dRunQuantity))*dRunQuantity;
								}
							}
						}
					}
					eleOrderLine.setDoubleAttribute(TelstraConstants.ORDERED_QTY, dNewOrderLineQty);
				}
			}
		}
	}

	/**
	 * HUB-7096: Processing orders having Multiple ShipNodes and multi-DACs with V&C at order line level  - Start
	 * @param orderLine
	 * @param docGetCommonCodeListOutput
	 */
	private void zipCodeValidation(YFCElement orderLine, YFCDocument docGetCommonCodeListOutput) {
		//Check Zip Code Validation settings from common code
		if(isValidationOn(docGetCommonCodeListOutput,"ZIP_CODE")) {         
			zipCodeValidation(orderLine);
		}
	}

	/**
	 * 
	 * @param orderLine
	 */
	private void zipCodeValidation(YFCElement orderLine) {
		// get ShipToId or ShipToKey from OrderLine
		String sShipToID = orderLine.getAttribute(
				TelstraConstants.SHIP_TO_ID, "");
		String sShipToKey = orderLine.getAttribute(
				TelstraConstants.SHIP_TO_KEY, "");

		if (YFCObject.isNull(sShipToID) && YFCObject.isNull(sShipToKey)) {
			// Fetch city, state, country and zip code from the bill to
			// address
			YFCElement eleShipTo = orderLine
					.getElementsByTagName(TelstraConstants.PERSON_INFO_SHIP_TO)
					.item(0);
			/*
			 * HUB-8584
			 * If shipToID, ShipToKey and eleShipTo all are blank, then throw an exception
			 */
			if(YFCCommon.isVoid(eleShipTo)){

				throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.CREATE_ORDER_SHIP_ADDRESS_MISSING_ERROR_CODE,
						new YFSException());
			}
			String sCity = eleShipTo.getAttribute(TelstraConstants.CITY, "");
			String sState = eleShipTo.getAttribute(TelstraConstants.STATE, "");
			String sCountry = eleShipTo.getAttribute(TelstraConstants.COUNTRY,
					"");
			String sZipCode = eleShipTo.getAttribute(TelstraConstants.ZIP_CODE,
					"");

			if ((sCity.equals("") && sState.equals("") && sCountry.equals(""))) {

				if (sZipCode.equals(""))
					throwZipCodeException("ZipCodeValidationFailureCode");
				else
					findLocation(eleShipTo, "", "", "", sZipCode, "ZIP");
			} else {
				if (sZipCode.equals(""))
					findLocation(eleShipTo, sCity, sState, sCountry, "",
							"CITY_STATE_COUNTRY");
				else
					findLocation(eleShipTo, sCity, sState, sCountry, sZipCode,
							"ALL");
			}
		}
	}

	/**
	 * HUB-7096: Processing orders having Multiple ShipNodes and multi-DACs with V&C at order line level  - Start
	 * @param sErrorCode
	 */

	private void throwZipCodeException(String sErrorCode) {
		// Throw Exception
		LoggerUtil.verboseLog("Cannot Determine Unique zipcode ", logger,
				" throwing exception");
		String strErrorCode = getProperty(sErrorCode, true);		
		throw ExceptionUtil.getYFSException(strErrorCode, new YFSException());
	}

	/**
	 * 
	 * @param set
	 * @return
	 */
	private String getOnlyElement(Set<String> set) {
		Iterator<String> iterator = set.iterator();
		return iterator.next();
	}

	/**
	 * 
	 * @param eleBillTo
	 * @param sPassedCity
	 * @param sPassedState
	 * @param sPassedCountry
	 * @param sPassedZipCode
	 * @param sValidate
	 */
	private void findLocation(YFCElement eleBillTo, String sPassedCity,
			String sPassedState, String sPassedCountry, String sPassedZipCode,
			String sValidate) {
		LoggerUtil.verboseLog("The findLocation method is invoked for", logger , sValidate);

		boolean bFindLocationWithAll = false;
		boolean bFindLocationWithCityStateCountry = false;
		boolean bFindLocationWithCityState = false;
		boolean bFindLocationWithCity = false;
		boolean bFindLocationWithCityZipcode = false;
		boolean bFindLocationWithZipcode = false;
		boolean bCouldntFind = false;

		if (sValidate.equals("ALL"))
			bFindLocationWithAll = true;
		else if (sValidate.equals("CITY_STATE_COUNTRY"))
			bFindLocationWithCityStateCountry = true;
		else if (sValidate.equals("CITY_STATE"))
			bFindLocationWithCityState = true;
		else if (sValidate.equals("CITY"))
			bFindLocationWithCity = true;
		else if (sValidate.equals("ZIP"))
			bFindLocationWithZipcode = true;
		else if (sValidate.equals("CITY_ZIP"))
			bFindLocationWithCityZipcode = true;

		String sZipCodeLocationListService = getProperty(
				"Service.GetZipCodeLocationList", true);

		String sCity = eleBillTo.getAttribute(TelstraConstants.CITY, "");
		String sState = eleBillTo.getAttribute(TelstraConstants.STATE, "");
		String sCountry = eleBillTo.getAttribute(TelstraConstants.COUNTRY, "");
		String sZipCode = eleBillTo.getAttribute(TelstraConstants.ZIP_CODE, "");

		if ((bFindLocationWithAll)
				|| (bFindLocationWithCityStateCountry && (!sCountry.equals("")))
				|| (bFindLocationWithCityState && (!sState.equals("")))
				|| (bFindLocationWithCity && (!sCity.equals("")))
				|| (bFindLocationWithZipcode && (!sZipCode.equals("")))
				|| (bFindLocationWithCityZipcode && (!sCity.equals("")) && (!sZipCode
						.equals("")))) {
			// call GpsGetZipCodeLocationList to get ZipCode with the passed
			// values
			YFCDocument docInput = YFCDocument.getDocumentFor("<ZipCodeLocation City='" + sPassedCity
					+ "' " + "State='" + sPassedState + "' "
					+ "Country='" + sPassedCountry + "' " + "ZipCode='"
					+ sPassedZipCode + "'/>");
			LoggerUtil.verboseLog("Calling " + sZipCodeLocationListService + " service with the following input ", logger , docInput.toString());

			YFCDocument yfcDocLocationList = invokeYantraService(
					sZipCodeLocationListService, docInput);

			YFCElement yfcEleLocationList = yfcDocLocationList
					.getDocumentElement();
			YFCNodeList<YFCElement> yfcNLLocation = yfcEleLocationList
					.getElementsByTagName("ZipCodeLocation");

			if (yfcNLLocation.getLength() == 0) {
				System.out.println("Output of the service is NULL");
				if (bFindLocationWithAll)
					findLocation(eleBillTo, sCity, sState, sCountry, "",
							"CITY_STATE_COUNTRY");
				if (bFindLocationWithCityStateCountry)
					findLocation(eleBillTo, sCity, sState, "", "", "CITY_STATE");
				if (bFindLocationWithCityState)
					findLocation(eleBillTo, sCity, "", "", "", "CITY");
				if (bFindLocationWithCity)
					findLocation(eleBillTo, "", "", "", sZipCode, "ZIP");
				if (bFindLocationWithZipcode)
					throwZipCodeException("ZipCodeDeterminationFailureCode");
				if (bFindLocationWithCityZipcode)
					throwZipCodeException("ZipCodeDeterminationFailureCode");
			} else if (yfcNLLocation.getLength() == 1) {
				LoggerUtil.verboseLog("Found a unique location with the passed input", logger , "");
				// If the getZipCodeLocationList service returns just one
				// record,
				// set the City, State, Country and ZipCode from the output of
				// the service.
				// This will replace blank values in the input are replaced by
				// valid values from the zip code list output.
				eleBillTo.setAttribute(TelstraConstants.CITY, (yfcNLLocation
						.item(0)).getAttribute(TelstraConstants.CITY));
				eleBillTo.setAttribute(TelstraConstants.STATE, (yfcNLLocation
						.item(0)).getAttribute(TelstraConstants.STATE));
				eleBillTo.setAttribute(TelstraConstants.COUNTRY, (yfcNLLocation
						.item(0)).getAttribute(TelstraConstants.COUNTRY));
				eleBillTo.setAttribute(TelstraConstants.ZIP_CODE,
						(yfcNLLocation.item(0))
						.getAttribute(TelstraConstants.ZIP_CODE));
			} else {
				LoggerUtil.verboseLog("Found multiple records with the passed input", logger , "");
				// If multiple records were found for the passed City, State,
				// Country and Zip Code
				Set<String> cityList = new HashSet<>();
				Set<String> stateList = new HashSet<>();
				Set<String> countryList = new HashSet<>();
				Set<String> zipCodeList = new HashSet<>();

				for (int i = 0; i < yfcNLLocation.getLength(); i++) {
					YFCElement yfcEleLocation = (YFCElement) yfcNLLocation
							.item(i);
					cityList.add(yfcEleLocation.getAttribute(
							TelstraConstants.CITY, ""));
					stateList.add(yfcEleLocation.getAttribute(
							TelstraConstants.STATE, ""));
					countryList.add(yfcEleLocation.getAttribute(
							TelstraConstants.COUNTRY, ""));
					zipCodeList.add(yfcEleLocation.getAttribute(
							TelstraConstants.ZIP_CODE, ""));
				}
				LoggerUtil.verboseLog("cityList ", logger , cityList);
				LoggerUtil.verboseLog("stateList ", logger , stateList);
				LoggerUtil.verboseLog("countryList ", logger , countryList);
				LoggerUtil.verboseLog("zipCodeList ", logger , zipCodeList);

				if (!cityList.contains(sCity)) {
					if (cityList.size() == 1) {
						eleBillTo.setAttribute(TelstraConstants.CITY,
								getOnlyElement(cityList));
					} else {
						bCouldntFind = true;
					}
				}
				if (!stateList.contains(sState)) {
					if (stateList.size() == 1) {
						eleBillTo.setAttribute(TelstraConstants.STATE,
								getOnlyElement(stateList));
					} else {
						bCouldntFind = true;

					}
				}
				if (!countryList.contains(sCountry)) {
					if (countryList.size() == 1) {
						eleBillTo.setAttribute(TelstraConstants.COUNTRY,
								getOnlyElement(countryList));
					} else {
						bCouldntFind = true;
					}
				}
				if (!zipCodeList.contains(sZipCode)) {
					if (zipCodeList.size() == 1) {
						eleBillTo.setAttribute(TelstraConstants.ZIP_CODE,
								getOnlyElement(zipCodeList));
					} else {
						bCouldntFind = true;
					}
				}

				if (bCouldntFind) {
					if (bFindLocationWithAll)
						findLocation(eleBillTo, sCity, sState, sCountry, "",
								"CITY_STATE_COUNTRY");
					if (bFindLocationWithCityStateCountry)
						findLocation(eleBillTo, sCity, sState, "", "",
								"CITY_STATE");
					if (bFindLocationWithCityState)
						findLocation(eleBillTo, sCity, "", "", "", "CITY");
					if (bFindLocationWithCity)
						findLocation(eleBillTo, "", "", "", sZipCode, "ZIP");
					if (bFindLocationWithZipcode) {
						if (sCity.equals(""))
							throwZipCodeException("ZipCodeDeterminationFailureCode");
						else
							findLocation(eleBillTo, sCity, "", "", sZipCode,
									"CITY_ZIP");
					}
					if (bFindLocationWithCityZipcode)
						throwZipCodeException("ZipCodeDeterminationFailureCode");
				}
			}
		} else if (bFindLocationWithCityStateCountry && (sCountry.equals(""))) {
			findLocation(eleBillTo, sCity, sState, "", "", "CITY_STATE");
		} else if (bFindLocationWithCityState && (sState.equals(""))) {
			findLocation(eleBillTo, sCity, "", "", "", "CITY");
		} else if (bFindLocationWithCity && (sCity.equals(""))) {
			findLocation(eleBillTo, "", "", "", sZipCode, "ZIP");
		} else if (bFindLocationWithZipcode && (sZipCode.equals(""))) {
			throwZipCodeException("ZipCodeDeterminationFailureCode");
		} else if (bFindLocationWithCityZipcode
				&& ((sCity.equals("")) || (!sZipCode.equals("")))) {
			throwZipCodeException("ZipCodeDeterminationFailureCode");
		}
	}

	/**
	 * 
	 * @param docOrderXml
	 * @param docGetCommonCodeListOutput
	 * @param orderLine
	 */
	private void costCenterValidation(YFCDocument docOrderXml, YFCDocument docGetCommonCodeListOutput, YFCElement orderLine) {
		//Check Cost Centre Validation settings
		if(isValidationOn(docGetCommonCodeListOutput, "COST_CENTER")) {
			String sDivision = null;
			YFCElement orderLineExtension = orderLine.getChildElement(TelstraConstants.EXTN);
			if(!YFCObject.isVoid(orderLineExtension)) {
				sDivision = orderLineExtension.getAttribute(TelstraConstants.DIVISION,"");
				if(sDivision.equals("")) {
					//get Division code from order xml
					sDivision = docOrderXml.getDocumentElement().getAttribute(TelstraConstants.DIVISION,"");
				}
			}

			if(YFCObject.isNull(sDivision)){
				//throw exception
				LoggerUtil.verboseLog("Cost Center Validation Failure ", logger, " throwing exception");
				String strErrorCode = getProperty("CostCenterValidationFailureCode", true);
				YFCDocument erroDoc = ExceptionUtil.getYFSExceptionDocument(strErrorCode);
				throw new YFSException(erroDoc.toString());	
			}
			else {
				//call GpsGetCostCenterList with input <CostCenter OrgCode="Division" ActiveFlag="Y"/>
				String sCostCenterListService = getProperty("Service.GetCostCenterList", true);
				YFCDocument docOutput = invokeYantraService(sCostCenterListService, YFCDocument.getDocumentFor("<CostCenter OrgCode='"+sDivision+"' ActiveFlag='Y'/>"));
				if(YFCObject.isVoid(docOutput.getDocumentElement().getChildElement(TelstraConstants.COST_CENTER))) {
					//throw exception
					LoggerUtil.verboseLog("Cost Center Validation Failure ", logger, " throwing exception");
					String strErrorCode = getProperty("CostCenterValidationFailureCode", true);
					YFCDocument erroDoc = ExceptionUtil.getYFSExceptionDocument(strErrorCode);
					throw new YFSException(erroDoc.toString());	

				}
			}			
		}		
	}

	/**
	 * 
	 * @param docGetCommonCodeListOutput
	 * @param sValidationType
	 * @return
	 */
	private boolean isValidationOn(YFCDocument docGetCommonCodeListOutput, String sValidationType) {
		boolean bValidation = false;
		for(YFCElement eleCommonCode : docGetCommonCodeListOutput.getElementsByTagName(TelstraConstants.COMMON_CODE)) {
			if(eleCommonCode.getAttribute(TelstraConstants.CODE_VALUE).equals(sValidationType) && eleCommonCode.getAttribute(TelstraConstants.CODE_SHORT_DESCRIPTION).equals(TelstraConstants.YES)) {
				bValidation = true;
				break;
			}
		}		
		return bValidation;
	}	

	/**
	 * 
	 * @param docOrderXml
	 * @param docGetCommonCodeListOutput
	 */
	private void calculateDatesForIntegralOrder(YFCElement orderLine, YFCDocument docGetCommonCodeListOutput, Priority priority, YDate dateToWham){       
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "calculateDatesForIntegralOrder", orderLine);
		if(priority == Priority.URGENT){
			handleForUrgentOrder(orderLine, docGetCommonCodeListOutput);
		}else if(priority == Priority.STANDARD_HOME_DELIVERY){
			handleForStandardHomeDeliveryOrder(orderLine,docGetCommonCodeListOutput, dateToWham);
		}else{
			handleForStandardOrder(orderLine,docGetCommonCodeListOutput, dateToWham);
		}
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "calculateDatesForIntegralOrder", orderLine);
	}

	/**
	 * 
	 * @param docOrderXml
	 * @param docGetCommonCodeListOutput
	 */
	private void calculateDatesForVectorOrder(YFCElement orderLine, YFCDocument docGetCommonCodeListOutput, Priority priority){
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "calculateDatesForVectorOrder", orderLine);
		if(priority == Priority.MEDICAL){
			handleForVectorMedicalOrder(orderLine, docGetCommonCodeListOutput);
		}else if(priority == Priority.URGENT){
			handleForUrgentVectorOrder(orderLine, docGetCommonCodeListOutput);
		}else{
			handleForLowPriorityVectorOrder(orderLine, docGetCommonCodeListOutput);
		}
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "calculateDatesForVectorOrder", orderLine);
	}

	/**
	 * HUB-7096: Processing orders having Multiple ShipNodes and multi-DACs with V&C at order line level  - Start
	 * @param docOrderXml
	 * @param docGetCommonCodeListOutput
	 */

	private void handleForVectorMedicalOrder(YFCElement orderLine, YFCDocument docGetCommonCodeListOutput){
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "handleForVectorMedicalOrder", orderLine);

		String shipNode = getShipNode(orderLine);
		String shipNodeState = getStateForShipNode(shipNode,docGetCommonCodeListOutput);
		if(YFCObject.isVoid(shipNodeState)){
			return;
		}
		YDate shipDate = orderLine.getYDateAttribute(TelstraConstants.REQUIRED_SHIP_DATE);
		if(shipDate == null){
			shipDate = new YDate(false);
			orderLine.setAttribute(TelstraConstants.REQUIRED_SHIP_DATE, shipDate);
		}
		YDate deliveryDate = orderLine.getYDateAttribute(TelstraConstants.REQUIRED_DELIVERY_DATE);
		if(deliveryDate != null){
			return;
		}
		YFCDocument calendarDoc = getCalendarDocument(shipNodeState);
		if(calendarDoc == null){
			return;
		}
		String materialClass = getMaterialClassFromDepartmentCode(docGetCommonCodeListOutput);
		if(YFCObject.isVoid(materialClass)){
			return;
		}
		YFCElement transportMatrix = getTransportMatrix(materialClass, TelstraConstants.ORDER_PRIORITY_MEDICAL, orderLine,docGetCommonCodeListOutput);
		if(transportMatrix == null){
			return;
		}
		int transitTime = OrderDateHelper.getTransitTime(shipDate,transportMatrix);
		deliveryDate = calculateDeliveryDate(orderLine,calendarDoc,orderLine.getYDateAttribute(TelstraConstants.REQUIRED_SHIP_DATE),transitTime,docGetCommonCodeListOutput);
		orderLine.setAttribute(TelstraConstants.REQUIRED_DELIVERY_DATE, deliveryDate);
		datesMap.put(shipNode+getReceivingState(orderLine)+orderLine.getAttribute("ShipToID"), shipDate+":"+deliveryDate);
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "handleForVectorMedicalOrder", orderLine);
	}
	// HUB-7096: Processing orders having Multiple ShipNodes and multi-DACs with V&C at order line level  - End

	/**
	 * 
	 * @param orderLine
	 * @param docGetCommonCodeListOutput
	 */
	// HUB-7096: Processing orders having Multiple ShipNodes and multi-DACs with V&C at order line level  - Start
	private void handleForLowPriorityVectorOrder(YFCElement orderLine, YFCDocument docGetCommonCodeListOutput){
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "handleForLowPriorityVectorOrder.OrderLine", orderLine);

		String shipNode = getShipNode(orderLine);
		String shipNodeState = getStateForShipNode(shipNode,docGetCommonCodeListOutput);
		if(YFCObject.isVoid(shipNodeState)){
			return;
		}

		YDate shipDate = orderLine.getYDateAttribute(TelstraConstants.REQUIRED_SHIP_DATE);
		boolean isShipDatePassed = true;
		if(shipDate == null){
			isShipDatePassed = false;
			shipDate = new YDate(false);
		}
		LoggerUtil.verboseLog("BeforeCreateOrderUE :: handleForLowPriorityVectorOrder :: shipDate", logger, shipDate);

		YFCDocument docShipNodeOrgDetails = getOrganizationHeirarchy(shipNode);
		YFCDocument calendarDoc = getCalendarDocument(shipNodeState);
		if(calendarDoc == null){
			return;
		}
		List<String> exceptionDates = OrderDateHelper.getExceptionDates(calendarDoc);

		YFCElement allDayShift = OrderDateHelper.getAllDayShiftElement(calendarDoc, shipDate);

		if(!isShipDatePassed){
			shipDate = OrderDateHelper.getNextBusinessDay(shipDate, allDayShift, exceptionDates);           
		}
		orderLine.setAttribute(TelstraConstants.REQUIRED_SHIP_DATE, shipDate);
		YDate deliveryDate = orderLine.getYDateAttribute(TelstraConstants.REQUIRED_DELIVERY_DATE);
		if(deliveryDate != null){
			return;
		}
		calculateDeliveryDateForLowPriorityVectorOrder(orderLine, docGetCommonCodeListOutput, docShipNodeOrgDetails, calendarDoc);

		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "handleForLowPriorityVectorOrder", orderLine);
	}
	// HUB-7096: Processing orders having Multiple ShipNodes and multi-DACs with V&C at order line level  - End    
	/**
	 * 
	 * @param docOrderXml
	 * @param docGetCommonCodeListOutput
	 * @param docShipNodeOrgDetails
	 * @param calendarDoc
	 */
	// HUB-7096: Processing orders having Multiple ShipNodes and multi-DACs with V&C at order line level  - Start
	private void calculateDeliveryDateForLowPriorityVectorOrder(YFCElement orderLine, YFCDocument docGetCommonCodeListOutput, YFCDocument docShipNodeOrgDetails,YFCDocument calendarDoc){

		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "calculateDeliveryDateForLowPriorityVectorOrder", orderLine);

		YDate shipDate = orderLine.getYDateAttribute(TelstraConstants.REQUIRED_SHIP_DATE);

		YFCElement extnEle = orderLine.getChildElement(TelstraConstants.EXTN);
		String priority = "";
		if(!YFCObject.isVoid(extnEle)) {
			priority = extnEle.getAttribute(TelstraConstants.PRIORITY_CODE);
		}
		else {
			priority = orderLine.getAttribute(TelstraConstants.PRIORITY_CODE);
		}

		String materialClass = getMaterialClassFromDepartmentCode(docGetCommonCodeListOutput);
		if(YFCObject.isVoid(priority) || YFCObject.isVoid(materialClass)){
			return;
		}
		Priority oPriority = getOrderOrOrderLinePriority(orderLine);
		YFCElement transportMatrix = getTransportMatrix(materialClass, oPriority.toString(), orderLine,docGetCommonCodeListOutput);

		if(transportMatrix == null){
			return;
		}
		int transitTime = OrderDateHelper.getTransitTime(shipDate,transportMatrix);
		int totalTransitTime = transitTime;
		boolean isHazmat = isHazmatOrderLine(orderLine);
		if(isHazmat){
			totalTransitTime = transitTime + 1;
		}
		YDate deliveryDate = calculateDeliveryDate(orderLine,calendarDoc,orderLine.getYDateAttribute(TelstraConstants.REQUIRED_SHIP_DATE),totalTransitTime,docGetCommonCodeListOutput);
		orderLine.setAttribute(TelstraConstants.REQUIRED_DELIVERY_DATE, deliveryDate);
		datesMap.put(getShipNode(orderLine)+getReceivingState(orderLine)+orderLine.getAttribute("ShipToID"), shipDate+":"+deliveryDate);
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "calculateDeliveryDateForLowPriorityVectorOrder", orderLine);
	}
	// HUB-7096: Processing orders having Multiple ShipNodes and multi-DACs with V&C at order line level  - End
	/**
	 * 
	 * @param orderLine
	 * @param docGetCommonCodeListOutput
	 */
	// HUB-7096: Processing orders having Multiple ShipNodes and multi-DACs with V&C at order line level  - Start
	private void handleForUrgentVectorOrder(YFCElement orderLine, YFCDocument docGetCommonCodeListOutput){

		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "handleForUrgentVectorOrder", orderLine);
		String shipNode = getShipNode(orderLine);
		String shipNodeState = getStateForShipNode(shipNode,docGetCommonCodeListOutput);
		if(YFCObject.isVoid(shipNodeState)){
			return;
		}
		YDate shipDate = orderLine.getYDateAttribute(TelstraConstants.REQUIRED_SHIP_DATE);
		boolean isShipDatePassed = true;
		if(shipDate == null){
			isShipDatePassed = false;
			shipDate = new YDate(false);
			orderLine.setAttribute(TelstraConstants.REQUIRED_SHIP_DATE, shipDate);
		}

		YFCDocument docShipNodeOrgDetails = getOrganizationHeirarchy(shipNode);
		LoggerUtil.verboseLog("BeforeCreateOrderUE :: handleForUrgentVectorOrder :: docShipNodeOrgDetails", logger, docShipNodeOrgDetails);

		YFCDocument calendarDoc = getCalendarDocument(shipNodeState);
		if(calendarDoc == null){
			return;
		}
		List<String> exceptionDates = OrderDateHelper.getExceptionDates(calendarDoc);
		YFCElement allDayShift = OrderDateHelper.getAllDayShiftElement(calendarDoc, shipDate);
		boolean isHazmat = isHazmatOrderLine(orderLine);
		LoggerUtil.verboseLog("BeforeCreateOrderUE :: handleForUrgentVectorOrder :: isHazmat", logger, isHazmat);

		if(!isShipDatePassed && !isBeforeCutOff(shipNode, docShipNodeOrgDetails, docGetCommonCodeListOutput)){
			shipDate = OrderDateHelper.getNextBusinessDay(shipDate, allDayShift, exceptionDates);
			orderLine.setAttribute(TelstraConstants.REQUIRED_SHIP_DATE, shipDate);
		}
		YDate reqDeliveryDate = orderLine.getYDateAttribute(TelstraConstants.REQUIRED_DELIVERY_DATE);
		if(reqDeliveryDate != null){
			return;
		}
		String materialClass = getMaterialClassFromDepartmentCode(docGetCommonCodeListOutput);

		YFCElement transportMatrix = getTransportMatrix(materialClass, "Urgent", orderLine,docGetCommonCodeListOutput);

		if(transportMatrix == null){
			return;
		}
		int transitTime = OrderDateHelper.getTransitTime(shipDate,transportMatrix);

		int totalTransitTime = transitTime;
		if(isHazmat){
			totalTransitTime = transitTime + 1;
		}
		YDate deliveryDate = calculateDeliveryDate(orderLine,calendarDoc,orderLine.getYDateAttribute(TelstraConstants.REQUIRED_SHIP_DATE),totalTransitTime,docGetCommonCodeListOutput);
		orderLine.setAttribute(TelstraConstants.REQUIRED_DELIVERY_DATE, deliveryDate);
		datesMap.put(shipNode+getReceivingState(orderLine)+orderLine.getAttribute("ShipToID"), shipDate+":"+deliveryDate);
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "handleForUrgentVectorOrder", orderLine);
	}
	// HUB-7096: Processing orders having Multiple ShipNodes and multi-DACs with V&C at order line level  - End

	/**
	 * 
	 * @param docOrderXml
	 * @param docGetCommonCodeListOutput
	 */
	// HUB-7096: Processing orders having Multiple ShipNodes and multi-DACs with V&C at order line level  - Start
	private void handleForUrgentOrder(YFCElement orderLine, YFCDocument docGetCommonCodeListOutput){

		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "handleForUrgentOrder", orderLine);
		String shipNode = getShipNode(orderLine);
		String shipNodeState = getStateForShipNode(shipNode,docGetCommonCodeListOutput);
		if(YFCObject.isVoid(shipNodeState)){
			return;
		}
		YDate shipDate = new YDate(false);
		YFCDocument docShipNodeOrgDetails = getOrganizationHeirarchy(shipNode);

		YFCDocument calendarDoc = getCalendarDocument(shipNodeState);

		if(calendarDoc == null){
			return;
		}
		List<String> exceptionDates = OrderDateHelper.getExceptionDates(calendarDoc);

		YFCElement allDayShift = OrderDateHelper.getAllDayShiftElement(calendarDoc, shipDate);

		boolean isHazmat = isHazmatOrderLine(orderLine);
		LoggerUtil.verboseLog("BeforeCreateOrderUE :: handleForUrgentOrder :: isHazmat", logger, isHazmat);

		if(!isBeforeCutOff(shipNode, docShipNodeOrgDetails, docGetCommonCodeListOutput)){
			shipDate = OrderDateHelper.getNextBusinessDay(shipDate, allDayShift, exceptionDates);
		}
		orderLine.setAttribute(TelstraConstants.REQUIRED_SHIP_DATE, shipDate);
		YFCElement transportMatrix = getTransportMatrix(TelstraConstants.MATERIALS, "Urgent", orderLine,docGetCommonCodeListOutput);

		if(transportMatrix == null){
			return;
		}
		int transitTime = OrderDateHelper.getTransitTime(shipDate,transportMatrix);

		int totalTransitTime = transitTime;
		if(isHazmat){
			totalTransitTime = transitTime + 1;
		}
		YDate deliveryDate = calculateDeliveryDate(orderLine,calendarDoc,orderLine.getYDateAttribute(TelstraConstants.REQUIRED_SHIP_DATE),totalTransitTime,docGetCommonCodeListOutput);

		orderLine.setAttribute(TelstraConstants.REQUIRED_DELIVERY_DATE, deliveryDate);
		datesMap.put(shipNode+getReceivingState(orderLine)+orderLine.getAttribute("ShipToID"), shipDate+":"+deliveryDate);
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "handleForUrgentOrder", orderLine);
	}
	// HUB-7096: Processing orders having Multiple ShipNodes and multi-DACs with V&C at order line level  - End

	/**
	 * 
	 * @param docOrderXml
	 * @param docGetCommonCodeListOutput
	 */
	// HUB-7096: Processing orders having Multiple ShipNodes and multi-DACs with V&C at order line level  - Start
	private void handleForStandardOrder(YFCElement orderLine,YFCDocument docGetCommonCodeListOutput, YDate dateToWham){

		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "handleForStandardOrder", orderLine);

		YDate shipDate = new YDate(false);
		String shipNode = getShipNode(orderLine);
		String shipNodeState = getStateForShipNode(shipNode,docGetCommonCodeListOutput);
		if(YFCObject.isVoid(shipNodeState)){
			return;
		}
		YFCDocument calendarDoc = getCalendarDocument(shipNodeState);
		if(calendarDoc == null){
			return;
		}
		List<String> exceptionDates = OrderDateHelper.getExceptionDates(calendarDoc);
		boolean isHazmat = isHazmatOrderLine(orderLine);

		YFCElement allDayShift = OrderDateHelper.getAllDayShiftElement(calendarDoc, shipDate);
		YFCElement dacSchedule = getDACSchedule(orderLine);
		YFCElement milkRunSchedule = null;
		YDate expShipDate;
		if(dateToWham == null){
			dateToWham = new YDate(false);
		}
		LoggerUtil.verboseLog("OrderDateHelper :: handleForStandardOrder :: dateToWham", logger, dateToWham);

		YFCDocument weekNumbers = getWeekNumbers();
		LoggerUtil.verboseLog("OrderDateHelper :: handleForStandardOrder :: weekNumbers", logger, weekNumbers);

		String routeId = null;
		if(dacSchedule != null){
			routeId = dacSchedule.getAttribute(TelstraConstants.ROUTE_ID);
			if(!YFCObject.isVoid(routeId)){
				milkRunSchedule = getMilkRunSchedule(routeId,dateToWham);
			}
			String schedule = dacSchedule.getAttribute(TelstraConstants.SCHEDULE);
			expShipDate = getNextBusinessDayBasedOnSchedule(dateToWham, schedule, weekNumbers, exceptionDates, allDayShift, true);

		}else{
			expShipDate = OrderDateHelper.getNextBusinessDay(dateToWham, allDayShift, exceptionDates);
		}
		orderLine.setAttribute(TelstraConstants.REQUIRED_SHIP_DATE, expShipDate);
		YDate deliveryDate;
		if(milkRunSchedule != null){
			String receivingState = getReceivingState(orderLine);
			if(YFCObject.isVoid(receivingState)){
				String receivingNode = getReceivingNode(orderLine);
				if(!YFCObject.isVoid(receivingNode)){
					receivingState = getStateForShipNode(receivingNode, docGetCommonCodeListOutput);
				}
			}
			YFCDocument receivingStateCalendarDoc = getCalendarDocument(receivingState);
			List<String> receivingStateExceptionDates = OrderDateHelper.getExceptionDates(receivingStateCalendarDoc);
			setRouteId(orderLine, routeId);
			deliveryDate = getMilkRunScheduledDate(orderLine,expShipDate,milkRunSchedule, receivingStateExceptionDates, allDayShift,docGetCommonCodeListOutput);
		}else{
			YFCElement transportMatrix = getTransportMatrix(TelstraConstants.MATERIALS, "Standard", orderLine,docGetCommonCodeListOutput);
			if(transportMatrix == null){
				return;
			}
			int transitTime = OrderDateHelper.getTransitTime(expShipDate,transportMatrix);
			int totalTransitTime = transitTime;
			if(isHazmat){
				totalTransitTime = transitTime + 1;
			}
			deliveryDate = calculateDeliveryDate(orderLine,calendarDoc,expShipDate,totalTransitTime,docGetCommonCodeListOutput);
		}
		orderLine.setAttribute(TelstraConstants.REQUIRED_DELIVERY_DATE, deliveryDate);
		datesMap.put(shipNode+getReceivingState(orderLine)+orderLine.getAttribute("ShipToID"), shipDate+":"+deliveryDate);
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "handleForStandardOrder", orderLine);

	}
	// HUB-7096: Processing orders having Multiple ShipNodes and multi-DACs with V&C at order line level  - End

	/**
	 * 
	 * @param docOrderXml
	 * @param docGetCommonCodeListOutput
	 */
	// HUB-7096: Processing orders having Multiple ShipNodes and multi-DACs with V&C at order line level  - Start
	private void handleForStandardHomeDeliveryOrder(YFCElement orderLine,YFCDocument docGetCommonCodeListOutput, YDate dateToWham){
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "handleForStandardHomeDeliveryOrder", orderLine);

		YDate shipDate = new YDate(false);
		String shipNode = getShipNode(orderLine);
		String shipNodeState = getStateForShipNode(shipNode,docGetCommonCodeListOutput);
		if(YFCObject.isVoid(shipNodeState)){
			return;
		}
		YFCDocument calendarDoc = getCalendarDocument(shipNodeState);
		if(calendarDoc == null){
			return;
		}
		List<String> exceptionDates = OrderDateHelper.getExceptionDates(calendarDoc);
		boolean isHazmat = isHazmatOrderLine(orderLine);
		YFCElement allDayShift = OrderDateHelper.getAllDayShiftElement(calendarDoc, shipDate);
		YFCElement dacSchedule = getDACSchedule(orderLine);
		YFCElement milkRunSchedule = null;
		YDate expShipDate;
		if(dateToWham == null){
			dateToWham = new YDate(false);
		}
		YFCDocument weekNumbers = getWeekNumbers();
		String routeId = null;
		if(dacSchedule != null){
			routeId = dacSchedule.getAttribute(TelstraConstants.ROUTE_ID);
			if(!YFCObject.isVoid(routeId)){
				milkRunSchedule = getMilkRunSchedule(routeId,dateToWham);
			}
			String schedule = dacSchedule.getAttribute(TelstraConstants.SCHEDULE);
			expShipDate = getNextBusinessDayBasedOnSchedule(dateToWham, schedule, weekNumbers, exceptionDates, allDayShift, true);

		}else{
			expShipDate = OrderDateHelper.getNextBusinessDay(dateToWham, allDayShift, exceptionDates);
		}
		orderLine.setAttribute(TelstraConstants.REQUIRED_SHIP_DATE, expShipDate);
		YDate deliveryDate;
		if(milkRunSchedule != null){
			setRouteId(orderLine, routeId);
			deliveryDate = getMilkRunScheduledDate(orderLine,expShipDate,milkRunSchedule, exceptionDates, allDayShift,docGetCommonCodeListOutput);
		}else{
			YFCElement transportMatrix = getTransportMatrix(TelstraConstants.MATERIALS, "Home Delivery", orderLine,docGetCommonCodeListOutput);
			if(transportMatrix == null){
				return;
			}
			int transitTime = OrderDateHelper.getTransitTime(expShipDate,transportMatrix);
			int totalTransitTime = transitTime;
			if(isHazmat){
				totalTransitTime = transitTime + 1;
			}
			deliveryDate = calculateDeliveryDate(orderLine,calendarDoc,expShipDate,totalTransitTime,docGetCommonCodeListOutput);
		}
		orderLine.setAttribute(TelstraConstants.REQUIRED_DELIVERY_DATE, deliveryDate);
		datesMap.put(shipNode+getReceivingState(orderLine)+orderLine.getAttribute("ShipToID"), shipDate+":"+deliveryDate);
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "handleForStandardHomeDeliveryOrder", orderLine);
	}
	// HUB-7096: Processing orders having Multiple ShipNodes and multi-DACs with V&C at order line level  - End
	/**
	 * 
	 * @param materialClass
	 * @param priority
	 * @param docOrderXml
	 * @param docGetCommonCodeListOutput
	 * @return
	 */
	// HUB-7096: Processing orders having Multiple ShipNodes and multi-DACs with V&C at order line level  - Start
	private YFCElement getTransportMatrix(String materialClass, String priority, YFCElement orderLine, YFCDocument docGetCommonCodeListOutput){

		YFCDocument materialToTransportMapInput =  YFCDocument.getDocumentFor("<MaterialToTransportMap MaterialClass='" + materialClass.toUpperCase() + "' Priority='"+ priority.toUpperCase() +"' />");
		YFCDocument materialToTransportMapOutput = invokeYantraService(TelstraConstants.API_GET_MATERIAL_TO_TRANSPORT_MAP, materialToTransportMapInput);
		if(materialToTransportMapOutput == null){
			LoggerUtil.verboseLog("No material to transport map found for material class : "+materialClass+ " and priority : "+priority, logger, materialToTransportMapInput);
			return null;
		}
		LoggerUtil.verboseLog("Fount material to transport map", logger, materialToTransportMapOutput);
		String matrixName = materialToTransportMapOutput.getDocumentElement().getAttribute(TelstraConstants.MATRIX_NAME);
		if(YFCObject.isVoid(matrixName)){
			return null;
		}
		// Santosh Nagaraj:  Changed the tag name fromo PersonInfoBillTo to PersonInfoShipTo
		YFCElement personInfoShipToElem = orderLine.getChildElement(TelstraConstants.PERSON_INFO_SHIP_TO);
		if(personInfoShipToElem == null){
			return null;
		}
		String toPostCode = personInfoShipToElem.getAttribute(TelstraConstants.ZIP_CODE);
		if(YFCObject.isVoid(toPostCode)){
			return null;
		}
		String suburb = personInfoShipToElem.getAttribute(TelstraConstants.CITY);
		String shipNode = getShipNode(orderLine);
		String shipNodeState = getStateForShipNode(shipNode,docGetCommonCodeListOutput);
		/*YFCDocument transportMatrixListInput = YFCDocument.getDocumentFor("<TransportMatrix MatrixName='"+matrixName+"' "
                + "FromState='"+shipNodeState.toUpperCase()+"' ToPostcode ='"+toPostCode.toUpperCase()+"' ToSuburb ='"+suburb.toUpperCase()+"'/>");*/

		//HUB-8596 [Begin]
		YFCDocument transportMatrixListInput = YFCDocument.getDocumentFor("<TransportMatrix/>");
		YFCElement yfcEleTransportMatrixIpRoot = transportMatrixListInput.getDocumentElement();
		yfcEleTransportMatrixIpRoot.setAttribute(TelstraConstants.MATRIX_NAME, matrixName);
		if(!YFCCommon.isStringVoid(shipNodeState)){
			yfcEleTransportMatrixIpRoot.setAttribute(TelstraConstants.FROM_STATE, shipNodeState.toUpperCase());
		}
		if(!YFCCommon.isStringVoid(toPostCode)){
			yfcEleTransportMatrixIpRoot.setAttribute(TelstraConstants.TO_POSTCODE, toPostCode.toUpperCase());
		}
		if(!YFCCommon.isStringVoid(suburb)){
			yfcEleTransportMatrixIpRoot.setAttribute(TelstraConstants.TO_SUBURB, suburb.toUpperCase());
		}
		//HUB-8596 [End]        
		LoggerUtil.verboseLog("getTransportMatrix :: transportMatrixListInput", logger, transportMatrixListInput);

		YFCDocument transportMatrixListOutput = invokeYantraService(TelstraConstants.API_GET_TRANSPORT_MATRIX_LIST, transportMatrixListInput);
		if(transportMatrixListOutput == null){
			return null;
		}
		YDate reqShipDate = orderLine.getYDateAttribute(TelstraConstants.REQUIRED_SHIP_DATE);
		if(reqShipDate == null){
			reqShipDate = new YDate(true);
		}

		YFCElement filteredTransportMatrix = filterTransportMatrixListOutput(transportMatrixListOutput,reqShipDate);
		if(filteredTransportMatrix != null){
			LoggerUtil.verboseLog("Filtered transport matrix based on ship date", logger, filteredTransportMatrix);
		}else{
			LoggerUtil.verboseLog("Filtered tranport matrix is null", logger, transportMatrixListOutput);
		}
		return filteredTransportMatrix;
	}
	// HUB-7096: Processing orders having Multiple ShipNodes and multi-DACs with V&C at order line level  - End

	/**
	 * 
	 * @param orderElem
	 * @param calendarDoc
	 * @param shipDate
	 * @param transitTime
	 * @param docGetCommonCodeListOutput
	 * @return
	 */

	private YDate calculateDeliveryDate(YFCElement orderElem, YFCDocument calendarDoc, YDate shipDate, int transitTime, YFCDocument docGetCommonCodeListOutput){

		if(!YFCCommon.isVoid(shipDate)){
			LoggerUtil.verboseLog("BeforeCreateOrderUE :: calculateDeliveryDate :: shipDate", logger, shipDate);
		}
		if(transitTime == 0){
			return shipDate;
		}
		LinkedList<YDate> transitDates = new LinkedList<>();
		//First get list of transit dates
		for(int i=1; i<=transitTime;i++){
			YDate transitDate = new YDate(shipDate.getString(TelstraConstants.DATE_TIME_FORMAT), TelstraConstants.DATE_TIME_FORMAT, false);
			YFCDateUtils.addHours(transitDate, 24*i);
			transitDates.add(transitDate);
		}
		/*HUB-7285/8088 -[START]


		/*  The below logic is to handle the Exception and Weekend dates removal for the delivery date in the receiving Node's state,
		    and to check if the computed delivery date falls on a holiday, if it does choose delivery date as the next business day in 
		    getDeliveryDateConsideringDeliveringState() in the receiving Node's state */

		String receivingState = getReceivingState(orderElem);
		if(YFCObject.isVoid(receivingState)){
			String receivingNode = getReceivingNode(orderElem);
			if(!YFCObject.isVoid(receivingNode)){
				receivingState = getStateForShipNode(receivingNode, docGetCommonCodeListOutput);
			}
		}

		YFCDocument receivingStateCalendarDoc = getCalendarDocument(receivingState);
		List<String> exceptionDates = OrderDateHelper.getExceptionDates(receivingStateCalendarDoc);
		YFCElement allDayShift = OrderDateHelper.getAllDayShiftElement(receivingStateCalendarDoc, shipDate);
		//Now remove and exception days
		OrderDateHelper.removeExceptionAndWeekends(transitDates, allDayShift,exceptionDates);
		YDate deliveryDate = transitDates.getLast();
		if(!YFCCommon.isVoid(deliveryDate)){
			LoggerUtil.verboseLog("BeforeCreateOrderUE :: calculateDeliveryDate :: deliveryDate", logger, deliveryDate);
		}

		return getDeliveryDateConsideringDeliveringState(orderElem, deliveryDate,docGetCommonCodeListOutput);
		//HUB-7285/8088 -[END]
	}

	/**
	 * 
	 * @param orderElem
	 * @param date
	 * @param milkRun
	 * @param exceptionDates
	 * @param allDayShift
	 * @param docGetCommonCodeListOutput
	 * @return
	 */

	private YDate getMilkRunScheduledDate(YFCElement orderElem,YDate date, YFCElement milkRun, List<String> exceptionDates, YFCElement allDayShift, YFCDocument docGetCommonCodeListOutput){
		String frequency = milkRun.getAttribute(TelstraConstants.FREQUENCY);
		YDate effectiveFrom = milkRun.getYDateAttribute("EffectiveFrom");
		YFCDocument weekNumbers = null;
		String schedule;
		YDate shipDateNextDate = new YDate(date.getString(TelstraConstants.TELSTRA_DATE_FORMAT),TelstraConstants.TELSTRA_DATE_FORMAT,true);
		YFCDateUtils.addHours(shipDateNextDate, 24);
		if(frequency.equalsIgnoreCase(TelstraConstants.FORTNIGHTLY)){
			schedule = "2" + milkRun.getAttribute(TelstraConstants.WEEKDAY).toUpperCase();
			weekNumbers = OrderDateHelper.createWeekNumberDocForFortnightlyFrequency(effectiveFrom,shipDateNextDate);
		}else{
			schedule = "0" + milkRun.getAttribute(TelstraConstants.WEEKDAY).toUpperCase();
		}
		YDate deliveryDate = getNextBusinessDayBasedOnSchedule(shipDateNextDate, schedule, weekNumbers, exceptionDates, allDayShift, false);

		return getDeliveryDateConsideringDeliveringState(orderElem, deliveryDate,docGetCommonCodeListOutput);
	}

	/**
	 * 
	 * @param orderElem
	 * @param deliveryDate
	 * @param docGetCommonCodeListOutput
	 * @return
	 */
	private YDate getDeliveryDateConsideringDeliveringState(YFCElement orderElem, YDate deliveryDate, YFCDocument docGetCommonCodeListOutput){
		String receivingState = getReceivingState(orderElem);
		if(YFCObject.isVoid(receivingState)){
			String receivingNode = getReceivingNode(orderElem);
			if(!YFCObject.isVoid(receivingNode)){
				receivingState = getStateForShipNode(receivingNode, docGetCommonCodeListOutput);
			}
		}
		if(YFCObject.isVoid(receivingState)){
			return deliveryDate;
		}
		YDate newDeliveryDate = OrderDateHelper.getActualDeliveryDateForReceivingState(deliveryDate, receivingState, getCalendarDocument(receivingState));
		if(!YFCCommon.isVoid(newDeliveryDate)){
			LoggerUtil.verboseLog("BeforeCreateOrderUE :: getDeliveryDateConsideringDeliveringState :: newDeliveryDate", logger, newDeliveryDate);
		}

		return newDeliveryDate;
	}

	/**
	 * 
	 * @param transportMatrixListOutput
	 * @param shipDate
	 * @return
	 */
	private YFCElement filterTransportMatrixListOutput(YFCDocument transportMatrixListOutput, YDate shipDate){
		if(transportMatrixListOutput == null || transportMatrixListOutput.getDocumentElement() == null){
			return null;
		}
		for(Iterator<YFCElement> itr = transportMatrixListOutput.getDocumentElement().getChildren().iterator();itr.hasNext();){
			YFCElement elem = itr.next();
			YDate fromDate = elem.getYDateAttribute("FromDate");
			YDate toDate = elem.getYDateAttribute("ToDate");
			if(fromDate == null && toDate == null){
				return elem;
			}
			if(fromDate == null || toDate == null){
				if(fromDate != null){
					if(shipDate.after(fromDate)){
						return elem;
					}
				}
				if(toDate != null){
					if(shipDate.before(fromDate)){
						return elem;
					}
				}
			}
			if(shipDate.after(fromDate) && shipDate.before(toDate)){		
				LoggerUtil.verboseLog("BeforeCreateOrderUE :: filterTransportMatrixListOutput :: elem", logger, elem);
				return elem;
			}
		}
		return null;
	}

	/**
	 * 
	 * @param shipNode
	 * @param docShipNodeOrgDetails
	 * @param docGetCommonCodeListOutput
	 * @return
	 */
	private boolean isBeforeCutOff(String shipNode, YFCDocument docShipNodeOrgDetails, YFCDocument docGetCommonCodeListOutput){
		try{
			String shipNodeLocaleCode = docShipNodeOrgDetails.getDocumentElement().getAttribute("LocaleCode");
			YFCLocale yfcLocale = YFCLocale.getYFCLocale(shipNodeLocaleCode);
			String shipNodeTimeZoneString = yfcLocale.getTimezone();
			java.util.TimeZone shipNodeTimeZone = java.util.TimeZone.getTimeZone(shipNodeTimeZoneString);

			//Currently calculating based on current time
			YDate now = new YDate(false);
			SimpleDateFormat hostFormatter = new SimpleDateFormat(TelstraConstants.TELSTRA_DATE_TIME_FORMAT);
			SimpleDateFormat nodeFormatter = new SimpleDateFormat(TelstraConstants.TELSTRA_DATE_TIME_FORMAT);

			nodeFormatter.setTimeZone(shipNodeTimeZone);
			String convertedDateString = nodeFormatter.format(now);
			Date convrtedJavaDate = hostFormatter.parse(convertedDateString); 
			YDate convertedDate = new YDate(convrtedJavaDate.getTime(), false);	
			YDate cutoffDate = getCutOffTimeForShipNode(shipNode, docGetCommonCodeListOutput);
			if(!YFCCommon.isVoid(cutoffDate)){
				LoggerUtil.verboseLog("BeforeCreateOrderUE :: isBeforeCutOff :: cutoffDate", logger, cutoffDate);
			}
			boolean rBoolean = cutoffDate == null ? false : convertedDate.before(cutoffDate); 
			LoggerUtil.verboseLog("BeforeCreateOrderUE :: isBeforeCutOff :: rBoolean", logger, rBoolean);
			return rBoolean;
		}catch(ParseException pe){
			throw new YFCException(pe);
		}
	}

	/**
	 * 
	 * @param shipNode
	 * @param docGetCommonCodeListOutput
	 * @return
	 */
	private YDate getCutOffTimeForShipNode(String shipNode, YFCDocument docGetCommonCodeListOutput){
		YFCElement elemCutOffTime = getCommonCodeFor(TelstraConstants.CUT_OFF_TIME, shipNode, docGetCommonCodeListOutput);
		if(elemCutOffTime == null){
			elemCutOffTime = getCommonCodeFor(TelstraConstants.CUT_OFF_TIME, "DEFAULT", docGetCommonCodeListOutput);
			if(elemCutOffTime == null){
				return null;
			}
		}
		String timeString = elemCutOffTime.getAttribute("CodeShortDescription");
		int hours = Integer.parseInt(timeString.substring(0, 2));
		int minutes = Integer.parseInt(timeString.substring(2));
		YDate today = new YDate(false);
		today.setHours(hours);
		today.setMinutes(minutes);
		today.setSeconds(0);
		return today;
	}

	/**
	 * 
	 * @param codeType
	 * @param codeValue
	 * @param docGetCommonCodeListOutput
	 * @return
	 */
	private YFCElement getCommonCodeFor(String codeType, String codeValue, YFCDocument docGetCommonCodeListOutput){
		if(!isValidCommonCodeList(docGetCommonCodeListOutput)){
			return null;
		}
		for(Iterator<YFCElement> itr = docGetCommonCodeListOutput.getDocumentElement().getChildren().iterator();itr.hasNext();){
			YFCElement elem = itr.next();
			String elmCodeType = elem.getAttribute("CodeType");
			String elmCodeValue = elem.getAttribute(TelstraConstants.CODE_VALUE);
			if(YFCObject.isVoid(elmCodeType) || YFCObject.isVoid(elmCodeValue)){
				continue;
			}
			if(elmCodeType.equalsIgnoreCase(codeType) && elmCodeValue.equalsIgnoreCase(codeValue)){
				LoggerUtil.verboseLog("BeforeCreateOrderUE :: getCommonCodeFor :: elem", logger, elem);
				return elem;
			}
		}
		return null;
	}

	/**
	 * 
	 * @param docGetCommonCodeListOutput
	 * @return
	 */
	private boolean isValidCommonCodeList(YFCDocument docGetCommonCodeListOutput){
		if(docGetCommonCodeListOutput == null || docGetCommonCodeListOutput.getDocumentElement() == null || docGetCommonCodeListOutput.getDocumentElement().getChildren() == null){
			return false;
		}
		return true;
	}

	/**
	 * 
	 * @param orderElem
	 * @param routeId
	 */
	private void setRouteId(YFCElement orderElem, String routeId){
		YFCElement elemOrderExtn = orderElem.getChildElement(TelstraConstants.EXTN, true);
		elemOrderExtn.setAttribute(TelstraConstants.ROUTE_ID, routeId);
	}

	/**
	 * 
	 * @param orderElem
	 * @return
	 */
	protected String getShipNode(YFCElement orderElem){
		// HUB-7096: Processing orders having Multiple ShipNodes and multi-DACs with V&C at order line level  - Start
		if("OrderLine".equals(orderElem.getNodeName())) {
			return orderElem.getAttribute(TelstraConstants.SHIP_NODE);
		}
		// HUB-7096: Processing orders having Multiple ShipNodes and multi-DACs with V&C at order line level  - End
		YFCElement elemOrderLines = orderElem.getChildElement("OrderLines");
		if(elemOrderLines == null){
			return null;
		}
		for(Iterator<YFCElement> itr = elemOrderLines.getChildren().iterator();itr.hasNext();){
			YFCElement elemOrderLine = itr.next();
			LoggerUtil.verboseLog("BeforeCreateOrderUE :: getShipNode :: shipNode", logger, elemOrderLine.getAttribute(TelstraConstants.SHIP_NODE));
			return elemOrderLine.getAttribute(TelstraConstants.SHIP_NODE);
		}
		return null;
	}

	/**
	 * 
	 * @param orderElem
	 * @return
	 */
	protected String getReceivingNode(YFCElement orderElem){
		// HUB-7096: Processing orders having Multiple ShipNodes and multi-DACs with V&C at order line level  - Start
		if("OrderLine".equals(orderElem.getNodeName())) {
			return orderElem.getAttribute(TelstraConstants.RECEIVING_NODE);
		}
		// HUB-7096: Processing orders having Multiple ShipNodes and multi-DACs with V&C at order line level  - End
		YFCElement elemOrderLines = orderElem.getChildElement("OrderLines");
		if(elemOrderLines == null){
			return null;
		}
		for(Iterator<YFCElement> itr = elemOrderLines.getChildren().iterator();itr.hasNext();){
			YFCElement elemOrderLine = itr.next();
			return elemOrderLine.getAttribute(TelstraConstants.RECEIVING_NODE);
		}
		return null;
	}

	/**
	 * 
	 * @param shipNode
	 * @param docGetCommonCodeListOutput
	 * @return
	 */
	private String getStateForShipNode(String shipNode,YFCDocument docGetCommonCodeListOutput){
		if(YFCObject.isVoid(shipNode)){
			return null;
		}
		YFCElement elemCommonCode =  getCommonCodeFor("DC_STATE_MAP", shipNode, docGetCommonCodeListOutput);
		if(elemCommonCode == null){
			return null;
		}
		LoggerUtil.verboseLog("BeforeCreateOrderUE :: getStateForShipNode :: state", logger, elemCommonCode.getAttribute("CodeShortDescription"));
		return elemCommonCode.getAttribute("CodeShortDescription");
	}

	/**
	 * 
	 * @param docOrderXml
	 * @return
	 */
	private Priority getOrderOrOrderLinePriority(YFCElement orderOrOrderLine){
		String priority = null;
		if(orderOrOrderLine.getNodeName().equals(TelstraConstants.ORDER_LINE)) {
			YFCElement extnEle = orderOrOrderLine.getChildElement(TelstraConstants.EXTN);
			if(!YFCObject.isVoid(extnEle)) {
				priority = extnEle.getAttribute(TelstraConstants.PRIORITY_CODE);
			}
		} else {
			priority = orderOrOrderLine.getAttribute(TelstraConstants.PRIORITY_CODE);
		}
		if(isUrgentPriority(priority)){
			return Priority.URGENT;
		}
		if(!YFCObject.isVoid(priority) && priority.equalsIgnoreCase(TelstraConstants.ORDER_PRIORITY_MEDICAL)){
			return Priority.MEDICAL;
		}
		String shipToId = orderOrOrderLine.getAttribute(TelstraConstants.SHIP_TO_ID);
		if(isHomeAddress(shipToId)){
			return Priority.STANDARD_HOME_DELIVERY;
		}
		return Priority.STANDARD;
	}

	private boolean isUrgentPriority(String priority){
		if(YFCObject.isVoid(priority)){
			return false;
		}
		//HUB-9333[START]
		/*Adding "URGENT" attribute value as a Urgent Priority Code*/
		if("Y".equalsIgnoreCase(priority) || "1".equalsIgnoreCase(priority) || "2".equalsIgnoreCase(priority) || "3".equalsIgnoreCase(priority) || "URGENT".equalsIgnoreCase(priority)){
			return true;
		}
		//HUB-9333[END]

		return false;
	}

	/**
	 * 
	 * @param orderElem
	 * @return
	 */
	private YFCElement getDACSchedule(YFCElement orderElem){
		LoggerUtil.verboseLog("OrderDateHelper :: getDACSchedule :: Begin",logger, null);
		String dac = orderElem.getAttribute(TelstraConstants.SHIP_TO_ID);
		String shipNode = getShipNode(orderElem);
		//[HUB-8525] - Begin
		YFCDocument getDACSchduleInput = YFCDocument.getDocumentFor("<DacSchedule/>");
		YFCElement yfcEleGetDACSchduleInputRoot = getDACSchduleInput.getDocumentElement();
		if(!YFCCommon.isStringVoid(shipNode)){
			yfcEleGetDACSchduleInputRoot.setAttribute("Node", shipNode);
		}
		if(!YFCCommon.isStringVoid(dac)){
			yfcEleGetDACSchduleInputRoot.setAttribute("DeliveryAddressCode", dac);
		}
		//[HUB-8525] - End
		LoggerUtil.verboseLog("OrderDateHelper :: getDACSchedule :: getDACSchduleInput", logger, getDACSchduleInput);

		YFCDocument dacScheduleOutput = invokeYantraService(TelstraConstants.API_GET_DAC_SCHEDULE, getDACSchduleInput);
		if(dacScheduleOutput == null || dacScheduleOutput.getDocumentElement() == null || dacScheduleOutput.getAttributes() == null){
			return null;
		}
		LoggerUtil.verboseLog("OrderDateHelper :: getDACSchedule :: dacScheduleOutput", logger, dacScheduleOutput);
		LoggerUtil.verboseLog("OrderDateHelper :: getDACSchedule :: End",logger, null);
		return dacScheduleOutput.getDocumentElement();
	}

	/**
	 * 
	 * @param routeId
	 * @param dateToWham
	 * @return
	 */
	private YFCElement getMilkRunSchedule(String routeId,YDate dateToWham){
		
		LoggerUtil.verboseLog("OrderDateHelper :: getMilkRunSchedule :: Begin",logger, null);
		
		YFCDocument milkRunSchedule = YFCDocument.getDocumentFor("<MilkRunSchedule RouteId='"+routeId.toUpperCase()+"' />");
		LoggerUtil.verboseLog("OrderDateHelper :: getMilkRunSchedule :: milkRunSchedule", logger, milkRunSchedule);

		YFCDocument milkRunScheduleOutput = invokeYantraService(TelstraConstants.API_GET_MILK_RUN_SCHEDULE, milkRunSchedule);
		if(milkRunScheduleOutput == null || milkRunScheduleOutput.getDocumentElement() == null || milkRunScheduleOutput.getDocumentElement().getAttributes() == null){
			LoggerUtil.verboseLog("OrderDateHelper :: getMilkRunSchedule :: End",logger, null);
			return null;
		}
		LoggerUtil.verboseLog("OrderDateHelper :: getMilkRunSchedule :: milkRunScheduleOutput", logger, milkRunScheduleOutput);

		YDate effectiveFrom = milkRunScheduleOutput.getDocumentElement().getYDateAttribute(TelstraConstants.EFFECTIVE_FROM);
		YDate effectiveTo = milkRunScheduleOutput.getDocumentElement().getYDateAttribute(TelstraConstants.EFFECTIVE_TO);
		if((effectiveFrom == null || dateToWham.after(effectiveFrom)) && (effectiveTo == null || dateToWham.before(effectiveTo))){
			LoggerUtil.verboseLog("OrderDateHelper :: getMilkRunSchedule :: End",logger, null);
			return milkRunScheduleOutput.getDocumentElement(); 
		}		
		LoggerUtil.verboseLog("OrderDateHelper :: getMilkRunSchedule :: End",logger, null);
		return null;
	}

	/**
	 * 
	 * @param shipDate
	 * @param schedule
	 * @param weekNumbers
	 * @param exceptionDates
	 * @param allDayShift
	 * @return
	 */
	private YDate getNextBusinessDayBasedOnSchedule(YDate shipDate,String schedule, YFCDocument weekNumbers, List<String> exceptionDates, YFCElement allDayShift, boolean forShipDate){
		
		LoggerUtil.verboseLog("OrderDateHelper :: getNextBusinessDayBasedOnSchedule :: Begin",logger, null);
		Schedule scheduleObj = new Schedule(schedule);
		YDate newShipDate = new YDate(shipDate.getString(TelstraConstants.TELSTRA_DATE_FORMAT), TelstraConstants.TELSTRA_DATE_FORMAT, true);
		//HUB-7283 - Stopping the below date modification if the method is called for Delivery Date computation - Start
		if(forShipDate) {
			YFCDateUtils.addHours(newShipDate,24);
		}
		//HUB-7283 - Stopping the below date modification if the method is called for Delivery Date computation - End
		int maxCount = 100000;
		int count = 0;

		while(true){
			count++;
			if(OrderDateHelper.isNonWorkingDay(newShipDate, allDayShift)){
				YFCDateUtils.addHours(newShipDate, 24);
			}else{
				boolean isValid;
				if(weekNumbers != null){
					int weekNumber = OrderDateHelper.getWeekNumber(weekNumbers, newShipDate);
					isValid = scheduleObj.isValidDate(weekNumber, newShipDate);
				}else{
					isValid = scheduleObj.isValidDay(newShipDate);
				}			
				if(isValid || count > maxCount){
					if(OrderDateHelper.isExceptionDay(newShipDate, exceptionDates)){
						newShipDate = OrderDateHelper.getNextBusinessDay(newShipDate, allDayShift, exceptionDates);
					}
					break;
				}
				YFCDateUtils.addHours(newShipDate, 24);
			}
		}
		if(count > maxCount){
			return null;
		}
		if(!YFCCommon.isVoid(newShipDate)){
			LoggerUtil.verboseLog("OrderDateHelper :: getNextBusinessDayBasedOnSchedule :: newShipDate", logger, newShipDate);

		}
		
		LoggerUtil.verboseLog("OrderDateHelper :: getNextBusinessDayBasedOnSchedule :: End",logger, null);
		return newShipDate;
	}

	/**
	 * 
	 * @param shipToId
	 * @return
	 */
	private boolean isHomeAddress(String shipToId){
		YFCDocument docGetCustomer = YFCDocument.getDocumentFor("<Customer CustomerID='"+shipToId+"' />");
		YFCDocument docGetCustomerOutput = invokeYantraApi(TelstraConstants.API_GET_CUSTOMER_LIST, docGetCustomer);
		LoggerUtil.verboseLog("OrderDateHelper :: isHomeAddress :: docGetCustomerOutput", logger, docGetCustomerOutput);

		if(docGetCustomerOutput != null && docGetCustomerOutput.getDocumentElement() != null){
			YFCElement elemCustomer = docGetCustomerOutput.getDocumentElement().getFirstChildElement();
			if(elemCustomer == null){
				return false;
			}
			String customerClassificationCode = elemCustomer.getAttribute(TelstraConstants.CUSTOMER_CLASSIFICATION_CODE);
			if(!YFCObject.isVoid(customerClassificationCode) && customerClassificationCode.equalsIgnoreCase(TelstraConstants.HOME_ADDRESS)){
				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 * @return
	 */
	private YFCDocument getWeekNumbers(){
		YFCDocument input = YFCDocument.getDocumentFor("<WeekNumber />");
		return invokeYantraService(TelstraConstants.API_GET_WEEK_NUMBER_LIST, input);
	}

	/**
	 * 
	 * @param node
	 * @return
	 */
	private YFCDocument getOrganizationHeirarchy(String node) {
		String orgXmlStr = "<Organization OrganizationCode='"+node+"' />";
		YFCDocument docOrgInput = YFCDocument.getDocumentFor(orgXmlStr);
		String orgTemplate = "<Organization LocaleCode=\"\" OrganizationCode=\"\"><CorporatePersonInfo State=\"\" City=\"\" /></Organization>";
		YFCDocument docOrgTemplate = YFCDocument.getDocumentFor(orgTemplate);
		YFCDocument docOrgOp = invokeYantraApi(TelstraConstants.API_GET_ORGANIZATION_HEIRARCHY, docOrgInput, docOrgTemplate);
		LoggerUtil.verboseLog("BeforeCreateOrderUE :: getOrganizationHeirarchy :: docOrgOp", logger, docOrgOp);

		return docOrgOp;
	}

	/**
	 * 
	 * @param docOrderXml
	 * @return
	 */
	private boolean requireOrderDateCalculations(YFCDocument docOrderXml){
		String entryType = docOrderXml.getDocumentElement().getAttribute(TelstraConstants.ENTRY_TYPE);
		if(YFCObject.isVoid(entryType)){
			return false;
		}
		//HUB-8462 -[START]

		if(entryType.equalsIgnoreCase(TelstraConstants.INTEGRAL_PLUS) || entryType.equalsIgnoreCase(TelstraConstants.VECTOR)
				|| entryType.equalsIgnoreCase(TelstraConstants.STERLING_SOURCE_SYSTEM)){
			return true;
		}
		//HUB-8462 -[END]

		return false;
	}

	/**
	 * 
	 * @param docOrderXml
	 * @return
	 */
	private boolean isIntegralOrder(YFCDocument docOrderXml){
		String entryType = docOrderXml.getDocumentElement().getAttribute(TelstraConstants.ENTRY_TYPE);
		if(YFCObject.isVoid(entryType)){
			return false;
		}
		//HUB-8462 -[START]

		if(entryType.equalsIgnoreCase(TelstraConstants.INTEGRAL_PLUS) || entryType.equalsIgnoreCase("STERLING")){
			return true;
		}

		//HUB-8462 -[END]

		return false;
	}

	/**
	 * 
	 * @param docOrderXml
	 * @return
	 */
	private boolean isVectorOrder(YFCDocument docOrderXml){
		String entryType = docOrderXml.getDocumentElement().getAttribute(TelstraConstants.ENTRY_TYPE);
		if(YFCObject.isVoid(entryType)){
			return false;
		}
		if(entryType.equalsIgnoreCase(TelstraConstants.VECTOR)){
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @param docOrderXml
	 * @param docGetCommonCodeListOutput
	 * @return
	 */
	private String getMaterialClassFromDepartmentCode(YFCDocument docGetCommonCodeListOutput){
		if(YFCObject.isVoid(departmentCode)){
			return null;
		}
		YFCElement elemCommonCode = getCommonCodeFor(TelstraConstants.DEPARTMENT_MATERIAL_CLASS_MAP, departmentCode, docGetCommonCodeListOutput);
		if(elemCommonCode == null){
			return null;
		}
		LoggerUtil.verboseLog("BeforeCreateOrderUE :: getMaterialClassFromDepartmentCode :: materialClass", logger, elemCommonCode.getAttribute("CodeShortDescription"));

		return elemCommonCode.getAttribute("CodeShortDescription");
	}

	/**
	 * 
	 * @param orderElem
	 * @return
	 */
	private String getReceivingState(YFCElement orderElem){
		// HUB-7096: Processing orders having Multiple ShipNodes and multi-DACs with V&C at order line level  - Start: Changed the Tag name from BillTo to ShipTo
		YFCElement personInfoShipToElem = orderElem.getChildElement(TelstraConstants.PERSON_INFO_SHIP_TO);
		if(personInfoShipToElem == null){
			return null;
		}
		LoggerUtil.verboseLog("BeforeCreateOrderUE :: getReceivingState :: recState", logger, personInfoShipToElem.getAttribute(TelstraConstants.STATE));
		return personInfoShipToElem.getAttribute(TelstraConstants.STATE);
	}

	/**
	 * 
	 * @param orderElem
	 * @return
	 */
	private boolean isHazmatOrderLine(YFCElement orderLine){

		YFCElement elemItem = orderLine.getChildElement(TelstraConstants.ITEM);
		if(elemItem == null){
			return false;
		}
		String itemId = elemItem.getAttribute(TelstraConstants.ITEM_ID);
		String unitOfMeasure = elemItem.getAttribute(TelstraConstants.UNIT_OF_MEASURE);
		if(YFCObject.isVoid(itemId)){
			return false;
		}
		if(isHazmatItem(itemId,unitOfMeasure) ){
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @param itemId
	 * @param unitOfMeasure
	 * @return
	 */
	private boolean isHazmatItem(String itemId,String unitOfMeasure){
		YFCElement elemItemPrimaryInfo = null;
		try{
			YFCDocument docItemDetails = getItemDetails(itemId,unitOfMeasure);
			if(docItemDetails == null || docItemDetails.getDocumentElement() == null){
				return false;
			}
			elemItemPrimaryInfo = docItemDetails.getDocumentElement().getChildElement("PrimaryInformation");
			if(elemItemPrimaryInfo == null){
				return false;
			}
		}catch(YFCException yfcEx){
			return false;
		}catch(YFSException yfsEx){
			return false;
		}
		return elemItemPrimaryInfo.getBooleanAttribute(TelstraConstants.IS_HAZMAT);
	}

	/**
	 * 
	 * @param itemId
	 * @param unitOfMeasure
	 * @return
	 */
	private YFCDocument getItemDetails(String itemId,String unitOfMeasure){
		YFCDocument docGetItemDetails = null;
		if(YFCObject.isVoid(unitOfMeasure)){
			docGetItemDetails = YFCDocument.getDocumentFor("<Item ItemID = '"+itemId+"' OrganizationCode='"+TelstraConstants.ORG_TELSTRA_SCLSA+"' />");
		}else{
			docGetItemDetails = YFCDocument.getDocumentFor("<Item ItemID = '"+itemId+"' OrganizationCode='"+TelstraConstants.ORG_TELSTRA_SCLSA+"' UnitOfMeasure='"+unitOfMeasure+"'/>");
		}
		return invokeYantraApi(TelstraConstants.API_GET_ITEM_DETAILS, docGetItemDetails);
	}

	/**
	 * 
	 * @param shipNodeState
	 * @return
	 */
	private YFCDocument getCalendarDocument(String shipNodeState){
		YFCDocument docGetCalendar = YFCDocument.getDocumentFor("<Calendar CalendarId='"+shipNodeState+"' OrganizationCode='"+TelstraConstants.ORG_TELSTRA_SCLSA+"'/>");
		YFCDocument calendarDoc =  getCalendarDocument(docGetCalendar);
		if((calendarDoc == null || calendarDoc.getDocumentElement() == null) && !TelstraConstants.DEFAULT_CALENDAR_STATE.equalsIgnoreCase(shipNodeState)){
			shipNodeState = TelstraConstants.DEFAULT_CALENDAR_STATE;
			docGetCalendar = YFCDocument.getDocumentFor("<Calendar CalendarId='"+shipNodeState+"' OrganizationCode='"+TelstraConstants.ORG_TELSTRA_SCLSA+"'/>");
			LoggerUtil.verboseLog("BeforeCreateOrderUE :: getCalendarDocument :: deliveryDate", logger, getCalendarDocument(docGetCalendar));

			YFCDocument docCalendarDoc = getCalendarDocument(docGetCalendar);
			LoggerUtil.verboseLog("BeforeCreateOrderUE :: getCalendarDocument :: docCalendarDoc", logger, docCalendarDoc);
			return docCalendarDoc;
		}
		LoggerUtil.verboseLog("BeforeCreateOrderUE :: getCalendarDocument :: deliveryDate", logger, calendarDoc);
		return calendarDoc; 
	}

	/**
	 * 
	 * @param inXml
	 */
	private void copyDateAttributesToLine(YFCDocument inXml){
		YFCElement orderElem = inXml.getDocumentElement();
		YDate reqShipDate = orderElem.getYDateAttribute(TelstraConstants.REQUIRED_SHIP_DATE);
		YDate reqDeliveryDate = orderElem.getYDateAttribute(TelstraConstants.REQUIRED_DELIVERY_DATE);

		YDate earliestReqDeliveryDate = new YDate("3099-01-01",TelstraConstants.DATE_TIME_FORMAT_2, true); //setting this to a high date
		YDate earliestReqShipDate = new YDate("3099-01-01",TelstraConstants.DATE_TIME_FORMAT_2, true); //setting this to a high date
		YDate previousLineReqDeliveryDate = null;
		YDate previousLineReqShipDate = null;
		Boolean bMultipleDeliveryDates = false;
		Boolean bMultipleShipDates = false;
		Boolean bMultipleShipToAddresses = false;
		YFCElement previousPersonInfoShipTo = null;
		YFCElement elemOrderLines = orderElem.getChildElement(TelstraConstants.ORDER_LINES);
		if(elemOrderLines == null || elemOrderLines.getChildren(TelstraConstants.ORDER_LINE) == null){
			return;
		}
		for(Iterator<YFCElement> itr = elemOrderLines.getChildren(TelstraConstants.ORDER_LINE);itr.hasNext();){
			YFCElement elemOrderLine = itr.next();
			YDate lineShipDate = elemOrderLine.getYDateAttribute(TelstraConstants.REQUIRED_SHIP_DATE);
			if(reqShipDate != null && lineShipDate == null){
				elemOrderLine.setAttribute(TelstraConstants.REQUIRED_SHIP_DATE, reqShipDate);
			}
			else if(reqShipDate == null && lineShipDate != null){
				//Fix introduced to identify that the order has multiple requested delivery dates at the line
				if(previousLineReqShipDate!=null && !bMultipleShipDates){
					if(!previousLineReqShipDate.equals(lineShipDate)){
						bMultipleShipDates = true;
					}
				}
				else{
					previousLineReqShipDate = lineShipDate;
				}

				if(earliestReqShipDate.after(lineShipDate)){
					earliestReqShipDate = lineShipDate;
					//This will stamp the earliest requested ship date from the order line to the order header
					//If the requested ship date at the order header is null
					orderElem.setAttribute(TelstraConstants.REQUIRED_SHIP_DATE, earliestReqShipDate);
				}
			}

			YDate lineDeliveryDate = elemOrderLine.getYDateAttribute(TelstraConstants.REQUIRED_DELIVERY_DATE);
			if(reqDeliveryDate != null && lineDeliveryDate == null){
				elemOrderLine.setAttribute(TelstraConstants.REQUIRED_DELIVERY_DATE, reqDeliveryDate);
			}
			else if(reqDeliveryDate == null && lineDeliveryDate != null){
				//Fix introduced to identify that the order has multiple requested delivery dates at the line
				if(previousLineReqDeliveryDate!=null && !bMultipleDeliveryDates){
					if(!previousLineReqDeliveryDate.equals(lineDeliveryDate)){
						bMultipleDeliveryDates = true;
					}
				}
				else{
					previousLineReqDeliveryDate = lineDeliveryDate;
				}

				if(earliestReqDeliveryDate.after(lineDeliveryDate)){
					earliestReqDeliveryDate = lineDeliveryDate;
					//This will stamp the earliest requested delivery date from the order line to the order header
					//If the requested delivery date at the order header is null
					orderElem.setAttribute(TelstraConstants.REQUIRED_DELIVERY_DATE, lineDeliveryDate);
				}
			}

			YFCElement personInfoShipTo = elemOrderLine.getElementsByTagName("PersonInfoShipTo").item(0);
			if(previousPersonInfoShipTo != null && !bMultipleShipToAddresses) {
				bMultipleShipToAddresses = comparePersonInfoShipToAttributes(personInfoShipTo, previousPersonInfoShipTo);
			} else {
				previousPersonInfoShipTo = personInfoShipTo;
			}
		}


		YFCElement orderExtensionElem = orderElem.createChild("CustomAttributes");
		if(bMultipleShipDates)
			orderExtensionElem.setAttribute("Boolean1", "Y");
		else
			orderExtensionElem.setAttribute("Boolean1", "N");

		if(bMultipleDeliveryDates)
			orderExtensionElem.setAttribute("Boolean2", "Y");
		else
			orderExtensionElem.setAttribute("Boolean2", "N");

		if(bMultipleShipToAddresses)
			orderExtensionElem.setAttribute("Boolean3", "N");
		else
			orderExtensionElem.setAttribute("Boolean3", "Y");
	}

	private Boolean comparePersonInfoShipToAttributes(YFCElement personInfoShipTo,
			YFCElement previousPersonInfoShipTo) {
		Map<String, String> previousPersonInfoShipToMap = new HashMap<String, String>();
		Map<String, String> personInfoShipToMap = new HashMap<String, String>();
		previousPersonInfoShipToMap = previousPersonInfoShipTo.getAttributes();
		personInfoShipToMap = personInfoShipTo.getAttributes();
		Set<Entry<String, String>> previousPersonInfoShipToMapSet = previousPersonInfoShipToMap.entrySet();
		List<Entry<String, String>> previousPersonInfoShipToMapSetList = new ArrayList<Entry<String, String>>(
				previousPersonInfoShipToMapSet);
		Collections.sort(previousPersonInfoShipToMapSetList, new Comparator<Entry<String, String>>() {
			public int compare(Entry<String, String> o1,
					Entry<String, String> o2) {
				return (o2.getValue()).compareTo(o1.getValue());
			}
		});

		Set<Entry<String, String>> personInfoShipToMapSet = personInfoShipToMap.entrySet();
		List<Entry<String, String>> personInfoShipToMapSetList = new ArrayList<Entry<String, String>>(
				personInfoShipToMapSet);
		Collections.sort(personInfoShipToMapSetList, new Comparator<Entry<String, String>>() {
			public int compare(Entry<String, String> o1,
					Entry<String, String> o2) {
				return (o2.getValue()).compareTo(o1.getValue());
			}
		});

		return personInfoShipToMap.equals(previousPersonInfoShipToMap);
	}

	private YFCDocument getCalendarDocument(YFCDocument docGetCalendar){
		try{
			return invokeYantraApi(TelstraConstants.API_GET_CALENDAR_DETAILS, docGetCalendar);
		}catch(YFSException ex){
			return null;
		}
	}

	private class Schedule{
		private String scheduleString;
		private int scheduleWeekNumber;
		private List<Integer> validWeekNumbers;
		private List<Integer> workingDays;

		Schedule(String scheduleString){
			this.scheduleString = scheduleString;
			String scheduleWeekNumberString = scheduleString.substring(0, 1);
			this.scheduleWeekNumber = Integer.parseInt(scheduleWeekNumberString);
			populateValidWeekNumbers();
			populateWeekDays();
		}

		private void populateValidWeekNumbers(){
			validWeekNumbers = new ArrayList<>();
			switch(scheduleWeekNumber){
			case 0:
				validWeekNumbers.add(1);
				validWeekNumbers.add(2);
				validWeekNumbers.add(3);
				validWeekNumbers.add(4);
				validWeekNumbers.add(5);
				validWeekNumbers.add(6);
				break;
			case 1:
				validWeekNumbers.add(1);
				validWeekNumbers.add(3);
				validWeekNumbers.add(5);
				break;
			case 2:
				validWeekNumbers.add(2);
				validWeekNumbers.add(4);
				validWeekNumbers.add(6);
				break;
			case 3:
				validWeekNumbers.add(3);
				break;
			case 4:
				validWeekNumbers.add(4);
				break;
			case 5:
				validWeekNumbers.add(5);
				break;
			default:
				validWeekNumbers.add(6);					
			}
		}

		private void populateWeekDays(){
			workingDays = new ArrayList<>();
			String scheduleWeekDayString = scheduleString.substring(1);
			for(int i=0;i<scheduleWeekDayString.length();i++){
				char weekDayChar = scheduleWeekDayString.charAt(i);
				switch(weekDayChar){
				case 'A' :
					workingDays.add(2);
					break;
				case 'B' :
					workingDays.add(3);
					break;
				case 'C' :
					workingDays.add(4);
					break;
				case 'D' :
					workingDays.add(5);
					break;
				case 'E' :
					workingDays.add(6);
					break;
				default:
					break;
				}
			}
		}

		boolean isValidDate(int weekNumber,YDate date){
			if(!validWeekNumbers.contains(weekNumber)){
				return false;
			}
			return isValidDay(date);
		}

		boolean isValidDay(YDate date){
			int dayOfWeek = date.getDayOfWeek();
			if(workingDays.contains(dayOfWeek)){
				return true;
			}
			return false;
		}
	}

	private enum Priority{
		URGENT,STANDARD,STANDARD_HOME_DELIVERY,MEDICAL,LOW
	}
}

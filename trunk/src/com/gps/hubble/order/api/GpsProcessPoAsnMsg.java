/***********************************************************************************************
 * File Name        : GpsProcessPoAsnMsg.java
 *
 * Description      : Custom API for ASN status update/upload for PO shipments (Both JMS and Excel upload)
 * 
 * Modification Log :
 * ---------------------------------------------------------------------------------------------
 * Ver #    Date        	  Author                 			 Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0						Karthikeyan Suruliappan    			Initial Version
 * 1.1		Jan 09,2017		Prateek Kumar						HUB-8083: Added a fix for a scenario where shipped (over shipped scenario) qty is greater than ordered qty.  
 * ---------------------------------------------------------------------------------------------
 **********************************************************************************************/

package com.gps.hubble.order.api;

import java.util.HashMap;
import java.util.Map;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.constants.TelstraErrorCodeConstants;
import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSException;

/**
 * Custom API for ASN status update/upload for PO shipments (Both JMS and Excel upload)
 * 
 * @author Karthikeyan Suruliappan
 * @version 1.0
 *
 * Extends AbstractCustomApi Class
 * 
 */

public class GpsProcessPoAsnMsg extends AbstractCustomApi{
	private static YFCLogCategory logger = YFCLogCategory.instance(ApplyDuplicateOrderHold.class);
	private HashMap<String, String> confirmShipAdditionalAtrMap = null;
	
	/**
	 * This is a first method which gets invoke from the service.
	 */
	public YFCDocument invoke(YFCDocument inXml) throws YFSException{

		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", inXml);
		YFCElement inEle = inXml.getDocumentElement();    
		validateInputXml(inEle);
		
		/*
		 * Calling getOrderList with the OrderName and DocumentType as PurchaseOrder
		 */
		YFCDocument outputGetOrdListForComQry = getOrderList(inEle);		
		//This method is no required anymore. It was only required for chained orders
		//YFCElement matchingPOEle = getMatchingPOEle(outputGetOrdListForComQry,inXml);

		YFCElement matchingPOEle = setShippedQtyForStandAlonePO(inEle,outputGetOrdListForComQry);

		changeOrderStatus(matchingPOEle);
		YFCDocument apiOutputDoc;

		//IF is for JMS upload shipments		
		/*
		 * If confirm shipment flag is passed as Y in the input, which means this request has come from the JMS queue.
		 * if the incoming shipment already exists in the order, then change shipment would be invoked otherwise new shipment would be created
		 */
		if(confirmShipAdditionalAtrMap.get(TelstraConstants.CONFIRM_SHIP).equalsIgnoreCase("Y")){
			if(XmlUtils.isVoid(confirmShipAdditionalAtrMap.get(TelstraConstants.SHIPMENT_NO))){
				apiOutputDoc = createShipment(matchingPOEle,inEle);
			}else{
				if(bShipmentBelongsToSameOrder(matchingPOEle)){
					apiOutputDoc = changeShipment(matchingPOEle,inEle);
				}else{
					apiOutputDoc= createShipment(matchingPOEle,inEle);
				}
			}
		}
		//ELSE is for Excel upload shipments
		else{
			if(XmlUtils.isVoid(confirmShipAdditionalAtrMap.get(TelstraConstants.SHIPMENT_NO))){
				if(shipmentAlreadyExists(matchingPOEle)){
					apiOutputDoc = changeShipment(matchingPOEle,inEle);
				}else{
					apiOutputDoc = createShipment(matchingPOEle,inEle);
				}
			}else{
				if(bShipmentBelongsToSameOrder(matchingPOEle)){
					apiOutputDoc = changeShipment(matchingPOEle,inEle);
				}else{
					apiOutputDoc= createShipment(matchingPOEle,inEle);
				}
			}
		}

		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", inXml);

		return apiOutputDoc;    
	}

	/**
	 * This method is to find the incoming shipment number is there in the system to process. 
	 * @param matchingPOEle
	 * @return
	 */
	private boolean shipmentAlreadyExists(YFCElement matchingPOEle) {

		String docType = "0005";
		String orderHdrKey = matchingPOEle.getAttribute(TelstraConstants.ORDER_HEADER_KEY);

		YFCDocument inApiDoc = YFCDocument.getDocumentFor("<Shipment  DocumentType='"+docType+"' OrderHeaderKey='"+orderHdrKey+"' " +
				"Status='"+getProperty("ShipmentCreatedStatus")+"'/>");
		YFCDocument apiTemplate = YFCDocument.getDocumentFor("<Shipments><Shipment OrderHeaderKey='' TrackingNo='' ShipmentKey='' OrderNo='' " +
				"Status='' ><Extn VendorShipNode=''  />" +
				"</Shipment></Shipments>");
		YFCDocument apiOutput = invokeYantraApi("getShipmentList", inApiDoc,apiTemplate);
		YFCNodeList<YFCElement> shipList = apiOutput.getDocumentElement().getElementsByTagName(TelstraConstants.SHIPMENT);
		int sSize = shipList.getLength();
		if(sSize == 0){
			return false;
		}

		String shipNode = "";
		String tNO="";
		if(confirmShipAdditionalAtrMap.containsKey(TelstraConstants.VENDOR_SHIP_NODE)){
			shipNode = confirmShipAdditionalAtrMap.get(TelstraConstants.VENDOR_SHIP_NODE);
		}
		if(confirmShipAdditionalAtrMap.containsKey(TelstraConstants.TRACKING_NO)){
			tNO = confirmShipAdditionalAtrMap.get(TelstraConstants.TRACKING_NO);
		} 

		for (YFCElement yfcElement : shipList) {
			String vShipNode = yfcElement.getChildElement("Extn",true).getAttribute(TelstraConstants.VENDOR_SHIP_NODE,"");           
			String stNo = yfcElement.getAttribute(TelstraConstants.TRACKING_NO,"");      
			String status = yfcElement.getAttribute("Status");
			if(shipNode.equalsIgnoreCase(vShipNode) && tNO.equalsIgnoreCase(stNo) && getProperty("ShipmentCreatedStatus").equalsIgnoreCase(status))
			{				
				confirmShipAdditionalAtrMap.put(TelstraConstants.SHIPMENT_KEY, yfcElement.getAttribute(TelstraConstants.SHIPMENT_KEY));
				return true;				
			}
		}

		return false;
	}

	/**
	 * This method is to changeShipment with the in coming shipment lines for the matching shipment found. 
	 * @param matchingPOEle
	 * @param inEle
	 * @return
	 */
	private YFCDocument changeShipment(YFCElement matchingPOEle, YFCElement inEle) {
		YFCDocument inApiDoc  = YFCDocument.getDocumentFor("<Shipment><ShipmentLines/></Shipment>");
		String ordHdrKeyPO = matchingPOEle.getAttribute(TelstraConstants.ORDER_HEADER_KEY);
		YFCElement inApiEle = inApiDoc.getDocumentElement();
		inApiEle.setAttribute(TelstraConstants.SHIPMENT_KEY,confirmShipAdditionalAtrMap.get(TelstraConstants.SHIPMENT_KEY));
		inApiEle.setAttribute(TelstraConstants.CONFIRM_SHIP,confirmShipAdditionalAtrMap.get(TelstraConstants.CONFIRM_SHIP));

		if(!XmlUtils.isVoid(confirmShipAdditionalAtrMap.get(TelstraConstants.ACTUAL_SHIPMENT_DATE))){
			inApiEle.setAttribute(TelstraConstants.ACTUAL_SHIPMENT_DATE,confirmShipAdditionalAtrMap.get(TelstraConstants.ACTUAL_SHIPMENT_DATE));
		}

		if(!XmlUtils.isVoid(confirmShipAdditionalAtrMap.get(TelstraConstants.EXPECTED_SHIPMENT_DATE))){
			inApiEle.setAttribute(TelstraConstants.EXPECTED_SHIPMENT_DATE,confirmShipAdditionalAtrMap.get(TelstraConstants.EXPECTED_SHIPMENT_DATE));
		}

		if(!XmlUtils.isVoid(confirmShipAdditionalAtrMap.get(TelstraConstants.SCAC))){
			//HUB 6801- [START]
			inApiEle.setAttribute(TelstraConstants.SCAC,confirmShipAdditionalAtrMap.get(TelstraConstants.SCAC).toUpperCase());
			//HUB 6801- [END]
		}

		YFCElement shipLinesEle = inApiEle.getChildElement("ShipmentLines");
		YFCNodeList<YFCElement> ordLineEleListPO = matchingPOEle.getElementsByTagName(TelstraConstants.ORDER_LINE);
		for (YFCElement yfcElement : ordLineEleListPO) {

			if(!XmlUtils.isVoid(yfcElement.getAttribute(TelstraConstants.SHIPPED_QTY))){
				YFCElement currShipLineEle = shipLinesEle.createChild(TelstraConstants.SHIPMENT_LINE);
				currShipLineEle.setAttribute(TelstraConstants.ORDER_LINE_KEY, yfcElement.getAttribute(TelstraConstants.ORDER_LINE_KEY));
				currShipLineEle.setAttribute(TelstraConstants.ORDER_HEADER_KEY,ordHdrKeyPO);
				currShipLineEle.setAttribute(TelstraConstants.QUANTITY, yfcElement.getAttribute(TelstraConstants.SHIPPED_QTY));
				currShipLineEle.setAttribute("Action", "Create");
			}

		}

		// set FromAddress
		if(!XmlUtils.isVoid(inEle.getChildElement(TelstraConstants.FROM_ADDRESS))){
			Map<String, String> fromAdrMap = inEle.getChildElement(TelstraConstants.FROM_ADDRESS).getAttributes();		
			if(!XmlUtils.isVoid(fromAdrMap)){
				inApiEle.createChild(TelstraConstants.FROM_ADDRESS).setAttributes(fromAdrMap);
			}
		}

		return invokeYantraApi("changeShipment", inApiDoc,YFCDocument.getDocumentFor("<Shipment ShipmentKey='' ShipmentNo='' />"));
	}

	/**
	 * This method is to find Shipment belong to same orderNo or not. 
	 * @param matchingPOEle
	 * @return
	 */
	private boolean bShipmentBelongsToSameOrder(YFCElement matchingPOEle) {

		String org = matchingPOEle.getAttribute("EnterpriseCode");
		String sellerOrg = matchingPOEle.getAttribute("SellerOrganizationCode");
		String shipmentNo = confirmShipAdditionalAtrMap.get(TelstraConstants.SHIPMENT_NO);
		String docType = "0005";
		String actualOrdHdrKey = matchingPOEle.getAttribute(TelstraConstants.ORDER_HEADER_KEY);

		YFCDocument inApiDoc = YFCDocument.getDocumentFor("<Shipment EnterpriseCode='"+org+"'  SellerOrganizationCode='"+sellerOrg+"' " +
				"ShipmentNo='"+shipmentNo+"' DocumentType='"+docType+"' />");
		YFCDocument apiTemplate = YFCDocument.getDocumentFor("<Shipments><Shipment OrderHeaderKey='' TrackingNo='' ShipmentKey='' OrderNo='' Status=''>" +
				"<Extn VendorShipNode=''/></Shipment></Shipments>");
		YFCDocument apiOutput = invokeYantraApi("getShipmentList", inApiDoc,apiTemplate);

		int shipmentSize = apiOutput.getDocumentElement().getElementsByTagName(TelstraConstants.SHIPMENT).getLength();
		if(shipmentSize == 0){
			return false;
		}

		String eVShipNode = XPathUtil.getXpathAttribute(apiOutput,"//Shipment/Extn/@VendorShipNode");
		String shipmentKey = XPathUtil.getXpathAttribute(apiOutput,"//Shipment/@ShipmentKey");
		String status = XPathUtil.getXpathAttribute(apiOutput,"//Shipment/@Status");
		String vShipNode = confirmShipAdditionalAtrMap.get(TelstraConstants.VENDOR_SHIP_NODE);
		String existingOrdHdrKey = XPathUtil.getXpathAttribute(apiOutput,"//Shipment/@OrderHeaderKey");
		String trackingNoFromShipment = XPathUtil.getXpathAttribute(apiOutput,"//Shipment/@TrackingNo");
		String trackingNoFromMap = confirmShipAdditionalAtrMap.get(TelstraConstants.TRACKING_NO);
		if( !existingOrdHdrKey.equalsIgnoreCase(actualOrdHdrKey) || getProperty(TelstraConstants.SHIPPED_STATUS_PO).equals(status) || 
				(!XmlUtils.isVoid(eVShipNode) && !XmlUtils.isVoid(vShipNode) && !eVShipNode.equalsIgnoreCase(vShipNode)) ||
				(!XmlUtils.isVoid(trackingNoFromShipment) && !XmlUtils.isVoid(trackingNoFromMap) && !trackingNoFromShipment.equalsIgnoreCase(trackingNoFromMap)) ){

			for(int i=0;;i++)
			{

				if(shipmentNo.contains(getProperty(TelstraConstants.SHIPMENT_SPLIT_CHAR))){
					String[] sArr = shipmentNo.split(getProperty(TelstraConstants.SHIPMENT_SPLIT_CHAR));					
					if(!XmlUtils.isVoid(sArr[1])){
						Integer iVar =  Integer.valueOf(sArr[1]);
						iVar++;
						shipmentNo = sArr[0]+getProperty(TelstraConstants.SHIPMENT_SPLIT_CHAR)+String.valueOf(iVar);
					}else{
						shipmentNo = shipmentNo+String.valueOf(i);
					}

				}else{
					shipmentNo = shipmentNo+getProperty(TelstraConstants.SHIPMENT_SPLIT_CHAR)+String.valueOf(i);
				}

				inApiDoc = YFCDocument.getDocumentFor("<Shipment EnterpriseCode='"+org+"'  SellerOrganizationCode='"+sellerOrg+"' " +
						"ShipmentNo='"+shipmentNo+"' DocumentType='"+docType+"' />");
				apiTemplate = YFCDocument.getDocumentFor("<Shipments><Shipment OrderHeaderKey='' TrackingNo='' ShipmentKey='' OrderNo='' Status='' >" +
						"<Extn/></Shipment>" +
						"</Shipments>");
				apiOutput = invokeYantraApi("getShipmentList", inApiDoc,apiTemplate);

				status = XPathUtil.getXpathAttribute(apiOutput, "//Shipment/@Status");
				String curShipNode = XPathUtil.getXpathAttribute(apiOutput, "//Shipment/Extn/@VendorShipNode");
				String curTNo = XPathUtil.getXpathAttribute(apiOutput, "//Shipment/@TrackingNo");
				existingOrdHdrKey = XPathUtil.getXpathAttributeWithDefaultValue(apiOutput,"//Shipment/@OrderHeaderKey","-1");

				if(apiOutput.getDocumentElement().getElementsByTagName(TelstraConstants.SHIPMENT).getLength()!=0){
					if(vShipNode.equalsIgnoreCase(curShipNode)){
						if(trackingNoFromMap.equalsIgnoreCase(curTNo)){
							if(status.equalsIgnoreCase(getProperty("ShipmentCreatedStatus"))){
								if(existingOrdHdrKey.equals(actualOrdHdrKey)){
									shipmentKey = XPathUtil.getXpathAttribute(apiOutput, "//Shipment/@ShipmentKey");
									confirmShipAdditionalAtrMap.put(TelstraConstants.SHIPMENT_KEY, shipmentKey);
									return true;
								}
							}
						}
					}
				}else{
					confirmShipAdditionalAtrMap.put(TelstraConstants.SHIPMENT_NO, shipmentNo);
					return false;
				}

			}

		}
		confirmShipAdditionalAtrMap.put(TelstraConstants.SHIPMENT_KEY,shipmentKey);
		return true;
	}


	/**
	 * This method is to changeOrderStatus for the non committed qty(s) to committed in case of more shipped qty(s). 
	 * @param matchingPOEle
	 */
	private void changeOrderStatus(YFCElement matchingPOEle) {

		YFCDocument inApiDoc = formChangeOrderStatusInputDoc(matchingPOEle);
		if(XmlUtils.isVoid(inApiDoc)){
			return;
		}
		invokeYantraApi("changeOrderStatus", inApiDoc);
	}

	/**
	 * This method is just forms changeOrderStatus input xml according to criteria met. 
	 * @param matchingPOEle
	 * @return
	 */
	private YFCDocument formChangeOrderStatusInputDoc(YFCElement matchingPOEle) {

		YFCNodeList<YFCElement> ordLineEle = matchingPOEle.getElementsByTagName(TelstraConstants.ORDER_LINE);
		YFCDocument inApiDoc = null;
		String comittedStatusCode = getProperty("ComittedStatusCode");
		double dbComittedStatusCode = Double.parseDouble(comittedStatusCode);
		inApiDoc = YFCDocument.getDocumentFor("<OrderStatusChange TransactionId='"+getProperty("MoveToComittedTranID")+"' " +
            "OrderHeaderKey='"+matchingPOEle.getAttribute(TelstraConstants.ORDER_HEADER_KEY)+"'><OrderLines/></OrderStatusChange >");
		for (YFCElement yfcElement : ordLineEle) {

			Double shippedQty = yfcElement.getDoubleAttribute(TelstraConstants.SHIPPED_QTY,0.0);
			Double comittedStsQty = 0.0;
			
			/*
			 * Loop through the order status for each line, retrieve the total quantity which can be moved to committed status i.e. all the 
			 * quantities which are less than committed status. If the shipped quantity is more than the already committed quantity then
			 * move the remaining possible quantity to commit status. If the shipped quantity is less than already commit quantities, then change order
			 * status will not be invoked and below logic will be skip
			 */
			double dQtyThatCanbeCommitted = 0.0;
			YFCNodeList<YFCNode> yfcnlOrderStatus = XPathUtil.getXpathNodeList(YFCDocument.getDocumentFor(yfcElement.toString()), "//OrderStatuses/OrderStatus");
			for(YFCNode yfcEleOrderStatus : yfcnlOrderStatus){

				double statusQty = ((YFCElement) yfcEleOrderStatus).getDoubleAttribute("StatusQty");
				double status = ((YFCElement) yfcEleOrderStatus).getDoubleAttribute("Status");
				if(status<dbComittedStatusCode){
					dQtyThatCanbeCommitted = dQtyThatCanbeCommitted+statusQty;
				}
				else if(status == dbComittedStatusCode){
					comittedStsQty = comittedStsQty + statusQty;
				}
			}

			if(0.0!=shippedQty && shippedQty>comittedStsQty && dQtyThatCanbeCommitted>0){
				YFCElement curOrdLineEle = inApiDoc.getDocumentElement().getChildElement("OrderLines").createChild(TelstraConstants.ORDER_LINE);
				curOrdLineEle.setAttribute("BaseDropStatus", comittedStatusCode);					
				curOrdLineEle.setAttribute(TelstraConstants.ORDER_LINE_KEY, yfcElement.getAttribute(TelstraConstants.ORDER_LINE_KEY));
				Double qtyToCommit;
				if(shippedQty>=(comittedStsQty+dQtyThatCanbeCommitted)){
					qtyToCommit = dQtyThatCanbeCommitted;
				}
				else{
					qtyToCommit = shippedQty - comittedStsQty;
				}					
				curOrdLineEle.setDoubleAttribute(TelstraConstants.QUANTITY, qtyToCommit);
			}	
		}
		return inApiDoc;
	}

	/**
	 *This method is set the shipped qty(s) for StandAlonePO which is used for forming create shipment/ change shipment / change order status api input
	 * in other method.
	 * @param inEle
	 * @param outputGetOrdListForComQry
	 * @return
	 */
	private YFCElement setShippedQtyForStandAlonePO(YFCElement inEle, YFCDocument outputGetOrdListForComQry) {

		YFCElement ordRootElePO = outputGetOrdListForComQry.getDocumentElement().getChildElement("Order");
		YFCNodeList<YFCElement> shipLineList = inEle.getElementsByTagName(TelstraConstants.SHIPMENT_LINE);

		for (YFCElement yfcElement : shipLineList) {
			String poLineNo = yfcElement.getAttribute("PrimeLineNo","0");
			String inEleItemID =yfcElement.getAttribute("ItemID");
			YFCElement matchingOrdLineEle = XPathUtil.getXPathElement(outputGetOrdListForComQry, 
					"//Order/OrderLines/OrderLine[@PrimeLineNo='"+poLineNo+"']");

			if(!XmlUtils.isVoid(matchingOrdLineEle)){
				String poLineItemId = XPathUtil.getXpathAttribute(YFCDocument.getDocumentFor(matchingOrdLineEle.toString()), "//Item/@ItemID");
				if(!inEleItemID.equals(poLineItemId)){
					//[HUB-6807] Begin
					/*
					 * For the PMC orders the vendor portal screens to show the Material No (captured as ItemDesc in the order line), 
					 * instead of the Project material Number (Vend_Db_No, captured as ItemId in the order line). Now the vendor 
					 * will pass the Material No in the excel upload. Also, the UI too is passing the same info.
					 * The back end logic has been enhanced to validate the ItemId in the createShipment input with the ItemId or the 
					 * ItemDesc in the order, and replace the ItemId in the createShipment with the ItemId from the order 
					 * (if it matches, else throw an exception).
					 *					  
					 */
					String sItemDesc = XPathUtil.getXpathAttribute(YFCDocument.getDocumentFor(matchingOrdLineEle.toString()), "//Item/@ItemDesc");
					if(!inEleItemID.equals(sItemDesc)){					
						throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.ASN_ITEMID_NOT_CORRECT, new YFSException());
					}
					yfcElement.setAttribute(TelstraConstants.ITEM_ID, sItemDesc);
					//[HUB-6807] End
				}
				String qty = yfcElement.getAttribute(TelstraConstants.QUANTITY);
				/*
				 * Setting shipped quantity at order line level in get order list op. This is used in other methods to know how many quantities are getting shipped for this
				 * line. If this was not done, we will have to again compare the order line with the incoming ASN line to know the shipped quantities
				 */
				matchingOrdLineEle.setAttribute(TelstraConstants.SHIPPED_QTY, qty);
			}else{
				throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.ASN_SHIPMENT_LINE_NOT_CORRECT, new YFSException());
			}
		}
		return ordRootElePO;
	}




	/**
	 * This method is to createShipment for JMS shipments as well for Excel upload shipments according to shipment status.
	 * @ 
	 * @param matchingPOEle
	 * @param inEle
	 * @return
	 */
	private YFCDocument createShipment(YFCElement matchingPOEle, YFCElement inEle) {

		YFCDocument inApiDoc  = YFCDocument.getDocumentFor("<Shipment><ShipmentLines/></Shipment>");
		String ordHdrKeyPO = matchingPOEle.getAttribute(TelstraConstants.ORDER_HEADER_KEY);
		YFCElement apiInEle = inApiDoc.getDocumentElement();
		YFCElement shipLinesEle = apiInEle.getChildElement("ShipmentLines");
		inApiDoc.getDocumentElement().setAttribute(TelstraConstants.ORDER_HEADER_KEY, ordHdrKeyPO);
		YFCNodeList<YFCElement> purchOrdLineNL = matchingPOEle.getElementsByTagName(TelstraConstants.ORDER_LINE);

		for (YFCElement purchOrdLineEle : purchOrdLineNL) {
			if(!XmlUtils.isVoid(purchOrdLineEle.getAttribute(TelstraConstants.SHIPPED_QTY))){
				YFCElement currShipLineEle = shipLinesEle.createChild(TelstraConstants.SHIPMENT_LINE);
				currShipLineEle.setAttribute(TelstraConstants.ORDER_LINE_KEY, purchOrdLineEle.getAttribute(TelstraConstants.ORDER_LINE_KEY));
				currShipLineEle.setAttribute(TelstraConstants.ORDER_HEADER_KEY,ordHdrKeyPO);
				currShipLineEle.setAttribute(TelstraConstants.QUANTITY, purchOrdLineEle.getAttribute(TelstraConstants.SHIPPED_QTY));
			}
		}

		if(!XmlUtils.isVoid(inEle.getChildElement(TelstraConstants.EXTN))){

			/*
			 * The extn element will contain vendor ship node and will come from the UI.
			 */
			Map<String, String> fromAdrMap = inEle.getChildElement(TelstraConstants.EXTN).getAttributes();		
			if(!XmlUtils.isVoid(fromAdrMap)){
				apiInEle.createChild(TelstraConstants.EXTN).setAttributes(fromAdrMap);
			}
		}

		if(confirmShipAdditionalAtrMap.size() != 0){
			inApiDoc.getDocumentElement().setAttributes(confirmShipAdditionalAtrMap);
		}

		// set FromAddress
		if(!XmlUtils.isVoid(inEle.getChildElement(TelstraConstants.FROM_ADDRESS))){
			Map<String, String> fromAdrMap = inEle.getChildElement(TelstraConstants.FROM_ADDRESS).getAttributes();		
			if(!XmlUtils.isVoid(fromAdrMap)){
				apiInEle.createChild(TelstraConstants.FROM_ADDRESS).setAttributes(fromAdrMap);
			}
		}

		//HUB-7083 [Start]

		// set ToAddress
		/*
		 * The ship to address in the order comes from the order batch file. The address of Z1 node is different from the address in the order. 
		 * Sterling copies the receiving node's address from the order to the shipment. To fix the issue we are copying address in the order to the shipment
		 */
		YFCElement eleToAddress = getToAddress(matchingPOEle);
		if(!YFCObject.isNull(eleToAddress)){
			apiInEle.importNode(eleToAddress);
			YFCElement eleShipmentToAddress = inApiDoc.getDocumentElement().getElementsByTagName(TelstraConstants.PERSON_INFO_SHIP_TO).item(0);
			inApiDoc.getDocument().renameNode(eleShipmentToAddress.getDOMNode(),null, "ToAddress");
		}
		//HUB-7083 [End]

		YFCDocument apiOutDoc = invokeYantraApi("createShipment", inApiDoc,getCreateShipmentTemplate());
		apiOutDoc.getDocumentElement().setAttribute(TelstraConstants.CONFIRM_SHIP, "Y");

		return apiOutDoc;
	}

	//HUB-7083 [Start]


	/**
	 * This method returns the create shipment op template
	 * @return template
	 */
	private YFCDocument getCreateShipmentTemplate() {
		YFCDocument template = YFCDocument.getDocumentFor("<Shipment EnterpriseCode='' DocumentType='' ReceivingNode='' ShipmentKey='' Status='' ShipmentNo=''>"
				+ "<ShipmentLines><ShipmentLine Quantity='' ShipmentLineKey='' ItemID='' ReceivedQuantity='' PrimeLineNo=''><OrderLine PrimeLineNo=''/></ShipmentLine></ShipmentLines></Shipment>");
		return template;
	}

	/**
	 * This method returns the ship to address in an order. This address is stamp as ToAddress in the a shipment
	 * @param matchingPOEle
	 * @return
	 */
	private YFCElement getToAddress(YFCElement matchingPOEle) {
		//Stamp the personInfoShip element at header level, if not found then stamp the person info ship to at the line level as the Toaddress in the shipment

		YFCDocument docmatchingPOEle = YFCDocument.getDocumentFor(matchingPOEle.toString());
		YFCElement eleToAddress=docmatchingPOEle.getDocumentElement();
		YFCElement eleOrderLine = eleToAddress.getElementsByTagName(TelstraConstants.ORDER_LINE).item(0);
		YFCElement elePersonInfoShipTo = eleOrderLine.getChildElement(TelstraConstants.PERSON_INFO_SHIP_TO);
		if(!YFCObject.isNull(elePersonInfoShipTo)){
			Map<String, String> mapToattibutessize = elePersonInfoShipTo.getAttributes();
			if(mapToattibutessize.size()==0){
				elePersonInfoShipTo = eleToAddress.getChildElement(TelstraConstants.PERSON_INFO_SHIP_TO);
			}
		}
		return elePersonInfoShipTo;
	}

	//HUB-7083 [End]

	/**
	 * This method is get the order list for the order name got from input message.
	 * @param inEle
	 * @return
	 */
	private YFCDocument getOrderList(YFCElement inEle) {

		YFCDocument inDocgetOrdList = YFCDocument.getDocumentFor("<Order DocumentType='0005' OrderName='"+inEle.getAttribute("OrderName")+"'/>");
		//HUB-7083 [Start]

		YFCDocument templateForGetOrdList = YFCDocument.getDocumentFor("<OrderList>" +
				"<Order DocumentType='' SellerOrganizationCode='' OrderHeaderKey='' HoldFlag=''  OrderNo='' EnterpriseCode='' >" +
				"<PersonInfoShipTo AddressLine1='' AddressLine2='' AddressLine3='' AddressLine4='' AddressLine5='' AddressLine6='' City='' Company='' Country='' EMailID='' State='' ZipCode='' DayPhone='' LastName='' FirstName='' />" +
				"<OrderLines>" +
				"<OrderLine PrimeLineNo='' SubLineNo='' OrderLineKey='' OrderHeaderKey=''> " +
				"<Item ItemID='' ItemDesc=''/>" +
				"<OrderStatuses>" +
				"<OrderStatus OrderReleaseKey='' Status='' StatusQty=''/>" +
				"</OrderStatuses>" +
				"<ChainedFromOrderLine PrimeLineNo='' OrderLineKey='' ChainedFromOrderHeaderKey=''/>" +	
				"<PersonInfoShipTo AddressLine1='' AddressLine2='' AddressLine3='' AddressLine4='' AddressLine5='' AddressLine6='' City='' Company='' Country='' EMailID='' State='' ZipCode='' DayPhone='' LastName='' FirstName='' />" +
				"</OrderLine>" +
				"</OrderLines>" +
				"</Order>" +
				"</OrderList>");		

		//HUB-7083 [End]

		YFCDocument outputGetOrdListForComQry =  invokeYantraApi("getOrderList", inDocgetOrdList,templateForGetOrdList);   
		int oSize=  outputGetOrdListForComQry.getDocumentElement().getElementsByTagName("Order").getLength();
		if(oSize == 0){
		  //Checking for Supplier Order Number
		  inDocgetOrdList = YFCDocument.getDocumentFor("<Order DocumentType='0005' CustomerPONo='"+inEle.getAttribute("OrderName")+"'/>");
		  outputGetOrdListForComQry =  invokeYantraApi("getOrderList", inDocgetOrdList,templateForGetOrdList); 
		  oSize=  outputGetOrdListForComQry.getDocumentElement().getElementsByTagName("Order").getLength();
		  if(oSize == 0){
		    throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.ASN_ORD_CHECK, new YFSException());           
		  }
		}
		return outputGetOrdListForComQry;    
	}

	/**
	 * This method is validates input message for mandatory fields.
	 * @param inEle
	 */
	private void validateInputXml(YFCElement inEle) {

		String ordName = inEle.getAttribute("OrderName", "");
		boolean validInputFlg = true;
		YFCNodeList < YFCElement > yfcLineList = inEle.getElementsByTagName(getProperty("LineName"));
		for (YFCElement yfcElement: yfcLineList) {
			String plNo = yfcElement.getAttribute("PrimeLineNo", "");
			String iID = yfcElement.getAttribute("ItemID", "");

			Double dQty = new Double(yfcElement.getDoubleAttribute(TelstraConstants.QUANTITY,0.0));

			if(XmlUtils.isVoid(plNo) || XmlUtils.isVoid(iID)){
				validInputFlg = false;
				break;
			}
			//HUB-6186[Begin]
			if (XmlUtils.equals(dQty, 0.0)) {
				LoggerUtil.errorLog("ASN quantity invalid", logger, dQty);
				throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.ASN_INVALID_QUANTITY_ERROR_CODE,
						new YFSException());
			}

			yfcElement.setDoubleAttribute("StatusQuantity", dQty);
			//HUB-6186[End]
		}

		if (!validInputFlg || (yfcLineList.getLength() == 0) || XmlUtils.isVoid(ordName)) {
			throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.ASN_MANDATORY_ERR, new YFSException());
		}

		formAdditionalConfirmShipAtrMap(inEle);
	}

	/**
	 * This method is to set the additional fields that are got from input message which will be passed in the shipment creation/modification.
	 * @param inEle
	 */
	private void formAdditionalConfirmShipAtrMap(YFCElement inEle) {

		confirmShipAdditionalAtrMap = new HashMap<String,String>();
		String actualShipDate = inEle.getAttribute(TelstraConstants.ACTUAL_SHIPMENT_DATE);
		setConfirmShipAdditionalAtrMap(TelstraConstants.ACTUAL_SHIPMENT_DATE,actualShipDate);

		String vShipNode = inEle.getChildElement("Extn",true).getAttribute(TelstraConstants.VENDOR_SHIP_NODE,"");
		setConfirmShipAdditionalAtrMap(TelstraConstants.VENDOR_SHIP_NODE,vShipNode);

		String trackingNo = inEle.getAttribute(TelstraConstants.TRACKING_NO);
		setConfirmShipAdditionalAtrMap(TelstraConstants.TRACKING_NO,trackingNo);

		String s1 = inEle.getAttribute(TelstraConstants.CONFIRM_SHIP, "Y");
		setConfirmShipAdditionalAtrMap(TelstraConstants.CONFIRM_SHIP,s1);

		String expectedShipmentDate = inEle.getAttribute(TelstraConstants.EXPECTED_SHIPMENT_DATE);
		setConfirmShipAdditionalAtrMap(TelstraConstants.EXPECTED_SHIPMENT_DATE,expectedShipmentDate);

		String shipmentNo = inEle.getAttribute(TelstraConstants.SHIPMENT_NO);
		setConfirmShipAdditionalAtrMap(TelstraConstants.SHIPMENT_NO,shipmentNo);

		String scac = inEle.getAttribute(TelstraConstants.SCAC);
		//HUB 6801- [START]
		if(!YFCCommon.isStringVoid(scac)){
			setConfirmShipAdditionalAtrMap(TelstraConstants.SCAC,scac.toUpperCase());
		}
		//HUB 6801- [END]
	}

	/**
	 * 
	 * @param atrName
	 * @param aStrValue
	 */
	private void setConfirmShipAdditionalAtrMap(String atrName,String aStrValue ) {
		if(!XmlUtils.isVoid(aStrValue)){
			confirmShipAdditionalAtrMap.put(atrName, aStrValue);
		}
	}
}
package com.gps.hubble.order.api;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.date.YTimestamp;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSException;

/**
 * Custom API for Order Status Summary for Vendor Portal Dashboard. This api summarizes the orders for a vendor by status.
 * 
 * @author Vinay Bhanu
 * @version 1.0
 *
 * Extends AbstractCustomApi Class
 * 
 */

public class OrsVendorSummaryViewApi extends AbstractCustomApi{
	private static YFCLogCategory logger = YFCLogCategory.instance(OrsVendorSummaryViewApi.class);
	@Override
	public YFCDocument invoke(YFCDocument inXml) throws YFSException {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", inXml);
		Map<String, String> attributeColumnMap = getAttributeColumnMap();
		//TODO: Need to double check if this is correct
		//Set<String> selectAttributes = getSelectAttributes(inXml,attributeColumnMap);
		Set<String> selectAttributes=new HashSet<>();
		String attri = getProperty("GroupByAttributes",true);


		Set<String> allowedStatuses = new HashSet<>();
		String allowedStatus1 = getProperty("AllowedStatus1",true);
		allowedStatuses.add(allowedStatus1);
		String allowedStatus2 = getProperty("AllowedStatus2",true);
		allowedStatuses.add(allowedStatus2);
		String allowedStatus3 = getProperty("AllowedStatus3",true);
		allowedStatuses.add(allowedStatus3);


		if(!XmlUtils.isVoid(attri)){
			if(!attri.contains("NoOfOrders")){
				YFCDocument erroDoc = ExceptionUtil.getYFSExceptionDocument("TEL_ERR_13061_001","Aggregate Attribute Not Provided.");
				throw new YFSException(erroDoc.toString());	
			}
			String[] s = attri.split(",");
			for (String string : s) {
				selectAttributes.add(string);
			}
		}

		if(!selectAttributes.isEmpty()){
			String sellerOrganizationCode = getSellerOrganizationCode(inXml);
			String query = getQuery(sellerOrganizationCode,selectAttributes,attributeColumnMap);
			logger.debug("query is "+query);
			Connection connection = getDBConnection();
			YFCDocument docOutput = null;
			try (PreparedStatement ps = createPS(connection,query,sellerOrganizationCode); ResultSet rs = ps.executeQuery()) {

				docOutput =  constructDocument(rs,selectAttributes, attributeColumnMap,allowedStatuses);

			}catch(SQLException sqlEx){
				logger.error("SqlException while execuring query", sqlEx);
				throw new YFCException(sqlEx);
			}

			LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", docOutput);
			return docOutput;
		}else{
			YFCDocument docOutput =  invokeYantraService("GpsGetOrsVendorSummaryList", inXml);
			LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", docOutput);
			return docOutput;
		}

	}

	private PreparedStatement createPS(Connection connection,String query,String sellerOrganizationCode) throws SQLException {
		PreparedStatement ps = connection.prepareStatement(query);
		if(!YFCObject.isVoid(sellerOrganizationCode)){
			ps.setString(1, sellerOrganizationCode);
		}
		return ps;

	}

	private Set<String> getSelectAttributes(YFCDocument inXml,Map<String, String> attributeColumnMap){
		Set<String> selectAttributes = new HashSet<>();
		if(inXml.getDocumentElement() == null || inXml.getDocumentElement().getChildElement("Attributes") == null){
			return selectAttributes;
		}
		YFCElement elemSelect = inXml.getDocumentElement().getChildElement("Attributes");
		if(elemSelect.getChildren("Attribute") == null){
			return selectAttributes;
		}
		for(Iterator<YFCElement> itr = elemSelect.getChildren("Attribute").iterator();itr.hasNext();){
			YFCElement elemAttribute = itr.next();
			String attribute = elemAttribute.getAttribute("Name");
			if(!attributeColumnMap.containsKey(attribute)){
				throw new YFCException("Invalid Attribute Specified"); //TODO set proper error code
			}
			selectAttributes.add(attribute);
		}
		return selectAttributes;
	}

	private String getQuery(String sellerOrganizationCode,Set<String> selectAttributes,Map<String, String> attributeColumnMap){
		StringBuilder builder = new StringBuilder();
		builder.append(getSelect(selectAttributes, attributeColumnMap));
		builder.append(" FROM GPS_ORS_VENDOR_SUMMARY ");
		builder.append(getWhere(sellerOrganizationCode));
		builder.append(getGroupBy(selectAttributes, attributeColumnMap));
		return builder.toString();
	}

	private String getSelect(Set<String> selectAttributes,Map<String, String> attributeColumnMap){
		StringBuilder builder = new StringBuilder();
		builder.append("SELECT ");
		boolean isFirst = true;
		for(String attribute : selectAttributes){
			if(!isFirst){
				builder.append(",");
			}
			isFirst = false;
			String columnName = attributeColumnMap.get(attribute);
			if(attribute.equals("NoOfOrders")){
				builder.append("SUM(NO_OF_ORDERS) AS NO_OF_ORDERS");
			}else if(attribute.equals("TotalAmount")){
				builder.append("SUM(TOTAL_AMOUNT) AS TOTAL_AMOUNT");
			}else if(attribute.equals("UpdatedDate")){
				builder.append("MAX(CREATETS) AS UPDATED_DATE");
			}else{
				builder.append(columnName);
			}
		}
		return builder.toString();
	}

	private String getWhere(String sellerOrganizationCode){
		if(YFCObject.isVoid(sellerOrganizationCode)){
			return "";
		}
		return " WHERE SELLER_ORGANIZATION_CODE = ? ";
	}

	private String getSellerOrganizationCode(YFCDocument inXml){
		YFCElement elem = inXml.getDocumentElement();
		if(elem == null){
			return null;
		}
		String sellerOrganizationCode = elem.getAttribute("SellerOrganizationCode");
		return sellerOrganizationCode;
	}

	private String getGroupBy(Set<String> selectAttributes,Map<String, String> attributeColumnMap){
		StringBuilder builder = new StringBuilder();
		builder.append("GROUP BY ");
		boolean isFirst = true;
		for(String attribute : selectAttributes){
			String columnName = attributeColumnMap.get(attribute);
			if(!attribute.equals("NoOfOrders") && !attribute.equals("TotalAmount") && !attribute.equals("UpdatedDate")){
				if(!isFirst){
					builder.append(",");
				}
				builder.append(columnName);
				isFirst = false;
			}

		}
		return builder.toString();
	}

	private Map<String, String> getAttributeColumnMap(){
		Map<String, String> attributeMap = new HashMap<String, String>();
		attributeMap.put("OrderType", "ORDER_TYPE");
		attributeMap.put("DocumentType", "DOCUMENT_TYPE");
		attributeMap.put("SellerOrganizationCode", "SELLER_ORGANIZATION_CODE");
		attributeMap.put("Status", "STATUS");
		attributeMap.put("StatusDescription", "STATUS_DESC");
		attributeMap.put("NoOfOrders", "NO_OF_ORDERS");
		attributeMap.put("UpdatedDate", "UPDATED_DATE");
		attributeMap.put("TotalAmount", "TOTAL_AMOUNT");
		return attributeMap;
	}


	private YFCDocument constructDocument(ResultSet rs, Set<String> attributes, Map<String, String> attributeColumnMap, Set<String> allowedStatuses) throws SQLException{ 
		YFCDocument docOrsSummaryList = YFCDocument.createDocument("OrsSummaryList");
		YFCElement elemOrsSummaryList = docOrsSummaryList.getDocumentElement();
		Set<String> missingStatuses = new HashSet<String>();
		missingStatuses.addAll(allowedStatuses);
		String sellerOrg=null;
		YTimestamp updatedDate= null;
		
		while(rs.next()){
			
			Timestamp updatedSqlDate = rs.getTimestamp("UPDATED_DATE");
			updatedDate= YTimestamp.newMutableTimestamp(updatedSqlDate);
			String status = rs.getString("STATUS").trim();
			if(allowedStatuses.contains(status)) {
				YFCElement elemOrsSummary = elemOrsSummaryList.createChild("OrsSummary");
				if(attributes.contains("OrderType")){
					String orderType = rs.getString("ORDER_TYPE");
					elemOrsSummary.setAttribute("OrderType", orderType);
				}
				if(attributes.contains("DocumentType")){
					String documentType = rs.getString("DOCUMENT_TYPE");
					elemOrsSummary.setAttribute("DocumentType", documentType);
				}			
				if(attributes.contains("SellerOrganizationCode")){
					sellerOrg = rs.getString("SELLER_ORGANIZATION_CODE");
					elemOrsSummary.setAttribute("SellerOrganizationCode", sellerOrg);
				}
				if(attributes.contains("Status")){
					elemOrsSummary.setAttribute("Status", status);
				}
				if(attributes.contains("StatusDescription")){
					String statusDesc = rs.getString("STATUS_DESC");
					elemOrsSummary.setAttribute("StatusDescription", statusDesc);
				}			
				if(attributes.contains("NoOfOrders")){
					long noOfOrders = rs.getLong("NO_OF_ORDERS");
					elemOrsSummary.setAttribute("NoOfOrders", noOfOrders);
				}
				if(attributes.contains("TotalAmount")){
					double totalAmount = rs.getDouble("TOTAL_AMOUNT");
					elemOrsSummary.setAttribute("TotalAmount", totalAmount);
				}
				if(attributes.contains("UpdatedDate")){
					elemOrsSummary.setDateTimeAttribute("UpdatedDate", updatedDate);
				}	
				
				missingStatuses.remove(status);
			}
		}
		
		if(!missingStatuses.isEmpty()) {
			for(Iterator<String> itr=missingStatuses.iterator();itr.hasNext();) {
				String missingStatus = itr.next();
				YFCElement elemOrsSummary = elemOrsSummaryList.createChild("OrsSummary");
				elemOrsSummary.setAttribute("Status", missingStatus);
				elemOrsSummary.setAttribute("SellerOrganizationCode", sellerOrg);
				String sStatusInput= "<Status Status= '"+missingStatus+"' ProcessTypeKey='PO_FULFILLMENT'/>";
				YFCDocument statusInput = YFCDocument.getDocumentFor(sStatusInput);
				YFCDocument statusListDoc = invokeYantraApi("getStatusList", statusInput);
				if(statusListDoc!= null) {
					YFCElement statusElem = statusListDoc.getDocumentElement().getFirstChildElement();
					if(statusElem!=null) {
						elemOrsSummary.setAttribute("StatusDescription",statusElem.getAttribute("Description"));
					}
				}
				elemOrsSummary.setAttribute("NoOfOrders", 0);
				if(updatedDate == null) {
					elemOrsSummary.setDateTimeAttribute("UpdatedDate",YTimestamp.newTimestamp()); 
				} else {
					elemOrsSummary.setDateTimeAttribute("UpdatedDate", updatedDate);
				}
				
			}
		}
		
		return docOrsSummaryList;
	}

}

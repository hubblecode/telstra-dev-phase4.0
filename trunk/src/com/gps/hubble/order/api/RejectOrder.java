/***********************************************************************************************
 * File Name        : RejectOrder.java
 *
 * Description      : Custom API for processing Cancellation Accepted from WMS
 * 
 * Modification Log :
 * ---------------------------------------------------------------------------------------------
 * Ver #	Date			Author				Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0		18 May, 2017	Prateek Kumar		Initial Version
 * ---------------------------------------------------------------------------------------------
 * Copyright (c) IBM Australia Limited 2017. All rights reserved.
 **********************************************************************************************/
package com.gps.hubble.order.api;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.constants.TelstraErrorCodeConstants;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSException;

/**
 * Custom API for WMS status update for rejection of an order
 * 
 * @author Prateek Kumar
 * @version 1.0
 *
 *          Extends AbstractCustomApi Class
 * 
 *          sample input
 * 
	<Shipment ServiceName="Service Rejected/Cancellation Accepted" OrderName="Vector Id">
		<ShipmentLines>
			<ShipmentLine PrimeLineNo="1(use the vector id to identify the line number)" SubLineNo="1" ItemID="ignore the item id passed here" 
			UnitOfMeasure="ignore the uom passed here" Quantity="only cancel the qty passed here">
			</ShipmentLine>
		</ShipmentLines>
	</Shipment>
 * 
 */

public class RejectOrder extends AbstractCustomApi {

	private static YFCLogCategory logger = YFCLogCategory.instance(RejectOrder.class);

	@Override
	public YFCDocument invoke(YFCDocument inXml) throws YFSException {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", inXml);

		YFCElement yfcEleInRoot = inXml.getDocumentElement();
		String sServiceName = yfcEleInRoot.getAttribute(TelstraConstants.SERVICE_NAME);

		String sVectorID = yfcEleInRoot.getAttribute("OrderName");

		YFCDocument yfcDocGetOrderLineListOp = callGetOrderLineList(sVectorID);

		int iOrderLineLength = yfcDocGetOrderLineListOp.getElementsByTagName(TelstraConstants.ORDER_LINE)
				.getLength();
		if (iOrderLineLength == 0) {
			throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.REJECT_ORDER_NO_ORDER_LINE_FOR_VECTOR_ID,
					new YFSException());
		} else if (iOrderLineLength > 1) {
			throw ExceptionUtil.getYFSException(
					TelstraErrorCodeConstants.REJECT_ORDER_MULTIPLE_ORDER_LINES_WITH_SAME_VECTOR_ID,
					new YFSException());
		}

		if("Service Rejected".equals(sServiceName)){
			String sRejectionReason = yfcEleInRoot.getAttribute(TelstraConstants.REJECTION_REASON);
			processServiceRejection(yfcDocGetOrderLineListOp,sRejectionReason);
		}
		else if("Cancellation Accepted".equals(sServiceName)){
			cancelOrderLine(yfcEleInRoot, yfcDocGetOrderLineListOp);
		}

		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", inXml);
		return inXml;
	}

	/**
	 * 
	 * @param yfcDocGetOrderLineListOp
	 * @param sRejectionReason 
	 */
	private void processServiceRejection(YFCDocument yfcDocGetOrderLineListOp, String sRejectionReason) {

		YFCElement yfcEleOrderLine =  yfcDocGetOrderLineListOp.getElementsByTagName(TelstraConstants.ORDER_LINE).item(0);
		String sOrderReleaseKey = XPathUtil.getXpathAttribute(yfcDocGetOrderLineListOp, "//OrderStatus[@Status='3200']/@OrderReleaseKey");
		if(!YFCCommon.isStringVoid(sOrderReleaseKey)){
			backOrderLine(yfcEleOrderLine, sOrderReleaseKey);
		}
		moveLineToServiceRejected(yfcEleOrderLine, sRejectionReason);
	}

	/**
	 * 
	 * @param yfcDocGetOrderLineListOp 
	 * @param yfcEleInRoot 
	 * @param yfcEleInRoot
	 */
	private void cancelOrderLine(YFCElement yfcEleInRoot, YFCDocument yfcDocGetOrderLineListOp) {

		double dCancelledQuantity = yfcEleInRoot.getElementsByTagName(TelstraConstants.SHIPMENT_LINE).item(0)
				.getDoubleAttribute(TelstraConstants.STATUS_QUANTITY, 0.00);
		cancelOrderLine(yfcDocGetOrderLineListOp, dCancelledQuantity);
	}

	/**
	 * 
	 * @param sQuantity
	 * @param sOrderHeaderKey
	 * @param sOrderLineKey
	 */
	private void cancelOrderLine(YFCDocument yfcDocGetOrderLineListOp, double dCancelledQuantity) {

		YFCElement yfcEleOrderLine = yfcDocGetOrderLineListOp.getElementsByTagName(TelstraConstants.ORDER_LINE).item(0);
		String sOrderLineKey = yfcEleOrderLine.getAttribute(TelstraConstants.ORDER_LINE_KEY);
		YFCElement yfcEleOrder = yfcEleOrderLine.getChildElement(TelstraConstants.ORDER);
		String sOrderHeaderKey = yfcEleOrder.getAttribute(TelstraConstants.ORDER_HEADER_KEY);
		double dOrderedQty = yfcEleOrderLine.getDoubleAttribute(TelstraConstants.ORDERED_QTY);

		double dNewOrderedQty = dOrderedQty - dCancelledQuantity;
		logger.verbose("RejectOrder::cancelOrderLine:: dNewOrderedQty\n" + dNewOrderedQty);

		if (dNewOrderedQty < 0) {
			logger.error("RejectOrder::cancelOrderLine:: Cancellation qty can not be greater than the ordered qty"
					+ dNewOrderedQty);
			throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.REJECT_ORDER_CANCELLED_QTY_MORE,
					new YFSException());
		}

		YFCDocument yfcDocChangeOrderIp = YFCDocument.getDocumentFor(
				"<Order  Override='Y' OrderHeaderKey='" + sOrderHeaderKey + "'> <OrderLines> <OrderLine Override='Y' OrderLineKey='"
						+ sOrderLineKey + "' OrderedQty='" + dNewOrderedQty + "'/> </OrderLines> </Order>");
		logger.verbose("RejectOrder::cancelOrderLine:: yfcDocChangeOrderIp\n" + yfcDocChangeOrderIp);

		invokeYantraApi(TelstraConstants.API_CHANGE_ORDER, yfcDocChangeOrderIp);
	}

	/**
	 * 
	 * @param yfcEleOrderLine
	 * @param sOrderReleaseKey 
	 */
	private void backOrderLine(YFCElement yfcEleOrderLine, String sOrderReleaseKey) {
		
		String sOrderHeaderKey = yfcEleOrderLine.getChildElement(TelstraConstants.ORDER).getAttribute(TelstraConstants.ORDER_HEADER_KEY);
		String sOrderLineKey = yfcEleOrderLine.getAttribute(TelstraConstants.ORDER_LINE_KEY);
		
		YFCDocument yfcDocChangeReleaseIp = YFCDocument.createDocument(TelstraConstants.ORDER_RELEASE);
		YFCElement yfcDocChangeReleaseRoot = yfcDocChangeReleaseIp.getDocumentElement();
		yfcDocChangeReleaseRoot.setAttribute(TelstraConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
		yfcDocChangeReleaseRoot.setAttribute(TelstraConstants.ORDER_RELEASE_KEY, sOrderReleaseKey);
		
		YFCElement yfcEleOrderLinesChngRel = yfcDocChangeReleaseRoot.createChild(TelstraConstants.ORDER_LINES);
		YFCElement yfcEleOrderLineChngRel = yfcEleOrderLinesChngRel.createChild(TelstraConstants.ORDER_LINE);
		yfcEleOrderLineChngRel.setAttribute(TelstraConstants.ACTION, TelstraConstants.BACKORDER);
		yfcEleOrderLineChngRel.setAttribute(TelstraConstants.ORDER_LINE_KEY, sOrderLineKey);
		
		invokeYantraApi(TelstraConstants.API_CHANGE_RELEASE, yfcDocChangeReleaseIp);		

	}

	/**
	 * 
	 * @param yfcEleOrderLine
	 * @param sRejectionReason 
	 */
	private void moveLineToServiceRejected(YFCElement yfcEleOrderLine, String sRejectionReason) {

		YFCElement yfcEleOrder = yfcEleOrderLine.getElementsByTagName(TelstraConstants.ORDER).item(0); 
		String sDocumentType = yfcEleOrder.getAttribute(TelstraConstants.DOCUMENT_TYPE);		
		String sOrderHeaderKey = yfcEleOrder.getAttribute(TelstraConstants.ORDER_HEADER_KEY);
		String sOrderLineKey = yfcEleOrderLine.getAttribute(TelstraConstants.ORDER_LINE_KEY);
		String sPrimeLineNo = yfcEleOrderLine.getAttribute(TelstraConstants.PRIME_LINE_NO);
		String date = getDateStamp();
		String sTransactionID = getProperty("ChangeOrderStatusToServiceRejected", true) + sDocumentType + ".ex";//SERVICE_REJECTED_UPDATE.0001.ex
		YFCDocument docchangeOrderinXml = YFCDocument
				.getDocumentFor("<Order OrderHeaderKey='" + sOrderHeaderKey + "' TransactionId='" + sTransactionID
						+ "' ModificationReasonText='" + sRejectionReason + "' ModificationReference1='Order Line#  " 
						+ sPrimeLineNo + " Move to service rejected status at " + date
						+ "'><OrderLines> <OrderLine BaseDropStatus='1100.5000' ChangeForAllAvailableQty='Y' OrderLineKey='"
						+ sOrderLineKey + "'/></OrderLines></Order>");
		LoggerUtil.verboseLog("RejectOrder:: moveLineToServiceRejected:: changeOrderStatus", logger, docchangeOrderinXml);

		invokeYantraApi("changeOrderStatus", docchangeOrderinXml);
	}

	/**
	 * 
	 * @return
	 */
	private YFCDocument callGetOrderLineList(String sVectorID) {

		YFCDocument inDocgetOrdLineList = YFCDocument.getDocumentFor(
				"<OrderLine><Order EntryType='VECTOR' /><Extn VectorID='" + sVectorID + "' /></OrderLine>");
		YFCDocument templateForGetOrdLineList = YFCDocument.getDocumentFor("<OrderLineList TotalLineList=''>"
				+ "<OrderLine PrimeLineNo='' OrderLineKey='' OrderedQty='' MinLineStatus=''><OrderStatuses><OrderStatus  OrderReleaseKey='' Status=''/>"
				+ " </OrderStatuses>  <Order OrderName='' OrderHeaderKey='' DocumentType=''/></OrderLine> </OrderLineList>");

		YFCDocument yfcDocGetOrderLineListOp = invokeYantraApi("getOrderLineList", inDocgetOrdLineList,
				templateForGetOrdLineList);

		logger.verbose("RejectOrder::callGetOrderLineList:: yfcDocGetOrderLineListOp\n" + yfcDocGetOrderLineListOp);

		return yfcDocGetOrderLineListOp;
	}

	/**
	 * 
	 * @return
	 */
	private String getDateStamp() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		// get current date time with Date()
		Date date = new Date();
		return dateFormat.format(date);
	}
}

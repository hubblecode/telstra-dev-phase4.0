/***********************************************************************************************
 * File	Name		: GpsAddWaitTimeToManageOrder.java
 *
 * Description		: This class is called from GpsManageOrder service
 * 					The purpose of this class to delete source id added from GpsAddWaitTimeToManageOrder.java
 * 
 * Modification	Log	:
 * -------------------------------------------------------------------------------------------------------------------------------------
 * Ver #	Date			Author					Modification
 * -------------------------------------------------------------------------------------------------------------------------------------
 * 1.0		July 3,2017	  	Prateek Kumar 		   	Initial	Version 
 * -------------------------------------------------------------------------------------------------------------------------------------
 * 
 */
package com.gps.hubble.order.api;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSException;

/**
 * 
 * @author Prateek
 *
 */

public class GpsDeleteSourceId  extends AbstractCustomApi {

	private static YFCLogCategory logger = YFCLogCategory.instance(GpsDeleteSourceId.class);

	@Override
	public YFCDocument invoke(YFCDocument yfcInDoc) throws YFSException {

		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", yfcInDoc);
		String sOrderName = yfcInDoc.getDocumentElement().getAttribute(TelstraConstants.ORDER_NAME);
		
		if(!YFCCommon.isStringVoid(sOrderName)){
			boolean isOrderNameExist = callGpsGetSourceId(sOrderName);
			if(isOrderNameExist){
				
				/*
				 * Delete source id
				 */
				
				YFCDocument yfcDocDeleteSourceIdIp = YFCDocument.getDocumentFor("<SourceID SourceID='"+sOrderName+"'/>");
				invokeYantraService(TelstraConstants.SERVICE_GPS_DELETE_SOURCE_ID, yfcDocDeleteSourceIdIp);
			}
		}
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", yfcInDoc);
		return yfcInDoc;
	}

	/**
	 * 
	 * @param sOrderName
	 * @return
	 */
	private boolean callGpsGetSourceId(String sOrderName) {

		boolean bRetFlag = false;
		YFCDocument yfcDocGetSourceIdIp = YFCDocument.getDocumentFor("<SourceID SourceID='"+sOrderName+"'/>");
		YFCDocument yfcDocGetSourceIdOp = invokeYantraService(TelstraConstants.SERVICE_GPS_GET_SOURCE_ID_LIST, yfcDocGetSourceIdIp);
		LoggerUtil.verboseLog(this.getClass().getName()+" :: callGpsGetSourceId :: yfcDocGetSourceIdOp\n", logger, yfcDocGetSourceIdOp);
		if(yfcDocGetSourceIdOp.getDocumentElement().hasChildNodes()){
			bRetFlag = true;
		}		

		return bRetFlag;
	}
}

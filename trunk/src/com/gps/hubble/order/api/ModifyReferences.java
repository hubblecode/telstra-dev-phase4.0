/***********************************************************************************************
 * File	Name		: ModifyReferences.java
 *
 * Description		: This class is called from the order interface before 
 * 						to change the Reference Names in such a way that it is understandable by the users 
 * 						and also configurable.
 * 
 * Modification	Log	:
 * ---------------------------------------------------------------------------------------------
 * Ver #	Date				Author					Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0		July 13,2017	  	Manish Kumar 		   	Initial	Version 
 * ---------------------------------------------------------------------------------------------
 * 
 */
package com.gps.hubble.order.api;

import java.util.HashMap;
import java.util.Map;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;

/**
 * Custom API to change reference names in the input to create/change order.
 * 
 * @author Manish Kumar
 * @version 1.0
 *
 * Extends AbstractCustomApi Class
 */

public class ModifyReferences extends AbstractCustomApi {
	private static YFCLogCategory logger = YFCLogCategory.instance(ManageOrder.class);
		
	public YFCDocument invoke(YFCDocument inDoc) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(),"invoke", inDoc);
		Map<String, String> inReferenceMap = new HashMap<String, String>();
		
		YFCElement inRootEle = inDoc.getDocumentElement();
		String strSourceSystem = inRootEle.getAttribute("EntryType");
		String strDepartmentCode = inRootEle.getAttribute("DepartmentCode");
		if(strSourceSystem.equalsIgnoreCase("VECTOR")) {
			strSourceSystem = "VECTOR";
		} else if(strSourceSystem.equalsIgnoreCase("INTEGRAL_PLUS")) {
			strSourceSystem = "INTEGRAL";
			strDepartmentCode = "ALL";
		} else {
			return inDoc;
		}
		
		
		YFCElement referencesELe = inRootEle.getChildElement("References");
		YFCNodeList<YFCElement> referenceNL = referencesELe.getElementsByTagName("Reference");
		for(YFCElement referenceEle : referenceNL) {
			String strReferenceName = referenceEle.getAttribute("Name"," ");
			if(!strReferenceName.equals(" ")) {
				String strReferenceValue = referenceEle.getAttribute("Value"," ");
				inReferenceMap.put(strReferenceName, strReferenceValue);
			}
		}
		
		logger.verbose("inReferenceMap : " + inReferenceMap);
		inRootEle.removeChild(referencesELe);
		logger.verbose("inRootEle : " + inRootEle.toString());
		Map<String, String> referenceNameMap = translateReferenceNames(strSourceSystem+"_"+strDepartmentCode);
		if(YFCObject.isNull(referenceNameMap)) {
			return inDoc;
		}
		
		
		YFCElement newReferencesELe = null;
		for (Map.Entry<String, String> referenceNameEntry : referenceNameMap.entrySet()) {
			String strRefName = referenceNameEntry.getKey();
			String strRefNameValue = referenceNameEntry.getValue();
			String strRefMandatory = strRefNameValue.substring(0, strRefNameValue.indexOf("-"));
			//logger.verbose("strRefMandatory = " + strRefMandatory);
			String strTranslatedRefName = strRefNameValue.substring(strRefNameValue.indexOf("-")+1);
			//logger.verbose("strTranslatedRefName = " + strTranslatedRefName);
			String strRefValue = " ";
			if(inReferenceMap.containsKey(strRefName)) {
				strRefValue = inReferenceMap.get(strRefName);
			}
		    logger.verbose("Reference Name = " + strRefName + ", Translated Reference Name = " + strTranslatedRefName
		    		+ ", Is Mandatory = " + strRefMandatory + ", Reference Value = " + strRefValue);
		    if(YFCObject.isNull(newReferencesELe)) {
	    		newReferencesELe = inRootEle.createChild("References");
	    	}
		    if(strTranslatedRefName.equalsIgnoreCase("LOGIC")) {
		    	Map<String, String> logicMap = getAdditionalReferences(strSourceSystem, strDepartmentCode, strRefValue, referenceNameMap);
		    	logger.verbose("logicMap: " + logicMap);
		    	for (Map.Entry<String, String> logicEntry : logicMap.entrySet()) {
		    		String strLogicName = logicEntry.getKey();
					String strLogicValue = logicEntry.getValue();
					YFCElement newReferenceELe = newReferencesELe.createChild("Reference");
			    	newReferenceELe.setAttribute("Name", strLogicName);
			    	newReferenceELe.setAttribute("Value", strLogicValue);
		    	}
		    }
		    else {
		    	if(!(strRefMandatory.equalsIgnoreCase("NO") && strRefValue.trim().equals(""))) {
			    	YFCElement newReferenceELe = newReferencesELe.createChild("Reference");
			    	newReferenceELe.setAttribute("Name", strTranslatedRefName);
			    	newReferenceELe.setAttribute("Value", strRefValue);
		    	}
		    }
	    }
		LoggerUtil.endComponentLog(logger, this.getClass().getName(),"invoke", inDoc);
		return inDoc;
	}

	/**
	 * This method takes care of any additional logic of processing the Reference Values.
	 * The method will only be triggered if the CodeLongDescription for a CodeValue is LOGIC.
	 * Additional logic needs to be written on need basis.
	 * @param strSourceSystem
	 * @param strDepartmentCode
	 * @param strRefValue
	 * @param referenceNameMap
	 * @return
	 */
	public Map<String, String> getAdditionalReferences(String strSourceSystem, String strDepartmentCode, 
														String strRefValue, Map<String, String> referenceNameMap) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(),"getAdditionalReferences", strRefValue);
		Map<String, String> logicMap = new HashMap<String, String>();
		
		if(strSourceSystem.equalsIgnoreCase("VECTOR")) {
			if(strDepartmentCode.equalsIgnoreCase("PMC")) {
				/*
				 * Example of the input:
				 * R=N:SCN=Y:CT=N:
				 * R=N means Dont include Reservations
				 * SCN=Y means Separate Con Note is Yes
				 * CT=N means Ceva Transport is No 
				 */
				String[] tokens = strRefValue.split(":");
				String strRemainingComments = "";
				for (int i = 0; i < tokens.length; i++) {
					String strToken = tokens[i];
				    System.out.println("strToken " + strToken);
				    if(strToken.startsWith("SCN=")) {
				    	if(strToken.endsWith("Y"))
				    		logicMap.put((referenceNameMap.get("SCN=")).substring((referenceNameMap.get("SCN=")).indexOf("-")+1), "Yes");
				    	else if(strToken.endsWith("N"))
				    		logicMap.put((referenceNameMap.get("SCN=")).substring((referenceNameMap.get("SCN=")).indexOf("-")+1), "No");
				    }
				    else if(tokens[i].startsWith("CT=")) {
				    	if(strToken.endsWith("Y"))
				    		logicMap.put((referenceNameMap.get("CT=")).substring((referenceNameMap.get("CT=")).indexOf("-")+1), "Yes");
				    	else if(strToken.endsWith("N"))
				    		logicMap.put((referenceNameMap.get("CT=")).substring((referenceNameMap.get("CT=")).indexOf("-")+1), "No");
				    }
				    else if(tokens[i].startsWith("R=")) {
				    	if(strToken.endsWith("Y"))
				    		logicMap.put((referenceNameMap.get("R=")).substring((referenceNameMap.get("R=")).indexOf("-")+1), "Yes");
				    	else if(strToken.endsWith("N"))
				    		logicMap.put((referenceNameMap.get("R=")).substring((referenceNameMap.get("R=")).indexOf("-")+1), "No");
				    }
				    else {
				    	if(!strRemainingComments.equals("")){
				    		strRemainingComments = strRemainingComments + ":";
				    	}
				    	strRemainingComments = strRemainingComments + strToken;
				    }
				}
				
				if(!strRemainingComments.equals("") && !YFCObject.isNull(referenceNameMap.get("In Case Of Exception"))){
					logicMap.put((referenceNameMap.get("In Case Of Exception")).substring((referenceNameMap.get("In Case Of Exception")).indexOf("-")+1), strRemainingComments);
		    	}
			}
			else if(strDepartmentCode.equalsIgnoreCase("RASS")) {
				try {
					/*
					 * Example of the input:
					 * 99C:R43873 SOT: UGP, ST: QU, SPD/BW: 40E, DOA: , WR: UPG SPEED TO 4M WT SERV TYPE CHG NCO 03379459, 
					 * SP: PM  ADITYA MARRIPALLI 0293077298          SQ ID 1161730. DEVICE  RDMPCQCPKR01C19 N W  N3016311R    
					 * DONT PROGRESS ORDER
					 * 
					 * 43873 is 99C:R43873 is an ID and the remaining are comments
					 */
					String strCompleteID = strRefValue;
					String strOtherComments = strRefValue;
					if(strRefValue.contains("SOT:")) {
						strCompleteID = strRefValue.substring(0, strRefValue.indexOf("SOT:"));
						strOtherComments = strRefValue.substring(strRefValue.indexOf("SOT:")).trim();
					}
					
					String strID = " ";
					if(strCompleteID.contains("99C:R")) {
						strID = strCompleteID.substring(strCompleteID.indexOf("99C:R")+5).trim();
						if(!strRefValue.contains("SOT:")) {
							strOtherComments = " ";
						}
					}
	
					System.out.println("strCompleteID = " + strCompleteID);
					System.out.println("strOtherComments = " + strOtherComments);
					System.out.println("strID = " + strID);
					
					logicMap.put((referenceNameMap.get("99C:R")).substring((referenceNameMap.get("99C:R")).indexOf("-")+1), strID);
					logicMap.put((referenceNameMap.get("SOT:")).substring((referenceNameMap.get("SOT:")).indexOf("-")+1), strOtherComments);
				}
				catch (Exception e) {
					if(!YFCObject.isNull(referenceNameMap.get("In Case Of Exception"))) {
						logicMap.put((referenceNameMap.get("In Case Of Exception")).substring((referenceNameMap.get("In Case Of Exception")).indexOf("-")+1), strRefValue);
					}
				}
			}
		}
		LoggerUtil.endComponentLog(logger, this.getClass().getName(),"getAdditionalReferences", strRefValue);
		return logicMap;
	}

	/**
	 * This method gets the Common Code values defined for the CodeType passed in the input and returns a map
	 * of the Input Reference Names thats need to be translated with the Translated Reference Names and whether 
	 * it is required to pass that attribute in the input to create order or not (this will only be used if the 
	 * attribute value is not passed or passed as blank).
	 * Example: If CodeLongDescription=Contact:Name CodeShortDescription=YES CodeValue=ContactInformation Contact
	 * The input reference name will be translated from "ContactInformation Contact" to "Contact:Name", even if 
	 * the reference name is in the input or not.
	 * Example: If CodeLongDescription=Ceva:Reservations CodeShortDescription=NO CodeValue=R=
	 * The input reference name will be translated from "R=" to "Ceva:Reservations", only if 
	 * the reference name is in the input with a non-blank value.
	 * If the Code Type is not defined, null is returned.
	 * 
	 * @param strCodeType
	 * @return
	 */
	public Map<String, String> translateReferenceNames(String strCodeType) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(),"translateReferenceNames", strCodeType);
		try {
			/*
			 *  call getCommonCodeList Api with the following input: <CommonCode CodeType="VECTOR_PMC"/>
			 *  and the following template:
			 *  <CommonCodeList>
			 *  <CommonCode CodeLongDescription='' CodeShortDescription='' CodeValue=''/>
			 *  </CommonCodeList>
			 */ 
			
			YFCDocument inXml = YFCDocument.getDocumentFor("<CommonCode CodeType='"+strCodeType+"'/>");
			YFCDocument tempXML = YFCDocument.getDocumentFor("<CommonCodeList>"
					+ "<CommonCode CodeLongDescription='' CodeShortDescription='' CodeValue=''/>"
					+ "</CommonCodeList>");
			
			YFCDocument commonCodeListOutDoc = invokeYantraApi("getCommonCodeList", inXml, tempXML);
			
			Map<String, String> referenceNameMap = new HashMap<String, String>(); 
			YFCElement commonCodeListRootEle = commonCodeListOutDoc.getDocumentElement();
			YFCNodeList<YFCElement> commonCodeNL = commonCodeListRootEle.getElementsByTagName("CommonCode");
			for(YFCElement commonCodeEle : commonCodeNL) {
				String strCodeValue = commonCodeEle.getAttribute("CodeValue"," ");
				String strCodeShortDescription = commonCodeEle.getAttribute("CodeShortDescription","NO");
				String strCodeLongDescription = commonCodeEle.getAttribute("CodeLongDescription"," ");
				referenceNameMap.put(strCodeValue, strCodeShortDescription+"-"+strCodeLongDescription);
			}
			logger.verbose("referenceNameMap : " + referenceNameMap);
			return referenceNameMap;
		}
		catch(Exception ex) {
			return null;
		}
		finally {
			LoggerUtil.endComponentLog(logger, this.getClass().getName(),"translateReferenceNames", strCodeType);
		}
		
	}

}

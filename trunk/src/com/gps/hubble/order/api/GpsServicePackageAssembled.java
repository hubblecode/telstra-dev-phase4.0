/***********************************************************************************************
 * File Name        : GpsServicePackageAssembled.java
 *
 * Description      : Custom API for processing package assembled message from WMS
 * 
 * Modification Log :
 * ---------------------------------------------------------------------------------------------
 * Ver #	Date			Author				Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0		11 Jun, 2017	Prateek Kumar		Initial Version
 * ---------------------------------------------------------------------------------------------
 * Copyright (c) IBM Australia Limited 2017. All rights reserved.
 **********************************************************************************************/
package com.gps.hubble.order.api;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.constants.TelstraErrorCodeConstants;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSException;

/**
 * 
 * @author Prateek
 *
 *<Shipment ServiceName="Package Assembled" OrderName="Vector Id">
		<ShipmentLines>
			<ShipmentLine PrimeLineNo="1(use the vector id to identify the line number)" SubLineNo="1" ItemID="ignore the item id passed here" 
			UnitOfMeasure="ignore the uom passed here" Quantity="only cancel the qty passed here">
			</ShipmentLine>
		</ShipmentLines>
	</Shipment>
 */
public class GpsServicePackageAssembled  extends AbstractCustomApi {

	private static YFCLogCategory logger = YFCLogCategory.instance(GpsServicePackageAssembled.class);
	private String orderName;
	private String shipNode;
	private String unitOfMeasure;
	private String itemId;
	private String primeLineNo;
	private String orderedQty;
	private String subLineNo;

	@Override
	public YFCDocument invoke(YFCDocument yfcInDoc) throws YFSException {
		
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", yfcInDoc);
		
		YFCElement yfcEleInRoot = yfcInDoc.getDocumentElement();

		String sVectorID = yfcEleInRoot.getAttribute(TelstraConstants.ORDER_NAME);

		callGetOrderLineList(sVectorID);
		
		YFCDocument yfcDocPickOrderIp = preparePickOrderServiceInput();
		
		logger.verbose("RejectOrder::invoke:: yfcDocPickOrderIp\n" + yfcDocPickOrderIp);
		
		invokeYantraService(TelstraConstants.GPS_PICK_ORDER, yfcDocPickOrderIp);
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", yfcInDoc);
		return yfcInDoc;
	}

	/**
	 * 
	 * @param yfcDocGetOrderLineListOp
	 * @return
	 */
	private YFCDocument preparePickOrderServiceInput() {

		YFCDocument yfcDocPickOrderIp = YFCDocument.createDocument(TelstraConstants.ORDER_RELEASE);
		
		YFCElement yfcElePickOrderRoot = yfcDocPickOrderIp.getDocumentElement();
		yfcElePickOrderRoot.setAttribute(TelstraConstants.ORDER_NAME, getOrderName());
		yfcElePickOrderRoot.setAttribute(TelstraConstants.SHIP_NODE, getShipNode());
		
		YFCElement yfcEleOrderLines = yfcElePickOrderRoot.createChild(TelstraConstants.ORDER_LINES);
		YFCElement yfcEleOrderLine = yfcEleOrderLines.createChild(TelstraConstants.ORDER_LINE);
		yfcEleOrderLine.setAttribute(TelstraConstants.ACTION, TelstraConstants.BACKORDER);
		yfcEleOrderLine.setAttribute(TelstraConstants.PRIME_LINE_NO, getPrimeLineNo());
		yfcEleOrderLine.setAttribute(TelstraConstants.SUB_LINE_NO, getSubLineNo());
		yfcEleOrderLine.setAttribute(TelstraConstants.STATUS_QUANTITY, getOrderedQty());
		yfcEleOrderLine.setAttribute(TelstraConstants.ITEM_ID, getItemId());
		yfcEleOrderLine.setAttribute(TelstraConstants.UNIT_OF_MEASURE, getUnitOfMeasure());
		
		return yfcDocPickOrderIp;
	}

	/**
	 * 
	 * @param sVectorID
	 * @return
	 */
	private void callGetOrderLineList(String sVectorID) {
		YFCDocument yfcDocgetOrdLineListIp = YFCDocument.getDocumentFor(
				"<OrderLine><Order EntryType='VECTOR' /><Extn VectorID='" + sVectorID + "' /></OrderLine>");
		YFCDocument templateForGetOrdLineList = YFCDocument.getDocumentFor("<OrderLineList TotalLineList=''>"
				+ "<OrderLine PrimeLineNo='' SubLineNo='' OrderedQty='' ShipNode=''><Order OrderName='' OrderHeaderKey=''/> <Item ItemID='' UnitOfMeasure=''/></OrderLine>"
				+ "</OrderLineList>");
		logger.verbose("RejectOrder::GpsServicePackageAssembled:: yfcDocgetOrdLineListIp\n" + yfcDocgetOrdLineListIp);

		YFCDocument yfcDocGetOrderLineListOp = invokeYantraApi("getOrderLineList", yfcDocgetOrdLineListIp,
				templateForGetOrdLineList);

		logger.verbose("RejectOrder::GpsServicePackageAssembled:: yfcDocGetOrderLineListOp\n" + yfcDocGetOrderLineListOp);

		int iOrderLineLength = yfcDocGetOrderLineListOp.getElementsByTagName(TelstraConstants.ORDER_LINE)
				.getLength();
		
		
		if(iOrderLineLength == 0){
			throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.VECTOR_ID_DOES_NOT_EXIST,
					new YFSException());
		}
		else if(iOrderLineLength > 1){
			throw ExceptionUtil.getYFSException(
					TelstraErrorCodeConstants.MULTIPLE_ORDER_LINES_WITH_SAME_VECTOR_ID,
					new YFSException());
		}
		
		YFCElement yfcEleOrderLineList =  yfcDocGetOrderLineListOp.getDocumentElement();		
		YFCElement yfcEleOrderLine = yfcEleOrderLineList.getChildElement(TelstraConstants.ORDER_LINE);
		setPrimeLineNo(yfcEleOrderLine.getAttribute(TelstraConstants.PRIME_LINE_NO));
		setSubLineNo(yfcEleOrderLine.getAttribute(TelstraConstants.SUB_LINE_NO));
		setOrderedQty(yfcEleOrderLine.getAttribute(TelstraConstants.ORDERED_QTY));
		setShipNode(yfcEleOrderLine.getAttribute(TelstraConstants.SHIP_NODE));
		
		YFCElement yfcEleOrder = yfcEleOrderLine.getChildElement(TelstraConstants.ORDER);
		setOrderName(yfcEleOrder.getAttribute(TelstraConstants.ORDER_NAME));
		
		YFCElement yfcEleItem = yfcEleOrderLine.getChildElement(TelstraConstants.ITEM);
		setItemId(yfcEleItem.getAttribute(TelstraConstants.ITEM_ID));
		setUnitOfMeasure(yfcEleItem.getAttribute(TelstraConstants.UNIT_OF_MEASURE));		
	}
	
	
	private String getOrderName() {
		return orderName;
	}

	private void setOrderName(String orderName) {
		this.orderName = orderName;
	}

	private String getShipNode() {
		return shipNode;
	}

	private void setShipNode(String shipNode) {
		this.shipNode = shipNode;
	}

	private String getUnitOfMeasure() {
		return unitOfMeasure;
	}

	private void setUnitOfMeasure(String unitOfMeasure) {
		this.unitOfMeasure = unitOfMeasure;
	}

	private String getItemId() {
		return itemId;
	}

	private void setItemId(String itemId) {
		this.itemId = itemId;
	}

	private String getPrimeLineNo() {
		return primeLineNo;
	}

	private void setPrimeLineNo(String primeLineNo) {
		this.primeLineNo = primeLineNo;
	}

	private String getOrderedQty() {
		return orderedQty;
	}

	private void setOrderedQty(String orderedQty) {
		this.orderedQty = orderedQty;
	}

	private String getSubLineNo() {
		return subLineNo;
	}

	private void setSubLineNo(String subLineNo) {
		this.subLineNo = subLineNo;
	}
}

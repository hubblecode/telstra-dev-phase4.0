/***********************************************************************************************
 * File Name        : ShipOrder.java
 *
 * Description      : Custom API for WMS status update for Ship (Integral+ and Vector Orders)
 * 
 * Modification Log :
 * ---------------------------------------------------------------------------------------------
 * Ver #    Date            	Author							Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0                      	Karthikeyan Suruliappan   		Initial Version
 * 1.1		Jan 18, 2017		Prateek Kumar					If the ship confirmation msg comes for a line which is completely shipped, throw an exception 
 * 1.2		Jun 01, 2017		Keerthi Yadav					HUB-9218: Remove "Order is on hold or Hold applied after scheduleOrder()" exception.
 * 1.3		Jun 12, 2017		Keerthi Yadav					HUB-9322: Ship Confirmation Message failure for a valid PMC Vector Sales Order in Order Picked status
 * 1.4      Jun 14, 2017		Keerthi Yadav					HUB-9349: Ship Order failing for Lines that are Partially Picked
 * 1.5		Jun 26, 2017		Keerthi Yadav					HUB-9411: Usage of Shipment Key to confirm Shipment in Ship Order
 * 1.6		Jun 27, 2017		Keerthi Yadav					HUB-9415: Adding Reason Text while calling Adjust Inventory for Audit
 * 1.7		Jun 29, 2017		Keerthi Yadav					HUB-9430: Invalid exception thrown when Invalid Prime Line No is passed in the Shipment confirmation message
 * 1.8		Aug 18, 2017		Prateek Kumar					HUB-9472: consuming the message if the vector recovery is already shipped
 * ---------------------------------------------------------------------------------------------
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
 **********************************************************************************************/
package com.gps.hubble.order.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.constants.TelstraErrorCodeConstants;
import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSException;

public class ShipOrder extends AbstractCustomApi {
    private static YFCLogCategory logger = YFCLogCategory.instance(ApplyDuplicateOrderHold.class);
    private boolean soFlag = true;
    private boolean callApiFlg = false;
    private String orderHeaderKey = "";
    private String shipmentKey = "";
    private String sOdrName = "";
    private boolean backOrdFlg = false;
    private boolean vOrdFlg = false;
    private Set < String > ordLineKeySet = null;
    private Set<String> orderLineKeySet = null;
    private HashMap < String, ArrayList < String >> ordRelsKeyToOrdLineNoMap = null;
    private HashMap < String, YFCDocument > shipMap = null;
    private ArrayList < String > eligibleOrdRelKeyList = null;
    private YFCDocument orderReleaseDoc = null;
    private HashMap < String,String > confirmShipAdditionalAtrMap = null;
    private HashMap <String, YFCDocument> shipConfirmationDocMap = null;
    private HashMap < String,ArrayList<YFCElement>> shipmentKeyMap = null;
    private HashMap <String,Double> inputSumMap = null;
    private ArrayList<String> resultList = null;
    private YFCDocument shipmentLineListDoc = null;
    private String sIncomingShipNode = TelstraConstants.BLANK;
    private String sourceSystem = null;
    private String tranID="";
    private String baseDropStatus="";
    private String sShipmentNoOp ="";
    
    private YFCDocument inXml;
    /**
     * Base Method execution starts here
     * 
     */
    public YFCDocument invoke(YFCDocument inXml) throws YFSException {
        LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", inXml);
        YFCElement inEle = inXml.getDocumentElement();
        
        String sServiceName = inEle.getAttribute(TelstraConstants.SERVICE_NAME);
        
		//HUB-9415[START]

        sOdrName = inEle.getAttribute(TelstraConstants.ORDER_NAME);
        
		//HUB-9415[END]
        
        if(!YFCCommon.isStringVoid(sServiceName)){
        
        	if(!isServiceNameConfigured(sServiceName)){
        		
        		throw ExceptionUtil.getYFSException(
    					TelstraErrorCodeConstants.SHIP_ORDER_SERVICE_NAME_NOT_CONFIGURED,"ServiceName : "+sServiceName ,
    					new YFSException());	
        	}
        }
        /*
         * If the incoming shipment message is for the vector order, update all the incoming fields
         * with that of the order
         */
        YFCDocument yfcDocGetOrderLineListOp = getOrderLineList(inXml);
        /*
         * calling get order list to find the details of the order
         */
        YFCDocument outputGetOrdListForComQry = getOrderList(inEle);
        //
        this.inXml = inXml;
        
        /*
         * If the incoming order is recovery, create a shipment and return back
         * 
         */
        if(TelstraConstants.EQUIPMENT_PICKED.equalsIgnoreCase(sServiceName)){
        	
        	int iOrderLineLength = yfcDocGetOrderLineListOp.getElementsByTagName(TelstraConstants.ORDER_LINE)
    				.getLength();
    		
    		
    		if(iOrderLineLength == 0){
    			throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.VECTOR_ID_DOES_NOT_EXIST,
    					new YFSException());
    		}
    		else if(iOrderLineLength > 1){
    			throw ExceptionUtil.getYFSException(
    					TelstraErrorCodeConstants.MULTIPLE_ORDER_LINES_WITH_SAME_VECTOR_ID,
    					new YFSException());
    		}
    		
        	YFCDocument yfcRetDoc = checkAndShipReturnOrder(yfcDocGetOrderLineListOp);
        	yfcRetDoc.getDocumentElement().setAttribute(TelstraConstants.SHIPMENT_NO, sShipmentNoOp);
        	
        	return yfcRetDoc;
        }
        
        //HUB-9411[START]
        /*Skip Validation if Input is passed*/
        shipmentKey = inEle.getAttribute(TelstraConstants.SHIPMENT_KEY,"");
        if(YFCObject.isVoid(shipmentKey)){
        	validateInputXml(inEle, outputGetOrdListForComQry);
        }
        
        //HUB-9411[END]

        if(YFCObject.isNull(shipConfirmationDocMap)) {
          shipConfirmationDocMap = new HashMap<String, YFCDocument>();
        }
        if(!YFCObject.isVoid(outputGetOrdListForComQry)) {
          
          String docType =  XPathUtil.getXpathAttribute(outputGetOrdListForComQry, "//Order/@DocumentType");
          
          sourceSystem = outputGetOrdListForComQry.getElementsByTagName("Order").item(0).getAttribute("EntryType");
          
          //HUB-9411[START]
          
          /* When shipment Key is passed in the Input for Ship Confirmation Message we would need to confirm the Shipment 
           * and Adjust inventory based on the Source System for the Order.
           *  This is implemented to invoke this method from carrier Update when we already have a shipment in less than shipped status*/
          
          if(!YFCObject.isVoid(shipmentKey)){
              formAdditionalConfirmShipAtrMap(inEle);
        	  confirmShipment();
        	  if(!YFCCommon.isStringVoid(sShipmentNoOp)){
  				inXml.getDocumentElement().setAttribute(TelstraConstants.SHIPMENT_NO, sShipmentNoOp);
  			}
        	  return inXml;
          }

          //HUB-9411[END]

          /*
           * If the document type is 0006 then set the soFlag as false.
           */
          if("0006".equals(docType)){
            setSoFlag(false);
          }
          
          /**
           * Splitting the Ship Confirmation message for each node
           */
          YFCElement orderEle = outputGetOrdListForComQry.getElementsByTagName("Order").item(0);
          YFCNodeList<YFCElement> orderLines = orderEle.getElementsByTagName("OrderLine");
          YFCNodeList<YFCElement> inputShipLines = inXml.getElementsByTagName("ShipmentLine");
          for(YFCElement shipmentLine : inputShipLines) {
            String primeLineNo = shipmentLine.getAttribute("PrimeLineNo");
            for(YFCElement orderLine : orderLines) {
              if(orderLine.getAttribute("PrimeLineNo").equals(primeLineNo)) {
                if(shipConfirmationDocMap.containsKey(orderLine.getAttribute("ShipNode"))) {
                  YFCDocument shipConfDoc = shipConfirmationDocMap.get(orderLine.getAttribute("ShipNode"));
                  YFCElement shipmentLines = shipConfDoc.getElementsByTagName("ShipmentLines").item(0);
                  shipmentLines.importNode(shipmentLine);
                  shipConfirmationDocMap.remove(orderLine.getAttribute("ShipNode"));
                  shipConfirmationDocMap.put(orderLine.getAttribute("ShipNode"), shipConfDoc);
                } else {
                  YFCDocument shipConfDoc = YFCDocument.getDocumentFor("<Shipment><ShipmentLines></ShipmentLines></Shipment>");
                  YFCElement shipConfDocEle = shipConfDoc.getDocumentElement();
                  shipConfDocEle.setAttributes(inEle.getAttributes());
                  YFCElement shipmentLines = shipConfDoc.getElementsByTagName("ShipmentLines").item(0);
                  shipmentLines.importNode(shipmentLine);
                  shipConfirmationDocMap.put(orderLine.getAttribute("ShipNode"), shipConfDoc);
                }
                break;
              }
            }
          }
        }
        
        /*
         * if the soFlag is set then the baseDropStatus and TranID wll be set for the Sales Order (SO).  Else baseDropStatus and TranID will be set for Transfer Order (TO).
         */
        if(!isSoFlag()){
            setBaseDropStatus(getProperty("TOPickBaseDropStatus"));
            setTranID(getProperty("TOPickTranID"));
        }else{
            setBaseDropStatus(getProperty("SOPickBaseDropStatus"));
            setTranID(getProperty("SOPickTranID"));
        }
        
        if(!YFCObject.isNull(shipConfirmationDocMap) && shipConfirmationDocMap.size() > 0) {
        	Set<Entry<String, YFCDocument>> shipConfirmationDocMapSet = shipConfirmationDocMap.entrySet();
        	Iterator<Entry<String, YFCDocument>> shipConfirmationDocMapSetItr = shipConfirmationDocMapSet.iterator();
        	while (shipConfirmationDocMapSetItr.hasNext()) {
        		Map.Entry<String, YFCDocument> shipConfirmationDocMapSetEntry = (Map.Entry<String, YFCDocument>) shipConfirmationDocMapSetItr.next();
        		YFCDocument shipConfirmationDoc = shipConfirmationDocMapSetEntry.getValue();
        		YFCElement shipConfirmationDocElement = shipConfirmationDoc.getDocumentElement();
        		uniqueOrderValidation(outputGetOrdListForComQry,shipConfirmationDocElement);
        		formShipmentKeySet(outputGetOrdListForComQry, inXml);
        		if (backOrdFlg) {
        			scheduleOrder();
        			outputGetOrdListForComQry = getOrderList(shipConfirmationDocElement);
        			mapOrdReleaseKeyAndOrdLineNo(outputGetOrdListForComQry);
        			callChangeRelease(inXml);
        			if(!YFCCommon.isStringVoid(sShipmentNoOp)){
        				inXml.getDocumentElement().setAttribute(TelstraConstants.SHIPMENT_NO, sShipmentNoOp);
        			}
        			LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", inXml);
        			return inXml;
        		}
        		getShipmentLineList(inXml);
        		//vOrdFlg for vector order condition (In case if no shipments in picked status for input message then this will be considered as vector order). 
        		if (vOrdFlg) {
        			/*shipConfirm message for multiple shipments in picked status for same order.*/
        			formShipmentKeyMap(outputGetOrdListForComQry);
        			ArrayList<String> bestFitSKSet = bestShipmentKeyMatchAlgorithm(inXml);
        			if(bestFitSKSet != null && bestFitSKSet.size() > 0){
        				LoggerUtil.verboseLog("BestFitSKSet", logger,bestFitSKSet.toString());
        				confirmMatchingShipments(bestFitSKSet);
        				if(!YFCCommon.isStringVoid(sShipmentNoOp)){
            				inXml.getDocumentElement().setAttribute(TelstraConstants.SHIPMENT_NO, sShipmentNoOp);
            			}
        				return inXml;
        			}
        			//HUB-6862[Start]
        			/*This method is to createShipment for the incoming lines by calling GpsPickOrderService.*/
        			boolean shipmentExists = false;
        			boolean changeShipmentCalledForLine = false;
        			YFCElement orderEle = outputGetOrdListForComQry.getElementsByTagName("Order").item(0);
        			YFCNodeList<YFCElement> orderLines = orderEle.getElementsByTagName("OrderLine");
        			YFCNodeList<YFCElement> inputShipLines = shipConfirmationDocElement.getElementsByTagName("ShipmentLine");
        			YFCDocument createShipDoc = YFCDocument.getDocumentFor("<Shipment><ShipmentLines></ShipmentLines></Shipment>");
        			YFCElement shipmentLines = createShipDoc.getElementsByTagName("ShipmentLines").item(0);
        			for(YFCElement shipmentLine : inputShipLines) {
        				String primeLineNo = shipmentLine.getAttribute("PrimeLineNo");
        				for(YFCElement orderLine : orderLines) {
        					if(orderLine.getAttribute("PrimeLineNo").equals(primeLineNo)) {
        						String orderLineKey = orderLine.getAttribute("OrderLineKey");
        						YFCDocument getShipLineListInput = getShipmentLineListInput(orderLineKey);
        						YFCDocument getShipLineListOutput = invokeYantraApi("getShipmentLineList", getShipLineListInput, getShipmentLineListTemplate());
        						if(!YFCObject.isVoid(getShipLineListOutput) && getShipLineListOutput.getDocumentElement().hasChildNodes()) {
        							YFCNodeList<YFCElement> shipmentLineList = getShipLineListOutput.getElementsByTagName("ShipmentLine");
        							changeShipmentCalledForLine = false;
        							for(YFCElement shipmentLineEle : shipmentLineList) {
        								shipmentExists = true;
        								if(shipmentLineEle.getDoubleAttribute("Quantity") == shipmentLine.getDoubleAttribute("StatusQuantity") && !changeShipmentCalledForLine) {        									
        									createShipDoc.getDocumentElement().setAttribute("EnterpriseCode", shipmentLineEle.getElementsByTagName("Shipment").item(0).getAttribute("EnterpriseCode"));
        									createShipDoc.getDocumentElement().setAttribute("SellerOrganizationCode", shipmentLineEle.getElementsByTagName("Shipment").item(0).getAttribute("SellerOrganizationCode"));
        									createShipDoc.getDocumentElement().setAttribute("ShipNode", shipmentLineEle.getElementsByTagName("Shipment").item(0).getAttribute("ShipNode"));
        									createShipDoc.getDocumentElement().setAttribute("DocumentType", shipmentLineEle.getElementsByTagName("Shipment").item(0).getAttribute("DocumentType"));
                                            
                                            //HUB-9259[START]
                                            createShipDoc.getDocumentElement().setAttribute(TelstraConstants.ORDER_HEADER_KEY, orderHeaderKey);
                                            //HUB-9259[END]    
                                            

        									shipmentLineEle.setAttribute("Action", "Delete");
        									YFCDocument changeShipmentDoc = getChangeShipmentDoc(shipmentLineEle);
        									invokeYantraApi("changeShipment", changeShipmentDoc);
        									shipmentLineEle.removeAttribute("ShipmentKey");
        									shipmentLineEle.removeAttribute("ShipmentLineKey");
        									shipmentLineEle.removeAttribute("ShipmentNo");
        									shipmentLineEle.removeAttribute("Action");
        									shipmentLineEle.removeChild(shipmentLineEle.getChildElement("Shipment"));
        									shipmentLines.importNode(shipmentLineEle);
        									changeShipmentCalledForLine = true;
            								//HUB-9349[START]
        									/*In the else condition if the qtys do not match, the flag shipmentExists is again set to false so that it avoids the createShipment
        									 * API and calls the Pick logic. This is for a scenario where the line has partially picked qty and if ship message comes for the other qty
        									 * say which is in created status,then the getShipmentLineList will still return an output as we pass only the OrderLinekey. We bypass the logic 
        									 * of createShipment (because we need the release key and no schedule and release Api has been called yet) by setting 
        									 *shipmentExists as false and invoke the createShipmentAndPickOrderLines logic instead */
        								}else{
            								shipmentExists = false;
        								}
        								//HUB-9349[END]
        							}
        						}

        						//boolean bThrowException = true;

        						/*
        						 *  Changed the below logic to throw exception if one of the order line status breakup is in Order Picked Status.
        						 *  Old Behaviour:  If the order line status was in the Created, Backordered, Released, Partially Released and Partially Backordered statuses
        						 *  then the exception was being thrown
        						 */
        						/*double pickedQty = 0.0;
                      for(YFCElement orderStatus : orderStatuses) {
                        if("3350.10000".equals(orderStatus.getAttribute("Status"))) {
                          pickedQty = pickedQty + orderStatus.getDoubleAttribute("StatusQty");
                          throw ExceptionUtil.getYFSException("Ship Order Failed", "Ship Confirmation Failed Since the line is Fully or Partially Picked/Shipped for the Prime Line No "+ primeLineNo +" ",
                              new YFSException());
                        }


        						 * check if there is any quantity in the incoming line which is less than shipped status. If not, then throw an exceltion
        						 * as there is no more quantity which can be shipped

                        double dStatus = orderStatus.getDoubleAttribute("Status");
                        if(bThrowException && dStatus< 3700){
                        	bThrowException = false;
                        }
                        if(bThrowException){
                        	 throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.SHIP_ORDER_LINE_ALREADY_SHIPPED_ERROR_CODE, new YFSException());
                        }
                      }*/

        						/*createShipDoc = YFCDocument.getDocumentFor("<Shipment/>");
                      if(pickedQty == shipmentLine.getDoubleAttribute("StatusQuantity")) {
                        //TODO
                        YFCDocument getShipLineListInput = getShipmentLineListInput(orderLine);
                        YFCDocument getShipLineListOutput = invokeYantraApi("getShipmentLineList", getShipLineListInput);
                        if(!YFCObject.isVoid(getShipLineListOutput) && getShipLineListOutput.getDocumentElement().hasChildNodes()) {
                          YFCNodeList<YFCElement> shipmentLineList = getShipLineListOutput.getElementsByTagName("ShipmentLine");
                          for(YFCElement shipmentLineEle : shipmentLineList) {
                            YFCElement createShipDocShipLineEle = createShipDoc.createElement("ShipmentLine");
                            createShipDocShipLineEle.importNode(shipmentLineEle);
                            shipmentLineEle.setAttribute("Action", "Delete");
                            invokeYantraApi("changeShipment", inXml);
                          }
                        }
                      }*/
        					}
        				}
        			}
					//HUB-9349[START]
        			//if(shipmentExists && changeShipmentCalledForLine) {
            			if(shipmentExists) {

        				YFCDocument createShipmentOutDoc = invokeYantraApi("createShipment", createShipDoc, YFCDocument
        						.getDocumentFor("<Shipment ShipmentKey='' DocumentType=''><ShipmentLines><ShipmentLine OrderHeaderKey='' OrderReleaseKey='' OrderLineKey='' ShipmentLineKey=''/></ShipmentLines></Shipment>"));
        				if(!YFCObject.isVoid(createShipmentOutDoc)) {
        					setShipmentKey(XPathUtil.getXpathAttribute(createShipmentOutDoc, "//@ShipmentKey"));
        					changeShimentStatus();
        					confirmShipment();
        				}
        			}  /*else if(shipmentExists && !changeShipmentCalledForLine) {
        				throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.SHIP_ORDER_FAILED, new YFSException());
        			} */
						//HUB-9349[END]
            			else {
        				createShipmentAndPickOrderLines(shipConfirmationDoc, outputGetOrdListForComQry);
        			}
        			//HUB-6862[End] 
        		} else {
        			confirmShipment();
        		}
        	}
        }
        if(!YFCCommon.isStringVoid(sShipmentNoOp)){
			inXml.getDocumentElement().setAttribute(TelstraConstants.SHIPMENT_NO, sShipmentNoOp);
		}
        LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", inXml);
        return inXml;
    }
    
    /**
     * 
     * @param yfcGetOrderLineListOp 
     * @return
     */
    private YFCDocument checkAndShipReturnOrder(YFCDocument yfcDocGetOrderLineListOp) {

    	boolean bIsOrderNotShippedShipped = checkShipmentForRo(yfcDocGetOrderLineListOp);

    	/*
    	 * HUB-9473
    	 * if(!bIsOrderNotShippedShipped){
    		throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.VECTOR_LINE_ALREADY_SHIPPED,
    				new YFSException());
    	}*/
    	if(bIsOrderNotShippedShipped){
    		YFCElement yfcEleOrder = yfcDocGetOrderLineListOp.getElementsByTagName(TelstraConstants.ORDER).item(0);
    		if(YFCCommon.isVoid(yfcEleOrder)){
    			throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.REJECT_ORDER_NO_ORDER_LINE_FOR_VECTOR_ID, new YFSException());
    		}
    		String sOrderHeaderKey = yfcEleOrder.getAttribute(TelstraConstants.ORDER_HEADER_KEY);
    		createAndConfirmReturnOrderShipment(sOrderHeaderKey);
    	}
    	return inXml;		
    }

    /**
     * @param outputGetOrdListForComQry 
     * @param sOrderHeaderKey 
     * 
     */
    private void createAndConfirmReturnOrderShipment(String sOrderHeaderKey) {
		
		YFCDocument docCreateShipmentIp = YFCDocument.createDocument(TelstraConstants.SHIPMENT);
		YFCElement eleShipment = docCreateShipmentIp.getDocumentElement();
		eleShipment.setAttribute(TelstraConstants.DOCUMENT_TYPE, TelstraConstants.DOCUMENT_TYPE_0003);
		String sShipNode = inXml.getElementsByTagName(TelstraConstants.SHIPMENT).item(0)
				.getAttribute(TelstraConstants.SHIP_NODE);

		
		//HUB-9259[START]
		eleShipment.setAttribute(TelstraConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
		//HUB-9259[END]

		YFCElement eleShipmentLines = docCreateShipmentIp.createElement(TelstraConstants.SHIPMENT_LINES);
		eleShipment.appendChild(eleShipmentLines);

		YFCNodeList<YFCElement> nlShipmentLine = inXml.getElementsByTagName(TelstraConstants.SHIPMENT_LINE);
		for (YFCElement eleShipmentLineIp : nlShipmentLine) {

			YFCElement eleShipmentLine = docCreateShipmentIp.createElement(TelstraConstants.SHIPMENT_LINE);
			eleShipmentLines.appendChild(eleShipmentLine);
			eleShipmentLine.setAttribute(TelstraConstants.RECEIVING_NODE, sShipNode);
			eleShipmentLine.setAttribute(TelstraConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
			eleShipmentLine.setAttribute(TelstraConstants.PRIME_LINE_NO,
					eleShipmentLineIp.getAttribute(TelstraConstants.PRIME_LINE_NO));
			eleShipmentLine.setAttribute(TelstraConstants.SUB_LINE_NO,
					eleShipmentLineIp.getAttribute(TelstraConstants.SUB_LINE_NO));
			eleShipmentLine.setAttribute(TelstraConstants.QUANTITY,
					eleShipmentLineIp.getAttribute(TelstraConstants.STATUS_QUANTITY));
		}
		LoggerUtil.verboseLog("Receive Order::createROShipment::docCreateShipmentIp ", logger, docCreateShipmentIp);
		YFCDocument docCreateShipmentOp = invokeYantraApi(TelstraConstants.API_CREATE_SHIPMENT, docCreateShipmentIp);		
		LoggerUtil.verboseLog("Receive Order::createROShipment::docCreateShipmentOp ", logger, docCreateShipmentOp);

		YFCDocument yfcDocConfShipOp = invokeYantraApi(TelstraConstants.API_CONFIRM_SHIPMENT, docCreateShipmentOp);		
        if(!YFCCommon.isVoid(yfcDocConfShipOp)){
        	YFCElement yfcEleShipment = yfcDocConfShipOp.getElementsByTagName(TelstraConstants.SHIPMENT).item(0);
        	sShipmentNoOp = yfcEleShipment.getAttribute(TelstraConstants.SHIPMENT_NO);
        }
		
	}

	/**
     * 
     * @param outputGetOrdListForComQry
     * @return
     */
    private boolean checkShipmentForRo(YFCDocument yfcDocGetOrderLineListOp) {

    	boolean bReturn = false;
    	for(YFCElement yfcEleOrderLine : yfcDocGetOrderLineListOp.getElementsByTagName(TelstraConstants.ORDER_LINE)){
    		bReturn = false;
    		for(YFCElement yfcEleOrderStatus : yfcEleOrderLine.getElementsByTagName(TelstraConstants.ORDER_STATUS)){

    			double dStatus = yfcEleOrderStatus.getDoubleAttribute(TelstraConstants.STATUS);
    			if(dStatus<3700){
    				bReturn = true;
    			}
    		}
    		if(!bReturn){			
    			break;		
    		}
    	}
    	return bReturn;
    }

	/**
     * This method check if the service name passed is configured or not
     * @param sServiceName 
     * @return
     */
    private boolean isServiceNameConfigured(String sServiceName) {

    	boolean bReturn = false;
    	String sServiceNames = getProperty("SERVICE_NAME", false);
    	if(!YFCCommon.isStringVoid(sServiceNames)){
    		String[] sServiceNameList = sServiceNames.split(TelstraConstants.COMMA_SEPARATOR);
    		for(String sServiceNameConfigured : sServiceNameList){
    			if(sServiceName.equalsIgnoreCase(sServiceNameConfigured)){
    				bReturn = true;
    				break;
    			}
    		}
    	}
    	return bReturn;
    }

    /**
     * 
     * @param inXml
     */
	private YFCDocument getOrderLineList(YFCDocument inXml) {
      YFCDocument inDocgetOrdLineList = YFCDocument.getDocumentFor("<OrderLine><Order EntryType='VECTOR' /><Extn VectorID='"+inXml.getDocumentElement().getAttribute("OrderName")+"' /></OrderLine>");
		LoggerUtil.verboseLog("ShipOrder :: callGetOrderListWithVectorIDEntryType :: get order list Ip \n", logger,
				inDocgetOrdLineList);
		YFCDocument templateForGetOrdLineList = YFCDocument.getDocumentFor(
				"<OrderLineList TotalLineList=''><OrderLine OrderedQty='' PrimeLineNo='' SubLineNo=''><Item ItemID='' UnitOfMeasure=''/><OrderStatuses><OrderStatus TotalQuantity='' StatusQty='' Status=''/>"
						+ "</OrderStatuses><Order OrderName='' OrderHeaderKey=''/></OrderLine> </OrderLineList>");
      YFCDocument yfcDocGetOrderLineListOp = invokeYantraApi("getOrderLineList", inDocgetOrdLineList, templateForGetOrdLineList);
		LoggerUtil.verboseLog("ShipOrder :: callGetOrderListWithVectorIDEntryType :: get order list Op \n", logger,
				yfcDocGetOrderLineListOp);
      if(!YFCObject.isVoid(yfcDocGetOrderLineListOp) 
          && yfcDocGetOrderLineListOp.getDocumentElement().getDoubleAttribute("TotalLineList") > 0) {
        if(yfcDocGetOrderLineListOp.getDocumentElement().getDoubleAttribute("TotalLineList") > 1) {
          throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.SHIP_ORDER_MULTIPLE_ORDER_LINES_WITH_SAME_VECTOR_ID, new YFSException());
        }
        String primeLineNo = yfcDocGetOrderLineListOp.getElementsByTagName("OrderLine").item(0).getAttribute("PrimeLineNo");
        String sSubLineNo = yfcDocGetOrderLineListOp.getElementsByTagName("OrderLine").item(0).getAttribute("SubLineNo");
        String sOrderedQty = yfcDocGetOrderLineListOp.getElementsByTagName("OrderLine").item(0).getAttribute("OrderedQty");
        String orderName = yfcDocGetOrderLineListOp.getElementsByTagName("Order").item(0).getAttribute("OrderName");
        String sItemID = yfcDocGetOrderLineListOp.getElementsByTagName("Item").item(0).getAttribute("ItemID");
        String sUnitOfMeasure = yfcDocGetOrderLineListOp.getElementsByTagName("Item").item(0).getAttribute("UnitOfMeasure");
        
        inXml.getDocumentElement().setAttribute("OrderName", orderName);
        inXml.getElementsByTagName("ShipmentLine").item(0).setAttribute("PrimeLineNo", primeLineNo);
        inXml.getElementsByTagName("ShipmentLine").item(0).setAttribute("SubLineNo", sSubLineNo);
        inXml.getElementsByTagName("ShipmentLine").item(0).setAttribute("ItemID", sItemID);
        inXml.getElementsByTagName("ShipmentLine").item(0).setAttribute("UnitOfMeasure", sUnitOfMeasure);
        /*
         * Vector line will always be full ship,
         */
        inXml.getElementsByTagName("ShipmentLine").item(0).setAttribute("StatusQuantity", sOrderedQty);
        /*
         * If the incoming qty is 0, assume complete line is getting shipped
         */
       /* double dQty = inXml.getElementsByTagName("ShipmentLine").item(0).getDoubleAttribute("StatusQuantity");
        if(dQty==0){
        	inXml.getElementsByTagName("ShipmentLine").item(0).setAttribute("StatusQuantity", sOrderedQty);
        }*/
        
        //HUB-9322[START]
        /* The changes are done to stamp Ordered Qty for a line only when all the Qty for the line
         * are above and equal to picked and less than shipped*/
        double dOrderedQty = yfcDocGetOrderLineListOp.getElementsByTagName("OrderLine").item(0).getDoubleAttribute(TelstraConstants.ORDERED_QTY,0.0);
        double dtotalPickedQty = getTotalPickedQty(yfcDocGetOrderLineListOp.getElementsByTagName("OrderLine").item(0));
        if(dOrderedQty == dtotalPickedQty && dOrderedQty!=0.0){
            inXml.getElementsByTagName("ShipmentLine").item(0).setAttribute("StatusQuantity", dtotalPickedQty);
        }
        
       		
        //HUB-9322[END]

      }
	return yfcDocGetOrderLineListOp;
    }

	/**
	 * Method for getTotalPickedQty
	 * 
	 * @param eleOrderLine
	 * @return dtotalPickedQty
	 */
	private double getTotalPickedQty(YFCElement eleOrderLine) throws YFCException {
		double dtotalPickedQty = 0;
    	String sMinStatus = getProperty("MIN_STATUS", true);
    	String sMaxStatus = getProperty("MAX_STATUS", true);
    	
    	double dMinStatus = Double.parseDouble(sMinStatus);
    	double dMaxStatus = Double.parseDouble(sMaxStatus);


		for (YFCElement eleOrderStatus : eleOrderLine.getElementsByTagName("OrderStatus")) {
			double dOrderStatus = eleOrderStatus.getDoubleAttribute("Status");
			if (dOrderStatus >= dMinStatus && dOrderStatus < dMaxStatus && dOrderStatus!=9000) {
				double dOrderStatusQty = eleOrderStatus.getDoubleAttribute("StatusQty");
				dtotalPickedQty = dtotalPickedQty + dOrderStatusQty;
			}
		}
		return dtotalPickedQty;
	}
	
    /**
     * This method is used move the created shipment to picked status
     */
    private void changeShimentStatus() {
      YFCDocument inputApiDoc = YFCDocument.getDocumentFor("<Shipment Action='Modify' BaseDropStatus='"+getBaseDropStatus()+"' " +
              "ShipmentKey='"+getShipmentKey()+"' " +
              "TransactionId='"+getTranID()+"' />");
      invokeYantraApi("changeShipmentStatus", inputApiDoc);
  }
    
    private YFCDocument getShipmentLineListTemplate() {
      YFCDocument getShipmentLineListTemp = YFCDocument.getDocumentFor("<ShipmentLines>"
          + "<ShipmentLine Quantity='' ItemID='' OrderLineKey='' OrderNo='' PrimeLineNo='' ShipmentKey='' ShipmentLineKey='' SubLineNo='' UnitOfMeasure='' ShipmentLineNo='' ReleaseNo=''> "
          + "<Shipment ShipNode='' ShipmentNo='' SellerOrganizationCode='' EnterpriseCode='' DocumentType=''/> </ShipmentLine> </ShipmentLines>");
      return getShipmentLineListTemp;
    }

    private YFCDocument getChangeShipmentDoc(YFCElement shipmentLineEle) {
      YFCDocument changeShipmentInput = YFCDocument.getDocumentFor("<Shipment><ShipmentLines><ShipmentLine /></ShipmentLines></Shipment>");
      YFCElement shipmentLine = changeShipmentInput.getElementsByTagName("ShipmentLine").item(0);
      shipmentLine.setAttributes(shipmentLineEle.getAttributes());
      shipmentLine.setAttribute("Action", "Delete");
      changeShipmentInput.getDocumentElement().setAttributes(shipmentLineEle.getElementsByTagName("Shipment").item(0).getAttributes());
      return changeShipmentInput;
    }

    private YFCDocument getShipmentLineListInput(String orderLineKey) {
      YFCDocument getShipmentLineListInput = YFCDocument.getDocumentFor("<ShipmentLine OrderLineKey='"+orderLineKey+"'><Shipment StatusQryType='LT' Status='1400' /></ShipmentLine>");
      return getShipmentLineListInput;
    }

    /**
     * 
     * @param bestFitSKSet
     */
    private void confirmMatchingShipments(ArrayList<String> bestFitSKSet) {
            Set<String> shipmentKeySet = new HashSet<>();
            for (String sk : bestFitSKSet) {                
                /*YFCElement matchingShipmentLine = XPathUtil.getXPathElement(shipmentLineListDocC, "//ShipmentLine[@OrderReleaseKey='"+rk+"']");
                if(XmlUtils.isVoid(matchingShipmentLine)){
                    throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.CEV_NO_SHIPMENT_FOUND,
                            new YFSException());
                }               
                String sk = matchingShipmentLine.getAttribute("ShipmentKey");*/               
                if(!shipmentKeySet.contains(sk)){
                    setShipmentKey(sk);
                    confirmShipment();
                }
                shipmentKeySet.add(sk);
            }
    }

    /**
     * This method helps to sum up the passed ORKs released qtys 
     * @param arr
     * @return
     */
    private HashMap<String, Double> constructComparisonMap(ArrayList<String> arr) {
        HashMap<String, Double> curr_Sum_Map = new HashMap<>();
        for (String key : arr) {
            ArrayList<YFCElement> curr_List= shipmentKeyMap.get(key);
            for (YFCElement currEle : curr_List) {
                String ln = currEle.getAttribute("PrimeLineNo");
                Double qty = currEle.getDoubleAttribute("Quantity");
                if(!curr_Sum_Map.containsKey(ln)){
                    curr_Sum_Map.put(ln, qty);
                }else{
                    Double prev_Qty = curr_Sum_Map.get(ln);
                    curr_Sum_Map.put(ln, prev_Qty+qty);
                }
            }   
        }
        return curr_Sum_Map;
    }

    /**
     * This method compares inputSumMap and curr_Sum_Map is same or not,this is to ensure that the releaseKeys and inXml lines are properly matching or not 
     * @param curr_Sum_Map
     * @return
     */
    private boolean compare(HashMap<String, Double> curr_Sum_Map) {
        if (curr_Sum_Map.equals(inputSumMap)){
            LoggerUtil.verboseLog("Input message LineNo - > Qty map ", logger,inputSumMap.toString());
            LoggerUtil.verboseLog("Match Found for the current releaseKey/s list", logger,curr_Sum_Map.toString());
            return true;
        }else{
            return false;
        }
    }

    /**
     * This method is to find the best release keys from the releaseKeyMap by making subset of the releaseKeyMap keys which is nothing but release keys 
     * @param inXml
     * @return
     */
    private ArrayList<String> bestShipmentKeyMatchAlgorithm(YFCDocument inXml) {

        findCorrectReleaseMap(inXml);   
        constructInputLineQuantityMap(inXml);
            
        /*if(releaseKeyMap == null || releaseKeyMap.size()==0){
            return null;
        }*/
        
        if(shipmentKeyMap == null || shipmentKeyMap.size() == 0) {
          return null;
        }
        
        int mapSize = shipmentKeyMap.size();
        ArrayList<String> shipmentKeys = getKeys();
        findAllSubSet(shipmentKeys,mapSize);
        return resultList;
    }

    /**
     * This method forms the line qty map for the inXml 
     * @param inXml
     */
    private void constructInputLineQuantityMap(YFCDocument inXml) {

        YFCNodeList<YFCElement> lineEleList = inXml.getElementsByTagName("ShipmentLine");
        for (YFCElement yfcElement : lineEleList) {
            String ln = yfcElement.getAttribute("PrimeLineNo");
            Double qty = yfcElement.getDoubleAttribute("StatusQuantity");
            if(qty <=0){
                continue;
            }
            if(inputSumMap == null){
                inputSumMap = new HashMap<>();
            }
            if(!inputSumMap.containsKey(ln)){
                inputSumMap.put(ln, qty);
            }else{
                Double prev_Qty = inputSumMap.get(ln);
                inputSumMap.put(ln, prev_Qty+qty);
            }
        }
    }

    /**
     * Finding all subsets of a given list. Ref:http://www.geeksforgeeks.org/finding-all-subsets-of-a-given-set-in-java
     * @param releaseKeys
     * @param n
     */
    private void findAllSubSet(ArrayList<String> shipmentKeys, int n) {
        for (int i = 0; i < (1<<n); i++)
        {
            ArrayList<String> tmp = new ArrayList<>();
            for (int j = 0; j < n; j++){                  
                if ((i & (1 << j)) > 0){
                    tmp.add(shipmentKeys.get(j));    
                }       
            }     
            if(tmp.size()>0){
                LoggerUtil.verboseLog("Current BestFit ORK List ", logger,tmp.toString());
                HashMap<String, Double> curr_Sum_Map= constructComparisonMap(tmp);
                if(compare(curr_Sum_Map)){
                    resultList = tmp;
                    return;
                }
            }
        }
    }

    /**
     * 
     * @return
     */
    private ArrayList<String> getKeys() {
        ArrayList<String> keys = new ArrayList<String>();       
        Set<Entry<String, ArrayList<YFCElement>>> entrySet = shipmentKeyMap.entrySet();
        Iterator<Entry<String, ArrayList<YFCElement>>> it = entrySet.iterator();      
        while (it.hasNext()) {  
            Map.Entry<String, ArrayList<YFCElement>> curMapEntry = (Map.Entry<String, ArrayList<YFCElement>>) it.next();
            keys.add((String) curMapEntry.getKey());
        }
        LoggerUtil.verboseLog("All ORK List ", logger,keys.toString());
        return keys;
    }

    /**
     * This will exclude the unnecessary releasKeys from the releaseKey map 
     * @param inXml
     */
    private void findCorrectReleaseMap(YFCDocument inXml) {
        
        /*if(releaseKeyMap == null || releaseKeyMap.size()==0){
            return;
        }*/
        
        if(shipmentKeyMap == null || shipmentKeyMap.size() == 0) {
          return;
        }
        
        Set<Entry<String, ArrayList<YFCElement>>> entrySet = shipmentKeyMap.entrySet();
        Iterator<Entry<String, ArrayList<YFCElement>>> it = entrySet.iterator();      
        ArrayList<String> removeSKList = new ArrayList<>();
        while (it.hasNext()) {          
            Map.Entry<String, ArrayList<YFCElement>> curMapEntry = (Map.Entry<String, ArrayList<YFCElement>>) it.next();
            String cur_SK =  (String) curMapEntry.getKey();
            ArrayList < YFCElement > curStatusEleList = (ArrayList < YFCElement > ) curMapEntry.getValue();
            for (YFCElement yfcElement : curStatusEleList) {
                
                String pLNo = yfcElement.getAttribute("PrimeLineNo","");
                YFCElement matchingInputLineEle = XPathUtil.getXPathElement(inXml, "//ShipmentLines/ShipmentLine[@PrimeLineNo='"+pLNo+"']");

                if(XmlUtils.isVoid(matchingInputLineEle)){
                  removeSKList.add(cur_SK);
                }else{
                    Double qtyStr = matchingInputLineEle.getDoubleAttribute("StatusQuantity");
                    if(qtyStr <= 0){
                      removeSKList.add(cur_SK);
                    }
                }
            }
        }

        for (String sk : removeSKList) {
            if(shipmentKeyMap.containsKey(sk)){
                shipmentKeyMap.remove(sk);
            }
        }

    }

    /**
     * This method is to map releaseKey -> Associated release quantities with line detail 
     * @param outputGetOrdListForComQry
     */
    private void formShipmentKeyMap(YFCDocument outputGetOrdListForComQry) {
        YFCElement rootEle = outputGetOrdListForComQry.getDocumentElement();
        YFCElement ordLinesEle = rootEle.getChildElement("Order",true).getChildElement("OrderLines",true);
        YFCNodeList<YFCElement> ordLineList = ordLinesEle.getElementsByTagName("OrderLine");
        
        for (YFCElement ordLineEle : ordLineList) {
            String pLNo = ordLineEle.getAttribute("PrimeLineNo");
            String sLNo = ordLineEle.getAttribute("SubLineNo","1");
            String orderLineKey = ordLineEle.getAttribute("OrderLineKey");
            //HUB-9349[START]
            /*Added a Status check to filter shipments under the shipped status*/
            
            YFCDocument getShipmentLineListInDoc = YFCDocument.getDocumentFor("<ShipmentLine OrderLineKey='"+orderLineKey+"'><Shipment StatusQryType='LT' Status='1400' /></ShipmentLine>");
            //HUB-9349[END]

            YFCDocument getShipmentLineListOutDoc = invokeYantraApi("getShipmentLineList", getShipmentLineListInDoc);
            if(!YFCObject.isVoid(getShipmentLineListOutDoc) && getShipmentLineListOutDoc.getDocumentElement().hasChildNodes()) {
              // TODO
              YFCNodeList<YFCElement> shipmentLineList = getShipmentLineListOutDoc.getElementsByTagName("ShipmentLine");
              for(YFCElement shipmentLine : shipmentLineList) {
                String shipmentKey = shipmentLine.getAttribute("ShipmentKey");
                if(shipmentKeyMap == null) {
                  shipmentKeyMap = new HashMap<>();
                }
                ArrayList<YFCElement> tmpNL = null;
                if(!shipmentKeyMap.containsKey(shipmentKey)) {
                  tmpNL = new ArrayList<>();
                } else {
                  tmpNL = shipmentKeyMap.get(shipmentKey);
                }
                tmpNL.add(shipmentLine);
                shipmentKeyMap.put(shipmentKey, tmpNL);
              }
            }     
        }

    }

    /**
     * This method is to validate the input xml for required attributes. 
     * @param inEle
     */
    private void validateInputXml(YFCElement inEle, YFCDocument outputGetOrdListForComQry) {
        String ordName = inEle.getAttribute("OrderName", "");
        String totalOrderList = outputGetOrdListForComQry.getDocumentElement().getAttribute("TotalOrderList", "0");
        if("0".equals(totalOrderList)){            
            throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.PICK_ORD_NOT_AVAILBALE, new YFSException());
        }    
        if (!"1".equals(totalOrderList)) {
            LoggerUtil.verboseLog("Order Name Duplicate Validation Failure ", logger, TelstraConstants.THROW_EXCEPTION);
            String strErrorCode = getProperty("DuplicateOrderName");
            throw ExceptionUtil.getYFSException(strErrorCode, new YFSException());
        }
        
        boolean validInputFlg = true;
        YFCNodeList < YFCElement > yfcLineList = inEle.getElementsByTagName(getProperty("LineName"));
        for (YFCElement yfcElement: yfcLineList) {
            String plNo = yfcElement.getAttribute("PrimeLineNo", "");
            YFCElement matchingEle = XPathUtil.getXPathElement(outputGetOrdListForComQry, "//Order/OrderLines/OrderLine[@PrimeLineNo='" + plNo + "']");
			
			//HUB-9430[START]
            /*Throw an Exception when an Invalid Prime Line No Exception is passed*/
            
            if(YFCObject.isVoid(matchingEle)){
				LoggerUtil.verboseLog("Invalid Prime Line No passed in the Ship Confirmation Message", logger, " throwing exception");
				throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.SHIP_ORDER_INVALID_PRIME_LINE_NO, "PrimeLine No = " +plNo, new YFSException());
						}
            
            //HUB-9430[END]
			
            String itemID = yfcElement.getAttribute("ItemID", "");
            String action = yfcElement.getAttribute("Action", "");
            Double dQty = new Double(yfcElement.getDoubleAttribute("StatusQuantity",0.0));
            if (XmlUtils.isVoid(ordName) || XmlUtils.isVoid(plNo)) {
                validInputFlg = false;
                break;
            }
            if(XmlUtils.isVoid(itemID)){
              if(!YFCObject.isVoid(matchingEle) && matchingEle.hasChildNodes()) {
                yfcElement.setAttribute("ItemID", matchingEle.getElementsByTagName("Item").item(0).getAttribute("ItemID"));
              } else {
                validInputFlg = false;
                break;
              }
            }
            
            if(XmlUtils.isVoid(action)) {
              yfcElement.setAttribute("Action", "BACKORDER");
            }
            
            int iQty = dQty.intValue();
            yfcElement.setIntAttribute("StatusQuantity", iQty);

            if(YFCObject.isVoid(yfcElement.getAttribute("SubLineNo",""))) {
                yfcElement.setAttribute("SubLineNo","1");
            }
        }
        if (!validInputFlg || (yfcLineList.getLength() == 0)) {
            LoggerUtil.verboseLog("CEV message validation failure", logger, TelstraConstants.THROW_EXCEPTION);
            String strErrorCode = getProperty("CevMsgValidation");
            throw ExceptionUtil.getYFSException(strErrorCode, new YFSException());
        }

        formAdditionalConfirmShipAtrMap(inEle);
    }

    /**
     * This method is to form a map for additional attributes that are part of input CEV message 
     * @param inEle
     */
    private void formAdditionalConfirmShipAtrMap(YFCElement inEle) {
        confirmShipAdditionalAtrMap = new HashMap<>();
        String actualShipDate = inEle.getAttribute("ActualShipmentDate");
        if(!XmlUtils.isVoid(actualShipDate)){
            confirmShipAdditionalAtrMap.put("ActualShipmentDate", actualShipDate);
        }    
        String carrierServiceCode = inEle.getAttribute("CarrierServiceCode");
        if(!XmlUtils.isVoid(carrierServiceCode)){
            confirmShipAdditionalAtrMap.put("CarrierServiceCode", carrierServiceCode);
        }
        String scac = inEle.getAttribute("SCAC");
        if(!XmlUtils.isVoid(scac)){
            confirmShipAdditionalAtrMap.put("SCAC", scac);
        }
        String trackingNo = inEle.getAttribute("TrackingNo");
        if(!XmlUtils.isVoid(trackingNo)){
            confirmShipAdditionalAtrMap.put("TrackingNo", trackingNo);
        }

    }

    /**
     * This method is to get shipment line list for the picked shipments as this gives associated Order release keys as well setting
     * the vector order flag if not shipments there for an order. 
     * @param inXml
     */
    private void getShipmentLineList(YFCDocument inXml) {
        YFCDocument inApiDoc = YFCDocument.getDocumentFor("<ShipmentLine OrderHeaderKey='" + getOrderHeaderKey() + "'><Shipment Status='1200.10000'/></ShipmentLine>");
        YFCDocument outputShipLnLst = invokeYantraApi("getShipmentLineList", inApiDoc);
        setShipmentLineListDoc(outputShipLnLst);
        formShipMap(outputShipLnLst);
        if (vOrdFlg) {
            return;
        }
        findCorrectShipment(inXml);
    }

	/**
	 * This method  is to determine the existing shipment according to the input CEV message, based on the unique Order release key set. 
	 * @param inXml
	 */
    @SuppressWarnings("rawtypes")
    private void findCorrectShipment(YFCDocument inXml) {
        if (shipMap != null) {
            Set entrySet = shipMap.entrySet();
            Iterator it = entrySet.iterator();
            YFCNodeList < YFCNode > shipEleListInMsg = XPathUtil.getXpathNodeList(inXml, "//ShipmentLine[@StatusQuantity!='0']");
            int lineSizeInMsg = shipEleListInMsg.getLength();
            while (it.hasNext()) {
                Map.Entry curMapEntry = (Map.Entry) it.next();
                YFCDocument shipDoc = (YFCDocument) curMapEntry.getValue();
                YFCElement shipEle = shipDoc.getDocumentElement();
                String shipKey = (String) curMapEntry.getKey();
                YFCNodeList < YFCElement > shipLineList = shipEle.getElementsByTagName("ShipmentLine");
                int lineSizeInShip = shipLineList.getLength();
                if (lineSizeInShip == lineSizeInMsg) {
                	Set < YFCElement > uniqueEleSet = null;
                	//HUB-7887 - Begin


                	if (uniqueEleSet == null) {
                		uniqueEleSet = new HashSet < YFCElement > ();
                	}

                	for (YFCNode yfcNode: shipEleListInMsg) {
                		YFCElement yfcEle = YFCDocument.getDocumentFor(yfcNode.toString()).getDocumentElement();
                		String iQty = yfcEle.getAttribute("StatusQuantity","0");
                		Integer iQty_i = Integer.valueOf(iQty);
                		YFCElement tEle = XPathUtil.getXPathElement(shipDoc, "//ShipmentLine[@IsValidated='N' and @ItemID='" + yfcEle.getAttribute("ItemID") + "' and @Quantity='"+String.valueOf(iQty_i)+".00"+"']");

                		if (!XmlUtils.isVoid(tEle)) {
                			uniqueEleSet.add(tEle);
                			tEle.setAttribute("IsValidated", "Y");
                		}
                	}
                	if (uniqueEleSet.size() == lineSizeInMsg && isShipmentReleaseKeysMatching(shipLineList)) {
                		setShipmentKey(shipKey);
                		return;
                	}                	
                	//HUB-7887 - End
                }
            }
        }
        vOrdFlg = true;
    }

    /**
     * This method is to determine shipment line order release key is mapping with incoming order release key or not. 
     * @param shipEle
     * @return
     */
    private boolean isShipmentReleaseKeysMatching(YFCNodeList < YFCElement > shipEle) {
        int i = 0;
        for (YFCElement yfcElement: shipEle) {
            String sOrdRelKey = yfcElement.getAttribute(TelstraConstants.ORDER_RELEASE_KEY);
            
            //HUB-9349[START]
            if (ordLineKeySet == null) {
                return false;
            }
            //HUB-9349[END]
            
            if (ordLineKeySet.contains(sOrdRelKey)) {
                i++;
            }
        }
        if (shipEle.getLength() == i) {
            return true;
        }
        return false;
    }

    /**
     * get shipment line list output will be grouped according the shipment key. 
     * @param outputShipLnLst
     */
    private void formShipMap(YFCDocument outputShipLnLst) {
        YFCNodeList < YFCElement > shipLineEleList = outputShipLnLst.getDocumentElement().getElementsByTagName("ShipmentLine");
        if (shipLineEleList.getLength() == 0) {
            vOrdFlg = true;
            return;
        }
        for (YFCElement yfcElement: shipLineEleList) {
            String sKey = yfcElement.getAttribute("ShipmentKey");

            if (shipMap == null) {
                shipMap = new HashMap < String, YFCDocument > ();
            }
            YFCDocument tD = shipMap.get(sKey);
            if (XmlUtils.isVoid(tD)) {
                tD = YFCDocument.getDocumentFor("<Shipment/>");
            }

            YFCDocument tO = tD.getDocumentElement().getOwnerDocument();
            yfcElement.setAttribute("IsValidated", "N");
            YFCElement sl = tO.importNode(yfcElement, true);
            tD.getDocumentElement().appendChild(sl);
            if (shipMap.get(sKey) == null) {
                shipMap.put(sKey, tD);
            }
        }
    }

    /**
     * This method is to determine the unique release keys of order lines according to the input xml passed. As well we are determining , need to back order or not. 
     * @param outputGetOrdListForComQry
     * @param inXml
     */
    private void formShipmentKeySet(YFCDocument outputGetOrdListForComQry, YFCDocument inXml) {
        YFCNodeList < YFCNode > ordLineNodeList = XPathUtil.getXpathNodeList(inXml, "//ShipmentLine[@StatusQuantity!='0']");
        if (ordLineNodeList.getLength() == 0) {
            backOrdFlg = true;
            return;
        }
        for (YFCNode yfcNode: ordLineNodeList) {
            YFCElement yfcElement = YFCDocument.getDocumentFor(yfcNode.toString()).getDocumentElement();
            String plNo = yfcElement.getAttribute("PrimeLineNo");
            String slNo = yfcElement.getAttribute("SubLineNo", "1");
            YFCElement matchingEle = XPathUtil.getXPathElement(outputGetOrdListForComQry, "//Order/OrderLines/OrderLine[@PrimeLineNo='" + plNo + "' and @SubLineNo='" + slNo + "']");
            if (!XmlUtils.isVoid(matchingEle)) {
                String relKey = XPathUtil.getXpathAttribute(YFCDocument.getDocumentFor(matchingEle.toString()), "//OrderStatuses/OrderStatus[@Status='" + getProperty("OrderPickedStatus") + "' and @StatusQty='" + yfcElement.getAttribute("StatusQuantity") + ".00']/@OrderReleaseKey");
                String orderLineKey = matchingEle.getAttribute("OrderLineKey");
                if (!XmlUtils.isVoid(relKey)) {
                    if (ordLineKeySet == null) {
                      ordLineKeySet = new HashSet < String > ();
                    }
                    ordLineKeySet.add(relKey);
                }
                
                if(!XmlUtils.isVoid(orderLineKey)) {
                  if(orderLineKeySet == null) {
                    orderLineKeySet = new HashSet<String>();
                  }
                  orderLineKeySet.add(orderLineKey);
                }
            }
        }
        if(ordLineKeySet == null || orderLineKeySet == null){
            vOrdFlg=true;       
        }
        vOrdFlg=false;
    }

    /**
     * Change released will be called for the input lines according to the Order release key map 
     * @param inXml
     */
    @SuppressWarnings({
        "unchecked",
        "rawtypes"
    })
    void callChangeRelease(YFCDocument inXml) {
        if (ordRelsKeyToOrdLineNoMap != null) {
            Set entrySet = ordRelsKeyToOrdLineNoMap.entrySet();
            Iterator it = entrySet.iterator();
            while (it.hasNext()) {
                Map.Entry curMapEntry = (Map.Entry) it.next();
                ArrayList < String > lnList = (ArrayList < String > ) curMapEntry.getValue();
                String relsKey = (String) curMapEntry.getKey();
                orderReleaseDoc = YFCDocument.getDocumentFor("<OrderRelease><OrderLines/></OrderRelease>");
                YFCElement ordRelsEle = orderReleaseDoc.getDocumentElement();
                ordRelsEle.setAttribute(TelstraConstants.ORDER_RELEASE_KEY, relsKey);
                YFCElement ordLnsEle = ordRelsEle.getChildElement("OrderLines");
                for (String string: lnList) {
                    YFCElement matchingOrdLineEle = XPathUtil.getXPathElement(inXml, "//ShipmentLines/ShipmentLine[@PrimeLineNo='" + string + "']");
                    if (!XmlUtils.isVoid(matchingOrdLineEle)) {
                        YFCElement tmpEle = YFCDocument.getDocumentFor("<OrderLine/>").getDocumentElement();
                        Map < String, String > shipLineAtrMap = matchingOrdLineEle.getAttributes();
                        tmpEle.setAttributes(shipLineAtrMap);
                        YFCDocument oRelDoc = ordLnsEle.getOwnerDocument();
                        YFCElement cevOrdLineEleTemp = oRelDoc.importNode(tmpEle, true);
                        ordLnsEle.appendChild(cevOrdLineEleTemp);
                        callApiFlg = true;
                    }
                }
                if (callApiFlg) {
                    if (XmlUtils.isVoid(eligibleOrdRelKeyList)) {
                        eligibleOrdRelKeyList = new ArrayList();
                    }
                    eligibleOrdRelKeyList.add(relsKey);
                    releaseChange();
                }
            }
        }
    }

    /**
     * Order lines associated to an order release key map is formed here. 
     * @param outputGetOrdListForComQry
     */
    void mapOrdReleaseKeyAndOrdLineNo(YFCDocument outputGetOrdListForComQry) {
        if (ordRelsKeyToOrdLineNoMap == null) {
            ordRelsKeyToOrdLineNoMap = new HashMap < String, ArrayList < String >> ();
        }
        YFCNodeList < YFCElement > ordLineList = outputGetOrdListForComQry.getDocumentElement().getElementsByTagName("OrderLine");
        for (YFCElement yfcElement: ordLineList) {
            YFCElement ordStsEle = XPathUtil.getXPathElement(YFCDocument.getDocumentFor(yfcElement.toString()), "//OrderStatuses/OrderStatus[@Status='" + getProperty("OrderReleaseStatus") + "' and @StatusQty!='0']");
            if (!XmlUtils.isVoid(ordStsEle)) {
                String ordRelsKey = ordStsEle.getAttribute(TelstraConstants.ORDER_RELEASE_KEY);
                String ordLnNo = yfcElement.getAttribute("PrimeLineNo");
                if (ordRelsKeyToOrdLineNoMap.containsKey(ordRelsKey)) {
                    ArrayList < String > tempArrLst = ordRelsKeyToOrdLineNoMap.get(ordRelsKey);
                    tempArrLst.add(ordLnNo);
                } else {
                    ArrayList < String > tempArrLst = new ArrayList < String > ();
                    tempArrLst.add(ordLnNo);
                    ordRelsKeyToOrdLineNoMap.put(ordRelsKey, tempArrLst);
                }
            }
        }
    }

    /**
     * Shipment will be confirmed else exception for already confirmed shipment will be thrown
     */
    private void confirmShipment() {
        if(XmlUtils.isVoid(getShipmentKey())){
            LoggerUtil.verboseLog("Shipment validation failure ", logger, TelstraConstants.THROW_EXCEPTION);
            String strErrorCode = getProperty("ShipmentNotFound");
            throw ExceptionUtil.getYFSException(strErrorCode, new YFSException());
        }
        YFCDocument inputApiDoc = YFCDocument.getDocumentFor("<Shipment ShipmentKey='" + getShipmentKey() + "' />");
        YFCDocument outputGetShipmentList = null;
        outputGetShipmentList = invokeYantraApi("getShipmentList", inputApiDoc,YFCDocument.getDocumentFor("<Shipments TotalNumberOfRecords=''><Shipment ShipmentKey='' ShipmentLineKey='' Status='' ShipmentNo='' ShipNode=''><ShipmentLines><ShipmentLine ItemID='' Quantity='' ShipmentLineKey='' PrimeLineNo='' UnitOfMeasure=''/></ShipmentLines></Shipment></Shipments>"));
        if(YFCObject.equals(sourceSystem, TelstraConstants.VECTOR) || YFCObject.equals(sourceSystem, TelstraConstants.INTEGRAL_PLUS)) {
          if(!YFCObject.isVoid(outputGetShipmentList) && outputGetShipmentList.getDocumentElement().hasChildNodes()) {
              adjustInventory(outputGetShipmentList);
          }
        }
        
        if(outputGetShipmentList.getChildNodes().getLength()>0){
          insertSerialNumInCustomTable(outputGetShipmentList);
        }
        
        if(confirmShipAdditionalAtrMap.size() != 0){
            addtionalDetails(inputApiDoc);
        }
        outputGetShipmentList = invokeYantraApi("getShipmentList", inputApiDoc,YFCDocument.getDocumentFor("<Shipments><Shipment ShipmentKey='' Status='' ShipNode=''><ShipmentLines><ShipmentLine ShipmentLineKey='' PrimeLineNo=''/></ShipmentLines></Shipment></Shipments>"));
        if(getProperty("ShipmentStatus") == XPathUtil.getXpathAttribute(outputGetShipmentList, "//Shipment/@Status")){
            LoggerUtil.verboseLog("Shipment confirm validation failure ", logger, TelstraConstants.THROW_EXCEPTION);
            String strErrorCode = getProperty("ConfirmShipmentStatus");
            throw ExceptionUtil.getYFSException(strErrorCode, new YFSException());
        }
        
        YFCDocument yfcDocConfShipOp = invokeYantraApi("confirmShipment", inputApiDoc);
        if(!YFCCommon.isVoid(yfcDocConfShipOp)){
        	YFCElement yfcEleShipment = yfcDocConfShipOp.getElementsByTagName(TelstraConstants.SHIPMENT).item(0);
        	sShipmentNoOp = yfcEleShipment.getAttribute(TelstraConstants.SHIPMENT_NO);
        }
         
    }
    
    /**
     * This method copy the incoming shipment line to confirm shipment input. On top of that it appends order header and shipment line key to each line
     * @param inputApiDoc
     * @param outputGetShipmentList
     */
    private void insertSerialNumInCustomTable (YFCDocument outputGetShipmentList) {

        YFCNodeList<YFCElement> nlShipmentSerialNumber = inXml.getElementsByTagName(TelstraConstants.SHIPMENT_SERIAL_NUMBER);
        if(nlShipmentSerialNumber.getLength()>0){
            String sShipNode = outputGetShipmentList.getElementsByTagName(TelstraConstants.SHIPMENT).item(0).getAttribute(TelstraConstants.SHIP_NODE);
            inXml.getDocumentElement().setAttribute(TelstraConstants.SHIP_NODE, sShipNode);
            setTxnObject("yfcDocShipmentSerialNum", inXml);
            YFCElement yfcEleShipemnt = outputGetShipmentList.getElementsByTagName(TelstraConstants.SHIPMENT).item(0);
            if(!YFCCommon.isVoid(yfcEleShipemnt)){
                String sShipmentKey = yfcEleShipemnt.getAttribute(TelstraConstants.SHIPMENT_KEY);
                for(YFCElement yfcEleShipmentLineIp : inXml.getElementsByTagName(TelstraConstants.SHIPMENT_LINE)){

                    String sPrimeLineNo = yfcEleShipmentLineIp.getAttribute(TelstraConstants.PRIME_LINE_NO);
                    String sShipmentLineKey = XPathUtil.getXpathAttribute(outputGetShipmentList, "//ShipmentLine[@PrimeLineNo='"+sPrimeLineNo+"']/@ShipmentLineKey");
                    if(!YFCCommon.isStringVoid(sShipmentLineKey)){
                        for(YFCElement yfcEleShipmentSerialNum : yfcEleShipmentLineIp.getElementsByTagName(TelstraConstants.SHIPMENT_SERIAL_NUMBER)){
                            String sSerialNumber = yfcEleShipmentSerialNum.getAttribute(TelstraConstants.SERIAL_NO);

                            YFCDocument yfcDocCreateShipmentSerialNum = YFCDocument.getDocumentFor("<ShipmentSerialNumber/>");
                            YFCElement yfcEleCreateShipmentSerialNumRoot = yfcDocCreateShipmentSerialNum.getDocumentElement();
                            yfcEleCreateShipmentSerialNumRoot.setAttribute(TelstraConstants.SHIPMENT_KEY, sShipmentKey);
                            yfcEleCreateShipmentSerialNumRoot.setAttribute(TelstraConstants.SHIPMENT_LINE_KEY, sShipmentLineKey);
                            yfcEleCreateShipmentSerialNumRoot.setAttribute(TelstraConstants.SERIAL_NO, sSerialNumber);
                            yfcEleCreateShipmentSerialNumRoot.setAttribute(TelstraConstants.TRANSACTION_TYPE, TelstraConstants.SHIPMENT);
                            invokeYantraService(TelstraConstants.API_CREATE_SHIPMENT_SERIAL_NUMBER, yfcDocCreateShipmentSerialNum);
                        }
                    }
                }
            }
        }
    }
    
    /**
     * This Method will do the inventory adjustment for the serials which has been shipped for the VECTOR Inventory.
     * @param inXmlDoc2
     */
    private void adjustInventory(YFCDocument outputGetShipmentList) {
      //SHIPMENT       
      YFCDocument adjustInventoryInDoc = YFCDocument.getDocumentFor("<Items />");
      YFCNodeList<YFCElement> shipmentLines = outputGetShipmentList.getElementsByTagName("ShipmentLine");
      String shipNode = outputGetShipmentList.getDocumentElement().getElementsByTagName("Shipment").item(0).getAttribute("ShipNode");
      String shipmentNo = outputGetShipmentList.getDocumentElement().getElementsByTagName("Shipment").item(0).getAttribute("ShipmentNo");

      for(YFCElement shipmentLine : shipmentLines) {
        YFCElement itemEle = adjustInventoryInDoc.getDocumentElement().createChild("Item");
        itemEle.setAttribute("AdjustmentType", "ADJUSTMENT");
        itemEle.setAttribute("ItemID", shipmentLine.getAttribute("ItemID",""));
        itemEle.setAttribute("Quantity", shipmentLine.getAttribute("Quantity",""));
        itemEle.setAttribute("UnitOfMeasure", shipmentLine.getAttribute("UnitOfMeasure",""));
        itemEle.setAttribute("ShipNode", shipNode);
        itemEle.setAttribute("SupplyType", "ONHAND");
        
		//HUB-9415[START]
		/*Stamping the Prime Line No and Order Name for Inventory Audit*/

        itemEle.setAttribute("ReasonText", "Increasing the supply for Shipment Confirmation : : Line No = "
        		+shipmentLine.getAttribute(TelstraConstants.PRIME_LINE_NO,"") +" : : Source ID = "+ sOdrName);
		//HUB-9415[END]

        itemEle.setAttribute("Reference_1", "ShipmentNo:"+shipmentNo);
        itemEle.setAttribute("Reference_2", "ShipmentLineKey:"+shipmentLine.getAttribute("ShipmentLineKey"));
        adjustInventoryInDoc.getDocumentElement().appendChild(itemEle);
      }
      invokeYantraApi("adjustInventory", adjustInventoryInDoc);
    }
    

    /**
     * set the additional shipment info(s) which are got form input CEV message to the confirm shipment input 
     * @param inputApiDoc
     */
    private void addtionalDetails(YFCDocument inputApiDoc) {
        inputApiDoc.getDocumentElement().setAttributes(confirmShipAdditionalAtrMap);    
    }

    //HUB-6862[Start]

    /**
     * This method is to createShipment for the incoming lines by calling GpsPickOrderService. 
     * @param inXml
     * @param outputGetOrdListForComQry
     */
    private void createShipmentAndPickOrderLines(YFCDocument inXml, YFCDocument outputGetOrdListForComQry) {
    	YFCElement eleShipment=inXml.getDocumentElement();
    	YFCDocument docinXmlShipment = YFCDocument.getDocumentFor(eleShipment.toString());
    	String sOrderName=eleShipment.getAttribute("OrderName","");
    	YFCDocument inDocPickOrder = YFCDocument.getDocumentFor("<OrderRelease OrderName='"+sOrderName+"'>" +"<OrderLines />" +"</OrderRelease> ");
    	YFCElement inDocPickOrderEle =  inDocPickOrder.getDocumentElement();
    	for (YFCElement eleShipmentLine : docinXmlShipment.getElementsByTagName("ShipmentLine")) {
    		String primeLineNo = eleShipmentLine.getAttribute("PrimeLineNo");
    		String shipNode = XPathUtil.getXpathAttribute(outputGetOrdListForComQry, "//OrderLine[@PrimeLineNo='"+primeLineNo+"']/@ShipNode");
    		String unitOfMeasure = XPathUtil.getXpathAttribute(outputGetOrdListForComQry, "//OrderLine[@PrimeLineNo='"+primeLineNo+"']/Item/@UnitOfMeasure");
    		inDocPickOrderEle.setAttribute("ShipNode", shipNode);
    		YFCElement eleReleases = inDocPickOrderEle.getChildElement("OrderLines");
    		YFCElement orderLineInpEle = inDocPickOrder.createElement("OrderLine");
    		Map<String, String> attributesMap = eleShipmentLine.getAttributes();
    		orderLineInpEle.setAttribute("UnitOfMeasure", unitOfMeasure);
    		orderLineInpEle.setAttributes(attributesMap);
    		eleReleases.appendChild(orderLineInpEle);
    	}

    	logger.verbose("Input for GpsPickOrder from ShipOrder class: " + docinXmlShipment.getDocumentElement().toString());
    	YFCDocument outputApiDoc = invokeYantraService("GpsPickOrder", inDocPickOrder);   
    	if(!YFCObject.isNull(outputApiDoc)){
    	    YFCNodeList<YFCElement> shipments = outputApiDoc.getElementsByTagName("Shipment");
    	    for(YFCElement shipment : shipments) {
      	      if(!YFCObject.isNull(shipment.getAttribute("ShipmentKey"))) {
                  setShipmentKey(XPathUtil.getXpathAttribute(outputApiDoc, "//@ShipmentKey"));
                  confirmShipment();
              }
    	    }
    	}      
    }
    //HUB-6862[End]
    /**
     * This method calls get order list and the return the op document
     * @param inEle
     * @return
     */
    YFCDocument getOrderList(YFCElement inEle) {
    	YFCDocument inDocgetOrdList = YFCDocument.getDocumentFor("<Order>" + 
    			"<ComplexQuery Operator='OR'><And><Or>" + 
    			"<Exp Name='DocumentType' QryType='EQ' Value='0001'/>" + 
    			"<Exp Name='DocumentType' QryType='EQ' Value='0003'/>" +
    			"<Exp Name='DocumentType' Value='0006' QryType='EQ'/></Or>" +
    			"<Exp Name='OrderName' Value='" + inEle.getAttribute("OrderName") + "' QryType='EQ'/>" + "</And>" + "</ComplexQuery>" + 
    			"</Order>");
    	YFCDocument templateForGetOrdList = YFCDocument.getDocumentFor("<OrderList>" +
    			"<Order DocumentType='' OrderHeaderKey='' HoldFlag='' EntryType='' >" +
    			"<OrderLines>" +
    			"<OrderLine PrimeLineNo='' SubLineNo='' OrderLineKey='' ShipNode='' Status=''> " +
    			"<Item ItemID='' UnitOfMeasure=''/>" +
    			"<OrderStatuses>" +
    			"<OrderStatus OrderReleaseKey='' Status='' StatusQty=''/>" +
    			"</OrderStatuses> " +
    			"</OrderLine>" +
    			"</OrderLines>" +
    			"</Order>" +
    			"</OrderList>");
    	YFCDocument outputGetOrdListForComQry = invokeYantraApi("getOrderList", inDocgetOrdList, templateForGetOrdList);
    	
        //HUB-9218[START]

        /* The below validation is removed to check if the Order is in hold or not*/

    	/*if ("Y".equals(XPathUtil.getXpathAttribute(outputGetOrdListForComQry, "//Order/@HoldFlag"))) {
    		LoggerUtil.verboseLog("Order Hold Validation Failure ", logger, TelstraConstants.THROW_EXCEPTION);
    		String strErrorCode = getProperty("OrderHoldValidation");
    		throw ExceptionUtil.getYFSException(strErrorCode, new YFSException());
    	}*/
    	
        //HUB-9218[START]

    	return outputGetOrdListForComQry;
    }

    /**
     * Change release for the input lines according to the release key map generated 
     */
    void releaseChange() {
        YFCDocument outTempForChangeRelease = YFCDocument.getDocumentFor("<OrderRelease OrderHeaderKey='' DocumentType='' EnterpriseCode='' OrderReleaseKey='' ReleaseNo=''/>");
        invokeYantraApi("changeRelease", orderReleaseDoc, outTempForChangeRelease);
    }

    /**
     * Schedule the order with ScheduleAndRelease 'Y' and CheckInventory 'N'
     */
    private void scheduleOrder() {
        String ordHdrKey = getOrderHeaderKey();
        YFCDocument inDocForSchOrd = YFCDocument.getDocumentFor("<ScheduleOrder ScheduleAndRelease='Y' IgnoreReleaseDate='Y' CheckInventory='N' OrderHeaderKey='" + ordHdrKey + "' />");
        invokeYantraApi("scheduleOrder", inDocForSchOrd);
    }

    /**
     * This method to ensure whether orderNo is unique across SO and TO
     * @param outputGetOrdListForComQry
     * @param inEle
     */
    void uniqueOrderValidation(YFCDocument outputGetOrdListForComQry,YFCElement inEle) {
        
        String docType = XPathUtil.getXpathAttribute(outputGetOrdListForComQry, "//Order/@DocumentType");
        setOrderHeaderKey(XPathUtil.getXpathAttribute(outputGetOrdListForComQry, "//Order/@OrderHeaderKey"));
        int ordLineCount = outputGetOrdListForComQry.getDocumentElement().getElementsByTagName("OrderLine").getLength();
        int inMsgNoOfLines = inEle.getElementsByTagName("ShipmentLine").getLength();
        if(ordLineCount!=inMsgNoOfLines){
            YFCElement inShipLinesEle = inEle.getChildElement("ShipmentLines",true);
            YFCNodeList<YFCElement> ordLineEleList = outputGetOrdListForComQry.getDocumentElement().getElementsByTagName("OrderLine");
            for (YFCElement ordLineElement : ordLineEleList) {
                // new
                
                String sShipNode = ordLineElement.getAttribute(TelstraConstants.SHIP_NODE);
                if(!YFCCommon.equalsIgnoreCase(sShipNode, sIncomingShipNode)){
                 continue;
                }
                
                
                String tPlineNo = ordLineElement.getAttribute("PrimeLineNo");
                YFCElement matchingEle = XPathUtil.getXPathElement(YFCDocument.getDocumentFor(inEle.getString()), "//ShipmentLines/ShipmentLine[@PrimeLineNo='"+tPlineNo+"']");
                if(XmlUtils.isVoid(matchingEle)){
                    String tItemID = XPathUtil.getXpathAttribute(YFCDocument.getDocumentFor(ordLineElement.getString()), "//Item/@ItemID");
                    YFCElement tEle = inShipLinesEle.createChild("ShipmentLine");
                    tEle.setAttribute("PrimeLineNo", tPlineNo);
                    tEle.setAttribute("ItemID", tItemID);
                    tEle.setAttribute("Action", "BACKORDER");
                    tEle.setAttribute("StatusQuantity", "0");
                    tEle.setAttribute("SubLineNo", "1");
                }

            }
        }

        if ("0006".equals(docType)) {
            setSOFlag(false);
        }

    }

    public boolean isSOFlag() {
        return soFlag;
    }

    public void setSOFlag(boolean sOFlag) {
        soFlag = sOFlag;
    }

    public String getOrderHeaderKey() {
        return orderHeaderKey;
    }

    public void setOrderHeaderKey(String orderHeaderKEY) {
        orderHeaderKey = orderHeaderKEY;
    }

    public String getShipmentKey() {
        return shipmentKey;
    }

    public void setShipmentKey(String shipmentKey) {
        this.shipmentKey = shipmentKey;
    }

    public YFCDocument getShipmentLineListDoc() {
        return shipmentLineListDoc;
    }

    public void setShipmentLineListDoc(YFCDocument shipmentLineListDoc) {
        this.shipmentLineListDoc = shipmentLineListDoc;
    }
    
    public String getTranID() {
      return tranID;
    }
    
    public void setTranID(String tranID) {
      this.tranID = tranID;
    }
    
    public boolean isSoFlag() {
      return soFlag;
    }

    public void setSoFlag(boolean soFlag) {
      this.soFlag = soFlag;
    }


    public String getBaseDropStatus() {
      return baseDropStatus;
    }


    public void setBaseDropStatus(String baseDropStatus) {
      this.baseDropStatus = baseDropStatus;
    }
}
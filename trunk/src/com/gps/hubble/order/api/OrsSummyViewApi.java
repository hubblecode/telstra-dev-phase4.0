package com.gps.hubble.order.api;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.yantra.yfc.date.YTimestamp;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSException;

public class OrsSummyViewApi extends AbstractCustomApi{
	private static YFCLogCategory logger = YFCLogCategory.instance(OrsSummyViewApi.class);
	@Override
	public YFCDocument invoke(YFCDocument inXml) throws YFSException {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", inXml);
		try{
			Map<String, String> attributeColumnMap = getAttributeColumnMap();
			Set<String> selectAttributes = new HashSet<>();
			String attributeList = getProperty("GroupByAttributes",true);
			if(!XmlUtils.isVoid(attributeList)){
			  if(!attributeList.contains("NoOfOrders")){
					YFCDocument erroDoc = ExceptionUtil.getYFSExceptionDocument("TEL_ERR_13061_001","Aggregate Attribute Not Provided.");
					throw new YFSException(erroDoc.toString());	
			}
			  String[] attributes = attributeList.split(",");
			  for (String attribute : attributes) {
			    selectAttributes.add(attribute);
				}
			}
			if(selectAttributes != null && !selectAttributes.isEmpty()){
				String query = getQuery(inXml.getDocumentElement().getAttribute("DocumentType"));
				logger.debug("query is "+query);
				Connection connection = getDBConnection();
				YFCDocument docOutput = null;
				try (PreparedStatement ps = connection.prepareStatement(query);
						ResultSet rs = ps.executeQuery()){
					docOutput =  constructDocument(rs,selectAttributes, attributeColumnMap);
				}catch(SQLException sqlEx){
					logger.error("SqlException while executing query ", sqlEx);
					throw new YFCException(sqlEx);
				}

				LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", docOutput);
				return  docOutput;
			}else{
				YFCDocument docOutput =  invokeYantraService("GpsGetOrsSummaryList", inXml);
				LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", docOutput);
				return docOutput;
			}

		}catch(Exception Ex){
			logger.error("SqlException while executing query ", Ex);
			throw new YFCException(Ex);
		}	
	}


	private Set<String> getSelectAttributes(YFCDocument inXml,Map<String, String> attributeColumnMap){
		Set<String> selectAttributes = new HashSet<>();
		if(inXml.getDocumentElement() == null || inXml.getDocumentElement().getChildElement("Attributes") == null){
			return selectAttributes;
		}
		YFCElement elemSelect = inXml.getDocumentElement().getChildElement("Attributes");
		if(elemSelect.getChildren("Attribute") == null){
			return selectAttributes;
		}
		for(Iterator<YFCElement> itr = elemSelect.getChildren("Attribute").iterator();itr.hasNext();){
			YFCElement elemAttribute = itr.next();
			String attribute = elemAttribute.getAttribute("Name");
			if(!attributeColumnMap.containsKey(attribute)){
				throw new YFCException(); 
			}
			selectAttributes.add(attribute);
		}
		return selectAttributes;
	}

	private String getQuery(Set<String> selectAttributes,Map<String, String> attributeColumnMap){
		StringBuilder builder = new StringBuilder();
		builder.append(getSelect(selectAttributes, attributeColumnMap));
		builder.append(" FROM GPS_ORS_SUMMARY ");
		builder.append(getGroupBy(selectAttributes, attributeColumnMap));
		return builder.toString();
	}
	
	private String getQuery(String documentType){
	  /*String sql = "select gos.DOCUMENT_TYPE AS DOCUMENT_TYPE,gos.STATUS AS STATUS,s.DESCRIPTION AS STATUS_DESCRIPTION, SUM(gos.NO_OF_ORDERS) AS NO_OF_ORDERS"  
          + " from GPS_ORS_SUMMARY gos, yfs_status s, YFS_PROCESS_TYPE pt"
          + " where gos.status = s.status and "
          + " pt.PROCESS_TYPE_KEY = s.PROCESS_TYPE_KEY "
          + " and gos.DOCUMENT_TYPE= pt.DOCUMENT_TYPE "
          + " and pt.DOCUMENT_TYPE='"+documentType+"' "
          + " and pt.BASE_PROCESS_TYPE='ORDER_FULFILLMENT' "
          + " group by gos.DOCUMENT_TYPE,gos.STATUS,s.DESCRIPTION "; */
		String sql = "select gos.DOCUMENT_TYPE AS DOCUMENT_TYPE,gos.STATUS AS STATUS,gos.STATUS_DESC AS STATUS_DESCRIPTION, SUM(gos.NO_OF_ORDERS) AS NO_OF_ORDERS, MAX(gos.CREATETS) AS UPDATED_DATE"  
		          + " from GPS_ORS_SUMMARY gos"
		          + " where "
		          + " gos.DOCUMENT_TYPE='"+documentType+"' "
		          + " group by gos.DOCUMENT_TYPE,gos.STATUS,gos.STATUS_DESC ";		
	  return sql; 
	}

	private String getSelect(Set<String> selectAttributes,Map<String, String> attributeColumnMap){
		StringBuilder builder = new StringBuilder();
		builder.append("SELECT ");
		boolean isFirst = true;
		for(String attribute : selectAttributes){
			if(!isFirst){
				builder.append(",");
			}
			String columnName = attributeColumnMap.get(attribute);
			if(attribute.equals("NoOfOrders")){
				builder.append("SUM(NO_OF_ORDERS) AS NO_OF_ORDERS");
			}else if(attribute.equals("TotalAmount")){
				builder.append("SUM(TOTAL_AMOUNT) AS TOTAL_AMOUNT");
			}else if(attribute.equals("UpdatedDate")) {
				builder.append("MAX(CREATETS) AS UPDATE_DATE");
			}else{
				builder.append(columnName);
			}
			isFirst = false;
		}
		return builder.toString();
	}

	private String getGroupBy(Set<String> selectAttributes,Map<String, String> attributeColumnMap){
		StringBuilder builder = new StringBuilder();
		builder.append("GROUP BY ");
		boolean isFirst = true;
		for(String attribute : selectAttributes){
			String columnName = attributeColumnMap.get(attribute);
			if(!attribute.equals("NoOfOrders") && !attribute.equals("TotalAmount") && !attribute.equals("UpdatedDate")){
				if(!isFirst){
					builder.append(",");
				}
				builder.append(columnName);
				isFirst = false;
			}

		}
		return builder.toString();
	}

	private Map<String, String> getAttributeColumnMap(){
		Map<String, String> attributeMap = new HashMap<String, String>();
		attributeMap.put("OrderType", "ORDER_TYPE");
		attributeMap.put("DocumentType", "DOCUMENT_TYPE");
		attributeMap.put("Status", "STATUS");
		attributeMap.put("StatusDescription", "STATUS_DESCRIPTION");
		attributeMap.put("NoOfOrders", "NO_OF_ORDERS");
		attributeMap.put("UpdatedDate", "UPDATE_DATE");
		attributeMap.put("TotalAmount", "TOTAL_AMOUNT");
		return attributeMap;
	}


	private YFCDocument constructDocument(ResultSet rs, Set<String> attributes, Map<String, String> attributeColumnMap) throws SQLException{ 
		YFCDocument docOrsSummaryList = YFCDocument.createDocument("OrsSummaryList");
		YFCElement elemOrsSummaryList = docOrsSummaryList.getDocumentElement();
		while(rs.next()){
			YFCElement elemOrsSummary = elemOrsSummaryList.createChild("OrsSummary");
			if(attributes.contains("OrderType")){
				String orderType = rs.getString("ORDER_TYPE");
				elemOrsSummary.setAttribute("OrderType", orderType);
			}
			if(attributes.contains("DocumentType")){
				String documentType = rs.getString("DOCUMENT_TYPE");
				elemOrsSummary.setAttribute("DocumentType", documentType);
			}
			if(attributes.contains("Status")){
				String status = rs.getString("STATUS");
				elemOrsSummary.setAttribute("Status", status);
			}
			if(attributes.contains("StatusDescription")){
				String statusDesc = rs.getString("STATUS_DESCRIPTION");
				elemOrsSummary.setAttribute("StatusDescription", statusDesc);
			}			
			if(attributes.contains("NoOfOrders")){
				long noOfOrders = rs.getLong("NO_OF_ORDERS");
				elemOrsSummary.setAttribute("NoOfOrders", noOfOrders);
			}
			if(attributes.contains("TotalAmount")){
				double totalAmount = rs.getDouble("TOTAL_AMOUNT");
				elemOrsSummary.setAttribute("TotalAmount", totalAmount);
			}
			if(attributes.contains("UpdatedDate")){
				Timestamp updatedSqlDate = rs.getTimestamp("UPDATED_DATE");
				YTimestamp updatedDate= YTimestamp.newMutableTimestamp(updatedSqlDate);
				elemOrsSummary.setDateTimeAttribute("UpdatedDate", updatedDate);
			}			
		}
		return docOrsSummaryList;
	}

}

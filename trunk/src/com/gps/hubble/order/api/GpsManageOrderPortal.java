/***********************************************************************************************
 * File	Name		: ManageOrder.java
 *
 * Description		: This class is called from General as a service. 
 * 						It mainly Creates or Updates Order related Information. 
 *                      It changes Shipment, Order and OrderLine Statuses.
 * 						It Validates BackOrder Cancel and Deliver.
 * 
 * Modification	Log	:
 * ---------------------------------------------------------------------------------------------
 * Ver #	Date			Author					Modification


 * ---------------------------------------------------------------------------------------------
 * 1.0		Jul 10,2016	  	Keerthi Yadav 		   	Initial	Version 
 * ---------------------------------------------------------------------------------------------
 * 
 */

package com.gps.hubble.order.api;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;


/**
 * Custom API to create or modify order from sterling UI portal
 * 
 * @author Sambeet Mohanty
 * @version 1.0
 *
 * Extends AbstractCustomApi Class
 */

public class GpsManageOrderPortal extends AbstractCustomApi {
	private static YFCLogCategory logger = YFCLogCategory.instance(GpsManageOrderPortal.class);	

	@Override
	public YFCDocument invoke(YFCDocument inXml) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), 
				"invoke", inXml);

		YFCDocument retDoc= YFCDocument.createDocument("Order");
		retDoc.getDocumentElement().setAttributes(inXml.getDocumentElement().getAttributes());

		YFCNodeList<YFCNode> nodeElements= inXml.getDocumentElement().getChildNodes();
		for (int i=0; i< nodeElements.getLength(); i++){
			YFCNode node=nodeElements.item(i).cloneNode(true);
			retDoc.getDocumentElement().importNode(node);
		}

		logger.verbose("Input to GpsManageOrder Service"+retDoc);
		YFCDocument manageOrderOutDoc= invokeYantraService("GpsCreateOrEditOrder", retDoc);

		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", inXml);
		return manageOrderOutDoc;
	}
}

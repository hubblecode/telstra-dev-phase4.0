/***********************************************************************************************
 * File Name : ManageOrder.java
 *
 * Description : This class is called from General as a service. It mainly Creates or Updates Order
 * related Information. It changes Shipment, Order and OrderLine Statuses. It Validates BackOrder
 * Cancel and Deliver.
 * 
 * Modification Log :
 * --------------------------------------------------------------------------------------------- Ver
 * # Date Author Modification
 * --------------------------------------------------------------------------------------------- 1.0
 * Jul 10,2016 Keerthi Yadav Initial Version
 * ---------------------------------------------------------------------------------------------
 * 
 */

package com.gps.hubble.order.api;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;

/**
 * Custom API to get list of Orders in Telstra Portal
 * 
 * @author Jyothi Garg
 * @version 1.0
 * 
 *          Extends AbstractCustomApi Class
 */

public class GpsGetOrderList extends AbstractCustomApi {
	private static YFCLogCategory logger = YFCLogCategory.instance(GpsGetOrderList.class);

	@Override
	public YFCDocument invoke(YFCDocument inXml) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(),"invoke", inXml);
		String loginId = inXml.getDocumentElement().getAttribute(TelstraConstants.LOGIN_ID, "");
		String templateId = inXml.getDocumentElement().getAttribute(TelstraConstants.TEMPLATE_ID);
		if(YFCCommon.isVoid(templateId)) {
			templateId = getProperty(TelstraConstants.TEMPLATE_ID);
		} 
		inXml.getDocumentElement().removeAttribute(TelstraConstants.TEMPLATE_ID);
		if (!YFCCommon.isVoid(loginId)) {
			String teamId = getTeamId(loginId);
			if (!YFCCommon.isVoid(teamId)) {
				YFCDocument getTeamListOutput = getTeamDetailsMethod(teamId);
				if (!YFCCommon.isVoid(getTeamListOutput)) {
					YFCElement teamDocEle = getTeamListOutput.getDocumentElement();
					YFCElement teamEle = teamDocEle.getChildElement("Team");
					String shipNodeAccess = teamEle.getAttribute("ShipNodeAccessMode");
					String docTypeAccess = teamEle.getAttribute("DocumentTypeAccessMode");
					List<String> docList = new ArrayList<>();
					List<String> orgList = new ArrayList<>();
					
					if(!YFCCommon.isVoid(docTypeAccess) && "02".equalsIgnoreCase(docTypeAccess)) {
						docList = getTeamDocumentTypeList(teamEle);
						appendOrderLevelComplexQuery(inXml, docList);
					}
					if (!YFCCommon.isVoid(shipNodeAccess)) {
						if ("02".equalsIgnoreCase(shipNodeAccess)) {
							String sNode = inXml.getDocumentElement().getAttribute(TelstraConstants.ORGANIZATION_CODE,"");
							//If the OrganizationCode is Telstra SCLSA and ShipNode Access Mode is all it means that the user has access to all Nodes
							if(!TelstraConstants.ORG_TELSTRA_SCLSA.equals(sNode)) {
								orgList.add(sNode);
							}							
							appendOrderLineLevelComplexQuery(inXml, orgList);
						} else if ("03".equalsIgnoreCase(shipNodeAccess)) {
							YFCElement teamNodeList = teamEle.getChildElement("TeamNodesList");
							YFCIterable<YFCElement> teamNdList = teamNodeList.getChildren("TeamNodes");
							for (YFCElement teamNode : teamNdList) {
								orgList.add(teamNode.getAttribute("ShipnodeKey"));
							}
							appendOrderLineLevelComplexQuery(inXml, orgList);
						}else {
							//ShipNode Access Mode 01, All access should be provided
							appendOrderLineLevelComplexQuery(inXml, null);
						}
					}
				}
			}
		} 
		// Adding logic here for Non Telstra Portal
		else {
			appendOrderLineLevelComplexQuery(inXml, null);
		}
		// This method is not required
		/*if (!YFCCommon.isVoid(shipToId)) {
			appendOrderLineLevelComplexQueryDaq(inXml,shipToId)
		}*/
		
		LoggerUtil.verboseLog("getOrderList Input", logger, inXml);

		Document doc = inXml.getDocument();
		doc.renameNode(doc.getDocumentElement(), doc.getNamespaceURI(), TelstraConstants.ORDER);
		YFCDocument inputDoc = YFCDocument.getDocumentFor(doc);

		YFCDocument getOrderListTemplate = getOutputTemplate(templateId);
		YFCDocument getOrderListOutput = invokeYantraApi("getOrderList",inputDoc, getOrderListTemplate);
		LoggerUtil.verboseLog("getOrderList Output", logger, getOrderListOutput);
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke",getOrderListOutput);
		return getOrderListOutput;
	}

	private List<String> getTeamDocumentTypeList(YFCElement teamEle) {
		List<String> docList = new ArrayList<>();
		YFCElement docTypeList = teamEle.getChildElement("TeamDocTypeList");
		YFCIterable<YFCElement> docTyList = docTypeList.getChildren("TeamDocType");
		for (YFCElement docType : docTyList) {
			String documentType = docType.getAttribute(TelstraConstants.DOCUMENT_TYPE);
			if (!docList.contains(documentType)) {
				docList.add(documentType);
			}
		}
		return docList;
	}

	private YFCDocument getTeamDetailsMethod(String teamId) {
		YFCDocument getTeamListInput = YFCDocument.getDocumentFor("<Team TeamId='' />");
		getTeamListInput.getDocumentElement().setAttribute(TelstraConstants.TEAM_ID, teamId);
		return invokeYantraApi(TelstraConstants.API_GET_TEAM_LIST , getTeamListInput);
	}

	private String getTeamId(String loginId) {
		YFCDocument getUserListInput = YFCDocument.getDocumentFor("<User Loginid='' />");
		getUserListInput.getDocumentElement().setAttribute(TelstraConstants.LOGIN_ID , loginId);

		YFCDocument getUserListTemplate = YFCDocument.getDocumentFor("<UserList> <User DataSecurityGroupId='' Loginid='' OrganizationKey=''/> </UserList>");
		YFCDocument getUserListOutput = invokeYantraApi(TelstraConstants.API_GET_USER_LIST ,getUserListInput, getUserListTemplate);
		if (getUserListOutput != null) {
			return XPathUtil.getXpathAttribute(getUserListOutput,"//User/@DataSecurityGroupId");
		}
		return null;
	}

	private void appendOrderLineLevelComplexQuery(YFCDocument inXml,List<String> orgNameList) {
		YFCElement orderEle = inXml.getDocumentElement();
		YFCElement orderLineEle = orderEle.getChildElement(TelstraConstants.ORDER_LINE,true);
		
		YFCElement complexQryDocEle = null; 
		YFCElement andEle = null;
		YFCElement shipToOrEle = null;
		YFCElement dsgOrEle = null;
		// We are adding the ShipToID logic within this method
		String shipToId = orderEle.getAttribute(TelstraConstants.SHIP_TO_ID);
		//Removing this attribute as we don't want this to be passed to getOrderList
		orderEle.removeAttribute(TelstraConstants.SHIP_TO_ID);
		// Need to check if YFCCommon can be used.. This is not a documented class
		if (orgNameList != null && !orgNameList.isEmpty()) {
			// Adding Complex Query Node only if OrgNameList is passed 
			if(!YFCCommon.isVoid(shipToId)) {
				complexQryDocEle = orderLineEle.createChild(TelstraConstants.COMPLEX_QUERY);
				// If ShipToId is passed the logic is different
				complexQryDocEle.setAttribute(TelstraConstants.OPERATOR, TelstraConstants.AND);
				andEle = complexQryDocEle.createChild("And");
				shipToOrEle = andEle.createChild(TelstraConstants.OR);
				YFCElement expShipToIdEle = shipToOrEle.createChild(TelstraConstants.EXP);
				expShipToIdEle.setAttribute(TelstraConstants.NAME, TelstraConstants.SHIP_TO_ID);
				expShipToIdEle.setAttribute(TelstraConstants.QRY_TYPE, "LIKE");
				expShipToIdEle.setAttribute(TelstraConstants.VALUE,shipToId);
				YFCElement expCustKeyEle = shipToOrEle.createChild(TelstraConstants.EXP);
				expCustKeyEle.setAttribute(TelstraConstants.NAME, "Extn_"+TelstraConstants.CUSTOMER_KEY);
				expCustKeyEle.setAttribute(TelstraConstants.QRY_TYPE, "LIKE");
				expCustKeyEle.setAttribute(TelstraConstants.VALUE,shipToId);
				dsgOrEle = andEle.createChild(TelstraConstants.OR);
			} else {
				complexQryDocEle = orderLineEle.createChild(TelstraConstants.COMPLEX_QUERY);
				complexQryDocEle.setAttribute(TelstraConstants.OPERATOR, TelstraConstants.AND);
				dsgOrEle = complexQryDocEle.createChild(TelstraConstants.OR);	
			}
			
			
			for (String orgName : orgNameList) {
				if (!YFCCommon.isVoid(orgName)) {
					YFCElement expShipNodeEle = dsgOrEle.createChild(TelstraConstants.EXP);
					expShipNodeEle.setAttribute(TelstraConstants.NAME, TelstraConstants.SHIP_NODE);
					expShipNodeEle.setAttribute(TelstraConstants.QRY_TYPE, "EQ");
					expShipNodeEle.setAttribute(TelstraConstants.VALUE, orgName);
					YFCElement expReceivingNodeEle = dsgOrEle.createChild(TelstraConstants.EXP);
					expReceivingNodeEle.setAttribute(TelstraConstants.NAME, TelstraConstants.RECEIVING_NODE);
					expReceivingNodeEle.setAttribute(TelstraConstants.QRY_TYPE, "EQ");
					expReceivingNodeEle.setAttribute(TelstraConstants.VALUE , orgName);
				}
			}
		} else {
			//This code will be called for non Telstra Portal.. 
			//TODO: Most of this code is a repeat of the above code. These can be refactored into a separate method
			if(!YFCCommon.isVoid(shipToId)) {
				//If ShipToId is passed we will add the additional node..
				complexQryDocEle = orderLineEle.createChild(TelstraConstants.COMPLEX_QUERY);
				// If ShipToId is passed the logic is different
				complexQryDocEle.setAttribute(TelstraConstants.OPERATOR, TelstraConstants.AND);
				YFCElement orElem = complexQryDocEle.createChild(TelstraConstants.OR);
				YFCElement expShipToIdEle = orElem.createChild(TelstraConstants.EXP);
				expShipToIdEle.setAttribute(TelstraConstants.NAME, TelstraConstants.SHIP_TO_ID);
				expShipToIdEle.setAttribute(TelstraConstants.QRY_TYPE, "LIKE");
				expShipToIdEle.setAttribute(TelstraConstants.VALUE,shipToId);
				
				YFCElement expCustKeyEle = orElem.createChild(TelstraConstants.EXP);
				expCustKeyEle.setAttribute(TelstraConstants.NAME, "Extn_"+TelstraConstants.CUSTOMER_KEY);
				expCustKeyEle.setAttribute(TelstraConstants.QRY_TYPE, "LIKE");
				expCustKeyEle.setAttribute(TelstraConstants.VALUE,shipToId);				
			} 			
		}
	}
	
	
	private void appendOrderLevelComplexQuery(YFCDocument inXml, List<String> docList) {
		YFCElement orderEle = inXml.getDocumentElement();
		
		if (docList != null && !docList.isEmpty()) {
			YFCElement complexQryDocEle = orderEle.createChild(TelstraConstants.COMPLEX_QUERY);
			complexQryDocEle.setAttribute(TelstraConstants.OPERATOR, TelstraConstants.AND);
			YFCElement orEle = complexQryDocEle.createChild(TelstraConstants.OR);
			for (String docType : docList) {
				if (!YFCCommon.isVoid(docType)) {
					YFCElement expDocTypeEle = orEle.createChild(TelstraConstants.EXP);
					expDocTypeEle.setAttribute(TelstraConstants.NAME, TelstraConstants.DOCUMENT_TYPE);
					expDocTypeEle.setAttribute(TelstraConstants.QRY_TYPE, "EQ");
					expDocTypeEle.setAttribute(TelstraConstants.VALUE, docType);
				}
			}
		}
	}
	
	//TODO.. This method can either be taken out or refactored 
	/*private void appendOrderLineLevelComplexQueryDaq(YFCDocument inXml,String shipToId) {
		YFCElement orderLineEle = inXml.getDocumentElement().getChildElement(TelstraConstants.ORDER_LINE,true);
		if(!(orderLineEle.ChildElement(TelstraConstants.EXTN,true))){
			YFCElement extnEle = orEle.createChild(TelstraConstants.EXTN);
			extnEle.setAttribute(TelstraConstants.CUSTOMER_KEY,shipToId);
			
		}
		YFCElement complexQryDocEle = orderLineEle.createChild(TelstraConstants.COMPLEX_QUERY);
		complexQryDocEle.setAttribute(TelstraConstants.OPERATOR, TelstraConstants.AND);
		YFCElement orEle = complexQryDocEle.createChild(TelstraConstants.OR);

					YFCElement expShipToIdEle = orEle.createChild(TelstraConstants.EXP);
					expShipNodeEle.setAttribute(TelstraConstants.NAME, TelstraConstants.SHIP_TO_ID);
					expShipNodeEle.setAttribute(TelstraConstants.QRY_TYPE, "LIKE");
					expShipNodeEle.setAttribute(TelstraConstants.VALUE,shipToId);

					
		} */
	
	private YFCDocument getOutputTemplate(String templateId) {
		//String templateId = getProperty(TelstraConstants.TEMPLATE_ID);
		
		String apiName = getProperty(TelstraConstants.API_NAME);
		YFCDocument getApiTemplateInput = YFCDocument.getDocumentFor("<ApiTemplate OrganizationCode='DEFAULT' TemplateType='00' ApiName='' TemplateId='' />");
		getApiTemplateInput.getDocumentElement().setAttribute(TelstraConstants.TEMPLATE_ID , templateId);
		getApiTemplateInput.getDocumentElement().setAttribute(TelstraConstants.API_NAME , apiName);
		YFCDocument getApiTemplateOutput = invokeYantraApi("getApiTemplateDetails", getApiTemplateInput);
		if(!YFCCommon.isVoid(getApiTemplateOutput)){
			YFCElement templateDataEle = getApiTemplateOutput.getDocumentElement().getChildElement("TemplateData");
			return YFCDocument.getDocumentFor(templateDataEle.getFirstChildElement().toString());
		}
		return null;
	}

}

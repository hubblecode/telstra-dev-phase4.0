/***********************************************************************************************
 * File	Name		: GpsAddWaitTimeToManageOrder.java
 *
 * Description		: This class is called from GpsManageOrder service
 * 					The purpose of this class to make sure multiple orders is not created with the same order name
 * 
 * Modification	Log	:
 * -------------------------------------------------------------------------------------------------------------------------------------
 * Ver #	Date			Author					Modification
 * -------------------------------------------------------------------------------------------------------------------------------------
 * 1.0		Jun 30,2017	  	Prateek Kumar 		   	Initial	Version 
 * -------------------------------------------------------------------------------------------------------------------------------------
 * 
 */
package com.gps.hubble.order.api;

import java.util.concurrent.TimeUnit;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.constants.TelstraErrorCodeConstants;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSException;

/**
 * 
 * @author Prateek
 *
 */
public class GpsAddWaitTimeToManageOrder extends AbstractCustomApi {

	private static YFCLogCategory logger = YFCLogCategory.instance(GpsAddWaitTimeToManageOrder.class);

	@Override
	public YFCDocument invoke(YFCDocument yfcInDoc) throws YFSException {

		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", yfcInDoc);

		String sOrderName = yfcInDoc.getDocumentElement().getAttribute(TelstraConstants.ORDER_NAME);
		if(!YFCCommon.isStringVoid(sOrderName)){
			boolean isOrderAlreadyCreated = callGetOrderListWithOrderName (sOrderName);
			LoggerUtil.verboseLog("GpsAddWaitTimeToManageOrder :: isOrderAlreadyCreated", logger, isOrderAlreadyCreated);
			if(!isOrderAlreadyCreated){
				boolean isOrderNameExist = callGpsGetSourceId(sOrderName);

				if (isOrderNameExist) {

					int iNoOfIteration = YFCCommon.intfromString(getProperty(TelstraConstants.ITERATION_COUNT, true));
					long lWaitTime = Long.valueOf(getProperty(TelstraConstants.WAIT_TIME, true));
					int i = 0;
					while (i < iNoOfIteration) {
						i++;
						try {
							TimeUnit.SECONDS.sleep(lWaitTime);
						} catch (InterruptedException e) {
							e.printStackTrace();
							throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.MANAGE_ORDER_UNABLE_TO_LOCK,
									"OrderName: " + sOrderName, e);
						}
						isOrderNameExist = callGpsGetSourceId(sOrderName);
						if (!isOrderNameExist) {
							LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", yfcInDoc);
							return yfcInDoc;
						}
					}
					throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.MANAGE_ORDER_UNABLE_TO_LOCK,
							"Source ID: " + sOrderName, new YFSException());
				}
				else{
					/* Insert record in Gps_source_id table*/
					YFCDocument yfcDocCreateSourceIdIp = YFCDocument.getDocumentFor("<SourceID SourceID='"+sOrderName+"'/>");
					invokeYantraService(TelstraConstants.SERVICE_GPS_CREATE_SOURCE_ID, yfcDocCreateSourceIdIp);			
				}
			}
		}
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", yfcInDoc);
		return yfcInDoc;
	}

	/**
	 * This method will check if the order is already created or not. If already created, it will skip all the subsequent line
	 * @param sOrderName
	 * @return
	 */
	private boolean callGetOrderListWithOrderName(String sOrderName) {

		boolean bRetFlag = true;
		YFCDocument yfcDocGetOrderListIp = YFCDocument.getDocumentFor("<Order OrderName='"+sOrderName+"'/>");
		YFCDocument yfcDocGetOrderListTemp = YFCDocument.getDocumentFor("<OrderList TotalNumberOfRecords=''/>");
		YFCDocument yfcDocGetOrderListOp = invokeYantraApi(TelstraConstants.GET_ORDER_LIST, yfcDocGetOrderListIp, yfcDocGetOrderListTemp);

		YFCElement yfcEleOrderList = yfcDocGetOrderListOp.getDocumentElement();
		double dTotalNoOfRecords = yfcEleOrderList.getDoubleAttribute(TelstraConstants.TOTAL_NUMBER_OF_RECORDS);
		if(dTotalNoOfRecords==0){
			bRetFlag = false;
		}
		return bRetFlag;
	}

	/**
	 * This method will fetch source id from Gps_source_id table. If the source id exist, it means some other thread 
	 * is in process of creating/updating an order with the same order name.
	 * @param sOrderName
	 * @return
	 */
	private boolean callGpsGetSourceId(String sOrderName) {

		boolean bRetFlag = false;
		YFCDocument yfcDocGetSourceIdIp = YFCDocument.getDocumentFor("<SourceID SourceID='"+sOrderName+"'/>");
		YFCDocument yfcDocGetSourceIdOp = invokeYantraService(TelstraConstants.SERVICE_GPS_GET_SOURCE_ID_LIST, yfcDocGetSourceIdIp);

		if(yfcDocGetSourceIdOp.getDocumentElement().hasChildNodes()){
			bRetFlag = true;
		}		

		return bRetFlag;
	}

}

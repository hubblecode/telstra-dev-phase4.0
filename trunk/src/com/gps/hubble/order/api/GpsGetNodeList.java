/**
 * 
 */
package com.gps.hubble.order.api;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.log.YFCLogCategory;

/**
 * Custom API to populate shipfrom location list for Order search screen  
 * @author Sambeet Mohanty
 *
 */
public class GpsGetNodeList extends AbstractCustomApi {


	private static YFCLogCategory logger = YFCLogCategory.instance(GpsGetNodeList.class);
	/* (non-Javadoc)
	 * @see com.bridge.sterling.framework.api.AbstractCustomApi#invoke(com.yantra.yfc.dom.YFCDocument)
	 */
	@Override
	public YFCDocument invoke(YFCDocument inpDoc)  {
		try {
			LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke",inpDoc);

			String attri = getProperty("NodeTypeAttributes",true);
			String nodeinp= "<ShipNode><ComplexQuery Operator='OR'><Or>";

			if(!XmlUtils.isVoid(attri)){
				String[] s= attri.split(",");
				for(String  str:s){
					nodeinp= nodeinp + "<Exp Name='NodeType' QryType='EQ' Value='"+str+"'/>";
				}
			}

			YFCDocument	inDoc=YFCDocument.getDocumentFor(nodeinp + "</Or></ComplexQuery></ShipNode>");
			logger.verbose("getNodeList input document"+ inDoc);

			YFCDocument retdoc = invokeShipNodeListAPI(inDoc);
			return retdoc;
		}
		finally {
			LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke",inpDoc);
		}

	}

	public YFCDocument invokeShipNodeListAPI(YFCDocument inDoc)
	{
		YFCDocument retDoc= invokeYantraApi("getShipNodeList", inDoc, getAPITemplate());
		return retDoc;
	}

	public YFCDocument getAPITemplate()
	{
		return YFCDocument.getDocumentFor("<ShipNodeList><ShipNode ShipNode='' ShipnodeKey='' NodeType='' /></ShipNodeList>");
	}
}



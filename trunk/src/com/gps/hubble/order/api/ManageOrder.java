/***********************************************************************************************
 * File	Name		: ManageOrder.java
 *
 * Description		: This class is called from General as a service. 
 * 						It mainly Creates or Updates Order related Information. 
 *                      It changes Shipment, Order and OrderLine Statuses.
 * 						It Validates BackOrder Cancel and Deliver.
 * 
 * Modification	Log	:
 * --------------------------------------------------------------------------------------------------------------------------------------------------------------
 * Ver #	Date			Author					Modification


 * --------------------------------------------------------------------------------------------------------------------------------------------------------------
 * 1.0		Jul 10,2016	  	Keerthi Yadav 		   	Initial	Version 
 * 1.1		Jan 25,2017		Keerthi Yadav	        HUB-8346: Return 'OrderHeaderKey' attribute from GpsMangeOrder class
 * 1.2      Feb 01,2017     Keerthi Yadav           HUB-8358: Moving the freight charge line to a status that is an extended status to Cancelled (Freight Line Closed).
 * 1.3      Feb 17,2017		Keerthi Yadav			HUB-8442: Changes in ManageOrder for UI
 * 1.4      Feb 22,2017		Keerthi Yadav			HUB-8462: Entry Type STERLING change for order creation in UI
 * 1.5		Feb,27,2017		Prateek Kumar			HUB-8472: If person info ship to/shipToID is missing for INT_ODR_4, copy the receiving node address to person info ship to
 * 1.6		Mar 27,2017		Prateek Kumar			If Entry type is Vector, GpsManageVectorOrder.java is being called.
 * 1.6		Mar 30,2017		Prateek Kumar			HUB-8610: pass interface number while calling change order. It is required in before create order ue
 * 1.7		May 08,2017		Prateek Kumar			HUB-8886: Integral Plus Order Stream determination - Additional Logic
 * 1.8		May 16,2017		Prateek Kumar			HUB-9070 The issue was with the way back ordered Qty in the order was getting calculated. It was taking summation of all the back ordered qty in the order instead of only that line.
 * 1.9		May 17,2017		Prateek Kumar			HUB-9067: Returning back from Manage order code as message is coming from INT_ODR_1/INT_ODR_2 for PO
 * 2.0      May 29,2017		Keerthi Yadav			HUB-9139: AUDIT: Remove unwanted alerts and order monitoring rules
 * 2.1		May 30,2017		Prateek Kumar			HUB-9178: In change order for new line, if the transport order is for NBN then don't throw an exception
 * 2.2		May 30,2017		Prateek Kumar			HUB-9184: In order update, if ship to id or ship to key is present for existing line, remove the PersonInfoShipTo element
 * 2.3      Jun 01,2017     Keerthi Yadav 			HUB-9217: Same Recv Node and Ship Node for an Integral Plus Order
 * 2.4		Jun 06,2017		Prateek Kumar			HUB-9078: If the NBN order update comes from INT_ODR_1/2, ignore the message.
 * 2.5		Jun 06,2017		Prateek Kumar			HUB-9078: If the OrderName from IntegralPlus exists in the system and Source System for the order is VECTOR, ignore the message.
 * 2.6      Jun 08,2017		Keerthi Yadav			HUB-9294: Neway delivery LRA
 * 2.7     	Jun 16,2017		Keerthi Yadav			HUB-9372: Implementation of Delivered Quantity for INT_ODR_4 interface
 * 2.8  	Jun 16,2017  	Prateek Kumar   		HUB-9345: Orders are getting created without a delivery address code.
 * 2.9 		Jun 21,2017		Keerthi Yadav			HUB-9390: Stamping Requested Delivery date and Processing Delivery Qty for PMC Order coming from INT_ODR_4
 * 3.0		Jun 23,2017		Keerthi Yadav			HUB-9409: Invalid Person Info Ship To and Item while updating Lines for Purchase Orders from Interface
 * 3.1		Jun 29,2017		Prateek Kumar			Only material reservations created for CTs should be created as transfer orders.Everything else, should be treated as sales orders.
 * 3.2		Aug 03,2017		Prateek Kumar			HUB-9460 moved delivery instructions from shipto to order line
 * --------------------------------------------------------------------------------------------------------------------------------------------------------------
 * 
 */

package com.gps.hubble.order.api;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.constants.TelstraErrorCodeConstants;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSException;

/**
 * Custom API to create Hold for Duplicate Order
 * 
 * @author Keerthi Yadav
 * @version 1.0
 *
 *          Extends AbstractCustomApi Class
 */

public class ManageOrder extends AbstractCustomApi {
	private static YFCLogCategory logger = YFCLogCategory.instance(ManageOrder.class);
	private String sOrderName;
	private String sDepartmentCode;
	private String sOrderType="";
	private String sInterfaceNo="";

	@Override
	public YFCDocument invoke(YFCDocument inXml) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", inXml);
		sOrderName = inXml.getDocumentElement().getAttribute(TelstraConstants.ORDER_NAME, "");
		sInterfaceNo = inXml.getDocumentElement().getAttribute(TelstraConstants.INTERFACE_NO, "");
		String sEntryType = inXml.getDocumentElement().getAttribute(TelstraConstants.ENTRY_TYPE, "");
		sOrderType = inXml.getDocumentElement().getAttribute(TelstraConstants.ORDER_TYPE, "");
		sDepartmentCode = inXml.getDocumentElement().getAttribute(TelstraConstants.DEPARTMENT_CODE, "");


		if (YFCObject.isVoid(sOrderName)) {
			/*
			 * When OrderName is null get the OrderNo sequence and stamp the
			 * OrderNo and OrderName attribute as the next sequence. ' Should
			 * Prefix with Y? 100009011, Y100009012
			 */
			stampOrderNoAndName(inXml);

		}
		/*
		 * HUB-9460 Move delivery instructions from PersonInfoShipTo to OrderLine
		 */
		moveDeliveryInstrToOrderLine(inXml);
		/*
		 * Vector order creation
		 * 
		 */
		if(TelstraConstants.VECTOR.equalsIgnoreCase(sEntryType)){

			//HUB-9294[START]
			/* This check is made invoke another service to create Adhoc Logistics Order*/

			if(YFCObject.equals(TelstraConstants.ADHOC_LOGISTICS_UPPER_CASE, sOrderType)){
				YFCDocument outXml = invokeYantraService("GpsCreateVectorAdhocLogisticsOrder", inXml);
				return outXml;
			}else{	
				//HUB-9294[END]

				GpsManageVectorOrder obj = new GpsManageVectorOrder();
				YFCDocument yfcRtrnDoc = obj.manageVectorOrder(inXml, getServiceInvoker());
				return yfcRtrnDoc;
			}
		}

		YFCDocument yfcDocReturn = null;
		if (!YFCObject.isVoid(sOrderName)) {
			YFCDocument docGetOrderListOutXml = getOrderList(sOrderName);
			YFCElement eleOrderOut = docGetOrderListOutXml.getElementsByTagName(TelstraConstants.ORDER).item(0);

			if (!YFCObject.isVoid(eleOrderOut)) {

				
				//HUB-9409 - [START]
				
				/*The Order Lines which are getting updated for a PO are having special character in the PersonInfoShipTo element in the Order Line such. 
				 * There is also Item Field populated with wrong item ID. Therefore change has been made in manage Order class, to stamp the correct Item 
				 * and PersonInfoShipTo values for a Line by overriding the input with the already existing values for the line from the Order.*/
				
				String sDocType = eleOrderOut.getAttribute(TelstraConstants.DOCUMENT_TYPE,"");
				if(YFCObject.equals(TelstraConstants.DOCUMENT_TYPE_0005, sDocType)){
					copyPersonInfoShipAndItemToLine(inXml,docGetOrderListOutXml);
					/* HUB-9555: This method will copy the address from the receiving node to the new line added*/
					appendPersonInfoShipToFromRecNodeAddress(inXml, sInterfaceNo);
				}
				
				//HUB-9409 - [END]

				// HUB-9390 - [START]

				/*Stamping Requested Delivery date and Processing Delivery Qty for PMC Order coming from INT_ODR_4*/
				if ("INT_ODR_4".equals(sInterfaceNo) && "PMC".equals(sDepartmentCode)) {
					/*This will update the existing order line with the requested Delivery date and Process the Delivery Qty for the Incoming Line*/

					YFCDocument outXml = processUpdateForPMC(inXml,docGetOrderListOutXml,eleOrderOut,sEntryType);
					
					/*The change is done to update the boolean flags to display the fields in the UI. This is maintained in the
					 * YFS_ORDER_HEADER_EXTENSION table.*/
					
					GpsUpdateOrderExtnFields obj = new GpsUpdateOrderExtnFields();
					yfcDocReturn = obj.updateOrderExtnFields(YFCDocument.getDocumentFor("<Order OrderName='" + sOrderName + "' />"),getServiceInvoker());
					
					return outXml;
					
				}
				
				// HUB-9390 - [END]


				if("INT_ODR_1".equals(sInterfaceNo)||"INT_ODR_2".equals(sInterfaceNo)){

					/*
					 * HUB-9078 (from comments) If the NBN order update comes from INT_ODR_1/2, ignore the message. 
					 * As, it is expected that INT_ODR_4 will be sending these messages.
					 */
					String sSellerOrgCode = XPathUtil.getXpathAttribute(docGetOrderListOutXml, "//Order/@SellerOrganizationCode");
					if("NBN".equals(sSellerOrgCode)){
						LoggerUtil.verboseLog("Returning from ManageOrder as NBN order update is coming from INT_ODR_1/INT_ODR_2", logger, null);
						return inXml;
					}

					/*
					 * HUB-9078 (from comments) If the OrderName from IntegralPlus exists in the system and Source System for the order is VECTOR, ignore the message.
					 * If there is a failure in the pick message, it has to be handled manually during exception re-processing.
					 */
					String sEntryTypeFromOrderList = XPathUtil.getXpathAttribute(docGetOrderListOutXml, "//Order/@EntryType");
					if(TelstraConstants.VECTOR.equalsIgnoreCase(sEntryTypeFromOrderList)){
						LoggerUtil.verboseLog("Returning from ManageOrder as Vector order update is coming from INT_ODR_1/INT_ODR_2", logger, null);
						return inXml;
					}
				}
				/*
				 * HUB-9184 - In case of MR getting updated through vector orders. There could be scenario where the line has some qty in released and some in 
				 * back ordered status. In that case we will update the LRA address to this line. Now when the message from IP comes for the backordered qty,
				 * it will come with old address. Since the line is in released with LRA address, it can not be change. So removing the address will make sure
				 * it does not try to revert to old address 
				 * 
				 * Adding a check for void interface number as address can be updated from UI portal 
				 */
				if(!YFCCommon.isStringVoid(sInterfaceNo)){
					if("INT_ODR_1".equalsIgnoreCase(sInterfaceNo) || "INT_ODR_2".equalsIgnoreCase(sInterfaceNo)) {

						removePersonInfoShipToForExistingLine(inXml, docGetOrderListOutXml);

					}
				}
				/*
				 * HUB-9067 :Returning back from Manage order code as message is coming from INT_ODR_1/INT_ODR_2 for PO
				 */
				String sDocumentType = eleOrderOut.getAttribute(TelstraConstants.DOCUMENT_TYPE);				
				if (TelstraConstants.DOCUMENT_TYPE_0005.equalsIgnoreCase(sDocumentType)
						&& ("INT_ODR_1".equalsIgnoreCase(sInterfaceNo) || "INT_ODR_2".equalsIgnoreCase(sInterfaceNo))) {

					LoggerUtil.verboseLog("Returning back from Manage order code as message is coming from INT_ODR_1/INT_ODR_2 for PO", logger, inXml);
					return inXml;
				}

				/*
				 * if the order update is coming from Sterling portal, call GpsSterlingOrderUpdate class for only the existing line			  
				 */
				if (YFCCommon.isStringVoid(sInterfaceNo) && (YFCObject.equals(sOrderType, "MATERIAL_RESERVATION")
						|| YFCObject.equals(sOrderType, "TRANSPORT_ORDER"))) {

					processSterlingOrderUpdate(inXml, docGetOrderListOutXml);
				}
				/*
				 * if there is any new line in the order update message then only below piece of code will get executed
				 */
				if(inXml.getElementsByTagName(TelstraConstants.ORDER_LINE).getLength()!=0){
					/*
					 * For order update
					 */
					processChangeOrder(inXml, eleOrderOut, docGetOrderListOutXml,
							sInterfaceNo, sEntryType);

				}
			} else {

				
				// HUB-9390 - [START]
				/*This is to prevent Order creation for PMC order as they get created only from INT_ODR_3*/
				
				if ("INT_ODR_4".equals(sInterfaceNo) && "PMC".equals(sDepartmentCode)) {
					return inXml;
				}
				
				// HUB-9390 - [END]

				/*
				 * For new order creation
				 */
				
				createOrder(inXml);
				
				// HUB-8346 -[END]
			}
			// HUB-6859 - [Start]
			GpsUpdateOrderExtnFields obj = new GpsUpdateOrderExtnFields();
			yfcDocReturn = obj.updateOrderExtnFields(YFCDocument.getDocumentFor("<Order OrderName='" + sOrderName + "' />"),
					getServiceInvoker());
			// HUB-6859 - [End]
			LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", yfcDocReturn);
			return yfcDocReturn;
		}	
		return inXml;
	}

	/**
	 * 
	 * @param inXml
	 */
	private void moveDeliveryInstrToOrderLine(YFCDocument inXml) {

		for(YFCElement yfcEleOrderLine : inXml.getElementsByTagName(TelstraConstants.ORDER_LINE)){
			
			YFCElement yfcEleShipTo = yfcEleOrderLine.getChildElement(TelstraConstants.PERSON_INFO_SHIP_TO);
			if(!YFCCommon.isVoid(yfcEleShipTo)){
				YFCElement yfcEleShipToExtn = yfcEleShipTo.getChildElement(TelstraConstants.EXTN);
				if(!YFCCommon.isVoid(yfcEleShipToExtn)){
					String sDeliveryInstruction = yfcEleShipToExtn.getAttribute(TelstraConstants.DELIVERY_INSTRUCTION);
					
					YFCElement yfcEleOrderLineExtn = yfcEleOrderLine.getChildElement(TelstraConstants.EXTN);
					if(YFCCommon.isVoid(yfcEleOrderLineExtn)){
						yfcEleOrderLineExtn = yfcEleOrderLine.createChild(TelstraConstants.EXTN);
					}
					yfcEleOrderLineExtn.setAttribute(TelstraConstants.DELIVERY_INSTRUCTION, sDeliveryInstruction);
					yfcEleShipToExtn.removeAttribute(TelstraConstants.DELIVERY_INSTRUCTION);
				}
			}
		}		
	}


	//HUB-9409 - [START]

	/**
	 * This method will Replace Person Info Ship To and Item Tag from the existing the line.
	 * @param inXml
	 * @param docGetOrderListOutXml
	 */
	private void copyPersonInfoShipAndItemToLine(YFCDocument inXml,
			YFCDocument docGetOrderListOutXml) {

		for (YFCElement eleInputOrderLine : inXml.getElementsByTagName(TelstraConstants.ORDER_LINE)) {
			String sPrimeLineNo = eleInputOrderLine.getAttribute(TelstraConstants.PRIME_LINE_NO, "");
			YFCElement eleTempOrderLine = XPathUtil.getXPathElement(docGetOrderListOutXml, "//Order/OrderLines/OrderLine[@PrimeLineNo='" + sPrimeLineNo + "']");
			if(!YFCObject.isVoid(eleTempOrderLine)){
				YFCElement eleInputPersonInfoShipTo = eleInputOrderLine.getElementsByTagName(TelstraConstants.PERSON_INFO_SHIP_TO).item(0);
				YFCElement eleInputOrderLineItem = eleInputOrderLine.getElementsByTagName(TelstraConstants.ITEM).item(0);
				YFCElement eleTempPersonInfoShipTo = eleTempOrderLine.getElementsByTagName(TelstraConstants.PERSON_INFO_SHIP_TO).item(0);
				YFCElement eleTempOrderLineItem = eleTempOrderLine.getElementsByTagName(TelstraConstants.ITEM).item(0);

				if(!YFCCommon.isVoid(eleInputPersonInfoShipTo)&&!YFCCommon.isVoid(eleTempPersonInfoShipTo)){
					LoggerUtil.verboseLog("Removing PersonInfoShipTo element from the OrderLine in copyPersonInfoShipAndItemToLine : : ", logger,eleInputPersonInfoShipTo.toString());
					eleInputPersonInfoShipTo.getParentElement().removeChild(eleInputPersonInfoShipTo);
					eleInputOrderLine.importNode(eleTempPersonInfoShipTo);
				}
				
				if(!YFCCommon.isVoid(eleInputOrderLineItem)){
					LoggerUtil.verboseLog("Removing Item element from the OrderLine in copyPersonInfoShipAndItemToLine : : ", logger,eleInputOrderLineItem.toString());
					eleInputOrderLineItem.getParentElement().removeChild(eleInputOrderLineItem);
					eleInputOrderLine.importNode(eleTempOrderLineItem);
				}				
			}
		}
	}

	//HUB-9409 - [END]

	//HUB-9390 - [START]

	/**
	 * This method will update the existing order line with the requested Delivery date and Process the Delivery Qty for the Incoming Line
	 * @param inXml
	 * @param eleOrderOut
	 */
	private YFCDocument processUpdateForPMC(YFCDocument inXml, YFCDocument docGetOrderListOutXml, YFCElement eleOrderOut,String sEntryType) {

		YFCDocument docChangeOrderOutput = createchangeOrderOutputDoc(eleOrderOut);
		YFCElement eleOrder = docGetOrderListOutXml.getElementsByTagName(TelstraConstants.ORDER).item(0);
		String date = getDateStamp();

		YFCDocument inDoc = YFCDocument.getDocumentFor(TelstraConstants.ORDER_ORDER_HEADER_KEY
				+ eleOrder.getAttribute(TelstraConstants.ORDER_HEADER_KEY)
				+ "' Action='MODIFY' ModificationReasonText='Order Line Updated at " + date
				+ "'><OrderLines></OrderLines></Order>");

		YFCDocument tempDoc = YFCDocument.getDocumentFor("<Order DocumentType='' OrderHeaderKey='' SellerOrganizationCode='' BuyerOrganizationCode='' OrderType='' EnterpriseCode='' EntryType='' DepartmentCode='' OrderName='' OrderNo='' Status='' >"
				+ "<OrderLines><OrderLine OrderLineKey='' SubLineNo='' PrimeLineNo='' OrderHeaderKey='' Status='' StatusQuantity='' OrderedQty='' ShipNode=''>"
				+ "<Item ItemID='' UnitOfMeasure=''  /><OrderStatuses><OrderStatus TotalQuantity='' StatusQty='' Status='' OrderReleaseKey='' />"
				+ "</OrderStatuses></OrderLine></OrderLines></Order>");

		HashMap <String,YFCElement> mapChangeOrderPLTempLine = null;
		mapChangeOrderPLTempLine = new HashMap<>();
		
		/*Creating a map for the all the existing lines and updating the requested delivery date in changeOrderToUpdatePMCOrderLine and then calling change Order Api*/

		for (YFCElement eleInputOrderLine : inXml.getElementsByTagName(TelstraConstants.ORDER_LINE)) {
			String sPrimeLineNo = eleInputOrderLine.getAttribute(TelstraConstants.PRIME_LINE_NO, "");
			YFCElement eleTempOrderLine = XPathUtil.getXPathElement(docGetOrderListOutXml, "//Order/OrderLines/OrderLine[@PrimeLineNo='" + sPrimeLineNo + "']");
			if(!YFCObject.isVoid(eleTempOrderLine)){
			eleTempOrderLine = changeOrderToUpdatePMCOrderLine(eleTempOrderLine,eleInputOrderLine,eleOrder);
				inDoc.getElementsByTagName(TelstraConstants.ORDER_LINES).item(0).importNode(eleTempOrderLine.cloneNode(true));
				mapChangeOrderPLTempLine.put(sPrimeLineNo, eleTempOrderLine);
			}
		}
		/*
		 * Adding interface number as an identifier whether change order is being invoke from the source or Sterling order
		 */

		if(mapChangeOrderPLTempLine != null){

			inDoc.getDocumentElement().setAttribute(TelstraConstants.INTERFACE_NO, sInterfaceNo);

			LoggerUtil.verboseLog("Input XML to changeOrder Api in processUpdateForPMC Method : : ", logger, inDoc.toString());
			YFCDocument outDoc = invokeYantraApi("changeOrder", inDoc, tempDoc);
			LoggerUtil.verboseLog("Output XML from changeOrder Api in processUpdateForPMC Method : : ", logger, outDoc.toString());

			/*Calling the processOrder for every order line to deliver the quantities if change status 'C' or DeliverQty is passed*/
			for (YFCElement eleInputOrderLine : inXml.getElementsByTagName(TelstraConstants.ORDER_LINE)) {
				String sPrimeLineNo = eleInputOrderLine.getAttribute(TelstraConstants.PRIME_LINE_NO, "");
				YFCElement eleMatchingOrderLine = mapChangeOrderPLTempLine.get(sPrimeLineNo);
				if(!YFCObject.isVoid(eleMatchingOrderLine)){
					String sOrderLineStatus = eleInputOrderLine.getAttribute(TelstraConstants.CHANGE_STATUS, "");
					if(YFCObject.equals(sOrderLineStatus,"C") || eleInputOrderLine.hasAttribute(TelstraConstants.DELIVERED_QTY)){
						processOrderLine(eleMatchingOrderLine, docGetOrderListOutXml, sOrderLineStatus, eleInputOrderLine,
								sInterfaceNo, sEntryType);
					}
				}
			}
		}
		return docChangeOrderOutput;
	}

	/**
	 * This method will set DeliveryDate , Ordered Qty and remove the ship to Key from th Line
	 * @param eleInputOrderLine
	 * @param eleTempOrderLine
	 * @param docGetOrderListOutXml
	 * @return
	 */
	private YFCElement changeOrderToUpdatePMCOrderLine(YFCElement eleTempOrderLine,YFCElement eleInputOrderLine, YFCElement eleOrder) {

		YFCElement eleMatchingOrderLine =null;
		String sDocumentType = eleOrder.getAttribute(TelstraConstants.DOCUMENT_TYPE, "");

		if (YFCObject.equals(sDocumentType, TelstraConstants.DOCUMENT_TYPE_0005)) {
			for (YFCElement eleOrderStatus : eleTempOrderLine.getElementsByTagName("OrderStatus")) {
				double dOrderStatus = eleOrderStatus.getDoubleAttribute("Status");
				if (dOrderStatus == 9000.5000) {
					return eleMatchingOrderLine;
				}
			}
		}
		
		eleTempOrderLine.setAttribute("Action", "MODIFY");
		eleTempOrderLine.setAttribute(TelstraConstants.REQ_DELIVERY_DATE, eleInputOrderLine.getAttribute(TelstraConstants.REQUIRED_DELIVERY_DATE,""));
		/*OrderQty update should happen from INT_ODR_3, allowing it in both the interface may cause discrepancy. Therefore only Delivery date and Qty is used from 
		 * the INT_ODR_4 interface.*/
		
		//eleTempOrderLine.setAttribute(TelstraConstants.ORDERED_QTY, eleInputOrderLine.getAttribute(TelstraConstants.ORDERED_QTY,""));
		
		eleTempOrderLine.removeAttribute(TelstraConstants.SHIP_TO_KEY);
		eleMatchingOrderLine = eleTempOrderLine;
		
		return eleMatchingOrderLine;

	}

	// HUB-9390 - [END]

	/**
	 * 
	 * @param inXml
	 * @param docGetOrderListOutXml
	 */
	private void removePersonInfoShipToForExistingLine(YFCDocument inXml, YFCDocument docGetOrderListOutXml) {

		for(YFCElement yfcEleOrderLineIp : inXml.getElementsByTagName(TelstraConstants.ORDER_LINE)){

			String sShipToId = yfcEleOrderLineIp.getAttribute(TelstraConstants.SHIP_TO_ID);
			String sShipToKey = yfcEleOrderLineIp.getAttribute(TelstraConstants.SHIP_TO_KEY);

			if(!YFCCommon.isStringVoid(sShipToId) || !YFCCommon.isStringVoid(sShipToKey)){

				String sPrimeLineNo = yfcEleOrderLineIp.getAttribute(TelstraConstants.PRIME_LINE_NO);
				if(!YFCCommon.isStringVoid(sPrimeLineNo)){

					YFCElement yfcEleOrderLineInOrder = XPathUtil.getXPathElement(docGetOrderListOutXml, "//OrderLine[@PrimeLineNo='"+sPrimeLineNo+"']");
					if(!YFCCommon.isVoid(yfcEleOrderLineInOrder)){
						YFCElement yfcElePersonInfoShipTo = yfcEleOrderLineIp.getChildElement(TelstraConstants.PERSON_INFO_SHIP_TO);
						if(!YFCCommon.isVoid(yfcElePersonInfoShipTo)){
							yfcElePersonInfoShipTo.getParentElement().removeChild(yfcElePersonInfoShipTo);
						}
					}
				}
			}
		}
	}

	/**
	 * 
	 * @param inXml
	 * @param docGetOrderListOutXml
	 */
	private void processSterlingOrderUpdate(YFCDocument inXml, YFCDocument docGetOrderListOutXml) {

		YFCDocument yfcDocSterlingOrderUpdate = YFCDocument.createDocument();
		YFCElement yfcEleSterlingOrderUpdateRoot = yfcDocSterlingOrderUpdate.importNode(inXml.getDocumentElement(), true);
		yfcDocSterlingOrderUpdate.appendChild(yfcEleSterlingOrderUpdateRoot);

		YFCElement yfcEleOrderLines = yfcDocSterlingOrderUpdate.getElementsByTagName(TelstraConstants.ORDER_LINES).item(0);
		yfcDocSterlingOrderUpdate.getDocumentElement().removeChild(yfcEleOrderLines);

		yfcEleSterlingOrderUpdateRoot.createChild(TelstraConstants.ORDER_LINES);
		yfcEleOrderLines = yfcEleSterlingOrderUpdateRoot.getElementsByTagName(TelstraConstants.ORDER_LINES).item(0);

		YFCNodeList<YFCElement> yfcNlOrderLine = inXml.getElementsByTagName(TelstraConstants.ORDER_LINE);
		List<String> lPrimeLineNoToAddinNewDoc = new ArrayList<>();

		for(YFCElement yfcEleOrderLine: yfcNlOrderLine){

			String sPrimeLineNo = yfcEleOrderLine.getAttribute(TelstraConstants.PRIME_LINE_NO);
			YFCElement yfcEleOrderLineInOrderList = XPathUtil.getXPathElement(docGetOrderListOutXml, "//OrderLine[@PrimeLineNo='"+sPrimeLineNo+"']");
			/*
			 * If existing prime line number exist in the incoming document, it means line is for the order update. Add the line number in the
			 * list and later remove the line from the incoming doc and copy that to the new doc by looping into the list
			 */
			if(!YFCCommon.isVoid(yfcEleOrderLineInOrderList)){
				lPrimeLineNoToAddinNewDoc.add(sPrimeLineNo);
			}
		}

		for(String sPrimeLineNo : lPrimeLineNoToAddinNewDoc){
			/*
			 * Add the line number in the new document for processing Sterling order update separately
			 */
			YFCElement yfcEleOrderLineOrderList = XPathUtil.getXPathElement(inXml, "//OrderLine[@PrimeLineNo='"+sPrimeLineNo+"']");
			YFCElement yfcEleSterlingOrderUpdateOrderLine = yfcDocSterlingOrderUpdate.importNode(yfcEleOrderLineOrderList, true);
			yfcEleOrderLines.appendChild(yfcEleSterlingOrderUpdateOrderLine);
			/*
			 * Remove the line from inDoc as it would be processed separately in the GpsSterlingOrderUpdate code
			 */
			yfcEleOrderLineOrderList.getParentNode().removeChild(yfcEleOrderLineOrderList);
		}
		/*
		 * process Sterling order update 
		 */
		YFCNodeList<YFCElement> yfcNlOrderLineChangeOrderIp = yfcDocSterlingOrderUpdate.getElementsByTagName(TelstraConstants.ORDER_LINE);
		/*
		 * If there is no order line present in change order Ip, then return back
		 */
		if(yfcNlOrderLineChangeOrderIp.getLength() != 0){
			GpsSterlingOrderUpdate obj = new GpsSterlingOrderUpdate();
			obj.updateSterlingOrder(yfcDocSterlingOrderUpdate, docGetOrderListOutXml, getServiceInvoker());			
		}
		else{
			LoggerUtil.verboseLog("Returning back from GpsSterlingOrderUpdate as there is no order line present to change", logger, yfcDocSterlingOrderUpdate);
		}		
	}

	/**
	 * When OrderName is null get the OrderNo sequence and stamp the OrderNo and
	 * OrderName attribute as the next sequence.
	 * 
	 * @param inXml
	 */
	private void stampOrderNoAndName(YFCDocument inXml) {

		long lNextOrderNo = getServiceInvoker().getNextSequenceNumber("SEQ_YFS_ORDER_NO");
		sOrderName = String.valueOf(lNextOrderNo);
		YFCElement eleInputOrder = inXml.getElementsByTagName(TelstraConstants.ORDER).item(0);

		// Stamping the next sequence as the Order No and Order Name
		eleInputOrder.setAttribute("OrderName", sOrderName);
		eleInputOrder.setAttribute("OrderNo", sOrderName);

	}

	/**
	 * This method is invoke for processing order update
	 * 
	 * @param inXml
	 * @param eleOrderOut
	 * @param docGetOrderListOutXml
	 * @param sInterfaceNo
	 * @param sEntryType
	 * @return
	 */
	private YFCDocument processChangeOrder(YFCDocument inXml, YFCElement eleOrderOut, YFCDocument docGetOrderListOutXml,
			String sInterfaceNo, String sEntryType) {

		// HUB-8346 -[START]

		YFCDocument docChangeOrderOutput = createchangeOrderOutputDoc(eleOrderOut);

		for (YFCElement eleInputOrderLine : inXml.getElementsByTagName(TelstraConstants.ORDER_LINE)) {
			String sPrimeLineNo = eleInputOrderLine.getAttribute(TelstraConstants.PRIME_LINE_NO, "");
			YFCElement eleTempOrderLine = null;
			for (YFCElement eleOrderListLine : eleOrderOut.getElementsByTagName(TelstraConstants.ORDER_LINE)) {
				String sOrderListPrimeLineNo = eleOrderListLine.getAttribute(TelstraConstants.PRIME_LINE_NO, "");
				if (sOrderListPrimeLineNo.equals(sPrimeLineNo)) {
					eleTempOrderLine = eleOrderListLine;
					break;
				}
			}
			if (YFCObject.isNull(eleTempOrderLine)) {
				/*
				 * change order with orderline action=CREATE
				 */
				
				//HUB-9372[START]
				/*The below changes are done to return the changeOrder output line details required for delivered qty computation*/

				eleTempOrderLine = changeOrderToCreateNewLine(eleInputOrderLine, docGetOrderListOutXml);

				//HUB-9372[END]

				String sOrderLineStatus = eleInputOrderLine.getAttribute(TelstraConstants.CHANGE_STATUS, "");
				if("B".equalsIgnoreCase(sOrderLineStatus)){//ChangeStatus="B" for backordered
					/*
					 * Moving backordered qty in the input to the backordered status
					 */
					double dQtyToBackorder = eleInputOrderLine.getDoubleAttribute(TelstraConstants.BACKORDERED_QTY, 0.00);
					YFCElement yfcEleOrderFromOrderList = docGetOrderListOutXml.getElementsByTagName(TelstraConstants.ORDER).item(0);
					String sOrderHeaderKey = yfcEleOrderFromOrderList.getAttribute(TelstraConstants.ORDER_HEADER_KEY);
					String sDocumentType = yfcEleOrderFromOrderList.getAttribute(TelstraConstants.DOCUMENT_TYPE);
					String sTransactionId = "SCHEDULE."+sDocumentType;
					String sModificationReasonText = "Order Line# "	+ eleInputOrderLine.getAttribute("PrimeLineNo", "") + " Backordered at " + getDateStamp();
					YFCDocument docchangeOrderStatusinXml = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderHeaderKey+"' TransactionId='"+sTransactionId+"' ModificationReasonText='"+sModificationReasonText+"'><OrderLines><OrderLine BaseDropStatus='1300' Quantity='" + dQtyToBackorder + "' PrimeLineNo='"+sPrimeLineNo+"' SubLineNo='1'/></OrderLines></Order>");							
					invokeYantraApi("changeOrderStatus", docchangeOrderStatusinXml);

					//HUB-9372[START]

					/*The below logic is invoked to Deliver the newly created lines if the changeStatus or DeliveredQty is passed for a PO*/

				}else if("INT_ODR_3".equalsIgnoreCase(sInterfaceNo) || "INT_ODR_4".equalsIgnoreCase(sInterfaceNo)){
					processOrderLine(eleTempOrderLine, docGetOrderListOutXml, sOrderLineStatus, eleInputOrderLine,
							sInterfaceNo, sEntryType);
				}

				//HUB-9372[END]

			} else {
				String sOrderLineStatus = eleInputOrderLine.getAttribute(TelstraConstants.CHANGE_STATUS, "");
				
				//HUB-9372[START]

				if (YFCObject.isNull(sOrderLineStatus) || (YFCObject.equals(sOrderLineStatus,"C"))) {
					/*
					 * call change order with order line action modify and pass
					 * inXml order line element and add OrderLineKey 
					 * The below logic takes care of adding the line with new qty 
					 */
					YFCElement eleMatchingOrderLine  = changeOrderToUpdateOrderLine(eleInputOrderLine, eleTempOrderLine, docGetOrderListOutXml,
							sInterfaceNo, sEntryType, 0.00);
					
			
					/* WhenEver change status C is passed or Delivered Qty is passed in the Order Line invoke processOrderLine Method*/
					if(!YFCObject.isVoid(eleMatchingOrderLine)){
						if( (YFCObject.equals(sOrderLineStatus,"C") || eleInputOrderLine.hasAttribute(TelstraConstants.DELIVERED_QTY))){
								processOrderLine(eleMatchingOrderLine, docGetOrderListOutXml, sOrderLineStatus, eleInputOrderLine,
										sInterfaceNo, sEntryType);
							}
						}
					//HUB-9372[END]

				} else {
					processOrderLine(eleTempOrderLine, docGetOrderListOutXml, sOrderLineStatus, eleInputOrderLine,
							sInterfaceNo, sEntryType);
				}
			}
		}
		return docChangeOrderOutput;
	}

	// HUB-8346 -[START]

	/**
	 * This method returns a document for changeOrder
	 * 
	 * @param eleOrderOut
	 * @return outXml
	 */
	private YFCDocument createchangeOrderOutputDoc(YFCElement eleOrderOut) {
		YFCDocument docChangeOrderOutput = YFCDocument
				.getDocumentFor("<Order OrderHeaderKey='' OrderName='' OrderNo='' EnterpriseCode='' DocumentType=''  "
						+ "OrderType='' EntryType='' DepartmentCode=''/>");
		YFCElement eleChangeOrderOutput = docChangeOrderOutput.getDocumentElement();
		eleChangeOrderOutput.setAttribute("OrderHeaderKey", eleOrderOut.getAttribute("OrderHeaderKey", ""));
		eleChangeOrderOutput.setAttribute("OrderName", eleOrderOut.getAttribute("OrderName", ""));
		eleChangeOrderOutput.setAttribute("OrderNo", eleOrderOut.getAttribute("OrderNo", ""));
		eleChangeOrderOutput.setAttribute("EnterpriseCode", eleOrderOut.getAttribute("EnterpriseCode", ""));
		eleChangeOrderOutput.setAttribute("DocumentType", eleOrderOut.getAttribute("DocumentType", ""));
		eleChangeOrderOutput.setAttribute("OrderType", eleOrderOut.getAttribute("OrderType", ""));
		eleChangeOrderOutput.setAttribute("EntryType", eleOrderOut.getAttribute("EntryType", ""));
		eleChangeOrderOutput.setAttribute("DepartmentCode", eleOrderOut.getAttribute("DepartmentCode", ""));

		return docChangeOrderOutput;
	}

	// HUB-8346 -[END]

	/**
	 * 
	 * @param eleTempOrderLine
	 * @param docGetOrderListOutXml
	 * @param sOrderLineStatus
	 * @param sEntryType
	 */
	private void processOrderLine(YFCElement eleTempOrderLine, YFCDocument docGetOrderListOutXml,
			String sOrderLineStatus, YFCElement eleInputOrderLine, String sInterfaceNo, String sEntryType) {
		// Configure in service arguments
		String sDeliverstatus = getProperty("ChangeStatusDeliver", true);// "C"
		String sBackorderstatus = getProperty("ChangeStatusBackorder", true); // "B"
		String sCancelstatus = getProperty("ChangeStatusCancel", true); // "D"
		String sPartialComplete = getProperty("ChangeStatusPartialComplete", true); // "PC"

		//HUB-9372[START]
		/*Setting changeStatus="PC", if DeliveredQty is passed*/

		/*Skipping this validation if ChangeStatus is already PC, this can happen while adding or updating a line */
		double dInputDeliveredQty = 0.0;
			if(YFCObject.equals(sInterfaceNo, "INT_ODR_4") && eleInputOrderLine.hasAttribute(TelstraConstants.DELIVERED_QTY)){
				dInputDeliveredQty = eleInputOrderLine.getDoubleAttribute(TelstraConstants.DELIVERED_QTY,0.0);
				if(dInputDeliveredQty>=0.0){
					sOrderLineStatus = "PC";
				}
		}else{
			dInputDeliveredQty = eleInputOrderLine.getDoubleAttribute(TelstraConstants.DELIVERED_QTY);
		}

		/*Since invoking this method for newly created lines this check will return out if 
		 * DeliveredQty or ChangeStatus is not passed for that orderLine*/
		if(YFCObject.isVoid(sOrderLineStatus)){
			return;
		}

		//HUB-9372[END]


		if (sOrderLineStatus.equalsIgnoreCase(sDeliverstatus)) {
			deliverOrderLine(eleTempOrderLine);
		} else if (sOrderLineStatus.equalsIgnoreCase(sCancelstatus)) {
			cancelOrderLine(eleTempOrderLine, docGetOrderListOutXml);
		} else if (sOrderLineStatus.equalsIgnoreCase(sBackorderstatus)) {
			double  dBackorderedQty= eleInputOrderLine.getDoubleAttribute(TelstraConstants.BACKORDERED_QTY,0.00);
			double  dOrderedQty= eleInputOrderLine.getDoubleAttribute(TelstraConstants.ORDERED_QTY,0.00);

			if (dBackorderedQty > dOrderedQty) {
				throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.ORDER_INVALID_BACKORDERED_QTY_ERROR_CODE,
						new YFSException());
			}
			/*
			 * This method is invoked for change order input coming to backorder quantities in the exisiting line
			 */
			backOrderLine(eleTempOrderLine, docGetOrderListOutXml, eleInputOrderLine, sInterfaceNo, sEntryType);
		}
		// HUB-8072 [START]
		else if (sOrderLineStatus.equalsIgnoreCase(sPartialComplete)) {

			//HUB-9372[START]
			/*This qty that is already delivered is computed below*/
			double dDeliveredQty = getDeliveredQtyforLine(eleTempOrderLine);
			
			/*This flag returns true if the line contains shipments below delivered status*/

			boolean bLineHasShipment = IsLineContainingShipment(eleTempOrderLine);

			if (bLineHasShipment) {

				eleTempOrderLine.setAttribute(TelstraConstants.IS_SHIPPED, "Y");
				LoggerUtil.verboseLog("The Order Line contains Shipment : : ", logger, eleTempOrderLine.toString());

			}

			double dNewDeliveredQty = dInputDeliveredQty - dDeliveredQty;
			if(dNewDeliveredQty > 0.0 ){
				eleTempOrderLine.setAttribute(TelstraConstants.STATUS_QUANTITY,dNewDeliveredQty);

				//HUB-9372[END]
				LoggerUtil.verboseLog("Invoking partialDeliverOrderLine from  ChangeStatus PC : : ", logger, eleTempOrderLine.toString());
				partialDeliverOrderLine(eleTempOrderLine);
			}

			// HUB-8072 [END]
		}
	}

	//HUB-9372[START]

	/**
	 * This method will return the Qty which is already delivered
	 * @param eleTempOrderLine
	 * @return dDeliveredQty
	 */
	private boolean IsLineContainingShipment(YFCElement eleTempOrderLine) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "IsLineHasShipment", "");
		boolean bLineHasShipment = false;
		for (YFCElement eleOrderStatus : eleTempOrderLine.getElementsByTagName("OrderStatus")) {
			double dOrderStatus = eleOrderStatus.getDoubleAttribute(TelstraConstants.STATUS);
			if (dOrderStatus < 3700.7776 && dOrderStatus > 3349) { 
				bLineHasShipment = true;
				break;	
			}
		}
		return bLineHasShipment;
	}

	/**
	 * This method will return the Qty which is already delivered
	 * @param eleTempOrderLine
	 * @return dDeliveredQty
	 */
	private double getDeliveredQtyforLine(YFCElement eleTempOrderLine) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "deliverOrderLine", "");
		double dDeliveredQty = 0.0;
		for (YFCElement eleOrderStatus : eleTempOrderLine.getElementsByTagName("OrderStatus")) {
			double dOrderStatus = eleOrderStatus.getDoubleAttribute(TelstraConstants.STATUS);
			if (dOrderStatus >= 3700.7777 && dOrderStatus!=9000) {
				double dLineStatusDeliveredQty = eleOrderStatus.getDoubleAttribute("StatusQty", 0.0);
				dDeliveredQty = dDeliveredQty + dLineStatusDeliveredQty;
			}
		}
		return dDeliveredQty;
	}

	//HUB-9372[END]

	/**
	 * If new ordered qty is more than the new back ordered qty, then delta qty needs to be added in created/released status
	 * based on the line status
	 * @param eleInputOrderLine
	 * @param eleTempOrderLine
	 * @param docGetOrderListOut
	 * @param sEntryType
	 * @param dNewQtyToBackorder
	 * @return 
	 */
	private YFCElement changeOrderToUpdateOrderLine(YFCElement eleInputOrderLine, YFCElement eleTempOrderLine,
			YFCDocument docGetOrderListOut, String sInterfaceNo, String sEntryType, double dNewQyToBackorder) {
		/*
		 * call changeOrder with input xml = <Order OrderHeaderKey=""
		 * Action="MODIFY"><OrderLines><OrderLine
		 * Action="MODIFY"></OrderLines></Order> change the outstanding Qty to
		 * Ordered Qty for INT_ODR_1/2 Do not call change Order if the Line is a
		 * Freight Charged Line(9000.5000) for PO and return out of this method
		 */

		// HUB-8358-[START]
		/*
		 * If the line status is Freight Line Closed (9000.5000), then returning from the method
		 * without doing anything
		 */
		YFCElement eleMatchingOrderLine = null;

		YFCElement eleOrder = docGetOrderListOut.getElementsByTagName(TelstraConstants.ORDER).item(0);
		String sDocumentType = eleOrder.getAttribute(TelstraConstants.DOCUMENT_TYPE, "");

		if (YFCObject.equals(sDocumentType, TelstraConstants.DOCUMENT_TYPE_0005)) {
			for (YFCElement eleOrderStatus : eleTempOrderLine.getElementsByTagName("OrderStatus")) {
				double dOrderStatus = eleOrderStatus.getDoubleAttribute("Status");
				if (dOrderStatus == 9000.5000) {
					return eleMatchingOrderLine;
				}
			}
		}

		// HUB-8358-[END]

		// HUB-8462 -[START]
		if (YFCObject.equals(sInterfaceNo, "INT_ODR_1") || YFCObject.equals(sInterfaceNo, "INT_ODR_2")
				|| (YFCObject.equals(sEntryType, "STERLING") && (YFCObject.equals(sOrderType, "MATERIAL_RESERVATION")
						|| YFCObject.equals(sOrderType, "TRANSPORT_ORDER")))) {
			itemRound(eleInputOrderLine, docGetOrderListOut);
			double dOutstandingQty = eleInputOrderLine.getDoubleAttribute("OrderedQty");
			// HUB-8457 [Begin]
			boolean bOrderLineInReleasedStatus = false;
			String sOrderReleaseKey = "";
			double dReleasedQty = 0.00;
			for (YFCElement eleOrderStatus : eleTempOrderLine.getElementsByTagName("OrderStatus")) {

				double dOrderStatus = eleOrderStatus.getDoubleAttribute("Status");
				if(dOrderStatus >= 3200 && dOrderStatus<9000){
					dReleasedQty = dReleasedQty+eleOrderStatus.getDoubleAttribute("StatusQty");	
				}
				if (!bOrderLineInReleasedStatus && dOrderStatus == 3200 && dReleasedQty != 0) {
					bOrderLineInReleasedStatus = true;
					sOrderReleaseKey = eleOrderStatus.getAttribute("OrderReleaseKey");					
				}
			}
			//			double dExistingOrderedQty = Double.valueOf(eleTempOrderLine.getAttribute(TelstraConstants.ORDERED_QTY));

			double dNewOrderedQty = dOutstandingQty;
			double dNonBOQty = 0.00;
			for (YFCElement eleOrderStatus : eleTempOrderLine.getElementsByTagName("OrderStatus")) {
				double dOrderStatus = eleOrderStatus.getDoubleAttribute(TelstraConstants.STATUS);
				/* 
				 * New Ordered qty is outstanding quantity plus non backordered qty
				 */
				if ((dOrderStatus != 1300) && dOrderStatus != 9000 && dOrderStatus != 1400) {
					dNonBOQty = eleOrderStatus.getDoubleAttribute("StatusQty");
					dNewOrderedQty = dNewOrderedQty + dNonBOQty;
				}
			}
			if (!YFCObject.isNull(dNewOrderedQty)) {
				/*
				 * Setting newOrderQty to OrderedQty for the line
				 */
				eleInputOrderLine.setAttribute("OrderedQty", dNewOrderedQty);
			}

			boolean bChangeReleasedInvoked = false;
			double dNewBackorderedQty = eleInputOrderLine.getDoubleAttribute("BackorderedQty");
			if (bOrderLineInReleasedStatus) {
				/*
				 * Adding the delta quantity into the same release through change release api
				 */
				double dQtyToRelease = dOutstandingQty - dNewBackorderedQty;
				if(dQtyToRelease>0){
					YFCDocument docOrderReleaseinXml = YFCDocument
							.getDocumentFor("<OrderRelease Action='MODIFY' ModificationReasonText='Added to a release at "
									+ getDateStamp() + "' OrderReleaseKey='" + sOrderReleaseKey + "'>"
									+ "<OrderLines><OrderLine Action='' OrderLineKey='"
									+ eleTempOrderLine.getAttribute(TelstraConstants.ORDER_LINE_KEY)
									+ "' ChangeInQuantity='" + dQtyToRelease + "'/></OrderLines></OrderRelease>");
					invokeYantraApi("changeRelease", docOrderReleaseinXml);
					bChangeReleasedInvoked = true;
				}
				/*
				 * if dExistingOrderedQty == dNewOrderedQty, total orderline quantity is same, no need to call change order
				 */
				/*if (dExistingOrderedQty == dNewOrderedQty) {
					bChangeReleasedInvoked = true;
				}*/
				/*
				 * Overriding the ordered qty value with the new ordered qty 
				 */
				eleInputOrderLine.setDoubleAttribute("OrderedQty", dOutstandingQty + dReleasedQty);
			}
			// HUB-8457 [End]
			if (!bChangeReleasedInvoked) {
				/*
				 * The scenario where new line is added with 0 backordered qty
				 */
				if (dNewBackorderedQty == 0.00) {
					double dBOQty = 0.00;
					for (YFCElement eleOrderStatus : eleTempOrderLine.getElementsByTagName("OrderStatus")) {
						double dOrderStatus = eleOrderStatus.getDoubleAttribute(TelstraConstants.STATUS);
						/* Adding Qty to the OrderLine with Qty that are already
						 * with Shipment to the OutstandingQty, Status 3350 is
						 * Included in Shipment.
						 */
						if (dOrderStatus == 1300) {
							dBOQty = eleOrderStatus.getDoubleAttribute("StatusQty");
						}
					}
					if (dBOQty != 0.00) {
						/*
						 * Moving the BO quantity which is no longer in BO to created status
						 */
						moveBOQuantityToCreatedStatus(docGetOrderListOut, eleTempOrderLine, getDateStamp(), dBOQty);
					}
				}
			}
		}
		/*
		 * Calling change order order to update the ordered qty
		 */
		//HUB-9372[START]

		YFCDocument tempDoc = YFCDocument.getDocumentFor("<Order DocumentType='' OrderHeaderKey='' SellerOrganizationCode='' BuyerOrganizationCode='' OrderType='' EnterpriseCode='' EntryType='' DepartmentCode='' OrderName='' OrderNo='' Status='' >"
				+ "<OrderLines><OrderLine OrderLineKey='' SubLineNo='' PrimeLineNo='' OrderHeaderKey='' Status='' StatusQuantity='' OrderedQty='' ShipNode=''>"
				+ "<Item ItemID='' UnitOfMeasure=''  /><OrderStatuses><OrderStatus TotalQuantity='' StatusQty='' Status='' OrderReleaseKey='' />"
				+ "</OrderStatuses></OrderLine></OrderLines></Order>");


		if("INT_ODR_3".equalsIgnoreCase(sInterfaceNo) || "INT_ODR_4".equalsIgnoreCase(sInterfaceNo)) {
			eleInputOrderLine.removeAttribute(TelstraConstants.SHIP_TO_KEY);
		}
		
		//HUB-9372[END]

		String date = getDateStamp();
		YFCDocument inDoc = YFCDocument.getDocumentFor(TelstraConstants.ORDER_ORDER_HEADER_KEY
				+ eleOrder.getAttribute(TelstraConstants.ORDER_HEADER_KEY)
				+ "' Action='MODIFY' ModificationReasonText='Order Line Updated at " + date
				+ "'><OrderLines></OrderLines></Order>");
		eleInputOrderLine.setAttribute("Action", "MODIFY");
		eleInputOrderLine.setAttribute(TelstraConstants.ORDER_LINE_KEY,
				eleTempOrderLine.getAttribute(TelstraConstants.ORDER_LINE_KEY));
		inDoc.getElementsByTagName(TelstraConstants.ORDER_LINES).item(0)
		.importNode(eleInputOrderLine.cloneNode(true));
		/*
		 * Adding interface number as an identifier whether change order is being invoke from the source or Sterling order
		 */
		inDoc.getDocumentElement().setAttribute(TelstraConstants.INTERFACE_NO, sInterfaceNo);
		
		LoggerUtil.verboseLog("Input XML to changeOrder Api in changeOrderToUpdateOrderLine Method : : ", logger, inDoc.toString());

		YFCDocument outDoc = invokeYantraApi("changeOrder", inDoc, tempDoc);

		LoggerUtil.verboseLog("Output XML from changeOrder Api in changeOrderToUpdateOrderLine Method : : ", logger, outDoc.toString());

		//HUB-9372[START]

		YFCElement eleOrderOut = outDoc.getDocumentElement();
		String sPrimeLineNo = eleInputOrderLine.getAttribute(TelstraConstants.PRIME_LINE_NO, "");
		for (YFCElement eleOrderListLine : eleOrderOut.getElementsByTagName(TelstraConstants.ORDER_LINE)) {
			String sOrderListPrimeLineNo = eleOrderListLine.getAttribute(TelstraConstants.PRIME_LINE_NO, "");
			if (sOrderListPrimeLineNo.equals(sPrimeLineNo)) {
				eleMatchingOrderLine = eleOrderListLine;
				break;
			}
		}

		return eleMatchingOrderLine;
		//HUB-9372[END]
	}

	/**
	 * itemRounding for INT_ODR_1, INT_ODR_2
	 * 
	 * @param eleInputOrderLine
	 * @param docGetOrderListOut
	 */
	private void itemRound(YFCElement eleInputOrderLine, YFCDocument docGetOrderListOut) {

		double dOrderLineQty = eleInputOrderLine.getDoubleAttribute(TelstraConstants.ORDERED_QTY, 0.0);
		double dNewOrderLineQty = dOrderLineQty;

		YFCDocument docGetCommonCodeListOutput = invokeYantraApi(TelstraConstants.API_GET_COMMON_CODE_LIST,
				YFCDocument.getDocumentFor("<CommonCode CodeType=\"ORDER_VALIDATION\"/>"));
		if (isValidationOn(docGetCommonCodeListOutput, "ITEM_ROUND")) {
			String sOrganizationCode = docGetOrderListOut.getDocumentElement()
					.getAttribute(TelstraConstants.ENTERPRISE_CODE);
			String sShipNode = eleInputOrderLine.getAttribute(TelstraConstants.SHIP_NODE, "");

			YFCElement eleItem = eleInputOrderLine.getChildElement(TelstraConstants.ITEM);
			String sItemID = eleItem.getAttribute(TelstraConstants.ITEM_ID);
			String sUnitOfMeasure = eleItem.getAttribute(TelstraConstants.UNIT_OF_MEASURE, "");

			/*
			 * call getItemNodeDefnList with input <ItemNodeDefn
			 * UnitOfMeasure="" OrganizationCode="" Node="" ItemID=""/>
			 */
			YFCDocument docInput = YFCDocument
					.getDocumentFor("<ItemNodeDefn UnitOfMeasure='' OrganizationCode='' Node='' ItemID=''/>");
			YFCElement eleInputRoot = docInput.getDocumentElement();
			eleInputRoot.setAttribute("UnitOfMeasure", sUnitOfMeasure);
			eleInputRoot.setAttribute("OrganizationCode", sOrganizationCode);
			eleInputRoot.setAttribute("Node", sShipNode);
			eleInputRoot.setAttribute("ItemID", sItemID);
			YFCDocument docTemplate = YFCDocument
					.getDocumentFor("<ItemNodeDefnList><ItemNodeDefn><Extn/></ItemNodeDefn></ItemNodeDefnList>");
			YFCDocument docOutput = invokeYantraApi(TelstraConstants.API_GET_ITEM_NODE_DEFN_LIST, docInput,
					docTemplate);

			YFCElement eleItemNodeDefn = docOutput.getElementsByTagName(TelstraConstants.ITEM_NODE_DEFN).item(0);
			if (!YFCObject.isVoid(eleItemNodeDefn)) {
				YFCElement eleExtn = eleItemNodeDefn.getChildElement(TelstraConstants.EXTN);
				if (!YFCObject.isVoid(eleExtn)) {
					double dRunQuantity = eleExtn.getDoubleAttribute(TelstraConstants.RUN_QUANTITY, 0.0);
					if (dRunQuantity > 0) {
						if (dOrderLineQty <= dRunQuantity) {
							dNewOrderLineQty = dRunQuantity;
						} else {
							dNewOrderLineQty = (Math.ceil(dOrderLineQty / dRunQuantity)) * dRunQuantity;
						}
					}
				}
			}
		}
		eleInputOrderLine.setDoubleAttribute(TelstraConstants.ORDERED_QTY, dNewOrderLineQty);
	}

	/**
	 * 
	 * @param docGetCommonCodeListOutput
	 * @param sValidationType
	 * @return
	 */
	private boolean isValidationOn(YFCDocument docGetCommonCodeListOutput, String sValidationType) {
		boolean bValidation = false;
		for (YFCElement eleCommonCode : docGetCommonCodeListOutput.getElementsByTagName(TelstraConstants.COMMON_CODE)) {
			if (eleCommonCode.getAttribute(TelstraConstants.CODE_VALUE).equals(sValidationType) && eleCommonCode
					.getAttribute(TelstraConstants.CODE_SHORT_DESCRIPTION).equals(TelstraConstants.YES)) {
				bValidation = true;
				break;
			}
		}
		return bValidation;
	}

	// HUB-7758 - [END]

	/**
	 * 
	 * @param eleOrderLine
	 * @param docGetOrderListOut
	 * @return 
	 */
	private YFCElement changeOrderToCreateNewLine(YFCElement eleOrderLine, YFCDocument docGetOrderListOut) {
		/*
		 * call changeOrder with input xml = <Order OrderHeaderKey=""
		 * Action="MODIFY"><OrderLines><OrderLine
		 * Action="CREATE"></OrderLines></Order>
		 */

		// HUB-8358-[START]

		YFCDocument tempDoc = YFCDocument.getDocumentFor("<Order DocumentType='' OrderHeaderKey=''>"
				+ "<OrderLines><OrderLine OrderLineKey='' SubLineNo='' PrimeLineNo='' OrderHeaderKey='' Status='' StatusQuantity='' OrderedQty=''>"
				+ "<Item ItemID='' UnitOfMeasure=''  /><OrderStatuses><OrderStatus TotalQuantity='' StatusQty='' Status='' OrderReleaseKey='' />"
				+ "</OrderStatuses></OrderLine></OrderLines></Order>");
		YFCElement eleOrder = docGetOrderListOut.getElementsByTagName(TelstraConstants.ORDER).item(0);

		// HUB - HUB-8058 Manage Order incorrectly stamps the ship node [START]

		String sShipNode = eleOrderLine.getAttribute(TelstraConstants.SHIP_NODE, "");

		// HUB - HUB-8058 Manage Order incorrectly stamps the ship node [END]
		// HUB - 8350 [Begin] - Defaulting ship node for new PO line
		String sDocumentType = XPathUtil.getXpathAttribute(docGetOrderListOut, "//Order/@DocumentType");
		String sSellerOrganizationCode = XPathUtil.getXpathAttribute(docGetOrderListOut,
				"//Order/@SellerOrganizationCode");
		if (YFCObject.equals(sDocumentType, "0005")) {
			if (!YFCObject.equals(sSellerOrganizationCode, "TELSTRA")
					&& !YFCObject.equals(sSellerOrganizationCode, "NBN")) {
				sShipNode = sSellerOrganizationCode + "_N1";
			}
		}
		// HUB - 8350 [End]
		String date = getDateStamp();
		YFCDocument inDoc = YFCDocument.getDocumentFor(TelstraConstants.ORDER_ORDER_HEADER_KEY
				+ eleOrder.getAttribute(TelstraConstants.ORDER_HEADER_KEY)
				+ "' Action='MODIFY' ModificationReasonText='New OrderLine Created at " + date + "' "
				+ TelstraConstants.ORDER_TYPE + "='" + eleOrder.getAttribute(TelstraConstants.ORDER_TYPE) + "' " + " "
				+ TelstraConstants.ENTRY_TYPE + "='" + eleOrder.getAttribute(TelstraConstants.ENTRY_TYPE) + "' " + " "
				+ TelstraConstants.DEPARTMENT_CODE + "='" + eleOrder.getAttribute(TelstraConstants.DEPARTMENT_CODE)
				+ "'><OrderLines></OrderLines></Order>");
		eleOrderLine.setAttribute("Action", "CREATE");
		// HUB-7096: Processing orders having Multiple ShipNodes and multi-DACs
		// with V&C at order line
		// level - Start
		String sEnterpriseCode = eleOrder.getAttribute("EnterpriseCode");
		String sExternalCustomerID = getDACNode(eleOrderLine, sEnterpriseCode);
		String sEntryType = eleOrder.getAttribute(TelstraConstants.ENTRY_TYPE);
		String sOrderType = eleOrder.getAttribute(TelstraConstants.ORDER_TYPE);
		String sDepartmentCode = eleOrder.getAttribute(TelstraConstants.DEPARTMENT_CODE); 
		String sReceivingNode = "";
		// HUB-8462 -[START]

		if (YFCObject.equals(sEntryType, "INTEGRAL_PLUS") || YFCObject.equals(sEntryType, "STERLING")) {

			// HUB-8462 -[END]

			if (YFCObject.equals(sOrderType, "MATERIAL_RESERVATION")) {
				if (!YFCObject.isVoid(sExternalCustomerID)) {
					sReceivingNode = sExternalCustomerID;
				}
			} else if (YFCObject.equals(sOrderType, "TRANSPORT_ORDER")) {
				if (!YFCObject.isVoid(sExternalCustomerID)) {
					sReceivingNode = sExternalCustomerID;
				} else if (!"NBN".equalsIgnoreCase(sDepartmentCode)){
					/*
					 *The above check( department code != NBN) has been introduced as a part of
					 * HUB-9178. NBN order update will not have DAC in the input  
					 */
					// throw exception
					LoggerUtil.verboseLog("TRANSPORT ORDER has invalid  line", logger, " throwing exception");
					String strErrorCode = getProperty("TransportOrderFailureCode", true);
					throw ExceptionUtil.getYFSException(strErrorCode, new YFSException());
				}
			} else if (YFCObject.equals(sOrderType, "PURCHASE_ORDER")) {
				if (!YFCObject.isVoid(sExternalCustomerID)) {
					sReceivingNode = sExternalCustomerID;
				}
			}
		} else if (YFCObject.equals(sEntryType, "MERIDIAN")) {
			if (YFCObject.equals(sOrderType, "PURCHASE_ORDER")) {
				if (!YFCObject.isVoid(sExternalCustomerID)) {
					sReceivingNode = sExternalCustomerID;
				}
			}
		}
		if(!YFCCommon.isStringVoid(sReceivingNode)){
			eleOrderLine.setAttribute(TelstraConstants.RECEIVING_NODE, sReceivingNode);
		}
		// HUB-7096: Processing orders having Multiple ShipNodes and multi-DACs
		// with V&C at order line
		// level - End

		inDoc.getElementsByTagName(TelstraConstants.ORDER_LINES).item(0).importNode(eleOrderLine.cloneNode(true));

		inDoc.getElementsByTagName(TelstraConstants.ORDER_LINE).item(0).setAttribute(TelstraConstants.SHIP_NODE,
				sShipNode);
		/*
		 * Adding interface number as an identifier whether change order is being invoke from the source or Sterling order
		 */
		inDoc.getDocumentElement().setAttribute(TelstraConstants.INTERFACE_NO, sInterfaceNo);
		YFCDocument outDoc = invokeYantraApi("changeOrder", inDoc, tempDoc);

		//HUB-9372[START]

		YFCElement eleOrderOut = outDoc.getDocumentElement();
		String sPrimeLineNo = eleOrderLine.getAttribute(TelstraConstants.PRIME_LINE_NO, "");
		YFCElement eleTempOrderLine = null;
		for (YFCElement eleOrderListLine : eleOrderOut.getElementsByTagName(TelstraConstants.ORDER_LINE)) {
			String sOrderListPrimeLineNo = eleOrderListLine.getAttribute(TelstraConstants.PRIME_LINE_NO, "");
			if (sOrderListPrimeLineNo.equals(sPrimeLineNo)) {
				eleTempOrderLine = eleOrderListLine;
				break;
			}
		}

		//HUB-9372[END]

		YFCElement eleFreightOrder = outDoc.getElementsByTagName(TelstraConstants.ORDER).item(0);
		String sOrderHeaderKey = eleFreightOrder.getAttribute(TelstraConstants.ORDER_HEADER_KEY, "");

		/*
		 * The below logic will move the freight charge line to Freight Line
		 * Closed which is an extended status of cancelled for the newLines
		 * being added
		 */

		cancelFreightChargeLine(eleOrderLine, sDocumentType, sOrderHeaderKey);

		// HUB-8358-[END]
		return eleTempOrderLine;
	}

	/**
	 * Method for getting OrderList
	 * 
	 * @param sOrderName
	 * @return
	 * @throws YFCException
	 */
	private YFCDocument getOrderList(String sOrderName) throws YFCException {
		YFCDocument docOrderListinXml = YFCDocument.getDocumentFor("<Order OrderName='" + sOrderName
				+ "' ><OrderLine ChainedFromOrderHeaderKeyQryType='ISNULL'/></Order>");
		YFCDocument doctempgetOrderListinXml = YFCDocument.getDocumentFor(
				"<OrderList><Order EnterpriseCode='' DocumentType='' OrderHeaderKey='' SellerOrganizationCode='' BuyerOrganizationCode='' OrderType='' "
						+ " EntryType='' DepartmentCode='' OrderName='' OrderNo='' Status='' > "
						+ "<OrderLines><OrderLine OrderLineKey='' PrimeLineNo='' OrderHeaderKey='' Status='' ShipNode='' StatusQuantity='' OrderedQty='' ShipToID='' ShipToKey=''><Item ItemID='' UnitOfMeasure='' ItemShortDesc='' ItemDesc='' SupplierItem='' /><OrderStatuses ><OrderStatus TotalQuantity='' StatusQty='' Status='' OrderReleaseKey=''/>"
						+ "</OrderStatuses><PersonInfoShipTo /></OrderLine></OrderLines></Order></OrderList>");
		return invokeYantraApi("getOrderList", docOrderListinXml, doctempgetOrderListinXml);
	}
	// HUB-8072 [START]

	/**
	 * Method for partialDeliverOrderLine
	 * 
	 * @param eleOrderLine
	 * @param eleInputOrderLine
	 * @param docGetOrderListOut
	 */
	private void partialDeliverOrderLine(YFCElement eleOrderLine) throws YFCException {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "partialdeliverOrderLine", "");
		String sPrimeLineNo = eleOrderLine.getAttribute(TelstraConstants.PRIME_LINE_NO, "");
		double dPartialCompleteQty = eleOrderLine.getDoubleAttribute(TelstraConstants.STATUS_QUANTITY);
		YFCDocument docGetShipmentListOutput = getShipmentListForPartialCompleteQty(sPrimeLineNo, dPartialCompleteQty);
		YFCNodeList<YFCElement> nlShipment = docGetShipmentListOutput.getElementsByTagName(TelstraConstants.SHIPMENT);
		if (nlShipment.getLength() == 0) {
			
			//HUB-9372[START]

			//Call carrier update if Shipment Exists for the line and getshipmentlist returns null

			if(eleOrderLine.hasAttribute(TelstraConstants.IS_SHIPPED)){
					//Create input to GpsCarrierUpdate with the lines.
					YFCDocument docinputToCarrierUpdate = inputToCarrierUpdatewithLines(eleOrderLine);
					LoggerUtil.verboseLog("Input to GpsCarrierUpdate : : ", logger, docinputToCarrierUpdate.toString());

					invokeYantraService("GpsCarrierUpdate", docinputToCarrierUpdate);
					return;
			}else{

				//double totalNonShippedQty = getTotalNonShippedQty(eleOrderLine);
				//if (totalNonShippedQty >= dPartialCompleteQty) {

				//CreateShipment for Order

				moveOrderToDelivered(eleOrderLine);
			}
			/*} else {
				// throw exception
				LoggerUtil.verboseLog("Not Enough Quantity to Deliver", logger, " throwing exception");
				throw ExceptionUtil.getYFSException("TEL_ERR_1001_006", new YFSException());
			}*/
			
			//HUB-9372[END]

		} else if (nlShipment.getLength() == 1) {
			YFCElement eleShipment = nlShipment.item(0);
			moveShipmentToDelivered(eleShipment);
		}

		else if (nlShipment.getLength() > 1) {
			YFCElement eleShipment = chooseBestShipmentToDeliver(nlShipment);
			moveShipmentToDelivered(eleShipment);
		}
	}

	/**
	 * Method to return input document for GpsCarrierUpdate service
	 * @param eleOrderLine
	 * @return
	 */
	private YFCDocument inputToCarrierUpdatewithLines(YFCElement eleOrderLine) {
		YFCDocument docinputToCarrierUpdate = YFCDocument.getDocumentFor("<Shipment OrderName='" + sOrderName
				+ "' ><ShipmentLines><ShipmentLine /></ShipmentLines><Extn><CarrierUpdateList><CarrierUpdate TransportStatus='Delivered' /></CarrierUpdateList></Extn></Shipment>");
		YFCElement eleShipment = docinputToCarrierUpdate.getDocumentElement().getElementsByTagName(TelstraConstants.SHIPMENT_LINE).item(0);
		eleShipment.setAttribute(TelstraConstants.PRIME_LINE_NO, eleOrderLine.getAttribute(TelstraConstants.PRIME_LINE_NO,""));
		eleShipment.setAttribute(TelstraConstants.SUB_LINE_NO, eleOrderLine.getAttribute(TelstraConstants.SUB_LINE_NO,""));
		eleShipment.setAttribute(TelstraConstants.QUANTITY, eleOrderLine.getAttribute(TelstraConstants.STATUS_QUANTITY,""));
		return docinputToCarrierUpdate;
	}



	/**
	 * Method for chooseBestShipmentToDeliver
	 * 
	 * @param nlShipment
	 * @return eleShipment
	 */
	private YFCElement chooseBestShipmentToDeliver(YFCNodeList<YFCElement> nlShipment) {

		Map<YFCElement, Double> shipMap = new HashMap<YFCElement, Double>();
		for (YFCElement eleShipment : nlShipment) {
			Double dShipmentStatus = eleShipment.getDoubleAttribute(TelstraConstants.STATUS);
			shipMap.put(eleShipment, dShipmentStatus);
		}

		Set<Entry<YFCElement, Double>> set = shipMap.entrySet();
		List<Entry<YFCElement, Double>> list = new ArrayList<Entry<YFCElement, Double>>(set);
		Collections.sort(list, new Comparator<Map.Entry<YFCElement, Double>>() {
			public int compare(Map.Entry<YFCElement, Double> o1, Map.Entry<YFCElement, Double> o2) {
				return (o2.getValue()).compareTo(o1.getValue());
			}
		});
		return list.iterator().next().getKey();
	}

/*
	 * Method for getTotalNonShippedQty
	 * 
	 * @param eleOrderLine
	 * @return dtotalNonShippedQty
	 *//*
	private double getTotalNonShippedQty(YFCElement eleOrderLine) throws YFCException {
		double dtotalNonShippedQty = 0;
		for (YFCElement eleOrderStatus : eleOrderLine.getElementsByTagName("OrderStatus")) {
			double dOrderStatus = eleOrderStatus.getDoubleAttribute("Status");
			if (dOrderStatus < 3349) {
				double dOrderStatusQty = eleOrderStatus.getDoubleAttribute("StatusQty");
				dtotalNonShippedQty = dtotalNonShippedQty + dOrderStatusQty;
			}
		}
		return dtotalNonShippedQty;
	}*/

	/**
	 * Method for moveOrderToDelivered
	 * 
	 * @param eleOrderLine
	 * @return docGetShipmentListOutput
	 */
	private void moveOrderToDelivered(YFCElement eleOrderLine) throws YFCException {
		YFCDocument docInputCreatePOASN = inputToCreateASN(eleOrderLine, sOrderName);
		// Creating shipment in shipped status
		LoggerUtil.verboseLog("Input to GpsProcessPOASNMsg : : ", logger, docInputCreatePOASN.toString());

		YFCDocument docOutputCreatePOASN = invokeYantraService("GpsProcessPOASNMsg", docInputCreatePOASN);

		String sShipmentNo = docOutputCreatePOASN.getDocumentElement().getAttribute(TelstraConstants.SHIPMENT_NO);
		YFCDocument docInputCarrierUpdate = inputToCarrierUpdate(sOrderName, sShipmentNo, "");
		// Moving shipment in Delivered status
		LoggerUtil.verboseLog("Input to GpsCarrierUpdate : : ", logger, docInputCarrierUpdate.toString());

		invokeYantraService("GpsCarrierUpdate", docInputCarrierUpdate);
	}

	/**
	 * Method for moveShipmentToDelivered
	 * 
	 * @param eleShipment
	 * @return
	 */
	private void moveShipmentToDelivered(YFCElement eleShipment) throws YFCException {
		String shipmentStatus = eleShipment.getAttribute("Status");
		// Assuming no Shipment will be in created status
		if ("1400".equals(shipmentStatus) || "1400.10000".equals(shipmentStatus)) {
			YFCDocument carrierUpdateInp = inputToCarrierUpdate(sOrderName, eleShipment.getAttribute("ShipmentNo"),eleShipment.getAttribute(TelstraConstants.TRACKING_NO));
			LoggerUtil.verboseLog("Input to GpsCarrierUpdate : : ", logger, carrierUpdateInp.toString());

			invokeYantraService("GpsCarrierUpdate", carrierUpdateInp);
		}
	}

	/**
	 * Method for getShipmentListForPartialCompleteQty
	 * 
	 * @param sPrimeLineNo
	 * @param sPartialCompleteQty
	 * @return docGetShipmentListOutput
	 */
	private YFCDocument getShipmentListForPartialCompleteQty(String sPrimeLineNo, double sPartialCompleteQty)
			throws YFCException {
		YFCDocument docGetShipmentListOutput = YFCDocument.getDocumentFor("<Shipment />");
		if (!YFCObject.isVoid(sPartialCompleteQty)) {
			YFCDocument docGetShipmentListInput = YFCDocument.getDocumentFor(
					"<Shipment StatusQryType='BETWEEN' FromStatus='1399' ToStatus='1499'><ShipmentLines><ShipmentLine Quantity='"
							+ sPartialCompleteQty + "' PrimeLineNo='" + sPrimeLineNo + "'><Order OrderName='"
							+ sOrderName + "'></Order></ShipmentLine></ShipmentLines></Shipment>");
			LoggerUtil.verboseLog("Input to getShipmentListForPartialCompleteQty : : ", logger, docGetShipmentListInput.toString());
			docGetShipmentListOutput = invokeYantraApi(TelstraConstants.GET_SHIPMENT_LIST_API, docGetShipmentListInput,
					getShipmentListTempate());			
			LoggerUtil.verboseLog("Output to getShipmentListForPartialCompleteQty : : ", logger, docGetShipmentListOutput.toString());
		}
		return docGetShipmentListOutput;
	}

	/**
	 * Method for getShipmentListTempate
	 * 
	 * @return template
	 */
	private YFCDocument getShipmentListTempate() {
		YFCDocument template = YFCDocument.getDocumentFor(
				"<Shipments><Shipment EnterpriseCode='' DocumentType='' ReceivingNode='' ShipmentNo='' ShipmentKey='' Status='' TrackingNo=''>"
						+ "<ShipmentLines><ShipmentLine Quantity='' ShipmentLineKey='' ItemID='' ReceivedQuantity='' PrimeLineNo=''><OrderLine PrimeLineNo=''/></ShipmentLine></ShipmentLines></Shipment></Shipments>");
		return template;
	}

	/**
	 * Method for DeliverOrder
	 * 
	 * @param eleOrderLine
	 * 
	 */

	private void deliverOrderLine(YFCElement eleOrderLine) throws YFCException {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "deliverOrderLine", "");
		for (YFCElement eleOrderStatus : eleOrderLine.getElementsByTagName("OrderStatus")) {
			double dOrderStatus = eleOrderStatus.getDoubleAttribute(TelstraConstants.STATUS);
			if (dOrderStatus < 3700.7776 && dOrderStatus!=9000) {
				String eleOrderStatusQty = eleOrderStatus.getAttribute("StatusQty", "");
				YFCElement eleTempOrderLine = (YFCElement) eleOrderLine.cloneNode(true);
				eleTempOrderLine.setAttribute(TelstraConstants.STATUS_QUANTITY, eleOrderStatusQty);
				if (dOrderStatus > 3349 && dOrderStatus!=9000) {
					LoggerUtil.verboseLog("The Order Line contains Shipment : : ", logger, eleTempOrderLine.toString());
					eleTempOrderLine.setAttribute(TelstraConstants.IS_SHIPPED, "Y");
				}
				LoggerUtil.verboseLog("Invoking partialDeliverOrderLine from  ChangeStatus C : : ", logger, eleTempOrderLine.toString());
				partialDeliverOrderLine(eleTempOrderLine);
			}

		}
	}

	// HUB-8072 [END]

	/**
	 * The method creates the Input Xml to GpsProcessPoAsnMsg Service
	 * 
	 * @param eleOrderLine
	 * @param sOrderName
	 * @return docinputToCreateASN
	 */

	private YFCDocument inputToCreateASN(YFCElement eleOrderLine, String sOrderName) {
		YFCDocument docinputToCreateASN = YFCDocument.getDocumentFor(
				"<Shipment OrderName='" + sOrderName + "'><ShipmentLines><ShipmentLine /></ShipmentLines></Shipment>");
		YFCElement eleShipmentline = docinputToCreateASN.getElementsByTagName(TelstraConstants.SHIPMENT_LINE).item(0);
		if (!YFCObject.isVoid(eleOrderLine.getElementsByTagName(TelstraConstants.ITEM).item(0))) {
			eleShipmentline.setAttribute(TelstraConstants.ITEM_ID, eleOrderLine
					.getElementsByTagName(TelstraConstants.ITEM).item(0).getAttribute(TelstraConstants.ITEM_ID, ""));
			eleShipmentline.setAttribute(TelstraConstants.UNIT_OF_MEASURE,
					eleOrderLine.getElementsByTagName(TelstraConstants.ITEM).item(0)
					.getAttribute(TelstraConstants.UNIT_OF_MEASURE, ""));
			eleShipmentline.setAttribute(TelstraConstants.QUANTITY,
					eleOrderLine.getAttribute(TelstraConstants.STATUS_QUANTITY, ""));
			eleShipmentline.setAttribute(TelstraConstants.PRIME_LINE_NO,
					eleOrderLine.getAttribute(TelstraConstants.PRIME_LINE_NO, ""));
		}
		return docinputToCreateASN;
	}

	/**
	 * The method creates the Input Xml to GpsCarrierUpdate Service to move the
	 * order to delivered
	 * 
	 * @param sOrderName
	 * @param sShipmentNo
	 * @param string 
	 * @return docinputToCarrierUpdate
	 */

	private YFCDocument inputToCarrierUpdate(String sOrderName, String sShipmentNo, String sTrackingNo) {
		YFCDocument docinputToCarrierUpdate = YFCDocument.getDocumentFor("<Shipment OrderName='" + sOrderName
				+ "' ShipmentNo='" + sShipmentNo + "' TrackingNo='" + sTrackingNo
				+ "' ><Extn><CarrierUpdateList><CarrierUpdate TransportStatus='Delivered' /></CarrierUpdateList></Extn></Shipment>");
		return docinputToCarrierUpdate;
	}

	/**
	 * Method for CancelOrder
	 * 
	 * @param sOrderHeaderKey
	 * @param sDocumentType
	 * @param sEnterpriseCode
	 */
	private void cancelOrderLine(YFCElement eleOrderLine, YFCDocument docGetOrderListOut) throws YFCException {

		//HUB-9139 - [START]

		/*Removing the creation of alerts on PO Cancellation*/

		//boolean bAlertRaised = false;

		//HUB-9139 - [END]

		// cancel Sales Order Line
		String sOrderLineKey = eleOrderLine.getAttribute(TelstraConstants.ORDER_LINE_KEY, "");
		String sOrderLineStatus = eleOrderLine.getAttribute("Status", "");

		// HUB-7559 : Begin

		/* Exit this method if the orderline is Cancelled */
		if (("Cancelled").equals(sOrderLineStatus)) {
			return;
		}

		// HUB-7559 : End

		if (sOrderLineStatus.equals("Shipped") || sOrderLineStatus.equals("Intransit")
				|| sOrderLineStatus.equals("Partially Shipped") || sOrderLineStatus.equals("Partially Intransit")) {
			// Shipment is in Shipped status we need to unconfirmShipment and
			// then call cancelOrder
			// get shipment associated with this Sales OrderLine
			String strShipFromStatus = "1400";
			String strShipToStatus = "1499";
			YFCElement eleOrderFromOrderList = docGetOrderListOut.getDocumentElement();
			YFCDocument docGetShipmentForSOInput = YFCDocument
					.getDocumentFor("<Shipment StatusQryType='BETWEEN' FromStatus='" + strShipFromStatus
							+ "' ToStatus='" + strShipToStatus + "' EnterpriseCode='"
							+ eleOrderFromOrderList.getChildElement(TelstraConstants.ORDER)
							.getAttribute("EnterpriseCode")
							+ "'  DocumentType='"
							+ eleOrderFromOrderList.getChildElement(TelstraConstants.ORDER).getAttribute("DocumentType")
							+ "'><ShipmentLines><ShipmentLine OrderLineKey='"
							+ eleOrderLine.getAttribute("OrderLineKey") + "' OrderHeaderKey='"
							+ eleOrderLine.getAttribute("OrderHeaderKey") + "' /></ShipmentLines></Shipment>");
			YFCDocument docGetShipmentListTemplate = YFCDocument
					.getDocumentFor("<Shipments><Shipment ShipmentKey='' Status=''/></Shipments>");
			YFCDocument docGetShipmentForSOOutput = invokeYantraApi("getShipmentList", docGetShipmentForSOInput,
					docGetShipmentListTemplate);
			YFCElement eleShipmentcheck = docGetShipmentForSOOutput.getElementsByTagName(TelstraConstants.SHIPMENT)
					.item(0);
			if (!YFCObject.isVoid(eleShipmentcheck)) {
				for (YFCElement eleShipment : docGetShipmentForSOOutput
						.getElementsByTagName(TelstraConstants.SHIPMENT)) {
					String sShipmentKey = eleShipment.getAttribute(TelstraConstants.SHIPMENT_KEY, "");
					// HUB-6233[Begin]
					Double shipmentStatus = eleShipment.getDoubleAttribute(TelstraConstants.STATUS);
					if (shipmentStatus < 1400.10000) {
						// Moving a Shipment from shipped to Intransit by
						// calling GpsCarrierUpdate

						// HUB-6212[Begin]

						YFCDocument docinputToCarrierUpdate = YFCDocument.getDocumentFor("<Shipment OrderName='"
								+ sOrderName + "'" + " ShipmentNo='" + eleShipment.getAttribute("ShipmentNo", "")
								+ "'><Extn><CarrierUpdateList><CarrierUpdate "
								+ "TransportStatus='Intransit' /></CarrierUpdateList></Extn></Shipment>");
						invokeYantraService("GpsCarrierUpdate", docinputToCarrierUpdate);

						// HUB-6212[End]
					}
					// HUB-6233[End]

					invokeYantraApi("unconfirmShipment",
							YFCDocument.getDocumentFor("<Shipment ShipmentKey='" + sShipmentKey + "'/>"));
				}
			}

			//HUB-9139 - [START]

			/*Removing the creation of alerts on PO Cancellation*/

			/*YFCElement eleOrder = eleOrderFromOrderList.getChildElement(TelstraConstants.ORDER);
			if (eleOrder.getAttribute(TelstraConstants.DOCUMENT_TYPE, "").equals(TelstraConstants.DOCUMENT_TYPE_0005)) {
				// Raise Shipped PO Cancelled Alert by calling =
				// GpsRaiseShippedPOCancelledAlert service
				YFCDocument docAlertInput = YFCDocument
						.getDocumentFor("<Alert OrderHeaderKey='" + eleOrder.getAttribute("OrderHeaderKey", "") + "' "
								+ "OrderNo='" + eleOrder.getAttribute("OrderNo", "") + "' DocumentType='"
								+ eleOrder.getAttribute("DocumentType", "") + "' " + "EnterpriseCode='"
								+ eleOrder.getAttribute("EnterpriseCode", "") + "' OrderName='"
								+ eleOrder.getAttribute("OrderName", "") + "' " + "DepartmentCode='"
								+ eleOrder.getAttribute("DepartmentCode", "") + "' EntryType='"
								+ eleOrder.getAttribute("EntryType", "") + "' " + "OrderType='"
								+ eleOrder.getAttribute("OrderType", "") + "' BuyerOrganizationCode='"
								+ eleOrder.getAttribute("BuyerOrganizationCode", "") + "' " + "SellerOrganizationCode='"
								+ eleOrder.getAttribute("SellerOrganizationCode", "") + "'/>");

				String sShippedPOCancelledService = getProperty("Service.ShippedPOCancelledAlert", true);
				invokeYantraService(sShippedPOCancelledService, docAlertInput);
				bAlertRaised = true;

			}*/

			//HUB-9139 - [END]
		}


		if (sOrderLineStatus.equals("Included In Shipment") || sOrderLineStatus.equals("Order Picked")
				|| sOrderLineStatus.equals("Shipped") || sOrderLineStatus.equals("Intransit")
				|| sOrderLineStatus.equals("Partially Intransit") || sOrderLineStatus.equals("Partially Shipped")
				|| sOrderLineStatus.equals("Partially Order Picked")) {
			/*
			 * Shipment is in Created status we need to call changeShipment with
			 * Action = Cancel
			 */
			String strShipFromStatus = "1100";
			String strShipToStatus = "1399";
			YFCElement eleOrderFromOrderList = docGetOrderListOut.getDocumentElement();
			YFCDocument docGetShipmentForSOInput = YFCDocument
					.getDocumentFor("<Shipment StatusQryType='BETWEEN' FromStatus='" + strShipFromStatus
							+ "' ToStatus='" + strShipToStatus + "' EnterpriseCode='"
							+ eleOrderFromOrderList.getChildElement(TelstraConstants.ORDER)
							.getAttribute("EnterpriseCode")
							+ "'  DocumentType='"
							+ eleOrderFromOrderList.getChildElement(TelstraConstants.ORDER).getAttribute("DocumentType")
							+ "'><ShipmentLines><ShipmentLine OrderLineKey='"
							+ eleOrderLine.getAttribute("OrderLineKey") + "' OrderHeaderKey='"
							+ eleOrderLine.getAttribute("OrderHeaderKey") + "' /></ShipmentLines></Shipment>");
			YFCDocument docGetShipmentListTemplate = YFCDocument
					.getDocumentFor("<Shipments><Shipment ShipmentKey='' Status=''/></Shipments>");
			YFCDocument docGetShipmentForSOOutput = invokeYantraApi("getShipmentList", docGetShipmentForSOInput,
					docGetShipmentListTemplate);
			YFCElement eleShipmentcheck = docGetShipmentForSOOutput.getElementsByTagName(TelstraConstants.SHIPMENT)
					.item(0);
			if (!YFCObject.isVoid(eleShipmentcheck)) {
				for (YFCElement eleShipment : docGetShipmentForSOOutput
						.getElementsByTagName(TelstraConstants.SHIPMENT)) {
					String sShipmentKey = eleShipment.getAttribute(TelstraConstants.SHIPMENT_KEY, "");
					invokeYantraApi("changeShipment", YFCDocument
							.getDocumentFor("<Shipment ShipmentKey='" + sShipmentKey + "' Action='Cancel'/>"));
				}
			}

		}

		String date = getDateStamp();
		String sOrderHeaderKey = docGetOrderListOut.getElementsByTagName(TelstraConstants.ORDER).item(0)
				.getAttribute(TelstraConstants.ORDER_HEADER_KEY, "");
		YFCDocument docchangeOrderinXml = YFCDocument.getDocumentFor(TelstraConstants.ORDER_ORDER_HEADER_KEY
				+ sOrderHeaderKey + "' " + "Action='MODIFY' ModificationReasonText='Order Line # "
				+ eleOrderLine.getAttribute("PrimeLineNo", "") + " CANCELLED at " + date
				+ "'><OrderLines><OrderLine Action='CANCEL' OrderLineKey='" + sOrderLineKey
				+ "'></OrderLine></OrderLines></Order>");
		invokeYantraApi("cancelOrder", docchangeOrderinXml);

		//HUB-9139 - [START]

		/*Removing the creation of alerts on PO Cancellation*/

		/*YFCElement eleOrderFromOrderList = docGetOrderListOut.getDocumentElement();
		YFCElement eleOrder = eleOrderFromOrderList.getChildElement(TelstraConstants.ORDER);


		if (eleOrder.getAttribute(TelstraConstants.DOCUMENT_TYPE, "").equals(TelstraConstants.DOCUMENT_TYPE_0005)
				&& !bAlertRaised) {
			// Raise Shipped PO Cancelled Alert by calling =
			// GpsRaiseShippedPOCancelledAlert service
			YFCDocument docAlertInput = YFCDocument
					.getDocumentFor("<Alert OrderHeaderKey='" + eleOrder.getAttribute("OrderHeaderKey", "") + "' "
							+ "OrderNo='" + eleOrder.getAttribute("OrderNo", "") + "' DocumentType='"
							+ eleOrder.getAttribute("DocumentType", "") + "' " + "EnterpriseCode='"
							+ eleOrder.getAttribute("EnterpriseCode", "") + "' OrderName='"
							+ eleOrder.getAttribute("OrderName", "") + "' " + "DepartmentCode='"
							+ eleOrder.getAttribute("DepartmentCode", "") + "' EntryType='"
							+ eleOrder.getAttribute("EntryType", "") + "' " + "OrderType='"
							+ eleOrder.getAttribute("OrderType", "") + "' BuyerOrganizationCode='"
							+ eleOrder.getAttribute("BuyerOrganizationCode", "") + "' " + "SellerOrganizationCode='"
							+ eleOrder.getAttribute("SellerOrganizationCode", "") + "'/>");

			String sPOCancelledService = getProperty("Service.POCancelledAlert", true);
			invokeYantraService(sPOCancelledService, docAlertInput);
			bAlertRaised = true;

		}*/

		//HUB-9139 - [END]

	}

	/**
	 * This method is invoked for change order input coming to backorder quantities in the exisiting line 
	 * @param eleOrderLine
	 * @throws YFCException
	 */

	private void backOrderLine(YFCElement eleOrderLine, YFCDocument docGetOrderListOutXml, YFCElement eleInputOrderLine,
			String sInterfaceNo, String sEntryType) throws YFCException {

		double dBackoredQtyInInput = eleInputOrderLine.getDoubleAttribute(TelstraConstants.BACKORDERED_QTY, 0.00);
		double dBackorderedQtyInOrder = 0.00;
		//HUB-9070
		YFCNodeList<YFCNode> yfcNlOrderStatus = XPathUtil.getXpathNodeList(YFCDocument.getDocumentFor(eleOrderLine.toString()), "//OrderStatus[@Status='1300']");

		for(YFCNode yfcNOrderStatus : yfcNlOrderStatus){
			double dStatusQty = ((YFCElement)yfcNOrderStatus).getDoubleAttribute(TelstraConstants.STATUS_QTY);
			dBackorderedQtyInOrder = dBackorderedQtyInOrder + dStatusQty;
		}
		LoggerUtil.verboseLog("moveQtyToCreatedOrReleaseStatus :: dBackorderedQtyInOrder", logger,
				dBackorderedQtyInOrder);
		double dNewQtyToBackorder = dBackoredQtyInInput - dBackorderedQtyInOrder;
		LoggerUtil.verboseLog("moveQtyToCreatedOrReleaseStatus :: dNewQtyToBackorder", logger,
				dNewQtyToBackorder);
		/*
		 * If dQtyToBackorder is positive, it means delta quantities need to be
		 * back ordered.
		 */
		if (dNewQtyToBackorder >= 0) {
			/*
			 * If new ordered qty is more than the new back ordered qty, then delta qty needs to be added in created/released status
			 * based on the line status
			 */
			changeOrderToUpdateOrderLine(eleInputOrderLine, eleOrderLine, docGetOrderListOutXml, sInterfaceNo,
					sEntryType, dNewQtyToBackorder);
			if (dNewQtyToBackorder > 0) {
				/*
				 * This method moves the new created quantity through change order to the backordered status
				 */
				moveQtyToBackOrder(docGetOrderListOutXml, eleOrderLine, dNewQtyToBackorder);
			}
		}
		/*
		 * If dQtyToBackorder is negative, it means that quantity either needs
		 * to be move to created/released status
		 */
		else if (dNewQtyToBackorder < 0) {
			moveQtyToCreatedOrReleaseStatus(docGetOrderListOutXml, eleOrderLine, dNewQtyToBackorder, eleInputOrderLine,
					dBackorderedQtyInOrder);
		}
	}

	/**
	 * This method is invoked for changeOrder scenario. It moves the delta
	 * quantity back to created/released status base on current order line
	 * status
	 * 
	 * @param docGetOrderListOutXml
	 * @param eleOrderLine
	 * @param eleInputOrderLine
	 * @param dBackoredQtyInInput
	 * @param dQtyToBackorder
	 */
	private void moveQtyToCreatedOrReleaseStatus(YFCDocument docGetOrderListOutXml, YFCElement eleOrderLine,
			double dNewQtyToBackorder, YFCElement eleInputOrderLine, double dBackorderedQtyInOrder) {

		String sOrderHeaderKey = eleOrderLine.getAttribute(TelstraConstants.ORDER_HEADER_KEY);
		String sOrderLineKey = eleOrderLine.getAttribute(TelstraConstants.ORDER_LINE_KEY);
		String date = getDateStamp();
		double dOrderedQtyInTheOrder = eleOrderLine.getDoubleAttribute(TelstraConstants.ORDERED_QTY);
		double dNewOrderedQty = eleInputOrderLine.getDoubleAttribute(TelstraConstants.ORDERED_QTY, 0.00);
		double dNewBackorderedQty = eleInputOrderLine.getDoubleAttribute(TelstraConstants.BACKORDERED_QTY, 0.00);
		double dQtyToMoveBack = -dNewQtyToBackorder;

		String sOrderReleaseKey = "";
		for (YFCElement eleOrderStatus : eleOrderLine.getElementsByTagName("OrderStatus")) {
			if ("3200".equals(eleOrderStatus.getAttribute(TelstraConstants.STATUS, ""))) {
				sOrderReleaseKey = eleOrderStatus.getAttribute(TelstraConstants.ORDER_RELEASE_KEY, "");
				break;
			}
		}
		/*
		 * If any of the quantity of this line is in released status, then new qty will
		 * be added in the existing release
		 */
		if (!YFCCommon.isStringVoid(sOrderReleaseKey)) {
			double dNewOrderedQtyForChangeOrder = 0.00;
			/*
			 * If ordered qty and backordered qty are equal and new backordered
			 * qty is less than the old one. We will have to move old BO - new
			 * BO quantity back to created/released based on the current line
			 * status
			 */
			if (dNewOrderedQty == dNewBackorderedQty) {
				dNewOrderedQtyForChangeOrder = dOrderedQtyInTheOrder - dQtyToMoveBack;
			}
			/*
			 * If new ordered qty and backordered qty are not same, then delta qty will be 
			 * added in the existing release and the extra backordered qty will be cancelled
			 * through change order by reducing the ordered qty			
			 */
			else {
				dQtyToMoveBack = dNewOrderedQty - dNewBackorderedQty;
				double dNonBOQtyInOrder = dOrderedQtyInTheOrder - dBackorderedQtyInOrder;
				dNewOrderedQtyForChangeOrder = dNonBOQtyInOrder + dNewBackorderedQty;
			}

			/*
			 * Back ordered quantity can not be move to the existing released
			 * through change release API. So calling change order to cancel the
			 * back ordered quantity which should move to the released status
			 * and through change release adding it back
			 */

			YFCDocument yfcDocChangeOrderIp = YFCDocument.getDocumentFor("<Order OrderHeaderKey='" + sOrderHeaderKey
					+ "'><OrderLines><OrderLine OrderLineKey='" + sOrderLineKey + "' OrderedQty='"
					+ dNewOrderedQtyForChangeOrder + "'/></OrderLines></Order>");
			LoggerUtil.verboseLog("moveQtyToCreatedOrReleaseStatus :: yfcDocChangeOrderIp", logger,
					yfcDocChangeOrderIp);
			/*
			 * Adding interface number as an identifier whether change order is being invoke from the source or Sterling order
			 */
			yfcDocChangeOrderIp.getDocumentElement().setAttribute(TelstraConstants.INTERFACE_NO, sInterfaceNo);
			invokeYantraApi("changeOrder", yfcDocChangeOrderIp);
			/*
			 * Adding the cancelled qty back through change release which will
			 * make sure it added in the existing release
			 */
			YFCDocument docOrderReleaseinXml = YFCDocument
					.getDocumentFor("<OrderRelease Action='MODIFY' ModificationReasonText='Added to a release at "
							+ date + "' OrderReleaseKey='" + sOrderReleaseKey + "'>"
							+ "<OrderLines><OrderLine Action='' OrderLineKey='" + sOrderLineKey + "' ChangeInQuantity='"
							+ dQtyToMoveBack + "'/></OrderLines></OrderRelease>");
			invokeYantraApi("changeRelease", docOrderReleaseinXml);

		}
		/*
		 * No quantity is in released status, so new quantity would be added in
		 * created status
		 */
		else {
			/*
			 * If new ordered quantity in not equal to new backordered qty, then
			 * total order line qty will be non BO quantity in the order line +
			 * total new ordered qty
			 */
			if (dNewOrderedQty != dNewBackorderedQty) {
				double dNonBOQtyInOrder = dOrderedQtyInTheOrder - dBackorderedQtyInOrder;
				double dNewOrderedQtyForChangeOrder = dNonBOQtyInOrder + dNewOrderedQty;

				YFCDocument yfcDocChangeOrderIp = YFCDocument.getDocumentFor("<Order OrderHeaderKey='" + sOrderHeaderKey
						+ "'><OrderLines><OrderLine OrderLineKey='" + sOrderLineKey + "' OrderedQty='"
						+ dNewOrderedQtyForChangeOrder + "'/></OrderLines></Order>");
				LoggerUtil.verboseLog("moveQtyToCreatedOrReleaseStatus :: yfcDocChangeOrderIp", logger,
						yfcDocChangeOrderIp);
				/*
				 * Adding interface number as an identifier whether change order is being invoke from the source or Sterling order
				 */
				yfcDocChangeOrderIp.getDocumentElement().setAttribute(TelstraConstants.INTERFACE_NO, sInterfaceNo);
				invokeYantraApi("changeOrder", yfcDocChangeOrderIp);
			}
			moveBOQuantityToCreatedStatus(docGetOrderListOutXml, eleOrderLine, date, dQtyToMoveBack);
		}
	}

	/**
	 * This method moves back the backordered qty passed in the parameter to
	 * created status through custom change order status transaction
	 * 
	 * @param docGetOrderListOutXml
	 * @param eleOrderLine
	 * @param date
	 * @param dQtyToMoveBack
	 */
	private void moveBOQuantityToCreatedStatus(YFCDocument docGetOrderListOutXml, YFCElement eleOrderLine, String date,
			double dQtyToMoveBack) {

		String sDocumentType = docGetOrderListOutXml.getElementsByTagName("Order").item(0)
				.getAttribute(TelstraConstants.DOCUMENT_TYPE);

		String sTransactionID = getProperty("ChangeOrderStatusToCreated", true) + sDocumentType + ".ex";// CHANGE_ORDER_STATUS_TO_CREATED.0001.ex
		YFCDocument docchangeOrderinXml = YFCDocument.getDocumentFor(TelstraConstants.ORDER_ORDER_HEADER_KEY
				+ eleOrderLine.getAttribute(TelstraConstants.ORDER_HEADER_KEY, "") + "' TransactionId='"
				+ sTransactionID + "' ModificationReasonText='Order Line# "
				+ eleOrderLine.getAttribute("PrimeLineNo", "") + " Move to created status at " + date
				+ "'><OrderLines><OrderLine BaseDropStatus='1100' Quantity='" + dQtyToMoveBack + "' OrderLineKey='"
				+ eleOrderLine.getAttribute(TelstraConstants.ORDER_LINE_KEY, "") + "'/></OrderLines></Order>");
		invokeYantraApi("changeOrderStatus", docchangeOrderinXml);

	}

	/**
	 * This method moves the new created quantity through change order to the backordered status
	 * 
	 * @param docGetOrderListOutXml
	 * @param eleOrderLine
	 * @param dQtyToBackorder
	 */
	private void moveQtyToBackOrder(YFCDocument docGetOrderListOutXml, YFCElement eleOrderLine,
			double dQtyToBackorder) {

		String sDocumentType = docGetOrderListOutXml.getElementsByTagName("Order").item(0)
				.getAttribute(TelstraConstants.DOCUMENT_TYPE);
		String date = getDateStamp();
		YFCDocument docchangeOrderStatusinXml = YFCDocument.getDocumentFor(TelstraConstants.ORDER_ORDER_HEADER_KEY
				+ eleOrderLine.getAttribute(TelstraConstants.ORDER_HEADER_KEY, "") + "' TransactionId='SCHEDULE."
				+ sDocumentType + "' ModificationReasonText='Order Line# "
				+ eleOrderLine.getAttribute("PrimeLineNo", "") + " Backordered at " + date
				+ "'><OrderLines><OrderLine BaseDropStatus='1300' Quantity='" + dQtyToBackorder + "' OrderLineKey='"
				+ eleOrderLine.getAttribute(TelstraConstants.ORDER_LINE_KEY, "") + "'/></OrderLines></Order>");

		invokeYantraApi("changeOrderStatus", docchangeOrderStatusinXml);
	}

	/**
	 * 
	 * @param inxml
	 * @return
	 * @throws YFCException
	 */
	// HUB-7096: Processing orders having Multiple ShipNodes and multi-DACs with
	// V&C at order line
	// level - Start
	private String getDACNode(YFCElement orderLine, String enterpriseCode) throws YFCException {
		String sExternalCustomerID = null;
		String sCustomerID = null;
		String sCustomerKey = null;
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "getDACNode", "");

		YFCDocument docCustomerListoutXml = getCustomer(orderLine);
		YFCElement eleCustomer = docCustomerListoutXml.getDocumentElement().getChildElement(TelstraConstants.CUSTOMER);
		YFCElement extnEle = orderLine.getChildElement(TelstraConstants.EXTN);
		if (YFCObject.isVoid(extnEle)) {
			extnEle = orderLine.createChild("Extn");
		}
		if (!YFCObject.isNull(eleCustomer)) {
			extnEle.setAttribute("CustomerKey", eleCustomer.getAttribute("CustomerKey"));
			sExternalCustomerID = eleCustomer.getAttribute(TelstraConstants.EXTERNAL_CUSTOMER_ID, "");
			sCustomerID = eleCustomer.getAttribute(TelstraConstants.CUSTOMER_ID, "");
			orderLine.setAttribute(TelstraConstants.BILL_TO_ID, sCustomerID);
			orderLine.setAttribute(TelstraConstants.SHIP_TO_ID, sCustomerID);
			orderLine.removeAttribute(TelstraConstants.BILL_TO_KEY);
			orderLine.removeAttribute(TelstraConstants.SHIP_TO_KEY);
		} else {
			// check common code
			YFCDocument docGetCommonCodeListOutput = invokeYantraApi(TelstraConstants.API_GET_COMMON_CODE_LIST,
					YFCDocument.getDocumentFor("<CommonCode CodeType=\"ORDER_VALIDATION\"/>"));
			boolean bValidation = false;
			String sValidationType = getProperty("ValidationType", true);
			for (YFCElement eleCommonCode : docGetCommonCodeListOutput
					.getElementsByTagName(TelstraConstants.COMMON_CODE)) {
				if (eleCommonCode.getAttribute(TelstraConstants.CODE_VALUE).equals(sValidationType)
						&& eleCommonCode.getAttribute(TelstraConstants.CODE_SHORT_DESCRIPTION).equals("Y")) {
					bValidation = true;
					break;
				}
			}

			sCustomerID = orderLine.getAttribute(TelstraConstants.SHIP_TO_ID, "");
			sCustomerKey = orderLine.getAttribute(TelstraConstants.SHIP_TO_KEY, "");

			if (bValidation) {

				if (YFCObject.isNull(sCustomerID)) {
					sCustomerID = sCustomerKey;
				}

				if (!YFCObject.isNull(sCustomerID)) {
					if (sCustomerID.startsWith("V")) {
						sExternalCustomerID = sCustomerID;
					}
					/*
					 * If CustomerId is Null, it means both CustomerId and
					 * CustomerKey are null (as CustomerId was set as
					 * CustomerKey).
					 */
					YFCDocument createCustomerOutput = createcustomer(orderLine, sCustomerID, sCustomerKey,
							sExternalCustomerID, enterpriseCode);
					if (!YFCObject.isVoid(createCustomerOutput)) {
						extnEle.setAttribute("CustomerKey",
								createCustomerOutput.getDocumentElement().getAttribute("CustomerKey"));
					}
				}
			}
		}
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "getDACNode", "");
		return sExternalCustomerID;
	}

	/**
	 * 
	 * @param orderLine
	 * @return
	 */
	private YFCDocument getCustomer(YFCElement orderLine) {

		String sShipToID = orderLine.getAttribute(TelstraConstants.SHIP_TO_ID, "");
		String sShipToKey = orderLine.getAttribute(TelstraConstants.SHIP_TO_KEY, "");
		if (sShipToKey.length() > 24) {
			sShipToKey = "";
		}

		if (sShipToID.equals("") && sShipToKey.equals("")) {
			return YFCDocument.getDocumentFor("<CustomerList/>");
		} else {
			YFCDocument docCustomerListinXml = YFCDocument.getDocumentFor(
					"<Customer>" + "<ComplexQuery Operator='OR'>" + "<Or>" + "<Exp Name='CustomerID' Value='"
							+ sShipToID + "' QryType='EQ'/>" + "<Exp Name='CustomerKey' Value='" + sShipToID
							+ "' QryType='EQ'/>" + "<Exp Name='CustomerID' Value='" + sShipToKey + "' QryType='EQ'/>"
							+ "<Exp Name='CustomerKey' Value='" + sShipToKey + "' QryType='EQ'/>" + "</Or>"
							+ "</ComplexQuery>" + "</Customer>");

			YFCDocument docCustomerListtempXml = YFCDocument.getDocumentFor(
					"<CustomerList><Customer ExternalCustomerID='' CustomerKey='' CustomerID=''/></CustomerList>");

			YFCDocument docCustomerListoutXml = invokeYantraApi("getCustomerList", docCustomerListinXml,
					docCustomerListtempXml);

			return docCustomerListoutXml;
		}

	}

	/**
	 * creating Consumer input xml after validation
	 * 
	 * @param inxml
	 * @param sCustomerID
	 * @throws YFCException
	 */

	private YFCDocument createcustomer(YFCElement orderLine, String sCustomerID, String sCustomerKey,
			String sExternalCustomerID, String enterpriseCode) throws YFCException {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "createcustomer", orderLine);
		YFCDocument docCustomer = YFCDocument.createDocument("Customer");

		if (!YFCObject.isNull(sCustomerID))
			docCustomer.getDocumentElement().setAttribute(TelstraConstants.CUSTOMER_ID, sCustomerID);
		else
			docCustomer.getDocumentElement().setAttribute(TelstraConstants.CUSTOMER_ID, sCustomerKey);
		if (!YFCObject.isNull(sCustomerKey))
			docCustomer.getDocumentElement().setAttribute(TelstraConstants.CUSTOMER_KEY, sCustomerKey);
		else
			docCustomer.getDocumentElement().setAttribute(TelstraConstants.CUSTOMER_KEY, sCustomerID);
		docCustomer.getDocumentElement().setAttribute("CustomerType", "02");
		docCustomer.getDocumentElement().setAttribute(TelstraConstants.ORGANIZATION_CODE, enterpriseCode);
		if (!YFCObject.isNull(sExternalCustomerID)) {
			docCustomer.getDocumentElement().setAttribute("ExternalCustomerID", sExternalCustomerID);
		}
		docCustomer.getDocumentElement().setAttribute(TelstraConstants.STATUS, "10");
		YFCElement docConsumer = docCustomer.getDocumentElement().createChild("Consumer");
		YFCElement eleBillingPersonInfo = orderLine.getChildElement(TelstraConstants.PERSON_INFO_SHIP_TO);
		/*
		 * HUB-8584
		 */
		if(YFCCommon.isVoid(eleBillingPersonInfo)){
			throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.CREATE_ORDER_CUSTOMER_CREATION_FAILED_ERROR_CODE,	new YFSException());
		}

		docConsumer.appendChild(docCustomer.importNode(eleBillingPersonInfo, true));
		docCustomer.getDocument().renameNode(
				docConsumer.getChildElement(TelstraConstants.PERSON_INFO_SHIP_TO).getDOMNode(), null,
				"BillingPersonInfo");
		String gpsManageDAC = getProperty("SERVICE_NAME_GpsManageDAC", true);
		logger.verbose("docCustomer:" + docCustomer);
		YFCDocument gpsManageDacOutput = invokeYantraService(gpsManageDAC, docCustomer);
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "createcustomer", orderLine);
		return gpsManageDacOutput;
	}

	// HUB-7096: Processing orders having Multiple ShipNodes and multi-DACs with
	// V&C at order line
	// level - Start

	/**
	 * 
	 * @param sExternalCustomerID
	 * @return
	 */
	// Finding the NodeType for the Node
	private String getNodeType(String sExternalCustomerID) {
		LoggerUtil.verboseLog("ExternalCustomerID" + sExternalCustomerID, logger, "");
		String sNodeType = null;
		YFCDocument docNodeListinXml = YFCDocument
				.getDocumentFor("<Shipment ShipNode='" + sExternalCustomerID + "' />");
		YFCDocument doctempgetNodeListinXml = YFCDocument
				.getDocumentFor("<ShipNodeList><ShipNode NodeType=''/></ShipNodeList>");
		YFCDocument outXml = invokeYantraApi("getShipNodeList", docNodeListinXml, doctempgetNodeListinXml);
		YFCElement eleShipNode = outXml.getDocumentElement().getChildElement("ShipNode");
		if (!YFCElement.isNull(eleShipNode)) {
			sNodeType = outXml.getDocumentElement().getChildElement("ShipNode").getAttribute(TelstraConstants.NODE_TYPE,
					"");
		}
		return sNodeType;
	}

	/**
	 * 
	 * @param sBillToID
	 * @return
	 */
	private Boolean getGoNoDAC(String sBillToID) {
		LoggerUtil.verboseLog("BillToID" + sBillToID, logger, "");

		boolean bValidation = false;
		YFCDocument docGetCommonCodeListOutput = invokeYantraApi(TelstraConstants.API_GET_COMMON_CODE_LIST,
				YFCDocument.getDocumentFor("<CommonCode CodeType=\"GODACLIST\" CodeValue='" + sBillToID + "'/>"));
		if (docGetCommonCodeListOutput.getElementsByTagName(TelstraConstants.COMMON_CODE).getLength() > 0) {
			bValidation = true;
		}
		return bValidation;
	}

	/**
	 * Creating order Setting Document Type node DepartmentCode
	 * 
	 * @param inxml
	 */

	private YFCDocument createOrder(YFCDocument inxml) {
		LoggerUtil.verboseLog("OrderNo ", logger, "");

		String sOrderType = inxml.getDocumentElement().getAttribute(TelstraConstants.ORDER_TYPE, "");
		String sEntryType = inxml.getDocumentElement().getAttribute(TelstraConstants.ENTRY_TYPE, "");
		sDepartmentCode = inxml.getDocumentElement().getAttribute(TelstraConstants.DEPARTMENT_CODE, "");
		String sInterfaceNo = inxml.getDocumentElement().getAttribute(TelstraConstants.INTERFACE_NO, "");

		String sSellerOrganizationCode = inxml.getDocumentElement()
				.getAttribute(TelstraConstants.SELLER_ORGANIZATION_CODE, "");
		String sEnterpriseCode = inxml.getDocumentElement().getAttribute(TelstraConstants.ENTERPRISE_CODE);

		boolean bGoNoDAC = false; // to find common code list
		String sDocumentType = null;
		String sExternalCustomerID = null;
		String sNodeType = null;
		Map<String, String> orderLineDocumentsMap = new HashMap<String, String>();
		YFCNodeList<YFCElement> orderLinesList = inxml.getElementsByTagName("OrderLine");
		boolean allLinesHaveReceivingNode = false;
		// HUB-7096: Processing orders having Multiple ShipNodes and multi-DACs
		// with V&C at order line
		// level - Begin
		for (YFCElement orderLine : orderLinesList) {
			String sReceivingNode = "";
			sExternalCustomerID = getDACNode(orderLine, sEnterpriseCode);

			// HUB-7096: Processing orders having Multiple ShipNodes and
			// multi-DACs with V&C at order line
			// level - Begin

			// HUB-8462 -[START]

			if (YFCObject.equals(sEntryType, "INTEGRAL_PLUS") || YFCObject.equals(sEntryType, "STERLING")) {

				// HUB-8462 -[END]

				if (YFCObject.equals(sOrderType, "MATERIAL_RESERVATION")) {
					if (!YFCObject.isVoid(sExternalCustomerID) && isDacCT(sExternalCustomerID)) {
						sDocumentType = TelstraConstants.DOCUMENT_TYPE_0006;
						orderLineDocumentsMap.put(orderLine.getAttribute("PrimeLineNo"), sExternalCustomerID);
						sReceivingNode = sExternalCustomerID;
						/*
						 * HUB-8525 - [Begin][[SP Create Order]User is able to
						 * create an Inventory Material-Material Reservation
						 * order which has one SO orderline and one TO orderline
						 */
						String sSourceSystem = inxml.getDocumentElement().getAttribute(TelstraConstants.SOURCE_SYSTEM);
						if (TelstraConstants.STERLING_SOURCE_SYSTEM.equals(sSourceSystem)
								&& YFCObject.isVoid(sReceivingNode)) {
							LoggerUtil.errorLog("Unable to determine Receiving Node for the line "
									+ orderLine.getAttribute("PrimeLineNo"), logger, " throwing exception");
							throw ExceptionUtil.getYFSException(
									TelstraErrorCodeConstants.CREATE_ORDER_BLANK_RECEIVING_NODE_FOR_TO,
									new YFSException());
						}
						/*
						 * HUB-8525 - [End][[SP Create Order]User is able to
						 * create an Inventory Material-Material Reservation
						 * order which has one SO orderline and one TO orderline
						 */
						allLinesHaveReceivingNode = true;
					} else {
						sDocumentType = TelstraConstants.DOCUMENT_TYPE_0001;
						allLinesHaveReceivingNode = false;
						break;
					}
				} else if (YFCObject.equals(sOrderType, "TRANSPORT_ORDER")) {
					if (YFCObject.equals(sDepartmentCode, "NBN")) {
						sDocumentType = TelstraConstants.DOCUMENT_TYPE_0005;
					} else if (!YFCObject.isVoid(sExternalCustomerID)) {
						sDocumentType = TelstraConstants.DOCUMENT_TYPE_0006;
						orderLineDocumentsMap.put(orderLine.getAttribute("PrimeLineNo"), sExternalCustomerID);
						allLinesHaveReceivingNode = true;
					} else {
						// throw exception
						allLinesHaveReceivingNode = false;
						LoggerUtil.verboseLog("TRANSPORT ORDER is not valid", logger, " throwing exception");
						String strErrorCode = getProperty("TransportOrderFailureCode", true);
						throw ExceptionUtil.getYFSException(strErrorCode, new YFSException());
					}
					sReceivingNode = sExternalCustomerID;
				} else if (YFCObject.equals(sOrderType, "PURCHASE_ORDER")) {
					sDocumentType = TelstraConstants.DOCUMENT_TYPE_0005;
					if (!YFCObject.isVoid(sExternalCustomerID)) {
						sReceivingNode = sExternalCustomerID;
					}
				}
			} else if (YFCObject.equals(sEntryType, "VECTOR")) {
				if (YFCObject.equals(sOrderType, "MATERIAL_RESERVATION")) {
					sDocumentType = TelstraConstants.DOCUMENT_TYPE_0001;
				} else if (YFCObject.equals(sOrderType, "RECOVERY_ORDER")) {
					sDocumentType = TelstraConstants.DOCUMENT_TYPE_0003;
				}
			} else if (YFCObject.equals(sEntryType, "MERIDIAN")) {
				if (YFCObject.equals(sOrderType, "PURCHASE_ORDER")) {
					if (!YFCObject.isVoid(sExternalCustomerID)) {
						sDocumentType = TelstraConstants.DOCUMENT_TYPE_0005;
						sReceivingNode = sExternalCustomerID;
					}
				}
			}
			if(!YFCCommon.isStringVoid(sReceivingNode)){
				orderLine.setAttribute(TelstraConstants.RECEIVING_NODE, sReceivingNode);
			}
		}

		/*
		 * HUB-7003
		 */
		for (YFCElement orderLine : orderLinesList) {
			//HUB-9345-Begin
			getDACNode(orderLine, sEnterpriseCode);
			//HUB-9345-End
			boolean determineNoGoDAC = getGoNoDAC(orderLine.getAttribute(TelstraConstants.SHIP_TO_ID)); // to
			/*
			 * find common code list
			 */
			if (determineNoGoDAC) {
				bGoNoDAC = true;

			}
		}

		if (!allLinesHaveReceivingNode && orderLineDocumentsMap.size() > 0) {
			for (YFCElement orderLine : orderLinesList) {
				if (orderLineDocumentsMap.containsKey(orderLine.getAttribute("PrimeLineNo"))) {
					orderLine.setAttribute("ReceivingNode", "");
					orderLineDocumentsMap.remove(orderLine.getAttribute("PrimeLineNo"));
				}

				if (YFCObject.equals(sDocumentType, "0005")) {
					if (!YFCObject.equals(sSellerOrganizationCode, "TELSTRA")
							&& !YFCObject.equals(sSellerOrganizationCode, "NBN")) {
						String sShipNode = sSellerOrganizationCode + "_N1";
						orderLine.setAttribute(TelstraConstants.SHIP_NODE, sShipNode);
					}
				}
			}
		}
		// HUB-7096: Processing orders having Multiple ShipNodes and multi-DACs
		// with V&C at order line
		// level - End

		if (!YFCObject.isNull(sExternalCustomerID)) {
			sNodeType = getNodeType(sExternalCustomerID);
		}

		if (YFCObject.equals(sInterfaceNo, "INT_ODR_4")) {
			if (YFCObject.isNull(sDepartmentCode)) {
				if (!YFCObject.isNull(sExternalCustomerID)) {
					sDepartmentCode = "INVENTORY_MATERIAL";
				} else {
					// HUB-6096: [Begin]
					String sOLReceivingNode = XPathUtil.getXpathAttribute(inxml,
							"//Order/OrderLines/OrderLine/@ReceivingNode");
					if (!YFCCommon.isStringVoid(sOLReceivingNode)) {
						String sOLNodeType = getNodeType(sOLReceivingNode);
						if ("DC".equalsIgnoreCase(sOLNodeType) || "LC".equalsIgnoreCase(sOLNodeType)
								|| "LCEP".equalsIgnoreCase(sOLNodeType) || "LS".equalsIgnoreCase(sOLNodeType)
								|| "LSCA".equalsIgnoreCase(sOLNodeType) || "LSRP".equalsIgnoreCase(sOLNodeType)
								|| "LSEP".equalsIgnoreCase(sOLNodeType) || "LSSP".equalsIgnoreCase(sOLNodeType)
								|| "LSPP".equalsIgnoreCase(sOLNodeType) || "LSVO".equalsIgnoreCase(sOLNodeType)
								|| "NSC".equalsIgnoreCase(sOLNodeType) || "CT".equalsIgnoreCase(sOLNodeType)) {
							sDepartmentCode = "INVENTORY_MATERIAL";
						}
						// HUB-6323: [Begin]
						else if ("WLVS".equalsIgnoreCase(sOLNodeType)) {
							sDepartmentCode = "WIRELESS";
						}
						// HUB-6323: [End]

						else {
							// HUB-6096: [End]
							// sDepartmentCode = "CISCO_INT";
							sDepartmentCode = "OTHERS";
						}
					}
				}
			}
		}

		if (YFCObject.equals(sInterfaceNo, "INT_ODR_1") || YFCObject.equals(sInterfaceNo, "INT_ODR_2")
				|| (YFCObject.equals(sEntryType, "STERLING") && (YFCObject.equals(sOrderType, "MATERIAL_RESERVATION")
						|| YFCObject.equals(sOrderType, "TRANSPORT_ORDER")))) {
			if (YFCObject.isNull(sDepartmentCode)) {
				// HUB-8442 - [START]
				// Implementing B2B logic for the Orders coming from Sterling
				// Portal to stamp Department Code
				// Assumption is that the entry type when created from the SP
				// will be 'STERLING' and only MATERIAL_RESERVATION and
				// TRANSPORT_ORDER will be created
				sDepartmentCode = determineDepartmentCode(sEntryType, sOrderType, inxml);
				// HUB-8442 - [END]

				/*if (bGoNoDAC && YFCObject.isNull(sDepartmentCode)) {
					sDepartmentCode = "PMC";
				}*/
				if (YFCObject.equals(sDocumentType, "0006") && YFCObject.isNull(sDepartmentCode)) {

					if ((YFCObject.equals(sNodeType, "DC")) || (YFCObject.equals(sNodeType, "LS"))
							|| (YFCObject.equals(sNodeType, "LSCA")) || (YFCObject.equals(sNodeType, "LSRP"))
							|| (YFCObject.equals(sNodeType, "LSEP")) || (YFCObject.equals(sNodeType, "LSSP"))
							|| (YFCObject.equals(sNodeType, "LSPP")) || (YFCObject.equals(sNodeType, "LSVO"))
							|| (YFCObject.equals(sNodeType, "NSC")) || (YFCObject.equals(sNodeType, "LC"))
							|| (YFCObject.equals(sNodeType, "LCEP")) || (YFCObject.equals(sNodeType, "CT"))) {
						sDepartmentCode = "INVENTORY_MATERIAL";
					} else if (YFCObject.equals(sNodeType, "WLVS")) {
						sDepartmentCode = "WIRELESS";
					} else if (YFCObject.equals(sNodeType, "WBVS")) {
						sDepartmentCode = "WIDEBAND";
					} else {
						sDepartmentCode = "INVENTORY_MATERIAL";
					}
				} else if (YFCObject.isNull(sDepartmentCode)) {
					// [HUB-7003] Begin					
					sDepartmentCode = "INVENTORY_MATERIAL";
					// [HUB-7003] End
				}				
			}
			//HUB-9164 Begin
			String sProjectNo = inxml.getDocumentElement().getAttribute(TelstraConstants.SEARCH_CRITERIA_1);
			if (bGoNoDAC && !YFCCommon.isStringVoid(sProjectNo)) {
				sDepartmentCode = "PMC";
			}
			//HUB-9164 End
		}

		// HUB-8472 :Start
		appendPersonInfoShipToFromRecNodeAddress(inxml, sInterfaceNo);
		// HUB-8472 :End

		inxml.getDocumentElement().setAttribute(TelstraConstants.DEPARTMENT_CODE, sDepartmentCode);
		inxml.getDocumentElement().setAttribute(TelstraConstants.DOCUMENT_TYPE, sDocumentType);	
		/*
		 * Commenting out interfaceNo removal so that it could be use as identifier in before create order ue whether order is coming from source or not
		 */
		//		inxml.getDocumentElement().removeAttribute(TelstraConstants.INTERFACE_NO);

		if (YFCObject.equals(sDocumentType, "0005")) {
			/*
			 * HUB-8886: IP order stream determination
			 */
			if(TelstraConstants.INTEGRAL_PLUS.equalsIgnoreCase(sEntryType)){
				String sRecivingNode = XPathUtil.getXpathAttribute(inxml, "//OrderLine/@ReceivingNode");
				if(!YFCCommon.isStringVoid(sRecivingNode)){

					String sNode = getProperty("CPE_ORDER_STREAM_NODE");
					String[] nodeList = sNode.split(",");				
					for (int i = 0; i < nodeList.length; i++) {
						if(sRecivingNode.equals(nodeList[i].trim())){
							sDepartmentCode = "CPE";
							break;
						}					
					}
				}	
			}
			/*
			 * HUB-8886: END
			 */
			if (!YFCObject.equals(sSellerOrganizationCode, "TELSTRA")
					&& !YFCObject.equals(sSellerOrganizationCode, "NBN")) {
				String sShipNode = sSellerOrganizationCode + "_N1";
				for (YFCElement eleInputOrderLine : inxml.getElementsByTagName(TelstraConstants.ORDER_LINE)) {
					eleInputOrderLine.setAttribute(TelstraConstants.SHIP_NODE, sShipNode);

				}
			}
		}

		inxml.getDocumentElement().setAttribute(TelstraConstants.DEPARTMENT_CODE, sDepartmentCode);

		//HUB-9217[START]
		/* The below changes are done for Material reservation Orders only, where in if the recv node and ship Node 
		 * for any of the lines are same then remove the receiving node for all the lines and stamp the document 
		 * type as a sales order.*/

		if(YFCObject.equals(sOrderType, "MATERIAL_RESERVATION")){
			boolean bIsSameNode = false;
			for (YFCElement orderLine : orderLinesList) {
				String sRecvNode = orderLine.getAttribute(TelstraConstants.RECEIVING_NODE,"");
				String sShipNode = orderLine.getAttribute(TelstraConstants.SHIP_NODE,"");
				if(!YFCObject.isVoid(sShipNode) &&  !YFCObject.isVoid(sRecvNode) && YFCObject.equals(sShipNode, sRecvNode)){
					bIsSameNode = true;
					break;
				}
			}

			if(bIsSameNode){
				inxml.getDocumentElement().setAttribute(TelstraConstants.DOCUMENT_TYPE, TelstraConstants.DOCUMENT_TYPE_0001);
				for (YFCElement orderLine : orderLinesList) {
					orderLine.removeAttribute(TelstraConstants.RECEIVING_NODE);
				}
			}
		}
		//HUB-9217[END]

		YFCDocument doctempOrderListinXml = YFCDocument.getDocumentFor(
				"<Order EnterpriseCode='' DocumentType='' OrderHeaderKey=''><OrderLines><OrderLine Status='' OrderHeaderKey='' OrderLineKey='' SubLineNo='' PrimeLineNo=''><Item ItemID='' /></OrderLine></OrderLines></Order>");
		YFCDocument outxml = invokeYantraApi("createOrder", inxml, doctempOrderListinXml); // template
		checkforBackorderandFreightLine(outxml, inxml);

		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "CreateOrder", "");

		// HUB-8346 -[START]
		return outxml;
		// HUB-8346 -[END]

	}

	// HUB-8442 - [START]

	/**
	 * Only material reservations created for CTs should be created as transfer orders.
	 * Everything else, should be treated as sales orders.
	 * @param sExternalCustomerID
	 * @return
	 */
	private boolean isDacCT(String sExternalCustomerID) {
		
		boolean isDacCT = false;
		/*
		 * Assumption: CT will always start with "V"
		 */
		if(sExternalCustomerID.startsWith("V")){
			
			String sToMatchWithRegex = sExternalCustomerID.substring(1);
			/*
			 * Assumption: Except first character rest of it will be numeric for CT 
			 */
			Pattern ptn = Pattern.compile("\\d{2}$");
	        Matcher mtch = ptn.matcher(sToMatchWithRegex);
	        if(mtch.find()){
	        	isDacCT = true;
	        }
		}		
		return isDacCT;
	}


	/**
	 * Implementing B2B logic for the Orders coming from Sterling Portal to
	 * stamp Department Code If Reference Name = DELIVERY_INSTRUCTIONS and
	 * Reference value contains Alliance, Department Code is ISGM
	 * 
	 * @param sEntryType
	 * @param sOrderType
	 * @param sDepartmentCode
	 * @param inxml
	 * @return boolean
	 */
	private String determineDepartmentCode(String sEntryType, String sOrderType, YFCDocument inxml) {
		for (YFCElement eleOrderReference : inxml.getDocumentElement()
				.getElementsByTagName(TelstraConstants.REFERENCE)) {
			String strReferenceName = eleOrderReference.getAttribute(TelstraConstants.NAME, "");
			if (YFCObject.equals(strReferenceName, "DELIVERY_INSTRUCTIONS")) {
				String strReferenceValue = eleOrderReference.getAttribute(TelstraConstants.VALUE, "").toUpperCase();
				if (strReferenceValue.contains("ALLIANCE")) {
					logger.verbose("Setting the Department Code to ISGM as the reference value is Alliance");
					return "ISGM";
				}
			}
		}
		return "";
	}

	// HUB-8442 - [END]

	/**
	 * 
	 * @return
	 */
	private String getDateStamp() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		// get current date time with Date()
		Date date = new Date();
		return dateFormat.format(date);
	}

	/**
	 * This method is invoked for the new order and moves the backordered qty in
	 * the input to backordered status
	 * 
	 * @param eleTempOrderLine
	 * @param sDocumentType
	 * @param eleInputOrderLine
	 */

	private void backOrderLineforCreated(YFCElement eleTempOrderLine, String sDocumentType,
			YFCElement eleInputOrderLine) {

		double dBackorderedQty = eleInputOrderLine.getDoubleAttribute(TelstraConstants.BACKORDERED_QTY, 0.00);
		double dOrderedQty = eleInputOrderLine.getDoubleAttribute(TelstraConstants.ORDERED_QTY, 0.00);

		if (dBackorderedQty != 0.00) {

			if (dBackorderedQty > dOrderedQty) {
				throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.ORDER_INVALID_BACKORDERED_QTY_ERROR_CODE,
						new YFSException());
			}

			String sOrderHeaderKey = eleTempOrderLine.getAttribute(TelstraConstants.ORDER_HEADER_KEY, "");
			String sOrderLineKey = eleTempOrderLine.getAttribute(TelstraConstants.ORDER_LINE_KEY, "");
			String sTransactionId = "SCHEDULE." + sDocumentType;
			YFCDocument docchangeOrderinXml = YFCDocument
					.getDocumentFor("<Order OrderHeaderKey='" + sOrderHeaderKey + "' TransactionId='" + sTransactionId
							+ "' ><OrderLines><OrderLine BaseDropStatus='1300' Quantity='" + dBackorderedQty
							+ "' OrderLineKey='" + sOrderLineKey + "' /></OrderLines></Order>");

			invokeYantraApi("changeOrderStatus", docchangeOrderinXml);
		}
	}

	/**
	 * Checking for BackOrdered Lines
	 * 
	 * @param outxml
	 * @param inxml
	 */
	private void checkforBackorderandFreightLine(YFCDocument outxml, YFCDocument inxml) {
		String sOrderHeaderKey = outxml.getDocumentElement().getAttribute(TelstraConstants.ORDER_HEADER_KEY, "");
		String sDocumentType = outxml.getDocumentElement().getAttribute(TelstraConstants.DOCUMENT_TYPE, "");

		if (!YFCObject.isVoid(sOrderHeaderKey)) {
			YFCElement eleOrderOut = outxml.getElementsByTagName(TelstraConstants.ORDER).item(0);
			if (!YFCObject.isVoid(eleOrderOut)) {
				for (YFCElement eleInputOrderLine : inxml.getElementsByTagName(TelstraConstants.ORDER_LINE)) {
					String sPrimeLineNo = eleInputOrderLine.getAttribute(TelstraConstants.PRIME_LINE_NO, "");
					YFCElement eleTempOrderLine = null;
					for (YFCElement eleOrderListLine : eleOrderOut.getElementsByTagName(TelstraConstants.ORDER_LINE)) {
						String sOrderListPrimeLineNo = eleOrderListLine.getAttribute(TelstraConstants.PRIME_LINE_NO,
								"");
						if (sOrderListPrimeLineNo.equals(sPrimeLineNo)) {
							eleTempOrderLine = eleOrderListLine;
							break;
						}
					}
					if (!YFCObject.isNull(eleTempOrderLine)) {
						String sOrderLineStatus = eleInputOrderLine.getAttribute(TelstraConstants.CHANGE_STATUS, "");
						if (!YFCObject.isNull(sOrderLineStatus)) {
							String sBackorderstatus = getProperty("ChangeStatusBackorder", true); // "B"
							if (sOrderLineStatus.equalsIgnoreCase(sBackorderstatus)) {
								/*
								 * This method is invoked for the new order and
								 * moves the backordered qty in the input to
								 * backordered status
								 */
								backOrderLineforCreated(eleTempOrderLine, sDocumentType, eleInputOrderLine);
							}
						}

						// HUB-8358-[START]
						/*
						 * The below logic will move the freight charge line to
						 * Freight Line Closed which is an extended status of
						 * cancelled
						 */

						cancelFreightChargeLine(eleTempOrderLine, sDocumentType, sOrderHeaderKey);

						// HUB-8358-[END]

					}
				}
			}
		}
	}

	// HUB-8358-[START]

	/**
	 * The below logic will move the freight charge line to Freight Line Closed
	 * which is an extended status of cancelled This method is invoked for PO's
	 * while creating an Order as well as creating a new Line after the order
	 * has been created. Assuming SubLineNo to be always '1'.
	 * 
	 * @param eleTempOrderLine
	 */
	private void cancelFreightChargeLine(YFCElement eleTempOrderLine, String sDocumentType, String sOrderHeaderKey) {
		if (YFCObject.equals(sDocumentType, TelstraConstants.DOCUMENT_TYPE_0005)) {
			YFCElement eleOrderLineItem = eleTempOrderLine.getElementsByTagName(TelstraConstants.ITEM).item(0);
			String sItemID = eleOrderLineItem.getAttribute(TelstraConstants.ITEM_ID, "");

			/*
			 * Items starting with 988 are assumed to be Fright Charged Lines,
			 * below creating a document to cancel these lines to a status
			 * Freight Line Closed
			 */

			if (sItemID.startsWith("988")) {
				LoggerUtil.verboseLog("Freightchargeline :: Element", logger, eleTempOrderLine);

				YFCDocument docInputFreightLine = YFCDocument.getDocumentFor(
						"<OrderStatusChange><OrderLines><OrderLine></OrderLine></OrderLines></OrderStatusChange>");
				YFCElement eleOrderStatusChange = docInputFreightLine.getDocumentElement();

				eleOrderStatusChange.setAttribute(TelstraConstants.DOCUMENT_TYPE, sDocumentType);
				eleOrderStatusChange.setAttribute(TelstraConstants.TRANSACTION_ID, "GPS_FREIGHT_LINE.0005.ex");
				eleOrderStatusChange.setAttribute(TelstraConstants.ORDER_HEADER_KEY, sOrderHeaderKey);

				YFCElement eleOrderStatusLine = eleOrderStatusChange.getElementsByTagName(TelstraConstants.ORDER_LINE)
						.item(0);

				eleOrderStatusLine.setAttribute(TelstraConstants.PRIME_LINE_NO,
						eleTempOrderLine.getAttribute(TelstraConstants.PRIME_LINE_NO, ""));
				// Assuming SubLineNo to be always '1'
				eleOrderStatusLine.setAttribute(TelstraConstants.SUB_LINE_NO, "1");
				eleOrderStatusLine.setAttribute("ChangeForAllAvailableQty", TelstraConstants.YES);
				eleOrderStatusLine.setAttribute("BaseDropStatus", "9000.5000");

				invokeYantraApi("changeOrderStatus", docInputFreightLine);
			}
		}
	}

	// HUB-8358-[END]

	/**
	 * HUB-5644 This method stamp person info ship to from receiving node if it
	 * is missing
	 * 
	 * @param yfcInDoc
	 * @param sInterfaceNo
	 * @param sBillToID
	 * @param sBillToKey
	 */

	private void appendPersonInfoShipToFromRecNodeAddress(YFCDocument yfcInDoc, String sInterfaceNo) {

		LoggerUtil.verboseLog("appendPersonInfoShipToFromRecNodeAddress :: InterfaceNo", logger, sInterfaceNo);
		String sInterfaceForPersonInfoBill = getProperty("InterfacesForPersonInfoBill");
		String[] serviceInterfaceNoList = sInterfaceForPersonInfoBill.split(",");
		/*
		 * Looping through the Interface's from arguments and stamping the
		 * address of the receiving node in the PersonInfoBillTo
		 */
		for (String serviceInterfaceNo : serviceInterfaceNoList) {

			if (!YFCObject.equals(sInterfaceNo, serviceInterfaceNo)) {
				continue;// continue if incoming interface is not configured
			}

			Map<String, Map<String, String>> mapNodeAddress = new HashMap<>();
			YFCNodeList<YFCElement> yfcNlOrderLine = yfcInDoc.getElementsByTagName(TelstraConstants.ORDER_LINE);
			for (YFCElement yfcEleOrderLine : yfcNlOrderLine) {

				String sShipToId = yfcEleOrderLine.getAttribute(TelstraConstants.SHIP_TO_ID);
				if (YFCCommon.isStringVoid(sShipToId)) {
					YFCElement elePersonInfoShipTo = yfcEleOrderLine
							.getChildElement(TelstraConstants.PERSON_INFO_SHIP_TO);
					if (YFCCommon.isVoid(elePersonInfoShipTo)) {
						elePersonInfoShipTo = yfcInDoc.createElement(TelstraConstants.PERSON_INFO_SHIP_TO);
						yfcEleOrderLine.appendChild(elePersonInfoShipTo);
					}
					Map<String, String> personInfoShipToAttrs = elePersonInfoShipTo.getAttributes();

					if (personInfoShipToAttrs.size() == 0) {
						String sReceivingNode = yfcEleOrderLine.getAttribute(TelstraConstants.RECEIVING_NODE, "");

						if (YFCObject.isNull(sReceivingNode)) {
							throw ExceptionUtil.getYFSException(
									TelstraErrorCodeConstants.CREATE_ORDER_BLANK_RECEIVING_NODE_ERROR_CODE,
									new YFSException());
						}

						if (!mapNodeAddress.containsKey(sReceivingNode)) {
							personInfoShipToAttrs = getNodeAddress(sReceivingNode);
							mapNodeAddress.put(sReceivingNode, personInfoShipToAttrs);
						} else {
							personInfoShipToAttrs = mapNodeAddress.get(sReceivingNode);
						}
						elePersonInfoShipTo.setAttributes(personInfoShipToAttrs);
					}
				}
			}
			break;
		}
	}

	/**
	 * This methods return the node address by calling getShipNodeList api
	 * 
	 * @param sNode
	 * @return
	 */
	private Map<String, String> getNodeAddress(String sNode) {

		Map<String, String> mapShipNodePersonInfo = new HashMap<>();
		YFCDocument docNodeListinXml = YFCDocument.getDocumentFor("<Shipment ShipNode='" + sNode + "' />");
		LoggerUtil.verboseLog("get ship node ip xml", logger, docNodeListinXml);

		YFCDocument doctempgetNodeListinXml = YFCDocument.getDocumentFor(
				"<ShipNodeList><ShipNode ShipNode=''><ShipNodePersonInfo FirstName='' LastName='' AddressLine1='' AddressLine2='' AddressLine3=''  AddressLine4='' City='' ZipCode='' State='' DayPhone='' EMailID='' DayFaxNo='' Country=''/></ShipNode></ShipNodeList>");
		YFCDocument outXml = invokeYantraApi("getShipNodeList", docNodeListinXml, doctempgetNodeListinXml);
		LoggerUtil.verboseLog("get ship node out xml", logger, outXml);

		YFCElement eleShipNode = outXml.getDocumentElement().getChildElement("ShipNode");
		if (!YFCCommon.isVoid(eleShipNode)) {
			YFCElement eleShipNodePersonInfo = eleShipNode.getElementsByTagName("ShipNodePersonInfo").item(0);
			if (!YFCCommon.isVoid(eleShipNodePersonInfo)) {
				mapShipNodePersonInfo = eleShipNodePersonInfo.getAttributes();
			}
		}
		return mapShipNodePersonInfo;
	}

	// End of method
}

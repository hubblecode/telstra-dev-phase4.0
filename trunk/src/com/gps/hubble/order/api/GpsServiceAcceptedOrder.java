/***********************************************************************************************
 * File Name        : GpsServiceAcceptedOrder.java
 *
 * Description      : Custom API for processing service acceptance from WMS
 * 
 * Modification Log :
 * ---------------------------------------------------------------------------------------------
 * Ver #	Date			Author				Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0		8 Jun, 2017		Prateek Kumar		Initial Version
 * ---------------------------------------------------------------------------------------------
 * Copyright (c) IBM Australia Limited 2017. All rights reserved.
 **********************************************************************************************/

package com.gps.hubble.order.api;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.constants.TelstraErrorCodeConstants;
import com.yantra.yfc.date.YDate;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSException;

/**
 * Custom API for WMS status update for acceptance of an order
 * 
 * @author Prateek
 *
 *         <Shipment ServiceName="Service Accepted" OrderName="Vector Id"
 *         > <ShipmentLines> <ShipmentLine PrimeLineNo=
 *         "1(use the vector id to identify the line number)" SubLineNo="1"
 *         ItemID="ignore the item id passed here" UnitOfMeasure=
 *         "ignore the uom passed here" Quantity=
 *         "ignore the qty as partial acceptance does not happen"
 *         > </ShipmentLine> </ShipmentLines> </Shipment>
 */
public class GpsServiceAcceptedOrder extends AbstractCustomApi {

	private static YFCLogCategory logger = YFCLogCategory.instance(GpsServiceAcceptedOrder.class);

	@Override
	public YFCDocument invoke(YFCDocument yfcInDoc) throws YFSException {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", yfcInDoc);

		YFCElement yfcEleInRoot = yfcInDoc.getDocumentElement();

		String sVectorID = yfcEleInRoot.getAttribute(TelstraConstants.ORDER_NAME);
		YFCDocument yfcDocGetOrderLineListOp = callGetOrderLineList(sVectorID);

		int iOrderLineLength = yfcDocGetOrderLineListOp.getElementsByTagName(TelstraConstants.ORDER_LINE)
				.getLength();
		if (iOrderLineLength == 0) {
			throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.ACCEPT_ORDER_NO_ORDER_LINE_FOR_VECTOR_ID,
					new YFSException());
		} else if (iOrderLineLength > 1) {
			throw ExceptionUtil.getYFSException(
					TelstraErrorCodeConstants.ACCEPT_ORDER_MULTIPLE_ORDER_LINES_WITH_SAME_VECTOR_ID,
					new YFSException());
		}
		
		String sDocumentType = XPathUtil.getXpathAttribute(yfcDocGetOrderLineListOp, "//Order/@DocumentType");

		if(TelstraConstants.DOCUMENT_TYPE_RETURN_ORDER.equals(sDocumentType)){
			acceptReturnOrder(yfcDocGetOrderLineListOp);
		}
		else{
			acceptSalesOrder(yfcDocGetOrderLineListOp, sVectorID);
		}

		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", yfcInDoc);
		return yfcInDoc;
	}

	/**
	 * 
	 * @param yfcDocGetOrderLineListOp
	 */
	private void acceptReturnOrder(YFCDocument yfcDocGetOrderLineListOp) {

		YFCElement yfcEleOrderStatus = XPathUtil.getXPathElement(yfcDocGetOrderLineListOp,
				"//OrderStatus[@Status='1100' or @Status='1100.5000']");

		if (YFCCommon.isVoid(yfcEleOrderStatus)) {
			YFCElement yfcEleOrderLine = yfcDocGetOrderLineListOp.getElementsByTagName(TelstraConstants.ORDER_LINE)
					.item(0);
			logger.verbose("GpsServiceAcceptedOrder :: Line passed is either service accepted/shipped/rejected"
					+ yfcEleOrderLine);
			return;
		}
		String sServiceAcceptedTransactionID = "SERVICE_ACCEPTED_UPDATE.0003.ex";// SERVICE_ACCEPTED_UPDATE.0001.ex
		moveLineToServiceAcceptedStatus(yfcDocGetOrderLineListOp, sServiceAcceptedTransactionID);
	}

	/**
	 * 
	 * @param yfcDocGetOrderLineListOp
	 * @param sVectorID
	 */
	private void acceptSalesOrder(YFCDocument yfcDocGetOrderLineListOp, String sVectorID) {

		YFCElement yfcEleOrderStatus = XPathUtil.getXPathElement(yfcDocGetOrderLineListOp,
				"//OrderStatus[@Status='3200']");

		if (YFCCommon.isVoid(yfcEleOrderStatus)) {
			YFCElement yfcEleOrderLine = yfcDocGetOrderLineListOp.getElementsByTagName(TelstraConstants.ORDER_LINE)
					.item(0);
			double dMinLineStatus = yfcEleOrderLine.getDoubleAttribute(TelstraConstants.MIN_LINE_STATUS);
			if (dMinLineStatus > 3200) {
				logger.verbose("GpsServiceAcceptedOrder :: Line passed is either service accepted/shipped/rejected"
						+ yfcEleOrderLine);
				return;
			}

			scheduleAndReleaseLine(yfcEleOrderLine);
			yfcDocGetOrderLineListOp = callGetOrderLineList(sVectorID);
			yfcEleOrderStatus = XPathUtil.getXPathElement(yfcDocGetOrderLineListOp,
					"//OrderStatus[@Status='3200']");
		}

		if (!YFCCommon.isVoid(yfcEleOrderStatus)) {
			String sServiceAcceptedTransactionID = getProperty("SERVICE_ACCEPTED_TRAN_ID", true);// SERVICE_ACCEPTED_UPDATE.0001.ex
			moveLineToServiceAcceptedStatus(yfcDocGetOrderLineListOp, sServiceAcceptedTransactionID);
		}
	}

	/**
	 * This method will move line from released to service accepted status
	 * 
	 * @param yfcDocGetOrderLineListOp
	 */
	private void moveLineToServiceAcceptedStatus(YFCDocument yfcDocGetOrderLineListOp, String sServiceAcceptedTransactionID) {

		YFCElement yfcEleOrderLine = yfcDocGetOrderLineListOp.getElementsByTagName(TelstraConstants.ORDER_LINE)
				.item(0);
		String sOrderLineKey = yfcEleOrderLine.getAttribute(TelstraConstants.ORDER_LINE_KEY);
		String sOrderHeaderKey = yfcEleOrderLine.getChildElement(TelstraConstants.ORDER)
				.getAttribute(TelstraConstants.ORDER_HEADER_KEY);

		String sServiceAcceptedDropStatus = getProperty("SERVICE_ACCEPTED_DROP_STATUS", true);// 3200.5000

		YFCDocument yfcDocChangeOrderStatusIp = YFCDocument.getDocumentFor("<OrderStatus TransactionId='"
				+ sServiceAcceptedTransactionID + "' OrderHeaderKey='" + sOrderHeaderKey
				+ "'> <OrderLines> <OrderLine BaseDropStatus='" + sServiceAcceptedDropStatus + "' OrderLineKey='"
				+ sOrderLineKey + "' ChangeForAllAvailableQty='Y'/> </OrderLines> </OrderStatus>");

		invokeYantraApi(TelstraConstants.API_CHANGE_ORDER_STATUS, yfcDocChangeOrderStatusIp);
	}

	/**
	 * This method will schedule and release the line if it is not yet released
	 * 
	 * @param yfcEleOrderLine
	 */

	private void scheduleAndReleaseLine(YFCElement yfcEleOrderLine) {

		String sOrderLineKey = yfcEleOrderLine.getAttribute(TelstraConstants.ORDER_LINE_KEY);
		String sOrderedQty = yfcEleOrderLine.getAttribute(TelstraConstants.ORDERED_QTY);
		String sShipNode = yfcEleOrderLine.getAttribute(TelstraConstants.SHIP_NODE);
		String sOrderHeaderKey = yfcEleOrderLine.getChildElement(TelstraConstants.ORDER)
				.getAttribute(TelstraConstants.ORDER_HEADER_KEY);

		/*
		 * Get the deliveryDate as the current date to pass it in the
		 * scheduelOrderLines API
		 */
		YDate yDDeliveryDate = new YDate(false);
		YFCDocument yfcDocScheduleOrderLinesIp = YFCDocument
				.getDocumentFor("<Promise OrderHeaderKey='" + sOrderHeaderKey
						+ "' IgnoreReleaseDate='Y' ScheduleAndRelease='Y'> <PromiseLines> <PromiseLine OrderLineKey='"
						+ sOrderLineKey + "' Quantity='" + sOrderedQty + "' ShipNode='" + sShipNode
						+ "'/> </PromiseLines></Promise>");

		yfcDocScheduleOrderLinesIp.getElementsByTagName(TelstraConstants.PROMISE_LINE).item(0)
		.setDateAttribute(TelstraConstants.DELIVERY_DATE, yDDeliveryDate);

		logger.verbose("GpsServiceAcceptedOrder::scheduleAndReleaseLine:: yfcDocScheduleOrderLinesIp\n"
				+ yfcDocScheduleOrderLinesIp);
		invokeYantraApi(TelstraConstants.API_SCHEDULE_ORDER_LINES, yfcDocScheduleOrderLinesIp);

	}

	/**
	 * 
	 * @param sVectorID
	 * @return
	 */
	private YFCDocument callGetOrderLineList(String sVectorID) {

		YFCDocument inDocgetOrdLineList = YFCDocument.getDocumentFor(
				"<OrderLine><Order EntryType='VECTOR' /><Extn VectorID='" + sVectorID + "' /></OrderLine>");

		YFCDocument templateForGetOrdLineList = YFCDocument.getDocumentFor("<OrderLineList TotalLineList=''>"
				+ "<OrderLine PrimeLineNo='' OrderLineKey='' OrderedQty='' MinLineStatus='' ShipNode=''><Order OrderName='' OrderHeaderKey='' DocumentType=''/> <OrderStatuses> "
				+ "<OrderStatus OrderReleaseKey='' Status=''/> </OrderStatuses></OrderLine> </OrderLineList>");

		YFCDocument yfcDocGetOrderLineListOp = invokeYantraApi("getOrderLineList", inDocgetOrdLineList,
				templateForGetOrdLineList);

		logger.verbose("GpsServiceAcceptedOrder::callGetOrderLineList:: yfcDocGetOrderLineListOp\n"
				+ yfcDocGetOrderLineListOp);

		return yfcDocGetOrderLineListOp;
	}

}

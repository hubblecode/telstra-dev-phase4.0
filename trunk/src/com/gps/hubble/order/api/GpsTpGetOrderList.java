/***********************************************************************************************
 * File Name : ManageOrder.java
 *
 * Description : This class is called from General as a service. It mainly Creates or Updates Order
 * related Information. It changes Shipment, Order and OrderLine Statuses. It Validates BackOrder
 * Cancel and Deliver.
 * 
 * Modification Log :
 * --------------------------------------------------------------------------------------------- Ver
 * # Date Author Modification
 * --------------------------------------------------------------------------------------------- 1.0
 * Jul 10,2016 Keerthi Yadav Initial Version
 * ---------------------------------------------------------------------------------------------
 * 
 */

package com.gps.hubble.order.api;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;

/**
 * Custom API to get list of Orders in Telstra Portal
 * 
 * @author Jyothi Garg
 * @version 1.0
 * 
 *          Extends AbstractCustomApi Class
 */

public class GpsTpGetOrderList extends AbstractCustomApi {
	private static YFCLogCategory logger = YFCLogCategory.instance(GpsTpGetOrderList.class);

	@Override
	public YFCDocument invoke(YFCDocument inXml) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(),"invoke", inXml);
		String loginId = inXml.getDocumentElement().getAttribute(TelstraConstants.LOGIN_ID, "");

		if (!YFCCommon.isVoid(loginId)) {
			String teamId = getTeamId(loginId);
			if (!YFCCommon.isVoid(teamId)) {
				YFCDocument getTeamListOutput = getTeamDetailsMethod(teamId);
				if (!YFCCommon.isVoid(getTeamListOutput)) {
					YFCElement teamDocEle = getTeamListOutput.getDocumentElement();
					YFCElement teamEle = teamDocEle.getChildElement("Team");
					String shipNodeAccess = teamEle.getAttribute("ShipNodeAccessMode");
					String docTypeAccess = teamEle.getAttribute("DocumentTypeAccessMode");
					List<String> docList = null;
					if (!YFCCommon.isVoid(docTypeAccess)) {
						if ("02".equalsIgnoreCase(docTypeAccess)) {
							docList = getTeamDocumentTypeList(teamEle);
							inXml = appendOrderLevelComplexQuery(inXml, docList);
						}
					}
					if (!YFCCommon.isVoid(shipNodeAccess)) {
						List<String> orgList = new ArrayList<>();
						if ("02".equalsIgnoreCase(shipNodeAccess)) {
							orgList.add(inXml.getDocumentElement().getAttribute(TelstraConstants.ORGANIZATION_CODE,""));
							inXml = appendOrderLineLevelComplexQuery(inXml, orgList);
						} else if ("03".equalsIgnoreCase(shipNodeAccess)) {
							YFCElement teamNodeList = teamEle.getChildElement("TeamNodesList");
							YFCIterable<YFCElement> teamNdList = teamNodeList.getChildren("TeamNodes");
							for (YFCElement teamNode : teamNdList) {
								orgList.add(teamNode.getAttribute("ShipnodeKey"));
							}
							inXml = appendOrderLineLevelComplexQuery(inXml, orgList);
						} 
					}
				}
			}
		}
		LoggerUtil.verboseLog("getOrderList Input", logger, inXml);

		Document doc = inXml.getDocument();
		doc.renameNode(doc.getDocumentElement(), doc.getNamespaceURI(), TelstraConstants.ORDER);
		YFCDocument inputDoc = YFCDocument.getDocumentFor(doc);

		YFCDocument getOrderListTemplate = getOutputTemplate();
		YFCDocument getOrderListOutput = invokeYantraApi("getOrderList",inputDoc, getOrderListTemplate);
		LoggerUtil.verboseLog("getOrderList Output", logger, getOrderListOutput);
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke",getOrderListOutput);
		return getOrderListOutput;
	}

	private List<String> getTeamDocumentTypeList(YFCElement teamEle) {
		List<String> docList = new ArrayList<>();
		YFCElement docTypeList = teamEle.getChildElement("TeamDocTypeList");
		YFCIterable<YFCElement> docTyList = docTypeList.getChildren("TeamDocType");
		for (YFCElement docType : docTyList) {
			String documentType = docType.getAttribute(TelstraConstants.DOCUMENT_TYPE);
			if (!docList.contains(documentType)) {
				docList.add(documentType);
			}
		}
		return docList;
	}

	private YFCDocument getTeamDetailsMethod(String teamId) {
		YFCDocument getTeamListInput = YFCDocument.getDocumentFor("<Team TeamId='' />");
		getTeamListInput.getDocumentElement().setAttribute(TelstraConstants.TEAM_ID, teamId);
		return invokeYantraApi(TelstraConstants.API_GET_TEAM_LIST , getTeamListInput);
	}

	private String getTeamId(String loginId) {
		YFCDocument getUserListInput = YFCDocument.getDocumentFor("<User Loginid='' />");
		getUserListInput.getDocumentElement().setAttribute(TelstraConstants.LOGIN_ID , loginId);

		YFCDocument getUserListTemplate = YFCDocument.getDocumentFor("<UserList> <User DataSecurityGroupId='' Loginid='' OrganizationKey=''/> </UserList>");
		YFCDocument getUserListOutput = invokeYantraApi(TelstraConstants.API_GET_USER_LIST ,getUserListInput, getUserListTemplate);
		if (getUserListOutput != null) {
			return XPathUtil.getXpathAttribute(getUserListOutput,"//User/@DataSecurityGroupId");
		}
		return null;
	}

	private YFCDocument appendOrderLineLevelComplexQuery(YFCDocument inXml,List<String> orgNameList) {
		YFCElement orderLineEle = inXml.getDocumentElement().getChildElement(TelstraConstants.ORDER_LINE,true);
		YFCElement complexQryDocEle = orderLineEle.createChild(TelstraConstants.COMPLEX_QUERY);
		complexQryDocEle.setAttribute(TelstraConstants.OPERATOR, TelstraConstants.AND);
		YFCElement orEle = complexQryDocEle.createChild(TelstraConstants.OR);

		if (!YFCCommon.isVoid(orgNameList)) {
			for (String orgName : orgNameList) {
				if (!YFCCommon.isVoid(orgName)) {
					YFCElement expShipNodeEle = orEle.createChild(TelstraConstants.EXP);
					expShipNodeEle.setAttribute(TelstraConstants.NAME, TelstraConstants.SHIP_NODE);
					expShipNodeEle.setAttribute(TelstraConstants.QRY_TYPE, "EQ");
					expShipNodeEle.setAttribute(TelstraConstants.VALUE, orgName);

					YFCElement expReceivingNodeEle = orEle.createChild(TelstraConstants.EXP);
					expReceivingNodeEle.setAttribute(TelstraConstants.NAME, TelstraConstants.RECEIVING_NODE);
					expReceivingNodeEle.setAttribute(TelstraConstants.QRY_TYPE, "EQ");
					expReceivingNodeEle.setAttribute(TelstraConstants.VALUE , orgName);
				}
			}
		}
		return inXml;
	}
	
	private YFCDocument appendOrderLevelComplexQuery(YFCDocument inXml, List<String> docList) {
		YFCElement orderEle = inXml.getDocumentElement();
		YFCElement complexQryDocEle = orderEle.createChild(TelstraConstants.COMPLEX_QUERY);
		complexQryDocEle.setAttribute(TelstraConstants.OPERATOR, TelstraConstants.AND);
		YFCElement orEle = complexQryDocEle.createChild(TelstraConstants.OR);
		
		if (!YFCCommon.isVoid(docList)) {
			for (String docType : docList) {
				if (!YFCCommon.isVoid(docType)) {
					YFCElement expDocTypeEle = orEle.createChild(TelstraConstants.EXP);
					expDocTypeEle.setAttribute(TelstraConstants.NAME, TelstraConstants.DOCUMENT_TYPE);
					expDocTypeEle.setAttribute(TelstraConstants.QRY_TYPE, "EQ");
					expDocTypeEle.setAttribute(TelstraConstants.VALUE, docType);
				}
			}
		}
		return inXml;
	}
	
	private YFCDocument getOutputTemplate() {
		String templateId = getProperty(TelstraConstants.TEMPLATE_ID);
		String apiName = getProperty(TelstraConstants.API_NAME);
		YFCDocument getApiTemplateInput = YFCDocument.getDocumentFor("<ApiTemplate OrganizationCode='DEFAULT' TemplateType='00' ApiName='' TemplateId='' />");
		getApiTemplateInput.getDocumentElement().setAttribute(TelstraConstants.TEMPLATE_ID , templateId);
		getApiTemplateInput.getDocumentElement().setAttribute(TelstraConstants.API_NAME , apiName);
		YFCDocument getApiTemplateOutput = invokeYantraApi("getApiTemplateDetails", getApiTemplateInput);
		if(!YFCCommon.isVoid(getApiTemplateOutput)){
			YFCElement templateDataEle = getApiTemplateOutput.getDocumentElement().getChildElement("TemplateData");
			return YFCDocument.getDocumentFor(templateDataEle.getFirstChildElement().toString());
		}
		return null;
	}

}

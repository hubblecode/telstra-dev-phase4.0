/***********************************************************************************************
 * File	Name		: GpsResolveDuplicateOrderAlert.java
 *
 * Description		: This class is called from change order on hold type status change event 
 * 					The purpose of this class is to resolve the duplicate order alert if the duplicate order alert has been closed
 * 
 * Modification	Log	:
 * -------------------------------------------------------------------------------------------------------------------------------------
 * Ver #	Date			Author					Modification
 * 
 * 
 * -------------------------------------------------------------------------------------------------------------------------------------
 * 1.0									 		   	Initial	Version 
 * 1.1		May 02,2017		Prateek Kumar			HUB-8865: Changed exception type value from "GPS_Duplicate_Order_Alert" to "GPS_DUPLICATE_ORDER_ALERT" while calling get exception list api
 * -------------------------------------------------------------------------------------------------------------------------------------
 * 
 */
package com.gps.hubble.order.api;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSException;

/**
 * 
 * @author Prateek
 *
 */
public class GpsResolveDuplicateOrderAlert extends AbstractCustomApi{

	private static YFCLogCategory logger = YFCLogCategory.instance(GpsResolveDuplicateOrderAlert.class);

	
	@Override
	public YFCDocument invoke(YFCDocument inDoc) throws YFSException {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", inDoc);
		LoggerUtil.verboseLog("GpsResolveDuplicateOrderAlert :: Ip Doc", logger, inDoc);
		String sHoldStatus = XPathUtil.getXpathAttribute(inDoc, "//OrderHoldType[@HoldType='GPS_DUP_ORD_HOLD']/@Status");
				
		if(TelstraConstants.STATUS_HOLD_RESOLVED.equalsIgnoreCase(sHoldStatus)){
			String sOrderHeaderKey = XPathUtil.getXpathAttribute(inDoc, "//OrderHoldType/@OrderHeaderKey");
			
			YFCDocument docGetExceptionListIp = YFCDocument.getDocumentFor("<Inbox OrderHeaderKey='"+sOrderHeaderKey+"' ExceptionType='GPS_DUPLICATE_ORDER_ALERT' Status='OPEN'/>");
			YFCDocument docGetExceptionListTemp = YFCDocument.getDocumentFor("<InboxList> <Inbox ExceptionType=''  Status='' OrderNo='' OrderHeaderKey='' InboxKey=''/> </InboxList>");
			LoggerUtil.verboseLog("GpsResolveDuplicateOrderAlert :: docGetExceptionListIp", logger, docGetExceptionListIp);

			YFCDocument docGetExceptionListOp = invokeYantraApi(TelstraConstants.API_GET_EXCEPTION_LIST, docGetExceptionListIp, docGetExceptionListTemp);
			LoggerUtil.verboseLog("GpsResolveDuplicateOrderAlert :: docGetExceptionListOp", logger, docGetExceptionListOp);

			String sInboxKey = XPathUtil.getXpathAttribute(docGetExceptionListOp, "//Inbox/@InboxKey");
			if(!YFCCommon.isStringVoid(sInboxKey)){
				
				YFCDocument docResolveExceptionIp = YFCDocument.getDocumentFor("<ResolutionDetails> <Inbox InboxKey='"+sInboxKey+"' /> </ResolutionDetails>");
				LoggerUtil.verboseLog("GpsResolveDuplicateOrderAlert :: docResolveExceptionIp", logger, docResolveExceptionIp);
				invokeYantraApi(TelstraConstants.API_RESOLVE_EXCEPTION, docResolveExceptionIp);
			}				
		}
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", inDoc);
		return inDoc;
	}

}

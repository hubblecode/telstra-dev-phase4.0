/***********************************************************************************************
 * File Name        : PickOrder.java
 *
 * Description      : Custom API for WMS status update for Pick (Integral Orders Only)
 * 
 * Modification Log :
 * ---------------------------------------------------------------------------------------------
 * Ver #    Date            Author                              Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0                      Karthikeyan Suruliappan             Initial Version
 * 1.1      Jan 12,2017     Santosh Nagaraj                     Modified for Pick Order issue
 * 1.2		Jun 01,2017		Keerthi Yadav						HUB-9218: Remove "Order is on hold or Hold applied after scheduleOrder()" exception.
 * 1.3      Jun 01,2017     Keerthi Yadav						HUB-9210: Inappropriate Error Message thrown when a pick confirmation message comes for an Order that has already been picked.
 * 1.4 		Jun 02,2017		Keerthi Yadav						HUB-9261: Over Picking does not working when the line is partially picked.
 * 1.5		Jun 27,2017		Keerthi Yadav						HUB-9415: Adding Reason Text while calling Adjust Inventory for Audit
 * 1.6		Aug 08,2017		Prateek Kumar						HUB-9472: consuming the message if the passed order is Vector recovery
 * ---------------------------------------------------------------------------------------------
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
 **********************************************************************************************/
package com.gps.hubble.order.api;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.constants.TelstraErrorCodeConstants;
import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.date.YDate;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSException;

public class PickOrder extends AbstractCustomApi {
	private static YFCLogCategory logger = YFCLogCategory.instance(PickOrder.class);
	private Set<String> ordLineRKSet = null;
	private HashMap<String, ArrayList<YFCElement>> releaseKeyMap = null;
	private ArrayList<String> resultList = null;
	private HashMap<String, Double> inputSumMap = null;
	private String orderHeaderKey = "";
	private HashMap <String, YFCDocument> shipMap = null;
	private YFCDocument shipmentLineListDoc = null;
	private String shipmentKey = "";
	private int rkl_size = 0;
	private HashMap <String, HashMap<String, String>> releaseShipToKeyMap = null;
	private boolean soFlag=true;
	private String baseDropStatus="";
	private String tranID="";
	private YFCDocument processPickXml = null;
	private YFCDocument createShipmentXml = null;
	private boolean noLinesToPick = false;
	private boolean scheduleOrderLinesCalled = false;
	private String sourceSystem = null;

	public YFCDocument invoke(YFCDocument inXml) throws YFSException {
		createShipmentXml = YFCDocument.getDocumentFor(inXml.getDocumentElement().toString());
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", inXml);
		YFCElement inEle = inXml.getDocumentElement();
		validateInputXml(inEle);
		/*
		 * Return the input xml without doing anything if the number of orderlines are 0.
		 */
		if(noLinesToPick) {
			return inXml;
		}
		YFCDocument outputGetOrdListForComQry = getOrderList(inEle);
		YFCElement orderEle = outputGetOrdListForComQry.getElementsByTagName("Order").item(0);

		//HUB-9210[START]

		/*The validation is done to ensure all the lines are in eligible status i.e less than picked status, 
		 * even if one of the min order status of the lines are in higher or equal to pick status then we throw an excpetion*/
		boolean bvalidationfailed = validateOrderStatus(outputGetOrdListForComQry,inEle);
		if(bvalidationfailed){
			//throw error 
			LoggerUtil.verboseLog("One or More Order Lines passed have already been picked", logger, " throwing exception");
			throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.ORDER_HAS_ALREADY_BEEN_PICKED, new YFSException());
		}

		//HUB-9210[END]

		if(YFCObject.isVoid(orderEle)) {
			/*
			 * HUB-9472: consume the message if the passed order is Vector recovery
			 */
			boolean bIsOrderVectorRecovery = checkForVectorRecoveryOrder(inEle);
			if(bIsOrderVectorRecovery){
				return inXml;
			}
			throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.PICK_ORD_NOT_AVAILBALE, new YFSException());
		}
		if(YFCObject.equals(orderEle.getAttribute("SellerOrganizationCode"), "NBN") && 
				YFCObject.equals(orderEle.getAttribute("OrderType"), "TRANSPORT_ORDER") && 
				YFCObject.equals(orderEle.getAttribute("DocumentType"), TelstraConstants.DOCUMENT_TYPE_0005)) {
			YFCDocument outXml = createASNForNBNOrder(inXml);
			return outXml;
		}

		uniqueOrderValidation(outputGetOrdListForComQry, inEle);
		sourceSystem = outputGetOrdListForComQry.getElementsByTagName("Order").item(0).getAttribute("EntryType");
		
		//HUB-9415[START]
		String sOrderName = outputGetOrdListForComQry.getElementsByTagName("Order").item(0).getAttribute("OrderName");
		//HUB-9415[END]

		
		/* 
		 * If the changeShipNodeForOrderLine property is enabled for the service, then call the updateShipNodeForOrderLine method to change the ship node for the orderline
		 * to update the ShipNode information for the order line with the incoming ShipNode value in the pick confirmation message
		 */
		String changeShipNodeForOrderLine = getProperty("ChangeShipNodeForOrderLine");
		if(YFCObject.equals(changeShipNodeForOrderLine, "Y")) {
			YFCDocument changeOrderInput = updateShipNodeForOrderLine(outputGetOrdListForComQry, inEle);
			/*
			 * If the changeOrderInput returned is not void, then call the changeOrder API
			 */
			if(!YFCObject.isVoid(changeOrderInput)) {
				invokeYantraApi("changeOrder", changeOrderInput);
			}
		}

		/*
		 * call processPickMessage method to pick the orderLines in the processPickXml
		 */
		YFCDocument outXml = processPickMessage(outputGetOrdListForComQry, processPickXml);
		YFCNodeList<YFCElement> shipments = outXml.getElementsByTagName("Shipment");
		for(YFCElement shipment : shipments) {
			if(!YFCObject.isVoid(outXml) && outXml.getDocumentElement().hasChildNodes()) {
				if(YFCObject.equals(sourceSystem, TelstraConstants.INTEGRAL_PLUS)) {
					adjustInventory(shipment,sOrderName);
				}
			}
		}
		return outXml;
	}

	//HUB-9210[START]

	private boolean checkForVectorRecoveryOrder(YFCElement inEle) {

		boolean bIsOrderVectorRecovery = false;
		
		YFCNodeList<YFCElement> yfcNlOrderLine = inEle.getElementsByTagName(TelstraConstants.ORDER_LINE);
		/*
		 * Assumption: Vector order will have only 1 order line in the input
		 */
		if(yfcNlOrderLine.getLength()==1){

			String sOrderName = inEle.getAttribute(TelstraConstants.ORDER_NAME);

			YFCDocument yfcDocgetOrdLineListIp = YFCDocument.getDocumentFor(
					"<OrderLine><Order EntryType='VECTOR' DocumentType='0003'/><Extn VectorID='" + sOrderName + "' /></OrderLine>");
			YFCDocument templateForGetOrdLineList = YFCDocument.getDocumentFor("<OrderLineList TotalLineList=''> <OrderLine PrimeLineNo=''/> </OrderLineList>");
			logger.verbose("RejectOrder::GpsServicePackageAssembled:: yfcDocgetOrdLineListIp\n" + yfcDocgetOrdLineListIp);

			YFCDocument yfcDocGetOrderLineListOp = invokeYantraApi("getOrderLineList", yfcDocgetOrdLineListIp,
					templateForGetOrdLineList);

			logger.verbose("RejectOrder::GpsServicePackageAssembled:: yfcDocGetOrderLineListOp\n" + yfcDocGetOrderLineListOp);

			double dOrderLineLength = yfcDocGetOrderLineListOp.getDocumentElement().getDoubleAttribute(TelstraConstants.TOTAL_LINE_LIST, 0.0);

			if(dOrderLineLength>0){
				bIsOrderVectorRecovery = true;
			}
		}
		return bIsOrderVectorRecovery;
	}

	/**
	 *The method below validates to ensure all the lines are in eligible status i.e less than picked status, 
	 *even if one of the min order status of the lines are in higher or equal to Order pick status(3350.10000) then we throw an exception
	 * @param orderListOpEle
	 * @param inEle 
	 * @return bvalidationfailed
	 */

	private boolean validateOrderStatus(YFCDocument outputGetOrdListForComQry, YFCElement inEle) {

		boolean bvalidationfailed = false;

		for (YFCElement eleInputOrderLine : inEle.getElementsByTagName(TelstraConstants.ORDER_LINE)) {
			
			String sPrimeLineNo = eleInputOrderLine.getAttribute(TelstraConstants.PRIME_LINE_NO, "");
			String sMinLineStatus = XPathUtil.getXpathAttribute(outputGetOrdListForComQry, "//OrderLine[@PrimeLineNo='"+sPrimeLineNo+"']/@MinLineStatus");
			
			if(!YFCObject.isVoid(sMinLineStatus)){
				double dMinLineStatus =  Double.parseDouble(sMinLineStatus); 
				if(dMinLineStatus>=3350.10000){
					bvalidationfailed = true;
					break;
				}
			}
		}
		return bvalidationfailed;
	}

	//HUB-9210[START]

	/**
	 *  <Shipment OrderName="TestRecvOrder1121" >
        <ShipmentLines>
         <ShipmentLine PrimeLineNo="1" ItemID="9400048082" UnitOfMeasure="EA" Quantity="10"/>
         </ShipmentLines>
      </Shipment>
	 * @param inXml
	 */
	private YFCDocument createASNForNBNOrder(YFCDocument inXml) {
		YFCDocument createAsnInDoc = YFCDocument.getDocumentFor("<Shipment OrderName='"+inXml.getDocumentElement().getAttribute("OrderName")+"'><ShipmentLines></ShipmentLines></Shipment>");
		YFCElement shipmentLines = createAsnInDoc.getElementsByTagName("ShipmentLines").item(0);
		YFCNodeList<YFCElement> orderLines = inXml.getElementsByTagName("OrderLine");
		for(YFCElement orderLine : orderLines) {
			YFCElement shipmentLine = createAsnInDoc.createElement("ShipmentLine");
			shipmentLine.setAttributes(orderLine.getAttributes());
			shipmentLine.setAttribute("Quantity", orderLine.getAttribute("StatusQuantity"));
			shipmentLines.appendChild(shipmentLine);
		}
		YFCDocument serviceOutputDoc = invokeYantraService("GpsProcessPOASNMsg", createAsnInDoc);
		YFCElement sOutEle = serviceOutputDoc.getDocumentElement();    
		if("Y".equalsIgnoreCase(sOutEle.getAttribute("ConfirmShip",""))){
			createAsynRequest(serviceOutputDoc);
		}
		return serviceOutputDoc;
	}

	/**
	 *This method createAsynRequest to newly created shipment xml with ConfirmShip flag Y. 
	 * */
	private void createAsynRequest(YFCDocument inXml) {
		YFCDocument asynRequestXml = YFCDocument.getDocumentFor("<CreateAsyncRequest />");
		String message = inXml.getDocumentElement().getString().replace("\\<\\?xml(.+?)\\?\\>", "").trim();
		asynRequestXml.getDocumentElement().setAttribute("ServiceName", "confirmShipment");
		asynRequestXml.getDocumentElement().setAttribute("Message", message);
		asynRequestXml.getDocumentElement().setAttribute(TelstraConstants.INTERFACE_NO, TelstraConstants.ASN_PO_UPLOAD);
		asynRequestXml.getDocumentElement().setAttribute("IsFlow", "N");
		getServiceInvoker().invokeYantraService("GpsCreateAsyncRequest", asynRequestXml);
	}

	/**
	 * This Method will do the inventory adjustment for the serials which has been shipped for the VECTOR Inventory.
	 * @param inXmlDoc2
	 */
	private void adjustInventory(YFCElement shipment,String sOdrName) {
		//SHIPMENT       
		YFCDocument adjustInventoryInDoc = YFCDocument.getDocumentFor("<Items />");
		YFCNodeList<YFCElement> shipmentLines = shipment.getElementsByTagName("ShipmentLine");
		String shipNode = shipment.getAttribute("ShipNode");
		String shipmentNo = shipment.getAttribute("ShipmentNo");
		for(YFCElement shipmentLine : shipmentLines) {
			YFCElement itemEle = adjustInventoryInDoc.getDocumentElement().createChild("Item");
			itemEle.setAttribute("AdjustmentType", "ADJUSTMENT");
			itemEle.setAttribute("ItemID", shipmentLine.getAttribute("ItemID",""));
			itemEle.setAttribute("Quantity", "-"+shipmentLine.getAttribute("Quantity",""));
			itemEle.setAttribute("UnitOfMeasure", shipmentLine.getAttribute("UnitOfMeasure",""));
			itemEle.setAttribute("ShipNode", shipNode);
			itemEle.setAttribute("SupplyType", "ONHAND");

			//HUB-9415[START]
			/*Stamping the Prime Line No and Order Name for Inventory Audit*/

	        itemEle.setAttribute("ReasonText", "Decreasing the supply for Integral Plus or Sterling Orders Pick Confirmation : : Line No = "
	        		+shipmentLine.getAttribute(TelstraConstants.PRIME_LINE_NO,"") +" : : Source ID = "+ sOdrName);
			//HUB-9415[END]

			itemEle.setAttribute("Reference_1", "ShipmentNo:"+shipmentNo);
			itemEle.setAttribute("Reference_2", "ShipmentLineKey:"+shipmentLine.getAttribute("ShipmentLineKey"));
			adjustInventoryInDoc.getDocumentElement().appendChild(itemEle);
		}
		invokeYantraApi("adjustInventory", adjustInventoryInDoc);
	}

	/**
	 * This method is used to change the shipNode for the order line if the shipNode is different in the PickConfirmation message than in the order
	 * @param outputGetOrdListForComQry
	 * @param inXml
	 * @return
	 */
	private YFCDocument updateShipNodeForOrderLine(YFCDocument outputGetOrdListForComQry, YFCElement inEle) {
		/*
		 * set the callChangeOrder flag to false by default
		 */
		boolean callChangeOrder = false;
		String incomingShipNode = inEle.getAttribute("ShipNode");
		String orderHeaderKey = outputGetOrdListForComQry.getElementsByTagName("Order").item(0).getAttribute("OrderHeaderKey");
		YFCDocument changeOrderInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+orderHeaderKey+"'><OrderLines></OrderLines></Order>");
		YFCElement orderLinesEle = changeOrderInput.getElementsByTagName("OrderLines").item(0);
		YFCNodeList<YFCElement> ordLineEleList = outputGetOrdListForComQry.getDocumentElement().getElementsByTagName("OrderLine");
		for (YFCElement ordLineElement : ordLineEleList) {
			String tPlineNo = ordLineElement.getAttribute("PrimeLineNo");
			YFCElement matchingEle = XPathUtil.getXPathElement(YFCDocument.getDocumentFor(inEle.getString()), "//OrderLines/OrderLine[@PrimeLineNo='"+tPlineNo+"']");
			String orderLineShipNode = ordLineElement.getAttribute("ShipNode");
			if(!YFCObject.isVoid(matchingEle) && !incomingShipNode.equals(orderLineShipNode)) {
				YFCElement orderLineForChangeOrder = changeOrderInput.createElement("OrderLine");
				orderLineForChangeOrder.setAttribute("ShipNode", incomingShipNode);
				orderLineForChangeOrder.setAttribute("OrderLineKey", ordLineElement.getAttribute("OrderLineKey"));
				orderLinesEle.appendChild(orderLineForChangeOrder);
				/*
				 * If the shipnode is different in the PickConfirmation and OrderLine, then set the callChangeOrder flag to true to call the changeOrder API
				 */
				callChangeOrder = true;
			}
		}
		/*
		 * If callChangeOrder flag is enabled then return the changeOrderInput, else return null
		 */
		if(callChangeOrder) {
			return changeOrderInput;
		} else {
			return null;
		}
	}

	/* This method to ensure whether orderNo is unique across SO and TO */
	/**
	 * This method is used to validate if the orderName passed is unique accross SO and TO.  In addition to this method will throw and exception if any one of the 
	 * order line has multiple released created.  Also this method will construct the actual pick XML which should be processed.
	 * @param outputGetOrdListForComQry
	 * @param inEle
	 */
	void uniqueOrderValidation(YFCDocument outputGetOrdListForComQry,YFCElement inEle) {
		String totalOrderList = outputGetOrdListForComQry.getDocumentElement().getAttribute("TotalOrderList", "0");

		/*
		 * if totalOrderList is 0, then throw an exception
		 */
		if("0".equals(totalOrderList)){          
			throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.PICK_ORD_NOT_AVAILBALE, new YFSException());
		}

		/*
		 * If totalOrderList is more than 1, then throw an exception
		 */
		if (!"1".equals(totalOrderList)) {
			LoggerUtil.verboseLog("Order Name Duplicate Validation Failure ", logger, TelstraConstants.THROW_EXCEPTION);
			String strErrorCode = getProperty("DuplicateOrderName");          
			throw ExceptionUtil.getYFSException(strErrorCode, new YFSException());
		}

		String docType =  XPathUtil.getXpathAttribute(outputGetOrdListForComQry, "//Order/@DocumentType");
		setOrderHeaderKey(XPathUtil.getXpathAttribute(outputGetOrdListForComQry, "//Order/@OrderHeaderKey"));

		/*
		 * If the document type is 0006 then set the soFlag as false.
		 */
		if("0006".equals(docType)){
			setSoFlag(false);
		}

		/*
		 * if the soFlag is set then the baseDropStatus and TranID wll be set for the Sales Order (SO).  Else baseDropStatus and TranID will be set for Transfer Order (TO).
		 */
		if(!isSoFlag()){
			setBaseDropStatus(getProperty("TOPickBaseDropStatus"));
			setTranID(getProperty("TOPickTranID"));
		}else{
			setBaseDropStatus(getProperty("SOPickBaseDropStatus"));
			setTranID(getProperty("SOPickTranID"));
		}

		YFCNodeList<YFCElement> incomingOrderLines = inEle.getElementsByTagName("OrderLine");
		/*
		 * processPickXml is used as a actual pick message
		 */
		processPickXml = YFCDocument.getDocumentFor("<OrderRelease><OrderLines></OrderLines></OrderRelease>");
		YFCElement processPickOrderLines = processPickXml.getElementsByTagName("OrderLines").item(0);
		processPickXml.getDocumentElement().setAttributes(inEle.getAttributes());
		for(YFCElement incomingOrderLine : incomingOrderLines) {
			int releaseCount = 0;
			Double toBePickedQuantity = incomingOrderLine.getDoubleAttribute("StatusQuantity");
			Double pickableQuantity = 0.0;
			YFCElement matchingOLEle =
					XPathUtil.getXPathElement(outputGetOrdListForComQry,
							"//Order/OrderLines/OrderLine[@PrimeLineNo='" + incomingOrderLine.getAttribute("PrimeLineNo") + "']");
			/*
			 * Throwing an exception if invalid line is passed in the input
			 */
			if(YFCCommon.isVoid(matchingOLEle)){
				throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.PICK_ORD_INVALID_LINE_PASSED, new YFSException());
			}

			YFCElement itemEle = matchingOLEle.getElementsByTagName("Item").item(0);
			String strUom = itemEle.getAttribute("UnitOfMeasure");
			incomingOrderLine.setAttribute("UnitOfMeasure", strUom);

			YFCElement matchingCreateShipmentEle =
					XPathUtil.getXPathElement(createShipmentXml,
							"//OrderRelease/OrderLines/OrderLine[@PrimeLineNo='" + incomingOrderLine.getAttribute("PrimeLineNo") + "']");
			matchingCreateShipmentEle.setAttribute("UnitOfMeasure", strUom);

			YFCNodeList<YFCElement> orderReleaseList = matchingOLEle.getElementsByTagName("OrderStatus");
			for(YFCElement orderRelease : orderReleaseList) {
				Double releaseStatus = orderRelease.getDoubleAttribute("Status");
				//HUB-9261[START]
				
				/*Backordered from Node status will remain for a line even after the status moves to a higher status
				 * therefore ignoring this status while computing the pickableQuantity */
				
				if(releaseStatus < 3201 && releaseStatus!=1400) {
					
					//HUB-9261[END]

					pickableQuantity = pickableQuantity + orderRelease.getDoubleAttribute("StatusQty");
					if(releaseStatus > 1400) {
						releaseCount++;
					}
					/*
					 * If multiple releases are present for the order line in Released Status, then an exception will be thrown
					 */
					if(releaseCount > 1) {
						LoggerUtil.verboseLog("Multiple Releases occur for the order line with PrimeLineNo "+incomingOrderLine.getAttribute("PrimeLineNo"), logger, TelstraConstants.THROW_EXCEPTION);
						throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.PICK_ORD_MULTIPLE_RELEASE, "Multiple Releases occur for the order line with PrimeLineNo "+incomingOrderLine.getAttribute("PrimeLineNo"), new YFSException());

					}
				}
			}
			YFCElement processPickOrderLine = processPickXml.importNode(incomingOrderLine, true);
			if(pickableQuantity >= toBePickedQuantity) {
				processPickOrderLine.setAttribute("StatusQuantity", toBePickedQuantity);
			} else {
				processPickOrderLine.setAttribute("StatusQuantity", pickableQuantity);
			}
			processPickOrderLines.appendChild(processPickOrderLine);
		}
	}

	/**
	 * This method is used to process the Pick Message to create shihpments for the order lines passed
	 * @param outputGetOrdListForComQry
	 * @param inXml
	 */
	private YFCDocument processPickMessage(YFCDocument outputGetOrdListForComQry, YFCDocument inXml) {
		ordLineRKSet = null;
		releaseKeyMap = null;
		shipMap = null;
		resultList = null;
		inputSumMap = null;
		releaseShipToKeyMap = null;
		setOrderHeaderKey(XPathUtil.getXpathAttribute(outputGetOrdListForComQry, "//Order/@OrderHeaderKey"));
		formRelKeySet(outputGetOrdListForComQry, inXml);
		getShipmentLineList(inXml);
		formReleaseKeyMap(outputGetOrdListForComQry);
		ArrayList<String> bestFitRKSet = bestReleaseKeyMatchAlgorithm(inXml);
		YFCDocument outXml = null;
		/*
		 * Check if bestFitRKSet is found.  If bestFitRKSet has values, then create the shipment for each shipToKey for the best fit release keys
		 */
		if(bestFitRKSet != null && bestFitRKSet.size() > 0){
			LoggerUtil.verboseLog("BestFitRKSet", logger,bestFitRKSet.toString());
			outXml = createShipmentPerShipToKeysForReleases(bestFitRKSet);
			return outXml;
		} else {
			/*
			 * Unable to find bestFitRK set, hence backorder the release quantity and call schedule orderlines for the input passed
			 */
			if(!scheduleOrderLinesCalled) {
				outXml = backOrderReleaseQuantityAndCallScheduleOrderLines(outputGetOrdListForComQry, inXml);
			} else {
				LoggerUtil.verboseLog("Unable to determine the best match release key set even after calling changeRelease to backorder and calling scheduleOrderLines", logger, TelstraConstants.THROW_EXCEPTION);
				throw ExceptionUtil.getYFSException("TEL_ERR_006_009", new YFSException());
			}
		}
		return outXml;
	}

	/**
	 * This Method will create shipment per shipToKey for the best match releases and then call changeShipmentStatus() to move the shipment to picked status.
	 * @param bestFitRKSet
	 */
	private YFCDocument createShipmentPerShipToKeysForReleases(ArrayList<String> bestFitRKSet) {
		HashMap<String, HashMap<String, String>> eligibleReleasesForShipToKeys = new HashMap<String, HashMap<String, String>>();
		for(String bestFitRK : bestFitRKSet) {
			if(releaseShipToKeyMap.containsKey(bestFitRK)) {
				Map<String, String> releaseKeyLineNoMap = releaseShipToKeyMap.get(bestFitRK);
				String shipToId = releaseKeyLineNoMap.entrySet().iterator().next().getKey();
				String primeLineNo = releaseKeyLineNoMap.get(shipToId);
				if(eligibleReleasesForShipToKeys.containsKey(shipToId)) {
					Map<String, String> eligibleReleasesMap = eligibleReleasesForShipToKeys.get(shipToId);
					if(eligibleReleasesMap.containsKey(bestFitRK)) {
						String mapPrimeLineNo = eligibleReleasesMap.get(bestFitRK);
						mapPrimeLineNo = mapPrimeLineNo+","+primeLineNo;
						eligibleReleasesMap.remove(bestFitRK);
						eligibleReleasesMap.put(bestFitRK, mapPrimeLineNo);
					} else {
						eligibleReleasesMap.put(bestFitRK, primeLineNo);
					}
					eligibleReleasesForShipToKeys.remove(shipToId);
					eligibleReleasesForShipToKeys.put(shipToId, (HashMap<String, String>) eligibleReleasesMap);
				} else {
					Map<String, String> eligibleReleasesMap = new HashMap<String, String>();
					eligibleReleasesMap.put(bestFitRK, primeLineNo);
					eligibleReleasesForShipToKeys.put(shipToId, (HashMap<String, String>) eligibleReleasesMap);
				}
			}
		}

		Set<Entry<String, HashMap<String, String>>> eligibleReleasesForShipToKeysSet = eligibleReleasesForShipToKeys.entrySet();
		Iterator<Entry<String, HashMap<String, String>>> it = eligibleReleasesForShipToKeysSet.iterator();
		YFCDocument outXml = YFCDocument.getDocumentFor("<Shipments></Shipments>");
		YFCElement outXmlRoot = outXml.getDocumentElement();
		while (it.hasNext()) {
			Map.Entry<String, HashMap<String, String>> curMapEntry = (Map.Entry<String, HashMap<String, String>>) it.next();

			HashMap<String, String> eligibileReleaseMap = curMapEntry.getValue();
			Set<Entry<String, String>> eligibileReleaseMapSet = eligibileReleaseMap.entrySet();
			Iterator<Entry<String, String>> eligibileReleaseMapSetItr = eligibileReleaseMapSet.iterator();
			YFCDocument inDocCreateShipment =
					YFCDocument.getDocumentFor("<Shipment Action='Create' OrderHeaderKey='"+getOrderHeaderKey()+"'>"
							+ "<ShipmentLines/>" + "</Shipment> ");
			while(eligibileReleaseMapSetItr.hasNext()) {
				Map.Entry<String, String> eligibileReleaseMapSetItrEntry = (Map.Entry<String, String>) eligibileReleaseMapSetItr.next();
				String releaseKey =  eligibileReleaseMapSetItrEntry.getKey();
				String primeLineNo = eligibileReleaseMapSetItrEntry.getValue();
				String[] primeLineNos = primeLineNo.split(",");
				YFCElement shipmentLines = inDocCreateShipment.getDocumentElement().getChildElement("ShipmentLines");
				for(String lineNo : primeLineNos) {
					YFCElement matchingEle = XPathUtil.getXPathElement(createShipmentXml, "//OrderLines/OrderLine[@PrimeLineNo='" + lineNo + "']");
					YFCElement shipmentLine = inDocCreateShipment.createElement("ShipmentLine");
					shipmentLine.setAttribute("PrimeLineNo", lineNo);
					shipmentLine.setAttribute("SubLineNo", "1");
					shipmentLine.setAttribute("ItemID", matchingEle.getAttribute("ItemID"));
					shipmentLine.setAttribute("UnitOfMeasure", matchingEle.getAttribute("UnitOfMeasure"));
					shipmentLine.setAttribute("OrderHeaderKey", getOrderHeaderKey());
					shipmentLine.setAttribute("OrderReleaseKey", releaseKey);
					shipmentLine.setAttribute("Quantity", matchingEle.getAttribute("StatusQuantity"));
					shipmentLines.appendChild(shipmentLine);
				}
			}

			YFCDocument createShipmentOutDoc =
					invokeYantraApi(
							"createShipment",
							inDocCreateShipment,
							YFCDocument
							.getDocumentFor("<Shipment ShipmentKey='' DocumentType='' Status='' ShipmentNo='' ShipNode=''><ShipmentLines><ShipmentLine OrderHeaderKey='' OrderReleaseKey='' OrderLineKey='' ShipmentLineKey='' ItemID='' Quantity=''  PrimeLineNo='' UnitOfMeasure='' /></ShipmentLines></Shipment>"));
			if(!YFCObject.isVoid(createShipmentOutDoc)) {
				YFCElement shipmentElement = createShipmentOutDoc.getDocumentElement();
				YFCElement outXmlShipmentEle = outXml.importNode(shipmentElement, true);
				outXmlRoot.appendChild(outXmlShipmentEle);
				setShipmentKey(XPathUtil.getXpathAttribute(createShipmentOutDoc, "//@ShipmentKey"));
				changeShimentStatus();
			}
		}
		return outXml;
	}

	/**
	 * This method is used move the created shipment to picked status
	 */
	private void changeShimentStatus() {
		YFCDocument inputApiDoc = YFCDocument.getDocumentFor("<Shipment Action='Modify' BaseDropStatus='"+getBaseDropStatus()+"' " +
				"ShipmentKey='"+getShipmentKey()+"' " +
				"TransactionId='"+getTranID()+"' />");
		invokeYantraApi("changeShipmentStatus", inputApiDoc);
	}

	/**
	 * This method is to map releaseKey -> Associated release quantities with line detail 
	 * @param outputGetOrdListForComQry
	 */
	private void formReleaseKeyMap(YFCDocument outputGetOrdListForComQry) {
		YFCElement rootEle = outputGetOrdListForComQry.getDocumentElement();
		YFCElement ordLinesEle = rootEle.getChildElement("Order",true).getChildElement("OrderLines",true);
		YFCNodeList<YFCElement> ordLineList = ordLinesEle.getElementsByTagName("OrderLine");

		for (YFCElement ordLineEle : ordLineList) {
			String pLNo = ordLineEle.getAttribute("PrimeLineNo");
			String sLNo = ordLineEle.getAttribute("SubLineNo","1");
			String shipToKey = ordLineEle.getAttribute("ShipToKey");
			YFCElement ordLnRlsStatusMainEle = ordLineEle.getChildElement("OrderStatuses",true);            
			YFCNodeList<YFCElement> ordLnRlsStatusSubEleList = ordLnRlsStatusMainEle.getElementsByTagName("OrderStatus");

			for (YFCElement rlsStatusEle : ordLnRlsStatusSubEleList) {
				String status = rlsStatusEle.getAttribute("Status","");
				if(getProperty("OrderReleaseStatus").equals(status)){
					String curOrdLineRK = rlsStatusEle.getAttribute("OrderReleaseKey","");

					if(releaseKeyMap == null){
						releaseKeyMap = new HashMap<>();
					}

					if(releaseShipToKeyMap == null) {
						releaseShipToKeyMap = new HashMap<String, HashMap<String, String>>();
					}

					rlsStatusEle.setAttribute("PrimeLineNo", pLNo);
					rlsStatusEle.setAttribute("SubLineNo", sLNo);

					ArrayList<YFCElement> tmpNL = null;
					if(!releaseKeyMap.containsKey(curOrdLineRK)){
						tmpNL = new ArrayList<>();
					}else{
						tmpNL = releaseKeyMap.get(curOrdLineRK);
					}

					tmpNL.add(rlsStatusEle);
					releaseKeyMap.put(curOrdLineRK, tmpNL);
					/*
					 * Adding the ShipToKey and primeLineNo for each release key and storing it in releaseShipToKeyMap.
					 * 
					 */
					if(releaseShipToKeyMap.containsKey(curOrdLineRK)) {
						Map<String, String> releaseMap = releaseShipToKeyMap.get(curOrdLineRK);
						String primeLineNo = releaseMap.get(shipToKey);
						primeLineNo = primeLineNo +","+pLNo;
						releaseMap.remove(shipToKey);
						releaseMap.put(shipToKey, primeLineNo);
						releaseShipToKeyMap.remove(curOrdLineRK);
						releaseShipToKeyMap.put(curOrdLineRK, (HashMap<String, String>) releaseMap);
					} else {
						Map<String, String> releaseMap = new HashMap<String, String>();
						releaseMap.put(shipToKey, pLNo);
						releaseShipToKeyMap.put(curOrdLineRK, (HashMap<String, String>) releaseMap);
					}

				}
			}
		}

	}

	/**
	 * This method is to find the best release keys from the releaseKeyMap by making subset of the releaseKeyMap keys which is nothing but release keys 
	 * @param inXml
	 * @return
	 */
	private ArrayList<String> bestReleaseKeyMatchAlgorithm(YFCDocument inXml) {

		findCorrectReleaseMap(inXml);   
		constructInputLineQuantityMap(inXml);

		if(releaseKeyMap == null || releaseKeyMap.size()==0){
			return null;
		}

		int mapSize = releaseKeyMap.size();
		ArrayList<String> releaseKeys = getKeys();
		findAllSubSet(releaseKeys,mapSize);
		return resultList;
	}

	/**
	 * This will exclude the unnecessary releasKeys from the releaseKey map 
	 * @param inXml
	 */
	private void findCorrectReleaseMap(YFCDocument inXml) {

		if(releaseKeyMap == null || releaseKeyMap.size()==0){
			return;
		}

		Set<Entry<String, ArrayList<YFCElement>>> entrySet = releaseKeyMap.entrySet();
		Iterator<Entry<String, ArrayList<YFCElement>>> it = entrySet.iterator();      
		ArrayList<String> removeRKList = new ArrayList<>();
		while (it.hasNext()) {          
			Map.Entry<String, ArrayList<YFCElement>> curMapEntry = (Map.Entry<String, ArrayList<YFCElement>>) it.next();
			String cur_ORK =  (String) curMapEntry.getKey();
			ArrayList < YFCElement > curStatusEleList = (ArrayList < YFCElement > ) curMapEntry.getValue();
			for (YFCElement yfcElement : curStatusEleList) {

				String pLNo = yfcElement.getAttribute("PrimeLineNo","");
				YFCElement matchingInputLineEle = XPathUtil.getXPathElement(inXml, "//OrderLines/OrderLine[@PrimeLineNo='"+pLNo+"']");

				if(XmlUtils.isVoid(matchingInputLineEle)){
					removeRKList.add(cur_ORK);
				}else{
					Double qtyStr = matchingInputLineEle.getDoubleAttribute("StatusQuantity");
					if(qtyStr <= 0){
						removeRKList.add(cur_ORK);
					}
				}
			}
		}

		for (String rk : removeRKList) {
			if(releaseKeyMap.containsKey(rk)){
				releaseKeyMap.remove(rk);
			}
			if(releaseShipToKeyMap.containsKey(rk)) {
				releaseShipToKeyMap.remove(rk);
			}
		}

	}

	/**
	 * This method forms the line qty map for the inXml 
	 * @param inXml
	 */
	private void constructInputLineQuantityMap(YFCDocument inXml) {

		YFCNodeList<YFCElement> lineEleList = inXml.getElementsByTagName("OrderLine");
		for (YFCElement yfcElement : lineEleList) {
			String ln = yfcElement.getAttribute("PrimeLineNo");
			Double qty = yfcElement.getDoubleAttribute("StatusQuantity");
			if(qty <=0){
				continue;
			}
			if(inputSumMap == null){
				inputSumMap = new HashMap<>();
			}
			if(!inputSumMap.containsKey(ln)){
				inputSumMap.put(ln, qty);
			}else{
				Double prev_Qty = inputSumMap.get(ln);
				inputSumMap.put(ln, prev_Qty+qty);
			}
		}
	}

	/**
	 * This method is to get shipment line list for the picked shipments as this gives associated Order release keys as well setting
	 * the vector order flag if not shipments there for an order. 
	 * @param inXml
	 */
	private void getShipmentLineList(YFCDocument inXml) {
		YFCDocument inApiDoc = YFCDocument.getDocumentFor("<ShipmentLine OrderHeaderKey='" + getOrderHeaderKey() + "'><Shipment Status='1200.10000'/></ShipmentLine>");
		YFCDocument outputShipLnLst = invokeYantraApi("getShipmentLineList", inApiDoc);
		setShipmentLineListDoc(outputShipLnLst);
		formShipMap(outputShipLnLst);
		findCorrectShipment(inXml);
	}

	/**
	 * This method  is to determine the existing shipment according to the input CEV message, based on the unique Order release key set. 
	 * @param inXml
	 */
	@SuppressWarnings("rawtypes")
	private void findCorrectShipment(YFCDocument inXml) {
		if (shipMap != null) {
			Set entrySet = shipMap.entrySet();
			Iterator it = entrySet.iterator();
			YFCNodeList < YFCNode > shipEleListInMsg = XPathUtil.getXpathNodeList(inXml, "//OrderLine[@StatusQuantity!='0']");
			int lineSizeInMsg = shipEleListInMsg.getLength();
			while (it.hasNext()) {
				Map.Entry curMapEntry = (Map.Entry) it.next();
				YFCDocument shipDoc = (YFCDocument) curMapEntry.getValue();
				YFCElement shipEle = shipDoc.getDocumentElement();
				String shipKey = (String) curMapEntry.getKey();
				YFCNodeList < YFCElement > shipLineList = shipEle.getElementsByTagName("OrderLine");
				int lineSizeInShip = shipLineList.getLength();
				if (lineSizeInShip == lineSizeInMsg) {
					Set < YFCElement > uniqueEleSet = null;
					//HUB-7887 - Begin
					if (uniqueEleSet == null) {
						uniqueEleSet = new HashSet < YFCElement > ();
					}

					for (YFCNode yfcNode: shipEleListInMsg) {
						YFCElement yfcEle = YFCDocument.getDocumentFor(yfcNode.toString()).getDocumentElement();
						String iQty = yfcEle.getAttribute("StatusQuantity","0");
						Integer iQty_i = Integer.valueOf(iQty);
						YFCElement tEle = XPathUtil.getXPathElement(shipDoc, "//OrderLine[@IsValidated='N' and @ItemID='" + yfcEle.getAttribute("ItemID") + "' and @Quantity='"+String.valueOf(iQty_i)+".00"+"']");

						if (!XmlUtils.isVoid(tEle)) {
							uniqueEleSet.add(tEle);
							tEle.setAttribute("IsValidated", "Y");
						}
					}

					if (uniqueEleSet.size() == lineSizeInMsg && isShipmentReleaseKeysMatching(shipLineList)) {
						setShipmentKey(shipKey);
						return;
					}                   
					//HUB-7887 - End
				}
			}
		}
	}

	/**
	 * This method is to determine shipment line order release key is mapping with incoming order release key or not. 
	 * @param shipEle
	 * @return
	 */
	private boolean isShipmentReleaseKeysMatching(YFCNodeList < YFCElement > shipEle) {
		int i = 0;
		for (YFCElement yfcElement: shipEle) {
			String sOrdRelKey = yfcElement.getAttribute(TelstraConstants.ORDER_RELEASE_KEY);
			if (ordLineRKSet.contains(sOrdRelKey)) {
				i++;
			}
		}
		if (shipEle.getLength() == i) {
			return true;
		}
		return false;
	}

	/**
	 * This method will group the shipment line list output according to the shipment key 
	 * @param outputShipLnLst
	 */
	private void formShipMap(YFCDocument outputShipLnLst) {
		YFCNodeList < YFCElement > shipLineEleList = outputShipLnLst.getDocumentElement().getElementsByTagName("ShipmentLine");
		if (shipLineEleList.getLength() == 0) {
			return;
		}
		for (YFCElement yfcElement: shipLineEleList) {
			String sKey = yfcElement.getAttribute("ShipmentKey");

			if (shipMap == null) {
				shipMap = new HashMap < String, YFCDocument > ();
			}
			YFCDocument tD = shipMap.get(sKey);
			if (XmlUtils.isVoid(tD)) {
				tD = YFCDocument.getDocumentFor("<Shipment/>");
			}

			YFCDocument tO = tD.getDocumentElement().getOwnerDocument();
			yfcElement.setAttribute("IsValidated", "N");
			YFCElement sl = tO.importNode(yfcElement, true);
			tD.getDocumentElement().appendChild(sl);
			if (shipMap.get(sKey) == null) {
				shipMap.put(sKey, tD);
			}
		}
	}

	/**
	 * This method will backorder the release quantity and then call the scheduleOrderLines API for the input passed
	 * @param outputGetOrdListForComQry
	 * @param inXml
	 */
	private YFCDocument backOrderReleaseQuantityAndCallScheduleOrderLines(
			YFCDocument outputGetOrdListForComQry, YFCDocument inXml) {
		scheduleOrderLinesCalled = true;
		YFCNodeList<YFCElement> inputOrderLines = inXml.getElementsByTagName("OrderLine");
		for(YFCElement inputOrderLine : inputOrderLines) {
			String inputPrimeLineNo = inputOrderLine.getAttribute("PrimeLineNo");

			YFCElement matchingOLEle =
					XPathUtil.getXPathElement(outputGetOrdListForComQry,
							"//Order/OrderLines/OrderLine[@PrimeLineNo='" + inputPrimeLineNo + "']");
			YFCElement ordLnRlsStatusMainEle = matchingOLEle.getChildElement("OrderStatuses", true);
			YFCNodeList<YFCElement> ordLnRlsStatusSubEleList = ordLnRlsStatusMainEle.getElementsByTagName("OrderStatus");
			Map<String, Double> statusMap = new HashMap<String, Double>();
			for(YFCElement orderLineStatus : ordLnRlsStatusSubEleList) {
				String status = orderLineStatus.getAttribute("Status");
				/*
				 * Identify if any orderline is in the pickable status and then add the release key and status quantity in statusMap for the releases that are in Released Status
				 */
				if("3200".equals(status) || "3200.5000".equals(status)) {
					Double statusQuantity = orderLineStatus.getDoubleAttribute("StatusQty");
					String strOrderReleaseKey = orderLineStatus.getAttribute("OrderReleaseKey");
					statusMap.put(strOrderReleaseKey, statusQuantity);
				}
			}

			Set<Entry<String, Double>> statusMapEntrySet = statusMap.entrySet();
			List<Entry<String, Double>> statusMapEntrySetList = new ArrayList<Entry<String, Double>>(statusMapEntrySet);

			/*
			 * Sort all the map entries into descending order
			 */
			Collections.sort(statusMapEntrySetList, new Comparator<Map.Entry<String, Double>>() {
				@Override
				public int compare( Map.Entry<String, Double> entry1, Map.Entry<String, Double> entry2){
					return (entry2.getValue()).compareTo( entry1.getValue() );
				}
			});
			Double changeReleaseQuantity = inputOrderLine.getDoubleAttribute("StatusQuantity");
			/*
			 * Iterate each entry in the statusMapEntrySetList from the last entry to get the release with least quantity
			 */
			for(int i=statusMapEntrySetList.size()-1; i>=0 ; i--) {
				String releaseKey = statusMapEntrySetList.get(i).getKey();
				Double statusQuantity = statusMapEntrySetList.get(i).getValue();
				YFCDocument changeReleaseDoc=YFCDocument.getDocumentFor("<OrderRelease><OrderLines/></OrderRelease>");
				YFCElement ordRelsEle = changeReleaseDoc.getDocumentElement();
				ordRelsEle.setAttribute("OrderReleaseKey",releaseKey);
				YFCElement ordLnsEle = ordRelsEle.getChildElement("OrderLines");  
				YFCElement changeReleaseOrdLineEleTemp = changeReleaseDoc.createElement("OrderLine");
				changeReleaseOrdLineEleTemp.setAttribute("Action", "BACKORDER");
				changeReleaseOrdLineEleTemp.setAttribute("PrimeLineNo", inputOrderLine.getAttribute("PrimeLineNo"));
				changeReleaseOrdLineEleTemp.setAttribute("SubLineNo", inputOrderLine.getAttribute("SubLineNo","1"));
				changeReleaseOrdLineEleTemp.setAttribute("ItemID", inputOrderLine.getAttribute("ItemID"));
				changeReleaseOrdLineEleTemp.setAttribute("UnitOfMeasure", inputOrderLine.getAttribute("UnitOfMeasure"));
				/*
				 * If the incoming quantity matches the release quantity then do nothing.  
				 */
				/*if(changeReleaseQuantity.equals(statusQuantity)) {
          continue;
        }*/
				if(changeReleaseQuantity > 0) {
					/*
					 *    If the  changeReleaseQuantity > statusQuantity, set the ChangeInQuantity as statusQuantity, else set the ChangeInQuantity as changeReleaseQuantity.
					 *    ChangeInQuantity is the quantity which will be backordered form the release
					 */
					if(changeReleaseQuantity > statusQuantity) {
						changeReleaseOrdLineEleTemp.setAttribute("ChangeInQuantity", "-"+statusQuantity);
					} else {
						changeReleaseOrdLineEleTemp.setAttribute("ChangeInQuantity", "-"+changeReleaseQuantity);
					}
					changeReleaseQuantity = changeReleaseQuantity - statusQuantity;
				}
				ordLnsEle.appendChild(changeReleaseOrdLineEleTemp);
				callChangeRelease(changeReleaseDoc); 
			}
		}

		callScheduleOrderLines(outputGetOrdListForComQry, inXml);
		/*
		 * Call getOrderList API to get the udpated Releases in the outputGetOrdLustForCompQry document
		 */
		outputGetOrdListForComQry = getOrderList(inXml.getDocumentElement());
		/*
		 * Call processPickMessage with the updated getOrderList Output
		 */
		YFCDocument outXml = processPickMessage(outputGetOrdListForComQry, inXml);
		return outXml;
	}

	/**
	 *  This method is used to call scheduleOrderLines API to include all the incoming order lines into the new releases
	 * @param outputGetOrdListForComQry
	 * @param inXml
	 */
	private void callScheduleOrderLines(YFCDocument outputGetOrdListForComQry, YFCDocument inXml) {
		YFCNodeList<YFCElement> inputOrderLines = inXml.getElementsByTagName("OrderLine");
		YFCElement orderEle = outputGetOrdListForComQry.getElementsByTagName("Order").item(0);
		String orderNo = orderEle.getAttribute("OrderNo");
		String documentType = orderEle.getAttribute("DocumentType");
		String enterpriseCode = orderEle.getAttribute("EnterpriseCode");
		String shipNode = inXml.getDocumentElement().getAttribute("ShipNode","");
		/*
		 * Get the deliveryDate as the current date to pass it in the scheduelOrderLines API
		 */
		YDate deliveryDate = new YDate(false);
		YFCDocument scheduleOrderLineInDoc = YFCDocument.getDocumentFor("<Promise DocumentType='"+documentType+"' EnterpriseCode='"+enterpriseCode+"' IgnoreReleaseDate='Y' "
				+ "OrderNo='"+orderNo+"' ScheduleAndRelease='Y'> <PromiseLines> </PromiseLines></Promise>");
		YFCElement promiseLines = scheduleOrderLineInDoc.getElementsByTagName("PromiseLines").item(0);
		for(YFCElement inputOrderLine : inputOrderLines) {
			YFCElement scheduleOrderLinePL = scheduleOrderLineInDoc.createElement("PromiseLine");
			scheduleOrderLinePL.setAttribute("ShipNode", shipNode);
			scheduleOrderLinePL.setAttribute("SubLineNo", "1");
			scheduleOrderLinePL.setAttribute("PrimeLineNo", inputOrderLine.getAttribute("PrimeLineNo"));
			scheduleOrderLinePL.setAttribute("Quantity", inputOrderLine.getAttribute("StatusQuantity"));
			scheduleOrderLinePL.setAttribute("DeliveryDate", deliveryDate);
			promiseLines.appendChild(scheduleOrderLinePL);
		}
		invokeYantraApi("scheduleOrderLines", scheduleOrderLineInDoc);
	}

	/**
	 * This method will be used to call changeRelease API to backorder the quantity passed in the changeReleaseDoc input
	 * @param changeReleaseDoc
	 */
	private void callChangeRelease(YFCDocument changeReleaseDoc) {
		YFCDocument outTempForChangeRelease = YFCDocument.getDocumentFor("<OrderRelease OrderHeaderKey='' DocumentType='' EnterpriseCode='' OrderReleaseKey='' ReleaseNo=''/>");
		invokeYantraApi("changeRelease",changeReleaseDoc,outTempForChangeRelease);
	}

	/**
	 * Get the order list for the input message order name
	 */
	YFCDocument getOrderList(YFCElement inEle) {


		YFCDocument inDocgetOrdList =
				YFCDocument.getDocumentFor("<Order>" + "<ComplexQuery Operator='OR'><And><Or>"
						+ "<Exp Name='DocumentType' QryType='EQ' Value='0001'/>"
						+ "<Exp Name='DocumentType' Value='0006' QryType='EQ'/>"
						+ "<Exp Name='DocumentType' Value='0005' QryType='EQ'/></Or>"
						+ "<Exp Name='OrderName' Value='" + inEle.getAttribute("OrderName")
						+ "' QryType='EQ'/>" + "</And>" + "</ComplexQuery>" + "</Order>");
		YFCDocument templateForGetOrdList =
				YFCDocument.getDocumentFor("<OrderList>"
						+ "<Order DocumentType='' OrderHeaderKey='' HoldFlag='' OrderName='' OrderNo='' EnterpriseCode='' EntryType='' SellerOrganizationCode='' OrderType=''>" + "<OrderLines>"
						+ "<OrderLine PrimeLineNo='' SubLineNo='' OrderLineKey='' ShipNode='' ShipToID='' ShipToKey='' DeliveryDate='' OrderedQty='' MinLineStatus=''> "
						+ "<Item ItemID='' OrganizationCode='' UnitOfMeasure=''></Item> "
						+ "<OrderStatuses>" + "<OrderStatus OrderReleaseKey='' Status='' StatusQty=''/>"
						+ "</OrderStatuses> " + "</OrderLine>" + "</OrderLines>" + "</Order>" + "</OrderList>");
		YFCDocument outputGetOrdListForComQry =
				invokeYantraApi("getOrderList", inDocgetOrdList, templateForGetOrdList);

		//HUB-9218[START]

		/* The below validation is removed to check if the Order is in hold or not*/

		/*if ("Y".equals(XPathUtil.getXpathAttribute(outputGetOrdListForComQry, "//Order/@HoldFlag"))) {
      LoggerUtil.verboseLog("Order Hold Validation Failure ", logger,
          TelstraConstants.THROW_EXCEPTION);
      String strErrorCode = getProperty("OrderHoldValidation");
      throw ExceptionUtil.getYFSException(strErrorCode, new YFSException());
    }*/

		//HUB-9218[END]

		return outputGetOrdListForComQry;
	}

	/* This method is to validate the input xml for required attributies. */
	void validateInputXml(YFCElement inEle) {
		String ordName = inEle.getAttribute("OrderName", "");
		boolean validInputFlg = true;
		YFCElement orderLines = inEle.getElementsByTagName(TelstraConstants.ORDER_LINES).item(0);
		YFCNodeList<YFCElement>yfcLineList = orderLines.getElementsByTagName(TelstraConstants.ORDER_LINE);
		for (YFCElement yfcElement : yfcLineList) {
			String plNo = yfcElement.getAttribute("PrimeLineNo", "");
			String itemID = yfcElement.getAttribute("ItemID", "");
			String action = yfcElement.getAttribute("Action", "");
			Double dQty = new Double(yfcElement.getDoubleAttribute("StatusQuantity", 0.0));
			/*
			 * If one of the line has StatusQuantity as 0, then remove the respective OrderLine element from the input xml
			 */
			if(dQty == 0.0) {
				orderLines.removeChild(yfcElement.getDOMNode());
				if(yfcLineList.getLength() == 0) {
					/*
					 * If the number of orderLines is 0, then consume the message but do nothing.
					 */
					noLinesToPick = true;
					return;
				}
				continue;
			}
			if (XmlUtils.isVoid(ordName) || XmlUtils.isVoid(plNo)) {
				validInputFlg = false;
				break;
			}
			if (XmlUtils.isVoid(itemID) || XmlUtils.isVoid(action)) {
				validInputFlg = false;
				break;
			}

			int iQty = dQty.intValue();
			yfcElement.setIntAttribute("StatusQuantity", iQty);

			if (YFCObject.isVoid(yfcElement.getAttribute("SubLineNo", ""))) {
				yfcElement.setAttribute("SubLineNo", "1");
			}

		}
		if (!validInputFlg || (yfcLineList.getLength() == 0)) {
			LoggerUtil.verboseLog("CEV message validation failure ", logger,
					TelstraConstants.THROW_EXCEPTION);
			String strErrorCode = getProperty("CevMsgValidation");
			// YFCDocument erroDoc = ExceptionUtil.getYFSExceptionDocument(strErrorCode);
			throw ExceptionUtil.getYFSException(strErrorCode, new YFSException());
		}
	}

	/**
	 * This method is to determine the unique release keys of order lines according to the input xml
	 * passed. As well we are determining , need to back order or not.
	 * 
	 * @param outputGetOrdListForComQry
	 * @param inXml
	 */
	private void formRelKeySet(YFCDocument outputGetOrdListForComQry, YFCDocument inXml) {
		YFCNodeList < YFCNode > ordLineNodeList = XPathUtil.getXpathNodeList(inXml, "//OrderLine[@StatusQuantity!='0']");
		if (ordLineNodeList.getLength() == 0) {
			return;
		}
		for (YFCNode yfcNode: ordLineNodeList) {
			YFCElement yfcElement = YFCDocument.getDocumentFor(yfcNode.toString()).getDocumentElement();
			String plNo = yfcElement.getAttribute("PrimeLineNo");
			String slNo = yfcElement.getAttribute("SubLineNo", "1");
			YFCElement matchingEle = XPathUtil.getXPathElement(outputGetOrdListForComQry, "//Order/OrderLines/OrderLine[@PrimeLineNo='" + plNo + "' and @SubLineNo='" + slNo + "']");
			if (!XmlUtils.isVoid(matchingEle)) {
				YFCNodeList<YFCElement> orderStatuses = matchingEle.getElementsByTagName("OrderStatus");
				for(YFCElement orderStatus : orderStatuses) {
					if(getProperty("OrderReleaseStatus").equals(orderStatus.getAttribute("Status")) && yfcElement.getDoubleAttribute("StatusQuantity") == orderStatus.getDoubleAttribute("StatusQty")) {
						String relKey = orderStatus.getAttribute("OrderReleaseKey");
						if (ordLineRKSet == null) {
							ordLineRKSet = new HashSet < String > ();
						}
						ordLineRKSet.add(relKey);
					}
				}
			}
		}
	}

	/**
	 * Finding all subsets of a given list.
	 * Ref:http://www.geeksforgeeks.org/finding-all-subsets-of-a-given-set-in-java
	 * 
	 * @param releaseKeys
	 * @param n
	 */
	private void findAllSubSet(ArrayList<String> releaseKeys, int n) {
		for (int i = 0; i < (1 << n); i++) {
			ArrayList<String> tmp = new ArrayList<>();
			for (int j = 0; j < n; j++) {
				if ((i & (1 << j)) > 0) {
					tmp.add(releaseKeys.get(j));
				}
			}
			if (tmp.size() > 0) {
				LoggerUtil.verboseLog("Current BestFit ORK List ", logger, tmp.toString());
				HashMap<String, Double> curr_Sum_Map = constructComparisonMap(tmp);
				if (compare(curr_Sum_Map)) {
					LoggerUtil.verboseLog("Current BestFit ORK List ", logger,tmp.toString());    
					int curr_rkl_size = tmp.size();
					if(rkl_size == 0 || rkl_size > curr_rkl_size ){
						rkl_size = curr_rkl_size;
						resultList = tmp;
					}
				}
			}
		}
	}

	/**
	 * This method helps to sum up the passed ORKs released qtys
	 * 
	 * @param arr
	 * @return
	 */
	private HashMap<String, Double> constructComparisonMap(ArrayList<String> arr) {
		HashMap<String, Double> curr_Sum_Map = new HashMap<>();
		for (String key : arr) {
			ArrayList<YFCElement> curr_List = releaseKeyMap.get(key);
			for (YFCElement currEle : curr_List) {
				String ln = currEle.getAttribute("PrimeLineNo");
				Double qty = currEle.getDoubleAttribute("StatusQty");
				if (!curr_Sum_Map.containsKey(ln)) {
					curr_Sum_Map.put(ln, qty);
				} else {
					Double prev_Qty = curr_Sum_Map.get(ln);
					curr_Sum_Map.put(ln, prev_Qty + qty);
				}
			}
		}
		return curr_Sum_Map;
	}

	/**
	 * 
	 * @return
	 */
	private ArrayList<String> getKeys() {
		ArrayList<String> keys = new ArrayList<String>();
		Set<Entry<String, ArrayList<YFCElement>>> entrySet = releaseKeyMap.entrySet();
		Iterator<Entry<String, ArrayList<YFCElement>>> it = entrySet.iterator();
		while (it.hasNext()) {
			Map.Entry<String, ArrayList<YFCElement>> curMapEntry =
					(Map.Entry<String, ArrayList<YFCElement>>) it.next();
			keys.add((String) curMapEntry.getKey());
		}
		LoggerUtil.verboseLog("All ORK List ", logger, keys.toString());
		return keys;
	}

	/**
	 * This method compares inputSumMap and curr_Sum_Map is same or not,this is to ensure that the
	 * releaseKeys and inXml lines are properly matching or not
	 * 
	 * @param curr_Sum_Map
	 * @return
	 */
	private boolean compare(HashMap<String, Double> curr_Sum_Map) {
		if (curr_Sum_Map.equals(inputSumMap)){
			LoggerUtil.verboseLog("Input message LineNo - > Qty map ", logger,inputSumMap.toString());
			LoggerUtil.verboseLog("Match Found for the current releaseKey/s list", logger,curr_Sum_Map.toString());
			return true;
		}else{
			return false;
		}
	}

	public YFCDocument getShipmentLineListDoc() {
		return shipmentLineListDoc;
	}

	public void setShipmentLineListDoc(YFCDocument shipmentLineListDoc) {
		this.shipmentLineListDoc = shipmentLineListDoc;
	}

	public String getOrderHeaderKey() {
		return orderHeaderKey;
	}

	public void setOrderHeaderKey(String orderHeaderKEY) {
		orderHeaderKey = orderHeaderKEY;
	}

	public String getShipmentKey() {
		return shipmentKey;
	}

	public void setShipmentKey(String shipmentKey) {
		this.shipmentKey = shipmentKey;
	}

	public boolean isSoFlag() {
		return soFlag;
	}

	public void setSoFlag(boolean soFlag) {
		this.soFlag = soFlag;
	}


	public String getBaseDropStatus() {
		return baseDropStatus;
	}


	public void setBaseDropStatus(String baseDropStatus) {
		this.baseDropStatus = baseDropStatus;
	}

	public String getTranID() {
		return tranID;
	}

	public void setTranID(String tranID) {
		this.tranID = tranID;
	}
}
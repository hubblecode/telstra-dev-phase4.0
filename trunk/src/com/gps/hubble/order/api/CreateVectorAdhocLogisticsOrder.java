/***********************************************************************************************
 * File Name        : CreateVectorAdhocLogisticsOrder.java
 *
 * Description      : Ad-hoc Logistics will be created in Vector, We will create a Sales Order and and a Shipment 
 * 					  and move it to Shipped Status with the logistics status as pending.
 *
 * Modification Log :
 * ---------------------------------------------------------------------------------------------
 * Ver #  	Date              Author				Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0		Jun 08,2017      Keerthi Yadav           Initial Version - HUB-9294: Neway delivery LRA
 * 1.1		Jun 29,2017		 Keerthi Yadav			 HUB-9431: Tracking Number to be captured for Vector Adhoc Logistics Order
 * ---------------------------------------------------------------------------------------------
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
 **********************************************************************************************/
package com.gps.hubble.order.api;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;

/**
 * 
 * @author
 *
 */
public class CreateVectorAdhocLogisticsOrder extends AbstractCustomApi {
	private static YFCLogCategory logger = YFCLogCategory.instance(CreateVectorAdhocLogisticsOrder.class);
	private YFCDocument yfcDocGetOrderListOp = null;

	public YFCDocument invoke(YFCDocument inXml) {
		YFCDocument outconfirmShipmentxml = inXml;
		String sOrderName = inXml.getDocumentElement().getAttribute(TelstraConstants.ORDER_NAME,"");
		boolean bIsOrderAlreadyCreated = validateIfOrderExists(sOrderName);
		if(!bIsOrderAlreadyCreated){
			outconfirmShipmentxml=createOrder(inXml);
			linkingAssociatedLraToNewLra(outconfirmShipmentxml);
		}
		else{
			/*
			 * In the UAT LRA was created without any association due to code defect. To reprocess the xml, creating update method.
			 */
			updateParentAndChildLRA(inXml);
		}


		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke",inXml);
		return outconfirmShipmentxml;

	}

	/**
	 * This method updates the parent and child LRA 
	 * @param inXml
	 */

	private void updateParentAndChildLRA(YFCDocument inXml) {


		YFCElement eleOrderExtn = inXml.getDocumentElement().getChildElement(TelstraConstants.EXTN);

		if(!YFCObject.isVoid(eleOrderExtn)){
			String sAssociatedLRA = eleOrderExtn.getAttribute("AssociatedLRA","");
			String sLRA = eleOrderExtn.getAttribute(TelstraConstants.LRA,"");

			if(!YFCObject.isVoid(sAssociatedLRA)){
				String sNewLRA = sLRA + ",AssociatedLRA:" + sAssociatedLRA;

				YFCElement yfcEleOrder = XPathUtil.getXPathElement(yfcDocGetOrderListOp, "//Order");
				if(!YFCCommon.isVoid(yfcEleOrder)){

					/*
					 * Update child LRA
					 */

					String sOrderHeaderKey = yfcEleOrder.getAttribute(TelstraConstants.ORDER_HEADER_KEY);
					YFCDocument yfcDocChangeOrderIp = YFCDocument.getDocumentFor("<Order Override='Y' OrderHeaderKey='"+sOrderHeaderKey+"'><Extn LRA='"+sNewLRA+"'/></Order>");

					YFCDocument outDocgetOrderList = callGetOrderListWithLRA(sAssociatedLRA);
					YFCElement eleOrder = outDocgetOrderList.getDocumentElement().getElementsByTagName(TelstraConstants.ORDER).item(0);
					if(!YFCObject.isVoid(eleOrder)){
						String sParentOrderHeaderKey = eleOrder.getAttribute(TelstraConstants.ORDER_HEADER_KEY,"");				
						String sParentOrderNo = eleOrder.getAttribute(TelstraConstants.ORDER_NO,"");
						String sText2 = sParentOrderHeaderKey + "," + sParentOrderNo + "," + sAssociatedLRA;

						YFCElement yfcEleCustomAttributes = yfcDocChangeOrderIp.getDocumentElement().createChild(TelstraConstants.CUSTOM_ATTRIBUTES);
						yfcEleCustomAttributes.setAttribute(TelstraConstants.TEXT_2, sText2);
					}


					invokeYantraApi(TelstraConstants.API_CHANGE_ORDER, yfcDocChangeOrderIp);

					/*
					 * Update Parent LRA
					 */
					String sOrderNo = yfcEleOrder.getAttribute(TelstraConstants.ORDER_NO);
					YFCDocument yfcDoc = YFCDocument
							.getDocumentFor("<Order AssociatedLRA='" + sAssociatedLRA + "' LRA='" + sLRA
									+ "' OrderHeaderKey='" + sOrderHeaderKey + "' OrderNo='" + sOrderNo + "'/>");
					linkingAssociatedLraToNewLra(yfcDoc);
				}
			}			
		}		
	}

	/**
	 * The method will check if an Order Already Exists with the Order Name and throw an Exception if it does.
	 * @param sOrderName
	 */
	private boolean validateIfOrderExists(String sOrderName) {
		boolean bIsOrderAlreadyCreated = false;
		YFCDocument docOrderListinXml = YFCDocument.getDocumentFor("<Order OrderName='" + sOrderName + "' />");
		YFCDocument yfcDocGetOrderListTemp = YFCDocument.getDocumentFor("<OrderList TotalNumberOfRecords=''><OrderLine><Order OrderHeaderKey='' OrderNo=''><Extn LRA=''/></Order></OrderLine></OrderList>");
		LoggerUtil.verboseLog("CreateVectorAdhocLogisticsOrder :: callGetOrderListWithOrderName :: get order list Ip \n", logger,
				docOrderListinXml);

		yfcDocGetOrderListOp = invokeYantraApi(TelstraConstants.API_GET_ORDER_LIST,
				docOrderListinXml, yfcDocGetOrderListTemp);
		LoggerUtil.verboseLog("CreateVectorAdhocLogisticsOrder :: callGetOrderList :: yfcDocGetOrderListOp\n", logger,
				yfcDocGetOrderListOp);
		double dTotalNoOfOrder = yfcDocGetOrderListOp.getDocumentElement().getDoubleAttribute(TelstraConstants.TOTAL_NUMBER_OF_RECORDS,0.0);
		if(dTotalNoOfOrder > 0){
			/*	LoggerUtil.errorLog("Order Name Already Exists in the system", logger, " throwing exception");
			throw ExceptionUtil.getYFSException(
					TelstraErrorCodeConstants.VECTOR_ORDER_NAME_ALREADY_EXIST,
					new YFSException());*/
			bIsOrderAlreadyCreated = true;
		}
		return bIsOrderAlreadyCreated;
	}

	/**
	 * The below method will take care of searching the New LRA with the Old LRA.
	 * @param inDoc
	 */
	private void linkingAssociatedLraToNewLra(YFCDocument outconfirmShipmentxml) {

		YFCElement eleRoot = outconfirmShipmentxml.getDocumentElement();
		if(!YFCObject.isVoid(eleRoot)){
			String sAssociatedLRA = eleRoot.getAttribute("AssociatedLRA");
			if(!YFCCommon.isStringVoid(sAssociatedLRA)){
				YFCDocument outDocgetOrderList = callGetOrderListWithLRA(sAssociatedLRA);
				YFCElement eleOrder = outDocgetOrderList.getDocumentElement().getElementsByTagName(TelstraConstants.ORDER).item(0);
				if(!YFCObject.isVoid(eleOrder)){
					String sOrderHeaderKey = eleOrder.getAttribute(TelstraConstants.ORDER_HEADER_KEY,"");
					if(!YFCObject.isVoid(sOrderHeaderKey)){
						callChangeOrderToUpdateParentLra(sOrderHeaderKey,outconfirmShipmentxml);
					}
				}
			}
		}
	}

	/**
	 * Call change Order to update Associated LRA yfs_orderheader_extension table with the new LRA OrderHeaderKey, OrderNo And OrderName in the text2 column
	 * @param sOrderHeaderKey
	 * @param outconfirmShipmentxml
	 */
	private void callChangeOrderToUpdateParentLra(String sOrderHeaderKey,YFCDocument outconfirmShipmentxml) {
		String sNewLraOrderHeaderKey = outconfirmShipmentxml.getDocumentElement().getAttribute(TelstraConstants.ORDER_HEADER_KEY,"");
		String sNewLraOrderName = outconfirmShipmentxml.getDocumentElement().getAttribute(TelstraConstants.LRA,"");
		String sNewOrderNo = outconfirmShipmentxml.getDocumentElement().getAttribute(TelstraConstants.ORDER_NO,"");
		String sText2 = sNewLraOrderHeaderKey + "," + sNewOrderNo + "," + sNewLraOrderName;

		YFCDocument yfcDocChangeOrderIp = YFCDocument.getDocumentFor("<Order Override='Y' OrderHeaderKey='" + sOrderHeaderKey+ "'><CustomAttributes Text2='"+ sText2 +"'/></Order>");
		LoggerUtil.verboseLog("Input Xml to ChangeOrder Api :: ", logger,yfcDocChangeOrderIp.toString());

		invokeYantraApi(TelstraConstants.API_CHANGE_ORDER,yfcDocChangeOrderIp);
	}

	/**
	 * Calling getOrderList of the Assoicated LRA
	 * @param sLRA
	 * @return yfcDocGetOrderListOp
	 */
	private YFCDocument callGetOrderListWithLRA(String sLRA){

		YFCDocument yfcDocGetOrderListIp = YFCDocument
				.getDocumentFor("<Order EntryType='VECTOR'><Extn LRA='"+ sLRA +"' /></Order>");

		YFCDocument yfcDocGetOrderListTemp = YFCDocument.getDocumentFor(
				"<OrderList><Order EnterpriseCode='' DocumentType='' OrderHeaderKey='' OrderNo='' OrderName=''></Order></OrderList>");

		LoggerUtil.verboseLog("GpsManageVectorOrder :: callGetOrderListWithLRA :: yfcDocGetOrderListIp\n", logger,
				yfcDocGetOrderListIp);


		YFCDocument yfcDocGetOrderListOp = invokeYantraApi(TelstraConstants.API_GET_ORDER_LIST,
				yfcDocGetOrderListIp, yfcDocGetOrderListTemp);

		LoggerUtil.verboseLog("GpsManageVectorAdhocOrder :: callGetOrderListWithLRA :: yfcDocGetOrderListOp\n", logger,
				yfcDocGetOrderListOp);

		return yfcDocGetOrderListOp;

	}


	/**
	 * Creating a Sales Order and shipment and moving it to shippped status.
	 * @param inXml
	 * @return outconfirmShipmentxml
	 */
	private YFCDocument createOrder(YFCDocument inXml) {
		YFCDocument outconfirmShipmentxml=null;
		String sLRA="";
		String sAssociatedLRA="";
		if(YFCObject.equals(inXml.getDocumentElement().getAttribute(TelstraConstants.ORDER_TYPE),"ADHOC_LOGISTICS")){

			//The assumption here is First Extn is Order Extn 
			YFCElement eleOrderExtn = inXml.getDocumentElement().getChildElement(TelstraConstants.EXTN);
			if(!YFCObject.isVoid(eleOrderExtn)){
				sAssociatedLRA = eleOrderExtn.getAttribute("AssociatedLRA","");
				sLRA = eleOrderExtn.getAttribute(TelstraConstants.LRA,"");
				if(!YFCObject.isVoid(sAssociatedLRA)){
					String sNewLRA = sLRA + ",AssociatedLRA:" + sAssociatedLRA;
					LoggerUtil.verboseLog("NewLRA value :: ", logger,sNewLRA);
					eleOrderExtn.setAttribute(TelstraConstants.LRA, sNewLRA);
					eleOrderExtn.removeAttribute("AssociatedLRA");
					LoggerUtil.verboseLog("Removed the Associated LRA from Input Xml ", logger,sNewLRA);

				}
			}
			/*
			 * linkParentLRA
			 */
			YFCDocument outDocgetOrderList = callGetOrderListWithLRA(sAssociatedLRA);
			YFCElement eleOrder = outDocgetOrderList.getDocumentElement().getElementsByTagName(TelstraConstants.ORDER).item(0);
			if(!YFCObject.isVoid(eleOrder)){
				String sParentOrderHeaderKey = eleOrder.getAttribute(TelstraConstants.ORDER_HEADER_KEY,"");				
				String sParentOrderNo = eleOrder.getAttribute(TelstraConstants.ORDER_NO,"");
				String sText2 = sParentOrderHeaderKey + "," + sParentOrderNo + "," + sAssociatedLRA;

				YFCElement yfcEleCustomAttributes = inXml.getDocumentElement().createChild(TelstraConstants.CUSTOM_ATTRIBUTES);
				yfcEleCustomAttributes.setAttribute(TelstraConstants.TEXT_2, sText2);
			}

			YFCElement eleOrderLine=inXml.getDocumentElement().getElementsByTagName(TelstraConstants.ORDER_LINE).item(0);
			if(!YFCObject.isNull(eleOrderLine)){

				YFCDocument doctempOrderListinXml = YFCDocument.getDocumentFor("<Order EnterpriseCode='' OrderNo='' DocumentType='' OrderHeaderKey='' ><OrderLines><OrderLine Status='' OrderLineKey='' PrimeLineNo='' /></OrderLines></Order>");
				LoggerUtil.verboseLog("Input Xml to CreateOrder Api :: ", logger,inXml.toString());
				YFCDocument outxml = invokeYantraApi("createOrder", inXml,doctempOrderListinXml);


				/* The changes below invoke a class to update the Order header extension table with the boolean values only 
				 * for Adhoc Order Creation*/

				GpsUpdateOrderExtnFields obj = new GpsUpdateOrderExtnFields();
				String sOrderNo = outxml.getDocumentElement().getAttribute(TelstraConstants.ORDER_NO);
				obj.updateOrderExtnFields(YFCDocument.getDocumentFor("<Order OrderNo='" + sOrderNo + "' />"),getServiceInvoker());

				//HUB-9188 [END]

				String sOrderHeaderKey=outxml.getDocumentElement().getAttribute(TelstraConstants.ORDER_HEADER_KEY,"");

				//Schedule and Release the Order
				String sOrderReleaseKey =scheduleAndReleaseOrder(sOrderHeaderKey);

				// Method to Create and Confirm Shipment with the container details and drop to Queue
				outconfirmShipmentxml= createAndConfirmShipment(inXml,sOrderNo,sOrderHeaderKey,sOrderReleaseKey);
				YFCElement eleShipment = outconfirmShipmentxml.getDocumentElement();
				eleShipment.setAttribute(TelstraConstants.ORDER_NO, sOrderNo);
				eleShipment.setAttribute(TelstraConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
				eleShipment.setAttribute(TelstraConstants.LRA, sLRA);
				eleShipment.setAttribute("AssociatedLRA", sAssociatedLRA);

			}

		}
		return outconfirmShipmentxml;
	}


	/**
	 * The method will create the Shipment for the SO and move it to shipped status
	 * @param inXml
	 * @param sOrderNo
	 * @param sOrderHeaderKey
	 * @param sOrderReleaseKey
	 * @return outcreateShipmentxml
	 */
	private YFCDocument createAndConfirmShipment(YFCDocument inXml,
			String sOrderNo, String sOrderHeaderKey, String sOrderReleaseKey) {
		String sCarrier="";
		String sService="";
		String sPriorityCode="";
		String sTrackingNo="";

		YFCElement eleOrderLine = inXml.getDocumentElement().getElementsByTagName(TelstraConstants.ORDER_LINE).item(0);
		YFCElement eleExtnOrderLine = eleOrderLine.getChildElement(TelstraConstants.EXTN);
		if(YFCObject.isVoid(eleExtnOrderLine)) {
			sPriorityCode=eleExtnOrderLine.getAttribute("PriorityCode","");
		}

		//HUB-9431[START]
		/*Reading the Tracking No from the Refernce level and stamping on the Shipment*/

		YFCElement eleReferences = inXml.getElementsByTagName("References").item(0);
		if(!YFCObject.isVoid(eleReferences)){
			for(YFCElement eleReference : eleReferences.getElementsByTagName(TelstraConstants.REFERENCE)){
				if(YFCObject.equals("TRACKING_NO", eleReference.getAttribute(TelstraConstants.NAME,""))){
					sTrackingNo= eleReference.getAttribute(TelstraConstants.VALUE,"");
					break;
				}
			}
		}

		//HUB-9431[END]


		//Priority based  SCAC and service is read from the common Code List
		YFCDocument docGetCommonCodeListOutput = invokeYantraApi(TelstraConstants.API_GET_COMMON_CODE_LIST,YFCDocument.getDocumentFor("<CommonCode CodeType=\"LOGISTICS_PRIORITY\" CodeValue='" + sPriorityCode + "'/>"));
		LoggerUtil.verboseLog("Output Xml of CommonCodeList Api :: ", logger,docGetCommonCodeListOutput.toString());
		if(!YFCObject.isNull(docGetCommonCodeListOutput.getElementsByTagName(TelstraConstants.COMMON_CODE).item(0).getAttribute(TelstraConstants.CODE_VALUE))){
			sCarrier=docGetCommonCodeListOutput.getDocumentElement().getElementsByTagName(TelstraConstants.COMMON_CODE).item(0).getAttribute("CodeShortDescription","");
			sService=docGetCommonCodeListOutput.getDocumentElement().getElementsByTagName(TelstraConstants.COMMON_CODE).item(0).getAttribute("CodeLongDescription","");
		}	    

		YFCDocument docCreateShipment = YFCDocument.getDocumentFor("<Shipment />");
		YFCElement eleShipment=docCreateShipment.getDocumentElement();
		eleShipment.setAttribute(TelstraConstants.ORDER_NO, sOrderNo);
		eleShipment.setAttribute(TelstraConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
		eleShipment.setAttribute("SCAC", sCarrier);
		eleShipment.setAttribute("CarrierServiceCode", sService);

		//HUB-9431[START]
		eleShipment.setAttribute(TelstraConstants.TRACKING_NO, sTrackingNo);
		//HUB-9431[END]

		eleShipment.createChild(TelstraConstants.ORDER_RELEASES).createChild(TelstraConstants.ORDER_RELEASE).setAttribute(TelstraConstants.ORDER_RELEASE_KEY, sOrderReleaseKey);
		eleShipment.createChild(TelstraConstants.EXTN).setAttribute("LogisticsStatus", "PENDING");

		//Stamping the SHIP_TO Address

		/*Changed PersonInfoShipTo as the ToAddress*/
		YFCElement elePersonInfoShipTo = inXml.getDocumentElement().getElementsByTagName(TelstraConstants.PERSON_INFO_SHIP_TO).item(0);
		eleShipment.appendChild(docCreateShipment.importNode(elePersonInfoShipTo,true));
		docCreateShipment.getDocument().renameNode(eleShipment.getChildElement(TelstraConstants.PERSON_INFO_SHIP_TO).getDOMNode(), null, "ToAddress");

		//Stamping the SHIP_FROM Address as the FromAddress where the address type is SHIP_FROM
		YFCElement eleAdditionalAddresses = inXml.getElementsByTagName(TelstraConstants.ADDITIONAL_ADDRESSES).item(0);
		for(YFCElement eleAdditionalAddress : eleAdditionalAddresses.getElementsByTagName(TelstraConstants.ADDITIONAL_ADDRESS)){
			if(YFCObject.equals("SHIP_FROM", eleAdditionalAddress.getAttribute("AddressType",""))){
				YFCElement elePersonInfo= eleAdditionalAddress.getElementsByTagName(TelstraConstants.PERSON_INFO).item(0);
				eleShipment.appendChild(docCreateShipment.importNode(elePersonInfo,true));
				docCreateShipment.getDocument().renameNode(eleShipment.getChildElement(TelstraConstants.PERSON_INFO).getDOMNode(), null, "FromAddress");
				break;
			}
		}



		LoggerUtil.verboseLog("Input Xml of createShipment Api :: ", logger,docCreateShipment.toString());
		YFCDocument outcreateShipmentxml = invokeYantraApi("createShipment", docCreateShipment, getCreateShipmentTemplate());
		LoggerUtil.verboseLog("Output Xml of createShipment Api :: ", logger,outcreateShipmentxml.toString());
		invokeYantraApi("confirmShipment", outcreateShipmentxml);

		return outcreateShipmentxml;
	}

	/**
	 * Scheduling and releasing the created Order
	 * @param sOrderHeaderKey
	 * @return sOrderReleaseKey
	 */
	private String scheduleAndReleaseOrder(String sOrderHeaderKey) {
		String sOrderReleaseKey=null;
		YFCDocument docInputscheduleXml=YFCDocument.getDocumentFor("<ScheduleOrder ScheduleAndRelease='Y' CheckInventory='N' IgnoreReleaseDate='Y' OrderHeaderKey='" + sOrderHeaderKey + "' />");
		invokeYantraApi("scheduleOrder", docInputscheduleXml);
		YFCDocument docOrderReleaseListoutXml = getOrderReleaseList(sOrderHeaderKey);
		LoggerUtil.verboseLog("Output Xml of getOrderReleaseList Api :: ", logger,docOrderReleaseListoutXml.toString());
		YFCElement eleReleaseorderlist=docOrderReleaseListoutXml.getDocumentElement();
		if(!YFCObject.isNull(eleReleaseorderlist.getElementsByTagName(TelstraConstants.ORDER_RELEASE).item(0).getAttribute(TelstraConstants.ORDER_HEADER_KEY))){
			sOrderReleaseKey=eleReleaseorderlist.getAttribute("LastOrderReleaseKey");
		}
		return sOrderReleaseKey;
	}

	/**
	 * Retrieving the releaseKey for the Order
	 * @param sOrderHeaderKey
	 * @return docOrderListOutXml
	 */
	private YFCDocument getOrderReleaseList(String sOrderHeaderKey) {
		YFCDocument docOrderListinXml = YFCDocument.getDocumentFor("<OrderRelease OrderHeaderKey='"+ sOrderHeaderKey+ "' />");
		YFCDocument doctempgetOrderListinXml = YFCDocument.getDocumentFor("<OrderReleaseList LastOrderReleaseKey='' TotalNumberOfRecords=''><OrderRelease  "
				+ "EnterpriseCode='' DocumentType='' OrderHeaderKey='' OrderNo='' /></OrderReleaseList>");
		return invokeYantraApi("getOrderReleaseList", docOrderListinXml,doctempgetOrderListinXml);
	}

	/**
	 * Create shipment Api template
	 * @return createShipmentTemplate
	 */
	private YFCDocument getCreateShipmentTemplate() {
		YFCDocument createShipmentTemplate = YFCDocument.getDocumentFor("<Shipment SellerOrganizationCode='' ShipNode='' ShipmentKey='' ShipmentNo='' TrackingNo='' />");
		return createShipmentTemplate;
	}
}

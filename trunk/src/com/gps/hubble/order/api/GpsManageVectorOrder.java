/***********************************************************************************************
 * File	Name		: GpsManageVectorOrder.java
 *
 * Description		: This class is called from ManageOrder.java
 * 					The purpose of this class is to create and manage Vector order
 * 
 * Modification	Log	:
 * -------------------------------------------------------------------------------------------------------------------------------------
 * Ver #	Date			Author					Modification
 * 
 * 
 * -------------------------------------------------------------------------------------------------------------------------------------
 * 1.0		Mar 27,2017	  	Prateek Kumar 		   	Initial	Version 
 * 1.1		May 02,2017		Prateek Kumar			HUB-8731: If MR is in released status, back ordering the order before updating the address and then moving it to created status
 * 1.2		May 10,2017		Prateek Kumar			Zip code validation for Standard vector order
 * 1.3		May 16,2017		Prateek Kumar			HUB-9069 throwing a valid exception if the cancellation comes for the new line and consuming the cancellation message if the line is already cancelled
 * 1.4		May 18,2017		Prateek Kumar			HUB-9067 Taking into account multiple release scenario while updating MR addresses
 * 1.5		Jun 06,2017		Prateek Kumar			HUB-9078: If the OrderName from Vector exists in the system and Source System for the order is Intergral plus, ignore the message.
 * 1.6		Jun 08,2017		Prateek Kumar			HUB-9293
 * 1.7		Jun19,2017		Prateek Kumar			Update product type from the common code long description
 * -------------------------------------------------------------------------------------------------------------------------------------
 * 
 */
package com.gps.hubble.order.api;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.bridge.sterling.framework.service.ServiceInvoker;
import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.constants.TelstraErrorCodeConstants;
import com.gps.hubble.utils.GpsManageOrderUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSException;

/**
 * Sample xml
 * 
 * @author Prateek
 *
 */
public class GpsManageVectorOrder {

	private static YFCLogCategory logger = YFCLogCategory.instance(GpsManageVectorOrder.class);

	enum ProductType{
		COMBINATION,CUSTOMIZED,STANDARD;
	}

	private ServiceInvoker serviceInvoker;
	private YFCDocument yfcInDoc;
	private String sOrderHeaderKey = "";

	/**
	 * 
	 * @param yfcInDoc
	 * @param serviceInvoker
	 * @return yfcInDoc
	 */

	public YFCDocument manageVectorOrder(YFCDocument yfcInDoc, ServiceInvoker serviceInvoker) {

		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", yfcInDoc);

		this.serviceInvoker = serviceInvoker;
		this.yfcInDoc = yfcInDoc;
		boolean isItemDefaulted = false;

		/*
		 * move the lra details from order extn to order line hangoff
		 */
		YFCElement yfcEleExtn = this.yfcInDoc.getDocumentElement().getChildElement(TelstraConstants.EXTN);
		if (!YFCCommon.isVoid(yfcEleExtn) && !YFCCommon.isStringVoid(yfcEleExtn.getAttribute(TelstraConstants.LRA))) {
			moveLraToOrderLineHangoff(yfcEleExtn);
		}
		/*
		 * This method update the order with primeLineNo
		 */
		YFCDocument yfcDocGetOrderListOp = updateOrderWithLineNo();
		/*
		 * Based on the product type, below method will update order with the change status Indicator
		 */
		updateOrderWithChangeStatusIndicator();
		YFCElement yfcEleItem = XPathUtil.getXPathElement(this.yfcInDoc, "//OrderLine/Item");

		if (!YFCCommon.isVoid(yfcEleItem)) {
			String sItemID = yfcEleItem.getAttribute(TelstraConstants.ITEM_ID);
			/*
			 * > will come for service type MANAGED_STORAGE 
			 */
			if(sItemID.contains(">")){
				String arrayItemID[] = sItemID.split("\\>");

				if(YFCCommon.isStringVoid(arrayItemID[0])){
					yfcEleItem.setAttribute(TelstraConstants.ITEM_ID, "STANDARD");
					yfcEleItem.setAttribute(TelstraConstants.UNIT_OF_MEASURE, "EA");
					yfcEleItem.setAttribute(TelstraConstants.ITEM_SHORT_DESC, "STANDARD INVENTORY");
					isItemDefaulted = true;
				}
				else{
					yfcEleItem.setAttribute(TelstraConstants.ITEM_ID, arrayItemID[0]);
				}				
				String sPutAwaySku = arrayItemID[1];
				YFCElement yfcEleCustomAttributes = (this.yfcInDoc.getDocumentElement()).createChild(TelstraConstants.CUSTOM_ATTRIBUTES);
				yfcEleCustomAttributes.setAttribute(TelstraConstants.PUT_AWAY_SKU, sPutAwaySku);
			}			
		} else{
			/*
			 * For Standard  product type
			 */
			YFCElement yfcEleOrderLine = XPathUtil.getXPathElement(this.yfcInDoc, "//OrderLine");
			yfcEleItem = yfcEleOrderLine.createChild(TelstraConstants.ITEM);
			yfcEleItem.setAttribute(TelstraConstants.ITEM_ID, "STANDARD");
			yfcEleItem.setAttribute(TelstraConstants.UNIT_OF_MEASURE, "EA");
			yfcEleItem.setAttribute(TelstraConstants.ITEM_SHORT_DESC, "STANDARD INVENTORY");
			isItemDefaulted = true;
		}

		/*
		 * Below method will replace the Vector Item ID and UOM with the
		 * Integral Plus
		 */
		if(!isItemDefaulted){
			updateOrderWithIpItemID();
		}
		YFCElement yfcEleOrderRoot = this.yfcInDoc.getDocumentElement();
		/*
		 * This method reads the incoming product type and if it exist in LRA_PRODUCT_TYPE common code,
		 * it overrides the incoming product type with the common code long description.
		 */
		updateProductTypeFromCommonCode(yfcEleOrderRoot);
		
		String sOrderName = yfcEleOrderRoot.getAttribute(TelstraConstants.ORDER_NAME);
		/*
		 * call get order list to get order header key from the order
		 */

		if (!YFCCommon.isStringVoid(sOrderName)) {
			YFCDocument yfcDocGetOrderListOpWithOrderName = callGetOrderListWithOrderName(sOrderName);

			String sEntryType = XPathUtil.getXpathAttribute(yfcDocGetOrderListOpWithOrderName, "//Order/@EntryType");
			if(TelstraConstants.INTEGRAL_PLUS.equals(sEntryType)){
				LoggerUtil.verboseLog("Returning from GpsManageVectorOrder as order has been created from Integral Plus", logger, null);
				return yfcInDoc;
			}

			sOrderHeaderKey = XPathUtil.getXpathAttribute(yfcDocGetOrderListOpWithOrderName, "//Order/@OrderHeaderKey");
			if(!YFCCommon.isStringVoid(sOrderHeaderKey)){
				/*
				 * Append action = "Create" for the new line. It is required for date computation in before create order ue
				 */
				appendActionCreateForNewLine(yfcDocGetOrderListOpWithOrderName);
				yfcEleOrderRoot.setAttribute(TelstraConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
			}
		}

		String sIncludeReservation = yfcEleOrderRoot.getAttribute(TelstraConstants.INCLUDE_RESERVATIONS);
		if (TelstraConstants.YES.equalsIgnoreCase(sIncludeReservation)) {
			serviceInvoker.invokeYantraService(TelstraConstants.GPS_UPDATE_VECTOR_LRA_DROP_Q, this.yfcInDoc);

		}
		/*
		 * If change status is not cancel, then only proceed forward. In case change status is cancel, 
		 * no further processing is required as order has already been cancelled. 
		 */
		YFCDocument yfcDocApiOp = null;
		if (YFCCommon.isStringVoid(sOrderHeaderKey)) {
			/*
			 * Order creation 
			 */
			yfcInDoc.getDocumentElement().setAttribute(TelstraConstants.ACTION, "Create");
			LoggerUtil.verboseLog("GpsManageVectorOrder :: manageVectorOrder :: Create order Ip \n", logger,
					yfcInDoc);
			YFCDocument yfcDocCreateOrderTemp = YFCDocument.getDocumentFor("<Order OrderNo='' OrderHeaderKey='' OrderName=''/>");
			yfcDocApiOp = serviceInvoker.invokeYantraApi(TelstraConstants.API_CREATE_ORDER, yfcInDoc, yfcDocCreateOrderTemp);						
		} else {
			yfcDocApiOp = processChangeOrder(yfcDocGetOrderListOp);
		}
		
		/*
		 * Adding parent LRA order number and order header key to the material reservation line which will part of this LRA
		 */
		if(!YFCCommon.isVoid(yfcDocApiOp) && TelstraConstants.YES.equalsIgnoreCase(sIncludeReservation)){
			YFCDocument yfcDocLra = YFCDocument.createDocument(TelstraConstants.LRA);
			yfcDocLra.getDocumentElement().setAttributes(yfcDocApiOp.getDocumentElement().getAttributes());		
			serviceInvoker.invokeYantraService(TelstraConstants.GPS_UPDATE_VECTOR_LRA_DROP_Q, yfcDocLra);
		}
		GpsUpdateOrderExtnFields obj = new GpsUpdateOrderExtnFields();
		obj.updateOrderExtnFields(YFCDocument.getDocumentFor("<Order OrderName='" + sOrderName + "' />"),serviceInvoker);
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", this.yfcInDoc);

		return yfcInDoc;
	}

	/**
	 * 
	 * @param yfcEleOrderRoot
	 */
	private void updateProductTypeFromCommonCode(YFCElement yfcEleOrderRoot) {

		YFCElement yfcEleLRA = XPathUtil.getXPathElement(yfcInDoc, "//LRA");
		if(!YFCCommon.isVoid(yfcEleLRA)){
			String sProductType = yfcEleLRA.getAttribute(TelstraConstants.PRODUCT_TYPE);
			if(!YFCCommon.isStringVoid(sProductType)){
				String sProdTypeFromCommonCode = callCommonCode("LRA_PRODUCT_TYPE",sProductType);	
				if(!YFCCommon.isStringVoid(sProdTypeFromCommonCode)){
					yfcEleLRA.setAttribute(TelstraConstants.PRODUCT_TYPE, sProdTypeFromCommonCode);
				}
			}
		}
	}

	/**
	 * 
	 * @param sCodeType
	 * @param sCodeValue
	 * @return
	 */
	private String callCommonCode(String sCodeType, String sCodeValue) {

		YFCDocument yfcDocCommonCodeListIp = YFCDocument.getDocumentFor("<CommonCode CodeType='"+sCodeType+"' CodeValue='"+sCodeValue+"'/>");
		YFCDocument yfcDocCommonCodeListTemp = YFCDocument.getDocumentFor("<CommonCodeList><CommonCode CodeType='' CodeValue='' CodeLongDescription=''/></CommonCodeList>");
		YFCDocument yfcDocCommonCodeListOp = serviceInvoker.invokeYantraApi(TelstraConstants.API_GET_COMMON_CODE_LIST, yfcDocCommonCodeListIp, yfcDocCommonCodeListTemp);

		String sLongDesc = "";
		YFCElement yfcEleCommonCode = yfcDocCommonCodeListOp.getElementsByTagName(TelstraConstants.COMMON_CODE).item(0);
		if(!YFCCommon.isVoid(yfcEleCommonCode)){
			sLongDesc = yfcEleCommonCode.getAttribute(TelstraConstants.CODE_LONG_DESCRIPTION);
		}
		return sLongDesc;
	}

	/**
	 * This method move the lra details from Order/Extn to //OrderLine/LRAList/LRA element
	 * @param yfcEleExtn2 
	 */
	private void moveLraToOrderLineHangoff(YFCElement yfcEleExtn) {

		YFCElement yfcEleOrderLine =  yfcInDoc.getElementsByTagName(TelstraConstants.ORDER_LINE).item(0);
		YFCElement yfcEleOLExtn = yfcEleOrderLine.getChildElement(TelstraConstants.EXTN);
		if(YFCCommon.isVoid(yfcEleOLExtn)){
			yfcEleOLExtn = yfcEleOrderLine.createChild(TelstraConstants.EXTN);
		}
		YFCElement yfcEleLraList = yfcEleOLExtn.createChild(TelstraConstants.LRA_LIST);
		YFCElement yfcEleLra = yfcEleLraList.createChild(TelstraConstants.LRA);

		yfcEleLra.setAttribute(TelstraConstants.LRA_ID, yfcEleExtn.getAttribute(TelstraConstants.LRA));
		yfcEleLra.setAttribute(TelstraConstants.LRA_SERVICE, yfcEleExtn.getAttribute(TelstraConstants.SERVICE));
		yfcEleLra.setAttribute(TelstraConstants.PRODUCT_TYPE, yfcEleExtn.getAttribute(TelstraConstants.PRODUCT_TYPE));
		yfcEleExtn.setAttribute(TelstraConstants.LRA_SERVICE, yfcEleExtn.getAttribute(TelstraConstants.SERVICE));
		yfcEleExtn.removeAttribute(TelstraConstants.SERVICE);
		yfcEleExtn.removeAttribute(TelstraConstants.PRODUCT_TYPE);

	}

	/**
	 * 
	 */
	private void updateOrderWithChangeStatusIndicator() {

		String sProductType = XPathUtil.getXpathAttribute(yfcInDoc, "//OrderLine/Extn/LRAList/LRA/@ProductType");
		String sChangeStatus = XPathUtil.getXpathAttribute(yfcInDoc, "//Order/OrderLines/OrderLine/@ChangeStatus");
		if ((ProductType.COMBINATION.toString().equals(sProductType)
				|| ProductType.STANDARD.toString().equals(sProductType))
				&& TelstraConstants.CHANGE_STATUS_CANCELLED.equalsIgnoreCase(sChangeStatus)) {
			yfcInDoc.getDocumentElement().setAttribute(TelstraConstants.CHANGE_STATUS, TelstraConstants.CHANGE_STATUS_CANCELLED);
		}		
	}

	/**
	 * This method update the order with the order line no 
	 */
	private YFCDocument updateOrderWithLineNo() {

		List<String> listVectorId = new ArrayList<>();
		/*
		 * preparing the set of the vector id
		 */
		for(YFCElement yfcEleOrderLine : yfcInDoc.getElementsByTagName(TelstraConstants.ORDER_LINE)){

			YFCElement yfcEleExtn = yfcEleOrderLine.getChildElement(TelstraConstants.EXTN);
			if(!YFCCommon.isVoid(yfcEleExtn)){

				String sVectorId = yfcEleExtn.getAttribute(TelstraConstants.VECTOR_ID);
				if(!YFCCommon.isStringVoid(sVectorId)){
					listVectorId.add(sVectorId);
				}
			}
		}
		/*
		 * fetch the map with vector id as key and prime line no as value
		 */
		GpsManageOrderUtil obj = new GpsManageOrderUtil();
		Map<String, String> mapVectorIdPrimeLineNo = obj.getVectorIdLineNoMap(listVectorId, serviceInvoker);
		YFCDocument yfcDocGetOrderListOp = obj.getYfcDocGetOrderListOp();

		/*
		 * update order with the prime line no from the map
		 */
		for(YFCElement yfcEleOrderLine : yfcInDoc.getElementsByTagName(TelstraConstants.ORDER_LINE)){

			YFCElement yfcEleExtn = yfcEleOrderLine.getChildElement(TelstraConstants.EXTN);
			if(!YFCCommon.isVoid(yfcEleExtn)){

				String sVectorId = yfcEleExtn.getAttribute(TelstraConstants.VECTOR_ID);
				if(!YFCCommon.isStringVoid(sVectorId)){

					String sPrimeLineNo = mapVectorIdPrimeLineNo.get(sVectorId);
					if(!YFCCommon.isStringVoid(sPrimeLineNo)){
						yfcEleOrderLine.setAttribute(TelstraConstants.PRIME_LINE_NO, sPrimeLineNo);
						yfcEleOrderLine.setAttribute(TelstraConstants.SUB_LINE_NO, "1");
					}
				}
			}
		}
		return yfcDocGetOrderListOp;
	}

	/**
	 * This method update the new order line with action = "CREATE"
	 * @param yfcDocGetOrderListOp
	 */
	private void appendActionCreateForNewLine(YFCDocument yfcDocGetOrderListOp) {

		YFCNodeList<YFCElement> yfcNlOrderLine = yfcInDoc.getElementsByTagName(TelstraConstants.ORDER_LINE);
		for(YFCElement yfcEleOrderLine : yfcNlOrderLine){

			String sPrimeLineNo = yfcEleOrderLine.getAttribute(TelstraConstants.PRIME_LINE_NO);
			YFCElement yfcEleOrderLineInOrderList = XPathUtil.getXPathElement(yfcDocGetOrderListOp, "//OrderLine[@PrimeLineNo='"+sPrimeLineNo+"']");
			if(YFCCommon.isVoid(yfcEleOrderLineInOrderList)){
				yfcEleOrderLine.setAttribute(TelstraConstants.ACTION, "CREATE");
			}			
		}		
	}


	/**
	 * This method replace the item id and UOM in the incoming message with the
	 * Integral Plus Item and UOM
	 */
	private void updateOrderWithIpItemID() {

		YFCElement yfcEleOrder = yfcInDoc.getDocumentElement();
		String sDeptCode = yfcEleOrder.getAttribute(TelstraConstants.DEPARTMENT_CODE);
		YFCElement yfcEleItem = yfcEleOrder.getElementsByTagName(TelstraConstants.ITEM).item(0);
		String sUnitPrice="";
		if("PMC".equalsIgnoreCase(sDeptCode)){
			updateItemForPmcOrder(yfcEleOrder.getElementsByTagName(TelstraConstants.ORDER_LINE).item(0));
		}
		else{
			sUnitPrice = updateItemForNonPmcOrder(yfcEleItem);
		}
		if(!YFCCommon.isStringVoid(sUnitPrice)){
			YFCElement yfcEleOrderLine = yfcEleOrder.getElementsByTagName(TelstraConstants.ORDER_LINE).item(0);
			YFCElement yfcEleLinePriceInfo = yfcEleOrderLine.createChild(TelstraConstants.LINE_PRICE_INFO);
			yfcEleLinePriceInfo.setAttribute(TelstraConstants.UNIT_PRICE, sUnitPrice);
			yfcEleLinePriceInfo.setAttribute(TelstraConstants.IS_PRICE_LOCKED, TelstraConstants.YES);
		}
	}

	/**
	 * 
	 * @param yfcEleItem
	 * @return 
	 */
	private void updateItemForPmcOrder(YFCElement yfcEleOrderLineIp) {

		YFCElement yfcEleItemIp = yfcEleOrderLineIp.getChildElement(TelstraConstants.ITEM);
		String sItemId = yfcEleItemIp.getAttribute(TelstraConstants.ITEM_ID);		
		YFCDocument yfcDocGetOrderLineListIp = YFCDocument
				.getDocumentFor("<OrderLine><Item  ItemID='" + sItemId + "'/><Order DocumentType='0005'/></OrderLine>");
		LoggerUtil.verboseLog("GpsManageVectorOrder :: updateItemForPmcOrder :: yfcDocGetOrderLineListIp \n", logger,
				yfcDocGetOrderLineListIp);
		YFCDocument yfcDocGetOrderLineListTemp = YFCDocument.getDocumentFor(
				"<OrderLineList><OrderLine OrderLineKey='' PrimeLineNo=''> <Item ItemID='' ItemShortDesc='' ItemDesc='' SupplierItem='' UnitOfMeasure=''/> <Order DocumentType='' OrderName='' OrderHeaderKey='' OrderNo=''/> </OrderLine> </OrderLineList>");

		YFCDocument yfcDocGetOrderLineListOp = serviceInvoker.invokeYantraApi(TelstraConstants.API_GET_ORDER_LINE_LIST, yfcDocGetOrderLineListIp, yfcDocGetOrderLineListTemp);
		LoggerUtil.verboseLog("GpsManageVectorOrder :: updateItemForPmcOrder :: yfcDocGetOrderLineListOp \n", logger,
				yfcDocGetOrderLineListOp);

		YFCNodeList<YFCElement> yfcNlOrderLine = yfcDocGetOrderLineListOp.getElementsByTagName(TelstraConstants.ORDER_LINE);

		YFCElement yfcEleOrderLine = null;
		if(yfcNlOrderLine.getLength()==1){			
			yfcEleOrderLine = yfcDocGetOrderLineListOp.getElementsByTagName(TelstraConstants.ORDER_LINE).item(0);
		}
		else if (yfcNlOrderLine.getLength()>1){
			String sLatestOrderLineKey = getLatestOrderLine(yfcNlOrderLine);
			yfcEleOrderLine = XPathUtil.getXPathElement(yfcDocGetOrderLineListOp, "//OrderLine[@OrderLineKey='"+sLatestOrderLineKey+"']");			
		}
		
		if(!YFCCommon.isVoid(yfcEleOrderLine)){
			
			String sPrimeLineNo = yfcEleOrderLine.getAttribute(TelstraConstants.PRIME_LINE_NO);
			YFCElement yfcEleOrder = yfcEleOrderLine.getChildElement(TelstraConstants.ORDER);
			String sOrderNo = yfcEleOrder.getAttribute(TelstraConstants.ORDER_NO);
			String sOrderHeaderKey = yfcEleOrder.getAttribute(TelstraConstants.ORDER_HEADER_KEY);
			String sOrderName = yfcEleOrder.getAttribute(TelstraConstants.ORDER_NAME);
			
			yfcEleOrderLineIp.setAttribute(TelstraConstants.CUSTOMER_PO_NO, sOrderHeaderKey+ "," +sOrderNo+ "," +sOrderName);
			yfcEleOrderLineIp.setAttribute(TelstraConstants.CUSTOMER_PO_LINE_NO, sPrimeLineNo);


			YFCElement yfcEleItemLineList = yfcEleOrderLine.getChildElement(TelstraConstants.ITEM);
			yfcEleItemIp.setAttribute(TelstraConstants.ITEM_ID, yfcEleItemLineList.getAttribute(TelstraConstants.ITEM_ID));
			yfcEleItemIp.setAttribute(TelstraConstants.ITEM_SHORT_DESC, yfcEleItemLineList.getAttribute(TelstraConstants.ITEM_SHORT_DESC));
			yfcEleItemIp.setAttribute(TelstraConstants.ITEM_DESC, yfcEleItemLineList.getAttribute(TelstraConstants.ITEM_DESC));
			yfcEleItemIp.setAttribute(TelstraConstants.SUPPLIER_ITEM, yfcEleItemLineList.getAttribute(TelstraConstants.SUPPLIER_ITEM));
			yfcEleItemIp.setAttribute(TelstraConstants.UNIT_OF_MEASURE, yfcEleItemLineList.getAttribute(TelstraConstants.UNIT_OF_MEASURE));			
		}		
	}

	/**
	 * 
	 * @param yfcNlOrderLine
	 * @return
	 */
	private String getLatestOrderLine(YFCNodeList<YFCElement> yfcNlOrderLine) {

		List<String> lOrderLineKey = new ArrayList<>();
		for(YFCElement yfcEleOrderLine : yfcNlOrderLine){			
			String sOrderLineKey = yfcEleOrderLine.getAttribute(TelstraConstants.ORDER_LINE_KEY);
			lOrderLineKey.add(sOrderLineKey);
		}
		Collections.reverse(lOrderLineKey);

		return lOrderLineKey.get(0);
	}

	/**
	 * 
	 * @param yfcEleItem
	 * @return 
	 */
	private String updateItemForNonPmcOrder(YFCElement yfcEleItem) {

		YFCDocument yfcDocGetItemListOp = callGetItemList(yfcEleItem.getAttribute(TelstraConstants.ITEM_ID),"VECTOR");
		YFCElement yfcEleItemItemList = yfcDocGetItemListOp.getElementsByTagName(TelstraConstants.ITEM).item(0);
		
		String sIntegralPlusItem = yfcEleItem.getAttribute(TelstraConstants.INTEGRAL_ITEM_ID);
		/*
		 * If integral plus item id is present use that a item id, get unit of measure from get item list op and use passed item id
		 * as supplier item
		 */
		if(!YFCCommon.isStringVoid(sIntegralPlusItem)){

			yfcEleItem.setAttribute(TelstraConstants.SUPPLIER_ITEM, yfcEleItem.getAttribute(TelstraConstants.ITEM_ID));
			yfcEleItem.setAttribute(TelstraConstants.ITEM_ID, sIntegralPlusItem);

			YFCDocument yfcDocGetItemListOpIp = callGetItemList(sIntegralPlusItem,"");
			YFCElement yfcEleItemItemListIp = yfcDocGetItemListOpIp.getElementsByTagName(TelstraConstants.ITEM).item(0);
			if(!YFCCommon.isVoid(yfcEleItemItemListIp)){
				String sUoM = yfcEleItemItemListIp.getAttribute(TelstraConstants.UNIT_OF_MEASURE);
				yfcEleItem.setAttribute(TelstraConstants.UNIT_OF_MEASURE, sUoM);
			}
		}
		else{
			String sItemID = yfcEleItem.getAttribute(TelstraConstants.ITEM_ID);
			String sUnitOfMeasure = yfcEleItem.getAttribute(TelstraConstants.UNIT_OF_MEASURE);
			YFCDocument yfcDocGetItemAsscocationOp = callGetItemAssociation(sItemID, sUnitOfMeasure);
			YFCElement yfcEleItemItemAssoc = yfcDocGetItemAsscocationOp.getElementsByTagName(TelstraConstants.ITEM).item(0);

			if(!YFCCommon.isVoid(yfcEleItemItemAssoc)){
				yfcEleItem.setAttribute(TelstraConstants.ITEM_ID, yfcEleItemItemAssoc.getAttribute(TelstraConstants.ITEM_ID));
				yfcEleItem.setAttribute(TelstraConstants.UNIT_OF_MEASURE, yfcEleItemItemAssoc.getAttribute(TelstraConstants.UNIT_OF_MEASURE));
				yfcEleItem.setAttribute(TelstraConstants.SUPPLIER_ITEM, sItemID);
			}
			else{
				if(YFCCommon.isVoid(yfcEleItemItemList)){
					throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.VECTOR_ITEM_DOES_NOT_EXIST,"ItemID: "+sItemID ,
							new YFSException());
				}				
				String sUoM = yfcEleItemItemList.getAttribute(TelstraConstants.UNIT_OF_MEASURE);
				yfcEleItem.setAttribute(TelstraConstants.UNIT_OF_MEASURE, sUoM);
			}
		}
		String sUnitPrice = "";
		if(!YFCCommon.isVoid(yfcEleItemItemList)){
			
			YFCElement yfcElePrimaryInfo = yfcEleItemItemList.getChildElement(TelstraConstants.PRIMARY_INFORMATION);
			if(!YFCCommon.isVoid(yfcElePrimaryInfo)){
				sUnitPrice = yfcElePrimaryInfo.getAttribute(TelstraConstants.UNIT_COST);
			}			
		}
		
		return sUnitPrice;
	}

	/**
	 * 
	 * @param sItemID
	 * @param sUnitOfMeasure
	 * @return
	 */
	private YFCDocument callGetItemAssociation(String sItemID, String sUnitOfMeasure) {

		YFCDocument yfcDocGetItemAssoIp = YFCDocument.getDocumentFor("<AssociationList ItemID='" + sItemID
				+ "' OrganizationCode='TELSTRA_SCLSA' UnitOfMeasure='" + sUnitOfMeasure + "'/>");
		LoggerUtil.verboseLog("GpsManageVectorOrder :: callGetItemAssociation :: yfcDocGetItemAssoIp \n", logger,
				yfcDocGetItemAssoIp);
		YFCDocument yfcDocGetItemAssocTemp = YFCDocument.getDocumentFor(
				"<AssociationList> <Association AssociatedKey=''><Item ItemID='' OrganizationCode='' UnitOfMeasure=''/></Association></AssociationList>");

		YFCDocument yfcDocGetItemAssocOp = serviceInvoker.invokeYantraApi(TelstraConstants.API_GET_ITEM_ASSOCIATION,
				yfcDocGetItemAssoIp, yfcDocGetItemAssocTemp);
		LoggerUtil.verboseLog("GpsManageVectorOrder :: callGetItemAssociation :: yfcDocGetItemAssocOp \n", logger,
				yfcDocGetItemAssocOp);
		return yfcDocGetItemAssocOp;
	}

	/**
	 * 
	 * @param sItemID
	 * @param sSourceSystem 
	 * @return
	 */
	private YFCDocument callGetItemList(String sItemID, String sSourceSystem) {

		YFCDocument yfcDocGetItemListIp;
		if (YFCCommon.isStringVoid(sSourceSystem)) {
			yfcDocGetItemListIp = YFCDocument
					.getDocumentFor("<Item ItemID='" + sItemID + "' OrganizationCode='TELSTRA_SCLSA'/>");
		} else {
			yfcDocGetItemListIp = YFCDocument.getDocumentFor("<Item ItemID='" + sItemID
					+ "' OrganizationCode='TELSTRA_SCLSA'><AdditionalAttributeList> <AdditionalAttribute  Name='SOURCE_SYSTEM' Value='"
					+ sSourceSystem + "'></AdditionalAttribute>    </AdditionalAttributeList></Item>");
		}
		LoggerUtil.verboseLog("GpsManageVectorOrder :: callGetItemList :: yfcDocGetItemListIp 1 \n", logger,
				yfcDocGetItemListIp);
		YFCDocument yfcDocGetItemListTemp = YFCDocument
				.getDocumentFor("<ItemList><Item ItemID='' UnitOfMeasure=''><PrimaryInformation UnitCost=''/></Item></ItemList>");

		YFCDocument yfcDocGetItemListOp = serviceInvoker.invokeYantraApi(TelstraConstants.API_GET_ITEM_LIST,
				yfcDocGetItemListIp, yfcDocGetItemListTemp);
		LoggerUtil.verboseLog("GpsManageVectorOrder :: callGetItemList :: yfcDocGetItemListOp \n", logger,
				yfcDocGetItemListOp);

		return yfcDocGetItemListOp;
	}

	/** 
	 * 
	 * @param yfcDocGetOrderListOp 
	 * @param sIncludeReservation
	 * 
	 */
	private YFCDocument processChangeOrder(YFCDocument yfcDocGetOrderListOp) {

		List<String> listPrimeLineNo = new ArrayList<>();
		YFCNodeList<YFCElement> yfcNlOrderLine = yfcInDoc.getElementsByTagName(TelstraConstants.ORDER_LINE);
		for (YFCElement yfcEleOrderLine : yfcNlOrderLine) {
			String sOrderLineChangeStatus = yfcEleOrderLine.getAttribute(TelstraConstants.CHANGE_STATUS);
			if (TelstraConstants.CHANGE_STATUS_CANCELLED.equalsIgnoreCase(sOrderLineChangeStatus)) {

				String sPrimeLineNo = yfcEleOrderLine.getAttribute(TelstraConstants.PRIME_LINE_NO);	

				if(YFCCommon.isStringVoid(sPrimeLineNo)){
					throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.VECTOR_ID_INVALID_ERROR_CODE,
							new YFSException());
				}

				YFCElement yfcEleOrderStatus = XPathUtil.getXPathElement(yfcDocGetOrderListOp, "//OrderLine[@PrimeLineNo='"+sPrimeLineNo+"']/OrderStatuses/OrderStatus[@Status='9000']");
				if(YFCElement.isVoid(yfcEleOrderStatus)){
					yfcEleOrderLine.setAttribute(TelstraConstants.ACTION, TelstraConstants.CANCEL);
				}
				else{
					listPrimeLineNo.add(sPrimeLineNo);
				}				
			}
		}

		for(String sPrimeLineNo : listPrimeLineNo){
			YFCElement yfcEleOrderLine = XPathUtil.getXPathElement(yfcInDoc, "//OrderLine[@PrimeLineNo='"+sPrimeLineNo+"']");
			yfcEleOrderLine.getParentElement().removeChild(yfcEleOrderLine);
		}

		YFCNodeList<YFCElement> yfcNlOrderLineChangeOrder = yfcInDoc.getElementsByTagName(TelstraConstants.ORDER_LINE);

		LoggerUtil.verboseLog("GpsManageVectorOrder :: processChangeOrder :: Change Order Ip \n", logger, yfcInDoc);
		YFCDocument yfcDocChangeOrderOp = null;
		if(yfcNlOrderLineChangeOrder.getLength()>0){
			YFCDocument yfcDocCreateOrderTemp = YFCDocument.getDocumentFor("<Order OrderNo='' OrderHeaderKey='' OrderName=''/>");
			yfcDocChangeOrderOp = serviceInvoker.invokeYantraApi(TelstraConstants.API_CHANGE_ORDER, yfcInDoc, yfcDocCreateOrderTemp);
		}
		return yfcDocChangeOrderOp;
	}

	/**
	 * 
	 * @param sOrderName
	 * @return
	 */
	private YFCDocument callGetOrderListWithOrderName(String sOrderName) {

		YFCDocument yfcDocGetOrderListIp = YFCDocument.getDocumentFor("<Order OrderName='" + sOrderName + "'/>");
		LoggerUtil.verboseLog("GpsManageVectorOrder :: callGetOrderList :: yfcDocGetOrderListIp\n", logger,
				yfcDocGetOrderListIp);
		YFCDocument yfcDocGetOrderListTemp = YFCDocument
				.getDocumentFor("<OrderList TotalNumberOfRecords=''><Order OrderHeaderKey='' EntryType=''><OrderLines><OrderLine PrimeLineNo=''/></OrderLines></Order></OrderList>");

		LoggerUtil.verboseLog("GpsManageVectorOrder :: callGetOrderListWithOrderName :: get order list Ip \n", logger,
				yfcDocGetOrderListIp);

		YFCDocument yfcDocGetOrderListOp = serviceInvoker.invokeYantraApi(TelstraConstants.API_GET_ORDER_LIST,
				yfcDocGetOrderListIp, yfcDocGetOrderListTemp);
		LoggerUtil.verboseLog("GpsManageVectorOrder :: callGetOrderList :: yfcDocGetOrderListOp\n", logger,
				yfcDocGetOrderListOp);

		return yfcDocGetOrderListOp;
	}
}

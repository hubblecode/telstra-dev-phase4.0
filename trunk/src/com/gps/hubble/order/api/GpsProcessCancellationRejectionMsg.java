/***********************************************************************************************
 * File	Name		: GpsProcessCancellationRejectionMsg.java
 *
 * Description		: This class is called from GpsShipOrRejectOrder service. 
 * 					It moves the line to Cancellation Rejected status
 * 
 * Modification	Log	:
 * --------------------------------------------------------------------------------------------------------------------------------------------------------------
 * Ver #	Date			Author					Modification
 * --------------------------------------------------------------------------------------------------------------------------------------------------------------
 * 1.0		Jun 15,2017	  	Prateek Kumar 		   	Initial	Version 
 * --------------------------------------------------------------------------------------------------------------------------------------------------------------
 * 
 */

package com.gps.hubble.order.api;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.constants.TelstraErrorCodeConstants;
import com.yantra.yfc.date.YDate;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSException;

/**
 * 
 * @author Prateek
 *
 */
public class GpsProcessCancellationRejectionMsg extends AbstractCustomApi {
	private static YFCLogCategory logger = YFCLogCategory.instance(GpsProcessCancellationRejectionMsg.class);

	@Override
	public YFCDocument invoke(YFCDocument yfcInDoc) throws YFSException {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", yfcInDoc);

		YFCElement yfcEleInRoot = yfcInDoc.getDocumentElement();

		String sVectorID = yfcEleInRoot.getAttribute(TelstraConstants.ORDER_NAME);
		YFCDocument yfcDocGetOrderLineListOp = callGetOrderLineList(sVectorID);

		int iOrderLineLength = yfcDocGetOrderLineListOp.getElementsByTagName(TelstraConstants.ORDER_LINE).getLength();
		if (iOrderLineLength == 0) {
			throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.ACCEPT_ORDER_NO_ORDER_LINE_FOR_VECTOR_ID,
					new YFSException());
		} else if (iOrderLineLength > 1) {
			throw ExceptionUtil.getYFSException(
					TelstraErrorCodeConstants.ACCEPT_ORDER_MULTIPLE_ORDER_LINES_WITH_SAME_VECTOR_ID,
					new YFSException());
		}

		processCancellationRejection(yfcDocGetOrderLineListOp);

		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", yfcInDoc);
		return null;
	}

	/**
	 * 
	 * @param yfcDocGetOrderLineListOp
	 */
	private void processCancellationRejection(YFCDocument yfcDocGetOrderLineListOp) {

		YFCElement yfcEleOrderLine = yfcDocGetOrderLineListOp.getElementsByTagName(TelstraConstants.ORDER_LINE).item(0);
		double dMinLineStatus = yfcEleOrderLine.getDoubleAttribute(TelstraConstants.MIN_LINE_STATUS);
		String sDocumentType = yfcEleOrderLine.getChildElement(TelstraConstants.ORDER)
				.getAttribute(TelstraConstants.DOCUMENT_TYPE);
		if (dMinLineStatus < 3200 && TelstraConstants.DOCUMENT_TYPE_0001.equalsIgnoreCase(sDocumentType)) {
			scheduleAndReleaseLine(yfcEleOrderLine);
		}
		moveLineToCancellationRejected(yfcEleOrderLine);
	}

	/**
	 * 
	 * @param yfcEleOrderLine
	 */
	private void moveLineToCancellationRejected(YFCElement yfcEleOrderLine) {

		YFCElement yfcEleOrder = yfcEleOrderLine.getElementsByTagName(TelstraConstants.ORDER).item(0);
		String sDocumentType = yfcEleOrder.getAttribute(TelstraConstants.DOCUMENT_TYPE);
		String sOrderHeaderKey = yfcEleOrder.getAttribute(TelstraConstants.ORDER_HEADER_KEY);
		String sOrderLineKey = yfcEleOrderLine.getAttribute(TelstraConstants.ORDER_LINE_KEY);
		String sPrimeLineNo = yfcEleOrderLine.getAttribute(TelstraConstants.PRIME_LINE_NO);
		String date = getDateStamp();
		String sTransactionID = getProperty("ChangeOrderStatusToCanRejected", true) + sDocumentType + ".ex";// SERVICE_REJECTED_UPDATE.0001.ex
		YFCDocument docchangeOrderinXml = YFCDocument.getDocumentFor("<Order OrderHeaderKey='" + sOrderHeaderKey
				+ "' TransactionId='" + sTransactionID + "' ModificationReasonText='Order Line#  " + sPrimeLineNo
				+ " Move to cancellation rejected status at " + date
				+ "'><OrderLines> <OrderLine BaseDropStatus='3200.7000' ChangeForAllAvailableQty='Y' OrderLineKey='"
				+ sOrderLineKey + "'/></OrderLines></Order>");
		LoggerUtil.verboseLog("GpsProcessCancellationRejectionMsg:: moveLineToCancellationRejected:: changeOrderStatus",
				logger, docchangeOrderinXml);
		invokeYantraApi("changeOrderStatus", docchangeOrderinXml);
	}

	/**
	 * 
	 * @param yfcEleOrderLine
	 */
	private void scheduleAndReleaseLine(YFCElement yfcEleOrderLine) {

		String sOrderLineKey = yfcEleOrderLine.getAttribute(TelstraConstants.ORDER_LINE_KEY);
		String sOrderedQty = yfcEleOrderLine.getAttribute(TelstraConstants.ORDERED_QTY);
		String sShipNode = yfcEleOrderLine.getAttribute(TelstraConstants.SHIP_NODE);
		String sOrderHeaderKey = yfcEleOrderLine.getChildElement(TelstraConstants.ORDER)
				.getAttribute(TelstraConstants.ORDER_HEADER_KEY);

		/*
		 * Get the deliveryDate as the current date to pass it in the
		 * scheduelOrderLines API
		 */
		YDate yDDeliveryDate = new YDate(false);
		YFCDocument yfcDocScheduleOrderLinesIp = YFCDocument
				.getDocumentFor("<Promise OrderHeaderKey='" + sOrderHeaderKey
						+ "' IgnoreReleaseDate='Y' ScheduleAndRelease='Y'> <PromiseLines> <PromiseLine OrderLineKey='"
						+ sOrderLineKey + "' Quantity='" + sOrderedQty + "' ShipNode='" + sShipNode
						+ "'/> </PromiseLines></Promise>");

		yfcDocScheduleOrderLinesIp.getElementsByTagName(TelstraConstants.PROMISE_LINE).item(0)
				.setDateAttribute(TelstraConstants.DELIVERY_DATE, yDDeliveryDate);

		logger.verbose("GpsServiceAcceptedOrder::scheduleAndReleaseLine:: yfcDocScheduleOrderLinesIp\n"
				+ yfcDocScheduleOrderLinesIp);
		invokeYantraApi(TelstraConstants.API_SCHEDULE_ORDER_LINES, yfcDocScheduleOrderLinesIp);
	}

	/**
	 * 
	 * @return
	 */
	private YFCDocument callGetOrderLineList(String sVectorID) {

		YFCDocument inDocgetOrdLineList = YFCDocument.getDocumentFor(
				"<OrderLine><Order EntryType='VECTOR' /><Extn VectorID='" + sVectorID + "' /></OrderLine>");
		YFCDocument templateForGetOrdLineList = YFCDocument.getDocumentFor("<OrderLineList TotalLineList=''>"
				+ "<OrderLine PrimeLineNo='' OrderLineKey='' OrderedQty='' MinLineStatus='' ShipNode=''><Order OrderName='' OrderHeaderKey='' DocumentType=''/> <OrderStatuses> "
				+ "<OrderStatus OrderReleaseKey='' Status=''/> </OrderStatuses></OrderLine> </OrderLineList>");

		YFCDocument yfcDocGetOrderLineListOp = invokeYantraApi("getOrderLineList", inDocgetOrdLineList,
				templateForGetOrdLineList);

		logger.verbose("RejectOrder::callGetOrderLineList:: yfcDocGetOrderLineListOp\n" + yfcDocGetOrderLineListOp);

		return yfcDocGetOrderLineListOp;
	}

	/**
	 * 
	 * @return
	 */
	private String getDateStamp() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		// get current date time with Date()
		Date date = new Date();
		return dateFormat.format(date);
	}
}

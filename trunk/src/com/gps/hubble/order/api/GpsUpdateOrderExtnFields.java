/***********************************************************************************************
 * File	Name		: GpsUpdateOrderExtnFields.java
 *
 * Description		: This class is called from ManageOrder.java. If there are multiple shipToAddress or ReqShipDate or ReqDeliveryDate
 * 					or PriortiyCode or Division it updates order extn boolean with Y other wise N. On top of that it updates earliest ship date and earliest delivery date
 * 					in the	header level ReqShipDate and ReqDeliveryDate resp.
 * 					Boolean1 - ReqShipDate
 * 					Boolean2 - ReqDeliveryDate
 * 					Boolean3 - ShipToAddress
 * 					Boolean4 - Division
 * 					Boolean5 - PriorityCode
 * 					Boolean6 - Contact
 * 
 * Modification	Log	:
 * ---------------------------------------------------------------------------------------------
 * Ver #	Date				Author					Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0		Feb 01, 2017	  	Prateek Kumar 		   	Initial	Version 
 * 1.1		Mar 27, 2017		Prateek Kumar			HUB-6930: Added boolean for the contact
 * ---------------------------------------------------------------------------------------------
 **********************************************************************************************/
package com.gps.hubble.order.api;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import com.bridge.sterling.framework.service.ServiceInvoker;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.date.YDate;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSException;

/**
 * 
 * @author Prateek
 *
 */
public class GpsUpdateOrderExtnFields {


	private static YFCLogCategory logger = YFCLogCategory.instance(GpsUpdateOrderExtnFields.class);
	private Set<YDate> setYDateReqShipDate= new TreeSet<>(new EarliestDateComparator());
	private Set<YDate> setYDateReqDeliveryDate= new TreeSet<>(new EarliestDateComparator());
	private Set<String> setDivision = new HashSet<>();
	private Set<String> setPriorityCode = new HashSet<>();
	private Set<String> setPersonInfoShipToKey = new HashSet<>();
	private Set<String> setContact = new HashSet<>();
	private ServiceInvoker serviceInvoker = null;

	/**
	 * 
	 * @param yfcInDoc
	 * @param serviceInvoker
	 * @return
	 * @throws YFSException
	 */
	public YFCDocument updateOrderExtnFields(YFCDocument yfcInDoc, ServiceInvoker serviceInvoker) throws YFSException {

		LoggerUtil.startComponentLog(logger, this.getClass().getName(),"invoke", yfcInDoc);
		this.serviceInvoker = serviceInvoker;
		/*
		 * setting the transaction object so that change order UE logic is not invoked due to change order call in this code
		 */
		serviceInvoker.setTxnObject("ORDER_ALREADY_UPDATED", true);
		/*
		 * This method will return the get order list op. Input passed is OrderName.
		 */
		YFCDocument yfcDocGetOrderListOp = callGetOrderList(yfcInDoc);
		
		
		LoggerUtil.verboseLog(this.getClass().getName()+":: yfcDocGetOrderListOp ::", logger, yfcDocGetOrderListOp);
		String sMinOrderStatus = XPathUtil.getXpathAttribute(yfcDocGetOrderListOp, "//Order/@MinOrderStatus");
		/*
		 * Including Freight as it is throwing modification rule exception
		 */
		if("9000".equalsIgnoreCase(sMinOrderStatus)||"9000.5000".equalsIgnoreCase(sMinOrderStatus)){
			LoggerUtil.verboseLog(this.getClass().getName()+":: Exiting as order is in Cancelled or Freight Line Closed status ::", logger, sMinOrderStatus);
			return yfcInDoc;
		}
		
		String sOrderHeaderKey = XPathUtil.getXpathAttribute(yfcDocGetOrderListOp, "//Order/@OrderHeaderKey");
		YFCDocument returnDoc = null;
		if(!YFCCommon.isStringVoid(sOrderHeaderKey)){
			YFCNodeList<YFCElement> yfcNlOrderLine = yfcDocGetOrderListOp.getElementsByTagName(TelstraConstants.ORDER_LINE);
			for(YFCElement yfcEleOrderLine: yfcNlOrderLine){

				String sStatus = yfcEleOrderLine.getAttribute(TelstraConstants.STATUS);
				/*
				 * Since the line is already cancelled,ignore it and continue with other lines.
				 */
				if(TelstraConstants.CANCELLED.equalsIgnoreCase(sStatus)){
					continue;
				}
				/*
				 * This method will prepare the different set of line level req delivery date, req ship date, ship to address key, priority and division.
				 */
				prepareSetofReqFields(yfcEleOrderLine);
			}
			/*
			 * This method will prepare the change order. If there are multiple value at the line level it will set boolean as Y otherwise as N.
			 * For example, if order has multiple req ship date then boolean1 would be set as Y otherwise as N. 
			 * Also it will stamp earliest ship and request date at the header level
			 */
			YFCDocument yfcDocChangeOrderIp = prepareChangeOrderIp (sOrderHeaderKey);
			LoggerUtil.verboseLog(this.getClass().getName()+":: yfcDocChangeOrderIp ::", logger, yfcDocChangeOrderIp);

			returnDoc = serviceInvoker.invokeYantraApi(TelstraConstants.API_CHANGE_ORDER, yfcDocChangeOrderIp, getChangeOrderTemplate());
		}
		LoggerUtil.endComponentLog(logger, this.getClass().getName(),"invoke", returnDoc);

		return returnDoc;
	}

	private YFCDocument getChangeOrderTemplate() {

		return YFCDocument.getDocumentFor("<Order OrderHeaderKey='' DocumentType='' EnterpriseCode='' OrderName='' OrderNo=''/>");
	}

	/**
	 * This method will prepare the change order. If there are multiple value at the line level it will set boolean as Y otherwise as N.
	 * For Eg, if order has multiple req ship date then boolean1 would be set as Y otherwise as N.
	 * Also it will stamp earliest ship and request date at the header level
	 * @param sOrderHeaderKey
	 * @return 
	 */
	private YFCDocument prepareChangeOrderIp(String sOrderHeaderKey) {

		YFCDocument yfcDocChangeOrderIp = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderHeaderKey+"' Override='Y'/>");
		YFCElement yfcEleRootChangeOrderIp = yfcDocChangeOrderIp.getDocumentElement();
		/*
		 * This set is already sorted in ascending order. The first date in the set is the earliest date
		 */
		if(!setYDateReqShipDate.isEmpty()){
			yfcEleRootChangeOrderIp.setDateAttribute(TelstraConstants.REQ_SHIP_DATE, setYDateReqShipDate.iterator().next());
		}
		/*
		 * This set is already sorted in ascending order. The first date in the set is the earliest date.
		 */
		if(!setYDateReqDeliveryDate.isEmpty()){
			yfcEleRootChangeOrderIp.setDateAttribute(TelstraConstants.REQ_DELIVERY_DATE, setYDateReqDeliveryDate.iterator().next());
		}
		/*
		 * If there are multiple value at the line level it will set boolean as Y otherwise as N.
		 */
		appendOrderExtnField(yfcEleRootChangeOrderIp);

		return yfcDocChangeOrderIp;
	}

	/**
	 * This method will prepare the different set of line level req delivery date, req ship date, ship to address key, priority and division.
	 * @param yfcEleOrderLine
	 */
	private void prepareSetofReqFields(YFCElement yfcEleOrderLine) {

		/*
		 * Preparing the set of req ship date
		 */
		YDate yDateReqShipDate = yfcEleOrderLine.getYDateAttribute(TelstraConstants.REQ_SHIP_DATE);
		if(!YFCCommon.isVoid(yDateReqShipDate)){
			setYDateReqShipDate.add(yDateReqShipDate);
		}
		/*
		 * Preparing the set of req delivery date
		 */
		YDate yDateReqDeliveryDate = yfcEleOrderLine.getYDateAttribute(TelstraConstants.REQ_DELIVERY_DATE);
		if(!YFCCommon.isVoid(yDateReqDeliveryDate)){
			setYDateReqDeliveryDate.add(yDateReqDeliveryDate);
		}
		/*
		 * Preparing the set of division, priority code and contact
		 */
		YFCElement yfcEleExtn = yfcEleOrderLine.getChildElement(TelstraConstants.EXTN);
		if(!YFCCommon.isVoid(yfcEleExtn)){
			String sDivision = yfcEleExtn.getAttribute(TelstraConstants.DIVISION);
			if(!YFCCommon.isStringVoid(sDivision)){
				setDivision.add(sDivision);
			}
			String sPriorityCode = yfcEleExtn.getAttribute(TelstraConstants.PRIORITY_CODE);
			if(!YFCCommon.isStringVoid(sPriorityCode)){
				setPriorityCode.add(sPriorityCode);
			}
			
			String sContact = yfcEleExtn.getAttribute(TelstraConstants.CONTACT);
			if(!YFCCommon.isStringVoid(sContact)){
				setContact.add(sContact);
			}
		}
		/*
		 * preparing the set of ship to key
		 */
		YFCElement yfcElePersonInfoShipTo = yfcEleOrderLine.getChildElement(TelstraConstants.PERSON_INFO_SHIP_TO);
		if(!YFCCommon.isVoid(yfcElePersonInfoShipTo)){
			String sPersonInfoKey = yfcElePersonInfoShipTo.getAttribute(TelstraConstants.PERSON_INFO_KEY);
			if(!YFCCommon.isStringVoid(sPersonInfoKey)){
				setPersonInfoShipToKey.add(sPersonInfoKey);
			}		
		}
	}

	/**
	 * This method will return the get order list op. Input passed is OrderName.
	 * @param yfcInDoc - <Order OrderName=''/>
	 * @return
	 */
	private YFCDocument callGetOrderList(YFCDocument yfcInDoc) {

		YFCDocument yfcDocGetOrderListTemp = YFCDocument.getDocumentFor(
				"<OrderList><Order OrderHeaderKey='' OrderNo='' MinOrderStatus=''><OrderLines> <OrderLine PrimeLineNo='' ReqDeliveryDate='' ReqShipDate='' Status=''  OrderLineKey=''> <Extn Division='' PriorityCode='' Contact=''/> <PersonInfoShipTo PersonInfoKey='' /> </OrderLine> </OrderLines> </Order></OrderList>");

		return serviceInvoker.invokeYantraApi(TelstraConstants.API_GET_ORDER_LIST, yfcInDoc, yfcDocGetOrderListTemp);
	}

	/**
	 * If there are multiple value at the line level it will set boolean as Y otherwise as N.
	 * @param yfcEleRootChangeOrderIp
	 */
	private void appendOrderExtnField(YFCElement yfcEleRootChangeOrderIp) {

		YFCElement yfcEleOrderExtension = yfcEleRootChangeOrderIp.createChild("CustomAttributes");
		/*
		 * checking and setting multiple req ship date if any
		 */
		String sMultipleReqShipDate = TelstraConstants.NO;
		if (!YFCCommon.isVoid(setYDateReqShipDate) && setYDateReqShipDate.size()>1) {
			sMultipleReqShipDate = TelstraConstants.YES;
		}
		yfcEleOrderExtension.setAttribute(TelstraConstants.BOOLEAN_1, sMultipleReqShipDate);
		/*
		 * checking and setting multiple req delivery date if any
		 */
		String sMultipleReqDeliveryDate = TelstraConstants.NO;
		if (!YFCCommon.isVoid(setYDateReqDeliveryDate) && setYDateReqDeliveryDate.size()>1) {
			sMultipleReqDeliveryDate = TelstraConstants.YES;
		}
		yfcEleOrderExtension.setAttribute(TelstraConstants.BOOLEAN_2, sMultipleReqDeliveryDate);
		/*
		 * checking and setting multiple ship to flag if any
		 */
		String sMultipleShipTo = TelstraConstants.NO;
		if (!YFCCommon.isVoid(setPersonInfoShipToKey) && setPersonInfoShipToKey.size()>1) {
			sMultipleShipTo =  TelstraConstants.YES;
		}
		yfcEleOrderExtension.setAttribute(TelstraConstants.BOOLEAN_3, sMultipleShipTo);
		/*
		 * checking and setting multiple division flag if any
		 */
		String sMultipleDivision = TelstraConstants.NO;
		if (!YFCCommon.isVoid(setDivision) && setDivision.size()>1) {
			sMultipleDivision =  TelstraConstants.YES;
		}
		yfcEleOrderExtension.setAttribute(TelstraConstants.BOOLEAN_4, sMultipleDivision);
		/*
		 * checking and setting multiple priority code flag if any
		 */
		String sMultiplePriority = TelstraConstants.NO;
		if (!YFCCommon.isVoid(setPriorityCode) && setPriorityCode.size()>1) {
			sMultiplePriority =  TelstraConstants.YES;
		}
		yfcEleOrderExtension.setAttribute(TelstraConstants.BOOLEAN_5, sMultiplePriority);		
		/*
		 * checking and setting multiple contact flag if any
		 */
		String sMultipleContact = TelstraConstants.NO;
		if (!YFCCommon.isVoid(setContact) && setContact.size()>1) {
			sMultipleContact =  TelstraConstants.YES;
		}
		yfcEleOrderExtension.setAttribute(TelstraConstants.BOOLEAN_6, sMultipleContact);
	}
	

	/**
	 * This class implements Comparator interface and is used for sorting the ship and delivery date in ascending order.
	 * @author Prateek
	 *
	 */
	public class EarliestDateComparator implements Comparator<YDate> {

		@Override
		public int compare(YDate yDate1, YDate yDate2) {			
			return yDate1.compareTo(yDate2);
		}
	}

}

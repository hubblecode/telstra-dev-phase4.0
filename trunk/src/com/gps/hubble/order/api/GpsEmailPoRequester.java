package com.gps.hubble.order.api;

import java.util.HashMap;
import java.util.Map;
import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.utils.TemplateProcessor;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.log.YFCLogCategory;


/**
 * Custom API for to send email confirmation to the PO requester when the PO is created in Sterling
 * 
 * @author Keerthi Yadav
 * @version 1.0
 *
 * Extends AbstractCustomApi Class
 * 
 */

public class GpsEmailPoRequester extends AbstractCustomApi {

	private static YFCLogCategory logger = YFCLogCategory.instance(GpsEmailPoRequester.class);
	private static final String PROCESSED_PO = "/global/template/email/html/PoCreation.html";
	private String sOrderName=null;

	/**
	 * EmailID would be taken from the /Order/PersonInfoBillTo/@EMailID Attribute for INT-ODR-3/INT-ODR-4/VEC-ODR-1/MER-ODR-1
	 * Base method Initial execution starts here
	 * Method name: invoke
	 * @param inXml
	 * @return outDocGpsSendEmail/inXml
	 */

	@Override
	public YFCDocument invoke(YFCDocument inXml){

		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", inXml);

		YFCDocument outDocGpsSendEmail=inXml;		
		String sToEmailID = validateEmailID(inXml);

		if(!YFCObject.isNull(sToEmailID)){
			YFCDocument inDocGpsSendEmail=createInputtoGpsSendEmail(sToEmailID);		
			outDocGpsSendEmail = callGpsSendEmail(inDocGpsSendEmail);		
		}

		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", inXml);
		return outDocGpsSendEmail;
	}

	/**
	 * Checking if ToEmailID is present
	 * Method name: validateEmailID
	 * @param inXml
	 * @return sToEmailID
	 */

	private String validateEmailID(YFCDocument inXml) {	
		YFCElement eleOrder=inXml.getDocumentElement();
		YFCElement elePersonInfoBillTo=eleOrder.getElementsByTagName(TelstraConstants.PERSON_INFO_BILL_TO).item(0);
		String sToEmailID = elePersonInfoBillTo.getAttribute("EMailID","");
		sOrderName=eleOrder.getAttribute("OrderName","");
		if(YFCObject.isNull(sToEmailID)){
			logger.error("The Purchase Order with OrderName : '" + sOrderName + "' and PersonID :" + elePersonInfoBillTo.getAttribute("PersonID","") + "' has a blank To EmailID, therefore the Email has failed.");			
		}
		return sToEmailID;
	}


	/**
	 * Sample input
	 * <SendMail>
        <MessageHeader FromAddress='noreply@au1.ibm.com'
            MailHost='146.89.213.203' MailPort='25' SubjectText='Can you provide the Proof of Deliver (POD) of the PO ORDER61'  FailureEmailAddress='' >
            <ToAddresses>
                <ToAddress EmailID=''/>
            </ToAddresses>
        </MessageHeader>
       <MessageDetail>Hi, &#xa;Kindly confirm the Proof of Delivery (POD) and POD Date of the PO, by replying this email.</MessageDetail>
    </SendMail>
	 */

	/**
	 * Constructing Input xml to the Service GpsSendEmail 
	 * Method name: createInputtoGpsSendEmail
	 * @param sToEmailID
	 * @return docchangeOrderinXml
	 */

	private YFCDocument createInputtoGpsSendEmail(String sToEmailID) {

		YFCDocument docchangeOrderinXml = YFCDocument.getDocumentFor("<SendMail><MessageHeader><ToAddresses><ToAddress /></ToAddresses></MessageHeader><MessageDetail /></SendMail>");
		YFCElement eleMessageHeader = docchangeOrderinXml.getDocumentElement().getElementsByTagName("MessageHeader").item(0);

		String sMessageDetail = getEmailDetail();
		String sSubjectText = getProperty("Subject_Text", true);
		String sFailureEmailAddress = getProperty("Failure_Email_ID", true);

		eleMessageHeader.setAttribute("FromAddress", TelstraConstants.EMAIL_FROM_ID);
		eleMessageHeader.setAttribute("MailHost", TelstraConstants.EMAIL_HOST);
		eleMessageHeader.setAttribute("MailPort", TelstraConstants.EMAIL_PORT);
		eleMessageHeader.setAttribute("SubjectText", sSubjectText + " " + sOrderName);
		eleMessageHeader.setAttribute("FailureEmailAddress", sFailureEmailAddress);

		YFCElement eleToAddress = eleMessageHeader.getElementsByTagName("ToAddress").item(0);
		eleToAddress.setAttribute("EmailID", sToEmailID);

		YFCNode eleMessageDetail= docchangeOrderinXml.getDocumentElement().getElementsByTagName("MessageDetail").item(0);
		eleMessageDetail.setNodeValue(sMessageDetail);

		return docchangeOrderinXml;
	}


	/**
	 * Calling Service GpsSendEmail to send the Email confirmation to the PO requester 
	 * Method name: callGpsSendEmail
	 * @param inDocGpsSendEmail
	 * @return outDocGpsSendEmail
	 */

	private YFCDocument callGpsSendEmail(YFCDocument inDocGpsSendEmail) {
		return invokeYantraService("GpsSendEmail", inDocGpsSendEmail);
	}


	/**
	 * This method is to create the MessageDetail from html format to string 
	 * Method name: getEmailDetail
	 * @return contentBuilder
	 */

	private String getEmailDetail() {

		String fileName= PROCESSED_PO;
		Map<String, Object> model = new HashMap<>();
		model.put("ordername", sOrderName);
		TemplateProcessor tpl = new TemplateProcessor();
		return tpl.process(fileName,model);
	}

}
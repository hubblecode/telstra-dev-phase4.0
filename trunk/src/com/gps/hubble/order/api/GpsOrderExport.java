package com.gps.hubble.order.api;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.json.JSONArray;
import org.apache.commons.json.JSONException;
import org.apache.commons.json.JSONObject;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.utils.GpsJSONXMLUtil;
import com.sterlingcommerce.woodstock.util.Base64;
import com.yantra.custom.dbi.GPS_Order_Export_Vw;
import com.yantra.shared.dbclasses.GPS_Order_Export_VwDBHome;
import com.yantra.shared.ycp.YFSContext;
import com.yantra.util.YFCUtils;
import com.yantra.yfc.dblayer.PLTQueryBuilder;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSException;

public class GpsOrderExport extends AbstractCustomApi {

	private static YFCLogCategory cat = YFCLogCategory.instance(GpsOrderExport.class);
	private static final String ATTR_LABEL = "Label";
	private static final String ATTR_BINDING = "Binding";
	private static final String ATTR_NAME = "Name";
	
	@Override
	public YFCDocument invoke(YFCDocument inDoc) throws YFSException {
		YFCElement inElem = inDoc.getDocumentElement();
		String searchCriteria =	inElem.getAttribute("ExportCriteria");
		String decodedCriteria = new String(Base64.decode(searchCriteria.getBytes()));
		String attrList = inElem.getAttribute("ExportColumns");
		String shipToID = "";
		YFCDocument outDoc = YFCDocument.createDocument("ExportData");
		//YFCDocument outDoc = YFCDocument.createDocument("OrderExportList");
		YFCElement rootElem = outDoc.getDocumentElement();
		YFCElement columnListElem = rootElem.createChild("ColumnList");
		YFCElement dataListElem = rootElem.createChild("DataList");
		
		YFCDocument templateDoc = YFCDocument.createDocument(GPS_Order_Export_VwDBHome.getInstance().getXMLName());
		YFCElement templateElem = templateDoc.getDocumentElement();
		Map<String,JSONObject> columnMapping = new LinkedHashMap<>();
		Set<String> dateColumns = new HashSet<>();
		try{	
			YFSContext ctx = getServiceInvoker().getContext();
			PLTQueryBuilder pltQry = new PLTQueryBuilder();
			JSONObject criteriaJSON = new JSONObject(decodedCriteria);
			JSONObject searchCriteriaObj = (JSONObject)criteriaJSON.get("SearchCriteria");
			JSONObject mappingListObj = (JSONObject)criteriaJSON.get("MappingList");
			YFCDocument criteriaDoc = GpsJSONXMLUtil.getXmlFromJSON(searchCriteriaObj,"Order");
			YFCElement criteriaElem = criteriaDoc.getDocumentElement();
			appendComplexQuery(criteriaElem,"OrderList");
			if(criteriaElem.hasAttribute(TelstraConstants.SHIP_TO_ID)) {
				shipToID = criteriaElem.getAttribute(TelstraConstants.SHIP_TO_ID);
				criteriaElem.removeAttribute(TelstraConstants.SHIP_TO_ID);
			}
			YFCDocument orderListtemplateDoc = YFCDocument.getDocumentFor("<OrderList><Order OrderHeaderKey=''/></OrderList>");
			
			YFCDocument getOrderListOutDoc = getServiceInvoker().invokeYantraApi(TelstraConstants.API_GET_ORDER_LIST, criteriaDoc,orderListtemplateDoc);
			//YFCDocument getOrderListOutDoc = getServiceInvoker().invokeYantraService("GpsGetOrderList", criteriaDoc);
			YFCElement getOrderListElem = getOrderListOutDoc.getDocumentElement();
			List<String> orderHeaderKeys=new ArrayList<>();
			for(Iterator<YFCElement> eleIter= getOrderListElem.getChildren();eleIter.hasNext();) {
				YFCElement orderElem = eleIter.next();
				orderHeaderKeys.add(orderElem.getAttribute(TelstraConstants.ORDER_HEADER_KEY));
			}
			if(orderHeaderKeys.isEmpty()) {
				return outDoc;
			}
			if(mappingListObj != null) {
				JSONArray mappingArray = (JSONArray)mappingListObj.get("Mapping");
				for(int i=0;i<mappingArray.length();i++) {
					JSONObject mappingObject=(JSONObject)mappingArray.get(i);
					String columnName = (String)mappingObject.get("Binding");
					String labelName = (String)mappingObject.get("Label");
					String isDate = "";
					if(mappingObject.containsKey("IsDate")) {
						isDate = (String)mappingObject.get("IsDate");
					}
					if(YFCUtils.isVoid(labelName)) {
						labelName=columnName;
					}
					if(!YFCUtils.isVoid(isDate) && "Y".equals(isDate)) {
						dateColumns.add(columnName);
					}
					columnMapping.put(columnName,mappingObject);
					
				}

			}

			String [] ohArr = orderHeaderKeys.toArray(new String[0]);
			YFCDocument orderExportDoc = YFCDocument.createDocument(GPS_Order_Export_VwDBHome.getInstance().getXMLName());
			YFCElement orderExportElem = orderExportDoc.getDocumentElement();
			makeOrderInput(orderExportElem,criteriaElem);
			if(!YFCUtils.isVoid(shipToID)) {
				orderExportElem.setAttribute(TelstraConstants.SHIP_TO_ID, shipToID);
				appendComplexQuery(orderExportElem,"OrderView");
			}
			
			
			String whereClause = GPS_Order_Export_VwDBHome.getInstance().getWhereClause(orderExportElem,GPS_Order_Export_VwDBHome.getInstance().getTableCode());
			if(ohArr.length > 0) {
				pltQry.appendWHERE();
				pltQry.appendStringForIN(GPS_Order_Export_VwDBHome.getInstance(), "ORDER_HEADER_KEY", ohArr);
				if(!YFCUtils.isVoid(whereClause)) {
					pltQry.appendAND();
					pltQry.append(whereClause);
					
				}
				cat.debug("The Generated Query is "+pltQry.getReadableWhereClause());				
			}
			if(YFCUtils.isVoid(attrList)) {
				attrList = getProperty("AttributeList");
			}
			if(!columnMapping.isEmpty()) {
				for(Iterator<String> mapIter=columnMapping.keySet().iterator();mapIter.hasNext(); ) {
					String colName= mapIter.next();
					//String colValue = columnMapping.get(colName);
					JSONObject colObj = columnMapping.get(colName);
					YFCElement columnElem = columnListElem.createChild("Column");
					columnElem.setAttribute("Name", colName);	
					columnElem.setAttribute("Label", (String)colObj.getString("Label"));
					if(colObj.containsKey("IsDate")) {
						columnElem.setAttribute("IsDate", (String)colObj.getString("IsDate"));
					}
					if(colObj.containsKey("EscapeChars")) {
						columnElem.setAttribute("EscapeChars", (String)colObj.getString("EscapeChars"));
					}
										
					/*if(dateColumns.contains(colName)) {
						columnElem.setAttribute("IsDate", "Y");
					}*/
					templateElem.setAttribute(colName, "");
				}
			} else if(!YFCUtils.isVoid(attrList)) {
				String [] attrArray = attrList.split(",");
				for(String attrName: attrArray) {
					YFCElement columnElem = columnListElem.createChild("Column");
					String [] attrLabels = attrName.split(":");
					if(attrLabels.length > 2) {
						columnElem.setAttribute("Name", attrLabels[0]);
						columnElem.setAttribute("Label", attrLabels[1]);
						columnElem.setAttribute("IsDate", attrLabels[2]);
					} else if(attrLabels.length > 1) {
						columnElem.setAttribute("Name", attrLabels[0]);
						columnElem.setAttribute("Label", attrLabels[1]);
					} else {
						columnElem.setAttribute("Name", attrLabels[0]);
						columnElem.setAttribute("Label", attrLabels[0]);
					}
					templateElem.setAttribute(attrLabels[0], "");
				}
			}else {
				Set<String> attrNames = GPS_Order_Export_Vw.getXMLAttributeNames();
				for(String attrName: attrNames) {
					YFCElement columnElem = columnListElem.createChild("Column");
					columnElem.setAttribute("Name", attrName);
					columnElem.setAttribute("Label", attrName);
					if(dateColumns.contains(attrName)) {
						columnElem.setAttribute("IsDate", "Y");
					}					
					templateElem.setAttribute(attrName, "");
				}
			}
			/*if(!YFCUtils.isVoid(attrList)) {
				String[] attrArr = attrList.split(",");
				for(String attrName: attrArr) {
					YFCElement columnElem = columnListElem.createChild("Column");
					if(columnMapping.containsKey(attrName)) {
						columnElem.setAttribute("Name", attrName);	
						columnElem.setAttribute("Label", columnMapping.get(attrName));
						templateElem.setAttribute(attrName, "");
					}

				}
			} else {
				Set<String> attrNames = GPS_Order_Export_Vw.getXMLAttributeNames();
				for(String attrName: attrNames) {
					YFCElement columnElem = columnListElem.createChild("Column");
					if(columnMapping.containsKey(attrName)) {
						columnElem.setAttribute("Name", attrName);
						columnElem.setAttribute("Label", columnMapping.get(attrName));
						templateElem.setAttribute(attrName, "");
					}					
				}
			}*/
			@SuppressWarnings("unchecked")
			List<GPS_Order_Export_Vw> resultList = GPS_Order_Export_VwDBHome.getInstance().listWithWhereUnCommited(ctx, pltQry, Integer.MAX_VALUE);
			for(Iterator<GPS_Order_Export_Vw> iter=resultList.iterator();iter.hasNext();) {
				GPS_Order_Export_Vw row = iter.next();
				YFCElement rowElem = row.getXML(outDoc, templateElem,"Data");
				dataListElem.appendChild(rowElem);
			}
		}catch(JSONException je) {
			je.printStackTrace();
			throw new YFSException("JSON Parse Error");
		}
		return outDoc;
	}
	
	private void makeOrderInput(YFCElement orderExportElem,YFCElement criteriaElem) {
		YFCElement orderLineElem = criteriaElem.getChildElement("OrderLine");
		YFCElement itemElem = orderLineElem.getChildElement("Item",true);
		YFCElement orderLineExtnElem = orderLineElem.getChildElement("Extn",true);

		if(!YFCUtils.isVoid(itemElem.getAttribute("ItemID"))) {
			orderExportElem.setAttribute(GPS_Order_Export_Vw.ITEM_ID, itemElem.getAttribute("ItemID"));
			orderExportElem.setAttribute(GPS_Order_Export_Vw.ITEM_ID+"QryType", itemElem.getAttribute("ItemIDQryType"));
		}
		if(!YFCUtils.isVoid(itemElem.getAttribute("ItemShortDesc"))) {
			orderExportElem.setAttribute(GPS_Order_Export_Vw.ITEM_SHORT_DESCRIPTION, itemElem.getAttribute("ItemShortDesc"));
			orderExportElem.setAttribute(GPS_Order_Export_Vw.ITEM_SHORT_DESCRIPTION+"QryType", itemElem.getAttribute("ItemShortDescQryType"));
		}
		if(!YFCUtils.isVoid(orderLineExtnElem.getAttribute("Division"))) {
			orderExportElem.setAttribute(GPS_Order_Export_Vw.COST_CENTER, orderLineExtnElem.getAttribute("Division"));
			orderExportElem.setAttribute(GPS_Order_Export_Vw.COST_CENTER+"QryType", orderLineExtnElem.getAttribute("DivisionQryType"));
		}
		if(!YFCUtils.isVoid(orderLineExtnElem.getAttribute("PriorityCode"))) {
			orderExportElem.setAttribute(GPS_Order_Export_Vw.PRIORITY, orderLineExtnElem.getAttribute("PriorityCode"));
			orderExportElem.setAttribute(GPS_Order_Export_Vw.PRIORITY+"QryType", orderLineExtnElem.getAttribute("PriorityCodeQryType"));
		}
		if(!YFCUtils.isVoid(orderLineExtnElem.getAttribute("MrpController"))) {
			orderExportElem.setAttribute("MrpController", orderLineExtnElem.getAttribute("MrpController"));
			orderExportElem.setAttribute("MrpControllerQryType", orderLineExtnElem.getAttribute("MrpControllerQryType"));
		}
		if(!YFCUtils.isVoid(orderLineExtnElem.getAttribute("VectorID"))) {
			orderExportElem.setAttribute(GPS_Order_Export_Vw.VECTOR_ID, orderLineExtnElem.getAttribute("VectorID"));
			orderExportElem.setAttribute(GPS_Order_Export_Vw.VECTOR_ID+"QryType", orderLineExtnElem.getAttribute("VectorIDQryType"));
		}		
		
		if(!YFCUtils.isVoid(orderLineElem.getAttribute("ShipNode"))) {
			orderExportElem.setAttribute(GPS_Order_Export_Vw.SHIP_NODE, orderLineExtnElem.getAttribute("ShipNode"));
			orderExportElem.setAttribute(GPS_Order_Export_Vw.SHIP_NODE+"QryType", orderLineExtnElem.getAttribute("ShipNodeQryType"));
		}
		if(!YFCUtils.isVoid(orderLineElem.getAttribute("ReceivingNode"))) {
			orderExportElem.setAttribute("ReceivingNode", orderLineElem.getAttribute("ReceivingNode"));
			orderExportElem.setAttribute("ReceivingNodeQryType", orderLineElem.getAttribute("ReceivingNodeQryType"));
		}
		if(!YFCUtils.isVoid(criteriaElem.getAttribute("FromReqDeliveryDate")) || !YFCUtils.isVoid(criteriaElem.getAttribute("ToReqDeliveryDate"))) {
			orderExportElem.setAttribute("ReqDeliveryDateQryType", "BETWEEN");
			orderExportElem.setAttribute("FromReqDeliveryDate", criteriaElem.getAttribute("FromReqDeliveryDate"));
			orderExportElem.setAttribute("ToReqDeliveryDate", criteriaElem.getAttribute("ToReqDeliveryDate"));
		}
		if(!YFCUtils.isVoid(criteriaElem.getAttribute("FromReqShipDate")) || !YFCUtils.isVoid(criteriaElem.getAttribute("ToReqShipDate"))) {
			orderExportElem.setAttribute("ReqShipDateQryType", "BETWEEN");
			orderExportElem.setAttribute("FromReqShipDate", criteriaElem.getAttribute("FromReqShipDate"));
			orderExportElem.setAttribute("ToReqShipDate", criteriaElem.getAttribute("ToReqShipDate"));
		}
		
		
		if(!YFCUtils.isVoid(criteriaElem.getAttribute("FromStatus")) || !YFCUtils.isVoid(criteriaElem.getAttribute("ToStatus"))) {
			orderExportElem.setAttribute("StatusQryType", "BETWEEN");
			orderExportElem.setAttribute("FromStatus", criteriaElem.getAttribute("FromStatus"));
			orderExportElem.setAttribute("ToStatus", criteriaElem.getAttribute("ToStatus"));
		}
		

	}
	
	private void appendComplexQuery(YFCElement criteriaElem,String type) {
		String shipToId;
		if("OrderList".equals(type)) {
			shipToId = criteriaElem.getAttribute(TelstraConstants.SHIP_TO_ID);

			if(!YFCCommon.isVoid(shipToId)) {
				YFCElement orderLineEle = criteriaElem.getChildElement("OrderLine",true);
				YFCElement complexQryDocEle = orderLineEle.createChild(TelstraConstants.COMPLEX_QUERY);
				// If ShipToId is passed the logic is different
				complexQryDocEle.setAttribute(TelstraConstants.OPERATOR, TelstraConstants.AND);
				YFCElement andEle = complexQryDocEle.createChild("And");
				YFCElement shipToOrEle = andEle.createChild(TelstraConstants.OR);
				YFCElement expShipToIdEle = shipToOrEle.createChild(TelstraConstants.EXP);
				expShipToIdEle.setAttribute(TelstraConstants.NAME, TelstraConstants.SHIP_TO_ID);
				expShipToIdEle.setAttribute(TelstraConstants.QRY_TYPE, "LIKE");
				expShipToIdEle.setAttribute(TelstraConstants.VALUE,shipToId);
				YFCElement expCustKeyEle = shipToOrEle.createChild(TelstraConstants.EXP);
				expCustKeyEle.setAttribute(TelstraConstants.NAME, "Extn_"+TelstraConstants.CUSTOMER_KEY);
				expCustKeyEle.setAttribute(TelstraConstants.QRY_TYPE, "LIKE");
				expCustKeyEle.setAttribute(TelstraConstants.VALUE,shipToId);
			}			
		} else {
			shipToId = criteriaElem.getAttribute(TelstraConstants.SHIP_TO_ID);
			criteriaElem.removeAttribute(TelstraConstants.SHIP_TO_ID);
			if(!YFCCommon.isVoid(shipToId)) {
				YFCElement complexQryDocEle = criteriaElem.createChild(TelstraConstants.COMPLEX_QUERY);
				// If ShipToId is passed the logic is different
				complexQryDocEle.setAttribute(TelstraConstants.OPERATOR, TelstraConstants.AND);
				YFCElement andEle = complexQryDocEle.createChild("And");
				YFCElement shipToOrEle = andEle.createChild(TelstraConstants.OR);
				YFCElement expShipToIdEle = shipToOrEle.createChild(TelstraConstants.EXP);
				expShipToIdEle.setAttribute(TelstraConstants.NAME, GPS_Order_Export_Vw.SHIP_TO_ID);
				expShipToIdEle.setAttribute(TelstraConstants.QRY_TYPE, "LIKE");
				expShipToIdEle.setAttribute(TelstraConstants.VALUE,shipToId);
				YFCElement expCustKeyEle = shipToOrEle.createChild(TelstraConstants.EXP);
				expCustKeyEle.setAttribute(TelstraConstants.NAME, GPS_Order_Export_Vw.DAC);
				expCustKeyEle.setAttribute(TelstraConstants.QRY_TYPE, "LIKE");
				expCustKeyEle.setAttribute(TelstraConstants.VALUE,shipToId);
			}			
			
		}
	}

}

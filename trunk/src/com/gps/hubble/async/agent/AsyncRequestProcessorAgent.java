package com.gps.hubble.async.agent;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;

import com.bridge.sterling.consts.ExceptionLiterals;
import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.constants.TelstraErrorCodeConstants;
import com.sterlingcommerce.baseutil.SCUtil;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.ycp.agent.server.YCPAbstractAgent;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

/**
 * Custom Agent classes for creating ItemAssociation. 
 * @author Murali Pachiappan
 * @version 1.0
 * 
 * Extends AbstractCustomBaseAgent class
 * 
 * Functionality
 * @see External methods
 */

public class AsyncRequestProcessorAgent extends YCPAbstractAgent  {
	private static YFCLogCategory logger = YFCLogCategory.instance(AsyncRequestProcessorAgent.class);

	/**
	 * Method picks up message from queue thats was
	 * dropped by getJobs and calls method changesAsyncReuest
	 * method for execution of message to create ItemAssocation
	 * @throws YIFClientCreationException 
	 * @throws RemoteException 
	 * @throws YFSException 
	 */


	/**
	 * Get Jobs method gets message from AsyncRequest table
	 * and send it to queue for execution.
	 */
	@Override
	public List getJobs(YFSEnvironment env, Document arg1, Document lastMessageCreated) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "getJobs", arg1);
		YFCDocument docInXML = YFCDocument.getDocumentFor(arg1);
		List<Document> jobList = new ArrayList<>();

		try {

			if (lastMessageCreated != null) {
				jobList = null;
				return jobList;
			}
			if(!SCUtil.isVoid(docInXML)) {
				String interfaceNo = docInXML.getDocumentElement().getAttribute(TelstraConstants.INTERFACE_NO);
				//HUB-6098 : [Begin]
					if (!YFCCommon.isStringVoid(interfaceNo)){
					YFCDocument asyncRequestInXml = null;
					if (interfaceNo.startsWith("INT_CAT_1")) {
						LoggerUtil.verboseLog("INT_CAT_1 interface", logger, interfaceNo);
						asyncRequestInXml = YFCDocument.getDocumentFor(
								"<AsyncRequest InterfaceNo='INT_CAT_1' InterfaceNoQryType='FLIKE' ErrorCount='0' ErrorCountQryType='GT'> <ComplexQuery Operator='OR'> <And> <Or> <Exp Name='InterfaceNo' Value='"
										+ interfaceNo
										+ "' InterfaceNoQryType='EQ'/> </Or> </And> </ComplexQuery> </AsyncRequest>");
					}
					else{
						LoggerUtil.verboseLog("Interface other than INT_CAT_1", logger, interfaceNo);
						asyncRequestInXml = YFCDocument.getDocumentFor("<AsyncRequestList InterfaceNo='"+interfaceNo+"'/>");
					}
					LoggerUtil.verboseLog("AsyncRequestProcessorAgent :: asyncRequestInXml", logger, asyncRequestInXml);
	
					//HUB-6098 : [End]
					YFCDocument asyncRequestOutXml = YFCDocument.getDocumentFor(YIFClientFactory.getInstance().getApi()
							.executeFlow(env,"GpsGetAsyncRequestList", asyncRequestInXml.getDocument()));
					LoggerUtil.verboseLog("AsyncRequestProcessorAgent :: asyncRequestOutXml", logger, asyncRequestOutXml);
					YFCElement asyncRequestOutEle = asyncRequestOutXml.getDocumentElement();
					YFCIterable<YFCElement> asyncRequestListEle = asyncRequestOutEle.getChildren("AsyncRequest");
					for(YFCElement asyncRequest:asyncRequestListEle) {
						YFCDocument inXml = YFCDocument.createDocument("AsyncRequestList");
						YFCElement inEle = inXml.getDocumentElement();
						inEle.importNode(asyncRequest);
						jobList.add(inXml.getDocument());
					}
				}
			}
		} catch (Exception exp) {
			// ERR9990001=Error in Get Jobs
			throw ExceptionUtil.getYFSException(ExceptionLiterals.ERRORCODE_GET_JOBS, exp);
		}

		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "getJobs", docInXML);
		return jobList;  
	}


	@Override
	public void executeJob(YFSEnvironment env, Document doc) throws RemoteException, YIFClientCreationException {
		YFCDocument inXml = YFCDocument.getDocumentFor(doc);
		changeAsyncRequest(inXml,env);
	}





	/**
	 * Method to call flow/API based on the flag isFlow
	 * with message from the table.
	 * 
	 * On success full execution call deleteAsyncRequest method
	 * to deleted message from the table.
	 * 
	 * On Exception call updateErrorCount to update error count
	 * @param inEle
	 * @throws YIFClientCreationException 
	 * @throws RemoteException 
	 * @throws YFSException 
	 * @throws Exception 
	 */
	private void changeAsyncRequest(YFCDocument inDoc, YFSEnvironment env) throws RemoteException, YIFClientCreationException{

		YFCElement inEle = inDoc.getDocumentElement().getChildElement(TelstraConstants.ASYNC_REQUEST);
		String message = inEle.getAttribute(TelstraConstants.MESSAGE);
		String apiFlowName = inEle.getAttribute(TelstraConstants.SERVICE_NAME);
		String isFlow = inEle.getAttribute(TelstraConstants.IS_FLOW);
		String asyncKey = inEle.getAttribute(TelstraConstants.ASYNCRONOUS_KEY);
		int errorCount = inEle.getIntAttribute(TelstraConstants.ERROR_COUNT);
		YFCDocument inXml = YFCDocument.getDocumentFor(message);
		try{
			if(TelstraConstants.MODIFY_ITEM_ASSOCIATION.equalsIgnoreCase(apiFlowName)){
				appendItemKeyInAssociatedItem(inXml,env);
			}

			if(TelstraConstants.YES.equalsIgnoreCase(isFlow)) {
				YIFClientFactory.getInstance().getApi()
				.executeFlow(env,apiFlowName, inXml.getDocument());

			} else if(TelstraConstants.NO.equalsIgnoreCase(isFlow)) {
				YIFClientFactory.getInstance().getApi()
				.invoke(env,apiFlowName, inXml.getDocument());

			}
			deleteAsyncRequest(asyncKey,env);
		}
		catch(Exception e) {    

			YFCDocument docErrorCountIp = updateErrorCount(asyncKey,errorCount,env); 
			YIFClientFactory.getInstance().getLocalApi().executeFlow(env,TelstraConstants.GPS_ASYNC_REQ_PROCESS_EXCEPTION_DROP_Q,docErrorCountIp.getDocument());
			YFCDocument docAlertReqIp = getAlertIpDoc(inDoc, e.getMessage());
			YIFClientFactory.getInstance().getLocalApi().executeFlow(env,TelstraConstants.GPS_ASYNC_REQ_PROCESS_EXCEPTION_DROP_Q, docAlertReqIp.getDocument());
		throw e;
		}		
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "changeAsyncRequest", inDoc);				

	}

	private YFCDocument getAlertIpDoc(YFCDocument inDoc, String message) {
		
		LoggerUtil.verboseLog(this.getClass().getName()+" :: getAlertIpDoc :: message", logger, message);
		
		YFCDocument docStackMessage = YFCDocument.getDocumentFor(message);
		String sStack = (docStackMessage.getElementsByTagName(TelstraConstants.STACK).item(0)).getNodeValue();

		sStack = sStack.length()> 2000?  sStack.substring(0,2000):sStack;

		YFCDocument docAlertIp = YFCDocument.createDocument();
		YFCElement eleNodeToImp = docAlertIp.importNode(inDoc.getDocumentElement(), true);
		docAlertIp.appendChild(eleNodeToImp);

		YFCElement eleAsync = docAlertIp.getDocumentElement().getChildElement(TelstraConstants.ASYNC_REQUEST);
		eleAsync.setAttribute(TelstraConstants.ERROR_CODE, XPathUtil.getXpathAttribute(docStackMessage, "/Errors/Error/@ErrorCode"));
		eleAsync.setAttribute(TelstraConstants.ERROR_DESCRIPTION, XPathUtil.getXpathAttribute(docStackMessage, "/Errors/Error/@ErrorDescription"));
		eleAsync.setAttribute(TelstraConstants.ERROR_RELATED_MORE_INFO, XPathUtil.getXpathAttribute(docStackMessage, "/Errors/Error/@ErrorRelatedMoreInfo"));
		eleAsync.setAttribute(TelstraConstants.ERROR_STACK_TRACE, sStack);
		return docAlertIp;
	}


	/**
	 * This method fetches the item key for associated item and update in the input xml.
	 * @param inXml
	 * @param env
	 * @return
	 * @throws YFSException
	 * @throws RemoteException
	 * @throws YIFClientCreationException
	 */
	private void appendItemKeyInAssociatedItem(YFCDocument inXml, YFSEnvironment env) throws RemoteException, YIFClientCreationException {

		YFCElement eleItem = inXml.getElementsByTagName(TelstraConstants.ITEM).item(0);
		if(!YFCCommon.isVoid(eleItem)){
			String sAssociatedItemID =  eleItem.getAttribute(TelstraConstants.ITEM_ID);
			if(StringUtils.isNotBlank(sAssociatedItemID)){
				String sAssociatedItemKey = getItemKey(env, sAssociatedItemID);
				if(StringUtils.isNotBlank(sAssociatedItemKey)){
					eleItem.setAttribute(TelstraConstants.ITEM_KEY, sAssociatedItemKey);
				}	
				else{
					throw ExceptionUtil.getYFSException(
							TelstraErrorCodeConstants.CATALOG_ASSOCIATED_ITEM_NOT_FOUND_ERROR_CODE, new YFSException());
				}
			}
		}			
	}

	/**
	 * This method fetches the item key from get item list by passing item id
	 * @param env
	 * @param sItemID
	 * @return sItemKey
	 * @throws RemoteException
	 * @throws YIFClientCreationException
	 */
	private String getItemKey(YFSEnvironment env, String sItemID) throws RemoteException, YIFClientCreationException {


		YFCDocument docGetItemListIp = YFCDocument.getDocumentFor("<Item ItemID='"+sItemID+"'/>");					
		LoggerUtil.verboseLog("AsyncRequestProcessorAgent::getItemKey::docGetItemListIp", logger, docGetItemListIp);
		YFCDocument docGetItemListTemplate = YFCDocument.getDocumentFor("<ItemList><Item ItemID='' ItemKey=''/></ItemList>");
		LoggerUtil.verboseLog("AsyncRequestProcessorAgent::getItemKey::docGetItemListTemplate", logger, docGetItemListTemplate);

		env.setApiTemplate(TelstraConstants.GET_ITEM_LIST, docGetItemListTemplate.getDocument());					
		YFCDocument docGetItemListOp = YFCDocument.getDocumentFor(YIFClientFactory.getInstance().getApi()
				.invoke(env,TelstraConstants.GET_ITEM_LIST, docGetItemListIp.getDocument()));
		env.clearApiTemplate(TelstraConstants.GET_ITEM_LIST);
		
		LoggerUtil.verboseLog("ManageCatalog::getItemKey::docGetItemListOp", logger, docGetItemListOp);
		YFCElement eleItemFromOp = docGetItemListOp.getElementsByTagName(TelstraConstants.ITEM).item(0);
		String sItemKey = TelstraConstants.BLANK;
		if(!YFCCommon.isVoid(eleItemFromOp)){
			sItemKey = eleItemFromOp.getAttribute(TelstraConstants.ITEM_KEY);
		}
		return sItemKey;
	}


	/**
	 * On Exception increment error count for the 
	 * message in AsyncRequest table
	 * @param asyncKey
	 * @param errorCount
	 * @throws YIFClientCreationException 
	 * @throws RemoteException 
	 * @throws YFSException 
	 */

	private YFCDocument updateErrorCount(String asyncKey,int errorCount,YFSEnvironment env) throws RemoteException, YIFClientCreationException {
		int count = errorCount + 1; 
		YFCDocument errorCountXml = YFCDocument.getDocumentFor("<AsyncRequest AsyncronousRequestKey='"+asyncKey+"'"
				+ " ErrorCount='"+count+"'  />");
		return errorCountXml;
			}

	/**
	 * Method to delete the message from the AsyncRequest 
	 * table after Item association is success full.
	 * 
	 * @param asyncKey
	 * @throws YIFClientCreationException 
	 * @throws RemoteException 
	 * @throws YFSException 
	 */
	private void deleteAsyncRequest(String asyncKey,YFSEnvironment env) throws RemoteException, YIFClientCreationException { 
		YFCDocument errorCountXml = YFCDocument.getDocumentFor("<AsyncRequest AsyncronousRequestKey='"+asyncKey+"' />");
		YIFClientFactory.getInstance().getApi()
		.executeFlow(env,"GpsDeleteAsyncRequest", errorCountXml.getDocument());
	}


}
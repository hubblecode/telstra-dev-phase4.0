/***********************************************************************************************
 * File Name        : GpsValidateOrderLineMrpController.java
 *
 * Description      :  Custom API class to validate and update OrderLines for Order with MrpController attribute.
 * 					   This class will be invoked from the integration server asynchronously invoked from the class ManageItemNodeDefinition
 * 
 * Modification Log :
 * ---------------------------------------------------------------------------------------------
 * Ver #    Date            Author					Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0      03/03/2017		Keerthi Yadav			Initial Version- HUB- 8489	
 * 1.1		17/05/2017		Prateek Kumar			HUB-9099: added override flag in the change order request
 * ---------------------------------------------------------------------------------------------
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
 **********************************************************************************************/

package com.gps.hubble.catalog.api;

import java.util.HashMap;
import java.util.Map.Entry;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSException;

/**
 * Sample Input:
 * 
 *<ItemNodeDefn ItemID="00300249" Node="DC12" OrganizationCode="TELSTRA_SCLSA" UnitOfMeasure="M" >
 *  <Extn MaterialStatus="A3" MrpType="VB" GrProcTime="0" DelPeriod="14" MrpController="CWA" ></Extn>
 *</ItemNodeDefn>
 *
 */

public class GpsValidateOrderLineMrpController extends AbstractCustomApi {
	private static YFCLogCategory logger = YFCLogCategory.instance(GpsValidateOrderLineMrpController.class);
	private HashMap <String, YFCDocument> orderMap = null;

	/** 
	 *Base Method: This class will be invoked from the integration server asynchronously invoked from the class ManageItemNodeDefinition.
	 *This class will call change Order Api for every OrderHeaderKey to update the MrpController at the Order Line level for Existing Orders.
	 * 
	 * @param yfcInDoc
	 * @return yfcInDoc
	 * @throws YFSException
	 */

	@Override
	public YFCDocument invoke(YFCDocument yfcInDoc) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(),"invoke", yfcInDoc);
		/* */
		YFCDocument docgetOrderLineListOutput = callGetOrderLineList(yfcInDoc);

		double dTotalLineList = docgetOrderLineListOutput.getDocumentElement().getDoubleAttribute("TotalLineList");

		if(dTotalLineList>0.0){

			/*Create Hash Map with OrderHeaderKey as Key and value as changeOrder input Document */
			createChangeOrderMap(docgetOrderLineListOutput,yfcInDoc);

			/*Update Order Lines with the MrpController for all the Orders */
			callChangeOrder(orderMap);

		}

		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", yfcInDoc);
		return yfcInDoc;
	}

	/**
	 * This method will call change Order Api for all the Orders by Iterating through the Hash Map
	 * @param orderMap
	 */
	private void callChangeOrder(HashMap<String, YFCDocument> orderMap) {
		/*Iterate through hash map and call change Order Api */

		if(!YFCObject.isNull(orderMap) || orderMap.size() > 0){

			for (Entry<String, YFCDocument> entry : orderMap.entrySet()) {
				logger.verbose("Change Order Input Document before Api call : : "+entry.getValue().toString());
				invokeYantraApi("changeOrder", entry.getValue());
			}
		}
	}

	/**
	 * This method will Iterate through OrderLines and create hash map of OrderHeaderKey(Key) and changeOrder Input Document(Value)
	 * @param yfcInDoc
	 */
	private void createChangeOrderMap(YFCDocument docgetOrderLineListOutput,YFCDocument yfcInDoc) {

		orderMap = new HashMap<String, YFCDocument>();

		YFCElement eleExtn = yfcInDoc.getDocumentElement().getChildElement(TelstraConstants.EXTN);
		String sMrpController="";
		if(!YFCObject.isVoid(eleExtn)){
			sMrpController = eleExtn.getAttribute(TelstraConstants.MRP_CONTROLLER,"");
		}

		for(YFCElement eleOrderLine : docgetOrderLineListOutput.getElementsByTagName(TelstraConstants.ORDER_LINE)){
			/*Method to add OrderHeaderKey(Key) and changeOrder Input Document(Value) into Map*/
			addOrderToMap(sMrpController,eleOrderLine);
		}
	}

	/**
	 * This method will add OrderHeaderKey(Key) and changeOrder Input Document(Value) into the map
	 * @param sMrpController
	 * @param eleOrderLine
	 * @param orderMap
	 */
	private void addOrderToMap(String sMrpController, YFCElement eleOrderLine) {

		String sOrderHeaderKey=eleOrderLine.getAttribute(TelstraConstants.ORDER_HEADER_KEY,"");
		String sOrderLineKey=eleOrderLine.getAttribute(TelstraConstants.ORDER_LINE_KEY,"");

		if(!YFCObject.isNull(sOrderHeaderKey) && !YFCObject.isNull(sOrderLineKey)){
			if(YFCObject.isNull(orderMap) || orderMap.size() == 0){
				/*Create New Document for changeOrder*/

				YFCDocument docchangeOrderInput = createChangeOrderInput(sOrderHeaderKey,sOrderLineKey,sMrpController);
				orderMap.put(sOrderHeaderKey, docchangeOrderInput);
			}else{
				if(!orderMap.containsKey(sOrderHeaderKey)){
					/*Create New Document for changeOrder*/

					YFCDocument docchangeOrderInput = createChangeOrderInput(sOrderHeaderKey,sOrderLineKey,sMrpController);
					orderMap.put(sOrderHeaderKey, docchangeOrderInput);
				}else{
					/*append Order Line to Existing Document*/

					YFCDocument docchangeOrderInput = orderMap.get(sOrderHeaderKey);
					YFCElement eleOrderLines = docchangeOrderInput.getElementsByTagName(TelstraConstants.ORDER_LINES).item(0);
					eleOrderLine.setAttribute(TelstraConstants.ACTION, "MODIFY");

					YFCElement eleExtn = eleOrderLine.createChild(TelstraConstants.EXTN);
					eleExtn.setAttribute(TelstraConstants.MRP_CONTROLLER, sMrpController);
					
					importToNode(eleOrderLines,eleOrderLine);

					logger.verbose("Appended Order Line to Change Order Doc : : "+docchangeOrderInput.toString());
				}
			}
		}
	}
	/**
	 * create input document For change Order Api
	 * @param sOrderHeaderKey
	 * @param sOrderLineKey
	 * @param sMrpController
	 * @return
	 */
	private YFCDocument createChangeOrderInput(String sOrderHeaderKey,
			String sOrderLineKey, String sMrpController) {
		YFCDocument docChangeOrderInput = YFCDocument.getDocumentFor("<Order />");
		YFCElement eleOrder = docChangeOrderInput.getDocumentElement();

		eleOrder.setAttribute(TelstraConstants.ACTION, "MODIFY");
		eleOrder.setAttribute(TelstraConstants.OVERRIDE, "Y");
		eleOrder.setAttribute(TelstraConstants.ORDER_HEADER_KEY, sOrderHeaderKey);

		YFCElement eleOrderLines = eleOrder.createChild(TelstraConstants.ORDER_LINES);
		YFCElement eleOrderLine = eleOrderLines.createChild(TelstraConstants.ORDER_LINE);

		eleOrderLine.setAttribute(TelstraConstants.ACTION, "MODIFY");
		eleOrderLine.setAttribute(TelstraConstants.ORDER_LINE_KEY, sOrderLineKey);
		eleOrderLine.setAttribute(TelstraConstants.OVERRIDE, "Y");

		YFCElement eleExtn = eleOrderLine.createChild(TelstraConstants.EXTN);

		eleExtn.setAttribute(TelstraConstants.MRP_CONTROLLER, sMrpController);
		logger.verbose("Creating Change Order : Input Document : : "+docChangeOrderInput.toString());

		return docChangeOrderInput;
	}

	/**
	 * This method will call getOrderLineList api for PO with ItemID, UOM and Node.
	 * @param sItemId
	 * @param sUOM
	 * @param sReceivingNode
	 */
	private YFCDocument callGetOrderLineList(YFCDocument yfcInDoc) {

		String sItemId = yfcInDoc.getDocumentElement().getAttribute(TelstraConstants.ITEM_ID,"");
		String sUOM = yfcInDoc.getDocumentElement().getAttribute(TelstraConstants.UNIT_OF_MEASURE,"");
		String sReceivingNode = yfcInDoc.getDocumentElement().getAttribute(TelstraConstants.NODE,"");

		YFCDocument docgetOrderLineListInput = YFCDocument.getDocumentFor("<OrderLine ReceivingNode='"+sReceivingNode+"'>"
				+ "<Item ItemID='"+sItemId+"' UnitOfMeasure='"+sUOM+"' /><Order DocumentType='0005' OrderComplete='N' /></OrderLine >");

		YFCDocument docgetOrderLineListTemp = YFCDocument.getDocumentFor("<OrderLineList TotalLineList=''><OrderLine "
				+ "OrderHeaderKey='' OrderLineKey=''></OrderLine></OrderLineList>");

		logger.verbose("getOrderLineList Api : : Input Document: : "+docgetOrderLineListInput.toString());


		return invokeYantraApi("getOrderLineList", docgetOrderLineListInput,docgetOrderLineListTemp);

	}
	
/**
* It is a helper method to import node to the specific element.
*
* @param yfcEleParentEle
* @param yfcEleChildEle
*/
private void importToNode(YFCElement yfcEleParentEle,
              YFCElement yfcEleChildEle) {
       LoggerUtil.verboseLog("yfcEleParentEle :", logger, yfcEleParentEle);
       LoggerUtil.verboseLog("yfcEleChildEle :", logger, yfcEleChildEle);
       YFCDocument yfcDocOwnerDoc = yfcEleParentEle.getOwnerDocument();
       YFCElement newEle = yfcDocOwnerDoc.importNode(yfcEleChildEle, true);
       yfcEleParentEle.appendChild(newEle);
}

}
/***********************************************************************************************
 * File Name        : GpsPriceMonitor.java
 *
 * Description      :  HUB-9550: Contract Price Check
 * 
 * Modification Log :
 * ---------------------------------------------------------------------------------------------
 * Ver #    Date            Author                              Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0      26/09/2017      Gladston Xavier                   Initial Version
 * ---------------------------------------------------------------------------------------------
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
 **********************************************************************************************/
package com.gps.hubble.catalog.api;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfs.japi.YFSException;

/**
 * Sample Input:
 * 
 * <ItemNodeDefn ItemID="00300249" Node="DC12" OrganizationCode="TELSTRA_SCLSA" UnitOfMeasure="M" >
 * <Extn MaterialStatus="A3" MrpType="VB" GrProcTime="0" DelPeriod="14" MrpController="CWA"
 * UnitCost='' OldUnitCost=''  ></Extn> </ItemNodeDefn>
 * 
 */
public class GpsPriceMonitor extends AbstractCustomApi {


  @Override
  public YFCDocument invoke(YFCDocument inXml) throws YFSException {

    double dDollarValue = Double.parseDouble(getProperty("DollarValue", true));
    double dVarThreshold = Double.parseDouble(getProperty("AllowedVariancePercentage", true));
    String sAlertService = getProperty("AlertService", true);
    
    YFCElement extnElem = inXml.getDocumentElement().getElementsByTagName("Extn").item(0);
    double dUnitCost = extnElem.getDoubleAttribute("UnitCost");
    double dContractPrice = getContractPrice(inXml);
    double dOldUnitCost = extnElem.getDoubleAttribute("OldUnitCost");
    boolean isAlerted = extnElem.getBooleanAttribute("IsAlerted");
    if(dContractPrice==0){
      dContractPrice=dOldUnitCost;
    }
    double dDifference = Math.abs(dContractPrice - dUnitCost);
    double dVariance = dDifference / dContractPrice;
    
    if(isAlerted){
      if (dVariance < dVarThreshold || dDifference <= dDollarValue) {
        extnElem.setAttribute("IsAlerted", false);
      }
    }else{
      if (dDifference > dDollarValue && dVariance >= dVarThreshold) {
        extnElem.setAttribute("IsAlerted", true);
        invokeYantraService(sAlertService, inXml);
      }
    }
    extnElem.removeAttribute("OldUnitCost");
    return inXml;
  }

  private double getContractPrice(YFCDocument inXml) {
    YFCDocument getConPriceListDoc = getConPriceList(inXml);
    if(!getConPriceListDoc.getDocumentElement().hasChildNodes()){
      return 0;
    }
    YFCElement conPriceElem = getConPriceListDoc.getDocumentElement().getElementsByTagName("ContractPrice").item(0);
    return conPriceElem.getDoubleAttribute("UnitPrice");
        
  }

  private YFCDocument getConPriceList(YFCDocument inXml) {

    String strItemID = inXml.getDocumentElement().getAttribute("ItemID");
    String strCurrentDate = getDBDate().getYDate().getString("yyyy-MM-dd");
    YFCDocument getConPriceListDoc =
        YFCDocument.getDocumentFor("<ContractPrice ItemID='" + strItemID + "' ContractStartDate='"
            + strCurrentDate + "' ContractStartDateQryType='LE' ContractEndDate='" + strCurrentDate
            + "' ContractEndDateQryType='GE'></ContractPrice>");
    YFCDocument getConPriceListOPDoc =
        invokeYantraService("GpsGetContractPriceList", getConPriceListDoc);
    if (!getConPriceListOPDoc.getDocumentElement().hasChildNodes()) {
      getConPriceListDoc =
          YFCDocument
              .getDocumentFor("<ContractPrice ItemID='" + strItemID + "' > </ContractPrice>");
      getConPriceListOPDoc = invokeYantraService("GpsGetContractPriceList", getConPriceListDoc);
    }
    return getConPriceListOPDoc;
  }

}

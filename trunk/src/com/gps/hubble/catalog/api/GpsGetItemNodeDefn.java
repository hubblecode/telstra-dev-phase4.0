package com.gps.hubble.catalog.api;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;

import com.sterlingcommerce.baseutil.SCUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCException;

public class GpsGetItemNodeDefn extends AbstractCustomApi {

	private static YFCLogCategory logger = YFCLogCategory.instance(GpsGetItemNodeDefn.class);
	PreparedStatement statement;
	YFCDocument retDoc;

	public YFCDocument invoke(YFCDocument inpDoc)   {

		try{

			LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke",inpDoc);

			String itemID=inpDoc.getDocumentElement().getAttribute("ItemID");
			String organizationCode=inpDoc.getDocumentElement().getAttribute("OrganizationCode");
			String uom=inpDoc.getDocumentElement().getAttribute("UnitOfMeasure");
			String shipNode=inpDoc.getDocumentElement().getAttribute("Node");
			int maxrec= 0;
			
			if(SCUtil.isVoid(itemID)){
				throw new YFCException("Item ID is null in the input");
			}

			if(SCUtil.isVoid(organizationCode)){
				throw new YFCException("Organization Code is null in the input");
			}
			
			if(SCUtil.isVoid(inpDoc.getDocumentElement().getAttribute("MaximumRecords"))){
				maxrec=100;				
			}				
			else{
				maxrec= Integer.parseInt(inpDoc.getDocumentElement().getAttribute("MaximumRecords"));				
			}

			Connection connection= getServiceInvoker().getDBConnection();
			try (PreparedStatement ps = createQuery(connection, itemID, organizationCode, uom, shipNode);
					ResultSet rs = ps.executeQuery()) {
				retDoc = constructDocument(rs, maxrec);
				ps.close();
				rs.close();

			}catch(SQLException sqlEx){
				logger.error("SQLException while executing query ",sqlEx);
				throw new YFCException(sqlEx);
			}

			return retDoc;
		}finally {
			LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke",inpDoc);
		}
	}

	public YFCDocument constructDocument(ResultSet rs, int maxrec) throws SQLException
	{

		YFCDocument nodeDefnListDoc= YFCDocument.createDocument("ItemNodeDefnList");
		YFCElement elem=nodeDefnListDoc.getDocumentElement();
		YFCElement eleNodes=elem.createChild("Nodes");
		int i=0;

		while(rs.next()){

			YFCElement node= eleNodes.createChild("Node");
			if(i< maxrec){
				YFCElement nodeDefnElem=elem.createChild("ItemNodeDefn");

				nodeDefnElem.setAttribute("ItemID", rs.getString("ITEM_ID").trim());
				nodeDefnElem.setAttribute("OrganizationCode", rs.getString("ORGANIZATION_CODE").trim());
				
				
				if(rs.getString("UOM")!= null){
					nodeDefnElem.setAttribute("UnitOfMeasure", rs.getString("UOM").trim());
				}
				else{
					nodeDefnElem.setAttribute("UnitOfMeasure", "");
				}
				
				if(rs.getString("EXTN_UNIT_COST") != null){
					nodeDefnElem.setAttribute("ExtnUnitCost", rs.getString("EXTN_UNIT_COST").trim());	
				}
				else{
					nodeDefnElem.setAttribute("ExtnUnitCost", "");
				}
				
				if(rs.getString("EXTN_IS_ALERTED") != null){
                  nodeDefnElem.setAttribute("IsAlerted", rs.getString("EXTN_IS_ALERTED").trim());   
                }
                else{
                    nodeDefnElem.setAttribute("IsAlerted", "");
                }
				
				nodeDefnElem.setAttribute("Node", rs.getString("SHIP_NODE").trim());
				nodeDefnElem.setAttribute("NodeName", rs.getString("DESCRIPTION").trim());

			}

			node.setAttribute("Node", rs.getString("SHIP_NODE").trim());
			node.setAttribute("NodeName", rs.getString("DESCRIPTION").trim());
			i++; 

		}
		logger.verbose("ItemNodeDefnDoc"+ nodeDefnListDoc);

		return nodeDefnListDoc;

	}

	private PreparedStatement createQuery(Connection connection, String itemID, String organizationCode, String uom, String shipNode) {
		// TODO Auto-generated method stub

		String query="SELECT YSN.SHIP_NODE,YSN.DESCRIPTION,YIND.ITEM_ID,YIND.UOM, YIND.ORGANIZATION_CODE,YIND.EXTN_UNIT_COST " 
				+" FROM YFS_SHIP_NODE YSN JOIN YFS_ITEM_NODE_DEFN YIND "
				+" ON YSN.SHIP_NODE=YIND.NODE " 
				+" WHERE YIND.ITEM_ID= ? "
				+" AND YIND.ORGANIZATION_CODE= ? "
				+" AND YIND.UOM= ? " ;

		if(!SCUtil.isVoid(shipNode)){
			query= query.concat("AND YSN.SHIP_NODE=? ");			
		}

		try {
			statement= connection.prepareStatement(query);
			statement.setString(1, itemID);
			statement.setString(2, organizationCode);
			statement.setString(3, uom);

			if(!SCUtil.isVoid(shipNode)){
				statement.setString(4, shipNode);		
			}


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return statement;
	}



}

/***********************************************************************************************
 * File	Name		: ManageVectorCatalog.java
 *
 * Description		: This class is called from the catalog interface when the InterfaceNo is 'VEC_CAT_1'.
 * 						It will create vector catalog and add item association to and from Integral Catalog, if it exists.
 * 
 * Modification	Log	:
 * ---------------------------------------------------------------------------------------------
 * Ver #	Date			Author					Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0		July 08,2017	  	Manish Kumar 		   	Initial	Version 
 * ---------------------------------------------------------------------------------------------
 * 
 */
package com.gps.hubble.catalog.api;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.sterlingcommerce.baseutil.SCUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSException;

/**
 * Custom API to create vector catalog and add associations with Integral Catalog, if it exists.
 * 
 * @author Manish Kumar
 * @version 1.0
 *
 * Extends AbstractCustomApi Class
 */


public class ManageVectorCatalog extends AbstractCustomApi
{
	
	private static YFCLogCategory logger = YFCLogCategory.instance(ManageVectorCatalog.class);
	
	/*
	 * yfInDoc : Input to the service 
	 * Sample Input: 	<ItemList>
	 * 						<Item Action="Manage" ItemID="CR2KPP" OrganizationCode="TELSTRA_SCLSA" UnitOfMeasure="EA">
	 * 							<PrimaryInformation Description="CR2000 CORDLESS (FEAT. PKGE)" IsHazmat="N" 
	 * 									ManufacturerItem="CR2KPP" ShortDescription="CR2000 CORDLESS (FEAT. PKGE)" 
	 * 									Status="3000" UnitCost="43.78"/>
	 * 							<AdditionalAttributeList>
	 * 								<AdditionalAttribute AttributeDomainID='ItemAttribute' 
	 * 										AttributeGroupID='ItemReferenceGroup' Name='SOURCE_SYSTEM' Value='VECTOR'/>
	 * 							</AdditionalAttributeList>
	 * 						</Item>
	 * 					</ItemList>
	 */
	public YFCDocument invoke(YFCDocument yfcInDoc) throws YFSException 
	{
		LoggerUtil.startComponentLog(logger, this.getClass().getName(),"invoke", yfcInDoc);
		
		logger.verbose("Input to ManageVectorCatalog Class: " + yfcInDoc.toString());
		
		YFCNodeList<YFCElement> yfcItemNL = yfcInDoc.getElementsByTagName("Item");
        for(YFCElement yfcItemEle : yfcItemNL) {
        	String strIntegralItemID = yfcItemEle.getAttribute("ItemID","");
        	String strOrganizationCode = yfcItemEle.getAttribute("OrganizationCode","");
        	String strUOM = yfcItemEle.getAttribute("UnitOfMeasure","");
        	String strVectorItemID = "";
        	YFCDocument yfcIntegralAssociationDoc = null;
        	YFCDocument yfcVectorAssociationDoc = null;
        	
        	logger.verbose("Item ID: " + strIntegralItemID);
        	logger.verbose("Catalog Organization Code: " + strOrganizationCode);
        	logger.verbose("Vector UOM: " + strUOM);
        	
        	YFCElement yfcItemAliasListEle = yfcItemEle.getChildElement("ItemAliasList");
        	if(yfcItemAliasListEle!=null){
        	  YFCNodeList<YFCElement> yfcItemAliasNL = yfcItemAliasListEle.getElementsByTagName("ItemAlias");
              if(yfcItemAliasNL!=null){
                for(YFCElement yfcItemAliasEle : yfcItemAliasNL) {
                  String strAliasName = yfcItemAliasEle.getAttribute("AliasName","");
                  if(strAliasName.equalsIgnoreCase("VECTOR_ITEM")) {
                      strVectorItemID = yfcItemAliasEle.getAttribute("AliasValue","");
                      if(!strVectorItemID.equals("")) {
                          yfcItemAliasEle.setAttribute("AliasName", "INTEGRAL_ITEM");
                          yfcItemAliasEle.setAttribute("AliasValue", strIntegralItemID);
                          yfcItemEle.setAttribute("ItemID", strVectorItemID);
                          
                          logger.verbose("Integral Item ID: " + strIntegralItemID);
                          logger.verbose("Vector Item ID: " + strVectorItemID);
                                          
                          //Create Association Document from Integral Item to Vector Item
                          yfcIntegralAssociationDoc = createItemAssociationDoc(strIntegralItemID, "", strOrganizationCode, strVectorItemID);
                          
                          //Create Association Document from Vector Item to Integral Item
                          yfcVectorAssociationDoc = createItemAssociationDoc(strVectorItemID, strUOM, strOrganizationCode, strIntegralItemID);
                      }
                  }
              }
              }
             
        	}
        	
        	
        	YFCDocument yfcManageItemOutDoc = YFCDocument.getDocumentFor("<ItemList/>");
        	YFCElement yfcManageItemRootEle = yfcManageItemOutDoc.getDocumentElement();
        	YFCElement yfcItemOutEle = yfcManageItemOutDoc.importNode(yfcItemEle, true);
        	yfcManageItemRootEle.appendChild(yfcItemOutEle);
        	
        	YFCElement yfcAdditionalAttributeListEle = yfcItemOutEle.createChild("AdditionalAttributeList");
        	YFCElement yfcAdditionalAttributeEle = yfcAdditionalAttributeListEle.createChild("AdditionalAttribute");
        	yfcAdditionalAttributeEle.setAttribute("AttributeDomainID", "ItemAttribute");
        	yfcAdditionalAttributeEle.setAttribute("AttributeGroupID", "ItemReferenceGroup");
        	yfcAdditionalAttributeEle.setAttribute("Name", "SOURCE_SYSTEM");
        	yfcAdditionalAttributeEle.setAttribute("Value", "VECTOR");
        	
        	//Input YFCDocument to manageItem API
        	droptoCatalogQ(yfcManageItemOutDoc);
        	//Input YFCDocument to GpsCreateAsyncRequestService that will internally invoke modifyItemAssociations
        	droptoCatalogQ(yfcIntegralAssociationDoc);
        	//Input YFCDocument to GpsCreateAsyncRequestService that will internally invoke modifyItemAssociations
        	droptoCatalogQ(yfcVectorAssociationDoc);
        }
        
        sendEOF();
		
		LoggerUtil.endComponentLog(logger, this.getClass().getName(),"invoke", yfcInDoc);
		return yfcInDoc;
	}
	

	private void sendEOF() {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "sendEOF", "");
		
		YFCDocument yfcAssociationDoc = YFCDocument.getDocumentFor("<EndOfCatalogFile CriteriaId='ASYNC_REQ_1' InterfaceNo='INT_CAT_1' YantraMessageGroupID='ManageCatalog' />");
		droptoCatalogQ(yfcAssociationDoc);
		
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "sendEOF", "");
		
	}


	/**
	 * 
	 * @param yfcInDoc : Input YFCDocument to manageItem API
	 * @param yfcIntegralAssociationDoc : Input YFCDocument to GpsCreateAsyncRequestService that will internally invoke modifyItemAssociations  
	 * @param yfcVectorAssociationDoc : Input YFCDocument to GpsCreateAsyncRequestService that will internally invoke modifyItemAssociations
	 */
	private void droptoCatalogQ(YFCDocument yfcInDoc) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "droptoCatalogQ", yfcInDoc);
		
		if((yfcInDoc!=null)) {
			logger.verbose("Dropping to Catalog Q: " + yfcInDoc.toString());
    		invokeYantraService("GpsCatalogDrop", yfcInDoc);
    	}
		else {
			logger.verbose("Not Dropping to Catalog Q, as the input is null");
		}
		
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "droptoCatalogQ", yfcInDoc);
	}

	/**
	 * 
	 * @param strParentItemID : The ItemID of the parent item with which the association needs to be defined.
	 * @param strUOM : The Unit of Measure of the parent item. If not passed, it will be fetched from the catalog. 
	 * 						If item is not in the catalog, association will not be defined.
	 * @param strOrganizationCode : The Catalog Organization Code
	 * @param strAssociatedItemID : The ItemID of the item that needs to be associated.
	 * @return : The input YFCDocument to create item association. 
	 */
	private YFCDocument createItemAssociationDoc(String strParentItemID, String strUOM, String strOrganizationCode, String strAssociatedItemID) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "createItemAssociationDoc", strParentItemID);
		if(strUOM.equals("")) {
			logger.verbose("The Unit Of Measure Passed in the input is null");
			logger.verbose("Invoking getItemList API to get the UOM");
			/*
			 * The UOM will not be passed for Integral Items.
			 * This is because Integral Items can have different Unit Of Measures.
			 * Whereas, Vector Items will only EA as the Unit Of Measure.
			 */
			//call getItemList to get the UOM from the catalog.
			YFCDocument yfcGetItemListInDoc = YFCDocument.getDocumentFor("<Item ItemID='"+strParentItemID+"'/>");
			YFCDocument yfcGetItemListTemplate = YFCDocument.getDocumentFor("<ItemList TotalNumberOfRecords=''><Item ItemID='' ItemKey='' UnitOfMeasure=''/></ItemList>");
			logger.verbose("Input to getItemList API: " + yfcGetItemListInDoc.toString());
			YFCDocument yfcGetItemListOutDoc =  invokeYantraApi("getItemList", yfcGetItemListInDoc, yfcGetItemListTemplate);
			logger.verbose("Output from getItemList API: " + yfcGetItemListOutDoc.toString());
			YFCElement yfcItemListEle = yfcGetItemListOutDoc.getDocumentElement();
			int iTotalRecords = yfcItemListEle.getIntAttribute("TotalNumberOfRecords",0);
			if(iTotalRecords==0) {
				/*
				 * If getItemList returned 0 records, it means the Integral Item is not created in the catalog.
				 * In this case, do not create the association.
				 */
				logger.verbose("Item does not exist in the catalog");
				logger.verbose("Hence, association will not be created");
				//return null;
			}
			else {
				strUOM = yfcItemListEle.getChildElement("Item").getAttribute("UnitOfMeasure","");
				logger.verbose("UOM from getItemList output: " + strUOM);
			}
		}
		
		YFCDocument yfcAssociationDoc = YFCDocument.getDocumentFor("<AssociationList ItemID='"+strParentItemID+"' UnitOfMeasure='"+strUOM+"' "
																+ 		"OrganizationCode='"+strOrganizationCode+"' InterfaceNo='INT_CAT_1'>"
																+ 	"<Association AssociationType='Alternative.Y' EffectiveFrom='2001-01-01T00:00:00' "
																+ 		"EffectiveTo='2099-01-01T00:00:00'>"
																+ 		"<Item ItemID='"+strAssociatedItemID+"'/>"
																+ 	"</Association></AssociationList>");
		
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "createItemAssociationDoc", strParentItemID);
		return yfcAssociationDoc;
	}

}

package com.gps.hubble.catalog.api;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;

/**
 * Custom API class to manage Catalog from different 
 * platform. API will get input from queues using 
 * Integration Server.
 * 
 * @author Murali Pachiappan
 * @version 1.0
 * 
 * Extends AbstractCustomApi Class
 * 
 * Functionality
 * @see External methods
 */
public class ManageCatalog extends AbstractCustomApi {

	/**
	 * Base method initial execution starts.
	 * Method calls different API/flow based on 
	 * root element.
	 * If root Element is ItemList then createItem API
	 * If root Element is ItemNodeDefn then flow GpsManageItemNodeDefn
	 * if root Element is AssociationList then createAsynRequest method
	 * if root Element is EOF then triggerAgentAPI method are called.
	 */
	private static YFCLogCategory logger = YFCLogCategory.instance(ManageCatalog.class);


	@Override
	public YFCDocument invoke(YFCDocument inXml) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(),"invoke", inXml);
		String rootElement = inXml.getDocumentElement().getNodeName();
		if("ItemList".equals(rootElement)) {
			if((inXml.getDocumentElement().getAttribute("InterfaceNo","")).equalsIgnoreCase("VEC_CAT_1")) {
				invokeYantraService("GpsManageVectorCatalog", inXml);
			}
			else {
				invokeYantraApi("manageItem", inXml);
			}
		} else if("ItemNodeDefn".equals(rootElement)){
			invokeYantraService("GpsManageItemNodeDefn", inXml);
		} else if("AssociationList".equals(rootElement)) {    	
			createAsynRequest(inXml);
		} else if("EndOfCatalogFile".equals(rootElement)){
			triggerAgentAPI(inXml);
		}
		LoggerUtil.endComponentLog(logger, this.getClass().getName(),"invoke", inXml);
		return inXml;
	}

	/**
	 * Method create input document for trigger agent and
	 * calls triggerAgent API based on EOF message
	 * 
	 * @param inXml
	 */
	private void triggerAgentAPI(YFCDocument inXml) {

		YFCDocument triggerAgentXml=createInputXMLForTriggerAgent(inXml);
		invokeYantraApi("triggerAgent", triggerAgentXml);
	}

	/**
	 * Method to create input document for triggerAgent API
	 * 
	 * @param interfaceNo
	 * @param inXml
	 * @return
	 */
	private YFCDocument createInputXMLForTriggerAgent(YFCDocument inXml) {
		YFCDocument triggerAgentXml = YFCDocument.getDocumentFor("<TriggerAgent><CriteriaAttributes>"
				+ "<Attribute/></CriteriaAttributes></TriggerAgent>");
		YFCElement  triggerAgentEle = triggerAgentXml.getDocumentElement();
		String agentCriteria = inXml.getDocumentElement().getAttribute("CriteriaId");
		String interfaceNo = inXml.getDocumentElement().getAttribute(TelstraConstants.INTERFACE_NO);
		triggerAgentEle.setAttribute("CriteriaId", agentCriteria);
		YFCElement triggerAgentAttrEle = triggerAgentEle.getChildElement("CriteriaAttributes").getChildElement("Attribute");
		triggerAgentAttrEle.setAttribute("Name",TelstraConstants.INTERFACE_NO);
		triggerAgentAttrEle.setAttribute("Value", interfaceNo);
		return triggerAgentXml;
	}

	/**
	 * Method creates input document and calls custom flow 
	 * to add message to AsyncRquest table that will be
	 * processed by custom agent at EOF message 
	 * @param inXml
	 */
	private void createAsynRequest(YFCDocument inXml) {
		YFCDocument asynRequestXml = YFCDocument.getDocumentFor("<CreateAsyncRequest />");
		String message = inXml.getDocumentElement().getString().replace("\\<\\?xml(.+?)\\?\\>", "").trim();
		String interfaceNo = inXml.getDocumentElement().getAttribute(TelstraConstants.INTERFACE_NO);
		asynRequestXml.getDocumentElement().setAttribute("ServiceName", "modifyItemAssociations");
		asynRequestXml.getDocumentElement().setAttribute("Message", message);
		asynRequestXml.getDocumentElement().setAttribute("TransactionReferenceNo", getTransactionReferenceNo(message));

		asynRequestXml.getDocumentElement().setAttribute(TelstraConstants.INTERFACE_NO, interfaceNo);
		asynRequestXml.getDocumentElement().setAttribute("IsFlow", "N");
		invokeYantraService("GpsCreateAsyncRequest", asynRequestXml);
	}

	/**
	 * This method returns the parent Item ID from item association xml
	 * @param message
	 * @return ItemID
	 */
	private String getTransactionReferenceNo(String message) {

		YFCDocument docItemAssocation = YFCDocument.getDocumentFor(message);
		YFCElement eleAssociationList = docItemAssocation.getDocumentElement();
		return eleAssociationList.getAttribute(TelstraConstants.ITEM_ID);
	}

}
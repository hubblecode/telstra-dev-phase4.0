/***********************************************************************************************
 * File Name        : ManageItemNodeDefinition.java
 *
 * Description      :  Custom API class to create or modify ItemNodeDefn
 * 
 * Modification Log :
 * ---------------------------------------------------------------------------------------------
 * Ver #    Date            Author                              Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0      28/06/2016      Murali Pachiappan                   Initial Version
 * 1.1      05/03/2017		Keerthi Yadav						HUB-8489: MRP controller is not getting stamp, if item node defn is created post order line creation
 * 1.2      27/09/2017      Gladston Xavier                     HUB-9550: Contract Price Check
 * ---------------------------------------------------------------------------------------------
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
 **********************************************************************************************/
package com.gps.hubble.catalog.api;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
/**
 * Sample Input:
 * 
 *<ItemNodeDefn ItemID="00300249" Node="DC12" OrganizationCode="TELSTRA_SCLSA" UnitOfMeasure="M" >
 *  <Extn MaterialStatus="A3" MrpType="VB" GrProcTime="0" DelPeriod="14" MrpController="CWA" IsAlerted='Y' ></Extn>
 *</ItemNodeDefn>
 *
 */

public class ManageItemNodeDefinition extends AbstractCustomApi {

	/**
	 * Base method initial execution starts.
	 * 
	 * Method gets ItemNodeDefn list of the item and
	 * calls createOrModifyItemNodeDefn method
	 * 
	 */
	@Override
	public YFCDocument invoke(YFCDocument inXml) {
		YFCDocument itemNodeDefnTemp = YFCDocument.getDocumentFor("<ItemNodeDefnList TotalNumberOfRecords=''><ItemNodeDefn ItemNodeDefnKey=''><Extn MrpController='' UnitCost='' IsAlerted='' /></ItemNodeDefn></ItemNodeDefnList>");
		YFCDocument getItemNodeList = prepareXml(inXml);
		YFCDocument itemNodeDefn = invokeYantraApi("getItemNodeDefnList", getItemNodeList,itemNodeDefnTemp);
		createOrModifyItemNodeDefnandOrderLine(inXml,itemNodeDefn);
		return inXml;
	}


	/**
	 * Method validate count from get ItemNodeDefnList If list 
	 * count is 0 then calls createItemNodeDefn else 
	 * calls modifyItemNodeDefn 
	 * 
	 * @param inXml
	 * @param count
	 */
	private void createOrModifyItemNodeDefnandOrderLine(YFCDocument inXml , YFCDocument itemNodeDefn) {
		String count = itemNodeDefn.getDocumentElement().getAttribute("TotalNumberOfRecords");

		//HUB-8489	[START]

		String sMrpController = "";
		String sExistingMrpController = "";
		Double dUnitCost = 0.0;
		Double dExistingUnitCost = 0.0;
		boolean isAlerted = false;

		YFCElement eleExtn = inXml.getDocumentElement().getElementsByTagName(TelstraConstants.EXTN).item(0);
		if(!YFCObject.isVoid(eleExtn)){
			sMrpController = eleExtn.getAttribute(TelstraConstants.MRP_CONTROLLER,"");
			dUnitCost = eleExtn.getDoubleAttribute("UnitCost",0.0);
		}


		if("0".equals(count)) {
			invokeYantraApi("createItemNodeDefn", inXml);

			/*When the Node Definition is created for the First time No existing orders would have the MrpController stamped. therefore,
       			update the OrderLines with the MrpController.*/

			if(!YFCObject.isVoid(sMrpController)){
				/*This service will drop message into a queue and will be picked up by a service to validate and call change Order Api 
				 * for every OrderHeaderKey to update the MrpController at the Order Line level.*/

				invokeYantraService("GpsDropOrderMrpCtrlQ", inXml);
			}
		}else{

			
			/*The assumption is that when the Node Definition is modified and the existing MrpController is different
			 *  from the new MrpController(null or new value) call async service update the created orderLines, if its the same don't do anything */

			YFCElement eleExistingExtn = itemNodeDefn.getDocumentElement().getElementsByTagName(TelstraConstants.EXTN).item(0);
			if(!YFCObject.isVoid(eleExistingExtn)){
				sExistingMrpController = eleExistingExtn.getAttribute(TelstraConstants.MRP_CONTROLLER,"");
				dExistingUnitCost = eleExistingExtn.getDoubleAttribute("UnitCost",0.0);
				isAlerted = eleExistingExtn.getBooleanAttribute("IsAlerted");
				eleExtn.setAttribute("IsAlerted", isAlerted);
			}

			if(!YFCObject.equals(sExistingMrpController, sMrpController)){
				/*This service will drop message into a queue and will be picked up by a service to validate and call change Order Api 
				 * for every OrderHeaderKey to update the MrpController at the Order Line level.*/

				invokeYantraService("GpsDropOrderMrpCtrlQ", inXml);
			}
			
			if(dUnitCost!=dExistingUnitCost){
              eleExtn.setAttribute("OldUnitCost", dExistingUnitCost);
              inXml=invokeYantraService("GpsPriceMonitor",inXml);
            }
			
			
			invokeYantraApi("modifyItemNodeDefn", inXml);
		}
	}

	//HUB-8489	[END]

	/**
	 * Method to create input XML for get ItemNodeDefnList
	 * 
	 * @param inXml
	 * @return getItemNodeList
	 */
	private YFCDocument prepareXml(YFCDocument inXml) {
		YFCDocument getItemNodeList = YFCDocument.getDocumentFor("<ItemNodeDefn />");
		getItemNodeList.getDocumentElement().setAttribute(TelstraConstants.ITEM_ID,
				inXml.getDocumentElement().getAttribute(TelstraConstants.ITEM_ID));
		getItemNodeList.getDocumentElement().setAttribute("UnitOfMeasure",
				inXml.getDocumentElement().getAttribute("UnitOfMeasure"));
		getItemNodeList.getDocumentElement().setAttribute("Node",
				inXml.getDocumentElement().getAttribute("Node"));
		getItemNodeList.getDocumentElement().setAttribute("OrganizationCode",
				inXml.getDocumentElement().getAttribute("OrganizationCode"));

		return getItemNodeList;
	}
}
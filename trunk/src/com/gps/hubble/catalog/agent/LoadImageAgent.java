package com.gps.hubble.catalog.agent;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;

import com.yantra.util.YFCUtils;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSException;

import com.bridge.sterling.framework.agent.AbstractCustomBaseAgent;
import com.bridge.sterling.utils.LoggerUtil;

public class LoadImageAgent extends AbstractCustomBaseAgent {
	private static YFCLogCategory logger = YFCLogCategory.instance(LoadImageAgent.class);

	public List<YFCDocument> getJobs(YFCDocument msgXml, YFCDocument lastMsgXml) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "getJobs", msgXml);
		
		List<YFCDocument> fileList = new ArrayList<>();
		YFCElement eleMsgXml = msgXml.getDocumentElement();
		String sInputDir = eleMsgXml.getAttribute("InputDir");
		String sCompDir = eleMsgXml.getAttribute("SuccessDir");
		String sErrorDir = eleMsgXml.getAttribute("ErrorDir");
		
		if (YFCUtils.isVoid(sInputDir) || YFCUtils.isVoid(sCompDir)
				|| YFCUtils.isVoid(sErrorDir)) {
			throw new YFSException("Agent Params Configuration Missing",
					"Agent Params Configuration Missing",
					"Directories configuration is required");
		}
		
		List<File> filesToBeProcessed = getFilesToProcess(sInputDir);
		if (!filesToBeProcessed.isEmpty()) {
			for (File file : filesToBeProcessed) {
				String sFilePath = file.getAbsolutePath();
				logger.debug("File Being Read: " + sFilePath);
				String sFileName = file.getName().substring(0,file.getName().lastIndexOf("."));
				YFCDocument doc = formXMLMsg(sFileName, file);
				if(!YFCObject.isNull(doc)) {
					doc.getDocumentElement().setAttribute("FileName", sFilePath);
					doc.getDocumentElement().setAttribute("InputDir", sInputDir);
					doc.getDocumentElement().setAttribute("SuccessDir",sCompDir);
					doc.getDocumentElement().setAttribute("ErrorDir", sErrorDir);
					
					fileList.add(doc);
				}
			}
		}
		
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "getJobs", msgXml);
		return fileList;
	}
	
	private YFCDocument formXMLMsg(String sFileName, File file){
		try{
			FileInputStream fileInputStream = new FileInputStream(file);
			byte[] byteArray = IOUtils.toByteArray(fileInputStream);
			byte[] encodeBase64 = Base64.encodeBase64(byteArray);
			String encodedString = new String(encodeBase64);
			
			String sItemKey = getItemKey(sFileName);
			if(YFCObject.isNull(sItemKey)){
				return null;
			}
			YFCDocument yfcManageItemDoc = YFCDocument.getDocumentFor("<ItemList><Item></ItemList>");
			YFCElement yfcItemlistEle = yfcManageItemDoc.getDocumentElement();
			YFCElement yfcItemEle = yfcItemlistEle.getChildElement("Item");
			yfcItemEle.setAttribute("ItemKey", sItemKey);
			yfcItemEle.setAttribute("ImageObject", encodedString);
			return yfcManageItemDoc;
		}
		catch(IOException ex) {
			throw new YFCException(ex);
		}
	}

	private String getItemKey(String sFileName){
		YFCDocument yfcGetItemListInDoc = YFCDocument.getDocumentFor("<Item ItemID=\""+sFileName+"\"/>");
		YFCDocument yfcGetItemListOutDoc = invokeYantraApi("getItemList", yfcGetItemListInDoc);
		YFCElement yfcItemListEle = yfcGetItemListOutDoc.getDocumentElement();
		YFCElement yfcItemEle = yfcItemListEle.getChildElement("Item");
		if(!YFCObject.isNull(yfcItemEle)){
			String sItemKey = yfcItemEle.getAttribute("ItemKey");
			return sItemKey;
		}
		return null;
		
	}

	protected List<File> getFilesToProcess(String directory) {
		File dir = new File(directory);
		File[] files = dir.listFiles();
		if (files == null) {
			return new ArrayList<File>();
		}
		return Arrays.asList(files);
	}
	
	public void executeJob(YFCDocument executeJobXml) {
		String sFileName = null;
		String sInputDir = null;
		String sSuccessDir = null;
		String sErrorDir = null;
		
		try {
			YFCElement eleRoot = executeJobXml.getDocumentElement();
			sFileName = eleRoot.getAttribute("FileName");
			eleRoot.removeAttribute("FileName");
			sInputDir = eleRoot.getAttribute("InputDir");
			eleRoot.removeAttribute("InputDir");
			sSuccessDir = eleRoot.getAttribute("SuccessDir");
			eleRoot.removeAttribute("SuccessDir");
			sErrorDir = eleRoot.getAttribute("ErrorDir");
			eleRoot.removeAttribute("ErrorDir");
			
			//invoke manageItem API
			invokeYantraApi("manageItem", eleRoot.getOwnerDocument());
			
			moveFile(sFileName, sInputDir , sSuccessDir);
		}
		catch (Exception exp) {
			logger.error(exp);
			YFCException yfcException = new YFCException(exp);
			if(null!= sFileName)
				moveFile(sFileName, sInputDir , sErrorDir);
			throw yfcException;
		}
		
	}
	
	protected void moveFile(String filesAbsPath, String srcDir, String destDir) {
		File fileSource = new File(filesAbsPath);
		File fileDestination = new File(filesAbsPath.replace(srcDir, destDir));
		if(fileDestination.exists()){
			String extn = fileDestination.getName().substring(fileDestination.getName().lastIndexOf("."));
			String filename = fileDestination.getName().substring(0,fileDestination.getName().lastIndexOf("."));
			SimpleDateFormat sdf =new SimpleDateFormat("yyyyMMddHHmmssSSS");
			String date = sdf.format(new Date());
			filename=filename+"_"+date+extn;
			fileDestination.renameTo(new File(destDir+File.separator+filename));
		}
		fileSource.renameTo(fileDestination);
	}
}

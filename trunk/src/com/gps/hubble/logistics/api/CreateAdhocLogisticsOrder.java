/***********************************************************************************************
 * File Name        : CreateAdhocLogisticsOrder.java
 *
 * Description      : Ad-hoc Logistics will be created in Sterling by IBM to move boxes from one address to another. 
 *                    The logistics request created by the vendor will be against an ASN. The request will be captured 
 *                    as sales order in Sterling. This order will be created for a dummy item. The system will also prepare the 
 *                    Adhoc logistics request STR_ODR_1 (3B12) message and drop it to an MQ.
 *
 * Modification Log :
 * ---------------------------------------------------------------------------------------------
 * Ver #  	Date              Author				Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0		10-01-2017      Keerthi Yadav           Initial Version 
 * 1.1		25-04-2017		Prateek Kumar			HUB-8803: Append sequence number in the outbound message
 * 1.2      30-05-2017		Keerthi Yadav			HUB-9188: The Order_header_extension table is not populated for the AdHoc Logistics order
 * 1.3		13-06-2017		Prateek Kumar			HUB-9303: Added extn customer key at line level
 * 1.4		21-06-2017      Keerthi Yadav        	HUB-9388: Changing Item ID for Integral Plus Adhoc Logictics Order
 * 1.5		27-09-2017		Prateek Kumar			Added instructions in the create shipment input
 * ---------------------------------------------------------------------------------------------
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
 **********************************************************************************************/
package com.gps.hubble.logistics.api;

import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Document;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XMLUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.order.api.GpsUpdateOrderExtnFields;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;

// Sample inXml to this class:-

/*<Order EnterpriseCode='TELSTRA_SCLSA' BuyerOrganizationCode='TELSTRA' SellerOrganizationCode='TELSTRA' OrderType='ADHOC_LOGISTICS' OrderNo=''
  EntryType='STERLING' DocumentType='0001' >
   <References>
     <Reference Name='TRACKING_NO' Value='446257'/>
   </References>
    <PersonInfoBillTo FirstName="" DayPhone="" EMailID="" AddressLine1="" AddressLine2="" AddressLine3="" AddressLine4="" AddressLine5="" City="" Country="" State="" ZipCode=""/>
   <AdditionalAddresses>
   <AdditionalAddress AddressType='SHIP_FROM'>
     <PersonInfo FirstName='Yadav' DayPhone='2786' EMailID='' AddressLine1='2' AddressLine2='2' AddressLine3='2' AddressLine4='2' AddressLine5='' City='SYdney' 
      Country='AU' State='' ZipCode=''/>
    </AdditionalAddress>
   </AdditionalAddresses>
   <OrderLines>
    <OrderLine  ShipNode='TestOrg9' >
       <Extn PriorityCode='STANDARD' />
       <PersonInfoShipTo FirstName='Keerthi' DayPhone='32321' EMailID='k@123' AddressLine1='1' AddressLine2='1' AddressLine3='1' AddressLine4='1' AddressLine5='1' City='Sydney' 
         Country='AU' State='' ZipCode=''/>
   </OrderLine>
  </OrderLines>
  <Containers>
   <Container ContainerType='TYPE1' ContainerNo='2' ContainerHeightUOM='EACH' ContainerHeight='2' ContainerLengthUOM='EACH' ContainerLength='2' ContainerWidthUOM='EACH' 
  ContainerWidth='2' ContainerVolumeUOM='EACH' ContainerVolume='2' ContainerGrossWeightUOM='' ContainerGrossWeight='2'/>
  </Containers>
  <Instructions>
	<Instruction InstructionText="" InstructionType="" />
  </Instructions>
 </Order>	
 */

public class CreateAdhocLogisticsOrder extends AbstractCustomApi {
	private static YFCLogCategory logger = YFCLogCategory.instance(CreateAdhocLogisticsOrder.class);
	private YFCDocument outconfirmShipmentxml=null;

	@Override
	public YFCDocument invoke(YFCDocument inXml) {
		//HUB 7273- [START]

		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke",inXml);

		outconfirmShipmentxml = inXml;

		/*HUB 8118- [START]
		 Removing the shipment check and delete/update container logic as BookFreight class has been implemented to perform this function */

		/*Renaming Node to 'Order' as JSON from the UI makes the root element as the service name*/

		Document doc = inXml.getDocument();
		doc.renameNode(doc.getDocumentElement(), doc.getNamespaceURI(), TelstraConstants.ORDER);
		YFCDocument inputDoc = YFCDocument.getDocumentFor(doc);
		
		outconfirmShipmentxml=createOrder(inputDoc);
		/*HUB 8118- [END]*/

		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke",inXml);
		return outconfirmShipmentxml;

		/*HUB 7273- [END]*/
	}

	// Creating a dummy sales Order with a dummy ShipNode and Item.
	private YFCDocument createOrder(YFCDocument inXml) {
		if(YFCObject.equals(inXml.getDocumentElement().getAttribute(TelstraConstants.ORDER_TYPE),"ADHOC_LOGISTICS")){
			YFCElement eleinXmlContainers=inXml.getDocumentElement().getElementsByTagName(TelstraConstants.CONTAINERS).item(0);
			YFCElement eleCreateOrder=inXml.getDocumentElement();
			eleCreateOrder.removeChild(eleinXmlContainers);
			/**/
			YFCElement yfcEleInstructions=eleCreateOrder.getElementsByTagName(TelstraConstants.INSTRUCTIONS).item(0);
			eleCreateOrder.removeChild(yfcEleInstructions);
			String sScac = eleCreateOrder.getAttribute(TelstraConstants.SCAC);
			eleCreateOrder.removeAttribute(TelstraConstants.SCAC);
			String sCarrierServiceCode = eleCreateOrder.getAttribute(TelstraConstants.CARRIER_SERVICE_CODE);
			eleCreateOrder.removeAttribute(TelstraConstants.CARRIER_SERVICE_CODE);
			
			YFCElement eleOrderLine=inXml.getDocumentElement().getElementsByTagName(TelstraConstants.ORDER_LINE).item(0);
			if(!YFCObject.isNull(eleOrderLine)){
				String sShipNode=eleOrderLine.getAttribute(TelstraConstants.SHIP_NODE,"");
				if(YFCObject.isNull(sShipNode)){
					sShipNode = getProperty("ADHOC_LOGISTICS_NODE", true);
					eleOrderLine.setAttribute(TelstraConstants.SHIP_NODE,sShipNode);
				}
				eleOrderLine.setAttribute(TelstraConstants.ORDERED_QTY, "1");
				eleOrderLine.setAttribute(TelstraConstants.PRIME_LINE_NO, "1");
				YFCElement eleOrderItemLine= eleOrderLine.createChild(TelstraConstants.ITEM);
				
				//HUB-9388[START]
				/*Changing the item ID from Adhoc_logictics to CONSIGNMENT*/
				eleOrderItemLine.setAttribute(TelstraConstants.ITEM_ID, "CONSIGNMENT");
				eleOrderItemLine.setAttribute("ItemDesc", "USED TO MANAGE ALL CONSIGNMENT BASED ORDERS WHERE THE ACTUAL CONTENT OF EACH CONTAINER IS UNKNOWN OR NOT APPLICABLE");
				//HUB-9388[END]

				eleOrderItemLine.setAttribute(TelstraConstants.UNIT_OF_MEASURE, "EA");
				
				//HUB-9188 [START]
				
				/* The Below changes are done to move the Person Info Ship To at the Line level from the Person Info 
				 * Bill to which was header along with Priority Code from header level to the Line level
				 */
				
				YFCElement elePersonInfoShipTo= eleOrderLine.createChild(TelstraConstants.PERSON_INFO_SHIP_TO);
				YFCElement eleExtn= eleOrderLine.createChild(TelstraConstants.EXTN);

				YFCElement elePersonInfoBillTo = inXml.getDocumentElement().getElementsByTagName(TelstraConstants.PERSON_INFO_BILL_TO).item(0);
				elePersonInfoShipTo.setAttributes(elePersonInfoBillTo.getAttributes());
				inXml.getDocumentElement().removeChild(elePersonInfoBillTo);
				
				String sPriorityCode = inXml.getDocumentElement().getAttribute(TelstraConstants.PRIORITY_CODE);
				eleExtn.setAttribute(TelstraConstants.PRIORITY_CODE,sPriorityCode);
				inXml.getDocumentElement().removeAttribute(TelstraConstants.PRIORITY_CODE);
				
				YFCDocument doctempOrderListinXml = YFCDocument.getDocumentFor("<Order EnterpriseCode='' OrderNo='' DocumentType='' OrderHeaderKey='' ><OrderLines><OrderLine Status='' OrderLineKey='' PrimeLineNo='' /></OrderLines></Order>");
				//HUB-9303 - Begin
				updateExtnCustomerKey(inXml);
				//HUB-9303 - End				
				YFCDocument outxml = invokeYantraApi("createOrder", inXml,doctempOrderListinXml);

				
				/* The changes below invoke a class to update the Order header extension table with the boolean values only 
				 * for Adhoc Order Creation*/
				
				GpsUpdateOrderExtnFields obj = new GpsUpdateOrderExtnFields();
				String sOrderNo = outxml.getDocumentElement().getAttribute(TelstraConstants.ORDER_NO);
				obj.updateOrderExtnFields(YFCDocument.getDocumentFor("<Order OrderNo='" + sOrderNo + "' />"),getServiceInvoker());
				
				//HUB-9188 [END]

				String sOrderHeaderKey=outxml.getDocumentElement().getAttribute(TelstraConstants.ORDER_HEADER_KEY,"");

				//Schedule and Release the Order
				String sOrderReleaseKey =scheduleAndReleaseOrder(sOrderHeaderKey);

				// Method to Create and Confirm Shipment with the container details and drop to Queue
				//HUB-9288 - Fixed to pass the priority code while creating shipment
				outconfirmShipmentxml= createAndConfirmShipment(inXml,eleinXmlContainers,sOrderNo,sOrderHeaderKey,sOrderReleaseKey,sPriorityCode,yfcEleInstructions,sScac,sCarrierServiceCode);

			}

		}
		return outconfirmShipmentxml;
	}

	/**
	 * Stamping customer key to order line extn
	 * @param inXml
	 */
	private void updateExtnCustomerKey(YFCDocument inXml) {

		Map<String, String> mapShipNodeCustKey = new HashMap<>();
		for(YFCElement yfcEleOrderLine : inXml.getElementsByTagName(TelstraConstants.ORDER_LINE)){
			String sCustKey = "";
			String sShipToId = yfcEleOrderLine.getAttribute(TelstraConstants.SHIP_TO_ID);
			if(!YFCCommon.isStringVoid(sShipToId)){
				if(mapShipNodeCustKey.containsKey(sShipToId)){
					sCustKey = mapShipNodeCustKey.get(sShipToId);				
				}			
				else{
					YFCDocument yfcDocGetCustListOp = getCustomer(sShipToId);
					if(yfcDocGetCustListOp.hasChildNodes()){
						sCustKey = XPathUtil.getXpathAttribute(yfcDocGetCustListOp, "//Customer/@CustomerKey");
						mapShipNodeCustKey.put(sShipToId, sCustKey);
					}
				}
				YFCElement yfcEleOlExtn = yfcEleOrderLine.getChildElement(TelstraConstants.EXTN);
				if(YFCCommon.isVoid(yfcEleOlExtn)){
					yfcEleOlExtn = yfcEleOrderLine.createChild(TelstraConstants.EXTN);					
				}
				yfcEleOlExtn.setAttribute(TelstraConstants.CUSTOMER_KEY, sCustKey);			
			}		
		}
	}

	/**
	 * Scheduling and releasing the created Order
	 * @param sOrderHeaderKey
	 * @return
	 */
	private String scheduleAndReleaseOrder(String sOrderHeaderKey) {
		String sOrderReleaseKey=null;
		YFCDocument docInputscheduleXml=YFCDocument.getDocumentFor("<ScheduleOrder ScheduleAndRelease='Y' CheckInventory='N' IgnoreReleaseDate='Y' OrderHeaderKey='" + sOrderHeaderKey + "' />");
		invokeYantraApi("scheduleOrder", docInputscheduleXml);
		YFCDocument docOrderReleaseListoutXml = getOrderReleaseList(sOrderHeaderKey);
		YFCElement eleReleaseorderlist=docOrderReleaseListoutXml.getDocumentElement();
		if(!YFCObject.isNull(eleReleaseorderlist.getElementsByTagName(TelstraConstants.ORDER_RELEASE).item(0).getAttribute(TelstraConstants.ORDER_HEADER_KEY))){
			sOrderReleaseKey=eleReleaseorderlist.getAttribute("LastOrderReleaseKey");
		}
		return sOrderReleaseKey;
	}

	/**
	 * Retrieving the releaseKey for the Order
	 * @param sOrderHeaderKey
	 * @return
	 */
	private YFCDocument getOrderReleaseList(String sOrderHeaderKey) {
		YFCDocument docOrderListinXml = YFCDocument.getDocumentFor("<OrderRelease OrderHeaderKey='"+ sOrderHeaderKey+ "' />");
		YFCDocument doctempgetOrderListinXml = YFCDocument.getDocumentFor("<OrderReleaseList LastOrderReleaseKey='' TotalNumberOfRecords=''><OrderRelease  "
				+ "EnterpriseCode='' DocumentType='' OrderHeaderKey='' OrderNo='' /></OrderReleaseList>");
		return invokeYantraApi("getOrderReleaseList", docOrderListinXml,doctempgetOrderListinXml);
	}

	

	/**
	 *Confirming the Shipment with the Container Details(Dimensions) of the box to track it at the shipment level with the total No of containers in the extended column
		and then a message is formed for STR_ODR_1 (3B12) for the logistics request  
	 * @param inXml
	 * @param eleinXmlContainers
	 * @param sOrderNo
	 * @param sOrderHeaderKey
	 * @param sOrderReleaseKey
	 * @param sPriorityCode
	 * @param yfcEleInstructions
	 * @param sCarrierServiceCode 
	 * @param sScac 
	 * @return
	 */
	private YFCDocument createAndConfirmShipment(YFCDocument inXml, YFCElement eleinXmlContainers, String sOrderNo,
			String sOrderHeaderKey, String sOrderReleaseKey, String sPriorityCode, YFCElement yfcEleInstructions,
			String sScac, String sCarrierServiceCode) {
		
		/*HUB-8104 [START]
		Reading the Tracking No from the Refernce level and stamping on the Shipment*/

		String sTrackingNo="";
		YFCElement eleReferences = inXml.getElementsByTagName("References").item(0);
		if(!YFCObject.isVoid(eleReferences)){
			for(YFCElement eleReference : eleReferences.getElementsByTagName(TelstraConstants.REFERENCE)){
				if(YFCObject.equals("TRACKING_NO", eleReference.getAttribute(TelstraConstants.NAME,""))){
					sTrackingNo= eleReference.getAttribute(TelstraConstants.VALUE,"");
					break;
				}
			}
		}
		
		if(YFCObject.isVoid(sTrackingNo)) {
			long lNextOrderNo = getServiceInvoker().getNextSequenceNumber("GPS_TRACKING_NO_SEQ");
			sTrackingNo = "IBMS"+lNextOrderNo;
		}

		//HUB-8104 [END]
		String sRequestedShipmentDate=inXml.getDocumentElement().getAttribute(TelstraConstants.REQUESTED_SHIPMENT_DATE,"");
		/*String sCarrier="";
		String sService="";

		//Priority based  SCAC and service is read from the common Code List
		YFCDocument docGetCommonCodeListOutput = invokeYantraApi(TelstraConstants.API_GET_COMMON_CODE_LIST,YFCDocument.getDocumentFor("<CommonCode CodeType=\"LOGISTICS_PRIORITY\" CodeValue='" + sPriorityCode + "'/>"));
		if(!YFCObject.isNull(docGetCommonCodeListOutput.getElementsByTagName(TelstraConstants.COMMON_CODE).item(0).getAttribute(TelstraConstants.CODE_VALUE))){
			sCarrier=docGetCommonCodeListOutput.getDocumentElement().getElementsByTagName(TelstraConstants.COMMON_CODE).item(0).getAttribute("CodeShortDescription","");
			sService=docGetCommonCodeListOutput.getDocumentElement().getElementsByTagName(TelstraConstants.COMMON_CODE).item(0).getAttribute("CodeLongDescription","");
		}	   */ 

		YFCDocument docCreateShipment = YFCDocument.getDocumentFor("<Shipment />");
		YFCElement eleShipment=docCreateShipment.getDocumentElement();
		eleShipment.setAttribute(TelstraConstants.ORDER_NO, sOrderNo);
		eleShipment.setAttribute(TelstraConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
		eleShipment.setAttribute(TelstraConstants.TRACKING_NO, sTrackingNo);
		eleShipment.setAttribute(TelstraConstants.REQUESTED_SHIPMENT_DATE, sRequestedShipmentDate);
		eleShipment.setAttribute("SCAC", sScac);
		eleShipment.setAttribute("CarrierServiceCode", sCarrierServiceCode);

		eleShipment.createChild(TelstraConstants.ORDER_RELEASES).createChild(TelstraConstants.ORDER_RELEASE).setAttribute(TelstraConstants.ORDER_RELEASE_KEY, sOrderReleaseKey);
		eleShipment.createChild(TelstraConstants.EXTN).setAttribute("LogisticsStatus", "PENDING");

		//Stamping the SHIP_TO Address
		
		//HUB-9188 - [START]
		
		/*Changed PersonInfoBillTo to PersonInfoShipTo*/
		YFCElement elePersonInfoShipTo = inXml.getDocumentElement().getElementsByTagName(TelstraConstants.PERSON_INFO_SHIP_TO).item(0);
		eleShipment.appendChild(docCreateShipment.importNode(elePersonInfoShipTo,true));
		docCreateShipment.getDocument().renameNode(eleShipment.getChildElement(TelstraConstants.PERSON_INFO_SHIP_TO).getDOMNode(), null, "ToAddress");
		
		//HUB-9188 - [END]

		//Stamping the SHIP_FROM Address
		YFCElement elePersonInfo = inXml.getElementsByTagName(TelstraConstants.PERSON_INFO).item(0);
		eleShipment.appendChild(docCreateShipment.importNode(elePersonInfo,true));
		docCreateShipment.getDocument().renameNode(eleShipment.getChildElement(TelstraConstants.PERSON_INFO).getDOMNode(), null, "FromAddress");

		YFCDocument docDroptoLogisticsQueue= XMLUtil.getDocumentFor(docCreateShipment
				.getDocumentElement().cloneNode(true));
		YFCElement eleShipmentContainers =eleShipment.createChild(TelstraConstants.CONTAINERS);
		YFCElement eleLogisticsContainers =docDroptoLogisticsQueue.getDocumentElement().createChild(TelstraConstants.CONTAINERS);

		//Adding the Containers(Boxes) to the Shipment
		for (YFCElement eleinXmlContainer : eleinXmlContainers.getElementsByTagName(TelstraConstants.CONTAINER)){
			int sContainerNo=eleinXmlContainer.getIntAttribute(TelstraConstants.CONTAINER_NO);
			if(sContainerNo > 0){
				eleinXmlContainer.removeAttribute(TelstraConstants.CONTAINER_NO);
				eleinXmlContainer.createChild(TelstraConstants.EXTN).setAttribute("NoOfContainer", sContainerNo);
				eleShipmentContainers.appendChild(docCreateShipment.importNode(eleinXmlContainer,true));
				eleinXmlContainer.removeChild(eleinXmlContainer.getChildElement(TelstraConstants.EXTN));
				for(int iterator=1; iterator<=sContainerNo; iterator++) {	
					eleLogisticsContainers.appendChild(docDroptoLogisticsQueue.importNode(eleinXmlContainer, true));
				}
			}
		}

		/*append instructions*/
		eleShipment.appendChild(docCreateShipment.importNode(yfcEleInstructions, true));
		YFCDocument outcreateShipmentxml = invokeYantraApi("createShipment", docCreateShipment, getCreateShipmentTemplate());
		outconfirmShipmentxml = invokeYantraApi("confirmShipment", outcreateShipmentxml);
		String sShipmentNo=outconfirmShipmentxml.getDocumentElement().getAttribute(TelstraConstants.SHIPMENT_NO,"");
		if(!YFCObject.isNull(sShipmentNo)){			
			docDroptoLogisticsQueue.getDocumentElement().setAttribute(TelstraConstants.SHIPMENT_NO, sShipmentNo);
			YFCElement eleinXmlReleases=docDroptoLogisticsQueue.getDocumentElement().getElementsByTagName(TelstraConstants.ORDER_RELEASES).item(0);
			docDroptoLogisticsQueue.getDocumentElement().removeChild(eleinXmlReleases);
			//HUB-8803 - Begin
			long lNextSequenceNo = getServiceInvoker().getNextSequenceNumber(TelstraConstants.GPS_SEQUENCE_ADHOC);
			String sSequenceNo = String.valueOf(lNextSequenceNo);
			docDroptoLogisticsQueue.getDocumentElement().setAttribute(TelstraConstants.SEQUENCE_NO, sSequenceNo);
			
			/* if is referred is passed move the shipment to Logistics Referred status */
			YFCDocument yfcDocCancelShipmentIp = null;
			String sIsReferred = inXml.getDocumentElement().getAttribute("IsReferred");
			String sShipmentKey = outconfirmShipmentxml.getDocumentElement().getAttribute(TelstraConstants.SHIPMENT_KEY);
			if (TelstraConstants.YES.equalsIgnoreCase(sIsReferred)) {
				yfcDocCancelShipmentIp = YFCDocument.getDocumentFor("<Shipment BaseDropStatus='1400.00500' ShipmentKey='"
						+ sShipmentKey + "' TransactionId='GPS_LOGISTICS_REFERRED.0001.ex'/>");
			} else {
				String sDroptoLogisticsQueue = getProperty("SERVICE_DropToLogisticsRequestQ", true);
				invokeYantraService(sDroptoLogisticsQueue, docDroptoLogisticsQueue);
				yfcDocCancelShipmentIp = YFCDocument.getDocumentFor("<Shipment BaseDropStatus='1400.01000' ShipmentKey='"
						+ sShipmentKey + "' TransactionId='GPS_LOGIS_REQ.0001.ex'/>");
			}
			invokeYantraApi(TelstraConstants.API_CHANGE_SHIPMENT_STATUS, yfcDocCancelShipmentIp);
			
			//HUB-8803 - End
			//Dropping the confirm shipment message to DropLogisticQueue
			
		}
		return outcreateShipmentxml;
	}

	/**
	 * 
	 * @return
	 */
	private YFCDocument getCreateShipmentTemplate() {
		YFCDocument createShipmentTemplate = YFCDocument.getDocumentFor("<Shipment SellerOrganizationCode='' ShipNode='' ShipmentKey='' ShipmentNo='' TrackingNo='' />");
		LoggerUtil.verboseLog("CreateAdhocLogisticsOrder :: getCreateShipmentTemplate :: createShipmentTemplate", logger, createShipmentTemplate);
		return createShipmentTemplate;
	}

	/**
	 * 
	 * @param sShipToId
	 * @return
	 */
	private YFCDocument getCustomer(String sShipToId) {

		YFCDocument docCustomerListinXml = YFCDocument.getDocumentFor(
				"<Customer>" + "<ComplexQuery Operator='OR'>" + "<Or>" + "<Exp Name='CustomerID' Value='" + sShipToId
						+ "' QryType='EQ'/>" + "<Exp Name='CustomerKey' Value='" + sShipToId
						+ "' QryType='EQ'/> </Or></ComplexQuery>" + "</Customer>");

		YFCDocument docCustomerListtempXml = YFCDocument.getDocumentFor(
				"<CustomerList><Customer ExternalCustomerID='' CustomerKey='' CustomerID=''/></CustomerList>");

		YFCDocument docCustomerListoutXml = invokeYantraApi("getCustomerList", docCustomerListinXml,
				docCustomerListtempXml);
		LoggerUtil.verboseLog("CreateAdhocLogisticsOrder :: getCustomer :: docCustomerListoutXml", logger, docCustomerListoutXml);
		return docCustomerListoutXml;
	}	
}	
/***********************************************************************************************
 * File Name        : ManageLogisticsResponse.java
 *
 * Description      : Custom API to book freight for Vendors. This api calls changeShipment to add containers to the Shipment and invokes a
 *                    service to put a outbound Logistics Request message into a queue
 *                                       
 * Modification Log :
 * ---------------------------------------------------------------------------------------------
 * Ver #    Date              Author                              Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0      01-12-2016      Vinay Bhanu                        Initial Version 
 * 1.1      12-01-2017      Keerthi Yadav                      Returning Order Number in request message TRA_ODR_1 (3B12c) and implemented delete/add containers
 *                                                             for the shipment for re-booking.
 * 1.2		03-10-2017		Prateek Kumar						Adhoc CR - 
 * ---------------------------------------------------------------------------------------------
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
 **********************************************************************************************/

package com.gps.hubble.logistics.api;

import java.util.Map;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XMLUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.constants.TelstraErrorCodeConstants;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSException;

public class BookFreight extends AbstractCustomApi {

	private static YFCLogCategory logger = YFCLogCategory.instance(BookFreight.class);

	@Override
	public YFCDocument invoke(YFCDocument inDoc)  {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", inDoc);
		YFCDocument outchangeShipmentxml=changeShipment(inDoc);
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke",inDoc);
		return outchangeShipmentxml;

	}
	
	private YFCDocument changeShipment(YFCDocument inXml) {
		String sPriorityCode=inXml.getDocumentElement().getAttribute("PriorityCode");
		String sTrackingNo=inXml.getDocumentElement().getAttribute("TrackingNo");
		if(YFCObject.isVoid(sTrackingNo)) {
          long lNextOrderNo = getServiceInvoker().getNextSequenceNumber("GPS_TRACKING_NO_SEQ");
          sTrackingNo = "IBMS"+lNextOrderNo;
        }
		String sShipmentNo= inXml.getDocumentElement().getAttribute(TelstraConstants.SHIPMENT_NO);
		String sShipmentKey= inXml.getDocumentElement().getAttribute("ShipmentKey");
		String sEnterpriseCode= inXml.getDocumentElement().getAttribute(TelstraConstants.ENTERPRISE_CODE);
		String sSellerOrgCode= inXml.getDocumentElement().getAttribute(TelstraConstants.SELLER_ORGANIZATION_CODE);		
		
		YFCElement eleinXmlContainers=inXml.getDocumentElement().getElementsByTagName(TelstraConstants.CONTAINERS).item(0);
//		YFCDocument erroDoc;
		YFCElement elemInFromAddress = inXml.getDocumentElement().getChildElement(TelstraConstants.FROM_ADDRESS,true);
		YFCElement elemInToAddress = inXml.getDocumentElement().getChildElement(TelstraConstants.TO_ADDRESS,true);
		if(YFCObject.isVoid(sShipmentKey) && (YFCObject.isVoid(sShipmentNo) || YFCObject.isVoid(sEnterpriseCode) || YFCObject.isVoid(sSellerOrgCode)  ) ) {
			LoggerUtil.verboseLog("Mandatory Attributes Not Passed ", logger, " throwing exception");
/*			erroDoc = ExceptionUtil.getYFSExceptionDocument("TEL_ERR_0931_002","No Shipment Identifier Provided.");
			throw new YFSException(erroDoc.toString());	*/
			throw ExceptionUtil.getYFSException("TEL_ERR_0931_002",	new YFSException());

		}
		/*if(YFCObject.isVoid(sPriorityCode)) {
			LoggerUtil.verboseLog("Mandatory Attributes Not Passed ", logger, " throwing exception");
			erroDoc = ExceptionUtil.getYFSExceptionDocument("TEL_ERR_0931_003","No Priority Provided.");
			throw new YFSException(erroDoc.toString());	
			throw ExceptionUtil.getYFSException("TEL_ERR_0931_003",	new YFSException());
		}*/		
		//Priority based  SCAC and service
		/*YFCDocument docGetCommonCodeListOutput = invokeYantraApi(TelstraConstants.API_GET_COMMON_CODE_LIST,YFCDocument.getDocumentFor("<CommonCode CodeType=\"LOGISTICS_PRIORITY\" CodeValue='" + sPriorityCode + "'/>"));
		if(!YFCObject.isNull(docGetCommonCodeListOutput.getElementsByTagName(TelstraConstants.COMMON_CODE).item(0).getAttribute(TelstraConstants.CODE_VALUE))){
			sCarrier=docGetCommonCodeListOutput.getDocumentElement().getElementsByTagName(TelstraConstants.COMMON_CODE).item(0).getAttribute("CodeShortDescription","");
			sService=docGetCommonCodeListOutput.getDocumentElement().getElementsByTagName(TelstraConstants.COMMON_CODE).item(0).getAttribute("CodeLongDescription","");
		}*/	    

		//HUB 7273- [START]
		//Delete Existing Containers for the Shipment if present

		YFCDocument docGetShipmentListOutput = getShipmentList(sShipmentKey);
		String sOrderNo=docGetShipmentListOutput.getDocumentElement().getElementsByTagName(TelstraConstants.SHIPMENT).item(0).getAttribute(TelstraConstants.ORDER_NO,"");
		if(!YFCObject.isNull(docGetShipmentListOutput.getDocumentElement().getElementsByTagName(TelstraConstants.SHIPMENT).item(0).getAttribute(TelstraConstants.SHIPMENT_NO))){
			deleteShipmentContainers(docGetShipmentListOutput);
		}		

		//HUB 7273- [END]

		YFCDocument docChangeShipment = YFCDocument.getDocumentFor("<Shipment />");
		YFCDocument docDroptoLogisticsQueue =YFCDocument.getDocumentFor("<Shipment/>");

		YFCElement logisticsElem = docDroptoLogisticsQueue.getDocumentElement();
		//Sending OrderNo in the Request Message to Logistics
		logisticsElem.setAttribute(TelstraConstants.ORDER_NO,sOrderNo);
		
		YFCElement eleShipment=docChangeShipment.getDocumentElement();

		eleShipment.setAttribute(TelstraConstants.SHIPMENT_KEY, sShipmentKey);
		eleShipment.setAttribute(TelstraConstants.SHIPMENT_NO, sShipmentNo);
		eleShipment.setAttribute(TelstraConstants.ENTERPRISE_CODE, sEnterpriseCode);
		eleShipment.setAttribute(TelstraConstants.SELLER_ORGANIZATION_CODE, sSellerOrgCode);
		eleShipment.setAttribute(TelstraConstants.TRACKING_NO, sTrackingNo);
		eleShipment.setAttribute(TelstraConstants.SCAC, inXml.getDocumentElement().getAttribute(TelstraConstants.SCAC));
		eleShipment.setAttribute(TelstraConstants.CARRIER_SERVICE_CODE, inXml.getDocumentElement().getAttribute(TelstraConstants.CARRIER_SERVICE_CODE));
		eleShipment.createChild(TelstraConstants.EXTN).setAttribute("LogisticsStatus", "PENDING");
		eleShipment.appendChild(docChangeShipment.importNode(elemInFromAddress,true));
		eleShipment.appendChild(docChangeShipment.importNode(elemInToAddress,true));

		logisticsElem.setAttributes(eleShipment.getAttributes());
		logisticsElem.appendChild(docDroptoLogisticsQueue.importNode(elemInFromAddress,true));
		logisticsElem.appendChild(docDroptoLogisticsQueue.importNode(elemInToAddress,true));


		YFCElement eleShipmentContainers =eleShipment.createChild(TelstraConstants.CONTAINERS);

		YFCElement eleLogisticsContainers =logisticsElem.createChild(TelstraConstants.CONTAINERS);

		//HUB 7273- [START]
		
		//Adding the Containers(Boxes) to the Shipment
		
		/* The new changes are to insert the total No. of containers in the Shipment container level and update in the extended column in shipment container 
		 * and remove this extn element while sending out the logistic request message, The No. of containers would be split in the response message. 
		 * Shipment container will be created for every container element coming in the inXml unless the ContainerNo is zero */
		
		for (YFCElement eleinXmlContainer : eleinXmlContainers.getElementsByTagName(TelstraConstants.CONTAINER)){
			int sContainerNo=eleinXmlContainer.getIntAttribute(TelstraConstants.CONTAINER_NO);
			if(sContainerNo > 0){
				eleinXmlContainer.removeAttribute(TelstraConstants.CONTAINER_NO);
				eleinXmlContainer.createChild(TelstraConstants.EXTN).setAttribute("NoOfContainer", sContainerNo);
				eleShipmentContainers.appendChild(docChangeShipment.importNode(eleinXmlContainer,true));
				eleinXmlContainer.removeChild(eleinXmlContainer.getChildElement(TelstraConstants.EXTN));
				for(int iterator=1; iterator<=sContainerNo; iterator++) {	
					eleLogisticsContainers.appendChild(docDroptoLogisticsQueue.importNode(eleinXmlContainer, true));
				}
			}
		}
		
		//HUB 7273- [END]


		YFCDocument outchangeShipmentxml = invokeYantraApi("changeShipment", docChangeShipment, getChangeShipmentTemplate());
		/*change shipment status to Logistics Booked*/
		
		
		/* if is referred is passed move the shipment to Logistics Referred status */
		YFCDocument yfcDocCancelShipmentIp = null;
		String sIsReferred = inXml.getDocumentElement().getAttribute("IsReferred");
		if (TelstraConstants.YES.equalsIgnoreCase(sIsReferred)) {
			yfcDocCancelShipmentIp = YFCDocument.getDocumentFor("<Shipment BaseDropStatus='1400.00500' ShipmentKey='"
					+ sShipmentKey + "' TransactionId='GPS_LOGISTICS_REFERRED.0005.ex'/>");
		} else {
			String sDroptoLogisticsQueue = getProperty("SERVICE_DropToLogisticsRequestQ", true);
			invokeYantraService(sDroptoLogisticsQueue, docDroptoLogisticsQueue);
			
			yfcDocCancelShipmentIp = YFCDocument.getDocumentFor("<Shipment BaseDropStatus='1400.01000' ShipmentKey='"
					+ sShipmentKey + "' TransactionId='GPS_PO_LOGIS_REQ.0005.ex'/>");
		}
		invokeYantraApi(TelstraConstants.API_CHANGE_SHIPMENT_STATUS, yfcDocCancelShipmentIp);
		
		
		

		return outchangeShipmentxml;
	}

	private YFCDocument getChangeShipmentTemplate() {
    return YFCDocument.getDocumentFor("<Shipment SellerOrganizationCode='' ShipNode='' ShipmentKey='' ShipmentNo='' TrackingNo='' />");
  }

  //HUB 7273- [START]
	/**
	 * This method returns getShipmentList document
	 * Method name: getShipmentList
	 * @param sShipmentKey
	 * @return docGetShipmentListOutput
	 */
	
	private YFCDocument getShipmentList(String sShipmentKey) {
		YFCDocument docGetShipmentListInput=YFCDocument.getDocumentFor("<Shipment ShipmentKey='"+ sShipmentKey+ "' Status='1400' />");
		YFCDocument docGetShipmentListTemplate = YFCDocument.getDocumentFor("<Shipments><Shipment OrderNo='' ShipmentNo='' ShipmentKey='' Status=''><Containers><Container ContainerNo='' /></Containers></Shipment></Shipments>");
		return invokeYantraApi("getShipmentList", docGetShipmentListInput,docGetShipmentListTemplate);
	}

	/**
	 * This method deletes all the shipment containers to insert the new one in the inXml
	 * Method name: deleteShipmentContainers
	 * @param docGetShipmentListOutput
	 */
	
	private void deleteShipmentContainers(YFCDocument docGetShipmentListOutput) {
		logger.verbose("getShipmentList Output : " + docGetShipmentListOutput.toString());
		YFCElement eleShipmentList=docGetShipmentListOutput.getDocumentElement().getElementsByTagName("Shipment").item(0);
		YFCDocument docchangeShipment= XMLUtil.getDocumentFor(eleShipmentList.cloneNode(true));
		YFCElement eleShipment=docchangeShipment.getDocumentElement();
		YFCElement elegetShipmentListContainers=eleShipment.getElementsByTagName(TelstraConstants.CONTAINERS).item(0);
		YFCElement elegetShipmentListContainer=elegetShipmentListContainers.getElementsByTagName(TelstraConstants.CONTAINER).item(0);
		if(!YFCObject.isNull(elegetShipmentListContainer)){
			Map<String, String> mapToattibutessize = elegetShipmentListContainer.getAttributes();
			if(mapToattibutessize.size()>0){
				for (YFCElement elegetShipmentContainer : elegetShipmentListContainers.getElementsByTagName(TelstraConstants.CONTAINER)){
					elegetShipmentContainer.setAttribute("Action", "Delete");
				}
				invokeYantraApi("changeShipment", docchangeShipment);
			}
		}
	}

	//HUB 7273- [END]
}

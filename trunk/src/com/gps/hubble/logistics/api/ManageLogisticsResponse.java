/***********************************************************************************************
 * File Name        : ManageLogisticsResponse.java
 *
 * Description      : A TRA_ODR_1 message is received in Sterling, an integration server will update the shipment for the order, 
 *                    showing the status of the logistics request. It can either by Accepted/Rejected. If multiple shipments or no 
 *                    Shipments are found passed with the input data Exception will be thrown.
 *                                       
 * Modification Log :
 * ---------------------------------------------------------------------------------------------
 * Ver #    Date              Author				Modification
 * --------------------------------------------------------------------------------------------
 * 1.0      12-01-2017      Keerthi Yadav			Initial Version 
 * 1.1		12-05-2017		Prateek Kumar			HUB-9036 - 3B12C message for vector order
 * 1.2		Jun 08,2017     Keerthi Yadav           HUB-9294: Neway delivery LRA
 * ---------------------------------------------------------------------------------------------
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
 **********************************************************************************************/

package com.gps.hubble.logistics.api;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.constants.TelstraErrorCodeConstants;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSException;

/**
 * 
 * 
 *
 */

public class ManageLogisticsResponse extends AbstractCustomApi {
	private static YFCLogCategory logger = YFCLogCategory.instance(ManageLogisticsResponse.class);
	private String sOrderType = "";
	private String sOrderHeaderKey = "";
	private String sDocumentType = "";
	/**
	 * This method will change the Shipment Status to Accept/Reject as responded by the logistics
	 * @param inXml
	 * @return outXml
	 */

	@Override
	public YFCDocument invoke(YFCDocument inXml) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", inXml);

		// Sample inXml to this class:-

		/*<Shipment LogisticsStatus="Accepted/Rejected" ShipmentNo="This value is vectorID in case of vector Adhoc" OrderNo="OrderNo3" TrackingNo="123" /> */

		/*
		 * HUB-9036 :Begin
		 */
		YFCElement yfcEleInRoot = inXml.getDocumentElement();
		String sLogisticsStatus = yfcEleInRoot.getAttribute(TelstraConstants.LOGISTICS_STATUS);

		//HUB-9294[START]

		String sShipmentNo = yfcEleInRoot.getAttribute(TelstraConstants.SHIPMENT_NO,"");

		/* For Vector Adhoc Logistics the Shipment No is the Vector ID hence calling getOrderLineList to validate */
		String sOrderTypeVectorAdhoc = "";

		if(!YFCObject.isVoid(sShipmentNo)){
			YFCDocument yfcDocGetOrderLineListOp = callGetOrderLineList(sShipmentNo);

			double dTotalLineList = yfcDocGetOrderLineListOp.getDocumentElement().getDoubleAttribute("TotalLineList");

			if(dTotalLineList > 1) {
				throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.SHIP_ORDER_MULTIPLE_ORDER_LINES_WITH_SAME_VECTOR_ID, new YFSException());
			}

			if(dTotalLineList == 1){
				YFCElement eleOrder = yfcDocGetOrderLineListOp.getDocumentElement().getElementsByTagName(TelstraConstants.ORDER).item(0);

				sOrderTypeVectorAdhoc = eleOrder.getAttribute(TelstraConstants.ORDER_TYPE,"");

				if(YFCObject.equals(TelstraConstants.ADHOC_LOGISTICS_UPPER_CASE, sOrderTypeVectorAdhoc)){
					/* Will not be determining the shipment No for Order since the assumption is one Vector LRA
					 * Order would only have one Shipment. Removing the attribute as this shipment No 
					 * was the Vector ID in the interface and will result in incorrect result when calling getShipmentList. 
					 */
					String sOrderNo = eleOrder.getAttribute(TelstraConstants.ORDER_NO,"");
					yfcEleInRoot.setAttribute(TelstraConstants.ORDER_NO,sOrderNo);
					yfcEleInRoot.removeAttribute(TelstraConstants.SHIPMENT_NO);
				}
				else if(TelstraConstants.REJECTED.equalsIgnoreCase(sLogisticsStatus) || TelstraConstants.ACCEPTED.equalsIgnoreCase(sLogisticsStatus)){	
					String sRejectionReason = yfcEleInRoot.getAttribute(TelstraConstants.REJECTION_REASON,"");
					YFCDocument yfcOpDoc = callShipOrRejectOrderService(sLogisticsStatus, sShipmentNo,sRejectionReason);
					LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke",inXml);
					return yfcOpDoc;						
				}					
			}
		}

		/*
		 * HUB-9036 :End
		 */
		//Change Shipment Status to Adhoc Accepted or Rejected
		//		YFCDocument outXml = changeShipmentStatus(inXml);

		processAdhocOrder(inXml,sLogisticsStatus);

		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke",inXml);
		return inXml;
	}

	/**
	 * 
	 * @param inXml
	 * @param sLogisticsStatus
	 */
	private void processAdhocOrder(YFCDocument inXml, String sLogisticsStatus) {

		String sShipmentKey = getShipmentKey(inXml);

		if(TelstraConstants.LOGISTICS_REJECTED.equalsIgnoreCase(sLogisticsStatus)){

			/*cancel Adhoc order */
			/*change shipment status to logistics rejected*/
			cancelShipment(sShipmentKey);
			if (TelstraConstants.ADHOC_LOGISTICS_UPPER_CASE.equalsIgnoreCase(this.sOrderType)
					&& TelstraConstants.DOCUMENT_TYPE_0001.equals(sDocumentType)) {
								
				changeShipmentStatus(sShipmentKey, "GPS_RES_LOGIS.0001.ex", "9000.01000");
				cancelOrder();
			}
			else if(TelstraConstants.DOCUMENT_TYPE_0005.equals(sDocumentType)){
				changeShipmentStatus(sShipmentKey, "GPS_PO_RES_LOGIS.0005.ex", "9000.01000");
			}
		}	

		else if(TelstraConstants.LOGISTICS_ACCEPTED.equalsIgnoreCase(sLogisticsStatus)){
			
			/*change shipment status to logistics accepted*/
			if(TelstraConstants.DOCUMENT_TYPE_0001.equals(sDocumentType)){
				changeShipmentStatus(sShipmentKey, "GPS_RES_LOGIS.0001.ex", "1400.02000");
			}
			else if(TelstraConstants.DOCUMENT_TYPE_0005.equals(sDocumentType)){
				changeShipmentStatus(sShipmentKey, "GPS_PO_RES_LOGIS.0005.ex", "1400.02000");
			}

		}
		else if(TelstraConstants.CANCELLATION_REQUESTED.equalsIgnoreCase(sLogisticsStatus)){

			/*change shipment status to cancellation rejected*/
			changeShipmentStatus(sShipmentKey, "GPS_LOGIS_CAN_REQUESTED."+sDocumentType+".ex", "1400.01500");
			/* publish to downstream system for cancellation request*/
			invokeYantraService("GpsDropToLogisticsRequestQ", getLogisticsRequestMessage(sShipmentKey,"CANCELLATION_REQUESTED"));

		}
		else if(TelstraConstants.CANCELLATION_ACCEPTED.equalsIgnoreCase(sLogisticsStatus)){

			/*cancel shipment*/
			cancelShipment(sShipmentKey);
			if(TelstraConstants.ADHOC_LOGISTICS_UPPER_CASE.equalsIgnoreCase(this.sOrderType)){		
				cancelOrder();
			}
		}
		else if(TelstraConstants.CANCELLATION_REJECTED.equalsIgnoreCase(sLogisticsStatus)){

			/*change shipment status to cancellation rejected*/
			changeShipmentStatus(sShipmentKey, "GPS_LOGISTIC_CAN_REJ."+sDocumentType+".ex", "1400.01600");
		}
		
		else if(TelstraConstants.REFER_ACCEPTED.equalsIgnoreCase(sLogisticsStatus)){
			/*publish message */
			invokeYantraService("GpsDropToLogisticsRequestQ", getLogisticsRequestMessage(sShipmentKey,"PENDING"));
			/* change status to Logistic Booked */
			
			
			if(TelstraConstants.DOCUMENT_TYPE_0001.equals(sDocumentType)){
				changeShipmentStatus(sShipmentKey, "GPS_LOGIS_REQ.0001.ex", "1400.01000");
			}
			else if(TelstraConstants.DOCUMENT_TYPE_0005.equals(sDocumentType)){
				changeShipmentStatus(sShipmentKey, "GPS_PO_LOGIS_REQ.0005.ex", "1400.01000");
			}			
		}
	}

	/**
	 * 
	 * @param sShipmentKey
	 * @param sLogisticStatus
	 * @return
	 */
	private YFCDocument getLogisticsRequestMessage(String sShipmentKey, String sLogisticStatus) {
		YFCDocument yfcDocGetShipmentDetIp = YFCDocument.getDocumentFor("<Shipment ShipmentKey='"+sShipmentKey+"'/>");
		YFCDocument yfcDocGetShipmentDetOp = invokeYantraApi("getShipmentDetails", yfcDocGetShipmentDetIp, getShipmentDetailsTemplate());
		LoggerUtil.verboseLog("ManageLogisticsResponse :: getLogisticsRequestMessage :: yfcDocGetShipmentDetOp", logger, yfcDocGetShipmentDetOp);
		YFCElement yfcEleExtn = XPathUtil.getXPathElement(yfcDocGetShipmentDetOp, "//Shipment/Extn");
		if(YFCCommon.isVoid(yfcEleExtn)){
			yfcDocGetShipmentDetOp.getDocumentElement().createChild(TelstraConstants.EXTN);
		}
		yfcEleExtn.setAttribute("LogisticsStatus", sLogisticStatus);
		return yfcDocGetShipmentDetOp;
	}

	private YFCDocument getShipmentDetailsTemplate() {

		return YFCDocument.getDocumentFor(
				"<Shipment CarrierServiceCode='' OrderHeaderKey='' OrderNo='' RequestedShipmentDate='' SCAC='' ShipmentNo='' TrackingNo='' EnterpriseCode='' SellerOrganizationCode=''><Extn LogisticsStatus=''/><ToAddress AddressLine1='' AddressLine2='' AddressLine3='' AddressLine4='' AddressLine5='' AddressLine6='' City='' Company='' Country='' Department='' EMailID='' FirstName='' LastName='' MobilePhone='' State='' ZipCode=''/><FromAddress AddressLine1='' AddressLine2='' AddressLine3='' AddressLine4='' AddressLine5='' AddressLine6='' City='' Company='' Country='' Department='' EMailID='' FirstName='' LastName='' MobilePhone='' State='' ZipCode=''/><Containers><Container ContainerGrossWeight='' ContainerGrossWeightUOM='' ContainerHeight='' ContainerHeightUOM='' ContainerLength='' ContainerLengthUOM='' ContainerType='' ContainerWidth='' ContainerWidthUOM=''/></Containers></Shipment>");
	}

	/**
	 * This method will cancel the Adhoc logistic order
	 */
	private void cancelOrder() {

		YFCDocument yfcDocCancelOrderIp = YFCDocument.getDocumentFor("<Order OrderHeaderKey='" + sOrderHeaderKey
				+ "' Action='CANCEL' Override='Y'/>");
		invokeYantraApi(TelstraConstants.API_CHANGE_ORDER, yfcDocCancelOrderIp);
	}

	/**
	 * 
	 * @param sShipmentKey
	 * @param sTransactionId
	 * @param sBaseDropStatus
	 */
	private void changeShipmentStatus(String sShipmentKey, String sTransactionId, String sBaseDropStatus) {

		YFCDocument yfcDocChangeShipmentStatusIp = YFCDocument.getDocumentFor("<Shipment BaseDropStatus='" + sBaseDropStatus
				+ "' ShipmentKey='" + sShipmentKey + "' TransactionId='" + sTransactionId + "'/>");
		invokeYantraApi(TelstraConstants.API_CHANGE_SHIPMENT_STATUS, yfcDocChangeShipmentStatusIp);

	}

	/**
	 * 
	 * @param sShipmentKey
	 */
	private void cancelShipment(String sShipmentKey) {


		invokeYantraApi(TelstraConstants.API_UNCONFIRM_SHIPMENT, YFCDocument.getDocumentFor("<Shipment ShipmentKey='"+ sShipmentKey + "'/>"));

		YFCDocument yfcDocCancelShipmentIp = YFCDocument.getDocumentFor("<Shipment Override='Y' Action='Cancel' ShipmentKey='"+ sShipmentKey + "'> <ShipmentStatusAudit ReasonText='Cancelled through ManageLogisticsResponse service' /> </Shipment>");
		invokeYantraApi(TelstraConstants.API_CHANGE_SHIPMENT, yfcDocCancelShipmentIp);
	}

	/**
	 * 
	 * @param inXml
	 * @return
	 */
	private String getShipmentKey(YFCDocument inXml) {

		YFCElement yfcEleRoot = inXml.getDocumentElement();
		String sShipmentNo = yfcEleRoot.getAttribute(TelstraConstants.SHIPMENT_NO,"");
		String sTrackingNo = yfcEleRoot.getAttribute(TelstraConstants.TRACKING_NO,"");
		String sOrderNo = yfcEleRoot.getAttribute(TelstraConstants.ORDER_NO,"");
		String sShipmentKey = "";

		if(!YFCObject.isNull(sOrderNo) || !YFCObject.isNull(sTrackingNo) || !YFCObject.isNull(sShipmentNo)){
			//Checking Shipment for Shipment Shipped Status
			/*commenting out as the shipment status can be other the Shipped*/
			//			YFCDocument docGetShipmentListInput=YFCDocument.getDocumentFor("<Shipment OrderNo='"+ sOrderNo+ "' ShipmentNo='"+ sShipmentNo + "' TrackingNo='"+ sTrackingNo +"' Status='1400'/>");
			YFCDocument docGetShipmentListInput=YFCDocument.getDocumentFor("<Shipment OrderNo='"+ sOrderNo+ "' ShipmentNo='"+ sShipmentNo + "' TrackingNo='"+ sTrackingNo +"'/>");
			YFCDocument docGetShipmentListTemplate = YFCDocument.getDocumentFor(
					"<Shipments><Shipment ShipmentNo='' ShipmentKey='' Status=''><ShipmentLines><ShipmentLine ShipmentLineNo=''><Order OrderType='' OrderHeaderKey='' DocumentType=''/></ShipmentLine></ShipmentLines></Shipment></Shipments>");
			YFCDocument docGetShipmentListOutput = invokeYantraApi("getShipmentList", docGetShipmentListInput,docGetShipmentListTemplate);	
			YFCNodeList<YFCElement> nlShipment = docGetShipmentListOutput.getElementsByTagName(TelstraConstants.SHIPMENT);

			if(nlShipment.getLength() ==  1 ) {
				sShipmentKey = docGetShipmentListOutput.getDocumentElement().getElementsByTagName(TelstraConstants.SHIPMENT).item(0).getAttribute(TelstraConstants.SHIPMENT_KEY,"");
				sOrderType = XPathUtil.getXpathAttribute(docGetShipmentListOutput, "//Order/@OrderType");
				sOrderHeaderKey = XPathUtil.getXpathAttribute(docGetShipmentListOutput, "//Order/@OrderHeaderKey");
				sDocumentType =  XPathUtil.getXpathAttribute(docGetShipmentListOutput, "//Order/@DocumentType");
			}else{
				LoggerUtil.verboseLog("Multiple Shipment found with the passed input", logger, " throwing exception");
				String strErrorCode = getProperty("ShipmentSearchFailureCode", true);
				throw  ExceptionUtil.getYFSException(strErrorCode,new YFSException());	
			}
		}else{
			LoggerUtil.verboseLog("Shipment was not Found", logger, " throwing exception");
			String strErrorCode = getProperty("ShipmentSearchFailureCode", true);
			throw  ExceptionUtil.getYFSException(strErrorCode,new YFSException());	
		}		


		return sShipmentKey;
	}

	/**
	 * This method will call gps ship or reject order
	 * @param sServiceName
	 * @param sShipmentNo
	 * @param sRejectionReason
	 * @return
	 */
	private YFCDocument callShipOrRejectOrderService(String sServiceName, String sShipmentNo, String sRejectionReason) {

		YFCDocument yfcDocRejectLineIp = YFCDocument.createDocument(TelstraConstants.SHIPMENT);
		YFCElement yfcEleRejectLineRoot = yfcDocRejectLineIp.getDocumentElement();
		yfcEleRejectLineRoot.setAttribute(TelstraConstants.ORDER_NAME, sShipmentNo);

		if(TelstraConstants.REJECTED.equalsIgnoreCase(sServiceName)){
			yfcEleRejectLineRoot.setAttribute(TelstraConstants.SERVICE_NAME, TelstraConstants.SERVICE_REJECTED);		
			yfcEleRejectLineRoot.setAttribute(TelstraConstants.REJECTION_REASON, sRejectionReason);
		}
		else if(TelstraConstants.ACCEPTED.equalsIgnoreCase(sServiceName)){
			yfcEleRejectLineRoot.setAttribute(TelstraConstants.SERVICE_NAME, TelstraConstants.SERVICE_ACCEPTED);	
		}

		YFCDocument yfcOutDOc = invokeYantraService(TelstraConstants.GPS_SHIP_OR_REJECT_ORDER, yfcDocRejectLineIp);

		LoggerUtil.verboseLog("ManageLogisticsResponse :: callShipOrRejectOrderService :: ShipOrRejectOrderOp ", logger, yfcOutDOc);
		return yfcOutDOc;
	}



	/*	
	 * This method will cancel the line having the incoming vector ID.

	private void rejectVectorOrderLine(YFCDocument yfcDocGetOrderLineListOp) {

		String sOrderLineKey = XPathUtil.getXpathAttribute(yfcDocGetOrderLineListOp, "//OrderLine/@OrderLineKey");
		String sOrderHeaderKey = XPathUtil.getXpathAttribute(yfcDocGetOrderLineListOp, "//OrderLine/Order/@OrderHeaderKey");

		YFCDocument yfcDocChangeOrderIp = YFCDocument.getDocumentFor(
				"<Order OrderHeaderKey='" + sOrderHeaderKey + "'><OrderLines><OrderLine Action='CANCEL' OrderLineKey='"
						+ sOrderLineKey + "'/> </OrderLines></Order>");
		LoggerUtil.verboseLog("ManageLogisticsResponse :: cancelRejectedVectorOrderLine :: yfcDocChangeOrderIp", logger, yfcDocChangeOrderIp);

		invokeYantraApi(TelstraConstants.API_CHANGE_ORDER, yfcDocChangeOrderIp);
	}
	 */
	/*
	 * call get order line list with incoming order no as vector id
	 */
	private YFCDocument callGetOrderLineList(String sOrderNo) {

		YFCDocument yfcDocGetOrderLineListIp = YFCDocument.getDocumentFor("<OrderLine><Order EntryType='VECTOR' /><Extn VectorID='"+sOrderNo+"' /></OrderLine>");
		LoggerUtil.verboseLog("ManageLogisticsResponse :: callGetOrderLineList :: yfcDocGetOrderLineListIp", logger, yfcDocGetOrderLineListIp);

		YFCDocument yfcDocGetOrderLineListTemp = YFCDocument.getDocumentFor("<OrderLineList TotalLineList=''>"
				+ "<OrderLine PrimeLineNo='' OrderLineKey=''><Order OrderHeaderKey='' OrderName='' OrderNo='' DocumentType='' OrderType='' /></OrderLine>"
				+ "</OrderLineList>");
		YFCDocument yfcDocGetOrderLineListOp = invokeYantraApi("getOrderLineList", yfcDocGetOrderLineListIp, yfcDocGetOrderLineListTemp);
		LoggerUtil.verboseLog("ManageLogisticsResponse :: callGetOrderLineList :: yfcDocGetOrderLineListOp", logger, yfcDocGetOrderLineListOp);
		return yfcDocGetOrderLineListOp;
	}

	/**
	 * 
	 * @param inXml
	 * @return outXml of changeShipment Api
	 *//*
	private YFCDocument changeShipmentStatus(YFCDocument inXml) {

		YFCDocument docChangeShipmentOutXml=inXml;

		String sShipmentNo=inXml.getDocumentElement().getAttribute(TelstraConstants.SHIPMENT_NO,"");
		String sTrackingNo=inXml.getDocumentElement().getAttribute(TelstraConstants.TRACKING_NO,"");
		String sOrderNo=inXml.getDocumentElement().getAttribute(TelstraConstants.ORDER_NO,"");

		Validate if Shipment is present with the passed input if multiple shipments or no shipments 
		are found passed with the input data Exception will be thrown. 

		if(!YFCObject.isNull(sOrderNo) || !YFCObject.isNull(sTrackingNo) || !YFCObject.isNull(sShipmentNo)){
			//Checking Shipment for Shipment Shipped Status
			YFCDocument docGetShipmentListInput=YFCDocument.getDocumentFor("<Shipment OrderNo='"+ sOrderNo+ "' ShipmentNo='"+ sShipmentNo + "' TrackingNo='"+ sTrackingNo +"' Status='1400'/>");
			YFCDocument docGetShipmentListTemplate = YFCDocument.getDocumentFor("<Shipments><Shipment ShipmentNo='' ShipmentKey='' Status=''/></Shipments>");
			YFCDocument docGetShipmentListOutput = invokeYantraApi("getShipmentList", docGetShipmentListInput,docGetShipmentListTemplate);	
			YFCNodeList<YFCElement> nlShipment = docGetShipmentListOutput.getElementsByTagName(TelstraConstants.SHIPMENT);

			if(nlShipment.getLength() ==  1 ) {
				String sShipmentKey = docGetShipmentListOutput.getDocumentElement().getElementsByTagName(TelstraConstants.SHIPMENT).item(0).getAttribute(TelstraConstants.SHIPMENT_KEY,"");
				if(!YFCObject.isNull(sShipmentKey)){
					//changing the Shipment Status as responded by logistics

					String sLogisticsStatus=inXml.getDocumentElement().getAttribute("LogisticsStatus","");
					String sRejectionReason=inXml.getDocumentElement().getAttribute("RejectionReason","");
					docChangeShipmentOutXml=invokeYantraApi("changeShipment",YFCDocument.getDocumentFor("<Shipment ShipmentKey='"+ sShipmentKey + "' "
							+ "Action='Modify'><Extn LogisticsStatus='" + sLogisticsStatus + "'  RejectionReason='" + sRejectionReason + "' /></Shipment>"));
				}
			}else{
				LoggerUtil.verboseLog("Shipment was not Found", logger, " throwing exception");
				String strErrorCode = getProperty("ShipmentSearchFailureCode", true);
				throw  ExceptionUtil.getYFSException(strErrorCode,new YFSException());	
			}
		}else{
			LoggerUtil.verboseLog("Shipment was not Found", logger, " throwing exception");
			String strErrorCode = getProperty("ShipmentSearchFailureCode", true);
			throw  ExceptionUtil.getYFSException(strErrorCode,new YFSException());	
		}
		return docChangeShipmentOutXml;
	}*/
}

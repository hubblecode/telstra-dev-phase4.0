package com.gps.test.api;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.gps.hubble.utils.ExcelUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.util.YFCException;

public class TestUploadFileFromLocal extends AbstractCustomApi {

	public YFCDocument invoke(YFCDocument indoc) {
		return uploadExcel(indoc);
	}

	private YFCDocument uploadExcel(YFCDocument indoc) {
		YFCDocument outDoc = YFCDocument.getDocumentFor("<FileUploadList />");
		YFCElement outEle = outDoc.getDocumentElement();
		YFCElement inEle = indoc.getDocumentElement();
		String excelFolder = inEle.getAttribute("ExcelFolder");
		outEle.setAttribute("UploadedExcelFolder", excelFolder);
		String aTransactionType = inEle.getAttribute("TransactionType", "ORG_LOAD");
		List<File> filesToProcess = ExcelUtil.getFilesToProcess(excelFolder);
		outEle.setAttribute("TotalExcelFiles", filesToProcess.size());
		for (File f : filesToProcess) {
			YFCDocument createDoc = YFCDocument.getDocumentFor("<UploadedFile/>");
			FileInputStream fileInputStream = null;
				try {
					fileInputStream = new FileInputStream(f);
					byte[] byteArray = IOUtils.toByteArray(fileInputStream);
					byte[] encodeBase64 = Base64.encodeBase64(byteArray);

					String encodedString = new String(encodeBase64);

					YFCElement yfcUploadFileEle = createDoc.getDocumentElement();
					yfcUploadFileEle.setAttribute("TransactionType", aTransactionType);
					yfcUploadFileEle.setAttribute("FileObject", encodedString);
					yfcUploadFileEle.setAttribute("ProcessedFlag", "N");
					yfcUploadFileEle.setAttribute("Status", "Pending Processing");
					yfcUploadFileEle.setAttribute("FileProgressed", "-1");
					yfcUploadFileEle.setAttribute("NotifyEmail", inEle.getAttribute("NotifyEmail", ""));
					yfcUploadFileEle.setAttribute("UserName", inEle.getAttribute("UserName", "admin"));
					YFCDocument createFileUploadOutput = invokeYantraService("GpsCreateFileUpload", createDoc);
					YFCElement createFileUploadOutputEle = createFileUploadOutput.getDocumentElement();
					createFileUploadOutputEle.setAttribute("FileName", f.getAbsolutePath());
					createFileUploadOutputEle.removeAttribute("FileObject");
					outEle.importNode(createFileUploadOutputEle.cloneNode(true));
				} catch (IOException e) {
					throw new YFCException(e);
				} 
		}
		return outDoc;
	}
}
package com.gps.test.api;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.mail.MessagingException;

import org.apache.commons.codec.binary.Base64;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.utils.ExcelUtil;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCConfigurator;
import com.yantra.yfc.util.YFCException;

public class TestDownloadFileFromDB extends AbstractCustomApi {

	public YFCDocument invoke(YFCDocument indoc) {
		return downloadExcel(indoc);
	}

	private YFCDocument downloadExcel(YFCDocument indoc) {
		
		String sExcelFolder = indoc.getDocumentElement().getAttribute("ExcelFolder", YFCConfigurator.getInstance().getProperty("si_config", "INSTALL_DIR","")+File.separator+"logs");
		
		YFCDocument dExcelList = invokeYantraService("GpsGetFileUploadList", indoc);
		YFCElement eExcelList = dExcelList.getDocumentElement();
		YFCIterable<YFCElement> childrenFileUpload = eExcelList.getChildren("FileUpload");
		for(YFCElement eFileUpload : childrenFileUpload){
			String sFileName = eFileUpload.getAttribute("TransactionType") + "_" + eFileUpload.getAttribute("FileUploadKey");
			String fileFullPath = sExcelFolder + File.separator + sFileName;
			try {
				String strEncodedExcel = eFileUpload.getAttribute("FileObject");
				
				byte[] decodedBytes = Base64.decodeBase64(strEncodedExcel.getBytes());
				InputStream file = new ByteArrayInputStream(decodedBytes);
				Workbook workbook = WorkbookFactory.create(file);

				if("Y".equalsIgnoreCase(indoc.getDocumentElement().getAttribute("WriteExcel","Y"))){
					writeExcelFile(workbook,fileFullPath,eFileUpload);
				}
				
				if("Y".equalsIgnoreCase(indoc.getDocumentElement().getAttribute("SendEMail", "N"))){
					sendEmail(eFileUpload, sFileName, strEncodedExcel, workbook);
				}
				
				eFileUpload.removeAttribute("FileObject");
				
			} catch (InvalidFormatException | MessagingException | IOException e) {
				throw new YFCException(e);
			} 
		}
		return dExcelList;
		
	}

	/**
	 * @param eFileUpload
	 * @param sFileName
	 * @param strEncodedExcel
	 * @param workbook
	 * @throws MessagingException
	 * @throws IOException
	 */
	private void sendEmail(YFCElement eFileUpload, String sFileName, String strEncodedExcel, Workbook workbook)
			throws MessagingException, IOException {
		String strToEmailId = eFileUpload.getAttribute("NotifyEmail");
		String strFromEmailId = YFCConfigurator.getInstance().getProperty("gps.email.from.id", TelstraConstants.EMAIL_FROM_ID);
		String strPort =		YFCConfigurator.getInstance().getProperty("gps.email.port", TelstraConstants.EMAIL_PORT);
		String strHost =		YFCConfigurator.getInstance().getProperty("gps.email.hostname", TelstraConstants.EMAIL_HOST);

		String sFileExtension = ".xls";
		if(workbook instanceof org.apache.poi.xssf.usermodel.XSSFWorkbook){
			sFileExtension = ".xlsx";
		} 
		
		if(!YFCCommon.isVoid(strToEmailId)){
			ExcelUtil.sendExcelAttachmentEmail(strEncodedExcel.getBytes(), strToEmailId, strFromEmailId, sFileName, sFileExtension, strPort, strHost);
			eFileUpload.setAttribute("EMailSent", "Y");
		}
	}

	private void writeExcelFile(Workbook workbook, String fileFullPath, YFCElement eFileUpload) throws InvalidFormatException, IOException {
		
		String filetoWrite = "";
		if(workbook instanceof org.apache.poi.xssf.usermodel.XSSFWorkbook){
			filetoWrite = fileFullPath+".xlsx";
		} else {
			filetoWrite = fileFullPath+".xls";
		} 
		
		FileOutputStream fileOut = new FileOutputStream(filetoWrite);
		
		workbook.write(fileOut);
		
		fileOut.close();
		
		eFileUpload.setAttribute("FileDownloadedPath", filetoWrite);
		
	}
}